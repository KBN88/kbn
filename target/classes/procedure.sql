
DELIMITER $$
CREATE PROCEDURE `acquirer_authoriseSummary`(IN fromdate varchar(250),IN todate varchar(250),
IN merchantPayId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
IF (fromdate = '') THEN
   Select * from (select t1.CREATE_DATE,t1.TXN_ID,t1.AMOUNT,t1.CUST_EMAIL,t1.PAYMENT_TYPE,t1.MOP_TYPE,t1.TXNTYPE,t1.PAY_ID,su.businessName, 
			t1.Status as 'STATUS',t1.ORDER_ID,t1.CURRENCY_CODE,t1.INTERNAL_CARD_ISSUER_BANK,t1.INTERNAL_CARD_ISSUER_COUNTRY,
			(select TXN_ID from TRANSACTION where ORIG_TXN_ID = t1.TXN_ID and TXNType='capture' and status='captured' ) as CAPTURE_TXN_ID,
			t1.RESPONSE_MESSAGE,t1.CUST_NAME,t1.CARD_MASK,t1.PRODUCT_DESC
			FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
			where (t1.TXNTYPE = 'AUTHORISE') and (status ='Approved')
			and (CASE when (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when t1.PAY_ID = merchantPayId then 1 else 0 END) END)
			and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
			and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
			and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END)) temp
	        where CAPTURE_TXN_ID is null order by CREATE_DATE desc;
ELSE
  Select * from (select t1.CREATE_DATE,t1.TXN_ID,t1.AMOUNT,t1.CUST_EMAIL,t1.PAYMENT_TYPE,t1.MOP_TYPE,t1.TXNTYPE,t1.PAY_ID,su.businessName, 
			t1.Status as 'STATUS',t1.ORDER_ID,t1.CURRENCY_CODE,t1.INTERNAL_CARD_ISSUER_BANK,t1.INTERNAL_CARD_ISSUER_COUNTRY,
			(select TXN_ID from TRANSACTION where ORIG_TXN_ID = t1.TXN_ID and TXNType='capture' and status='captured' ) as CAPTURE_TXN_ID,
			t1.RESPONSE_MESSAGE,t1.CUST_NAME,t1.CARD_MASK,t1.PRODUCT_DESC
			FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
			where (t1.CREATE_DATE between fromdate and todate) 
			and (t1.TXNTYPE = 'AUTHORISE') and (status ='Approved')
			and (CASE when (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when t1.PAY_ID = merchantPayId then 1 else 0 END) END)
			and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
			and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
			and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END)) temp
	        where CAPTURE_TXN_ID is null order by CREATE_DATE desc;
            END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `acquirer_countRefundReport`(IN fromdate datetime, IN todate datetime,IN payid varchar(250),
IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
Select count(*) recordsTotal
	   from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId 
       left join Settlement se on t1.TXN_ID = se.txnId where t1.ORIG_TXN_ID in (Select distinct t1.TXN_ID  from TRANSACTION t1 where (txntype = 'SALE' AND t1.status = 'Captured')
	   OR (t1.txntype = 'AUTHORISE' AND t1.status = 'Approved')) and (t1.TXNTYPE = 'REFUND' and t1.STATUS = 'Captured')
	   and (t1.CREATE_DATE between fromdate and todate) 
	   and (CASE when (payid = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when t1.PAY_ID = payid then 1 else 0 END) END)
	   and (CASE when paymenttype='ALL' then 1 else (CASE when (select count(Payment_Type) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID and PAYMENT_TYPE = paymenttype) >0  then 1 else 0 END) END)
	   and  (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
	   and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END);
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `acquirer_failedSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId varchar(250),
IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250) ,IN start int,IN length int)
BEGIN
  if((merchantPayId = 'ALL')) then
		   SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
                `t1`.`INTERNAL_CARD_ISSUER_BANK` AS `INTERNAL_CARD_ISSUER_BANK`,
                `t1`.`INTERNAL_CARD_ISSUER_COUNTRY` AS `INTERNAL_CARD_ISSUER_COUNTRY`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                IFNULL(`t1`.`PG_TXN_MESSAGE`, '') AS `PG_TXN_MESSAGE`
		   FROM (`TRANSACTION` `t1`
		   LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;   
   ELSE
		 SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
                `t1`.`INTERNAL_CARD_ISSUER_BANK` AS `INTERNAL_CARD_ISSUER_BANK`,
                `t1`.`INTERNAL_CARD_ISSUER_COUNTRY` AS `INTERNAL_CARD_ISSUER_COUNTRY`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                IFNULL(`t1`.`PG_TXN_MESSAGE`, '') AS `PG_TXN_MESSAGE`
			FROM (`TRANSACTION` `t1`
		   LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
		   and t1.PAY_ID = merchantPayId 
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END)  order by t1.TXN_ID DESC  LIMIT start,length;  
	       END IF;   
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `acquirer_refundReport`(IN fromdate datetime, IN todate datetime,IN payid varchar(250),
IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN start int,IN length int)
BEGIN
	Select t1.ORDER_ID, t1.TXN_ID as 'TXN_ID', t1.PAY_ID, su.businessName As 'BUSINESS_NAME', CURRENCY_CODE,t1.INTERNAL_USER_EMAIL,t1.INTERNAL_CUST_IP,t1.CUST_EMAIL, se.status as STATUS, (Select min(CREATE_DATE) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'TXN_DATE',
     	   t1.CREATE_DATE as 'REFUND_DATE', t1.AMOUNT as 'REFUND_AMOUNT', (Select min(Amount) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'TXN_AMOUNT', (Select min(Amount) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) - (Select IFNULL(sum(AMOUNT),0.00) from TRANSACTION where OID = t1.OID and TxnType = 'REFUND' and status ='Captured') As 'REFUNDABLE_AMOUNT',
	      (Select Payment_Type from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'PAYMENT_TYPE', (Select MOP_TYPE from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'MOP_TYPE'
	from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId 
    left join Settlement se on t1.TXN_ID = se.txnId where t1.ORIG_TXN_ID in (Select distinct t1.TXN_ID  from TRANSACTION t1 where (txntype = 'SALE' AND t1.status = 'Captured')
	OR (t1.txntype = 'AUTHORISE' AND t1.status = 'Approved')) and (t1.TXNTYPE = 'REFUND' and t1.STATUS = 'Captured')
	and (t1.CREATE_DATE between fromdate and todate) 
	and (CASE when (payid = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when t1.PAY_ID = payid then 1 else 0 END) END)
	and (CASE when paymenttype='ALL' then 1 else (CASE when (select count(Payment_Type) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID and PAYMENT_TYPE = paymenttype) >0  then 1 else 0 END) END)
	and  (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
	and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `acquirer_transactionsAllMerch`(IN fromdate datetime, IN todate datetime,IN payId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN userType varchar(250) ,IN start int,IN length int)
BEGIN
	SELECT 
        `t1`.`ORIG_TXN_ID` AS `OID`,
        `t1`.`CREATE_DATE` AS `CREATE_DATE`,
        `t1`.`TXN_ID` AS `TXN_ID`,
        `t1`.`ORDER_ID` AS `ORDER_ID`,
        `t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
        `t1`.`PAY_ID` AS `PAY_ID`,
        `t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
        `t1`.`PAYMENT_TYPE` AS `PAYMENT_TYPE`,
        `t1`.`MOP_TYPE` AS `MOP_TYPE`,
        `t1`.`TXNTYPE` AS `TXNTYPE`,
        `t1`.`CURRENCY_CODE` AS `CURRENCY_CODE`,
        `t1`.`AUTH_CODE` AS `AUTH_CODE`,
        `su`.`businessName` AS `businessName`,
        `t1`.`AMOUNT` AS `CAPTURED_AMOUNT`,
        `se`.`status` as STATUS,
        '0.00' AS `REFUND_AMOUNT`,
        '0.00' AS `CHARGEBACK_AMOUNT`
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
        left join Settlement se on TXN_ID = se.txnId
    where (CREATE_DATE between fromdate and todate) and ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'AUTHORISE' AND `t1`.`STATUS` = 'Approved'))
    and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and (CASE when (userType='ACQUIRER' or userType='SUBACQUIRER') and (payId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END) order by t1.TXN_ID DESC   LIMIT start,length;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `acquirerBarChart`(IN acquirer varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
  select  totalCredit,totalDebit,net,other from ((Select count(*) as totalCredit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (t1.ACQUIRER_TYPE = acquirer) and (t1.CURRENCY_CODE= currency) and ((t1.MOP_TYPE='AX') or (t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='DN'))  and (t1.PAYMENT_TYPE='CC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) totalCredit,
	     (Select count(*) as totalDebit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured') or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved') ) and (t1.ACQUIRER_TYPE = acquirer) and (t1.CURRENCY_CODE= currency) and ((t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='MS')) and (t1.PAYMENT_TYPE='DC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) totalDebit,
	     (Select count(*) as net from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured'))  and (t1.ACQUIRER_TYPE = acquirer) and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) net,
	     (Select count(*) as other from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.ACQUIRER_TYPE = acquirer)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) other);

END$$
DELIMITER ;


DELIMITER $$
CREATE  PROCEDURE `acquirer_CapturedSummary`(IN fromdate datetime, IN todate datetime,
IN merchantPayId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN start int,IN length int)
BEGIN
SELECT t1.CREATE_DATE,t1.TXN_ID,t1.AMOUNT,t1.CURRENCY_CODE,t1.INTERNAL_CARD_ISSUER_BANK,t1.INTERNAL_CARD_ISSUER_COUNTRY,
t1.CUST_EMAIL,t1.PAYMENT_TYPE,t1.MOP_TYPE,t1.TXNTYPE,t1.PAY_ID,t1.Status as 'STATUS' ,t1.ORDER_ID,t1.RESPONSE_MESSAGE,t1.CUST_NAME,t1.ACQUIRER_TYPE,t1.ORIG_TXN_ID,
t1.CARD_MASK,t1.PRODUCT_DESC, su.businessName,t1.AMOUNT - (Select  IFNULL(sum(AMOUNT),0.00) from TRANSACTION where OID = t1.OID and TxnType = 'REFUND' and status ='Captured') As 'RefundableAmount'
   FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId 
   where (CREATE_DATE between fromdate and todate) 
   and  (TxnType IN ('SALE','CAPTURE') and status ='Captured') and (CASE when (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = merchantPayId then 1 else 0 END) END)
   and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
   and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
   and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
   order by t1.TXN_ID DESC   LIMIT start,length;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `acquirer_IncompleteSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
varchar(250),IN paymenttype varchar(250),IN status varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN start int,IN length int)
BEGIN
   if((merchantPayId = 'ALL')) then
		   SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                `bd`.`CUST_PHONE` AS `CUST_PHONE`
			FROM
				(`TRANSACTION` `t1`
		   LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))  
           LEFT JOIN `BILLING_DETAILS` `bd` ON (`t1`.`TXN_ID` = `bd`.`TXN_ID`)
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank'))
           or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;
   ELSE
			SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                `bd`.`CUST_PHONE` AS `CUST_PHONE` 
			FROM
				(`TRANSACTION` `t1`
			LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
			LEFT JOIN `BILLING_DETAILS` `bd` ON (`t1`.`TXN_ID` = `bd`.`TXN_ID`)
		    where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank'))
		    or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
            and t1.PAY_ID = merchantPayId 
		    and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		    and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		    and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		    and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;  
	       END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `acquirerLineChart`(IN acquirer varchar(250),IN currency varchar(250),
IN dateFrom datetime, IN dateTo datetime)
BEGIN
	SELECT date(t1.CREATE_DATE) as txndate,t1.CURRENCY_CODE,
	SUM(CASE WHEN (t1.TXNTYPE ='SALE' or t1.TXNTYPE = 'AUTHORISE') and (t1.ACQUIRER_TYPE = acquirer)  and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.ACQUIRER_TYPE = acquirer)and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN 1 ELSE 0 END) totalSuccess,
	SUM(CASE WHEN ((t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (t1.ACQUIRER_TYPE = acquirer) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN 1 ELSE 0 END) totalRefunded,
    SUM(CASE WHEN (((t1.TXNTYPE ='NEWORDER')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) and (t1.ACQUIRER_TYPE = acquirer) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalFailed
	FROM TRANSACTION t1 where ((t1.CREATE_DATE between dateFrom and dateTo)  and  t1.ACQUIRER_TYPE = acquirer and t1.PAYMENT_TYPE is not NULL) and (t1.CURRENCY_CODE = currency)
	group by DAYOFMONTH(t1.CREATE_DATE),t1.CURRENCY_CODE
	order by t1.CREATE_DATE ASC; 
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `acquirerPieChart`(IN acquirer varchar(250),IN currency varchar(250),
IN startDate datetime, IN endDate datetime)
BEGIN
        select  visa,mastercard,amex,diners,net,maestro,ezeeClick,other from ((Select count(*) as visa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.ACQUIRER_TYPE = acquirer)  and (t1.MOP_TYPE='VI')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) visa,
		(Select count(*) as mastercard from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='MC') and (t1.ACQUIRER_TYPE = acquirer)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) mastercard,
        (Select count(*) as amex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='AX') and (t1.ACQUIRER_TYPE = acquirer)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) amex,
        (Select count(*) as net from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.ACQUIRER_TYPE = acquirer)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) net,
		(Select count(*) as diners from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='DN') and (t1.ACQUIRER_TYPE = acquirer)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) diners,
		(Select count(*) as maestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='MS')  and (t1.ACQUIRER_TYPE = acquirer) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) maestro,
		(Select count(*) as ezeeClick from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='EZ')   and (t1.ACQUIRER_TYPE = acquirer) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) ezeeClick,
        (Select count(*) as other from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency)  and (t1.ACQUIRER_TYPE = acquirer)  and (t1.PAYMENT_TYPE='WL') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) other);
END$$
DELIMITER ;
        
DELIMITER $$
CREATE  PROCEDURE `analyticsWalletSummary`(IN payId varchar(250),IN currency varchar(250),
IN startDate datetime, IN endDate datetime)
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
       select  totalTransaction,totalSuccess,totalFailed,totalDropped,totalBaunced,totalCancelled from ((Select count(t1.STATUS) as totalTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalTransaction,
       (Select count(*) as totalSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccess,
	   (Select count(*) as totalFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailed,
	   (Select count(*) as totalDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDropped,
	   (Select count(*) as totalBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBaunced,
	   (Select count(*) as totalCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelled);
		
	ELSE
       select  totalTransaction,totalSuccess,totalFailed,totalDropped,totalBaunced,totalCancelled from ((Select count(t1.STATUS) as totalTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalTransaction,
       (Select count(*) as totalSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccess,
	   (Select count(*) as totalFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailed,
	   (Select count(*) as totalDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDropped,
	   (Select count(*) as totalBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBaunced,
	   (Select count(*) as totalCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.CURRENCY_CODE= currency)  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelled);
	   END IF;      
END$$
DELIMITER ;
        
DELIMITER $$
CREATE  PROCEDURE `aquirer_statisticsSummary`(IN acquirer varchar(250),IN currency varchar(250))
BEGIN
select  totalSuccess,totalFailed, totalRefunded,refundedAmount,approvedAmount from ((Select count(*) as totalSuccess from TRANSACTION t1 where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved'))  and (t1.ACQUIRER_TYPE = acquirer) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY)))  totalSuccess, 
        (Select count(*) as totalFailed from TRANSACTION t1 where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed'))  and (t1.ACQUIRER_TYPE = acquirer) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) totalFailed,
		(Select COALESCE(sum(t1.Amount),0) as approvedAmount from TRANSACTION t1 where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved'))  and (t1.ACQUIRER_TYPE = acquirer) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) approvedAmount,
        (Select count(*) as totalRefunded from TRANSACTION t1 where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured')  and (t1.ACQUIRER_TYPE = acquirer) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) totalRefunded,
		(Select  COALESCE(sum(t1.Amount),0) as refundedAmount from TRANSACTION t1 where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured')  and (t1.ACQUIRER_TYPE = acquirer) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) refundedAmount);			
END$$
DELIMITER ;
        
DELIMITER $$
CREATE  PROCEDURE `authoriseSummary`(IN fromdate varchar(250), IN todate varchar(250),IN merchantPayId varchar(250), 
IN userType varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
		IF (fromdate = '') THEN
			Select * from (select t1.CREATE_DATE,t1.TXN_ID,t1.AMOUNT,t1.CUST_EMAIL, 
			t1.PAYMENT_TYPE,t1.MOP_TYPE,t1.TXNTYPE,t1.PAY_ID,su.businessName,t1.Status as 'STATUS',t1.ORDER_ID,t1.CURRENCY_CODE,t1.INTERNAL_CARD_ISSUER_BANK,t1.INTERNAL_CARD_ISSUER_COUNTRY,
			(select TXN_ID from TRANSACTION where ORIG_TXN_ID = t1.TXN_ID and TXNType='capture' and status='captured' ) as CAPTURE_TXN_ID,
			t1.RESPONSE_MESSAGE,t1.CUST_NAME,t1.CARD_MASK,t1.PRODUCT_DESC
			FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
			where 
			(t1.TXNTYPE = 'AUTHORISE') and (status ='Approved')
			and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when t1.PAY_ID = merchantPayId then 1 else 0 END) END)
			and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
			and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
			and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END)) temp
	        where CAPTURE_TXN_ID is null order by CREATE_DATE desc;
ELSE
	Select * from (select t1.CREATE_DATE,t1.TXN_ID,t1.AMOUNT,t1.CUST_EMAIL, 
			t1.PAYMENT_TYPE,t1.MOP_TYPE,t1.TXNTYPE,t1.PAY_ID,su.businessName,t1.Status as 'STATUS',t1.ORDER_ID,t1.CURRENCY_CODE,t1.INTERNAL_CARD_ISSUER_BANK,t1.INTERNAL_CARD_ISSUER_COUNTRY,
			(select TXN_ID from TRANSACTION where ORIG_TXN_ID = t1.TXN_ID and TXNType='capture' and status='captured' ) as CAPTURE_TXN_ID,
			t1.RESPONSE_MESSAGE,t1.CUST_NAME,t1.CARD_MASK,t1.PRODUCT_DESC
			FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
			where (t1.CREATE_DATE between fromdate and todate) 
			and (t1.TXNTYPE = 'AUTHORISE') and (status ='Approved')
			and (CASE when (userType='ADMIN'  or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when t1.PAY_ID = merchantPayId then 1 else 0 END) END)
			and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
			and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
			and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END)) temp
	        where CAPTURE_TXN_ID is null order by CREATE_DATE desc;
	        END IF;
END$$
DELIMITER ;
        
        
DELIMITER $$
CREATE  PROCEDURE `barChartTotalSummary`(IN payId varchar(250),IN currency varchar(250),
IN startDate datetime, IN endDate datetime)
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
      select  totalCredit,totalDebit,net,other from ((Select count(*) as totalCredit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and ((t1.MOP_TYPE='AX') or (t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='DN')) and (t1.PAYMENT_TYPE='CC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCredit,
              (Select count(*) as totalDebit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and ((t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='MS')) and (t1.PAYMENT_TYPE='DC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebit,
              (Select count(*) as net from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) net,
              (Select count(*) as other from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) other);
              
   ELSE
       select  totalCredit,totalDebit,net,other from ((Select count(*) as totalCredit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and ((t1.MOP_TYPE='AX') or (t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='DN'))  and (t1.PAYMENT_TYPE='CC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) totalCredit,
		         (Select count(*) as totalDebit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured') or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved') ) and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and ((t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='MS')) and (t1.PAYMENT_TYPE='DC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) totalDebit,
                 (Select count(*) as net from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) net,
		         (Select count(*) as other from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) other);
	             END IF;   
END$$
DELIMITER ;
        
        
DELIMITER $$
CREATE  PROCEDURE `cancelledSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId varchar(250),
IN userType varchar(250), IN currency varchar(250),IN start int,IN length int)
BEGIN
   if((userType='ADMIN'  or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL')) then
		   SELECT 
				`t1`.`OID` AS `OID`,
                IFNULL(`t1`.`ORIG_TXN_ID`, '') AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`
		FROM
				(`TRANSACTION` `t1`
		   LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled'))
           and (su.userType <> 'POSMERCHANT')
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC   LIMIT start,length;   
   ELSE
			SELECT 
				`t1`.`OID` AS `OID`,
                IFNULL(`t1`.`ORIG_TXN_ID`, '') AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`
		   FROM
				(`TRANSACTION` `t1`
		   LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled'))
		   and t1.PAY_ID = merchantPayId 
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;  
	       END IF;   
END$$
DELIMITER ;
        
DELIMITER $$
CREATE  PROCEDURE `capturedSummary`(IN fromdate datetime, IN todate datetime,
IN merchantPayId varchar(250), IN userType varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),
IN currency varchar(250),IN start int,IN length int)
BEGIN
SELECT t1.CREATE_DATE,
t1.TXN_ID,t1.AMOUNT,t1.CURRENCY_CODE,
t1.INTERNAL_CARD_ISSUER_BANK,t1.INTERNAL_CARD_ISSUER_COUNTRY,
t1.CUST_EMAIL,
t1.PAYMENT_TYPE,
t1.MOP_TYPE,
t1.TXNTYPE,
t1.PAY_ID,t1.Status as 'STATUS' ,
t1.ORDER_ID,t1.RESPONSE_MESSAGE,t1.CUST_NAME,t1.ACQUIRER_TYPE,t1.ORIG_TXN_ID,
t1.CARD_MASK,t1.PRODUCT_DESC, su.businessName,t1.AMOUNT - (Select  IFNULL(sum(AMOUNT),0.00) from TRANSACTION where OID = t1.OID and TxnType = 'REFUND' and status ='Captured') As 'RefundableAmount'
   FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId 
   where (CREATE_DATE between fromdate and todate) 
   and  (TxnType IN ('SALE','CAPTURE') and status ='Captured') and (CASE when (userType='ADMIN' or userType='SUBADMIN'  or userType='SUPERADMIN') and (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = merchantPayId then 1 else 0 END) END)
   and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
   and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
   and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
   order by t1.TXN_ID DESC   LIMIT start,length;
END$$
DELIMITER ;
        
        
DELIMITER $$
CREATE  PROCEDURE `chartTotalSummary`(IN payId varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
    select  visa,mastercard,amex,diners,net,maestro,ezeeClick,other from ((Select count(*) as visa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='VI')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) visa,
		(Select count(*) as mastercard from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='MC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) mastercard,
        (Select count(*) as amex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='AX')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) amex,
        (Select count(*) as net from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) net,
		(Select count(*) as diners from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='DN')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) diners,
		(Select count(*) as maestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='MS')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) maestro,
		(Select count(*) as ezeeClick from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='EZ')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) ezeeClick,
		(Select count(*) as other from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) other);
        
	ELSE
        select  visa,mastercard,amex,diners,net,maestro,ezeeClick,other from ((Select count(*) as visa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId)  and (t1.MOP_TYPE='VI')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) visa,
		(Select count(*) as mastercard from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='MC') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) mastercard,
        (Select count(*) as amex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='AX') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) amex,
        (Select count(*) as net from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) net,
		(Select count(*) as diners from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) diners,
		(Select count(*) as maestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='MS')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) maestro,
		(Select count(*) as ezeeClick from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.MOP_TYPE='EZ')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) ezeeClick,
        (Select count(*) as other from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId)  and (t1.PAYMENT_TYPE='WL') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) other);
         END IF;          
 END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `count_reseller_searchPayment`(IN fromdate datetime, IN todate datetime,IN transactionId varchar(250),
IN orderId varchar(250),IN customerEmail varchar(250),IN payId varchar(250),
IN paymenttype varchar(250),IN  Userstatus varchar(250),IN currency varchar(250),IN resellerId varchar(250),IN userType varchar(250))
BEGIN
	SELECT count(*) recordsTotal
    FROM `TRANSACTION` `transaction` left join User subuser on PAY_ID = subuser.payId
	where (CREATE_DATE >= fromdate and CREATE_DATE  <= todate) 
    and ((`transaction`.`TXNTYPE` in ('SALE','REFUND') and `transaction`.`STATUS` = 'Captured') 
    or 	(`transaction`.`TXNTYPE` = 'NEWORDER' and `transaction`.`STATUS` IN ('Rejected','Cancelled'))
    or 	(`transaction`.`TXNTYPE` = 'AUTHORISE' and `transaction`.`STATUS` = 'Approved') 
	or (`transaction`.`TXNTYPE` = 'ENROLL' and `transaction`.`STATUS` IN ( 'Timeout','Enrolled') )
    or  (`transaction`.`TXNTYPE` in ('SALE','AUTHORISE') and `transaction`.`status` IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed','Invalid','Pending','Timeout','Browser Closed','Sent to Bank'))
    or 	((`transaction`.`TXNTYPE` = 'ENROLL' and transaction.status in ('Invalid')))) 
	and (CASE when transactionId='' then 1 else (CASE when TXN_ID = transactionId then 1 else 0 END) END)
    and (CASE when orderId='' then 1 else (CASE when ORDER_ID = orderId then 1 else 0 END) END)
	and (CASE when customerEmail='' then 1 else (CASE when CUST_EMAIL = customerEmail then 1 else 0 END) END)
	and (CASE when paymenttype='' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when (subuser.resellerId=resellerId) and (payId = 'ALL') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when Userstatus='' then 1 else (CASE when status = Userstatus then 1 else 0 END) END)
    and (CASE when currency='' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and (CASE when ((userType='ADMIN'  or userType='SUBADMIN' or userType='SUPERADMIN' or userType='RESELLER') and (payId = 'ALL') and (subuser.userType <> 'POSMERCHANT'))then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END);
 END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `count_resellerCancelledSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId varchar(250),  IN resellerId varchar(250),IN currency varchar(250))
BEGIN
	if(merchantPayId = 'ALL') then
		   SELECT count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
		   LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled'))
		   and su.resellerId=resellerId
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END); 
   ELSE
			SELECT count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
		   LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled'))
		   and t1.PAY_ID = merchantPayId and su.resellerId=resellerId
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);
	       END IF;        
END$$
DELIMITER ;
        
DELIMITER $$
CREATE  PROCEDURE `count_resellerCapturedSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId varchar(250),IN resellerId varchar(250),
IN paymenttype varchar(250),IN acquirer varchar(250), IN currency varchar(250))
BEGIN
SELECT count(*) recordsTotal
   FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId 
   where (CREATE_DATE between fromdate and todate) 
   and  (TxnType = 'SALE' and status ='Captured') and (CASE when (su.resellerId=resellerId) and (merchantPayId = 'ALL') then 1 else (CASE when PAY_ID = merchantPayId then 1 else 0 END) END)
   and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
   and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
   and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END);
END$$
DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `count_resellerFailedSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN resellerId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
  if(merchantPayId = 'ALL') then
		   SELECT  count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
         and su.resellerId=resellerId
         and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);
   ELSE
			SELECT  count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
		   and t1.PAY_ID = merchantPayId and su.resellerId=resellerId
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);
	END IF;   
 END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `count_resellerIncompleteSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250),IN resellerId varchar(250),IN paymenttype varchar(250),IN status varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
 if(merchantPayId = 'ALL') then
		   SELECT count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank'))
           or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
		    and su.resellerId=resellerId
           and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);
   ELSE
			SELECT  count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank'))
		   or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
           and t1.PAY_ID = merchantPayId and su.resellerId=resellerId
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);  
	END IF;   
   END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `count_resellerTransactionsAllMerch`(IN fromdate datetime, IN todate datetime,IN payId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN resellerId varchar(250))
BEGIN
 if(payId = 'ALL') then
	SELECT count(*) recordsTotal
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
    where (CREATE_DATE between fromdate and todate) and ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'AUTHORISE' AND `t1`.`STATUS` = 'Approved'))
    and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and su.resellerId=resellerId;
   ELSE
			SELECT count(*) recordsTotal
			FROM
			  `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
    where (CREATE_DATE between fromdate and todate) and ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'AUTHORISE' AND `t1`.`STATUS` = 'Approved'))
    and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
    and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
     and t1.PAY_ID = payId and su.resellerId=resellerId; 
	END IF;   
   END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `countAcquirer_FailedSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
  if((merchantPayId = 'ALL')) then
		   SELECT count(*) recordsTotal
			FROM 
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);   
   ELSE
			SELECT count(*) recordsTotal
			
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
		   and t1.PAY_ID = merchantPayId 
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);  
	END IF;   
   END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `countAcquirer_TransactionsAllMerch`(IN fromdate datetime, IN todate datetime,IN payId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN userType varchar(250))
BEGIN
	SELECT count(*) recordsTotal
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
        left join Settlement se on TXN_ID = se.txnId
    where (CREATE_DATE between fromdate and todate) and ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'AUTHORISE' AND `t1`.`STATUS` = 'Approved'))
    and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and (CASE when (userType='SUBACQUIRER' or userType='ACQUIRER') and (payId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END);
END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `count_AcquirerCaptured`(IN fromdate datetime, IN todate datetime,
IN merchantPayId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),
IN currency varchar(250))
BEGIN
SELECT count(*) recordsTotal
   FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
   where (CREATE_DATE between fromdate and todate) and
   (TxnType IN ('SALE','CAPTURE') and status ='Captured') and (CASE when (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = merchantPayId then 1 else 0 END) END)
   and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
   and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
   and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END);
   END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `count_AcquirerIncompleteSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250),IN paymenttype varchar(250),IN status varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
   if((merchantPayId = 'ALL')) then
		   SELECT count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))  
                LEFT JOIN `BILLING_DETAILS` `bd` ON (`t1`.`TXN_ID` = `bd`.`TXN_ID`)
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank'))
           or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);
   ELSE
			SELECT count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
				LEFT JOIN `BILLING_DETAILS` `bd` ON (`t1`.`TXN_ID` = `bd`.`TXN_ID`)
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank'))
		   or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
           and t1.PAY_ID = merchantPayId 
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);  
	END IF;
   END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `countCancelledSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId varchar(250), IN userType varchar(250), IN currency varchar(250))
BEGIN
	
   if((userType='ADMIN'  or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL')) then
		   SELECT count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled'))
           and (su.userType <> 'POSMERCHANT')
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);   
   ELSE
			SELECT count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled'))
		   and t1.PAY_ID = merchantPayId 
		  and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);  
	END IF;   
     END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `countCaptured`(IN fromdate datetime, IN todate datetime,
IN merchantPayId varchar(250), IN userType varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),
IN currency varchar(250))
BEGIN
SELECT count(*) recordsTotal
   FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
   where (CREATE_DATE between fromdate and todate) and
   (TxnType IN ('SALE','CAPTURE') and status ='Captured') and (CASE when (userType='ADMIN' or userType='SUBADMIN'  or userType='SUPERADMIN') and (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = merchantPayId then 1 else 0 END) END)
   and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
   and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
   and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END);
     END$$
 DELIMITER ;
 
 
 
 
DELIMITER $$
CREATE  PROCEDURE `countFailedSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN userType varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
  if((userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL')) then
		   SELECT count(*) recordsTotal
			FROM 
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
           and (su.userType <> 'POSMERCHANT')
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);   
   ELSE
			SELECT count(*) recordsTotal
			
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
		   and t1.PAY_ID = merchantPayId 
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);  
	END IF;   
     END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `countIncompleteSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN userType varchar(250),IN paymenttype varchar(250),IN status varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
   if((userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL')) then
		   SELECT count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))  
                LEFT JOIN `BILLING_DETAILS` `bd` ON (`t1`.`TXN_ID` = `bd`.`TXN_ID`)
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank'))
           or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
           and (su.userType <> 'POSMERCHANT')
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);
   ELSE
			SELECT count(*) recordsTotal
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
				LEFT JOIN `BILLING_DETAILS` `bd` ON (`t1`.`TXN_ID` = `bd`.`TXN_ID`)
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank'))
		   or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
           and t1.PAY_ID = merchantPayId 
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);  
	END IF;
    END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `countMisReports`(IN dateFrom varchar(250),IN dateTo varchar(250),IN payId varchar(250), IN acquirer varchar(250),IN userType varchar(250))
BEGIN
	SELECT count(*) recordsTotal
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
    where (CREATE_DATE between dateFrom and dateTo)  and   ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'REFUND' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'CAPTURE' AND `t1`.`STATUS` = 'Captured'))
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL'  then ACQUIRER_TYPE not in('CITRUS') else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END);
   END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `countRefundReport`(IN fromdate datetime, IN todate datetime,IN payid varchar(250), IN userType varchar(250),
IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
	Select count(*) recordsTotal
	 from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId 
     left join Settlement se on t1.TXN_ID = se.txnId where t1.ORIG_TXN_ID in (Select distinct t1.TXN_ID  from TRANSACTION t1 where (txntype = 'SALE' AND t1.status = 'Captured')
	OR (t1.txntype = 'AUTHORISE' AND t1.status = 'Approved')) and (t1.TXNTYPE = 'REFUND' and t1.STATUS = 'Captured')
	and (t1.CREATE_DATE between fromdate and todate) 
	and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payid = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when t1.PAY_ID = payid then 1 else 0 END) END)
	and (CASE when paymenttype='ALL' then 1 else (CASE when (select count(Payment_Type) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID and PAYMENT_TYPE = paymenttype) >0  then 1 else 0 END) END)
	and  (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
	and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END);
 END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `countSearchPayment`(IN fromdate datetime, IN todate datetime,IN transactionId varchar(250),
IN orderId varchar(250),IN customerEmail varchar(250),IN payId varchar(250),
IN paymenttype varchar(250),IN  Userstatus varchar(250),IN currency varchar(250),IN userType varchar(250))
BEGIN
	SELECT count(*) recordsTotal
    FROM `TRANSACTION` `transaction` left join User subuser on PAY_ID = subuser.payId
	where (CREATE_DATE >= fromdate and CREATE_DATE  <= todate) 
    and ((`transaction`.`TXNTYPE` in ('SALE','REFUND') and `transaction`.`STATUS` = 'Captured') 
    or 	(`transaction`.`TXNTYPE` = 'NEWORDER' and `transaction`.`STATUS` IN ('Rejected','Cancelled'))
    or 	(`transaction`.`TXNTYPE` = 'AUTHORISE' and `transaction`.`STATUS` = 'Approved') 
	or (`transaction`.`TXNTYPE` = 'ENROLL' and `transaction`.`STATUS` IN ( 'Timeout','Enrolled') )
    or  (`transaction`.`TXNTYPE` in ('SALE','AUTHORISE') and `transaction`.`status` IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed','Invalid','Pending','Timeout','Browser Closed','Sent to Bank'))
    or 	((`transaction`.`TXNTYPE` = 'ENROLL' and transaction.status in ('Invalid')))) 
	and (CASE when transactionId='' then 1 else (CASE when TXN_ID = transactionId then 1 else 0 END) END)
    and (CASE when orderId='' then 1 else (CASE when ORDER_ID = orderId then 1 else 0 END) END)
	and (CASE when customerEmail='' then 1 else (CASE when CUST_EMAIL = customerEmail then 1 else 0 END) END)
	and (CASE when paymenttype='' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when Userstatus='' then 1 else (CASE when status = Userstatus then 1 else 0 END) END)
    and (CASE when currency='' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and (CASE when ((userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payId = 'ALL') and (subuser.userType <> 'POSMERCHANT'))then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END);
 END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `countTransactionsAllMerch`(IN fromdate datetime, IN todate datetime,IN payId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN userType varchar(250))
BEGIN
	SELECT count(*) recordsTotal
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
        left join Settlement se on TXN_ID = se.txnId
    where (CREATE_DATE between fromdate and todate) and ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'AUTHORISE' AND `t1`.`STATUS` = 'Approved'))
    and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END);
 END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `creditCardsAnalticsReport`(IN payId varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
IF (payId = 'ALL MERCHANTS') THEN
       select  totalCreditTransaction,totalCreditSuccess,totalCreditFailed,totalCreditDropped,totalCreditBaunced,totalCreditCancelled,
       totalDebitTransaction,totalDebitSuccess,totalDebitFailed,totalDebitDropped,totalDebitBaunced,totalDebitCancelled,
       totalNetBankTransaction,totalNetBankSuccess,totalNetBankFailed,totalNetBankDropped,totalNetBankBaunced,totalNetBankCancelled,
       totalWalletTransaction,totalWalletSuccess,totalWalletFailed,totalWalletDropped,totalWalletBaunced,totalWalletCancelled
       from ((Select count(t1.STATUS) as totalCreditTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT')  and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditTransaction,
       (Select count(*) as totalCreditSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT')  and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditSuccess,
	   (Select count(*) as totalCreditFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.PAYMENT_TYPE='CC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditFailed,
	   (Select count(*) as totalCreditDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditDropped,
	   (Select count(*) as totalCreditBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (t1.PAYMENT_TYPE='CC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditBaunced,
	   (Select count(*) as totalCreditCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE IN ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC')   and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditCancelled),
       ((Select count(t1.STATUS) as totalDebitTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitTransaction,
       (Select count(*) as totalDebitSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitSuccess,
	   (Select count(*) as totalDebitFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitFailed,
	   (Select count(*) as totalDebitDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER'  and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitDropped,
	   (Select count(*) as totalDebitBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitBaunced,
	   (Select count(*) as totalDebitCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE IN ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')   and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitCancelled),
       ((Select count(t1.STATUS) as totalNetBankTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankTransaction,
       (Select count(*) as totalNetBankSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankSuccess,
	   (Select count(*) as totalNetBankFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankFailed,
	   (Select count(*) as totalNetBankDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankDropped,
	   (Select count(*) as totalNetBankBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankBaunced,
	   (Select count(*) as totalNetBankCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankCancelled),
       ((Select count(t1.STATUS) as totalWalletTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletTransaction,
       (Select count(*) as totalWalletSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletSuccess,
	   (Select count(*) as totalWalletFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletFailed,
	   (Select count(*) as totalWalletDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletDropped,
	   (Select count(*) as totalWalletBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletBaunced,
	   (Select count(*) as totalWalletCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletCancelled);
	   
	   ELSE
	   
     select  totalCreditTransaction,totalCreditSuccess,totalCreditFailed,totalCreditDropped,totalCreditBaunced,totalCreditCancelled,
             totalDebitTransaction,totalDebitSuccess,totalDebitFailed,totalDebitDropped,totalDebitBaunced,totalDebitCancelled,
			 totalNetBankTransaction,totalNetBankSuccess,totalNetBankFailed,totalNetBankDropped,totalNetBankBaunced,totalNetBankCancelled,
			 totalWalletTransaction,totalWalletSuccess,totalWalletFailed,totalWalletDropped,totalWalletBaunced,totalWalletCancelled,
			 totalWalletTransaction,totalWalletSuccess,totalWalletFailed,totalWalletDropped,totalWalletBaunced,totalWalletCancelled
        from ((Select count(t1.STATUS) as totalCreditTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT')  and (t1.PAYMENT_TYPE='CC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditTransaction,
       (Select count(*) as totalCreditSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT')  and (t1.PAYMENT_TYPE='CC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditSuccess,
	   (Select count(*) as totalCreditFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditFailed,
	   (Select count(*) as totalCreditDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER'  and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')   and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditDropped,
	   (Select count(*) as totalCreditBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')  and (t1.PAYMENT_TYPE='CC')  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditBaunced,
	   (Select count(*) as totalCreditCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE IN ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC')   and (t1.CURRENCY_CODE= currency)  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditCancelled),
       ((Select count(t1.STATUS) as totalDebitTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitTransaction,
       (Select count(*) as totalDebitSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitSuccess,
	   (Select count(*) as totalDebitFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.PAYMENT_TYPE='DC')   and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitFailed,
	   (Select count(*) as totalDebitDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER'  and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')   and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitDropped,
	   (Select count(*) as totalDebitBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitBaunced,
	   (Select count(*) as totalDebitCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE IN ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')   and (t1.CURRENCY_CODE= currency)  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitCancelled),
       ((Select count(t1.STATUS) as totalNetBankTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankTransaction,
       (Select count(*) as totalNetBankSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankSuccess,
	   (Select count(*) as totalNetBankFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankFailed,
	   (Select count(*) as totalNetBankDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankDropped,
	   (Select count(*) as totalNetBankBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankBaunced,
	   (Select count(*) as totalNetBankCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.CURRENCY_CODE= currency)  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankCancelled),
       ((Select count(t1.STATUS) as totalTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalTransaction,
       (Select count(*) as totalSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccess,
	   (Select count(*) as totalFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailed,
	   (Select count(*) as totalDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDropped,
	   (Select count(*) as totalBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBaunced,
	   (Select count(*) as totalCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelled),
       ((Select count(t1.STATUS) as totalWalletTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletTransaction,
       (Select count(*) as totalWalletSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletSuccess,
	   (Select count(*) as totalWalletFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletFailed,
	   (Select count(*) as totalWalletDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletDropped,
	   (Select count(*) as totalWalletBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletBaunced,
	   (Select count(*) as totalWalletCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='WL') and (t1.CURRENCY_CODE= currency)  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletCancelled);
		
    END IF;      
 END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `creditCardsMopTypeSummary`(IN payId varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
       select  totalVisa,totalSuccessVisa,totalFailedVisa,totalDroppedVisa,totalBauncedVisa,totalCancelledVisa,
       totalMaster,totalSuccessMaster,totalFailedMaster,totalDroppedMaster,totalBauncedMaster,totalCancelledMaster,
       totalAmex,totalSuccessAmex,totalFailedAmex,totalDroppedAmex,totalBauncedAmex,totalCancelledAmex, 
       totalMestro,totalSuccessMestro,totalFailedMestro,totalDroppedMestro,totalBauncedMestro,totalCancelledMestro,
	  totalDiner,totalSuccessDiner,totalFailedDiner,totalDroppedDiner,totalBauncedDiner,totalCancelledDiner from ((Select count(t1.STATUS) as totalVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='VI')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalVisa,
       (Select count(*) as totalSuccessVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId  where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured') or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='VI') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessVisa,
	   (Select count(*) as totalFailedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='VI') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedVisa,
	   (Select count(*) as totalDroppedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='VI') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedVisa,
	   (Select count(*) as totalBauncedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='VI') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedVisa,
	   (Select count(*) as totalCancelledVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='VI')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledVisa,
       (Select count(t1.STATUS) as totalMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and  (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='MC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalMaster,
       (Select count(*) as totalSuccessMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessMaster,
	   (Select count(*) as totalFailedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MC') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedMaster,
	   (Select count(*) as totalDroppedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedMaster,
	   (Select count(*) as totalBauncedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedMaster,
	   (Select count(*) as totalCancelledMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MC')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledMaster,
	   (Select count(t1.STATUS) as totalAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='AX')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalAmex,
       (Select count(*) as totalSuccessAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessAmex,
	   (Select count(*) as totalFailedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='AX') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedAmex,
	   (Select count(*) as totalDroppedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedAmex,
	   (Select count(*) as totalBauncedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedAmex,
	   (Select count(*) as totalCancelledAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='AX')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledAmex,
	   (Select count(t1.STATUS) as totalMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and  (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='MS')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalMestro,
       (Select count(*) as totalSuccessMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessMestro,
	   (Select count(*) as totalFailedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MS') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedMestro,
	   (Select count(*) as totalDroppedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedMestro,
	   (Select count(*) as totalBauncedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedMestro,
	   (Select count(*) as totalCancelledMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MS')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledMestro,
	   (Select count(t1.STATUS) as totalDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and  (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='DN')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDiner,
       (Select count(*) as totalSuccessDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='DN') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessDiner,
	   (Select count(*) as totalFailedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='DN') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedDiner,
	   (Select count(*) as totalDroppedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='DN') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedDiner,
	   (Select count(*) as totalBauncedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='DN') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedDiner,
	   (Select count(*) as totalCancelledDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='DN')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledDiner);
	  
	  
	  
	  
	ELSE
	select  totalVisa,totalSuccessVisa,totalFailedVisa,totalDroppedVisa,totalBauncedVisa,totalCancelledVisa,
       totalMaster,totalSuccessMaster,totalFailedMaster,totalDroppedMaster,totalBauncedMaster,totalCancelledMaster,
       totalAmex,totalSuccessAmex,totalFailedAmex,totalDroppedAmex,totalBauncedAmex,totalCancelledAmex, 
       totalMestro,totalSuccessMestro,totalFailedMestro,totalDroppedMestro,totalBauncedMestro,totalCancelledMestro,
       totalDiner,totalSuccessDiner,totalFailedDiner,totalDroppedDiner,totalBauncedDiner,totalCancelledDiner from ((Select count(t1.STATUS) as totalVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency)and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalVisa,
       (Select count(*) as totalSuccessVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessVisa,
	   (Select count(*) as totalFailedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedVisa,
	   (Select count(*) as totalDroppedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedVisa,
	   (Select count(*) as totalBauncedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedVisa,
	   (Select count(*) as totalCancelledVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='VI')  and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledVisa,
	   (Select count(t1.STATUS) as totalMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MC') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalMaster,
       (Select count(*) as totalSuccessMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessMaster,
	   (Select count(*) as totalFailedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed'))  and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MC') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedMaster,
	   (Select count(*) as totalDroppedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedMaster,
	   (Select count(*) as totalBauncedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.PAY_ID = payId)  and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedMaster,
	   (Select count(*) as totalCancelledMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MC') and (t1.PAY_ID = payId)  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledMaster,
       (Select count(t1.STATUS) as totalAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='AX') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalAmex,
       (Select count(*) as totalSuccessAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.PAY_ID = payId) and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessAmex,
	   (Select count(*) as totalFailedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.PAY_ID = payId) and (t1.MOP_TYPE='AX') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedAmex,
	   (Select count(*) as totalDroppedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedAmex,
	   (Select count(*) as totalBauncedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.PAY_ID = payId)  and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedAmex,
	   (Select count(*) as totalCancelledAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='AX')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledAmex,
        (Select count(t1.STATUS) as totalMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and  (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MS') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalMestro,
       (Select count(*) as totalSuccessMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessMestro,
	   (Select count(*) as totalFailedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MS') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedMestro,
	   (Select count(*) as totalDroppedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedMestro,
	   (Select count(*) as totalBauncedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedMestro,
	   (Select count(*) as totalCancelledMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='MS')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledMestro,
        (Select count(t1.STATUS) as totalDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and  (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDiner,
       (Select count(*) as totalSuccessDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='DN') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessDiner,
	   (Select count(*) as totalFailedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedDiner,
	   (Select count(*) as totalDroppedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedDiner,
	   (Select count(*) as totalBauncedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedDiner,
	   (Select count(*) as totalCancelledDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC') and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledDiner);
	  
  END IF;    
   END$$
 DELIMITER ;
 
 
 
DELIMITER $$
CREATE  PROCEDURE `creditCardsTransactionSummary`(IN payId varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
IF (payId = 'ALL MERCHANTS') THEN
       select  totalCreditTransaction,totalCreditSuccess,totalCreditFailed,totalCreditDropped,totalCreditBaunced,totalCreditCancelled
       from ((Select count(t1.STATUS) as totalCreditTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT')  and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditTransaction,
       (Select count(*) as totalCreditSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT')  and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditSuccess,
	   (Select count(*) as totalCreditFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.PAYMENT_TYPE='CC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditFailed,
	   (Select count(*) as totalCreditDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditDropped,
	   (Select count(*) as totalCreditBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (t1.PAYMENT_TYPE='CC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditBaunced,
	   (Select count(*) as totalCreditCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE IN ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC')   and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditCancelled);
	
	ELSE
     select  totalCreditTransaction,totalCreditSuccess,totalCreditFailed,totalCreditDropped,totalCreditBaunced,totalCreditCancelled
        from ((Select count(t1.STATUS) as totalCreditTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT')  and (t1.PAYMENT_TYPE='CC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditTransaction,
       (Select count(*) as totalCreditSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT')  and (t1.PAYMENT_TYPE='CC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditSuccess,
	   (Select count(*) as totalCreditFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditFailed,
	   (Select count(*) as totalCreditDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER'  and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')   and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditDropped,
	   (Select count(*) as totalCreditBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')  and (t1.PAYMENT_TYPE='CC')  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditBaunced,
	   (Select count(*) as totalCreditCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE IN ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='CC')   and (t1.CURRENCY_CODE= currency)  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditCancelled);
	END IF;      
 END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `debitCardsMopTypeSummary`(IN payId varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
       select  totalVisa,totalSuccessVisa,totalFailedVisa,totalDroppedVisa,totalBauncedVisa,totalCancelledVisa,
       totalMaster,totalSuccessMaster,totalFailedMaster,totalDroppedMaster,totalBauncedMaster,totalCancelledMaster,
       totalAmex,totalSuccessAmex,totalFailedAmex,totalDroppedAmex,totalBauncedAmex,totalCancelledAmex, 
       totalMestro,totalSuccessMestro,totalFailedMestro,totalDroppedMestro,totalBauncedMestro,totalCancelledMestro,
	  totalDiner,totalSuccessDiner,totalFailedDiner,totalDroppedDiner,totalBauncedDiner,totalCancelledDiner from ((Select count(t1.STATUS) as totalVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='VI')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalVisa,
       (Select count(*) as totalSuccessVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId  where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured') or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='VI') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessVisa,
	   (Select count(*) as totalFailedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='VI') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedVisa,
	   (Select count(*) as totalDroppedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='VI') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedVisa,
	   (Select count(*) as totalBauncedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='VI') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedVisa,
	   (Select count(*) as totalCancelledVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='VI')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledVisa,
       (Select count(t1.STATUS) as totalMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and  (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='MC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalMaster,
       (Select count(*) as totalSuccessMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessMaster,
	   (Select count(*) as totalFailedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MC') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedMaster,
	   (Select count(*) as totalDroppedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedMaster,
	   (Select count(*) as totalBauncedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedMaster,
	   (Select count(*) as totalCancelledMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MC')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledMaster,
	   (Select count(t1.STATUS) as totalAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='AX')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalAmex,
       (Select count(*) as totalSuccessAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessAmex,
	   (Select count(*) as totalFailedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='AX') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedAmex,
	   (Select count(*) as totalDroppedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedAmex,
	   (Select count(*) as totalBauncedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedAmex,
	   (Select count(*) as totalCancelledAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='AX')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledAmex,
	   (Select count(t1.STATUS) as totalMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and  (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='MS')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalMestro,
       (Select count(*) as totalSuccessMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessMestro,
	   (Select count(*) as totalFailedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MS') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedMestro,
	   (Select count(*) as totalDroppedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedMestro,
	   (Select count(*) as totalBauncedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedMestro,
	   (Select count(*) as totalCancelledMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MS')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledMestro,
	   (Select count(t1.STATUS) as totalDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and  (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='DN')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDiner,
       (Select count(*) as totalSuccessDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='DN') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessDiner,
	   (Select count(*) as totalFailedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='DN') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedDiner,
	   (Select count(*) as totalDroppedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='DN') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedDiner,
	   (Select count(*) as totalBauncedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='DN') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedDiner,
	   (Select count(*) as totalCancelledDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='DN')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledDiner);

	ELSE
	select  totalVisa,totalSuccessVisa,totalFailedVisa,totalDroppedVisa,totalBauncedVisa,totalCancelledVisa,
       totalMaster,totalSuccessMaster,totalFailedMaster,totalDroppedMaster,totalBauncedMaster,totalCancelledMaster,
       totalAmex,totalSuccessAmex,totalFailedAmex,totalDroppedAmex,totalBauncedAmex,totalCancelledAmex, 
       totalMestro,totalSuccessMestro,totalFailedMestro,totalDroppedMestro,totalBauncedMestro,totalCancelledMestro,
       totalDiner,totalSuccessDiner,totalFailedDiner,totalDroppedDiner,totalBauncedDiner,totalCancelledDiner from ((Select count(t1.STATUS) as totalVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency)and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalVisa,
       (Select count(*) as totalSuccessVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessVisa,
	   (Select count(*) as totalFailedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedVisa,
	   (Select count(*) as totalDroppedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedVisa,
	   (Select count(*) as totalBauncedVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='VI') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedVisa,
	   (Select count(*) as totalCancelledVisa from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='VI')  and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledVisa,
	   (Select count(t1.STATUS) as totalMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MC') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalMaster,
       (Select count(*) as totalSuccessMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessMaster,
	   (Select count(*) as totalFailedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed'))  and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MC') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedMaster,
	   (Select count(*) as totalDroppedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedMaster,
	   (Select count(*) as totalBauncedMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.PAY_ID = payId)  and (t1.MOP_TYPE='MC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedMaster,
	   (Select count(*) as totalCancelledMaster from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MC') and (t1.PAY_ID = payId)  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledMaster,
        (Select count(t1.STATUS) as totalAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='AX') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalAmex,
       (Select count(*) as totalSuccessAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.PAY_ID = payId) and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessAmex,
	   (Select count(*) as totalFailedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.PAY_ID = payId) and (t1.MOP_TYPE='AX') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedAmex,
	   (Select count(*) as totalDroppedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedAmex,
	   (Select count(*) as totalBauncedAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.PAY_ID = payId)  and (t1.MOP_TYPE='AX') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedAmex,
	   (Select count(*) as totalCancelledAmex from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='AX')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledAmex,
	   (Select count(t1.STATUS) as totalMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and  (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MS') and (t1.PAY_ID = payId)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalMestro,
	   (Select count(*) as totalSuccessMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessMestro,
	   (Select count(*) as totalFailedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MS') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedMestro,
	   (Select count(*) as totalDroppedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedMestro,
	   (Select count(*) as totalBauncedMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='MS') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedMestro,
	   (Select count(*) as totalCancelledMestro from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='MS')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledMestro,
	   (Select count(t1.STATUS) as totalDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and  (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDiner,
       (Select count(*) as totalSuccessDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='DN') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccessDiner,
	   (Select count(*) as totalFailedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedDiner,
	   (Select count(*) as totalDroppedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Sent to Bank','Timeout')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDroppedDiner,
	   (Select count(*) as totalBauncedDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS='Invalid') and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBauncedDiner,
	   (Select count(*) as totalCancelledDiner from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC') and (t1.MOP_TYPE='DN') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelledDiner);
	  
  END IF;      
  END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `debitCardsTransactionSummary`(IN payId varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
       select  totalDebitTransaction,totalDebitSuccess,totalDebitFailed,totalDebitDropped,totalDebitBaunced,totalDebitCancelled
       from ((Select count(t1.STATUS) as totalDebitTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitTransaction,
       (Select count(*) as totalDebitSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitSuccess,
	   (Select count(*) as totalDebitFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')  and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitFailed,
	   (Select count(*) as totalDebitDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER'  and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitDropped,
	   (Select count(*) as totalDebitBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitBaunced,
	   (Select count(*) as totalDebitCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE IN ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')   and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitCancelled);
	  
	ELSE
        select  totalDebitTransaction,totalDebitSuccess,totalDebitFailed,totalDebitDropped,totalDebitBaunced,totalDebitCancelled
	   from ((Select count(t1.STATUS) as totalDebitTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitTransaction,
       (Select count(*) as totalDebitSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')   and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitSuccess,
	   (Select count(*) as totalDebitFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.PAYMENT_TYPE='DC')   and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitFailed,
	   (Select count(*) as totalDebitDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER'  and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')   and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitDropped,
	   (Select count(*) as totalDebitBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitBaunced,
	   (Select count(*) as totalDebitCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE IN ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='DC')   and (t1.CURRENCY_CODE= currency)  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitCancelled);
	END IF;      
 END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `failedSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN userType varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250) ,IN start int,IN length int)
BEGIN
  if((userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL')) then
		   SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
                `t1`.`INTERNAL_CARD_ISSUER_BANK` AS `INTERNAL_CARD_ISSUER_BANK`,
                `t1`.`INTERNAL_CARD_ISSUER_COUNTRY` AS `INTERNAL_CARD_ISSUER_COUNTRY`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                IFNULL(`t1`.`PG_TXN_MESSAGE`, '') AS `PG_TXN_MESSAGE`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
           and (su.userType <> 'POSMERCHANT')
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;   
   ELSE
			SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
                `t1`.`INTERNAL_CARD_ISSUER_BANK` AS `INTERNAL_CARD_ISSUER_BANK`,
                `t1`.`INTERNAL_CARD_ISSUER_COUNTRY` AS `INTERNAL_CARD_ISSUER_COUNTRY`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                IFNULL(`t1`.`PG_TXN_MESSAGE`, '') AS `PG_TXN_MESSAGE`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
		   and t1.PAY_ID = merchantPayId 
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;  
	END IF;   
    END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `fraudSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN userType varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
  if((userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL')) then
		   SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
                `t1`.`INTERNAL_CARD_ISSUER_BANK` AS `INTERNAL_CARD_ISSUER_BANK`,
                `t1`.`INTERNAL_CARD_ISSUER_COUNTRY` AS `INTERNAL_CARD_ISSUER_COUNTRY`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                IFNULL(`t1`.`PG_TXN_MESSAGE`, '') AS `PG_TXN_MESSAGE`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.RESPONSE_CODE='012' )
           and (su.userType <> 'POSMERCHANT')
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);   
   ELSE
			SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
                `t1`.`INTERNAL_CARD_ISSUER_BANK` AS `INTERNAL_CARD_ISSUER_BANK`,
                `t1`.`INTERNAL_CARD_ISSUER_COUNTRY` AS `INTERNAL_CARD_ISSUER_COUNTRY`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                IFNULL(`t1`.`PG_TXN_MESSAGE`, '') AS `PG_TXN_MESSAGE`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
		   and t1.PAY_ID = merchantPayId 
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END);  
	END IF;   
   END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `incompleteSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN userType varchar(250),IN paymenttype varchar(250),IN status varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN start int,IN length int)
BEGIN
   if((userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL')) then
		   SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                `bd`.`CUST_PHONE` AS `CUST_PHONE`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))  
                LEFT JOIN `BILLING_DETAILS` `bd` ON (`t1`.`TXN_ID` = `bd`.`TXN_ID`)
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank'))
           or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
           and (su.userType <> 'POSMERCHANT')
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;
   ELSE
			SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`userType` AS `userType`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`,
                `bd`.`CUST_PHONE` AS `CUST_PHONE`
              
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
				LEFT JOIN `BILLING_DETAILS` `bd` ON (`t1`.`TXN_ID` = `bd`.`TXN_ID`)
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank'))
		   or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
           and t1.PAY_ID = merchantPayId 
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;  
	END IF;
  END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `invalidSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN userType varchar(250))
BEGIN
   SELECT t1.CREATE_DATE,t1.TXN_ID ,t1.AMOUNT,IFNULL(t1.CUST_EMAIL,'') as 'CUST_EMAIL' ,IFNULL(t1.PAYMENT_TYPE,'') as 'PAYMENT_TYPE' ,IFNULL(t1.MOP_TYPE,'') as 'MOP_TYPE',IFNULL(t1.TXNTYPE,'') as 'TXNTYPE',
   t1.PAY_ID,su.businessName,t1.Status as 'STATUS' ,IFNULL(t1.ORDER_ID,'0') as 'ORDER_ID',t1.RESPONSE_MESSAGE,IFNULL(t1.CUST_NAME,'') as 'CUST_NAME',IFNULL(t1.CARD_MASK,'') as 'CARD_MASK',IFNULL(t1.PRODUCT_DESC,'') as 'PRODUCT_DESC',
   IFNULL(t1.INTERNAL_REQUEST_FIELDS,'') as 'INTERNAL_REQUEST_FIELDS'
   FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
   where (t1.CREATE_DATE between fromdate and todate) and
   (((t1.TXNTYPE in ('INVALID','ENROLL','NEWORDER','SALE','AUTHORISE') and t1.status in ('Invalid'))))
   and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when t1.PAY_ID = merchantPayId then 1 else 0 END) END);
 END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `invoiceList`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN userType varchar(250),IN invoiceNo varchar(250),IN customerEmail varchar(250),IN currency varchar(250),IN invoiceType varchar(250))
BEGIN
   Select inv.invoiceId, inv.createDate,su.businessName, inv.name,inv.email,inv.invoiceNo, inv.currencyCode, inv.totalAmount, inv.invoiceType
	from Invoice inv left join User su on inv.payId = su.payId
	where (inv.createDate between fromdate and todate) 
	and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (merchantPayId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when inv.payId = merchantPayId then 1 else 0 END) END)  
    and (CASE when invoiceNo='' then 1 else (CASE when inv.invoiceNo = invoiceNo then 1 else 0 END) END)
    and (CASE when customerEmail='' then 1 else (CASE when inv.email = customerEmail then 1 else 0 END) END)
	and (CASE when currency='ALL' then 1 else (CASE when inv.currencyCode = currency then 1 else 0 END) END)
    and (CASE when invoiceType='ALL' then 1 else (CASE when inv.invoiceType = invoiceType then 1 else 0 END) END);
   
 END$$
 DELIMITER ; 
 
 
DELIMITER $$
CREATE  PROCEDURE `merchtransactionstats`(IN fromdate datetime, IN todate datetime,IN payid varchar(250), IN account varchar(250),IN paymenttype varchar(250))
BEGIN
IF (payid = 'ALL') THEN
	SELECT date(t1.UPDATE_DATE) as txndate, t1.PAYMENT_TYPE as paymentMethod,t1.CURRENCY_CODE, 
	SUM(CASE WHEN (t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = account) and (t1.PAYMENT_TYPE=paymenttype) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) )THEN 1 ELSE 0 END) totalSuccess , 
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalPending,
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')))THEN 1 ELSE 0 END) totalDropped,
	SUM(CASE WHEN ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid'))) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN 1 ELSE 0 END) totalBaunced, 
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN t1.AMOUNT ELSE 0 END) approvedAmount
	from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.UPDATE_DATE between fromdate and todate and su.userType <> 'POSMERCHANT' and t1.PAYMENT_TYPE is not NULL)
	group by DAYOFMONTH(t1.UPDATE_DATE),t1.PAYMENT_TYPE
	order by t1.UPDATE_DATE desc;
ELSE
	SELECT date(t1.UPDATE_DATE) as txndate, t1.PAYMENT_TYPE as paymentMethod,t1.CURRENCY_CODE,  
	SUM(CASE WHEN (t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = account) and (t1.PAYMENT_TYPE=paymenttype) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN 1 ELSE 0 END) totalSuccess , 
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalPending,
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')))THEN 1 ELSE 0 END) totalDropped,
	SUM(CASE WHEN ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid'))) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN 1 ELSE 0 END) totalBaunced, 
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN t1.AMOUNT ELSE 0 END) approvedAmount
	from TRANSACTION t1 where (t1.UPDATE_DATE between fromdate and todate and t1.PAY_ID = payid and t1.PAYMENT_TYPE is not NULL)
	group by DAYOFMONTH(t1.UPDATE_DATE),PAYMENT_TYPE
	order by UPDATE_DATE desc;
END IF;
 END$$
 DELIMITER ; 
 
DELIMITER $$
CREATE  PROCEDURE `merchtransactionstatsAccount`(IN fromdate datetime, IN todate datetime, IN payid varchar(250), IN acquirer varchar(250))
BEGIN
IF (payid = 'ALL') THEN
	SELECT date(t1.UPDATE_DATE) as txndate, t1.PAYMENT_TYPE as paymentMethod,t1.CURRENCY_CODE, 
	SUM(CASE WHEN (t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = acquirer) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = acquirer))THEN 1 ELSE 0 END) totalSuccess, 
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.ACQUIRER_TYPE = acquirer))THEN 1 ELSE 0 END) totalPending,
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (t1.ACQUIRER_TYPE = acquirer))THEN 1 ELSE 0 END) totalDropped,
	SUM(CASE WHEN ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid'))) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = acquirer))THEN 1 ELSE 0 END) totalBaunced, 
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = acquirer))THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.UPDATE_DATE between fromdate and todate) and su.userType <> 'POSMERCHANT' and t1.PAYMENT_TYPE is not NULL)
	group by DAYOFMONTH(t1.UPDATE_DATE),t1.PAYMENT_TYPE
	order by t1.UPDATE_DATE desc;
ELSE
	SELECT date(t1.UPDATE_DATE) as txndate, t1.PAYMENT_TYPE as paymentMethod,t1.CURRENCY_CODE, 
	SUM(CASE WHEN (t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = acquirer) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = acquirer))THEN 1 ELSE 0 END) totalSuccess, 
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.ACQUIRER_TYPE = acquirer))THEN 1 ELSE 0 END) totalPending, 
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (t1.ACQUIRER_TYPE = acquirer))THEN 1 ELSE 0 END) totalDropped,
	SUM(CASE WHEN ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid'))) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = acquirer))THEN 1 ELSE 0 END) totalBaunced, 
    SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.ACQUIRER_TYPE = acquirer))THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1  where ((t1.UPDATE_DATE between fromdate and todate) and t1.PAY_ID = payid and t1.PAYMENT_TYPE is not NULL)
	group by DAYOFMONTH(t1.UPDATE_DATE),t1.PAYMENT_TYPE
	order by t1.UPDATE_DATE desc;
END IF;
 
END$$
 DELIMITER ; 
 
 
 
DELIMITER $$
CREATE  PROCEDURE `merchtransactionstatsAll`(IN fromdate datetime, IN todate datetime ,IN payid varchar(250))
BEGIN
IF (payid = 'ALL') THEN
	SELECT date(t1.UPDATE_DATE) as txndate, t1.PAYMENT_TYPE as paymentMethod,t1.CURRENCY_CODE,
	SUM(CASE WHEN (t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and UPDATE_DATE <= todate) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) )THEN 1 ELSE 0 END) totalSuccess, 
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalPending,
    SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')))THEN 1 ELSE 0 END) totalDropped,
	SUM(CASE WHEN ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid'))) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN 1 ELSE 0 END) totalBaunced, 
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.UPDATE_DATE between fromdate and todate) and su.userType <> 'POSMERCHANT' and t1.PAYMENT_TYPE is not NULL)
	group by DAYOFMONTH(t1.UPDATE_DATE) ,t1.PAYMENT_TYPE,t1.CURRENCY_CODE
	order by t1.UPDATE_DATE desc;
else
	SELECT date(t1.UPDATE_DATE) as txndate, t1.PAYMENT_TYPE as paymentMethod,t1.CURRENCY_CODE,
	SUM(CASE WHEN (t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN 1 ELSE 0 END) totalSuccess, 
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalPending,
    SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')))THEN 1 ELSE 0 END) totalDropped,
	SUM(CASE WHEN ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid'))) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN 1 ELSE 0 END) totalBaunced, 
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate))THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1 where ((t1.UPDATE_DATE between fromdate and todate)  and  t1.PAY_ID = payid and t1.PAYMENT_TYPE is not NULL)
	group by DAYOFMONTH(t1.UPDATE_DATE),t1.PAYMENT_TYPE,t1.CURRENCY_CODE
	order by t1.UPDATE_DATE desc;
END IF;
 
 
END$$
 DELIMITER ; 
 
 
DELIMITER $$
CREATE  PROCEDURE `merchtransactionstatsPayment`(IN paymenttype varchar(250),IN fromdate datetime, IN todate datetime,IN payid varchar(250))
BEGIN
IF (payid = 'ALL') THEN
	SELECT date(t1.UPDATE_DATE) as txndate, t1.PAYMENT_TYPE as paymentMethod,t1.CURRENCY_CODE,
	SUM(CASE WHEN (t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.PAYMENT_TYPE = paymenttype) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.PAYMENT_TYPE = paymenttype))THEN 1 ELSE 0 END) totalSuccess , 
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed'))  and (t1.PAYMENT_TYPE = paymenttype))THEN 1 ELSE 0 END) totalPending,
    SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (t1.PAYMENT_TYPE = paymenttype))THEN 1 ELSE 0 END) totalDropped,
	SUM(CASE WHEN ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid'))) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.PAYMENT_TYPE = paymenttype))THEN 1 ELSE 0 END) totalBaunced, 
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.PAYMENT_TYPE = paymenttype))THEN t1.AMOUNT ELSE 0 END) approvedAmount
	FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.UPDATE_DATE between fromdate and todate and su.userType <> 'POSMERCHANT' and t1.PAYMENT_TYPE is not NULL)
	group by DAYOFMONTH(t1.UPDATE_DATE),t1.PAYMENT_TYPE
	order by t1.UPDATE_DATE desc;
ELSE
	SELECT date(t1.UPDATE_DATE) as txndate, t1.PAYMENT_TYPE as paymentMethod,t1.CURRENCY_CODE,
	SUM(CASE WHEN (t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.PAYMENT_TYPE = paymenttype) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.PAYMENT_TYPE = paymenttype))THEN 1 ELSE 0 END) totalSuccess , 
	SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed'))  and (t1.PAYMENT_TYPE = paymenttype))THEN 1 ELSE 0 END) totalPending,
    SUM(CASE WHEN ((t1.TXNTYPE='NEWORDER') and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (t1.PAYMENT_TYPE = paymenttype))THEN 1 ELSE 0 END) totalDropped,
	SUM(CASE WHEN ((t1.TXNTYPE ='NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid'))) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.PAYMENT_TYPE = paymenttype))THEN 1 ELSE 0 END) totalBaunced, 
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.UPDATE_DATE >= fromdate and t1.UPDATE_DATE <= todate) and (t1.PAYMENT_TYPE = paymenttype))THEN t1.AMOUNT ELSE 0 END) approvedAmount
	FROM TRANSACTION t1 where (t1.UPDATE_DATE between fromdate and todate and PAY_ID = payid and t1.PAYMENT_TYPE is not NULL)
	group by DAYOFMONTH(t1.UPDATE_DATE),t1.PAYMENT_TYPE
	order by t1.UPDATE_DATE desc;
END IF;

END$$
 DELIMITER ; 
 
 
 
DELIMITER $$
CREATE  PROCEDURE `misReports`(IN dateFrom varchar(250),IN dateTo varchar(250),IN payId varchar(250), IN acquirer varchar(250),IN userType varchar(250),IN start int,IN length int)
BEGIN
	SELECT
        `t1`.`PAY_ID` AS `PAY_ID`,
        `t1`.`CREATE_DATE` AS `CREATE_DATE`,
        `t1`.`TXN_ID` AS `TXN_ID`,
        `t1`.`UPDATE_DATE` AS `UPDATE_DATE`,
        `t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
        `t1`.`PAY_ID` AS `PAY_ID`,
        `t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
        `t1`.`PAYMENT_TYPE` AS `PAYMENT_TYPE`,
        `t1`.`MOP_TYPE` AS `MOP_TYPE`,
        `t1`.`TXNTYPE` AS `TXNTYPE`,
        `t1`.`CURRENCY_CODE` AS `CURRENCY_CODE`,
        `su`.`businessName` AS `businessName`,
		`su`.`ifscCode` AS `ifscCode`,
		`su`.`bankName` AS `bankName`,
		`su`.`accountNo` AS `accountNo`,
        `t1`.`AMOUNT` AS `AMOUNT`,
        `t1`.`status` as STATUS
       
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
    where (CREATE_DATE between dateFrom and dateTo)  and   ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'REFUND' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'CAPTURE' AND `t1`.`STATUS` = 'Captured'))
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL'  then ACQUIRER_TYPE not in('CITRUS') else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END) LIMIT start,length;
END$$
 DELIMITER ; 
 
DELIMITER $$
CREATE  PROCEDURE `netBankingSummary`(IN payId varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
       select  totalTransaction,totalSuccess,totalFailed,totalDropped,totalBaunced,totalCancelled from ((Select count(t1.STATUS) as totalTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalTransaction,
       (Select count(*) as totalSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccess,
	   (Select count(*) as totalFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailed,
	   (Select count(*) as totalDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDropped,
	   (Select count(*) as totalBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBaunced,
	   (Select count(*) as totalCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelled);
		
	ELSE
        select  totalTransaction,totalSuccess,totalFailed,totalDropped,totalBaunced,totalCancelled from ((Select count(t1.STATUS) as totalTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalTransaction,
       (Select count(*) as totalSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalSuccess,
	   (Select count(*) as totalFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailed,
	   (Select count(*) as totalDropped from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='NEWORDER' and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDropped,
	   (Select count(*) as totalBaunced from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid')))) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalBaunced,
	   (Select count(*) as totalCancelled from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT') and (t1.PAYMENT_TYPE='NB') and (t1.CURRENCY_CODE= currency)  and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelled);
	END IF;           
END$$
 DELIMITER ; 
 
 
DELIMITER $$
CREATE  PROCEDURE `paymentMethodsSummary`(IN payId varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
	select  totalNetBankingTransaction,totalCreditCardsTransaction,totalDebitCardsTransaction,totalWalletTransaction
	from ((Select count(t1.STATUS) as totalNetBankingTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='NB')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankingTransaction,
   (Select count(t1.STATUS) as totalCreditCardsTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and  (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='CC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditCardsTransaction,
   (Select count(t1.STATUS) as totalDebitCardsTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='DC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitCardsTransaction,
   (Select count(t1.STATUS) as totalWalletTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAYMENT_TYPE='WL')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletTransaction);

	ELSE
   select  totalNetBankingTransaction,totalCreditCardsTransaction,totalDebitCardsTransaction,totalWalletTransaction
   from ((Select count(t1.STATUS) as totalNetBankingTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId)   and (t1.PAYMENT_TYPE='NB')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalNetBankingTransaction,
   (Select count(t1.STATUS) as totalCreditCardsTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId)  and (t1.PAYMENT_TYPE='CC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCreditCardsTransaction,
   (Select count(t1.STATUS) as totalDebitCardsTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId)   and (t1.PAYMENT_TYPE='DC')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebitCardsTransaction,
   (Select count(t1.STATUS) as totalWalletTransaction from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where  t1.TXNTYPE='NEWORDER' and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.PAY_ID = payId)   and (t1.PAYMENT_TYPE='WL')   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalWalletTransaction);
		
	END IF;  
  END$$
 DELIMITER ; 
 
DELIMITER $$
CREATE  PROCEDURE `refundReport`(IN fromdate datetime, IN todate datetime,IN payid varchar(250), IN userType varchar(250),
IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN start int,IN length int)
BEGIN
	Select t1.ORDER_ID, t1.TXN_ID as 'TXN_ID', t1.PAY_ID, su.businessName As 'BUSINESS_NAME', CURRENCY_CODE,t1.INTERNAL_USER_EMAIL,t1.INTERNAL_CUST_IP,t1.CUST_EMAIL, se.status as STATUS, (Select min(CREATE_DATE) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'TXN_DATE',
	t1.CREATE_DATE as 'REFUND_DATE', t1.AMOUNT as 'REFUND_AMOUNT', (Select min(Amount) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'TXN_AMOUNT', (Select min(Amount) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) - (Select IFNULL(sum(AMOUNT),0.00) from TRANSACTION where OID = t1.OID and TxnType = 'REFUND' and status ='Captured') As 'REFUNDABLE_AMOUNT',
	(Select Payment_Type from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'PAYMENT_TYPE', (Select MOP_TYPE from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'MOP_TYPE'
	 from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId 
     left join Settlement se on t1.TXN_ID = se.txnId where t1.ORIG_TXN_ID in (Select distinct t1.TXN_ID  from TRANSACTION t1 where (txntype = 'SALE' AND t1.status = 'Captured')
	OR (t1.txntype = 'AUTHORISE' AND t1.status = 'Approved')) and (t1.TXNTYPE = 'REFUND' and t1.STATUS = 'Captured')
	and (t1.CREATE_DATE between fromdate and todate) 
	and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payid = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when t1.PAY_ID = payid then 1 else 0 END) END)
	and (CASE when paymenttype='ALL' then 1 else (CASE when (select count(Payment_Type) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID and PAYMENT_TYPE = paymenttype) >0  then 1 else 0 END) END)
	and  (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
	and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC LIMIT start,length;
 END$$
 DELIMITER ; 
 
 
 
DELIMITER $$
CREATE  PROCEDURE `remittanceTransactionAmount`(IN fromdate datetime, IN todate datetime,IN payId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN userType varchar(250))
BEGIN
	SELECT 
        `t1`.`ORIG_TXN_ID` AS `OID`,
        `t1`.`CREATE_DATE` AS `CREATE_DATE`,
        `t1`.`TXN_ID` AS `TXN_ID`,
        `t1`.`ORDER_ID` AS `ORDER_ID`,
        `t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
        `t1`.`PAY_ID` AS `PAY_ID`,
        `t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
        `t1`.`PAYMENT_TYPE` AS `PAYMENT_TYPE`,
        `t1`.`MOP_TYPE` AS `MOP_TYPE`,
        `t1`.`TXNTYPE` AS `TXNTYPE`,
        `t1`.`CURRENCY_CODE` AS `CURRENCY_CODE`,
        `t1`.`AUTH_CODE` AS `AUTH_CODE`,
        `su`.`businessName` AS `businessName`,
        `t1`.`AMOUNT` AS `CAPTURED_AMOUNT`,
        `se`.`status` as STATUS,
        '0.00' AS `REFUND_AMOUNT`,
        '0.00' AS `CHARGEBACK_AMOUNT`
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
        left join Settlement se on TXN_ID = se.txnId
    where (CREATE_DATE between fromdate and todate) and ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'AUTHORISE' AND `t1`.`STATUS` = 'Approved'))
    and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END);
 END$$
 DELIMITER ;
 
 
 
DELIMITER $$
CREATE  PROCEDURE `reseller_searchPayment`(IN fromdate datetime, IN todate datetime,IN transactionId varchar(250),
IN orderId varchar(250),IN customerEmail varchar(250),IN payId varchar(250),
IN paymenttype varchar(250),IN  Userstatus varchar(250),IN currency varchar(250),IN resellerId varchar(250),IN userType varchar(250),IN start int,IN length int)
BEGIN
	SELECT 
		`transaction`.`TXN_ID` AS `TXN_ID`,
		`transaction`.`PAY_ID` AS `PAY_ID`,
		`transaction`.`INTERNAL_CARD_ISSUER_BANK` AS `INTERNAL_CARD_ISSUER_BANK`,
		`transaction`.`INTERNAL_CARD_ISSUER_COUNTRY` AS `INTERNAL_CARD_ISSUER_COUNTRY`,
		`transaction`.`CUST_NAME` AS `CUST_NAME`,
        `transaction`.`CUST_EMAIL` AS `CUST_EMAIL`,
		`subuser`.`businessName` AS `businessName`,
		`transaction`.`TXNTYPE` AS `TXNTYPE`,
		`transaction`.`PAYMENT_TYPE` AS `PAYMENT_TYPE`,
        `transaction`.`MOP_TYPE` AS `MOP_TYPE`,
		`transaction`.`CARD_MASK` AS `CARD_MASK`,
		`transaction`.`status` as STATUS,
        `transaction`.`CREATE_DATE` AS `CREATE_DATE`,
		`transaction`.`AMOUNT` AS `CAPTURED_AMOUNT`,
        `transaction`.`ORDER_ID` AS `ORDER_ID`,
		`transaction`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
	    `transaction`.`CURRENCY_CODE` AS `CURRENCY_CODE`
    FROM `TRANSACTION` `transaction` left join User subuser on PAY_ID = subuser.payId
	where (CREATE_DATE >= fromdate and CREATE_DATE  <= todate) 
    and ((`transaction`.`TXNTYPE` in ('SALE','REFUND') and `transaction`.`STATUS` = 'Captured') 
    or 	(`transaction`.`TXNTYPE` = 'NEWORDER' and `transaction`.`STATUS` IN ('Rejected','Cancelled'))
    or 	(`transaction`.`TXNTYPE` = 'AUTHORISE' and `transaction`.`STATUS` = 'Approved') 
	or (`transaction`.`TXNTYPE` = 'ENROLL' and `transaction`.`STATUS` IN ( 'Timeout','Enrolled') )
    or  (`transaction`.`TXNTYPE` in ('SALE','AUTHORISE') and `transaction`.`status` IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed','Invalid','Pending','Timeout','Browser Closed','Sent to Bank'))
    or 	((`transaction`.`TXNTYPE` = 'ENROLL' and transaction.status in ('Invalid')))) 
	and (CASE when transactionId='' then 1 else (CASE when TXN_ID = transactionId then 1 else 0 END) END)
    and (CASE when orderId='' then 1 else (CASE when ORDER_ID = orderId then 1 else 0 END) END)
	and (CASE when customerEmail='' then 1 else (CASE when CUST_EMAIL = customerEmail then 1 else 0 END) END)
	and (CASE when paymenttype='' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when (subuser.resellerId=resellerId) and (payId = 'ALL') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when Userstatus='' then 1 else (CASE when status = Userstatus then 1 else 0 END) END)
    and (CASE when currency='' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and (CASE when ((userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN' or userType='RESELLER') and (payId = 'ALL') and (subuser.userType <> 'POSMERCHANT'))then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;
 END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `resellerAuthoriseSummary`(IN fromdate varchar(250), IN todate varchar(250),IN merchantPayId varchar(250),IN resellerId varchar(250), 
                IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
		IF (fromdate = '') THEN
			Select * from (select t1.CREATE_DATE,t1.TXN_ID,t1.AMOUNT,t1.CUST_EMAIL, 
			t1.PAYMENT_TYPE,t1.MOP_TYPE,t1.TXNTYPE,t1.PAY_ID,su.businessName,t1.Status as 'STATUS',t1.ORDER_ID,t1.CURRENCY_CODE,t1.INTERNAL_CARD_ISSUER_BANK,t1.INTERNAL_CARD_ISSUER_COUNTRY,
            (select TXN_ID from TRANSACTION where ORIG_TXN_ID = t1.TXN_ID and TXNType='capture' and status='captured' ) as CAPTURE_TXN_ID,
			t1.RESPONSE_MESSAGE,t1.CUST_NAME,t1.CARD_MASK,t1.PRODUCT_DESC
			FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
			where 
			(t1.TXNTYPE = 'AUTHORISE') and (status ='Approved')
			and (CASE when (su.resellerId=resellerId) and (merchantPayId = 'ALL') then 1 else (CASE when t1.PAY_ID = merchantPayId then 1 else 0 END) END)
			and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
			and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
			and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END)
		) temp
	 where CAPTURE_TXN_ID is null order by CREATE_DATE DESC;
ELSE
	Select * from (select t1.CREATE_DATE,t1.TXN_ID,t1.AMOUNT,t1.CUST_EMAIL, 
			t1.PAYMENT_TYPE,t1.MOP_TYPE,t1.TXNTYPE,t1.PAY_ID,su.businessName,t1.Status as 'STATUS',t1.ORDER_ID,t1.CURRENCY_CODE,t1.INTERNAL_CARD_ISSUER_BANK,t1.INTERNAL_CARD_ISSUER_COUNTRY,
            (select TXN_ID from TRANSACTION where ORIG_TXN_ID = t1.TXN_ID and TXNType='capture' and status='captured' ) as CAPTURE_TXN_ID,
			t1.RESPONSE_MESSAGE,t1.CUST_NAME,t1.CARD_MASK,t1.PRODUCT_DESC
			FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
			where 
		    (CASE when (su.resellerId=resellerId) and  (merchantPayId = 'ALL')   then 1 else (CASE when t1.PAY_ID = merchantPayId then 1 else 0 END) END)
			and (CASE when resellerId = 'ALL' then 1 else (CASE when su.resellerId=resellerId then 1 else 0 END) END)
            and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
			and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
			and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END)
            and (t1.CREATE_DATE between fromdate and todate) 
			and TXNTYPE = 'AUTHORISE' and status ='Approved') temp
	   where CAPTURE_TXN_ID is null order by CREATE_DATE DESC;
END IF;
 END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `resellerBarChartTransactionRecord`(IN payId varchar(250),IN resellerId varchar(250),IN currency varchar(250),IN startDate datetime, IN endDate datetime)
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
      select  totalCredit,totalDebit,net,other from ((Select count(*) as totalCredit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and ((t1.MOP_TYPE='AX') or (t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='DN')) and (t1.PAYMENT_TYPE='CC')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCredit,
       (Select count(*) as totalDebit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and ((t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='MS')) and (t1.PAYMENT_TYPE='DC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalDebit,
       (Select count(*) as net from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and (t1.PAYMENT_TYPE='NB')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) net,
	   (Select count(*) as other from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and (t1.PAYMENT_TYPE='WL')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) other);

	ELSE
        select  totalCredit,totalDebit,net,other  from ((Select count(*) as totalCredit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (t1.PAY_ID = payId) and (su.resellerId=resellerId) and (t1.CURRENCY_CODE= currency) and ((t1.MOP_TYPE='AX') or (t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='DN'))  and (t1.PAYMENT_TYPE='CC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) totalCredit,
		         (Select count(*) as totalDebit from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured') or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved') ) and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and ((t1.MOP_TYPE='VI') or (t1.MOP_TYPE='MC') or (t1.MOP_TYPE='MS')) and (t1.PAYMENT_TYPE='DC') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) totalDebit,
                 (Select count(*) as net from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and (t1.PAYMENT_TYPE='NB')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) net,
				(Select count(*) as other from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and (t1.PAYMENT_TYPE='WL')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate) ) other);

	END IF;      
END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `resellerCancelledSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId varchar(250),  IN resellerId varchar(250),IN currency varchar(250),IN start int,IN length int)
BEGIN
	
   if(merchantPayId = 'ALL') then
		   SELECT 
				`t1`.`OID` AS `OID`,
                IFNULL(`t1`.`ORIG_TXN_ID`, '') AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`resellerId` AS `resellerId`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled'))
		    and su.resellerId=resellerId
		    and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length; 
   ELSE
			SELECT 
				`t1`.`OID` AS `OID`,
                IFNULL(`t1`.`ORIG_TXN_ID`, '') AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`resellerId` AS `resellerId`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled'))
		   and t1.PAY_ID = merchantPayId and su.resellerId=resellerId
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;
	END IF;        
END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `resellerCapturedSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId varchar(250),IN resellerId varchar(250),
										IN paymenttype varchar(250),IN acquirer varchar(250), IN currency varchar(250),IN start int,IN length int)
BEGIN
SELECT t1.CREATE_DATE,
t1.TXN_ID,t1.AMOUNT,t1.CURRENCY_CODE,
t1.INTERNAL_CARD_ISSUER_BANK,t1.INTERNAL_CARD_ISSUER_COUNTRY,
t1.CUST_EMAIL,
t1.PAYMENT_TYPE,
t1.MOP_TYPE,
t1.TXNTYPE,
t1.PAY_ID,t1.Status as 'STATUS' ,
t1.ORDER_ID,t1.RESPONSE_MESSAGE,t1.CUST_NAME,t1.ORIG_TXN_ID,
t1.CARD_MASK,t1.PRODUCT_DESC, su.businessName,t1.AMOUNT - (Select  IFNULL(sum(AMOUNT),0.00) from TRANSACTION where OID = t1.OID and TxnType = 'REFUND' and status ='Captured') As 'RefundableAmount'
   FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId 
   where (CREATE_DATE between fromdate and todate) 
   and  (TxnType = 'SALE' and status ='Captured') and (CASE when (su.resellerId=resellerId) and (merchantPayId = 'ALL') then 1 else (CASE when PAY_ID = merchantPayId then 1 else 0 END) END)
   and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
   and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
   and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
  
   order by t1.TXN_ID DESC   LIMIT start,length;
END$$
 DELIMITER ;
 
 
 
 
DELIMITER $$
CREATE  PROCEDURE `resellerFailedSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN resellerId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN start int,IN length int)
BEGIN
  if(merchantPayId = 'ALL') then
		   SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`resellerId` AS `resellerId`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
         and su.resellerId=resellerId
         and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC LIMIT start,length;
   ELSE
			SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`resellerId` AS `resellerId`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and (t1.TXNTYPE='NEWORDER' and t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed'))
		   and t1.PAY_ID = merchantPayId and su.resellerId=resellerId
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;
	END IF;   
   END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `resellerIncompleteSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250),IN resellerId varchar(250),IN paymenttype varchar(250),IN status varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN start int,IN length int)
BEGIN
 if(merchantPayId = 'ALL') then
		   SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`resellerId` AS `resellerId`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Sent to Bank'))
           or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
		    and su.resellerId=resellerId
           and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) order by t1.TXN_ID DESC  LIMIT start,length;
   ELSE
			SELECT 
				`t1`.`OID` AS `OID`,
                `t1`.`ORIG_TXN_ID` AS `ORIG_TXN_ID`,
				`t1`.`CREATE_DATE` AS `CREATE_DATE`,
				`t1`.`TXN_ID` AS `TXN_ID`,
				`t1`.`AMOUNT` AS `AMOUNT`,
				IFNULL(`t1`.`CURRENCY_CODE`, '') AS `CURRENCY_CODE`,
				`t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
				IFNULL(`t1`.`PAYMENT_TYPE`, '') AS `PAYMENT_TYPE`,
				IFNULL(`t1`.`MOP_TYPE`, '') AS `MOP_TYPE`,
				`t1`.`TXNTYPE` AS `TXNTYPE`,
				`t1`.`PAY_ID` AS `PAY_ID`,
				`su`.`businessName` AS `businessName`,
				`t1`.`STATUS` AS `STATUS`,
				`t1`.`ORDER_ID` AS `ORDER_ID`,
				`t1`.`RESPONSE_MESSAGE` AS `RESPONSE_MESSAGE`,
				`t1`.`CUST_NAME` AS `CUST_NAME`,
				`t1`.`CARD_MASK` AS `CARD_MASK`,
				`t1`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
				`su`.`resellerId` AS `resellerId`,
				`t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
				IFNULL(`t1`.`INTERNAL_REQUEST_FIELDS`, '') AS `INTERNAL_REQUEST_FIELDS`
			FROM
				(`TRANSACTION` `t1`
				LEFT JOIN `User` `su` ON ((`t1`.`PAY_ID` = `su`.`payId`)))   
		   where (t1.CREATE_DATE between fromdate and todate) and ((t1.TXNTYPE in ('NEWORDER','SALE','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank'))
		   or (t1.TXNTYPE='ENROLL' and t1.STATUS='ENROLLED' and (Select count(*) from TRANSACTION  where TXNTYPE in('SALE','AUTHORISE') and ORIG_TXN_ID=t1.ORIG_TXN_ID)=0))
           and t1.PAY_ID = merchantPayId and su.resellerId=resellerId
		   and (CASE when paymenttype='ALL' then 1 else (CASE when t1.PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
		   and (CASE when status='ALL' then 1 else (CASE when t1.STATUS = status then 1 else 0 END) END)
		   and (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
		   and (CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END)  order by t1.TXN_ID DESC  LIMIT start,length;  
	END IF;   
    END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `resellerInvalidSummary`(IN fromdate datetime, IN todate datetime,IN merchantPayId 
						  varchar(250), IN resellerId varchar(250))
BEGIN
   SELECT t1.CREATE_DATE,t1.TXN_ID ,t1.AMOUNT,IFNULL(t1.CUST_EMAIL,'') as 'CUST_EMAIL' ,IFNULL(t1.PAYMENT_TYPE,'') as 'PAYMENT_TYPE' ,IFNULL(t1.MOP_TYPE,'') as 'MOP_TYPE',IFNULL(t1.TXNTYPE,'') as 'TXNTYPE',
   t1.PAY_ID,su.businessName,t1.Status as 'STATUS' ,IFNULL(t1.ORDER_ID,'0') as 'ORDER_ID',t1.RESPONSE_MESSAGE,IFNULL(t1.CUST_NAME,'') as 'CUST_NAME',IFNULL(t1.CARD_MASK,'') as 'CARD_MASK',IFNULL(t1.PRODUCT_DESC,'') as 'PRODUCT_DESC',
   IFNULL(t1.INTERNAL_REQUEST_FIELDS,'') as 'INTERNAL_REQUEST_FIELDS'
   FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId
   where (t1.CREATE_DATE between fromdate and todate) and
   ((t1.TXNTYPE = 'NEWORDER' and t1.status in ('Invalid')) or ((t1.TXNTYPE = 'ENROLL' and t1.status in ('Invalid'))) or ((t1.TXNTYPE in ('SALE','AUTHORISE') and t1.status in ('Invalid'))))
   and (CASE when (merchantPayId = 'ALL') then 1 else (CASE when t1.PAY_ID = merchantPayId then 1 else 0 END) END);
 END$$
DELIMITER ;
 
 
 
DELIMITER $$
CREATE  PROCEDURE `resellerLineChartTransactionRecord`(IN payId varchar(250),IN resellerId varchar(250),IN currency varchar(250),IN dateFrom datetime, IN dateTo datetime)
BEGIN
IF (payId = 'ALL MERCHANTS') THEN
	SELECT date(t1.CREATE_DATE) as txndate,t1.CURRENCY_CODE,
	SUM(CASE WHEN (t1.TXNTYPE ='SALE'  or t1.TXNTYPE = 'AUTHORISE')  and (t1.CREATE_DATE >= dateFrom and CREATE_DATE <= dateTo) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved'))  and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) )THEN 1 ELSE 0 END) totalSuccess, 
	SUM(CASE WHEN ((t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN 1 ELSE 0 END) totalRefunded,
    SUM(CASE WHEN (((t1.TXNTYPE ='NEWORDER')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalFailed,
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.CREATE_DATE between dateFrom and dateTo) and su.userType <> 'POSMERCHANT' and t1.PAYMENT_TYPE is not NULL) and (su.resellerId = resellerId) and (t1.CURRENCY_CODE = currency) 
	group by DAYOFMONTH(t1.CREATE_DATE) ,t1.CURRENCY_CODE
	order by t1.CREATE_DATE ASC;
else
	SELECT date(t1.CREATE_DATE) as txndate,t1.CURRENCY_CODE,
	SUM(CASE WHEN (t1.TXNTYPE ='SALE' or t1.TXNTYPE = 'AUTHORISE')  and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN 1 ELSE 0 END) totalSuccess,
	SUM(CASE WHEN ((t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN 1 ELSE 0 END) totalRefunded,
    SUM(CASE WHEN (((t1.TXNTYPE ='NEWORDER')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalFailed,
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1 where ((t1.CREATE_DATE between dateFrom and dateTo)  and  t1.PAY_ID = payid and t1.PAYMENT_TYPE is not NULL)  and (t1.CURRENCY_CODE = currency)
	group by DAYOFMONTH(t1.CREATE_DATE),t1.CURRENCY_CODE
	order by t1.CREATE_DATE ASC;
	END IF;   
   END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `resellerRefundReport`(IN fromdate datetime, IN todate datetime,IN payid varchar(250), IN resellerId varchar(250),
IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250))
BEGIN
	Select t1.ORDER_ID, t1.ORIG_TXN_ID as 'TXN_ID', t1.PAY_ID, su.businessName As 'BUSINESS_NAME', CURRENCY_CODE,t1.INTERNAL_USER_EMAIL,t1.INTERNAL_CUST_IP, (Select min(CREATE_DATE) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'TXN_DATE',
	t1.CREATE_DATE as 'REFUND_DATE', t1.AMOUNT as 'REFUND_AMOUNT', (Select min(Amount) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'TXN_AMOUNT', (Select min(Amount) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) - (Select IFNULL(sum(AMOUNT),0.00) from TRANSACTION where OID = t1.OID and TxnType = 'REFUND' and status ='Captured') As 'REFUNDABLE_AMOUNT',
	(Select Payment_Type from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'PAYMENT_TYPE', (Select MOP_TYPE from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID) as 'MOP_TYPE'
	 from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId  where t1.ORIG_TXN_ID in (Select distinct t1.TXN_ID  from TRANSACTION t1 where (txntype = 'SALE' AND t1.status = 'Captured') 
	OR (t1.txntype = 'AUTHORISE' AND t1.status = 'Approved')) and (t1.TXNTYPE = 'REFUND' and t1.STATUS = 'Captured')
	and (t1.CREATE_DATE between fromdate and todate) and (su.resellerId=resellerId)
    and (CASE when (su.resellerId=resellerId) and (payid = 'ALL') then 1 else (CASE when PAY_ID = payid then 1 else 0 END) END)
    and (CASE when paymenttype='ALL' then 1 else (CASE when (select count(Payment_Type) from TRANSACTION where TXN_ID = t1.ORIG_TXN_ID and PAYMENT_TYPE = paymenttype) >0  then 1 else 0 END) END)
	and  (CASE when acquirer='ALL' then 1 else (CASE when t1.ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
	and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END);
   END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `resellerStatisticsSummary`(IN payId varchar(250),IN resellerId varchar(250),IN currency varchar(250))
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
    select  totalSuccess,totalFailed, totalRefunded,refundedAmount,approvedAmount from ((Select count(*) as totalSuccess from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY)))  totalSuccess,
        (Select count(*) as totalFailed from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId)  and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) totalFailed,
		(Select COALESCE(sum(t1.Amount),0) as approvedAmount from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved'))  and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and  (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) approvedAmount,
		(Select count(*) as totalRefunded from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId)   and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) totalRefunded,
        (Select  COALESCE(sum(t1.Amount),0) as refundedAmount from TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (su.resellerId=resellerId) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) refundedAmount);
	ELSE
        select  totalSuccess,totalFailed, totalRefunded,refundedAmount,approvedAmount from ((Select count(*) as totalSuccess from TRANSACTION t1 where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY)))  totalSuccess, 
        (Select count(*) as totalFailed from TRANSACTION t1 where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed'))  and (t1.PAY_ID = payId)  and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) totalFailed,
		(Select COALESCE(sum(t1.Amount),0) as approvedAmount from TRANSACTION t1 where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved'))  and (t1.PAY_ID = payId)  and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) approvedAmount,
        (Select count(*) as totalRefunded from TRANSACTION t1 where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured')  and (t1.PAY_ID = payId)  and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) totalRefunded,
		(Select  COALESCE(sum(t1.Amount),0) as refundedAmount from TRANSACTION t1 where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured')  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= DATE(NOW()) and t1.CREATE_DATE < date_add(DATE(NOW()), INTERVAL 1 DAY))) refundedAmount);
	END IF;             
      END$$
 DELIMITER ;
 
DELIMITER $$
CREATE  PROCEDURE `resellerTransactionsAllMerch`(IN fromdate datetime, IN todate datetime,IN payId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN resellerId varchar(250),IN start int,IN length int )
BEGIN
 if(payId = 'ALL') then
	SELECT 
        `t1`.`ORIG_TXN_ID` AS `OID`,
        `t1`.`CREATE_DATE` AS `CREATE_DATE`,
        `t1`.`TXN_ID` AS `TXN_ID`,
        `t1`.`ORDER_ID` AS `ORDER_ID`,
        `t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
        `t1`.`PAY_ID` AS `PAY_ID`,
        `t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
        `t1`.`PAYMENT_TYPE` AS `PAYMENT_TYPE`,
        `t1`.`MOP_TYPE` AS `MOP_TYPE`,
        `t1`.`TXNTYPE` AS `TXNTYPE`,
        `t1`.`CURRENCY_CODE` AS `CURRENCY_CODE`,
        `t1`.`AUTH_CODE` AS `AUTH_CODE`,
        `su`.`businessName` AS `businessName`,
        `t1`.`AMOUNT` AS `CAPTURED_AMOUNT`,
        '0.00' AS `REFUND_AMOUNT`,
        '0.00' AS `CHARGEBACK_AMOUNT`
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
    where (CREATE_DATE between fromdate and todate) and ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'AUTHORISE' AND `t1`.`STATUS` = 'Approved'))
    and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and su.resellerId=resellerId  order by t1.TXN_ID DESC  LIMIT start,length;
   ELSE
			SELECT 
				 `t1`.`ORIG_TXN_ID` AS `OID`,
        `t1`.`CREATE_DATE` AS `CREATE_DATE`,
        `t1`.`TXN_ID` AS `TXN_ID`,
        `t1`.`ORDER_ID` AS `ORDER_ID`,
        `t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
        `t1`.`PAY_ID` AS `PAY_ID`,
        `t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
        `t1`.`PAYMENT_TYPE` AS `PAYMENT_TYPE`,
        `t1`.`MOP_TYPE` AS `MOP_TYPE`,
        `t1`.`TXNTYPE` AS `TXNTYPE`,
        `t1`.`CURRENCY_CODE` AS `CURRENCY_CODE`,
        `t1`.`AUTH_CODE` AS `AUTH_CODE`,
        `su`.`businessName` AS `businessName`,
        `t1`.`AMOUNT` AS `CAPTURED_AMOUNT`,
        '0.00' AS `REFUND_AMOUNT`,
        '0.00' AS `CHARGEBACK_AMOUNT`
			FROM
			  `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
    where (CREATE_DATE between fromdate and todate) and ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'AUTHORISE' AND `t1`.`STATUS` = 'Approved'))
    and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
    and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
     and t1.PAY_ID = payId and su.resellerId=resellerId order by t1.TXN_ID DESC   LIMIT start,length; 
	END IF;   
       END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `searchPayment`(IN fromdate datetime, IN todate datetime,IN transactionId varchar(250),
IN orderId varchar(250),IN customerEmail varchar(250),IN payId varchar(250),
IN paymenttype varchar(250),IN  Userstatus varchar(250),IN currency varchar(250),IN userType varchar(250),IN start int,IN length int)
BEGIN
	SELECT 
		`transaction`.`TXN_ID` AS `TXN_ID`,
		`transaction`.`PAY_ID` AS `PAY_ID`,
		`transaction`.`INTERNAL_CARD_ISSUER_BANK` AS `INTERNAL_CARD_ISSUER_BANK`,
		`transaction`.`INTERNAL_CARD_ISSUER_COUNTRY` AS `INTERNAL_CARD_ISSUER_COUNTRY`,
		`transaction`.`CUST_NAME` AS `CUST_NAME`,
        `transaction`.`CUST_EMAIL` AS `CUST_EMAIL`,
		`subuser`.`businessName` AS `businessName`,
		`transaction`.`TXNTYPE` AS `TXNTYPE`,
		`transaction`.`PAYMENT_TYPE` AS `PAYMENT_TYPE`,
        `transaction`.`MOP_TYPE` AS `MOP_TYPE`,
		`transaction`.`CARD_MASK` AS `CARD_MASK`,
		`transaction`.`status` as STATUS,
        `transaction`.`CREATE_DATE` AS `CREATE_DATE`,
		`transaction`.`AMOUNT` AS `CAPTURED_AMOUNT`,
        `transaction`.`ORDER_ID` AS `ORDER_ID`,
		`transaction`.`PRODUCT_DESC` AS `PRODUCT_DESC`,
	    `transaction`.`CURRENCY_CODE` AS `CURRENCY_CODE`
    FROM `TRANSACTION` `transaction` left join User subuser on PAY_ID = subuser.payId
	where (CREATE_DATE >= fromdate and CREATE_DATE  <= todate) 
    and ((`transaction`.`TXNTYPE` in ('SALE','REFUND') and `transaction`.`STATUS` = 'Captured') 
    or 	(`transaction`.`TXNTYPE` = 'NEWORDER' and `transaction`.`STATUS` IN ('Rejected','Cancelled'))
    or 	(`transaction`.`TXNTYPE` = 'AUTHORISE' and `transaction`.`STATUS` = 'Approved') 
	or (`transaction`.`TXNTYPE` = 'ENROLL' and `transaction`.`STATUS` IN ( 'Timeout','Enrolled') )
    or  (`transaction`.`TXNTYPE` in ('SALE','AUTHORISE') and `transaction`.`status` IN ('Declined','Rejected','Error','Denied by risk','Failed','Authentication Failed','Invalid','Pending','Timeout','Browser Closed','Sent to Bank'))
    or 	((`transaction`.`TXNTYPE` = 'ENROLL' and transaction.status in ('Invalid')))) 
	and (CASE when transactionId='' then 1 else (CASE when TXN_ID = transactionId then 1 else 0 END) END)
    and (CASE when orderId='' then 1 else (CASE when ORDER_ID = orderId then 1 else 0 END) END)
	and (CASE when customerEmail='' then 1 else (CASE when CUST_EMAIL = customerEmail then 1 else 0 END) END)
	and (CASE when paymenttype='' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when Userstatus='' then 1 else (CASE when status = Userstatus then 1 else 0 END) END)
    and (CASE when currency='' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and (CASE when ((userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payId = 'ALL') and (subuser.userType <> 'POSMERCHANT'))then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END) order by transaction.TXN_ID DESC  LIMIT start,length;
    END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `settlementRecord`(IN dateFrom varchar(250),IN dateTo varchar(250),IN payId varchar(250), IN currency varchar(250),IN paymenttype varchar(250))
BEGIN
	IF (payId = 'ALL') THEN
		SELECT txnDate, creationTimeStamp, merchant, txnId, paymentMethod, mop, currencyCode, txnAmount, tdr, serviceTax, netAmount, merchantFixCharge, txnType
		from Settlement s1 left join User su on s1.payId = su.payId where (su.userType <> 'POSMERCHANT') and  creationTimeStamp >= dateFrom and creationTimeStamp <= dateTo 
 		and (CASE when currency='ALL' then 1 else (CASE when s1.currencyCode = currency then 1 else 0 END) END)
 		and  (CASE when paymenttype='ALL' then 1 else (CASE when s1.paymentMethod = paymenttype then 1 else 0 END) END);
 	ELSE
 		SELECT txnDate, creationTimeStamp, merchant, txnId, paymentMethod, mop, currencyCode, txnAmount, tdr, serviceTax, netAmount, merchantFixCharge, txnType
 		from Settlement s1 left join User su on s1.payId = su.payId where (s1.payId = payId) and creationTimeStamp >= dateFrom and creationTimeStamp <= dateTo 
 		and (CASE when currency='ALL' then 1 else (CASE when s1.currencyCode = currency then 1 else 0 END) END)
 		and (CASE when paymenttype='ALL' then 1 else (CASE when s1.paymentMethod = paymenttype then 1 else 0 END) END);
 	END IF;
   END$$
 DELIMITER ;
 
 
 
DELIMITER $$
CREATE PROCEDURE `statisticsSummary`(IN startDate varchar(250),IN endDate varchar(250),IN payId varchar(250),IN currency varchar(250))
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
    select  totalSuccess,totalFailed, totalRefunded,refundedAmount,approvedAmount,totalFailedAmount,totalPending,totalPendingAmount,totalCancel,totalCancelAmount,totalInvalid,totalInvalidAmount from ((Select count(*) as totalSuccess from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate))  totalSuccess,
      (Select count(*) as totalFailed from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Rejected by acquirer','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailed,
      (Select COALESCE(sum(t1.Amount),0) as totalFailedAmount from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Rejected by acquirer','Failed','Authentication Failed')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedAmount,
      (Select count(*) as totalPending from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalPending,
		(Select COALESCE(sum(t1.Amount),0) as totalPendingAmount from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalPendingAmount,
		(Select COALESCE(sum(t1.Amount),0) as approvedAmount from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved'))  and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) approvedAmount,
		(Select count(*) as totalCancel from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancel,
		(Select COALESCE(sum(t1.Amount),0) as totalCancelAmount from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelAmount,
		(Select count(*) as totalInvalid from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('INVALID','ENROLL','NEWORDER','SALE','AUTHORISE') and t1.status in ('Invalid')) and (su.userType <> 'POSMERCHANT') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalInvalid,
		(Select COALESCE(sum(t1.Amount),0) as totalInvalidAmount from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('INVALID','ENROLL','NEWORDER','SALE','AUTHORISE') and t1.status in ('Invalid')) and (su.userType <> 'POSMERCHANT')  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalInvalidAmount,
		(Select count(*) as totalRefunded from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency)   and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalRefunded,
      (Select  COALESCE(sum(t1.Amount),0) as refundedAmount from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (su.userType <> 'POSMERCHANT') and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) refundedAmount);

	ELSE
      select  totalSuccess,totalFailed, totalRefunded,refundedAmount,approvedAmount,totalFailedAmount,totalPending,totalPendingAmount,totalCancel,totalCancelAmount,totalInvalid,totalInvalidAmount from ((Select count(*) as totalSuccess from TRANSACTION t1 where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate))  totalSuccess, 
      (Select count(*) as totalFailed from TRANSACTION t1 where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Rejected by acquirer','Failed','Authentication Failed'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailed,
      (Select COALESCE(sum(t1.Amount),0) as totalFailedAmount from TRANSACTION t1 where t1.TXNTYPE='NEWORDER' and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Rejected by acquirer','Failed','Authentication Failed'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalFailedAmount,
      (Select count(*) as totalPending from TRANSACTION t1 where (t1.TXNTYPE in ('NEWORDER','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalPending,
      (Select COALESCE(sum(t1.Amount),0)  as totalPendingAmount from TRANSACTION t1 where (t1.TXNTYPE in ('NEWORDER','AUTHORISE') and t1.STATUS IN ('Pending','Timeout','Browser Closed','Enrolled','Sent to Bank'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalPendingAmount,
		(Select COALESCE(sum(t1.Amount),0) as approvedAmount from TRANSACTION t1 where ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved'))  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) approvedAmount,
     	(Select count(*) as totalCancel from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency)  and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancel,
		(Select COALESCE(sum(t1.Amount),0) as totalCancelAmount from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('NEWORDER','ENROLL','SALE','AUTHORISE') and t1.STATUS IN ('Cancelled')) and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT')and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalCancelAmount,
		(Select count(*) as totalInvalid from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('INVALID','ENROLL','NEWORDER','SALE','AUTHORISE') and t1.status in ('Invalid')) and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalInvalid,
		(Select COALESCE(sum(t1.Amount),0) as totalInvalidAmount from TRANSACTION t1 left join USER su on t1.PAY_ID = su.payId where (t1.TXNTYPE in ('INVALID','ENROLL','NEWORDER','SALE','AUTHORISE') and t1.status in ('Invalid')) and (t1.PAY_ID = payId) and (su.userType <> 'POSMERCHANT') and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalInvalidAmount,
	   (Select count(*) as totalRefunded from TRANSACTION t1 where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured')  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) totalRefunded,
		(Select  COALESCE(sum(t1.Amount),0) as refundedAmount from TRANSACTION t1 where (t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured')  and (t1.PAY_ID = payId) and (t1.CURRENCY_CODE= currency) and (t1.CREATE_DATE >= startDate and t1.CREATE_DATE <= endDate)) refundedAmount);
	END IF;           
END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `totalTransactionRecord`(IN payId varchar(250),IN currency varchar(250),IN dateFrom datetime, IN dateTo datetime)
BEGIN
IF (payId = 'ALL MERCHANTS') THEN
	SELECT date(t1.CREATE_DATE) as txndate,t1.CURRENCY_CODE,
	SUM(CASE WHEN (t1.TXNTYPE ='SALE'  or t1.TXNTYPE = 'AUTHORISE')  and (t1.CREATE_DATE >= dateFrom and CREATE_DATE <= dateTo) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved'))  and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) )THEN 1 ELSE 0 END) totalSuccess, 
	SUM(CASE WHEN ((t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN 1 ELSE 0 END) totalRefunded,
    SUM(CASE WHEN (((t1.TXNTYPE ='NEWORDER')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalFailed,
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where ((t1.CREATE_DATE between dateFrom and dateTo) and su.userType <> 'POSMERCHANT' and t1.PAYMENT_TYPE is not NULL) and (t1.CURRENCY_CODE = currency)
	group by DAYOFMONTH(t1.CREATE_DATE) ,t1.CURRENCY_CODE
	order by t1.CREATE_DATE ASC;
else
	SELECT date(t1.CREATE_DATE) as txndate,t1.CURRENCY_CODE,
	SUM(CASE WHEN (t1.TXNTYPE ='SALE' or t1.TXNTYPE = 'AUTHORISE')  and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN 1 ELSE 0 END) totalSuccess,
	SUM(CASE WHEN ((t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (t1.PAY_ID = payId) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN 1 ELSE 0 END) totalRefunded,
    SUM(CASE WHEN (((t1.TXNTYPE ='NEWORDER')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo) and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalFailed,
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and (t1.CREATE_DATE >= dateFrom and t1.CREATE_DATE <= dateTo))THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1 where ((t1.CREATE_DATE between dateFrom and dateTo)  and  t1.PAY_ID = payid and t1.PAYMENT_TYPE is not NULL) and (t1.CURRENCY_CODE = currency)
	group by DAYOFMONTH(t1.CREATE_DATE),t1.CURRENCY_CODE
	order by t1.CREATE_DATE ASC;
END IF;
 
END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `transactionsAllMerch`(IN fromdate datetime, IN todate datetime,IN payId varchar(250),IN paymenttype varchar(250),IN acquirer varchar(250),IN currency varchar(250),IN userType varchar(250) ,IN start int,IN length int)
BEGIN
	SELECT 
        `t1`.`ORIG_TXN_ID` AS `OID`,
        `t1`.`CREATE_DATE` AS `CREATE_DATE`,
        `t1`.`TXN_ID` AS `TXN_ID`,
        `t1`.`ORDER_ID` AS `ORDER_ID`,
        `t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
        `t1`.`PAY_ID` AS `PAY_ID`,
        `t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
        `t1`.`PAYMENT_TYPE` AS `PAYMENT_TYPE`,
        `t1`.`MOP_TYPE` AS `MOP_TYPE`,
        `t1`.`TXNTYPE` AS `TXNTYPE`,
        `t1`.`CURRENCY_CODE` AS `CURRENCY_CODE`,
        `t1`.`AUTH_CODE` AS `AUTH_CODE`,
        `su`.`businessName` AS `businessName`,
        `t1`.`AMOUNT` AS `CAPTURED_AMOUNT`,
        `se`.`status` as STATUS,
        '0.00' AS `REFUND_AMOUNT`,
        '0.00' AS `CHARGEBACK_AMOUNT`
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
        left join Settlement se on TXN_ID = se.txnId
    where (CREATE_DATE between fromdate and todate) and ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'AUTHORISE' AND `t1`.`STATUS` = 'Approved'))
    and (CASE when paymenttype='ALL' then 1 else (CASE when PAYMENT_TYPE = paymenttype then 1 else 0 END) END)
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL' then 1 else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when currency='ALL' then 1 else (CASE when CURRENCY_CODE = currency then 1 else 0 END) END)
    and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END) order by t1.TXN_ID DESC LIMIT start,length;
END$$
 DELIMITER ;
 
 
DELIMITER $$
CREATE  PROCEDURE `weeklyTransaction`(IN payId varchar(250),IN currency varchar(250),IN fromdate varchar(250), IN todate varchar(250))
BEGIN
	IF (payId = 'ALL MERCHANTS') THEN
		SELECT date(t1.CREATE_DATE) as txndate,
		SUM(CASE WHEN((CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) and PAYMENT_TYPE = 'CC' and ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and CREATE_DATE >= fromdate and CREATE_DATE<= todate) THEN 1 else 0 END)CC,
		SUM(CASE WHEN((CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) and PAYMENT_TYPE = 'DC' and ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and CREATE_DATE >= fromdate and CREATE_DATE<= todate) THEN 1 else 0 END)DC,
		SUM(CASE WHEN((CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) and PAYMENT_TYPE = 'NB' and ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and CREATE_DATE >= fromdate and CREATE_DATE<= todate) THEN 1 else 0 END)NB,
		SUM(CASE WHEN((CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) and PAYMENT_TYPE = 'WL' and ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and CREATE_DATE >= fromdate and CREATE_DATE<= todate) THEN 1 else 0 END)WL
        FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (su.userType <> 'POSMERCHANT') and CREATE_DATE >= fromdate and CREATE_DATE<= todate GROUP BY DATE(CREATE_DATE);

	ELSE
		SELECT date(t1.CREATE_DATE) as txndate,
		SUM(CASE WHEN((CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) and PAYMENT_TYPE = 'CC' and ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and CREATE_DATE >= fromdate and CREATE_DATE<= todate) THEN 1 else 0 END)CC,
		SUM(CASE WHEN((CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) and PAYMENT_TYPE = 'DC' and ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and CREATE_DATE >= fromdate and CREATE_DATE<= todate) THEN 1 else 0 END)DC,
		SUM(CASE WHEN((CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) and PAYMENT_TYPE = 'NB' and ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and CREATE_DATE >= fromdate and CREATE_DATE<= todate) THEN 1 else 0 END)NB,
        SUM(CASE WHEN((CASE when currency='ALL' then 1 else (CASE when t1.CURRENCY_CODE = currency then 1 else 0 END) END) and PAYMENT_TYPE = 'WL' and ((t1.TXNTYPE ='SALE' and  t1.STATUS ='Captured')  or (t1.TXNTYPE ='AUTHORISE' and  t1.STATUS ='Approved')) and CREATE_DATE >= fromdate and CREATE_DATE<= todate) THEN 1 else 0 END)WL
		FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (t1.PAY_ID = payId) and CREATE_DATE >= fromdate and CREATE_DATE<= todate GROUP BY DATE(CREATE_DATE);
	END IF;
END$$
 DELIMITER ;
 
 
 
DELIMITER $$
 CREATE  PROCEDURE `industry_base_totalTransaction`(IN payId varchar(250), IN industryCategory varchar(250), IN currency varchar(250),IN dateFrom datetime, IN dateTo datetime)
BEGIN
IF (payId = 'ALL') THEN
	SELECT HOUR(t1.CREATE_DATE) as txndate,t1.CURRENCY_CODE,su.industryCategory, 
	SUM(CASE WHEN (t1.TXNTYPE ='SALE'  or t1.TXNTYPE = 'AUTHORISE') THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')))THEN 1 ELSE 0 END) totalSuccess, 
	SUM(CASE WHEN ((t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured'))THEN 1 ELSE 0 END) totalRefunded,
    SUM(CASE WHEN (((t1.TXNTYPE ='NEWORDER'))  and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalFailed,
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')))THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1 left join User su on t1.PAY_ID = su.payId where (( t1.CREATE_DATE BETWEEN dateFrom + +'00:00:00' AND dateTo +'23:59:59') and su.userType <> 'POSMERCHANT' and t1.PAYMENT_TYPE is not NULL) and (t1.CURRENCY_CODE = currency)
	and (CASE when industryCategory='ALL' then 1 else (CASE when su.industryCategory = industryCategory then 1 else 0 END) END)
    group by Hour(t1.CREATE_DATE) ,t1.CURRENCY_CODE
	order by t1.CREATE_DATE ASC;
else
	SELECT HOUR(t1.CREATE_DATE) as txndate,t1.CURRENCY_CODE,su.industryCategory, 
	SUM(CASE WHEN (t1.TXNTYPE ='SALE' or t1.TXNTYPE = 'AUTHORISE')   THEN 1 ELSE 0 END) totalTxns,  
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and su.industryCategory = industryCategory )THEN 1 ELSE 0 END) totalSuccess,
	SUM(CASE WHEN ((t1.TXNTYPE ='REFUND' and t1.STATUS ='Captured') and (t1.PAY_ID = payId)  and su.industryCategory = industryCategory )THEN 1 ELSE 0 END) totalRefunded,
    SUM(CASE WHEN (((t1.TXNTYPE ='NEWORDER')) and su.industryCategory = industryCategory  and (t1.STATUS IN ('Declined','Rejected','Error','Denied by risk','Duplicate','Failed','Authentication Failed')))THEN 1 ELSE 0 END) totalFailed,
	SUM(CASE WHEN ((t1.TXNTYPE ='SALE' and t1.STATUS IN ('Captured')) or (t1.TXNTYPE = 'AUTHORISE' and t1.STATUS IN ('Approved')) and su.industryCategory = industryCategory )THEN t1.AMOUNT ELSE 0 END) approvedAmount  
	FROM TRANSACTION t1  left join User su on t1.PAY_ID = su.payId where (( t1.CREATE_DATE BETWEEN dateFrom + +'00:00:00' AND dateTo +'23:59:59') and t1.PAY_ID = payId and t1.PAYMENT_TYPE is not NULL) and (t1.CURRENCY_CODE = currency)
    and (CASE when industryCategory='ALL' then 1 else (CASE when su.industryCategory = industryCategory then 1 else 0 END) END)
	group by Hour(t1.CREATE_DATE),t1.CURRENCY_CODE
	order by t1.CREATE_DATE ASC;
END IF;
 
END$$
 DELIMITER ;
 
 
DELIMITER $$ 
CREATE  PROCEDURE `misReportsExport`(IN dateFrom varchar(250),IN dateTo varchar(250),IN payId varchar(250), IN acquirer varchar(250),IN userType varchar(250))
BEGIN
	SELECT
        `t1`.`PAY_ID` AS `PAY_ID`,
        `t1`.`CREATE_DATE` AS `CREATE_DATE`,
        `t1`.`TXN_ID` AS `TXN_ID`,
        `t1`.`UPDATE_DATE` AS `UPDATE_DATE`,
        `t1`.`CUST_EMAIL` AS `CUST_EMAIL`,
        `t1`.`PAY_ID` AS `PAY_ID`,
        `t1`.`ACQUIRER_TYPE` AS `ACQUIRER_TYPE`,
        `t1`.`PAYMENT_TYPE` AS `PAYMENT_TYPE`,
        `t1`.`MOP_TYPE` AS `MOP_TYPE`,
        `t1`.`TXNTYPE` AS `TXNTYPE`,
        `t1`.`CURRENCY_CODE` AS `CURRENCY_CODE`,
        `su`.`businessName` AS `businessName`,
		`su`.`ifscCode` AS `ifscCode`,
		`su`.`bankName` AS `bankName`,
		`su`.`accountNo` AS `accountNo`,
        `t1`.`AMOUNT` AS `AMOUNT`,
        `t1`.`status` as STATUS
       
    FROM
        `TRANSACTION` `t1` left join User su on PAY_ID = su.payId
    where (CREATE_DATE between dateFrom and dateTo)  and   ((`t1`.`TXNTYPE` = 'SALE' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'REFUND' AND `t1`.`STATUS` = 'Captured') or (`t1`.`TXNTYPE` = 'CAPTURE' AND `t1`.`STATUS` = 'Captured'))
	and (CASE when payId='ALL' then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END)
    and (CASE when acquirer='ALL'  then ACQUIRER_TYPE not in('CITRUS') else (CASE when ACQUIRER_TYPE = acquirer then 1 else 0 END) END)
    and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') and (payId = 'ALL') and (su.userType <> 'POSMERCHANT') then 1 else (CASE when PAY_ID = payId then 1 else 0 END) END);
END$$
DELIMITER ;

DELIMITER $$ 
CREATE  PROCEDURE `binRange_Records`(IN cardType varchar(250),IN mopType varchar(250),IN userType varchar(250),IN start int,IN length int)
BEGIN
SELECT
        `b`.`binCode` AS `binCode`,
        `b`.`issuerBankName` AS `issuerBankName`,
        `b`.`mopType` AS `mopType`,
        `b`.`cardType` AS `cardType`,
        `b`.`issuerCountry` AS `issuerCountry`,
        `b`.`productName` AS `productName`,
        `b`.`groupCode` AS `groupCode`,
        `b`.`rfu1` AS `rfu1`,
        `b`.`rfu2` AS `rfu2`
     FROM
	   `BinRange` `b` 
      where (CASE when cardType='ALL' then 1 else (CASE when `b`.`cardType` = cardType then 1 else 0 END) END)
	  and (CASE when mopType='ALL' then 1 else (CASE when `b`.`mopType` = mopType then 1 else 0 END) END)
     and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') then 1 else 0 END) LIMIT start,length;
END$$
DELIMITER ;

DELIMITER $$ 
CREATE  PROCEDURE `binRange_Records_count`(IN cardType varchar(250),IN mopType varchar(250),IN userType varchar(250))
BEGIN
SELECT count(*) recordsTotal
     FROM
	 `BinRange` `b` 
      where (CASE when cardType='ALL' then 1 else (CASE when cardType = cardType then 1 else 0 END) END)
	  and (CASE when mopType='ALL' then 1 else (CASE when mopType = mopType then 1 else 0 END) END)
      and (CASE when (userType='ADMIN' or userType='SUBADMIN' or userType='SUPERADMIN') then 1 else 0 END);
END$$
DELIMITER ;

