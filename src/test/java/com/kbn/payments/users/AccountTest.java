package com.kbn.payments.users;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.AccountDao;

import junit.framework.TestCase;

public class AccountTest extends TestCase {

	public void test(){
		
		AccountDao accountDao =new AccountDao();
		accountDao.find(16L);
		List<Long> list = new ArrayList<Long>();
		list.add(17L);
		list.add(18L);
		list.add(19L);
		list.add(20L);
		System.out.println(System.nanoTime());
		for(long id:list){
			for(int i=0;i<10;i++){
				Account account = accountDao.find(id);
				Session session;
				session = HibernateSessionProvider.getSession();
				Transaction tx = session.beginTransaction();
				session.load(account,account.getId());
				Set<AccountCurrency> set = account.getAccountCurrencySet();
				System.out.println(set.size());
				tx.commit();
				session.close();
			}
		}
		System.out.println(System.nanoTime());
	}
}
