package com.kbn.payments.users;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.json.JSONReader;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.citruspay.CitrusPayUtil;
import com.kbn.citruspay.Response;
import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.crypto.ChecksumUtils;
import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountDao;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.CitrusPaySubscription;
import com.kbn.commons.user.CitrusPaySubscriptionDao;
import com.kbn.commons.user.Mop;
import com.kbn.commons.user.MopDao;
import com.kbn.commons.user.MopTransaction;
import com.kbn.commons.user.MopTransactionDao;
import com.kbn.commons.user.Payment;
import com.kbn.commons.user.Permissions;
import com.kbn.commons.user.PermissionsDao;
import com.kbn.commons.user.Settlement;
import com.kbn.commons.user.SettlementDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DateCreater;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.emailer.Emailer;
import com.kbn.mobikwik.Constants;
import com.kbn.mobikwik.MobikwikIntegrator;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.RequestRouter;
import com.kbn.timesofmoney.DirecpayUtil;

import junit.framework.TestCase;

public class UserTest extends TestCase {
			
	public void testUser() throws Exception {
      
		
		
	//	timer1.schedule(test, 10000);
		 
	//	List<CitrusPaySubscription> list = dao.getCompletedSubs();
	//	System.out.println("test done" + list.toString());
		
	//	String pass = "RRN=604134079687&PAY_ID=1507281443471000&RESPONSE_DATE_TIME=2016-02-10 15:37:10&AVR=N&CARD_MASK=411111******1111&TXN_ID=1602101537091006&RETURN_URL=http://www.merchant.mmadpay.com/crm/jsp/sdkResponse.jsp&AMOUNT=100&STATUS=Captured&RESPONSE_CODE=000&HASH=252E41126B971E64312DCAEC46D97A329F87D3AA4626E12526B56F66E5113FA5&CUST_NAME=CITRUS DEMO&ORDER_ID=CITRUS0001&MOP_TYPE=VI&TXNTYPE=SALE&PRODUCT_DESC=CITRUS Demo Transaction&PAYMENT_ID=9222244361560410&PAYMENT_TYPE=CC&ACQ_ID=9222244361560410&ACQUIRER_TYPE=FSS&AUTH_CODE=999999&CURRENCY_CODE=356&CUST_EMAIL=demo@demo.com&CUST_PHONE=9911889966&RESPONSE_MESSAGE=SUCCESS";
	//	System.out.println(Hasher.getHash(pass));
		 /*GregorianCalendar cal = new GregorianCalendar();
		 GregorianCalendar cal1 = new GregorianCalendar();
		 Date currentDate = new Date();
		 Date createDate = new Date();
		 
		 cal.setTime(createDate);
		 cal.add(Calendar.DATE, -28);
		 
		 long diff = currentDate.getTime();
		 System.out.println(cal.getTime());
		 System.out.println(cal.getTime().getTime());
		 Long dateDifference =  currentDate.getTime() - cal.getTime().getTime();
		 System.out.println(dateDifference);*/
		 
//	String hash = CitrusPayUtil.generateHMAC("merchantAccessKey=PF7TNU4B3Y4UFI3JX3F3&transactionId=1603181701101002&amount=1.00", "2eef32e9e6b687a4b153c93b38488740101f6680");

		/*Map<String,String> requestMap = new HashMap<String,String>(); 

		requestMap.put(FieldType.ORIG_TXN_ID.getName(),"1601041419111001");
		requestMap.put(FieldType.PAY_ID.getName(), "1507291728371000");
		requestMap.put(FieldType.TXNTYPE.getName(),
				TransactionType.STATUS.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), "356");

		requestMap
				.put(FieldType.HASH.getName(),
						"1234567890123456789012345678901234567890123456789012345678901234");

		requestMap.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(), "N");

		// Preparing fields
		Fields fields = new Fields(requestMap);

		fields.logAllFields("All request fields :");
		RequestRouter router = new RequestRouter(fields);
	Fields	responseMap = new Fields(router.route());*/
       
   //    System.out.println(user5.getEmailId());
		UserDao dao = new UserDao();
	//	dao.getUserClass("1507291728371000");
		/*dao.getUserClass("1607281126041000");*/
	/*	dao.delete(dao.find("isha@mmadpay.com"));
		dao.delete(dao.find("test@test.com"));
		dao.delete(dao.find("test1@mmadpay.com"));
		dao.delete(dao.find("puneet1@mmadpay.com"));
		dao.delete(dao.find("harpreet@mmadpay.com"));*/
	/*	dao.delete(dao.find("test@tset.com"));
		
		System.out.println("success");*/
		
		
		Emailer emailer = new Emailer();
		//emailer.emailSender("Hiiiiiiiii", "Test mail", "puneet@mmadpay.com", false);
		
	}
	
	public static String testJsonParsing() {
		Response responseObj = new Response();
		String response = "{ \"merchantId\": \"mmadpay\",\"amount\": {\"value\": 1,\"currency\": \"INR\"},\"subscriberEmail\": \"puneet@mmadpay.com\",\"subscriptionId\": \"573f0653e4b0afa92be5ed20\","
				+ "\"lastPaymentDate\": \"\",\"nextBillingDate\": \"2016-05-20T12:42:59.995Z\",\"schedule\": {\"billingPeriodUnit\": 1,\"billingPeriodCycle\": \"monthly\"}}";
		Object obj;
		try {
			JSONParser parser = new JSONParser();
			obj = parser.parse(response);
			JSONObject jObject = (JSONObject) obj;
			
			Object amount = jObject.get("amount");
			if (null != amount) {
				JSONObject jSonArray = (JSONObject) amount;
				Object amt = (Object) jSonArray.get("value");
				if(null != amt){
					if(amt.getClass().equals(Long.class)){
						responseObj.setAmount(((Long) amt).toString());
					}else{
						responseObj.setAmount((String) amt);
					}
				}
				responseObj.setCurrency((String) jSonArray.get("currency"));
			}

			Object schedule = jObject.get("schedule");
			if (null != schedule) {
				JSONObject jSonArray = (JSONObject) schedule;
				Object billingPeriodUnitObj = jSonArray.get("billingPeriodUnit");
				if(billingPeriodUnitObj.getClass().equals(Long.class)){
					//set as long
				}else{
					//set as string
				}
			}
			
			Object subscriptionId = jObject.get("subscriptionId");
			if (null != subscriptionId) {
				System.out.println((String) subscriptionId);
			}
		} catch (ParseException parseException) {

		}
		return null;
	}
	public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
           throw new RuntimeException(ex);
        }
    }

}
