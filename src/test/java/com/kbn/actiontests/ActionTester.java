package com.kbn.actiontests;

import org.apache.struts2.StrutsTestCase;

import com.kbn.crm.action.IndexAction;
import com.opensymphony.xwork2.ActionProxy;

public class ActionTester extends StrutsTestCase{

	 public void testAction() throws Exception {
		 
	        request.setParameter("emailId", "neeraj@kbn.com");
	        request.setParameter("password", "Test@123");
	 
	        ActionProxy proxy = getActionProxy("/login");
	 
	        IndexAction action = (IndexAction) proxy.getAction();
	 
	        String result = proxy.execute();
	 
	        assertTrue("Problem There were errors present in fieldErrors but there should not have been any errors present", action.getFieldErrors().size() == 0);
	        assertEquals("Result returned form executing the action was not success but it should have been.", "success", result);
	 
	    }
}
