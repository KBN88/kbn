package com.kbn.gpay;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

public class GooglePayInitiatePayment {
	private static final String INIT_ENDPOINT_PROD = "https://nbupayments.googleapis.com/v1/merchantPayments:initiate";
    private static final String GOOGLE_MERCHANT_ID = "BCR2DN6TUWHNNVCK";

    public static void main(String[] args) {

        //  Code to generate accessToken
        String accessToken = createAccessToken();
        System.out.println(accessToken);

        /*
         *  Google pay Initiate post api call using access token.
         *  Make sure to whielist your merchant Id created using merchant console before testing this sample.
         * */

        String payLoad = createGooglePayInitiatePayLoad();
        String response = null;
        try {
            response = doPostRequestToGooglePayInitiate(payLoad, accessToken);
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean flag = validateGooglePayResponse(response);
        if (flag)
            System.out.println("Init request is successful, please make GET Api call to know transaction status");
        else
            System.out.println("Request failed with following response " + response);

    }

    public static String createAccessToken() {
        FileInputStream serviceAccount = null;
        try {

            serviceAccount = new FileInputStream("D://GPAY//BpayGpay-7702a95e1a11.json");
            // Authenticate a Google credential with the service account
            GoogleCredential googleCred = GoogleCredential.fromStream(serviceAccount);

            // Add the required scopes to the Google credential
            GoogleCredential scoped = googleCred.createScoped(
                    Arrays.asList("https://www.googleapis.com/auth/nbupaymentsmerchants")
            );

            // Use the Google credential to generate an access token
            scoped.refreshToken();
            String token = scoped.getAccessToken();

            return token;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String createGooglePayInitiatePayLoad() {
        /*
         * Transaction ID in payload has to be unique for each call (provided by PSP / Bank).
         * update your mobile number and googleMerchantId to test this sample.
         * */
       /* String payLoad = "{\n" +
                "        \"merchantInfo\": {\n" +
                "        \"googleMerchantId\": \"" + GOOGLE_MERCHANT_ID + "\"\n" +
                "        },\n" +
                "        \"userInfo\": {\n" +
                "        \"phoneNumber\": \"+918860705801\"\n" +
                "        },\n" +
                "        \"merchantTransactionDetails\": {\n" +
                "        \"transactionId\": \"847646644646\",\n" +
                "        \"amountPayable\": {\n" +
                "        \"currencyCode\": \"INR\",\n" +
                "        \"units\": 100,\n" +
                "        \"nanos\": 0\n" +
                "        },\n" +
                "        \"description\": \"Sample description\"\n" +
                "        },\n" +
                "        \"originatingPlatform\": \"ANDROID_APP\"\n" +
                "        }";*/
        String payLoad = "{\r\n" + 
        		"      \"merchantInfo\": {\r\n" + 
        		"            \"googleMerchantId\": \"BCR2DN6TUWHNNVCK\"\r\n" + 
        		"    },\r\n" + 
        		"      \"userInfo\": {\r\n" + 
        		"        \"phoneNumber\": \"+918860705801\"\r\n" + 
        		"       },\r\n" + 
        		"    \"merchantTransactionDetails\": {\r\n" + 
        		"        \"transactionId\": \"78676767675656\",\r\n" + 
        		"        \"amountPayable\": {\r\n" + 
        		"            \"currencyCode\": \"INR\",\r\n" + 
        		"            \"units\": 1.00,\r\n" + 
        		"            \"nanos\": 0\r\n" + 
        		"        },\r\n" + 
        		"           \"upiPaymentDetails\": {\r\n" + 
        		"                 \"vpa\": \"mmadpay@axisbank\"\r\n" + 
        		"           }\r\n" + 
        		"    },\r\n" + 
        		"      \"expiryTime\": \"2019-12-04T17:22:30Z\",\r\n" + 
        		"     \r\n" + 
        		"      \"originatingPlatform\": \"ANDROID_APP\"\r\n" + 
        		"}";
        return payLoad;
    }

    private static String doPostRequestToGooglePayInitiate(String payload, String accessToken) throws IOException {
        OutputStream os =null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(INIT_ENDPOINT_PROD);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "Bearer " + accessToken);

            os = conn.getOutputStream();
            os.write(payload.getBytes());
            os.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output = br.readLine();
            System.out.println("Output from Server .... \n" + output);
            conn.disconnect();
            return output;
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            conn.disconnect();
            os.close();
        }
        return null;
    }

    private static boolean validateGooglePayResponse(String response) {
        /*
         * Init call response will be empty JSON in success scenario. Make GET Api call to know the transaction status.
         * */
        if (response.equals("{}"))
            return true;
        else
            return false;
    }

    
    

}
