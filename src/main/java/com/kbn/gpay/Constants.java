package com.kbn.gpay;

public class Constants {

	public static final String OPENING_BRACE = "{";
	public static final String CLOSING_BRACE = "}";

	public static final String MERCHANT_INFO = "merchantInfo";
	public static final String GOOGLE_MERCHANT_ID = "googleMerchantId";
	public static final String USER_INFO = "userInfo";
	public static final String PHONE_NUMBER = "phoneNumber";

	public static final String MERCHANT_TRANSACTION_DETAILS = "merchantTransactionDetails";
	public static final String TRANSACTION_ID = "transactionId";
	public static final String AMOUNTPAYABLE = "amountPayable";
	public static final String CURRENCY_CODE = "currencyCode";

	public static final String UNITS = "units";
	public static final String NANOS = "nanos";

	public static final String UPI_PAYMENT_DETAILS = "upiPaymentDetails";
	public static final String VPA = "vpa";

	public static final String EXPIRY_TIME = "expiryTime";
	public static final String ORGINATIONG_PLATFORM = "originatingPlatform";
	public static final String TRANSACTION_IDENTIIFIER = "transactionIdentifier";
	
	public static final String MERCHANT_TRANSACTION_ID = "merchantTransactionId";
	

}
