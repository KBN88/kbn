package com.kbn.gpay;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Processor;

public class GPAYProcessor implements Processor {

	@Override
	public void preProcess(Fields fields) throws SystemException {
	
		
	}

	@Override
	public void process(Fields fields) throws SystemException {
		if (fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.NEWORDER.getName())) {
			// New Order Transactions are not processed by GPAY BANK UPI
			return;
		}
		if (!fields.get(FieldType.ACQUIRER_TYPE.getName()).equals(AcquirerType.GPAY.getCode())) {
			return;
		}
		(new GpayIntegrator()).process(fields);
	}

	@Override
	public void postProcess(Fields fields) throws SystemException {
	
		
	}

}
