package com.kbn.gpay;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import org.apache.log4j.Logger;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.pg.core.TransactionProcessor;

public class GPAYStatusTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(GPAYStatusTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		String getGpayStatus = converter.createGpayStatus(fields);
		logger.info("Gpay Status Verfiy Create request :" + getGpayStatus);
		// Code to generate accessToken
		String accessToken1 = createAccessToken();
		logger.info("Gpay accessToken:" + accessToken1);
		String response = null;
		try {
			response = TransactionCommunicator.getTransactionDetails(getGpayStatus, accessToken1);
		} catch (IOException exception) {
			// TODO Auto-generated catch block
			logger.info("Gpay Status Response:" + response);
		}
		GPAYTransformer gpayTransformer = new GPAYTransformer(response);
		gpayTransformer.gpayUpdate(fields);
		fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));
	}

	// token
	public static String createAccessToken() {
		FileInputStream serviceAccount = null;
		try {
			String keyPath = new PropertiesManager().getSystemProperty("GpayUpiPath");
			serviceAccount = new FileInputStream(keyPath);// Live
			logger.info("Gpay path:" + serviceAccount);
			GoogleCredential googleCred = GoogleCredential.fromStream(serviceAccount);
			// Add the required scopes to the Google credential
			GoogleCredential scoped = googleCred
					.createScoped(Arrays.asList("https://www.googleapis.com/auth/nbupaymentsmerchants"));
			// Use the Google credential to generate an access token
			scoped.refreshToken();
			String token = scoped.getAccessToken();
			return token;
		} catch (FileNotFoundException exception) {
			exception.printStackTrace();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		return null;
	}
}
