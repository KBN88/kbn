package com.kbn.gpay;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.apache.log4j.Logger;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.TransactionProcessor;

public class GPAYYSaleTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(GPAYYSaleTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createSaleTransaction(fields);
		logger.info("Request to GPAY UPI:" + request);
		// Code to generate accessToken
		String accessToken = createAccessToken();
		logger.info("Gpay accessToken:" + accessToken);
		String response = null;
		try {
			response = communicator.doPostRequestToGooglePayInitiate(request, accessToken);
			logger.info("GPAY UPI Response:" + response);
			System.out.println("Init request is successful, please make GET Api call to know transaction status");
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.SENT_TO_BANK.getName());
		} catch (IOException exception) {
			exception.printStackTrace();
			logger.error("GPAY UPI Response:" + exception);
		}
		
		
	
	}

	// token
	public static String createAccessToken() {
		FileInputStream serviceAccount = null;
		try {
			String keyPath = new PropertiesManager().getSystemProperty("GpayUpiPath");
			serviceAccount = new FileInputStream(keyPath);// Live
			//serviceAccount = new FileInputStream("D://GPAY//BpayGpay-7702a95e1a11.json");// local
			// Authenticate a Google credential with the service account
			logger.info("Gpay path:"+serviceAccount);
			GoogleCredential googleCred = GoogleCredential.fromStream(serviceAccount);

			// Add the required scopes to the Google credential
			GoogleCredential scoped = googleCred.createScoped(Arrays.asList("https://www.googleapis.com/auth/nbupaymentsmerchants"));

			// Use the Google credential to generate an access token
			scoped.refreshToken();
			String token = scoped.getAccessToken();
			logger.info("Generate Gpay token :"+token);

			return token;

		} catch (FileNotFoundException exception) {
			exception.printStackTrace();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		return null;
	}

	/*private static boolean validateGooglePayResponse(String response) {
		
		 * Init call response will be empty JSON in success scenario. Make GET Api call
		 * to know the transaction status.
		 
		if (response.equals("{}"))
			return true;
		else
			return false;
	}*/
}
