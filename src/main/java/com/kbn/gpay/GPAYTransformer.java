package com.kbn.gpay;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class GPAYTransformer {
	private static Logger logger = Logger.getLogger(GPAYTransformer.class.getName());
	private String responseMap;

	public GPAYTransformer(String responseField) {
		this.responseMap = responseField;
		logger.info("GPAY GPAYTransformer Response:" + responseMap);
	}

	public void gpayUpdate(Fields fields) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(responseMap);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jObject = (JSONObject) obj;
		logger.info("GPAY GPAYTransformer Response:" + jObject.toString());

		// Status
		String status = jObject.get("transactionStatus").toString();
		JSONParser parserstatus = new JSONParser();
		Object obj1 = null;
		try {
			obj1 = parserstatus.parse(status);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jObjectstatus = (JSONObject) obj1;
		logger.info("GPAY jObjectstatus Response:" + jObjectstatus.toString());
		JSONObject jObjectRRN = null;
		JSONObject jObjectamount = null;
		String pendingstatuts = jObjectstatus.get("status").toString();
		String declinestatuts = jObjectstatus.get("status").toString();

		if (pendingstatuts.equals("SUCCESS") || declinestatuts.equals("FAILURE")) {

			// getRRN
			String rrn = jObject.get("upiTransactionDetails").toString();
			JSONParser parserRRN = new JSONParser();
			Object obj2 = null;

			try {
				obj2 = parserRRN.parse(rrn);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			jObjectRRN = (JSONObject) obj2;

			// Amount
			String amount = jObject.get("amountPaid").toString();
			JSONParser parseramount = new JSONParser();
			Object obj3 = null;
			try {
				obj3 = parseramount.parse(amount);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			jObjectamount = (JSONObject) obj3;

		} else {

		}

		StatusType responsestatus = getSaleStatus(jObjectstatus.get("status").toString());
		ErrorType errorType = getSaleResponse(jObjectstatus.get("status").toString());
		fields.put(FieldType.STATUS.getName(), responsestatus.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getCode());
		fields.put(FieldType.PG_REF_NUM.getName(), jObject.get("googleTransactionId").toString());
		fields.put(FieldType.PG_DATE_TIME.getName(), jObject.get("lastUpdatedTime").toString());
		if (pendingstatuts.equals("SUCCESS") || declinestatuts.equals("FAILURE")) {
			fields.put(FieldType.RRN.getName(), jObjectRRN.get("upiRrn").toString());

		} else {

		}

	}

	private ErrorType getSaleResponse(String respCode) {

		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("SUCCESS")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("FAILURE")) {
			errorType = ErrorType.REJECTED;
		} else if (respCode.equals("DECLINED")) {
			errorType = ErrorType.DECLINED;
		} else if (respCode.equals("PAYMENT_NOT_INITIATED")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("IN_PROGRESS")) {
			errorType = ErrorType.SUCCESS;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;

	}

	private StatusType getSaleStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("SUCCESS")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("FAILURE")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("DECLINED")) {
			status = StatusType.DECLINED;
		} else if (respCode.equals("IN_PROGRESS")) {
			status = StatusType.SENT_TO_BANK;
		} else if (respCode.equals("PAYMENT_NOT_INITIATED")) {
			status = StatusType.SENT_TO_BANK;
		} else {
			status = StatusType.FAILED;
		}

		return status;
	}
}
