package com.kbn.gpay;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Currency;
import com.kbn.pg.core.pageintegrator.Transaction;

public class TransactionConverter extends Transaction {
	private static Logger logger = Logger.getLogger(TransactionConverter.class.getName());

	public String createSaleTransaction(Fields fields) {
		// Code to generate accessToken

		StringBuilder request = new StringBuilder();
		createGooglePayInitiatePayLoad(fields, request);
		StringBuilder payLoad = new StringBuilder();
		payLoad.append(Constants.OPENING_BRACE);
		payLoad.append(request);
		payLoad.append(Constants.CLOSING_BRACE);
		logger.info("Gpay accessToken:" + payLoad.toString());

		return payLoad.toString();
	}

	private void createGooglePayInitiatePayLoad(Fields fields, StringBuilder request) {
		GenerateNano nanosamount = new GenerateNano();
		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(tz);
		appendJsonField(Constants.MERCHANT_INFO, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.GOOGLE_MERCHANT_ID, fields.get(FieldType.MERCHANT_ID.getName()), request);
		request.append(Constants.CLOSING_BRACE);
		request.append(",");
		appendJsonField(Constants.USER_INFO, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.PHONE_NUMBER, fields.get(FieldType.UPI.getName()), request);
		request.append(Constants.CLOSING_BRACE);
		request.append(",");

		appendJsonField(Constants.MERCHANT_TRANSACTION_DETAILS, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.TRANSACTION_ID, fields.get(FieldType.TXN_ID.getName()), request);
		request.append(",");
		appendJsonField(Constants.AMOUNTPAYABLE, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.CURRENCY_CODE,
				Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName())), request);
		String amount =  Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()), fields.get(FieldType.CURRENCY_CODE.getName()));
		String unitamount = amount.substring(0, amount.indexOf('.'));
	    int amountnanos = nanosamount.nanois(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()), fields.get(FieldType.CURRENCY_CODE.getName())));
		appendJsonFieldWithoutDoboleCode(Constants.UNITS,unitamount,request);
		appendJsonFieldWithoutDoboleCode(Constants.NANOS, String.valueOf(amountnanos), request);

		request.append(Constants.CLOSING_BRACE);
		request.append(",");

		appendJsonField(Constants.UPI_PAYMENT_DETAILS, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.VPA, "bhartipay@axisbank", request);
		request.append(Constants.CLOSING_BRACE);
		request.append(Constants.CLOSING_BRACE);
		request.append(",");
		appendJsonField1(Constants.EXPIRY_TIME, df.format(new Date()), request);
		appendJsonField(Constants.ORGINATIONG_PLATFORM, "DESKTOP", request);

	}

	public String createGpayStatus(Fields fields) {
		UserDao userDao = new UserDao();
		User user = userDao.findBySafexpayUser(fields.get(FieldType.PAY_ID.getName()));
		logger.info("Gpay PayId:" + user.getPayId());
		Account account = user.getAccountUsingAcquirerCode(fields.get(FieldType.ACQUIRER_TYPE.getName()));
		logger.info("Gpay AcquireName:" + account.getAcquirerName()+"Gpay  Field AcquireName:"+fields.get(FieldType.ACQUIRER_TYPE.getName()));
		logger.info("Gpay AcquirePayId:" + account.getAcquirerPayId());
		AccountCurrency accountCurrency = null;
		try {
			logger.info("Gpay Currency:" + fields.get(FieldType.CURRENCY_CODE.getName()));
			accountCurrency = account.getAccountCurrency(fields.get(FieldType.CURRENCY_CODE.getName()));
			logger.info("Gpay MerchantId:" + accountCurrency.getMerchantId());
		} catch (SystemException exception) {
			exception.printStackTrace();
		}
		// Get key
		String merchantID = accountCurrency.getMerchantId();
		
		logger.info("Gpay merchantID:" + merchantID);
		
		fields.put(FieldType.MERCHANT_ID.getName(), merchantID);
		
		StringBuilder request = new StringBuilder();
		createGooglePayGetStatuts(fields, request);
		StringBuilder payLoad = new StringBuilder();
		payLoad.append(Constants.OPENING_BRACE);
		payLoad.append(request);
		payLoad.append(Constants.CLOSING_BRACE);
		logger.info("Gpay accessToken:" + payLoad.toString());

		return payLoad.toString();
	}

	private void createGooglePayGetStatuts(Fields fields, StringBuilder request) {
		appendJsonField(Constants.MERCHANT_INFO, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.GOOGLE_MERCHANT_ID, fields.get(FieldType.MERCHANT_ID.getName()), request);
		request.append(Constants.CLOSING_BRACE);
		request.append(",");
		
		appendJsonField(Constants.TRANSACTION_IDENTIIFIER, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.MERCHANT_TRANSACTION_ID, fields.get(FieldType.TXN_ID.getName()), request);
		request.append(Constants.CLOSING_BRACE);
		logger.info("Request:" + request.toString());
	}

}
