package com.kbn.gpay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionSearchService;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;

public class GpayStatusEnquirer {
	private static Logger logger = Logger.getLogger(GpayStatusEnquirer.class.getName());

	public void statusEnquirer() {
		TransactionSearchService transactionSearchService = new TransactionSearchService();
		try {
			List<TransactionSummaryReport> transactionList = transactionSearchService
					.getTransactionsForStatusUpdate(AcquirerType.GPAY.getCode());
			for (TransactionSummaryReport transaction : transactionList) {
				Map<String, String> reqMap = prepareFields(transaction);
				Fields fields = new Fields(reqMap);
				GPAYStatusTransactionProcessor processor = new GPAYStatusTransactionProcessor();
				processor.transact(fields);
				String response = fields.get(FieldType.STATUS.getName());
				if (!response.equals("Error")) {
					// update new order transaction
					fields.updateStatus();
					// Update sale txn
					fields.updateTransactionDetails();
				}

			}
		} catch (SystemException systemException) {
			logger.error("Error updating status for GPAY UPI ", systemException);
		}
	}

	private Map<String, String> prepareFields(TransactionSummaryReport transaction) {
		UserDao userDao = new UserDao();
		User user = userDao.findBySafexpayUser(transaction.getPayId());
		Account account = user.getAccountUsingAcquirerCode(AcquirerType.GPAY.getCode());
		String currencyCode = transaction.getCurrencyCode();
		AccountCurrency accountCurrency = null;
		try {
			accountCurrency = account.getAccountCurrency(currencyCode);
		} catch (SystemException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		// Get key
		String merchantKey = accountCurrency.getTxnKey();
		String merchantID = accountCurrency.getMerchantId();

		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put(FieldType.PAY_ID.getName(), transaction.getPayId());
		requestMap.put(FieldType.TXN_ID.getName(), transaction.getTransactionId());
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), transaction.getOrigTransactionId());
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.GPAY.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), transaction.getCurrencyCode());
		requestMap.put(FieldType.MERCHANT_ID.getName(), merchantID);
		requestMap.put(FieldType.TXN_KEY.getName(), merchantKey);
		return requestMap;

	}

	private Map<String, String> prepareFields(String txnID) throws SystemException {
		GPAYTransactionFetchService fetchTxn = new GPAYTransactionFetchService();
		Map<String, String> responseMap = fetchTxn.fetchTransaction(txnID);

		UserDao userDao = new UserDao();
		User user = userDao.findBySafexpayUser(responseMap.get("payID"));
		Account account = user.getAccountUsingAcquirerCode(AcquirerType.GPAY.getCode());
		String currencyCode = responseMap.get("currencyCode");
		AccountCurrency accountCurrency = null;
		try {
			accountCurrency = account.getAccountCurrency(currencyCode);
		} catch (SystemException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		// Get key
		String merchantKey = accountCurrency.getTxnKey();
		String merchantID = accountCurrency.getMerchantId();

		Map<String, String> requestMap = new HashMap<String, String>();

		requestMap.put(FieldType.PAY_ID.getName(), responseMap.get("payID"));
		requestMap.put(FieldType.TXN_ID.getName(), txnID);
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), responseMap.get("origTxnID"));
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.GPAY.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), responseMap.get("currencyCode"));
		requestMap.put(FieldType.MERCHANT_ID.getName(), merchantID);
		requestMap.put(FieldType.TXN_KEY.getName(), merchantKey);

		return requestMap;
	}

	public String manualStatusEnquirer(String orderId) {

		String response = null;

		try {

			Map<String, String> reqMap = prepareFields(orderId);
			Fields fields = new Fields(reqMap);

			GPAYStatusTransactionProcessor processor = new GPAYStatusTransactionProcessor();
			processor.transact(fields);

			response = fields.get(FieldType.STATUS.getName());
			logger.info("GPAY Status Response:" + response);
			if (!response.equals("Error")) {
				// update new order transaction
				fields.updateStatus();
				// Update sale txn
				fields.updateTransactionDetails();

			}

		} catch (SystemException systemException) {
			logger.error("Error updating manual status for GPAY UPI", systemException);
		}
		logger.info("GPAY UPI Status Response before return Statement:");
		return response;
	}

}
