package com.kbn.gpay;

import com.kbn.pg.core.Processor;

public class GPAYProcessorFactory {

	public static Processor getInstance() {

		return new GPAYProcessor();
	}

}
