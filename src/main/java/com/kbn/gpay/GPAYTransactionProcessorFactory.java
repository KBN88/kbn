package com.kbn.gpay;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AbstractTransactionProcessorFactory;
import com.kbn.pg.core.TransactionProcessor;

public class GPAYTransactionProcessorFactory implements AbstractTransactionProcessorFactory {
	private TransactionProcessor transactionProcessor;

	@Override
	public TransactionProcessor getInstance(Fields fields) throws SystemException {
		switch (TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))) {
		case REFUND:
			//transactionProcessor = new GPAYRefundTransactionProcessor();
			break;
		case SALE:
		case AUTHORISE:
		case ENROLL:
			transactionProcessor = new GPAYYSaleTransactionProcessor();
			break;
		case STATUS:
			transactionProcessor = new GPAYStatusTransactionProcessor();
			break;
		default:
			throw new SystemException(ErrorType.ACQUIRER_ERROR, "Unsupported transaction type for BillDesk");
		}
		return transactionProcessor;
	}
}