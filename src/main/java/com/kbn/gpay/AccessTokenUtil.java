package com.kbn.gpay;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import org.springframework.core.io.ClassPathResource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class AccessTokenUtil {
	public static void main(String[] args) {
		/*
		 * Code to generate accessToken
		 */
		String accessToken = createAccessToken();
		System.out.println(accessToken);
		/*
		 * After generation of accessToken, you can proceed with init request: In
		 * Header, add following details: Authorization: Bearer <access_token>
		 */
	}

	public static String createAccessToken(){
        FileInputStream serviceAccount = null;
        try {
            serviceAccount = new FileInputStream(new ClassPathResource("service-account-key.json").getFile());
            // Authenticate a Google credential with the service account
            GoogleCredential googleCred = GoogleCredential.fromStream(serviceAccount);

            // Add the required scopes to the Google credential
            GoogleCredential scoped = googleCred.createScoped(
                    Arrays.asList(
                            "https://www.googleapis.com/auth/nbupaymentsmerchants"
                    )
            );

            // Use the Google credential to generate an access token
            scoped.refreshToken();
            String token = scoped.getAccessToken();

            return token;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}