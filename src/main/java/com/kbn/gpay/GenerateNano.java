package com.kbn.gpay;

public class GenerateNano {
	
	public int nanois(String number) {
	  int temp=0, decimal_no,nano = 0;
			try {
			  
				//float number;
				//String floatValue = Float.toString(number);
				String decimal = number.substring(number.indexOf('.')).replace(".", "");//retrieving decimal value from the float value( for ex:55)
				System.out.println(decimal);
				decimal_no = 9-decimal.length();//As per 10^-9 subtracting length of the number from 9 (for ex:7(9-2))
				nano = Integer.parseInt(decimal);
				while(decimal_no != 0) { 
					temp=(nano*10);  // multiplying decimal value with 10 and storing into temp variable
					decimal_no--;    // decrementing the decimal_no by one
					nano=temp;       // swapping temp value into nano
				}
				System.out.println("Converted number in Nanos:"+temp);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			return nano;

}
}

