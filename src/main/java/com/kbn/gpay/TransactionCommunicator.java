package com.kbn.gpay;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class TransactionCommunicator {
	private static final String INIT_ENDPOINT_PROD = "https://nbupayments.googleapis.com/v1/merchantPayments:initiate";
	private static final String STATUS_INIT_ENDPOINT_PROD = "https://nbupayments.googleapis.com/v1/merchantTransactions:get";

	public String doPostRequestToGooglePayInitiate(String payload, String accessToken) throws IOException {
		OutputStream os = null;
		HttpURLConnection conn = null;
		try {
			URL url = new URL(INIT_ENDPOINT_PROD);
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Bearer " + accessToken);
			os = conn.getOutputStream();
			os.write(payload.getBytes());
			os.close();
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output = br.readLine();
			System.out.println("Output from Server .... \n" + output);
			conn.disconnect();
			return output;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			conn.disconnect();
			os.close();
		}
		return null;
	}


	
	public static String getTransactionDetails(String payload, String accessToken)  throws IOException{
		//transactionID = "5dd7834e0a897336939759a5";
		String accessToken1 =null;
		 try {
			  	String url = "https://nbupayments.googleapis.com/v1/merchantTransactions:get";
				URL obj = new URL(url);
				HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

				con.setRequestMethod("POST");
				con.setRequestProperty("Authorization", "Bearer "+accessToken);
				con.setRequestProperty("Content-Type", "application/json");
				con.setDoOutput(true);

				String str = payload;
				byte[] outputInBytes = str.getBytes("UTF-8");
				OutputStream os = con.getOutputStream();
				os.write( outputInBytes );    
				os.close();
				
				int responseCode = con.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("Post parameters : " + str);
				System.out.println("Response Code : " + responseCode);

				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				accessToken1 = response.toString();
				System.out.println(response.toString());
				//return true;
		
			 }catch(Exception e) {
				 System.out.println(e.getMessage());
				// return false;
			 }
		return accessToken1;
	}

	@SuppressWarnings("unused")
	private static boolean validateGooglePayResponse(String response) {
		/*
		 * Init call response will be empty JSON in success scenario. Make GET Api call
		 * to know the transaction status.
		 */
		if (response.equals("{}"))
			return true;
		else
			return false;
	}

}
