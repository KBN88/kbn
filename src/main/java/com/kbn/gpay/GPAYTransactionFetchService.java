package com.kbn.gpay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;

public class GPAYTransactionFetchService extends HibernateAbstractDao {

	private static Logger logger = Logger.getLogger(GPAYTransactionFetchService.class.getName());
	private static final String transactionQuery = "Select  CURRENCY_CODE, ORIG_TXN_ID, PAY_ID from TRANSACTION WHERE TXN_ID=? ";

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public Map<String, String> fetchTransaction(String txnId) throws SystemException {
		Map<String, String> requestMap = new HashMap<String, String>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(transactionQuery)) {
				preparedStatement.setString(1, txnId);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {

						requestMap.put("currencyCode", resultSet.getString("CURRENCY_CODE"));
						requestMap.put("origTxnID", resultSet.getString("ORIG_TXN_ID"));
						requestMap.put("payID", resultSet.getString("PAY_ID"));
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching MErchant Id for Manual status enquery " + sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		} finally {
			autoClose();
		}
		return requestMap;
	}// fetchTransaction}
}
