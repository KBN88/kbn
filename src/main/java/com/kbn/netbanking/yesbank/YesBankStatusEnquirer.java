package com.kbn.netbanking.yesbank;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionSearchService;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.netbanking.yesbank.StatusTransactionProcessor;
import com.kbn.netbanking.yesbank.YesBankStatusEnquirer;
import com.kbn.pg.core.AcquirerType;

public class YesBankStatusEnquirer {
	private static Logger logger = Logger.getLogger(YesBankStatusEnquirer.class
			.getName());

	public void statusEnquirer() {

		TransactionSearchService transactionSearchService = new TransactionSearchService();
		try {
			List<TransactionSummaryReport> transactionList = transactionSearchService
					.getTransactionsForStatusUpdate(AcquirerType.YESBANK
							.getCode());
			for (TransactionSummaryReport transaction : transactionList) {

				Fields fields = new Fields(prepareFields(transaction));

				StatusTransactionProcessor processor = new StatusTransactionProcessor();
				processor.transact(fields);

			}
		} catch (SystemException systemException) {
			logger.error("Error updating status for yes bank", systemException);
		}
	}

	public Map<String, String> prepareFields(
			TransactionSummaryReport transaction) {
		Map<String, String> requestMap = new HashMap<String, String>();

		requestMap.put(FieldType.PAY_ID.getName(), transaction.getPayId());
		requestMap.put(FieldType.TXN_ID.getName(),
				transaction.getTransactionId());
		requestMap.put(FieldType.TXNTYPE.getName(),
				TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(),
				AcquirerType.YESBANK.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(),
				TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(),
				transaction.getCurrencyCode());

		return requestMap;
	}

}
