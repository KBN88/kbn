package com.kbn.netbanking.yesbank;

public class Constants {
	public static final String EQUATOR = "=";
	public static final String SEPARATOR = "&";
	public static final String QUESTION_MARK = "?";
	public static final String CONFIGSEPARATOR = "$";
	
	public static final String MERCHANT_ID = "fldMerchCode ";	
	public static final String CLIENT_CODE = "fldClientCode";
	public static final String CURRENCY = "fldTxnCurr";
	public static final String AMOUNT = "fldTxnAmt";
	public static final String SERVICE_CHARGE = "fldTxnScAmt";
	public static final String TXN_ID = "fldMerchRefNbr";
	public static final String PG_DATE_TIME = "fldDatTimeTxn";
	public static final String RRN = "BankRefNo";
	public static final String CLIENT_ACC_NO = "fldClientAcctNo";
	public static final String RETURN_URL = "RU"; 
	public static final String VERIFICAION_FLAG = "flgVerify";
	public static final String STATUS = "flgSuccess";
	public static final String RESPONSE_MESSAGE = "Message";
	
}
