package com.kbn.netbanking.yesbank;

import com.kbn.netbanking.yesbank.YesBankProcessor;
import com.kbn.pg.core.Processor;

public class YesBankProcessorFactory {
	public static Processor getInstance(){
		return new YesBankProcessor();
	}

}
