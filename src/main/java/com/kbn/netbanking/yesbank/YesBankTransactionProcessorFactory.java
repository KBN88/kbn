package com.kbn.netbanking.yesbank;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.netbanking.yesbank.RefundTransactionProcessor;
import com.kbn.netbanking.yesbank.SaleTransactionProcessor;
import com.kbn.netbanking.yesbank.StatusTransactionProcessor;
import com.kbn.pg.core.AbstractTransactionProcessorFactory;
import com.kbn.pg.core.TransactionProcessor;

public class YesBankTransactionProcessorFactory implements
		AbstractTransactionProcessorFactory {

	private TransactionProcessor transactionProcessor;

	public TransactionProcessor getInstance(Fields fields)
			throws SystemException {

		switch (TransactionType.getInstance(fields.get(FieldType.TXNTYPE
				.getName()))) {
		case REFUND:
			transactionProcessor = new RefundTransactionProcessor();
			break;
		case SALE:
		case AUTHORISE:
			transactionProcessor = new SaleTransactionProcessor();
			break;
		case STATUS:
			transactionProcessor = new StatusTransactionProcessor();
			break;
		default:
			throw new SystemException(ErrorType.ACQUIRER_ERROR,
					"Unsupported transaction type for yes bank");
		}
		return transactionProcessor;
	}
}
