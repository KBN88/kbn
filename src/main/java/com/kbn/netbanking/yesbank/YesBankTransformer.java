package com.kbn.netbanking.yesbank;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class YesBankTransformer {

	private Response response;
	private static Logger logger = Logger
			.getLogger(TransactionCommunicator.class.getName());

	public YesBankTransformer() {
		this.response = response;
	}

	public void updateResponse(Fields fields) {

		fields.put(FieldType.STATUS.getName(), StatusType.CAPTURED.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), "SUCCESS");
		fields.put(FieldType.RESPONSE_CODE.getName(), "SUCCESS");
	}

	public ErrorType getResponseMessage(String BankRefNo) {
		ErrorType errorType = null;

		if (null == BankRefNo) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (BankRefNo.equals("0")) {
			errorType = ErrorType.REJECTED;
		} else {
			errorType = ErrorType.SUCCESS;
		}

		return errorType;
	}

	public StatusType getStatus(String BankRefNo) {
		StatusType status = null;

		if (null == BankRefNo) {
			status = StatusType.ERROR;
		} else if (BankRefNo.equals("0")) {
			status = StatusType.REJECTED;
		} else {
			status = StatusType.CAPTURED;
		}

		return status;
	}

	public ErrorType getResponseMessageForStatusEnquiry(String flgSuccess) {
		ErrorType errorType = null;

		if (null == flgSuccess) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (flgSuccess.equals("S")) {
			errorType = ErrorType.SUCCESS;
		} else if (flgSuccess.equals("F")) {
			errorType = ErrorType.REJECTED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getStatusForStatusEnquiry(String flgSuccess) {
		StatusType status = null;

		if (null == flgSuccess) {
			status = StatusType.ERROR;
		} else if (flgSuccess.equals("S")) {
			status = StatusType.APPROVED;
		} else if (flgSuccess.equals("F")) {
			status = StatusType.REJECTED;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public static Map<String, String> parse(Map<String, String> responseMap) {

		YesBankTransformer transformer = new YesBankTransformer();

		StatusType status = transformer.getStatusForStatusEnquiry(responseMap
				.get("flgSuccess"));
		ErrorType errorType = transformer
				.getResponseMessageForStatusEnquiry(responseMap
						.get("flgSuccess"));

		logger.info("Response from yes bank: " + responseMap);

		Map<String, String> mapValues = new HashMap<String, String>();
		mapValues.put(FieldType.RESPONSE_CODE.getName(),
				errorType.getResponseCode());
		mapValues.put(FieldType.RESPONSE_MESSAGE.getName(),
				errorType.getResponseMessage());
		mapValues.put(FieldType.STATUS.getName(), status.getName());
		mapValues.put(FieldType.ACQ_ID.getName(), responseMap.get("BankRefNo"));
		// mapValues.put(FieldType.AMOUNT.getName(),responseMap.get("fldTxnAmt"));
		mapValues.put(FieldType.PG_DATE_TIME.getName(),
				responseMap.get("fldDatTimeTxn"));

		return mapValues;

	}
}
