package com.kbn.netbanking.yesbank;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.SystemConstants;
import com.kbn.pg.core.Amount;

public class TransactionConverter {
	private static Logger logger = Logger
			.getLogger(TransactionConverter.class.getName());

	public String prepareFieldsForRefund(Fields fields) throws SystemException {

		StringBuilder refundString = new StringBuilder();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String currentDate = sdf.format(date);

		refundString.append(fields.get(FieldType.MERCHANT_ID.getName()));
		refundString.append(fields.get(FieldType.PG_DATE_TIME.getName()));
		refundString.append(currentDate);

		refundString.append(fields.get(FieldType.ACQ_ID.getName()));
		refundString.append(fields.get(FieldType.TXN_ID.getName()));
		refundString.append(Amount.toDecimal(
				fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName())));

		return refundString.toString();
	}

	public String createSaleTransaction(Fields fields) throws SystemException {

		String amount = Amount.toDecimal(
				fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));

		// Fetching Details from Property file
		PropertiesManager propertiesManager = new PropertiesManager();
		String yesbankTransactionUrl = propertiesManager
				.getNetbankingProperty("YesBankRequestUrl");
		String yesbankMID = propertiesManager
				.getNetbankingProperty("YesBankMID");
		String yesbankKey = fields.get(FieldType.PAY_ID.getName());
		String taxAmount = propertiesManager
				.getNetbankingProperty("YesBankServiceTaxAmount");
		String currency = propertiesManager
				.getNetbankingProperty("YesBankCurrency");
		String mdValue = propertiesManager.getNetbankingProperty("YesBankMD");
		String txnID = fields.get(FieldType.TXN_ID.getName());
		String returnUrl = propertiesManager
				.getNetbankingProperty("YesBankReturnUrl");

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String currentDate = sdf.format(date);
		fields.put(FieldType.PG_DATE_TIME.getName(), currentDate);

		StringBuilder url = new StringBuilder();
		url.append(yesbankTransactionUrl);
		url.append(Constants.QUESTION_MARK);
		try {
			PrintWriter out;

			out = ServletActionContext.getResponse().getWriter();

			url.append("fldClientCode");
			url.append(Constants.EQUATOR);
			url.append(URLEncoder.encode(yesbankKey, SystemConstants.DEFAULT_ENCODING_UTF_8));
			url.append(Constants.SEPARATOR);
			url.append("fldMerchCode");
			url.append(Constants.EQUATOR);
			url.append(URLEncoder.encode(yesbankMID, SystemConstants.DEFAULT_ENCODING_UTF_8));
			url.append(Constants.SEPARATOR);
			url.append("fldTxnCurr");
			url.append(Constants.EQUATOR);
			url.append(URLEncoder.encode(currency, SystemConstants.DEFAULT_ENCODING_UTF_8));
			url.append(Constants.SEPARATOR);
			url.append("fldTxnAmt");
			url.append(Constants.EQUATOR);
			url.append(URLEncoder.encode(amount, SystemConstants.DEFAULT_ENCODING_UTF_8));
			url.append(Constants.SEPARATOR);
			url.append("fldTxnScAmt");
			url.append(Constants.EQUATOR);
			url.append(URLEncoder.encode(taxAmount, SystemConstants.DEFAULT_ENCODING_UTF_8));
			url.append(Constants.SEPARATOR);
			url.append("fldMerchRefNbr");
			url.append(Constants.EQUATOR);
			url.append(URLEncoder.encode(txnID, SystemConstants.DEFAULT_ENCODING_UTF_8));
			url.append(Constants.SEPARATOR);
			url.append("fldDatTimeTxn");
			url.append(Constants.EQUATOR);
			url.append(URLEncoder.encode(currentDate, SystemConstants.DEFAULT_ENCODING_UTF_8));
			url.append(Constants.SEPARATOR);
			url.append("MD");
			url.append(Constants.EQUATOR);
			url.append(mdValue);
			url.append(Constants.SEPARATOR);
			url.append("RU");
			url.append(Constants.EQUATOR);
			url.append(URLEncoder.encode(returnUrl, SystemConstants.DEFAULT_ENCODING_UTF_8));

			StringBuilder httpRequest = new StringBuilder();
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent() { ");
			httpRequest.append("window.location.assign('");
			httpRequest.append(url);
			httpRequest.append("') }");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");
			logger.info("final request sent " + httpRequest);
			out.write(httpRequest.toString());

		} catch (IOException ioException) {
			logger.error(ioException);
		}
		return url.toString();

	}

}
