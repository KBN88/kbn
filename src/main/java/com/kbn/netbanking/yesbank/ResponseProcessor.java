package com.kbn.netbanking.yesbank;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;

public class ResponseProcessor implements Processor {
	private Response response;

	public void preProcess(Fields fields) throws SystemException {

	}

	public void process(Fields fields) throws SystemException {

		fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(),
				fields.get(FieldType.TXNTYPE.getName()));
		if (null != fields.get(FieldType.TXN_ID.getName())) {
			fields.updateNewOrderDetails();
			fields.updateTransactionDetails();
		}
	}

	public void postProcess(Fields fields) throws SystemException {
		fields.removeInternalFields();
		new ResponseCreator().create(fields);
	}

	public void updateInternalFields(Fields fields) {
		String pgResponseCode = fields.get(FieldType.STATUS.getName());

		if (pgResponseCode.equals("Y")) {
			fields.put(FieldType.STATUS.getName(),
					StatusType.CAPTURED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.SUCCESS.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.SUCCESS.getResponseMessage());
		} else {
			fields.put(FieldType.STATUS.getName(),
					StatusType.DECLINED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.DECLINED.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.DECLINED.getResponseMessage());
		}
	}

}
