package com.kbn.netbanking.yesbank;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Currency;

public class TransactionCommunicator {
	private static Logger logger = Logger
			.getLogger(TransactionCommunicator.class.getName());

	public String sendAuthorization(String request, Fields fields)
			throws SystemException {
		PrintWriter out;
		String response = "";
		logger.info("Request sent to yesbank: " + request);
		try {
			out = ServletActionContext.getResponse().getWriter();

			// Return response
			response = ErrorType.SUCCESS.getCode();
			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.SUCCESS.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.SUCCESS.getResponseMessage());

		} catch (IOException iOException) {
			logger.error(iOException);
			response = ErrorType.UNKNOWN.getCode();
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					iOException, "Network Exception with yesbank");
		}
		return response;
	}

	public String transactStatus(Fields fields, String hostUrl)
			throws SystemException {
		PostMethod postMethod = new PostMethod(hostUrl);
		PropertiesManager propertiesManager = new PropertiesManager();
		String amount = Amount.toDecimal(
				fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));
		String merchantID = fields.get(FieldType.MERCHANT_ID.getName());
		String payID = fields.get(FieldType.PAY_ID.getName());
		String origTxnID = fields.get(FieldType.ORIG_TXN_ID.getName());
		String pgDateTime = fields.get(FieldType.PG_DATE_TIME.getName());
		String currency = Currency.getAlphabaticCode(fields
				.get(FieldType.CURRENCY_CODE.getName()));
		String taxAmount = propertiesManager
				.getNetbankingProperty("YesBankServiceTaxAmount");
		String returnUrl = propertiesManager
				.getNetbankingProperty("YesBankReturnUrl");
		String verificationFlag = propertiesManager
				.getNetbankingProperty("YesBankVerificationFlag");

		postMethod.getParameter(Constants.QUESTION_MARK);
		postMethod.addParameter("fldMerchCode", merchantID);
		postMethod.addParameter("fldClientCode", payID);
		postMethod.addParameter("fldTxnCurr", currency);
		postMethod.addParameter("fldTxnAmt", amount);
		postMethod.addParameter("fldTxnScAmt", taxAmount);
		postMethod.addParameter("fldMerchRefNbr", origTxnID);
		postMethod.addParameter("fldDatTimeTxn", pgDateTime);
		postMethod.addParameter("RU", returnUrl);
		postMethod.addParameter("flgVerify", verificationFlag);

		return transact(postMethod, hostUrl);

	}

	public String transact(HttpMethod httpMethod, String hostUrl)
			throws SystemException {
		String response = "";

		try {
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(httpMethod);

			if (httpMethod.getStatusCode() == HttpStatus.SC_OK) {
				response = httpMethod.getResponseBodyAsString();
				logger.info("Response from yes bank: " + response);
			} else {
				throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
						"Network Exception with yes bank " + hostUrl.toString()
								+ "recieved response code"
								+ httpMethod.getStatusCode());
			}
		} catch (IOException ioException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					ioException, "Network Exception with yes bank "
							+ hostUrl.toString());
		}
		return response;

	}

}
