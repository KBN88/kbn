package com.kbn.netbanking.yesbank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.DateCreater;
import com.kbn.commons.util.FieldType;

public class RefundFieldsCreator {

	private static final String dd = "dd/MM/yyyy";
	private static final String td = "dd/MM/yyyy HH:mm:ss";

	private static Logger logger = Logger.getLogger(RefundFieldsCreator.class
			.getName());

	private static final String getTotalRefundTxnQuery = "Select Amount as REFUND_AMOUNT,(Select min(Amount) from TRANSACTION where TXN_ID = t.ORIG_TXN_ID) as 'TXN_AMOUNT',(Select CREATE_DATE from TRANSACTION where TXN_ID = t.ORIG_TXN_ID) as 'PG_DATE_TIME',CREATE_DATE,PAY_ID,ORIG_TXN_ID,ACQ_ID,@rownum := @rownum + 1 AS rowNumber from TRANSACTION t ,(SELECT @rownum := 0) r where TXNTYPE='REFUND' and STATUS='Sent to Bank' and MOP_TYPE= '1001' and (CREATE_DATE >= ? AND CREATE_DATE< ?)";

	public RefundFieldsCreator() {
	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public List<TransactionSummaryReport> getTransactionRefundReport(
			String dateFrom, String dateTo) throws SystemException {
		
		List<TransactionSummaryReport> transactionRefundList = new ArrayList<TransactionSummaryReport>();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement(getTotalRefundTxnQuery)) {
				;

				dateTo = DateCreater.formatToDate(dateTo);
				dateFrom = DateCreater.formatFromDate(dateFrom);
				prepStmt.setString(1, dateFrom);
				prepStmt.setString(2, dateTo);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport refundReport = new TransactionSummaryReport();
						String dateTime = rs.getString(FieldType.CREATE_DATE
								.toString());
						String txnDate = rs.getString(FieldType.PG_DATE_TIME
								.toString());
						SimpleDateFormat dbfomat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss.S");
						SimpleDateFormat bankformat = new SimpleDateFormat(
								"dd/MM/yyyy HH:mm:ss");

						Date date1 = null;
						Date tDate = null;

						try {
							date1 = dbfomat.parse(dateTime);
							tDate = dbfomat.parse(txnDate);
						} catch (Exception exception) {
							logger.error("Exception", exception);
						}
						String refunddate = bankformat.format(date1);
						String txdate = bankformat.format(tDate);
						
						Date reftxndate = null;
						Date saletxndate = null;
						
						try {
							reftxndate = new SimpleDateFormat(td).parse(refunddate);
							saletxndate = new SimpleDateFormat(td).parse(txdate);
						} catch (Exception exception) {
							logger.error("Exception", exception);
						}

						SimpleDateFormat dayFormat = new SimpleDateFormat(dd);
						String RefundTransactionDate = (dayFormat.format(reftxndate));
						String SaleTransactionDate = (dayFormat.format(saletxndate));

						refundReport.setRowNumber(rs
								.getString(CrmFieldConstants.ROW_NUMBER
										.getValue()));
						refundReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						refundReport.setTxnDate(SaleTransactionDate);
						refundReport.setRefundDate(RefundTransactionDate);
						refundReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						refundReport.setRefundedAmount(rs
								.getString(CrmFieldConstants.REFUND_AMOUNT
										.toString()));
						refundReport.setNetAmount(rs
								.getString(CrmFieldConstants.TXN_AMOUNT
										.toString()));
						refundReport.setAcqId(rs.getString(FieldType.ACQ_ID
								.toString()));

						transactionRefundList.add(refundReport);

					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error");
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionRefundList;

	}
}
