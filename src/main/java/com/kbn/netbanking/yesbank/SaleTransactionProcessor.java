package com.kbn.netbanking.yesbank;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.netbanking.yesbank.TransactionCommunicator;
import com.kbn.netbanking.yesbank.TransactionConverter;
import com.kbn.pg.core.Transaction;
import com.kbn.pg.core.TransactionProcessor;

public class SaleTransactionProcessor implements Transaction,
		TransactionProcessor {

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();

		String request = converter.createSaleTransaction(fields);
		communicator.sendAuthorization(request, fields);
		fields.put(FieldType.STATUS.getName(),
				StatusType.SENT_TO_BANK.getName());

	}

}
