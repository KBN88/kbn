package com.kbn.netbanking.yesbank;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.pg.core.Transaction;
import com.kbn.pg.core.TransactionProcessor;

public class StatusTransactionProcessor implements Transaction, TransactionProcessor {

	@Override
	public void transact(Fields fields) throws SystemException {

		PropertiesManager propertiesManager = new PropertiesManager();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String statusEnquiryUrl = propertiesManager.getNetbankingProperty("YesBankStatusEnquiryUrl");
		
		String response = communicator.transactStatus(fields, statusEnquiryUrl);
//		Response responseObject = converter.toTransaction(response);

		YesBankTransformer transformer = new YesBankTransformer();
//		transformer.parse(fields);
		
	}
}
