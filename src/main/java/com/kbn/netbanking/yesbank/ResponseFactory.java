package com.kbn.netbanking.yesbank;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.StatusType;

public class ResponseFactory {

	private static Logger logger = Logger.getLogger(ResponseFactory.class
			.getName());

	public static Map<String, String> parse(Map<String, String> responseMap) {

		YesBankTransformer transformer = new YesBankTransformer();

		StatusType status = transformer.getStatus(responseMap.get("BankRefNo"));
		ErrorType errorType = transformer.getResponseMessage(responseMap
				.get("BankRefNo"));

		logger.info("Response from yes bank: " + responseMap);

		Map<String, String> mapValues = new HashMap<String, String>();
		mapValues.put(FieldType.RESPONSE_CODE.getName(),
				errorType.getResponseCode());
		mapValues.put(FieldType.RESPONSE_MESSAGE.getName(),
				errorType.getResponseMessage());
		mapValues.put(FieldType.STATUS.getName(), status.getName());
		mapValues.put(FieldType.ACQ_ID.getName(), responseMap.get("BankRefNo"));

		mapValues.put(FieldType.PG_DATE_TIME.getName(),
				responseMap.get("fldDatTimeTxn"));

		return mapValues;

	}

}
