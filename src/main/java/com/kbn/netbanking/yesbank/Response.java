package com.kbn.netbanking.yesbank;

public class Response {
	private String merchantCode;
	private String clientCode;
	private String currency;
	private String amount;
	private String serviceChargeAmount;
	private String txnId;
	private String pgDateTime;
	private String rrn;
	private String clientAccountNo;
	private String verification;
	private String status;
	private String responseMessage;
	
	
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getServiceChargeAmount() {
		return serviceChargeAmount;
	}
	public void setServiceChargeAmount(String serviceChargeAmount) {
		this.serviceChargeAmount = serviceChargeAmount;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getPgDateTime() {
		return pgDateTime;
	}
	public void setPgDateTime(String pgDateTime) {
		this.pgDateTime = pgDateTime;
	}
	public String getRrn() {
		return rrn;
	}
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	public String getClientAccountNo() {
		return clientAccountNo;
	}
	public void setClientAccountNo(String clientAccountNo) {
		this.clientAccountNo = clientAccountNo;
	}
	public String getVerification() {
		return verification;
	}
	public void setVerification(String verification) {
		this.verification = verification;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getRespCode() {
		// TODO Auto-generated method stub
		return null;
	}

}
