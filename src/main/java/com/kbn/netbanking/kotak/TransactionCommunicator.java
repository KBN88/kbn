package com.kbn.netbanking.kotak;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.SystemConstants;
import com.kbn.netbanking.kotak.Constants;
import com.kbn.netbanking.kotak.TransactionCommunicator;

public class TransactionCommunicator {
	private static Logger logger = Logger
			.getLogger(TransactionCommunicator.class.getName());

	public String sendAuthorization(String request, Fields fields)
			throws SystemException {
		PrintWriter out;
		String response = "";
		logger.info("Request sent to kotak: " + request);
		try {
			out = ServletActionContext.getResponse().getWriter();

			out.write(request);
			// Return response
			response = ErrorType.SUCCESS.getCode();
			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.SUCCESS.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.SUCCESS.getResponseMessage());

		} catch (IOException iOException) {
			logger.error(iOException);
			response = ErrorType.UNKNOWN.getCode();
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					iOException, "Network Exception with Kotak");
		}
		return response;
	}

	public String transactStatus(Fields fields, String hostUrl)
			throws SystemException {
		StringBuilder getUrl = new StringBuilder(hostUrl);
		/*
		 * PropertiesManager propertiesManager = new PropertiesManager(); String
		 * msgcode = propertiesManager
		 * .getNetbankingProperty("StatusEnquiryCode");
		 */

		TransactionConverter converter = new TransactionConverter();
		String request = "";
		try {
			request = URLEncoder.encode(converter
					.prepareFieldsForStatus(fields).toString(), SystemConstants.DEFAULT_ENCODING_UTF_8);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		getUrl.append(Constants.QUESTION_MARK);
		getUrl.append("msg");
		getUrl.append(Constants.EQUATOR);
		getUrl.append(request);

		GetMethod getMethod = new GetMethod(getUrl.toString());

		logger.info("Request url to kotak: " + getUrl.toString());

		return transact(getMethod, hostUrl);
	}

	// HttpMethodBase
	public String transact(HttpMethod httpMethod, String hostUrl)
			throws SystemException {
		String response = "";

		try {
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(httpMethod);

			if (httpMethod.getStatusCode() == HttpStatus.SC_OK) {
				response = httpMethod.getResponseBodyAsString();
				logger.info("Response from kotak: " + response);
			} else {
				throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
						"Network Exception with kotak " + hostUrl.toString()
								+ "recieved response code"
								+ httpMethod.getStatusCode());
			}
		} catch (IOException ioException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					ioException, "Network Exception with kotak "
							+ hostUrl.toString());
		}
		return response;

	}

}
