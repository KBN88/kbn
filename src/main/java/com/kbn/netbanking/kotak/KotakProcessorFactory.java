package com.kbn.netbanking.kotak;

import com.kbn.netbanking.kotak.KotakProcessor;
import com.kbn.pg.core.Processor;

public class KotakProcessorFactory {
	public static Processor getInstance(){
		return new KotakProcessor();
	}

}
