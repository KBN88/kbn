package com.kbn.netbanking.kotak;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.Constants;

public class BatchFileCreator {

	private static final String dd= "ddMMYYYY000001";
	private static final String td= "ddMMyyyyHHmmss";
	private static final String rd= "dd-MMM-yyyy";
//	private static final String  = "000001";
	
	public String createFile(
			List<TransactionSummaryReport> transactionRefundList)
			throws ParseException {
		Integer lineNumber = 1;
		float totalAmount = (float) 0.00;
		String formattedAmount = "";
		String finaltext = "";

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(dd );
		String fileName = sdf.format(date) + ".txt";

		List<TransactionSummaryReport> responseArray = transactionRefundList;

		StringBuilder rs = new StringBuilder();
		for (TransactionSummaryReport tsr : responseArray) {
			String dateTime = tsr.getTxnDate();
			Date txndate = new SimpleDateFormat(td)
					.parse(dateTime);
			SimpleDateFormat dayFormat = new SimpleDateFormat(rd);
			String TxnDate = (dayFormat.format(txndate));

			rs.append(lineNumber);
			rs.append(Constants.KOTAK_SEPARATOR.getValue());
			rs.append(tsr.getPayId());
			rs.append(Constants.KOTAK_SEPARATOR.getValue());
			rs.append(TxnDate);
			rs.append(Constants.KOTAK_SEPARATOR.getValue());
			rs.append(tsr.getOrigTransactionId());
			rs.append(Constants.KOTAK_SEPARATOR.getValue());
			rs.append(tsr.getRefundedAmount());
			rs.append(Constants.KOTAK_SEPARATOR.getValue());
			rs.append(tsr.getAcqId());
			rs.append("\r\n");
			lineNumber++;

			float amount = Float.parseFloat(tsr.getRefundedAmount());
			totalAmount = totalAmount + amount;
			formattedAmount = String.format("%.2f", totalAmount);
		}
		// to remove last \r\n
		String fileText = rs.substring(0, rs.lastIndexOf("\r\n"));

		StringBuilder line = new StringBuilder();
		line.append(fileName);
		line.append(Constants.KOTAK_SEPARATOR.getValue());
		line.append(lineNumber - 1);
		line.append(Constants.KOTAK_SEPARATOR.getValue());
		line.append(formattedAmount);
		line.append(Constants.KOTAK_SEPARATOR.getValue());
		line.append("CHECKSUM");
		line.append("\r\n");

		String firstLine = line.toString();
		String content = fileText.toString();

		StringBuilder text = new StringBuilder();
		text.append(firstLine);
		text.append(content);
		finaltext = text.toString();

		return finaltext;

	}

}