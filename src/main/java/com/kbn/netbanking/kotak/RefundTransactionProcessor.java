package com.kbn.netbanking.kotak;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.TransactionProcessor;

public class RefundTransactionProcessor implements TransactionProcessor {

	public void transact(Fields fields) throws SystemException {
		
		fields.put(FieldType.STATUS.getName(),StatusType.SENT_TO_BANK.getName());
		fields.put(FieldType.RESPONSE_CODE.getName(),ErrorType.SUCCESS.getCode());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.SUCCESS.getResponseMessage());
	
	}

}
