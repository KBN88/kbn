package com.kbn.netbanking.kotak;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.TransactionReportService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.DateCreater;
import com.kbn.commons.util.FieldType;

public class RefundFieldsCreator {

	private static Logger logger = Logger
			.getLogger(TransactionReportService.class.getName());

	private static final String getTotalRefundTxnQuery = "Select Amount as REFUND_AMOUNT,PG_DATE_TIME,PAY_ID,ORIG_TXN_ID,ACQ_ID where TXNTYPE='REFUND' and STATUS='Pending' and MOP_TYPE= '1012' and (CREATE_DATE >= ? AND CREATE_DATE< ?)";

	public RefundFieldsCreator() {
	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public List<TransactionSummaryReport> getTransactionRefundReport(String dateFrom,String dateTo)
			throws SystemException {

		List<TransactionSummaryReport> transactionRefundList = new ArrayList<TransactionSummaryReport>();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement(getTotalRefundTxnQuery)) {
				
				
				dateTo = DateCreater.formatToDate(dateTo);
				dateFrom = DateCreater.formatFromDate(dateFrom);
				prepStmt.setString(1, dateFrom);
				prepStmt.setString(2, dateTo);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport refundReport = new TransactionSummaryReport();

						refundReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						refundReport.setTxnDate(rs
								.getString(FieldType.PG_DATE_TIME.toString()));
						refundReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						refundReport.setRefundedAmount(rs
								.getString(CrmFieldConstants.REFUND_AMOUNT
										.toString()));
						refundReport.setAcqId(rs.getString(FieldType.ACQ_ID
								.toString()));
						transactionRefundList.add(refundReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error");
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionRefundList;
	}
}
