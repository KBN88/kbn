package com.kbn.netbanking.kotak;

public class Constants {
	public static final String EQUATOR = "=";
	public static final String SEPARATOR = "&";
	public static final String QUESTION_MARK = "?";
	public static final String CONFIGSEPARATOR = "$";
	
	public static final String MESSAGE_CODE = "Message Code";
	public static final String PG_DATE_TIME = "Date and Time";
	public static final String MERCHANT_ID = "Merchant Id";
	public static final String TXN_ID = "Trace number";
	public static final String AMOUNT = "Amount";
	public static final String CUST_EMAIL = "Transaction Description";
	public static final String HASH = "Checksum";
	public static final String STATUS = "Authorisation Status";
	public static final String RRN = "Bank Reference Number";
	public static final String PG_TXN_STATUS = "Auth Status";
	public static final String PARAMETER_NAME = "msg";

}
