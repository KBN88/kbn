package com.kbn.netbanking.kotak;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.StatusType;
import com.kbn.netbanking.kotak.KotakTransformer;
import com.kbn.netbanking.kotak.TransactionCommunicator;
import com.kbn.pg.core.TransactionProcessor;
public class StatusTransactionProcessor implements TransactionProcessor {
	public static final String STATUS_MSG_OPEN_TAG =   "<form method=post>";
	public static final String STATUS_MSG_CLOSE_TAG =  "</form>";
	// @Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		
		PropertiesManager propertiesManager = new PropertiesManager();
		String kotakTransactionUrl = propertiesManager
				.getNetbankingProperty("KotakStatusEnquiryUrl");

		TransactionCommunicator communicator = new TransactionCommunicator();
		String response = communicator.transactStatus(fields, kotakTransactionUrl);
	
		String responseTrimmed = converter.getTextBetweenTags(response, STATUS_MSG_OPEN_TAG, STATUS_MSG_CLOSE_TAG);

		KotakTransformer transformer = new KotakTransformer(responseTrimmed);
		transformer.updateKotakStatusResponse(fields);
		fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));	
		
		fields.updateStatus();
		fields.put(FieldType.STATUS.getName(), StatusType.APPROVED.getName());
		
	}

}
