package com.kbn.netbanking.kotak;

import java.util.zip.CRC32;
import java.util.zip.Checksum;

import com.kbn.commons.exception.SystemException;

public class CalculateChecksum {
	public static String main(StringBuilder msg2) throws SystemException {

		StringBuilder input = msg2;
		// get bytes from string

		byte bytes[] = input.toString().getBytes();

		Checksum checksum = new CRC32();

		// update the current checksum with the specified array of bytes

		checksum.update(bytes, 0, bytes.length);

		// get the current checksum value

		long checksumValue = checksum.getValue();

		System.out.println("CRC32 checksum for input string is: "
				+ checksumValue);
		return String.valueOf(checksumValue);

	}

}
