package com.kbn.netbanking.kotak;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

public class ResponseFactory {
	private static Logger logger = Logger.getLogger(ResponseFactory.class
			.getName());
	private String[] kotakResponse;

	public ResponseFactory(String responseString) {
		this.kotakResponse = responseString.split("\\|");
	}

	public static Response createResponse(String responseString)
			throws SystemException {

		Response response = new Response();

		response.setStatus(responseString);
		return response;
	}

	public void updateKotakResponse(Fields fields) {

		fields.put(FieldType.PG_DATE_TIME.getName(), kotakResponse[1]);
		fields.put(FieldType.MERCHANT_ID.getName(), kotakResponse[2]);
		fields.put(FieldType.TXN_ID.getName(), kotakResponse[3]);
		fields.put(FieldType.RRN.getName(), kotakResponse[6]);
	}

}
