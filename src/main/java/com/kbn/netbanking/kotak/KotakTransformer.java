package com.kbn.netbanking.kotak;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.Amount;

public class KotakTransformer {

	private String[] kotakResponse;
	private String responseString;
	PropertiesManager propertiesManager = new PropertiesManager();
	String kotakkey = propertiesManager.getNetbankingProperty("KotakKey");

	public KotakTransformer(String responseString) {
		this.responseString = responseString;
		this.kotakResponse = responseString.split("\\|");
	}

	public void updateResponse(Fields fields) {

		fields.put(FieldType.STATUS.getName(), StatusType.CAPTURED.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), "SUCCESS");
		fields.put(FieldType.RESPONSE_CODE.getName(), "SUCCESS");
	}

	public ErrorType getResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("Y")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("N")) {
			errorType = ErrorType.REJECTED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Y")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("N")) {
			status = StatusType.REJECTED;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public void updateKotakResponse(Fields fields) throws SystemException {
		KotakUtil kotakutil = new KotakUtil();

		if (!kotakutil.compareResponseHash(fields, responseString)) {
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.SIGNATURE_MISMATCH.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.FAILED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.SIGNATURE_MISMATCH.getResponseCode());
			return;
		}

		StatusType status = getStatus(kotakResponse[5]);
		ErrorType errorType = getResponse(kotakResponse[5]);

		fields.put(FieldType.PG_DATE_TIME.getName(), kotakResponse[1]);
		fields.put(FieldType.MERCHANT_ID.getName(), kotakResponse[2]);
		fields.put(FieldType.TXN_ID.getName(), kotakResponse[3]);
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.ACQ_ID.getName(), kotakResponse[6]);
		fields.put(FieldType.RESPONSE_MESSAGE.getName(),
				errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(),
				errorType.getResponseCode());
		
	}

	public void updateKotakStatusResponse(Fields fields) throws SystemException {
		KotakUtil kotakutil = new KotakUtil();

		if (!kotakutil.compareStatusResponseHash(fields, responseString)) {
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.SIGNATURE_MISMATCH.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.FAILED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.SIGNATURE_MISMATCH.getResponseCode());
			return;
		}

		StatusType status = getStatus(kotakResponse[5]);
		ErrorType errorType = getResponse(kotakResponse[5]);

		fields.put(FieldType.PG_DATE_TIME.getName(), kotakResponse[1]);
		fields.put(FieldType.MERCHANT_ID.getName(), kotakResponse[2]);
		fields.put(FieldType.ORIG_TXN_ID.getName(), kotakResponse[3]);
		fields.put(
				FieldType.AMOUNT.getName(),
				Amount.formatAmount(kotakResponse[4],
						fields.get(FieldType.CURRENCY_CODE.getName())));
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.ACQ_ID.getName(), kotakResponse[6]);
		fields.put(FieldType.RESPONSE_MESSAGE.getName(),
				errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(),
				errorType.getResponseCode());

	}

}
