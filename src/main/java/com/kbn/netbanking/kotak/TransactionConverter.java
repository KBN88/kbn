package com.kbn.netbanking.kotak;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.pg.core.Amount;

public class TransactionConverter {

	private static Logger logger = Logger
			.getLogger(TransactionCommunicator.class.getName());
	String requestparameter;

	/*public String prepareFieldsForRefund(Fields fields) throws SystemException {

		StringBuilder refundString = new StringBuilder();
		
		refundString.append(Constants.KOTAK_SEPARATOR.getValue());
		refundString.append(fields.get(FieldType.MERCHANT_ID.getName()));
		refundString.append(Constants.KOTAK_SEPARATOR.getValue());
		refundString.append(fields.get(FieldType.PG_DATE_TIME.getName()));
		refundString.append(Constants.KOTAK_SEPARATOR.getValue());
		refundString.append(fields.get(FieldType.TXN_ID.getName()));
		refundString.append(Constants.KOTAK_SEPARATOR.getValue());
		refundString.append(Amount.toDecimal(
				fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName())));
		refundString.append(Constants.KOTAK_SEPARATOR.getValue());
		refundString.append(fields.get(FieldType.ACQ_ID.getName()));

		return refundString.toString();
	}*/

	public String createSaleTransaction(Fields fields) throws SystemException {

		StringBuilder request = new StringBuilder();
		String amount = Amount.toDecimal(
				fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));

		// Fetching Details from Property file
		PropertiesManager propertiesManager = new PropertiesManager();

		String kotakTransactionUrl = propertiesManager
				.getNetbankingProperty("KotakRequestUrl");
		String kotakMID = propertiesManager.getNetbankingProperty("KotakMID");
		String kotakkey = propertiesManager.getNetbankingProperty("KotakKey");

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
		String currentDate = sdf.format(date);
		fields.put(FieldType.PG_DATE_TIME.getName(), currentDate);

		StringBuilder requestfields = request.append(propertiesManager
				.getNetbankingProperty("RequestCode"));
		request.append(Constants.KOTAK_SEPARATOR.getValue());
		request.append(currentDate);
		request.append(Constants.KOTAK_SEPARATOR.getValue());
		request.append(kotakMID);
		request.append(Constants.KOTAK_SEPARATOR.getValue());
		request.append(fields.get(FieldType.TXN_ID.getName()));
		request.append(Constants.KOTAK_SEPARATOR.getValue());
		request.append(amount);
		request.append(Constants.KOTAK_SEPARATOR.getValue());
		request.append(fields.get(FieldType.PAY_ID.getName()));

		StringBuilder secretkey = new StringBuilder();
		secretkey.append(requestfields);
		secretkey.append(Constants.KOTAK_SEPARATOR.getValue());
		secretkey.append(kotakkey);

		String hash = CalculateChecksum.main(secretkey);

		StringBuilder finalrequestwithhash = new StringBuilder();
		finalrequestwithhash.append(requestfields);
		finalrequestwithhash.append(Constants.KOTAK_SEPARATOR.getValue());
		finalrequestwithhash.append(hash);

		// Request formation for seam less transaction
		StringBuilder httpRequest = new StringBuilder();
		httpRequest.append("<HTML>");
		httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
		httpRequest.append("<form name=\"form1\" action=\"");
		httpRequest.append(kotakTransactionUrl);
		httpRequest.append("\" method=\"post\">");

		httpRequest.append("<input type=\"hidden\" name=\"msg\" value=\"");
		httpRequest.append(finalrequestwithhash);
		httpRequest.append("\">");
		httpRequest.append("</form>");
		httpRequest.append("<script language=\"JavaScript\">");
		httpRequest.append("function OnLoadEvent()");
		httpRequest.append("{document.form1.submit();}");
		httpRequest.append("</script>");
		httpRequest.append("</BODY>");
		httpRequest.append("</HTML>");
		return httpRequest.toString();
	}

	public StringBuilder prepareFieldsForStatus(Fields fields)
			throws SystemException {
		PropertiesManager propertiesManager = new PropertiesManager();
		String kotakKey = propertiesManager.getNetbankingProperty("KotakKey");
		StringBuilder hashString = new StringBuilder();

		StringBuilder requestfieldsforstatus = hashString
				.append(propertiesManager
						.getNetbankingProperty("StatusEnquiryCode"));
		hashString.append(Constants.KOTAK_SEPARATOR.getValue());
		hashString.append(fields.get(FieldType.PG_DATE_TIME.getName()));
		hashString.append(Constants.KOTAK_SEPARATOR.getValue());
		hashString.append(fields.get(FieldType.MERCHANT_ID.getName()));
		hashString.append(Constants.KOTAK_SEPARATOR.getValue());
		hashString.append(fields.get(FieldType.ORIG_TXN_ID.getName()));
		hashString.append(Constants.KOTAK_SEPARATOR.getValue());
		hashString.append(Constants.KOTAK_SEPARATOR.getValue());

		StringBuilder secretkey = new StringBuilder();
		secretkey.append(requestfieldsforstatus);
		secretkey.append(Constants.KOTAK_SEPARATOR.getValue());
		secretkey.append(kotakKey);

		String hash = CalculateChecksum.main(secretkey);

		StringBuilder finalrequestwithhash = new StringBuilder();
		finalrequestwithhash.append(requestfieldsforstatus);
		finalrequestwithhash.append(Constants.KOTAK_SEPARATOR.getValue());
		finalrequestwithhash.append(hash);
		return finalrequestwithhash;

	}

	public String getRequestparameter() {
		return requestparameter;
	}

	public void setRequestparameter(String requestparameter) {
		this.requestparameter = requestparameter;
	}

	public String getTextBetweenTags(String response, String tag1, String tag2) {

		int leftIndex = response.indexOf(tag1);
		if (leftIndex == -1) {
			return null;
		}

		int rightIndex = response.indexOf(tag2);
		if (rightIndex != -1) {
			leftIndex = leftIndex + tag1.length();
			return response.substring(leftIndex, rightIndex).trim();
		}

		return null;
	}// getTextBetweenTags()
}
