package com.kbn.netbanking.kotak;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.netbanking.kotak.KotakTransactionProcessorFactory;
import com.kbn.pg.core.AbstractTransactionProcessorFactory;
import com.kbn.pg.core.TransactionProcessor;



public class KotakIntegrator {

	private CryptoManager cryptoManager = CryptoManagerFactory.getCryptoManager();

	private AbstractTransactionProcessorFactory transactionProcessorFactory = new KotakTransactionProcessorFactory();

	public void process(Fields fields) throws SystemException {

		addDefaultFields(fields);

		send(fields);

		cryptoManager.secure(fields);
	}
	public void send(Fields fields) throws SystemException {

		TransactionProcessor transactionProcessor = transactionProcessorFactory.getInstance(fields);
		transactionProcessor.transact(fields);
	}

	public static void addDefaultFields(Fields fields) {

	}
}

