package com.kbn.netbanking.kotak;

import com.kbn.netbanking.kotak.KotakResultType;

public enum KotakResultType {
	TXN_SUCCESS		("TXN_SUCCESS"),
	TXN_FAILURE		("TXN_FAILURE");
	
	private KotakResultType(String name){
		this.name = name;
	}
	
	private final String name;

	public String getName() {
		return name;
	}
	
	public static KotakResultType getInstance(String name){
		if(null == name){
			return TXN_FAILURE;
		}
		
		KotakResultType[] kotakResultTypes = KotakResultType.values();
		
		for(KotakResultType kotakResultType : kotakResultTypes){
			if(kotakResultType.getName().startsWith(name)){
				return kotakResultType;
			}
		}
		
		//Return error if unexpected value is returned in parameter "respCode"
		return TXN_FAILURE;
	}

}
