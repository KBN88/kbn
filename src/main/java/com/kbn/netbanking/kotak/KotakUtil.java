package com.kbn.netbanking.kotak;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.SystemConstants;

public class KotakUtil {
	private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
	private String[] kotakResponse;
	PropertiesManager propertiesManager = new PropertiesManager();
	String kotakkey = propertiesManager.getNetbankingProperty("KotakKey");

	public static String generateHMAC(String data, String hexEncodedKey)
			throws SystemException {
		String result = "";
		try {
			byte[] keyBytes = hexEncodedKey.getBytes();
			SecretKeySpec signingKey = new SecretKeySpec(keyBytes,
					HMAC_SHA1_ALGORITHM);

			Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
			mac.init(signingKey);
			byte[] rawHmac = mac.doFinal(data.getBytes());

			byte[] hexBytes = new Hex().encode(rawHmac);
			result = new String(hexBytes, SystemConstants.DEFAULT_ENCODING_UTF_8);
		} catch (NoSuchAlgorithmException noSuchAlgorithmException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					noSuchAlgorithmException,
					"No such Hashing algoritham with Kotak");
		} catch (InvalidKeyException invalidKeyException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					invalidKeyException, "No such key with Kotak");
		} catch (UnsupportedEncodingException unsupportedEncodingException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					unsupportedEncodingException,
					"No such encoding supported with kotak");
		}

		return result;
	}

	private static Logger logger = Logger.getLogger(KotakUtil.class.getName());

	private static Key generateKeyFromString(String secretKey) throws Exception {
		byte[] keyValue = Base64.decodeBase64(secretKey.getBytes());
		Key key = new SecretKeySpec(keyValue, "AES");
		return key;
	}

	public static String encrypt(String valueToEnc, String secretKey)
			throws SystemException {
		String encryptedValue = null;
		try {
			Key key = generateKeyFromString(secretKey);
			Cipher c = Cipher.getInstance("AES");
			c.init(1, key);
			byte[] encValue = c.doFinal(valueToEnc.getBytes(""));

			encryptedValue = new String(Base64.encodeBase64(encValue));
		} catch (Exception exception) {
			logger.error("Exception", exception);
			throw new SystemException(ErrorType.CRYPTO_ERROR,
					ErrorType.CRYPTO_ERROR.getResponseMessage());
		}
		return encryptedValue;
	}

	public static String decrypt(String encryptedValue, String secretKey)
			throws SystemException {
		String decryptedValue = null;

		try {
			Key key = generateKeyFromString(secretKey);
			Cipher c = Cipher.getInstance("AES");
			c.init(2, key);

			byte[] decordedValue = Base64.decodeBase64(encryptedValue
					.getBytes());
			byte[] decValue = c.doFinal(decordedValue);
			decryptedValue = new String(decValue);
		} catch (Exception exception) {
			logger.error("Exception", exception);
			throw new SystemException(ErrorType.CRYPTO_ERROR,
					ErrorType.CRYPTO_ERROR.getResponseMessage());
		}
		return decryptedValue;
	}

	public boolean compareResponseHash(Fields fields, String responseString)
			throws SystemException {
			
		String responsewithouthash = (responseString.substring(0,
				responseString.lastIndexOf('|')));

		StringBuilder createrequest = new StringBuilder();
		createrequest.append(responsewithouthash);
		createrequest.append(Constants.KOTAK_SEPARATOR.getValue());
		createrequest.append(kotakkey);

		String calculatedHash = CalculateChecksum.main(createrequest);

		String[] responseArray = responseString.split("\\|");
		String receivedHash = responseArray[7];

		return calculatedHash.equals(receivedHash);

	}

	public boolean compareStatusResponseHash(Fields fields,
			String responseString) throws SystemException {
		String response = responseString;
		String responsewithouthash = (response.substring(0,
				response.length() - 11));

		StringBuilder createrequest = new StringBuilder();
		createrequest.append(responsewithouthash);
		createrequest.append(Constants.KOTAK_SEPARATOR.getValue());
		createrequest.append(kotakkey);

		String calculatedHash = CalculateChecksum.main(createrequest);

		String[] p = responseString.split("\\|");
		String receivedHash = p[7];

		return calculatedHash.equals(receivedHash);

	}

}
