package com.kbn.amex;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.SystemConstants;
import com.kbn.commons.util.TransactionType;

/**
 * @author Sunil
 *
 */
public class TransactionCommunicator {
	private static Logger logger = Logger
			.getLogger(TransactionCommunicator.class.getName());

	public String getResponseString(String request, Fields fields)
			throws SystemException {
		String url;
		String response = "";
	
			switch (TransactionType.getInstance(fields.get(FieldType.TXNTYPE
					.getName()))) {
			case ENROLL:
				url = ConfigurationConstants.AMEX_TRANSACTION_URL.getValue();
				response = sendEnrollTransaction(fields, request, url);
				break;
			case AUTHORISE:
				url = ConfigurationConstants.AMEX_SUPPORT_URL.getValue();
				response = transact(fields, request, url);
				break;
			case CAPTURE:
				url = ConfigurationConstants.AMEX_SUPPORT_URL.getValue();
				response = transact(fields, request, url);
				break;
			case REFUND:
				url = ConfigurationConstants.AMEX_SUPPORT_URL.getValue();
				response = transact(fields, request, url);
				break;
			case STATUS:
				url = ConfigurationConstants.AMEX_SUPPORT_URL.getValue();
				response = transact(fields, request, url);
				break;
			default:
				break;
			}		
		return response;
	}

	public Transaction getResponseObject(String request, Fields fields)
			throws SystemException {
		TransactionConverter txnConverter = new TransactionConverter();
		String response = getResponseString(request, fields);
		return txnConverter.getResponse(response);
	}

	public String transact(Fields fields, String request, String hostUrl)
			throws SystemException {
		logRequest(request,fields);
		String response = "";
		try {
			URL url = new URL(hostUrl);
			HttpsURLConnection connection = (HttpsURLConnection) url
					.openConnection();

			connection.setRequestProperty("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestMethod("POST");

			DataOutputStream dataoutputstream = new DataOutputStream(
					connection.getOutputStream());

			dataoutputstream.write(request.getBytes(SystemConstants.DEFAULT_ENCODING_UTF_8));
			dataoutputstream.flush();
			dataoutputstream.close();

			BufferedReader bufferedreader = new BufferedReader(
					new InputStreamReader(connection.getInputStream()));
			String decodedString;

			while ((decodedString = bufferedreader.readLine()) != null) {
				response = response + decodedString;
			}
			bufferedreader.close();
			connection.disconnect();
		} catch (IOException ioException) {
			logger.error(ioException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					ioException, "Network Exception with Amex for "
							+ fields.get(FieldType.TXNTYPE.getName()) + " txn "
							+ hostUrl.toString());
		}
		logResponse(response, fields);
		return response;
	}
	public String executePost(String targetURL, String urlParameters) throws Exception
	  {
	    URL url;
	    HttpURLConnection connection = null;  
	    String resStr = null;
	    try {
	      //Create connection
	      url = new URL(targetURL);
	      connection = (HttpURLConnection)url.openConnection();
	      connection.setRequestMethod("POST");
	      connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				
	      connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
	      connection.setRequestProperty("Content-Language", "en-US");  
				
	      connection.setUseCaches (false);
	      connection.setDoInput(true);
	      connection.setDoOutput(true);

	      //Send request
	      DataOutputStream wr = new DataOutputStream (connection.getOutputStream ());
	      wr.writeBytes (urlParameters);
	      wr.flush ();
	      wr.close ();

	      //Get Response	
	      InputStream is = connection.getInputStream();
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	      String line;
	      StringBuffer response = new StringBuffer(); 
	      while((line = rd.readLine()) != null) {
	        response.append(line);
	        response.append('\r');
	      }
	      rd.close();
	      
	      resStr = response.toString();
	     
	      if(resStr != null)
	    	  return resStr.trim();
	      else
	    	  return null; 

	    } catch (Exception exception) {
	    	logger.error(exception);
	        throw new SystemException(ErrorType.ACQUIRER_ERROR,"Unable to get respose from ezeeclick");
	    } finally {

	      if(connection != null) {
	        connection.disconnect(); 
	      }
	    }
	  }

	public String sendEnrollTransaction(Fields fields, String request,
			String hostUrl) throws SystemException {

		PrintWriter out;
		try {
			out = ServletActionContext.getResponse().getWriter();

			out.write(request);

		} catch (IOException iOException) {
			logger.error(iOException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					iOException, "Network Exception with Amex for auth"
							+ hostUrl.toString());
		}
		return ErrorType.SUCCESS.getResponseMessage();
	}
	
	public void logRequest(String requestMessage, Fields fields){
		log("Request message to Amex:" + requestMessage, fields);
	}
	
	public void logResponse(String responseMessage, Fields fields){
		log("Response message from Amex:" + responseMessage, fields);
	}
	
	public void log(String message, Fields fields){
		message = Pattern.compile("(vpc_CardNum=)([\\s\\S]*?)(&)").matcher(message).replaceAll("$1$3");
		message = Pattern.compile("(vpc_CardExp=)([\\s\\S]*?)(&)").matcher(message).replaceAll("$1$3");
		message = Pattern.compile("(vpc_CardSecurityCode=)([\\s\\S]*?)(&)").matcher(message).replaceAll("$1$3");
		
		MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(), fields.getCustomMDC());
		logger.info(message);
	}

}
