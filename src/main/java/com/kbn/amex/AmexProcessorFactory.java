/**
 * 
 */
package com.kbn.amex;

import com.kbn.pg.core.Processor;

/**
 * @author Sunil
 *
 */
public class AmexProcessorFactory {
	public static Processor getInstance(){
		return new AmexProcessor();
	}
}