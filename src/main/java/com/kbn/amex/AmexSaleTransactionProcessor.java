package com.kbn.amex;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.TransactionProcessor;

public class AmexSaleTransactionProcessor implements TransactionProcessor {

	@Override
	public void transact(Fields fields) throws SystemException {

		TransactionCommunicator communicator = new TransactionCommunicator();
		CryptoManager cryptoManager = CryptoManagerFactory.getCryptoManager();
		TransactionConverter transactionConverter = new TransactionConverter();
        
		//to put OID for vpc_orderInfo field
		String oid = fields.get(FieldType.OID.getName());
		if(StringUtils.isBlank(oid)){
			fields.put(FieldType.OID.getName(),fields.get(FieldType.INTERNAL_ORIG_TXN_ID.getName()));
		}

		String request = transactionConverter.getRequest(fields);

		communicator.getResponseString(request, fields);
		fields.put(FieldType.STATUS.getName(), StatusType.ENROLLED.getName());
		fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getResponseCode());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
		cryptoManager.secure(fields);
	}
}