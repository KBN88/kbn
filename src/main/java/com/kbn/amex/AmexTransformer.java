package com.kbn.amex;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;

/**
 * @author Sunil
 *
 */
public class AmexTransformer {
	private Transaction response;
	private String[] ezeeClickResponse;
    private static final String separator = "\\|";
    private static final String zero = "0";
    private static final String one = "1";
    private static final String two = "2";    
    
	public AmexTransformer(Transaction response){
		this.response = response;
	}

	public AmexTransformer(String responseString){		
		this.ezeeClickResponse = responseString.split(separator);
	}

	public void updateResponse(Fields fields){
		//change according to txn type
		String status = getStatus();
		ErrorType errorType = getResponseCode();
		if(!(StatusType.APPROVED.getName().equals(status))){
			//This is applicable when we sent invalid request to server
			fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.REJECTED.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.REJECTED.getName());
			fields.put(FieldType.PG_TXN_MESSAGE.getName(), response.getMessage());
			return;
		}

		fields.put(FieldType.STATUS.getName(), status);
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
		fields.put(FieldType.RRN.getName(), response.getRRN());
		fields.put(FieldType.PG_REF_NUM.getName(), response.getPgTransactionNo());
		fields.put(FieldType.ACQ_ID.getName(), response.getAuthorizeId());
		if(fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.CAPTURE.getName()) || (fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.REFUND.getName()))){
			if(fields.get(FieldType.STATUS.getName()).equals(StatusType.APPROVED.getName())){
				fields.put(FieldType.STATUS.getName(),StatusType.CAPTURED.getName());
			}
		}else{
			fields.put(FieldType.TXNTYPE.getName(), fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()));
		}
	}

	public ErrorType getResponseCode(){
		String respCode = response.getResponseCode();
		ErrorType errorType = null;

		if(null == respCode){
			errorType = ErrorType.ACQUIRER_ERROR;
		} else {
			errorType = Mapper.getErrorType(respCode);
		}		
		return errorType;
	}

	public String getStatus(){
		String respCode = response.getResponseCode();
		String status = "";

		if(null == respCode){
			status = StatusType.ERROR.getName();
		} else if(respCode.equals(zero)){
			status = StatusType.APPROVED.getName();
		} else if(respCode.equals(one)){
			status = StatusType.ERROR.getName();
		} else if(respCode.equals(two)){
			status = StatusType.FAILED.getName();
		} else {
			status = StatusType.ERROR.getName();
		}

		return status;
	}

	public ErrorType getEzeeClickResponse(String respCode){
		ErrorType errortype = null;

		if(null == respCode){
			errortype = ErrorType.ACQUIRER_ERROR;
		} else if(respCode.equals("S")){
			errortype = ErrorType.SUCCESS;
		}else if(respCode.equals("F")){
			errortype = ErrorType.REJECTED;
		} else {
			errortype = ErrorType.ACQUIRER_ERROR;
		}
		return errortype;
	}

	public StatusType getEzeeClickStatus(String respCode){
		StatusType status = null;

		if(null == respCode){
			status = StatusType.ERROR;
		} else if(respCode.equals("S")){
			status = StatusType.CAPTURED;
		}else if(respCode.equals("F")){
			status = StatusType.REJECTED;
		}else {
			status = StatusType.ERROR;
		}
		return status;
	}

	public void updateEzeeClickResponse(Fields fields){
		ErrorType errorType = null;
		StatusType status = null;
		if(ezeeClickResponse.length!=35){
			errorType =  getEzeeClickResponse(null);
			fields.put(FieldType.STATUS.getName(),StatusType.ERROR.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(),errorType.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),errorType.getResponseMessage());
			return;
		}
		status = getEzeeClickStatus(ezeeClickResponse[2]);
		errorType = getEzeeClickResponse(ezeeClickResponse[2]);

		fields.put(FieldType.ACQ_ID.getName(),ezeeClickResponse[0]);
		fields.put(FieldType.STATUS.getName(),status.getName());
		fields.put(FieldType.RESPONSE_CODE.getName(),errorType.getResponseCode());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(),errorType.getResponseMessage());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(),ezeeClickResponse[34]);
	//	fields.put(FieldType.AMOUNT.getName(),ezeeClickResponse[4]);
		fields.put(FieldType.RRN.getName(),ezeeClickResponse[11]);
		fields.put(FieldType.PG_REF_NUM.getName(),ezeeClickResponse[30]);
	}
}
