package com.kbn.amex;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class EzeeClickRefundTransactionProcessor implements TransactionProcessor{
	
	private TransactionConverter transactionConverter = new TransactionConverter();
	private TransactionCommunicator communicator = new TransactionCommunicator();
	
	@Override
	public void transact(Fields fields) throws SystemException {
	
		String request = transactionConverter.getRefundRequest(fields);
		String responseString = communicator.getResponseString(request, fields);

		Transaction response = transactionConverter.getResponse(responseString);
		AmexTransformer amexTransformer = new AmexTransformer(response);
		amexTransformer.updateResponse(fields);
	}
}
