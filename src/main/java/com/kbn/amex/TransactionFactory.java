package com.kbn.amex;

import java.util.Map;
import java.util.TreeMap;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;


/**
 * @author Sunil
 *
 */

public class TransactionFactory {

	public static String getInstance(Fields fields) throws SystemException{
		String request="";
			
		// Fetching Details from Property file
		PropertiesManager propertiesManager = new PropertiesManager();

		Map<String, String> treeMap = new TreeMap<String, String>(propertiesManager.getAllProperties(PropertiesManager.getAmexpropertiesfile()));
		
		for (String key : treeMap.keySet()) {
			String value = treeMap.get(key);
			if (value.startsWith(Constants.CONFIG_SEPARATOR)){		
				value = value.replace(Constants.CONFIG_SEPARATOR, "");				
			}else{
				value = fields.get(value);
			}
			
			if(key.equals(Constants.CARDEXP)){				
				value=AmexUtil.parseDate(value);
			}
			treeMap.put(key, value);
		}
		
		StringBuilder allFields = new StringBuilder();
	//	allFields.append(getSecureSecret());

		for (String key : treeMap.keySet()) {				
			String value = treeMap.get(key);
					
			allFields.append(key);
			allFields.append(Constants.EQUATOR);
			allFields.append(value);
		allFields.append(Constants.SEPARATOR);
		}

		String temp = allFields.toString();
	   int length = temp.length();
		String allFields1 = allFields.substring(0,length-1);
		
		String secureHash = AmexUtil.calculateMac( allFields1,fields.get(FieldType.TXN_KEY.getName()));
		
		request = allFields1 +"&" + Constants.SECUREHASH + "=" + secureHash +"&"+ Constants.SECURE_HASH_TYPE +"="+ Constants.HASHALGO;
		
		return request;
	}
}
