package com.kbn.amex;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AbstractTransactionProcessorFactory;
import com.kbn.pg.core.TransactionProcessor;

public class EzeeClickTransactionProcessorFactory implements
		AbstractTransactionProcessorFactory {

		private TransactionProcessor transactionProcessor;

		@Override
		public TransactionProcessor getInstance(Fields fields) throws SystemException{		

			switch(TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))){
			case REFUND:
				transactionProcessor = new EzeeClickRefundTransactionProcessor();
				break;
			case SALE:
			case ENROLL:
			case AUTHORISE:
				transactionProcessor = new EzeeClickSaleTransactionProcessor();
				break;
			case STATUS:
				transactionProcessor = new EzeeClickStatusTransactionProcessor();
				break;
			default:
				throw new SystemException(ErrorType.ACQUIRER_ERROR, "Unsupported transaction type for ezeeclick");
			}
			return transactionProcessor;
		}
}