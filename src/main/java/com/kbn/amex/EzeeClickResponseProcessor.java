package com.kbn.amex;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;

public class EzeeClickResponseProcessor implements Processor {

	private static Logger logger = Logger.getLogger(EzeeClickResponseProcessor.class.getName());

	String decryptedResponse = "";

	@Override
	public void preProcess(Fields fields) throws SystemException {
		String response =fields.get(Constants.EZEE_CLICK_RESPONSE);
		logger.info("Response recieved from amex" + response);
		String encryptionKey = fields.get(FieldType.TXN_KEY.getName());
		fields.remove(Constants.EZEE_CLICK_RESPONSE);

		if(!StringUtils.isEmpty(response)){
			decryptedResponse = AmexUtil.decrypt(response,encryptionKey);
			logger.info("Decrypted response from amex" + decryptedResponse);		
		}
	}

	@Override
	public void process(Fields fields) throws SystemException {
		AmexTransformer transformer = new AmexTransformer(decryptedResponse);
		transformer.updateEzeeClickResponse(fields);
		fields.updateTransactionDetails();
		fields.updateNewOrderDetails();		
	}

	@Override
	public void postProcess(Fields fields) throws SystemException {
		new ResponseCreator().create(fields);
		fields.removeVpcFields();
		fields.removeInternalFields();
		fields.put(FieldType.HASH.getName(), Hasher.getHash(fields));
	}
}
