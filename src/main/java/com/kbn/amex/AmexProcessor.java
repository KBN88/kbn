package com.kbn.amex;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Processor;

/**
 * @author Sunil
 *
 */
public class AmexProcessor implements Processor {
	
	public void preProcess(Fields fields) throws SystemException {
	}

	public void process(Fields fields) throws SystemException {
		if (fields.get(FieldType.TXNTYPE.getName()).equals(
				TransactionType.NEWORDER.getName())) {
			// New Order Transactions are not processed by Amex
			return;
		}
		
		if(!fields.get(FieldType.ACQUIRER_TYPE.getName()).equals(AcquirerType.AMEX.getCode())){
			return;
		}
		
		(new AmexIntegrator()).process(fields);
	}

	public void postProcess(Fields fields) throws SystemException {
	}
}
