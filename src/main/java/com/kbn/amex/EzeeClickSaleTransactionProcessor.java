package com.kbn.amex;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.TransactionProcessor;

/**
 * @author Puneet
 *
 */
public class EzeeClickSaleTransactionProcessor implements TransactionProcessor{
	TransactionConverter transactionConverter = new TransactionConverter();
	TransactionCommunicator communicator = new TransactionCommunicator();

	@Override
	public void transact(Fields fields) throws SystemException {
		String request = transactionConverter.getEzeeClickRequest(fields);
		communicator.sendEnrollTransaction(fields, request, "");
		fields.put(FieldType.STATUS.getName(),StatusType.SENT_TO_BANK.getName());
		fields.put(FieldType.RESPONSE_CODE.getName(),ErrorType.SUCCESS.getResponseCode());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.SUCCESS.getResponseMessage());
	}
}
