package com.kbn.amex;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class EzeeClickStatusTransactionProcessor implements TransactionProcessor{

	private TransactionConverter transactionConverter = new TransactionConverter();
	private TransactionCommunicator communicator = new TransactionCommunicator();

	@Override
	public void transact(Fields fields) throws SystemException {
		String request = transactionConverter.getEzeeClickStatusRequest(fields);
		String url = ConfigurationConstants.AMEX_EZEE_CLICK_STATUS_TXN_URL.getValue();
		String responseString = null;
		try {
			responseString = communicator.executePost(url,request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	//	Transaction response = transactionConverter.getResponse(responseString);
	//	AmexTransformer amexTransformer = new AmexTransformer(response);
	//	amexTransformer.updateResponse(fields);
	}
}
