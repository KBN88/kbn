package com.kbn.amex;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.TransactionProcessor;

/**
 * @author Sunil
 *
 */
public class AmexIntegrator {
	private TransactionCommunicator communicator = new TransactionCommunicator();
	private CryptoManager cryptoManager = CryptoManagerFactory.getCryptoManager();
	private TransactionConverter transactionConverter = new TransactionConverter();
	
	public void process(Fields fields) throws SystemException {
		send(fields);
		cryptoManager.secure(fields);
	}//process

	public void send(Fields fields) throws SystemException {
		
		//TODO.... make processor for refund and capture also
		if(fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.ENROLL.getName())){
			TransactionProcessor transactionProcessor = new AmexSaleTransactionProcessor();
			transactionProcessor.transact(fields);
			return;
		}
		String request = transactionConverter.getRequest(fields);

		String responseString = communicator.getResponseString(request, fields);

		Transaction response = transactionConverter.getResponse(responseString);

		AmexTransformer amexTransformer = new AmexTransformer(response);

		amexTransformer.updateResponse(fields);
	}

	public TransactionCommunicator getCommunicator() {
		return communicator;
	}
	public void setCommunicator(TransactionCommunicator communicator) {
		this.communicator = communicator;
	}
}
