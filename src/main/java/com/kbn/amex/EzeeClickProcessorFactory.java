package com.kbn.amex;

import com.kbn.pg.core.Processor;

public class EzeeClickProcessorFactory {
	public static Processor getInstance(){
		return new EzeeClickProcessor();
	}
}
