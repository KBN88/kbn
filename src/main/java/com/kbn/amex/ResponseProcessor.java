package com.kbn.amex;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.ModeType;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionManager;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.ProcessManager;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.core.UpdateProcessorFactory;

/**
 * @author Sunil
 *
 */
public class ResponseProcessor implements Processor {

	private Transaction responseObject = new Transaction();
	private ResponseFactory responseFactory = new ResponseFactory();
	private static final String zero = "0";

	public void preProcess(Fields fields) throws SystemException {

		try {
			// Hash Calculator
			HashComparator hashComparator = new HashComparator();
			hashComparator.compareHash(fields);
			responseObject = responseFactory.createResponse(fields);
		} catch (SystemException systemException) {
			fields.put(FieldType.STATUS.getName(), StatusType.ERROR.getName());
			fields.put(FieldType.TXNTYPE.getName(),
					fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()));
			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.SIGNATURE_MISMATCH.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.SIGNATURE_MISMATCH.getResponseMessage());			
			fields.updateNewOrderDetails();
			new ResponseCreator().create(fields);
			fields.removeVpcFields();
			fields.removeInternalFields();			
			throw systemException;
		}
	}

	public void process(Fields fields) throws SystemException {

		String responseCodeFromAmex = responseObject.getResponseCode();
		if(!responseCodeFromAmex.equals(zero)){
			updateFailedResponse(fields);
			return;
		}
		//If 3ds enrolled status is Y and status is A or Y then success
		if((responseObject.getThreeDSenrolled().equals(Constants.Y) && responseObject.getThreeDSstatus().equals(Constants.Y)) || 
		    (responseObject.getThreeDSenrolled().equals(Constants.Y) && responseObject.getThreeDSstatus().equals(Constants.A))){

			fields.put(FieldType.TXNTYPE.getName(),TransactionType.AUTHORISE.getName());
			//send request for auth
			fields.put(FieldType.TXN_ID.getName(), TransactionManager.getNewTransactionId());
			new AmexIntegrator().process(fields);

			String payId = fields.get(FieldType.PAY_ID.getName());
			User user = new UserDao().getUserClass(payId);
			String trnsactionType = ModeType.getDefaultPurchaseTransaction(user.getModeType()).getName();
			if(trnsactionType.equals(TransactionType.SALE.getName())){
				fields.put(FieldType.TXNTYPE.getName(),TransactionType.AUTHORISE.getName());
			//	fields.insert();
				ProcessManager.flow(UpdateProcessorFactory.getInstance(), fields, true);
				//capture the transaction
				if(fields.get(FieldType.STATUS.getName()).equals(StatusType.APPROVED.getName())){
					fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(),TransactionType.AUTHORISE.getName());
					fields.put(FieldType.ORIG_TXN_ID.getName(),fields.get(FieldType.TXN_ID.getName()));
					fields.put(FieldType.TXNTYPE.getName(),TransactionType.CAPTURE.getName());
					
					fields.put(FieldType.TXN_ID.getName(), TransactionManager.getNewTransactionId());
					new AmexIntegrator().process(fields);
					fields.insert();
					String responseCode = fields.get(FieldType.RESPONSE_CODE.getName());
					if (null == responseCode || !responseCode.equals(ErrorType.SUCCESS.getCode())) {
						//TODO discuss whether to throw an error or continue with current status
					}
				}
			}else{
			//	fields.insert();
				ProcessManager.flow(UpdateProcessorFactory.getInstance(), fields, true);
			}
		}else{
			updateFailedResponse(fields);
		}
	}

	private void updateFailedResponse(Fields fields) throws SystemException{
		//return to user after UPDATING transaction: NEW ORDER for error			
		AmexTransformer amexTransformer = new AmexTransformer(responseObject);
		amexTransformer.updateResponse(fields);
		fields.put(FieldType.STATUS.getName(),StatusType.REJECTED.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.REJECTED.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(),ErrorType.REJECTED.getResponseCode());
		fields.updateNewOrderDetails();		
	}

	public void postProcess(Fields fields) throws SystemException {
		//clean fields
		new ResponseCreator().create(fields);
		fields.removeVpcFields();
		fields.removeInternalFields();
	}
}
