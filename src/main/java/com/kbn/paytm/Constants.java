/**
 * 
 */
package com.kbn.paytm;

/**
 * @author Sunil
 *
 */
public class Constants {
	public static final String EQUATOR = "=";
	public static final String SEPARATOR = "&";
	public static final String OPENING_BRACE = "{";
	public static final String CLOSING_BRACE = "}";
	
	public static final String MID = "MID";	
	public static final String TXNID = "TXNID";
	public static final String ORDERID = "ORDERID";
	public static final String REFUNDAMOUNT = "REFUNDAMOUNT";
	public static final String TXNTYPE = "TXNTYPE";
	public static final String COMMENTS = "COMMENTS";
	public static final String TXNAMOUNT = "TXNAMOUNT";
	public static final String TXNDATE = "TXNDATE";
	public static final String RESPCODE = "RESPCODE";
	public static final String RESPMSG = "RESPMSG";
	public static final String STATUS = "STATUS";
	public static final String BANKTXNID = "BANKTXNID";
	public static final String GATEWAYNAME = "GATEWAYNAME";
	public static final String BANKNAME = "BANKNAME";
	public static final String PAYMENTMODE = "PAYMENTMODE";
	public static final String CHECKSUMHASH = "CHECKSUMHASH";
}
