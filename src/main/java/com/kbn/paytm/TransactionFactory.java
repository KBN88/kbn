package com.kbn.paytm;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;

/**
 * @author Sunil
 *
 */
public class TransactionFactory {
	@SuppressWarnings("incomplete-switch")
	public static String getInstance(Fields fields){
		PaytmTransaction transaction = new PaytmTransaction();
		String request = "";
		
		switch(TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))){
		case REFUND:
			request = transaction.createRefund(fields);
			break;
		case STATUS:
			request = transaction.createEnquiry(fields);
			break;
		}
		
		return request;
	}
}
