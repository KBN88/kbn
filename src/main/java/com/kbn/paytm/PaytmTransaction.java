package com.kbn.paytm;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.pageintegrator.Transaction;


public class PaytmTransaction extends Transaction {
	
	public String createRefund(Fields fields) {
		StringBuilder request = new StringBuilder();
		Fields previous = fields.getPrevious();	
		appendJsonField(Constants.MID, fields.get(FieldType.MERCHANT_ID.getName()), request);	
		appendJsonField(Constants.TXNID, previous.get(FieldType.PG_REF_NUM.getName()), request);
		appendJsonField(Constants.ORDERID, previous.get(FieldType.TXN_ID.getName()), request);	
		appendJsonField(Constants.TXNAMOUNT, previous.get(FieldType.AMOUNT.getName()), request);	
		appendJsonField(Constants.REFUNDAMOUNT, fields.get(FieldType.REFUNDAMOUNT.getName()), request);	
		appendJsonField(Constants.TXNTYPE, fields.get(FieldType.TXNTYPE.getName()), request);
		appendJsonField(Constants.COMMENTS, fields.get(FieldType.RESPONSE_MESSAGE.getName()), request);	
		appendJsonField(Constants.CHECKSUMHASH, fields.get(FieldType.HASH.getName()), request);
		
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);		
		return jsonRequest.toString();		
	}

	public String createEnquiry(Fields fields) {
		StringBuilder request = new StringBuilder();
		appendOrderInfo(fields, request);
		
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);		
		return jsonRequest.toString();
	}
	

	public void appendOrderInfo(Fields fields, StringBuilder request){
		Fields previous = fields.getPrevious();
		appendJsonField(Constants.MID, fields.get(FieldType.MERCHANT_ID.getName()), request);
		appendJsonField(Constants.ORDERID, previous.get(FieldType.ORDER_ID.getName()), request);	
	}
	
}
