/**
 * 
 */
package com.kbn.paytm;


/**
 * @author Sunil
 *
 */
public enum PaytmResultType {
	TXN_SUCCESS		("TXN_SUCCESS"),
	TXN_FAILURE		("TXN_FAILURE");
	
	private PaytmResultType(String name){
		this.name = name;
	}
	
	private final String name;

	public String getName() {
		return name;
	}
	
	public static PaytmResultType getInstance(String name){
		if(null == name){
			return TXN_FAILURE;
		}
		
		PaytmResultType[] paytmResultTypes = PaytmResultType.values();
		
		for(PaytmResultType paytmResultType : paytmResultTypes){
			if(paytmResultType.getName().startsWith(name)){
				return paytmResultType;
			}
		}
		
		//Return error if unexpected value is returned in parameter "respCode"
		return TXN_FAILURE;
	}
}

