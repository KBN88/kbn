/**
 * 
 */
package com.kbn.paytm;

import com.kbn.pg.core.Processor;

/**
 * @author Sunil
 *
 */
public class PaytmProcessorFactory {
	public static Processor getInstance(){
		return new PaytmProcessor();
	}
}