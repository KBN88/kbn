package com.kbn.paytm;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.SystemConstants;

public class TransactionCommunicator {

	public String getResponseString(String request, Fields fields)
			throws SystemException {

			//Get transaction URL from configurations - based on type of transaction
			String url = Mapper.getTransactionUrl(fields);
			
			//Perform transaction with Paytm
			String response = transact(fields, request, url);

			return response;
	}
	
	public Response getResponseObject(String request, Fields fields) throws SystemException{
		String response = getResponseString(request, fields);
		return ResponseFactory.createResponse(response);
	}
	
	public String transact(Fields fields, String request, String hostUrl) throws SystemException{
		
		String response = "";
		
		try {
			
			URL url = new URL(hostUrl);
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Accept", "application/json");	
			connection.setRequestProperty("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestMethod("POST");
			
			DataOutputStream dataoutputstream = new DataOutputStream(
					connection.getOutputStream());

			dataoutputstream.write(request.getBytes(SystemConstants.DEFAULT_ENCODING_UTF_8));
			dataoutputstream.flush();
			dataoutputstream.close();
			BufferedReader bufferedreader = new BufferedReader(
					new InputStreamReader(connection.getInputStream()));
			String decodedString;
			while ((decodedString = bufferedreader.readLine()) != null) {
				response = response + decodedString;
			}
		} catch (IOException ioException) {
			ioException.printStackTrace();
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					ioException, "Network Exception with Paytm "
							+ hostUrl.toString());
		}
		
		return response;
	}
}
