/**
 * 
 */
package com.kbn.paytm;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;

/**
 * @author Sunil
 *
 */
public class PaytmIntegrator {
	private TransactionCommunicator communicator = new TransactionCommunicator();
	private CryptoManager cryptoManager = CryptoManagerFactory.getCryptoManager();
	
	public void process(Fields fields) throws SystemException {
	
		send(fields);	
				
		cryptoManager.secure(fields);
	}//process

	public void send(Fields fields) throws SystemException {
		String request = TransactionFactory.getInstance(fields);
		
		Response response = communicator.getResponseObject(request, fields);
		PaytmTransformer paytmTransformer = new PaytmTransformer(response);
		
		paytmTransformer.updateResponse(fields);
	}
	public TransactionCommunicator getCommunicator() {
		return communicator;
	}
	public void setCommunicator(TransactionCommunicator communicator) {
		this.communicator = communicator;
	}
}
