package com.kbn.paytm;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;

/**
 * @author Sunil
 *
 */
public class ResponseFactory {
	private static Logger logger = Logger.getLogger(ResponseFactory.class
			.getName());

	public static Response createResponse(String responseString)
			throws SystemException {

		Response response = new Response();
		JSONParser parser = new JSONParser();
		Object obj;
		try {
			obj = parser.parse(responseString);

			JSONObject jObject = (JSONObject) obj;

			Object mid = jObject.get(Constants.MID);
			if (null != mid) {
				response.setRedirectUrl((String) mid);
			}

			Object txnId = jObject.get(Constants.TXNID);
			if (null != txnId) {
				response.setRedirectUrl((String) txnId);
			}

			Object orderId = jObject.get(Constants.ORDERID);
			if (null != orderId) {
				response.setRespCode((String) orderId);
			}

			Object refundAmount = jObject.get(Constants.REFUNDAMOUNT);
			if (null != refundAmount) {
				response.setRespMsg((String) refundAmount);
			}

			Object txnType = jObject.get(Constants.TXNTYPE);
			if (null != txnType) {
				response.setTransactionId((String) txnType);
			}

			Object comments = jObject.get(Constants.COMMENTS);
			if (null != comments) {
				response.setTransactionId((String) comments);
			}

			Object txnAmount = jObject.get(Constants.TXNAMOUNT);
			if (null != txnAmount) {
				response.setTransactionId((String) txnAmount);
			}

			Object txnDate = jObject.get(Constants.TXNDATE);
			if (null != txnDate) {
				response.setTransactionId((String) txnDate);
			}

			Object respCode = jObject.get(Constants.RESPCODE);
			if (null != respCode) {
				response.setTransactionId((String) respCode);
			}

			Object respMsg = jObject.get(Constants.RESPMSG);
			if (null != respMsg) {
				response.setTransactionId((String) respMsg);
			}

			Object status = jObject.get(Constants.STATUS);
			if (null != status) {
				response.setTransactionId((String) status);
			}

			Object bankTxnId = jObject.get(Constants.BANKTXNID);
			if (null != bankTxnId) {
				response.setTransactionId((String) bankTxnId);
			}

			Object gatewayNmae = jObject.get(Constants.GATEWAYNAME);
			if (null != gatewayNmae) {
				response.setTransactionId((String) gatewayNmae);
			}

			Object bankName = jObject.get(Constants.BANKNAME);
			if (null != bankName) {
				response.setTransactionId((String) bankName);
			}

			Object paymentMode = jObject.get(Constants.PAYMENTMODE);
			if (null != paymentMode) {
				response.setTransactionId((String) paymentMode);
			}

		} catch (ParseException parseException) {
			logger.error("Exception", parseException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					parseException, "Unable to parse PaytmResponse ");
		}

		return response;
	}
}
