/**
 * 
 */
package com.kbn.paytm;

/**
 * @author Surender
 *
 */
public enum PaymentMode
{
  NET_BANKING,  CREDIT_CARD,  DEBIT_CARD, WALLET;
  
  private PaymentMode() {}
}
