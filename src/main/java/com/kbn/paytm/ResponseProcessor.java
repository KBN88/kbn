package com.kbn.paytm;

import java.util.TreeMap;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.Processor;

/**
 * @author Sunil
 *
 */
public class ResponseProcessor implements Processor {

	public void preProcess(Fields fields) throws SystemException {

		// Checksum Calculator 
		String checksum =fields.get(Constants.CHECKSUMHASH); 
		fields.remove(Constants.CHECKSUMHASH);
		TreeMap<String, String> treeMap = new TreeMap<String, String>(
				fields.getFields());
	//	CheckSumServiceHelper checkSumServiceHelper = CheckSumServiceHelper
	//			.getCheckSumServiceHelper();
		PropertiesManager propertiesManager = new PropertiesManager();
		String paytmMerchantkey = propertiesManager.getSystemProperty("PaytmMerchantKey");
		try {
	/*		boolean isValidChecksum = checkSumServiceHelper.verifycheckSum (paytmMerchantkey, treeMap, checksum);
			if (!isValidChecksum) {
				throw new SystemException(ErrorType.SIGNATURE_MISMATCH,	"Invalid signature");
			}*/
		} catch (Exception e) {
			// catch block
			throw new SystemException(ErrorType.SIGNATURE_MISMATCH,	"Invalid signature");
		}
	}

	public void process(Fields fields) throws SystemException {		
				
		mapPaytmResponse(fields);
		
		//If response contains TXN_ID, update database
		if(null != fields.get(Constants.ORDERID)){
			fields.updateForAuthorization();
		}		
	}

	public void postProcess(Fields fields) throws SystemException {
	}
	
	public void mapPaytmResponse(Fields fields){
		//Note: Not all response parameters from Paytm are needed to store/processed in response
		//Consider mapping only those parameters which we need to process for subsequent transactions like refund
		//pgRespCode is part of Signature check, no need to validate
		
		String pgResponseCode = fields.get(Constants.RESPCODE);		
		if(null != pgResponseCode){
			fields.put(FieldType.PG_RESP_CODE.getName(), pgResponseCode);
		}
		
		String txMessage = fields.get(Constants.RESPMSG);
		if(null != txMessage){
			//TODO: Validate and then assign
			fields.put(FieldType.PG_TXN_MESSAGE.getName(), txMessage);
		}
		
		String authCode = "null";
		if(null != authCode){
			fields.put(FieldType.AUTH_CODE.getName(), authCode);
		}
		
		String txStatus = fields.get(Constants.STATUS);
		if(null != txStatus){
			fields.put(FieldType.PG_TXN_STATUS.getName(), txStatus);
		}
		
		String txGateway = fields.get(Constants.GATEWAYNAME);
		if(null != txGateway){
			fields.put(FieldType.PG_GATEWAY.getName(), txGateway);
		}
		
		String txId = fields.get(Constants.ORDERID);
		if(null != txId){
			fields.put(FieldType.TXN_ID.getName(), txId);
		}
		
		String transactionId = fields.get(Constants.TXNID);
		if(null != transactionId){
			fields.put(FieldType.PG_REF_NUM.getName(), transactionId);
		}
		
		String pgTxnNo = fields.get(Constants.TXNID);
		if(null != pgTxnNo){
			fields.put(FieldType.ACQ_ID.getName(), pgTxnNo);
		}
		
		String issuerRefNum = fields.get(Constants.BANKTXNID);
		if(null != issuerRefNum){
			fields.put(FieldType.RRN.getName(), issuerRefNum);
		}
		
		String responseDateTime = fields.get(Constants.TXNDATE);
		if(null != issuerRefNum){
			fields.put(FieldType.PG_DATE_TIME.getName(), responseDateTime);
		}
		//Note: By default sale is choosen as type of transaction for all merchants
		//TODO: Update to support auth-capture model
		fields.put(FieldType.TXNTYPE.getName(), TransactionType.SALE.getName());
		
		updateInternalFields(fields);
	}
	
	public void updateInternalFields(Fields fields){
		String pgResponseCode = fields.get(Constants.RESPCODE);	
		if(null == pgResponseCode){
			//By default "rejected by bank" - Ideally this should be a non-reachable code
			pgResponseCode = "1"; 
		}

		ErrorType error = Mapper.getErrorType(pgResponseCode);
		fields.put(FieldType.RESPONSE_CODE.getName(), error.getCode());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), error.getResponseMessage());
		
		StatusType status = Mapper.getStatusType(pgResponseCode);
		fields.put(FieldType.STATUS.getName(), status.getName());
	}
}
