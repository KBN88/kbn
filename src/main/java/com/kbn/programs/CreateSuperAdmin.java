package com.kbn.programs;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.PermissionType;
import com.kbn.commons.user.Permissions;
import com.kbn.commons.user.Roles;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.SaltFactory;
import com.kbn.commons.util.SaltFileManager;
import com.kbn.commons.util.TransactionManager;
import com.kbn.commons.util.UserStatusType;

public class CreateSuperAdmin {
		
	private static Logger logger = Logger.getLogger(CreateSuperAdmin.class.getName());
	private static UserDao userDao = new UserDao();	
	private static SaltFileManager saltFileManager = new SaltFileManager();

	public static void main(String[] args) {
		
	//	 timer1.scheduleAtFixedRate(test, 10000, 1000);
		createSuperAdmin();
		createAdmin();
		createFSSAcq();
		createCitrusAcq();
		createPaytmAcq();	
		createBarclayAcquirer();
		createDirecpayAcquirer();

		createAMEXAcquirer();
		createMobikwikAcquirer();
		createAmexEzeeClickAcquirer();
		createYesbankAcquirer();
		createKotakAcquirer();
		
		createSafexPay();
		createCyberSource();
		createEPayLater();
		 createSBI();
		createISGPAY();
		createBILLDESK();
		createAxixBankAcq();
		createGPAYAcq();
		createAtomAcq();
	}
	public static void createSuperAdmin(){
		String salt = SaltFactory.generateRandomSalt();
		User user = new User();
		user.setPassword(hasher(salt));
		user.setUserType(UserType.SUPERADMIN);
		user.setUserStatus(UserStatusType.ACTIVE);
		user.setPayId(TransactionManager.getNewTransactionId());
		user.setBusinessName("SuperAdmin");
		user.setCompanyName("KBN payment Solutions");
		user.setFirstName("KBN");
		user.setLastName("Payments communication");
		user.setEmailId("superadmin@asiancheckout.com");
		user.setContactPerson("Neeraj");
		user.setAddress("105 Competent House, F - 14,Middle Circle");
		user.setCity("Delhi");
		user.setState("Delhi");
		user.setCountry("India");
		user.setWebsite("www.kbn.com");
        userDao.create(user);
		boolean isSaltInserted = saltFileManager.insertSalt(user.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(user);				
		}	
	}
	
	
	public static void createAdmin() {
		String salt = SaltFactory.generateRandomSalt();
		User user = new User();
		Permissions permission1 = new Permissions();
		Permissions permission2 = new Permissions();
		Permissions permission3 = new Permissions();
		Permissions permission4 = new Permissions();
		
		Set<Permissions> permissions = new HashSet<Permissions>();
		
		permission1.setPermissionType(PermissionType.CREATEUSER);
		permission2.setPermissionType(PermissionType.DELETEUSER);
		permission3.setPermissionType(PermissionType.LOGIN);
		permission4.setPermissionType(PermissionType.VIEW_REPORTS);
		permissions.add(permission1);
		permissions.add(permission2);
		permissions.add(permission3);
		permissions.add(permission4);
		
		Set<Roles> roles = new HashSet<Roles>();
		Roles role = new Roles();
		
		role.setPermissions(permissions);
		role.setName(UserType.SUPERADMIN.name());
		roles.add(role);
		
		user.setRoles(roles);
		user.setPassword(hasher(salt));
		user.setUserType(UserType.ADMIN);
		user.setUserStatus(UserStatusType.ACTIVE);
		user.setPayId(TransactionManager.getNewTransactionId());
		user.setBusinessName("KBN Admin");
		user.setCompanyName("KBN payment Solutions");
		user.setFirstName("KBN");
		user.setLastName("Payments");
		user.setEmailId("admin@asiancheckout.com");
		user.setContactPerson("Neeraj");
		user.setAddress("C-94 Saraswati Kunj");
		user.setCity("Gurgaon");
		user.setState("Haryana");
		user.setCountry("India");
		user.setWebsite("www.kbn.com");
		
			
		userDao.create(user);
		
		boolean isSaltInserted = saltFileManager.insertSalt(user.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(user);				
		}	
	}
	
	public static void createFSSAcq(){
		String salt = SaltFactory.generateRandomSalt();
		User fss = new User();
		
		fss.setEmailId("hdfc@mmadpay.com");
		fss.setUserStatus(UserStatusType.ACTIVE);
		fss.setUserType(UserType.ACQUIRER);
		fss.setPayId("1");
		fss.setPassword(hasher(salt));
		fss.setFirstName("FSS");
		fss.setBusinessName("HDFC Bank");
		
		userDao.create(fss);	
		boolean isSaltInserted = saltFileManager.insertSalt(fss.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(fss);				
		}	
		
	}
	
	public static void createCitrusAcq(){
		User citrus_pay = new User();
		String salt = SaltFactory.generateRandomSalt();
		
		citrus_pay.setEmailId("citrus@mmadpay.com");
		citrus_pay.setUserStatus(UserStatusType.ACTIVE);
		citrus_pay.setUserType(UserType.ACQUIRER);
		citrus_pay.setPayId("2");
		citrus_pay.setPassword(hasher(salt));
		citrus_pay.setFirstName("CITRUS");
		citrus_pay.setBusinessName("Yes Bank");
			
		userDao.create(citrus_pay);
		boolean isSaltInserted = saltFileManager.insertSalt(citrus_pay.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(citrus_pay);				
		}
		
	}
	
	public static void createPaytmAcq(){
		String salt = SaltFactory.generateRandomSalt();
		User paytm = new User();
		
		paytm.setEmailId("paytm@mmadpay.com");
		paytm.setUserStatus(UserStatusType.ACTIVE);
		paytm.setUserType(UserType.ACQUIRER);
		paytm.setPayId("3");
		paytm.setPassword(hasher(salt));
		paytm.setFirstName("PAYTM");
		paytm.setBusinessName("WALLET PAYTM");
		
		userDao.create(paytm);
		boolean isSaltInserted = saltFileManager.insertSalt(paytm.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(paytm);				
		}
	}
	
	public static void createBarclayAcquirer(){
		String salt = SaltFactory.generateRandomSalt();
		User barclay = new User();
		
		barclay.setEmailId("barclay@mmadpay.com");
		barclay.setUserStatus(UserStatusType.ACTIVE);
		barclay.setUserType(UserType.ACQUIRER);
		barclay.setPayId("4");
		barclay.setPassword(hasher(salt));
		barclay.setFirstName("BARCLAY");
		barclay.setBusinessName("Barclay Bank");
		
		userDao.create(barclay);
		boolean isSaltInserted = saltFileManager.insertSalt(barclay.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(barclay);				
		}
	}
	
	public static void createDirecpayAcquirer(){
		String salt = SaltFactory.generateRandomSalt();
		User direcpay = new User();
		
		direcpay.setEmailId("direcpay@mmadpay.com");
		direcpay.setUserStatus(UserStatusType.ACTIVE);
		direcpay.setUserType(UserType.ACQUIRER);
		direcpay.setPayId("5");
		direcpay.setPassword(hasher(salt));
		direcpay.setFirstName("DIRECPAY");
		direcpay.setBusinessName("DIRECPAY");
		
		userDao.create(direcpay);
		boolean isSaltInserted = saltFileManager.insertSalt(direcpay.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(direcpay);				
		}
	}
	
	public static void createAMEXAcquirer(){
		String salt = SaltFactory.generateRandomSalt();
		User amex = new User();
		
		amex.setEmailId("amex@kbn.com");
		amex.setUserStatus(UserStatusType.ACTIVE);
		amex.setUserType(UserType.ACQUIRER);
		amex.setPayId("6");
		amex.setPassword(hasher(salt));
		amex.setFirstName("AMEX");
		amex.setBusinessName("AMERICAN EXPRESS");
		
		userDao.create(amex);
		boolean isSaltInserted = saltFileManager.insertSalt(amex.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(amex);				
		}
	}

	public static void createAmexEzeeClickAcquirer(){
		String salt = SaltFactory.generateRandomSalt();
		User amex = new User();
		
		amex.setEmailId("amexezeeclick@kbn.com");
		amex.setUserStatus(UserStatusType.ACTIVE);
		amex.setUserType(UserType.ACQUIRER);
		amex.setPayId("66");
		amex.setPassword(hasher(salt));
		amex.setFirstName("EZEECLICCK");
		amex.setBusinessName("AMEX EZEECLICK");
		
		userDao.create(amex);
		boolean isSaltInserted = saltFileManager.insertSalt(amex.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(amex);				
		}
	}

	public static void createMobikwikAcquirer(){
		String salt = SaltFactory.generateRandomSalt();
		User amex = new User();
		
		amex.setEmailId("mobikwik@kbn.com");
		amex.setUserStatus(UserStatusType.ACTIVE);
		amex.setUserType(UserType.ACQUIRER);
		amex.setPayId("7");
		amex.setPassword(hasher(salt));
		amex.setFirstName("MOBIKWIK");
		amex.setBusinessName("WALLET MOBIKWIK");
		
		userDao.create(amex);
		boolean isSaltInserted = saltFileManager.insertSalt(amex.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(amex);				
		}
	}

	public static void createYesbankAcquirer(){
		String salt = SaltFactory.generateRandomSalt();
		User yesbank = new User();
		
		yesbank.setEmailId("yesbank@kbn.com");
		yesbank.setUserStatus(UserStatusType.ACTIVE);
		yesbank.setUserType(UserType.ACQUIRER);
		yesbank.setPayId("8");
		yesbank.setPassword(hasher(salt));
		yesbank.setFirstName("YESBANK");
		yesbank.setBusinessName("NETBANKING YES Bank");
		
		userDao.create(yesbank);
		boolean isSaltInserted = saltFileManager.insertSalt(yesbank.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(yesbank);				
		}
	}
	public static void createKotakAcquirer(){
		String salt = SaltFactory.generateRandomSalt();
		User kotak = new User();
		
		kotak.setEmailId("kotak@kbn.com");
		kotak.setUserStatus(UserStatusType.ACTIVE);
		kotak.setUserType(UserType.ACQUIRER);
		kotak.setPayId("9");
		kotak.setPassword(hasher(salt));
		kotak.setFirstName("KOTAK");
		kotak.setBusinessName("NETBANKING KOTAK Bank");
		
		userDao.create(kotak);
		boolean isSaltInserted = saltFileManager.insertSalt(kotak.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(kotak);				
		}
	}
	private static void createAtomAcq() {
		String salt = SaltFactory.generateRandomSalt();
		User atom = new User();
		
		atom.setEmailId("atom@kbn.com");
		atom.setUserStatus(UserStatusType.ACTIVE);
		atom.setUserType(UserType.ACQUIRER);
		atom.setPayId("21");
		atom.setPassword(hasher(salt));
		atom.setFirstName("ATOM");
		atom.setBusinessName("ATOM");
		
		userDao.create(atom);	
		boolean isSaltInserted = saltFileManager.insertSalt(atom.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(atom);				
		}			
	}
	// GPAY UPI 
	private static void createGPAYAcq() {
		String salt = SaltFactory.generateRandomSalt();
		User gpay = new User();
		
		gpay.setEmailId("gpay@kbn.com");
		gpay.setUserStatus(UserStatusType.ACTIVE);
		gpay.setUserType(UserType.ACQUIRER);
		gpay.setPayId("20");
		gpay.setPassword(hasher(salt));
		gpay.setFirstName("GPAY");
		gpay.setBusinessName("GPAY");
		
		userDao.create(gpay);	
		boolean isSaltInserted = saltFileManager.insertSalt(gpay.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(gpay);				
		}			
	}
	private static void createAxixBankAcq() {
		String salt = SaltFactory.generateRandomSalt();
		User airtelBank = new User();
		
		airtelBank.setEmailId("axix@kbn.com");
		airtelBank.setUserStatus(UserStatusType.ACTIVE);
		airtelBank.setUserType(UserType.ACQUIRER);
		airtelBank.setPayId("19");
		airtelBank.setPassword(hasher(salt));
		airtelBank.setFirstName("AXISBANKUPI");
		airtelBank.setBusinessName("AXIS Bank UPI");
		
		userDao.create(airtelBank);	
		boolean isSaltInserted = saltFileManager.insertSalt(airtelBank.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(airtelBank);				
		}	
		
	}
	
	private static void createCyberSource() {
		String salt = SaltFactory.generateRandomSalt();
		User cyberSource = new User();
		cyberSource.setEmailId("cybersource@kbn.com");
		cyberSource.setUserStatus(UserStatusType.ACTIVE);
		cyberSource.setUserType(UserType.ACQUIRER);
		cyberSource.setPayId("17");
		cyberSource.setPassword(hasher(salt));
		cyberSource.setFirstName("CYBERSOURCE");
		cyberSource.setBusinessName("CyberSource");
		userDao.create(cyberSource);	
		boolean isSaltInserted = saltFileManager.insertSalt(cyberSource.getPayId(), salt);
		if(!isSaltInserted){
		// Rollback user creation				
			  userDao.delete(cyberSource);				
		}
		
	}

	private static void createSafexPay() {
		String salt = SaltFactory.generateRandomSalt();
		User safexPay = new User();
		safexPay.setEmailId("safexpay@kbn.com");
		safexPay.setUserStatus(UserStatusType.ACTIVE);
		safexPay.setUserType(UserType.ACQUIRER);
		safexPay.setPayId("16");
		safexPay.setPassword(hasher(salt));
		safexPay.setFirstName("SAFEXPAY");
		safexPay.setBusinessName("SafexPay");
		userDao.create(safexPay);	
		boolean isSaltInserted = saltFileManager.insertSalt(safexPay.getPayId(), salt);
		if(!isSaltInserted){
		// Rollback user creation				
			  userDao.delete(safexPay);				
		}
		
	}

	private static void createSBI() {
		String salt = SaltFactory.generateRandomSalt();
		User sbi = new User();
		sbi.setEmailId("sbi@kbn.com");
		sbi.setUserStatus(UserStatusType.ACTIVE);
		sbi.setUserType(UserType.ACQUIRER);
		sbi.setPayId("15");
		sbi.setPassword(hasher(salt));
		sbi.setFirstName("SBI");
		sbi.setBusinessName("SBI");
		userDao.create(sbi);	
		boolean isSaltInserted = saltFileManager.insertSalt(sbi.getPayId(), salt);
		if(!isSaltInserted){
		// Rollback user creation				
			  userDao.delete(sbi);				
		}
		
	}

	private static void createISGPAY() {
		String salt = SaltFactory.generateRandomSalt();
		User isgPay = new User();
		isgPay.setEmailId("isgpay@kbn.com");
		isgPay.setUserStatus(UserStatusType.ACTIVE);
		isgPay.setUserType(UserType.ACQUIRER);
		isgPay.setPayId("13");
		isgPay.setPassword(hasher(salt));
		isgPay.setFirstName("ISGPAY");
		isgPay.setBusinessName("ISGPAY");
		userDao.create(isgPay);	
		boolean isSaltInserted = saltFileManager.insertSalt(isgPay.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(isgPay);				
		}
		
	}
	private static void createEPayLater() {
		
		String salt = SaltFactory.generateRandomSalt();
		User ePayLater = new User();
		ePayLater.setEmailId("ePayLater@kbn.com");
		ePayLater.setUserStatus(UserStatusType.ACTIVE);
		ePayLater.setUserType(UserType.ACQUIRER);
		ePayLater.setPayId("14");
		ePayLater.setPassword(hasher(salt));
		ePayLater.setFirstName("EPAYLATER");
		ePayLater.setBusinessName("WALLET EPAYLATER");
		userDao.create(ePayLater);	
		boolean isSaltInserted = saltFileManager.insertSalt(ePayLater.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(ePayLater);				
		}
		
	}
	
	private static void createBILLDESK() {
		String salt = SaltFactory.generateRandomSalt();
	     User billdesk = new User();
	     billdesk.setEmailId("billdesk@billdesk.com");
	     billdesk.setUserStatus(UserStatusType.ACTIVE);
	     billdesk.setUserType(UserType.ACQUIRER);
	     billdesk.setPayId("11");
	     billdesk.setPassword(hasher(salt));
	     billdesk.setFirstName("BILLDESK");
	     billdesk.setBusinessName("BILL DESK");
		userDao.create(billdesk);	
		boolean isSaltInserted = saltFileManager.insertSalt(billdesk.getPayId(), salt);
		if(!isSaltInserted){
			//  Rollback user creation				
			  userDao.delete(billdesk);				
		}
		
	}

	
	public static String hasher(String salt) {
		String password = null;
		try {
			password = Hasher.getHash("Test@123".concat(salt));
		} catch (SystemException systemException) {
			logger.error("Error Creating hash for password", systemException);
		}
		return password;
	}

}
