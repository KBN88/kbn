
package com.kbn.programs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.transaction.SystemException;

import com.kbn.commons.crypto.Scrambler;
import com.kbn.commons.dao.KeysDao;
import com.kbn.commons.util.PropertiesManager;

/**
 * @author Surender
 *
 */
public class KeyManager {

	private final Scanner scanner = new Scanner(System.in);
	
	public KeyManager() {
	}
	
	public static void main(String[] args) throws SystemException {
		
		KeyManager keyManager = new KeyManager();
		keyManager.display("Enter your choice:");
		keyManager.display("1 : Add new Key");
		
		int choice = keyManager.getScanner().nextInt();
		switch(choice){
		case 1: 
			keyManager.addKey();
		}
	}
	
	//to read the key entered by the user
	public String getInput() throws SystemException{
		String result = null;
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		try{
			result = bufferedReader.readLine();
		} catch(IOException ioException){
			ioException.printStackTrace();
		}
		if(result.length()!=8){
			throw new SystemException("Invalid key length");
		}
		return result;
	}
	
	public void display(String message){
		System.out.println(message);
	}

	private void addKey() throws SystemException{
		display("Enter Key ID (It should be a number in range 1 to 100):");
		int keyId = getScanner().nextInt();
		
		addFileKey(keyId);
		addDataBaseKey(keyId);
	}

	private void addDataBaseKey(int keyId) throws SystemException {
		display("Enter key text for Data base:");
		String text = getInput();
		
		Scrambler scrambler = new Scrambler((new PropertiesManager()).getSystemProperty("KEK"));
		text = scrambler.encrypt(text);
		
		//Save in database
		KeysDao keysDao = new KeysDao();
		if(!keysDao.insertKey(keyId, text)){
			display("Unable to insert key in database");
		}
	}

	private void addFileKey(int keyId) throws SystemException {
		display("Enter key text for file:");
		String text = getInput();
		
		Scrambler scrambler = new Scrambler((new PropertiesManager()).getSystemProperty("KEK"));
		text = scrambler.encrypt(text);
		
		(new PropertiesManager()).setKey(Integer.toString(keyId), text);
	}

	public Scanner getScanner() {
		return scanner;
	}
}
