package com.kbn.epaylater;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class StatusTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(StatusTransactionProcessor.class.getName());
	
	private	TransactionConverter converter = new TransactionConverter();		
	private	TransactionCommunicator communicator = new TransactionCommunicator();
	@Override
	public void transact(Fields fields) throws SystemException {
		String request = "";
		try {
			request = converter.createStatusRequest(fields);
		} catch (Exception exception) {
			logger.error("EPayLater Status Converter " + exception);
		}
		String statusResponse = "";
		try {
			statusResponse = communicator.transactSatusApi(request, ConfigurationConstants.EPAYLATER_STATUS_URL.getValue()+fields.get(FieldType.TXN_ID.getName()));
			logger.info("EPayLater Status Enquiry Response:-" + statusResponse);
		} catch (Exception exception) {
			logger.error("EPayLater Status Communicator " + exception);
		}

		try {
			EPayLaterTransformer transformer = new EPayLaterTransformer(statusResponse);
			transformer.updateEpayLaterStatusResponse(fields);
			fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));

		} catch (Exception exception) {
			logger.error("EPayLater Status Transformer " + exception);
		}

	}
}
