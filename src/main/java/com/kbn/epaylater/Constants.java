package com.kbn.epaylater;

public class Constants {
	public static final String OPENING_BRACE = "{";
	public static final String CLOSING_BRACE = "}";
	public static final String CHECKSUMHASH = "checksum";
	public static final String EQUATOR = "=";
	public static final String AND_SEPRATOR = "&";
	public static final String QUESTION_MARK = "?";
	public static final String OPENING_SQUARE_BRACE = "[";
	public static final String CLOSING_SQUARE_BRACE = "]";
	public static final String COLON = ":";
	public static final String REDIRECT_TYPE = "redirectType";
	public static final String MARKETPALACE_ORDERID = "marketplaceOrderId";
	public static final String MERCHANT_ORDERID = "merchantOrderId";
	public static final String M_CODE = "mCode";
	public static final String CALL_BACK_URL = "callbackUrl";
	public static final String CUSTOMER_EMAIL_VERIFIED = "customerEmailVerified";
	public static final String CUSTOMER_TELEPHONE_NUMBER_VERIFIED = "customerTelephoneNumberVerified";
	public static final String CUSTOMER_LOGGEDIN = "customerLoggedin";
	public static final String AMOUNT = "amount";
	public static final String RETURN_AMOUNT = "returnAmount";

	public static final String RETURN_ACCEPTED_DATE = "returnAcceptedDate";
	public static final String RETURN_SHIPMENT_RECEIVED_DATE = "returnShipmentReceivedDate";
	public static final String REFUND_DATE = "refundDate";
	public static final String RETURN_TYPE = "returnType";

	public static final String CURRENCY_CODE = "currencyCode";
	public static final String DATE = "date";
	public static final String CATEGORY = "category";
	public static final String CUSTOMER = "customer";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String EMAIL_ADDRESS = "emailAddress";
	public static final String TELEPHONE_NUMBER = "telephoneNumber";

	public static final String DEVICE = "device";
	public static final String DEVICE_TYPE = "deviceType";
	public static final String DEVICE_CLIENT = "deviceClient";
	public static final String DEVICE_NUMBER = "deviceNumber";

	public static final String ADDRESS = "address";
	public static final String LINE1 = "line1";
	public static final String LINE2 = "line2";
	public static final String LINE3 = "line3";
	public static final String CITY = "city";
	public static final String POST_CODE = "postcode";

	public static final String MEMBER_SINCE = "memberSince";
	public static final String MARKET_PLACE_CUSTOMER_ID = "marketplaceCustomerId";

	public static final String MERCHANT = "merchant";
	public static final String MARKET_PALCE_MERCHANT_ID = "marketplaceMerchantId";
	public static final String NAME = "name";
	public static final String MERCHANT_PALCE_SPECIFIC_SECTION = "marketplaceSpecificSection";
	public static final String ORDER_HISTORY = "orderHistory";
	public static final String ORDER_ID = "orderId";
	public static final String PAYMENT_METHOD = "paymentMethod";
	public static final String RETURNED = "returned";

}
