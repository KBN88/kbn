package com.kbn.epaylater;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EpayLaterEncryptDecryptUtil {

	public static String createChecksum(String input) throws IOException, NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(input.getBytes("UTF-8"));
		byte[] hash = md.digest();
		StringBuilder hexString = new StringBuilder();
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xff & hash[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}
		return hexString.toString().toUpperCase();
	}

	public static String encryptAES256AndBase64(String encryptionKey, String iv, String jsonBody) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv.getBytes("UTF-8")));
		byte[] encbyte = cipher.doFinal(jsonBody.getBytes("UTF-8"));
		return Base64.getEncoder().encodeToString(encbyte);
	}

	public static String decryptBase64EncodedAES256(String encryptionKey, String iv, String inputParam)
			throws Exception {
		byte[] decodedInput = Base64.getDecoder().decode(inputParam);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
		SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv.getBytes("UTF-8")));
		return new String(cipher.doFinal(decodedInput), "UTF-8");
	}
	 public static void main(String[] args) throws Exception {    
		final String encryptionKey = "9378C9DBF13F2A7C8A782849175F085C";  
		final String iv = "981C3CB3945F9505";   
		final String jsonBody = "abc";
	    String checksum = createChecksum(jsonBody); 
	    System.out.println(checksum);//BA7816BF8F01CFEA414140DE5DAE2223B00361A396177A9CB410FF61F20015AD
	    String encdata = encryptAES256AndBase64(encryptionKey, iv, jsonBody); 
	    System.out.println(encdata);//ISSIswPGau201AyJBKPdgg==
	    System.out.println(decryptBase64EncodedAES256(encryptionKey, iv,encdata));//{'key':'val'} 
	 }
}