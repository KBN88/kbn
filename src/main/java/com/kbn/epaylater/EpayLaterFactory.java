package com.kbn.epaylater;

import com.kbn.pg.core.Processor;

public class EpayLaterFactory {
	
	public static Processor getInstance(){
		return new EpayLaterProcessor();
	}

}
