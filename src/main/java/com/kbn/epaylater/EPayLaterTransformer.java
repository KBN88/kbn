package com.kbn.epaylater;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
public class EPayLaterTransformer {

	private String responseMap;

	public EPayLaterTransformer(String responseField) {
		this.responseMap = responseField;

	}

	public ErrorType getSaleResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("EPL0000")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("EPL0020")) {
			errorType = ErrorType.REJECTED;
		} else if (respCode.equals("EPL0014")) {
			errorType = ErrorType.AUTHENTICATION_UNAVAILABLE;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getSaleStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("EPL0000")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("EPL0020")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("EPL0014")) {
			status = StatusType.AUTHENTICATION_FAILED;
		}else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public ErrorType getRefundResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("returned")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("N")) {
			errorType = ErrorType.REJECTED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getRefundEnquriyStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("returned")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("N")) {
			status = StatusType.REJECTED;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public ErrorType getResponse(String respCode, String authStatus) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("Y") && authStatus.equals("0300")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("Y") && authStatus.equals("0002")) {
			errorType = ErrorType.DECLINED;
		} else if (respCode.equals("Y") && authStatus.equals("0399")) {
			errorType = ErrorType.REJECTED;
		} else if (respCode.equals("Y") && authStatus.equals("0001")) {
			errorType = ErrorType.REJECTED;
		} else if (respCode.equals("N")) {
			errorType = ErrorType.REJECTED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getStatus(String respCode, String authStatus) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Y") && authStatus.equals("0300")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("Y") && authStatus.equals("0399")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("Y") && authStatus.equals("0002")) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Y") && authStatus.equals("0001")) {
			status = StatusType.ERROR;
		} else if (respCode.equals("N")) {
			status = StatusType.ERROR;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public void updateEpayLaterResponse(Fields fields) throws ParseException, SystemException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(responseMap);
		JSONObject jObject = (JSONObject) obj;
		StatusType status = getSaleStatus(jObject.get("statusCode").toString());
		ErrorType errorType = getSaleResponse(jObject.get("statusCode").toString());
		String pgRef = null;
		
		if(jObject.get("eplOrderId")!=null) {
		 pgRef = jObject.get("eplOrderId").toString();

		}
		String epayLaterCode = jObject.get("statusCode").toString();
		if(epayLaterCode.equals("EPL0000")) {
			TransactionCommunicator communicator = new TransactionCommunicator();
			StringBuilder request = new StringBuilder();
			epayLaterConferm(fields,request,pgRef);
			StringBuilder jsonRequest = new StringBuilder();
			jsonRequest.append(Constants.OPENING_BRACE);
			jsonRequest.append(request);
			jsonRequest.append(Constants.CLOSING_BRACE);
			String response = communicator.transactConfirmed(jsonRequest.toString(), ConfigurationConstants.EPAYLATER_TRANSACTION_CONFIRM_URL.getValue()+pgRef+"/confirmed"+"/"+fields.get(FieldType.TXN_ID.getName())+"?"+"delivered=true");
			//EPayLaterTransformer transformer = new EPayLaterTransformer(response);
			System.out.println(response);
			
		}

		//{"mCode":"mmadpay","marketplaceOrderId":"1811221717491018","eplOrderId":"637274","amount":"100.00","currencyCode":"INR","category":"FOOD","status":"Failure","statusDesc":"Sorry this transaction is not eligible for ePayLater","statusCode":"EPL0014"}
		//{"marketplaceOrderId":"1811221357431001","amount":"100.00","statusDesc":"Cancelled by user","eplOrderId":null,"mCode":"mmadpay","category":"FOOD","currencyCode":"INR","status":"Failure","statusCode":"EPL0020"}
	//	fields.put(FieldType.MERCHANT_ID.getName(), jObject.get("MerchantId").toString());
		fields.put(FieldType.TXN_ID.getName(), jObject.get("marketplaceOrderId").toString());
		//fields.put(FieldType.PG_DATE_TIME.getName(), jObject.get("date").toString());
		fields.put(FieldType.STATUS.getName(), status.getName());
		
		fields.put(FieldType.PG_REF_NUM.getName(), pgRef);
		fields.put(FieldType.PG_RESP_CODE.getName(), jObject.get("status").toString());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), jObject.get("statusDesc").toString());
		//fields.put(FieldType.AUTH_CODE.getName(), jObject.get("AuthCode").toString());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
	}

	private void epayLaterConferm(Fields fields, StringBuilder request ,String pgRef) {
		Date now = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		appendJsonField("id",pgRef, request);
		appendJsonField(Constants.RETURN_AMOUNT,fields.get(FieldType.AMOUNT.getName()), request);
		appendJsonField(Constants.CURRENCY_CODE,"INR", request);
		appendJsonField(Constants.DATE,dateFormat.format(now), request);
		appendJsonField("paylater",true, request);
		appendJsonField("status","delivered", request);
		appendJsonField(Constants.MARKETPALACE_ORDERID,fields.get(FieldType.TXN_ID.getName()), request);
		
	}

	public void updateEpayLaterRefundResponse(Fields fields) {
		//{"type":"epaylater","reason":"operationNotAllowed","code":"400","message":"Status update to Returned not allowed on current Status CustomerAgreed","eplErrorCode":"EPL1021"}
		//{"id":637637,"amount":100.00000,"currencyCode":"INR","date":"2018-11-27T08:30:05.563Z","paylater":true,"status":"returned","marketplaceOrderId":"1811271356311001"}
		//{"type":"epaylater","reason":"operationNotAllowed","code":"400","message":"Status update to Returned not allowed on current Status CustomerAgreed","eplErrorCode":"EPL1021"}
		//{"id":637611,"amount":100.00000,"currencyCode":"INR","date":"2018-11-27T07:29:33.288Z","paylater":true,"status":"cancelled","marketplaceOrderId":"1811271255551001"}
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(responseMap);
		} catch (ParseException exception) {
			exception.printStackTrace();
		}
		JSONObject jObject = (JSONObject) obj;
		StatusType status = getRefundEnquriyStatus(jObject.get("status").toString());
		ErrorType errorType = getRefundResponse(jObject.get("status").toString());
		
		fields.put(FieldType.PG_REF_NUM.getName(), jObject.get("id").toString());
		fields.put(FieldType.PG_DATE_TIME.getName(), jObject.get("date").toString());
		fields.put(FieldType.STATUS.getName(), status.getName());
		
		fields.put(FieldType.PG_RESP_CODE.getName(),jObject.get("status").toString());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), jObject.get("status").toString());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
	
}

public void appendJsonField(String name, String value, StringBuilder request) {
	if(null == value || value.isEmpty()){
		return;
	}
	if(request.length() > 0){
		//Append comma
		request.append(',');
	}

	//Append name
	request.append('"');
	request.append(name);
	request.append('"');

	//Append colon
	request.append(':');

	//Append value
	request.append('"');
	request.append(value);
	request.append('"');
}

public void appendJsonField(String name, boolean flag, StringBuilder request) {

	if(request.length() > 0){
		//Append comma
		request.append(',');
	}
	//Append name
	request.append('"');
	request.append(name);
	request.append('"');
	//Append colon
		request.append(':');
	//Append value
	request.append(flag);
}

public void updateEpayLaterStatusResponse(Fields fields) {
	// TODO Auto-generated method stub
	
}
}
