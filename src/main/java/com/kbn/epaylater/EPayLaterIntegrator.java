package com.kbn.epaylater;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.AbstractTransactionProcessorFactory;
import com.kbn.pg.core.TransactionProcessor;

/**
 * @author neeraj
 *
 */
public class EPayLaterIntegrator {
/*	private TransactionConverter converter = new TransactionConverter();
	private TransactionCommunicator communicator = new TransactionCommunicator();
	private EPayLaterTransformer ePayLaterTransformer = null;*/
	private CryptoManager cryptoManager = CryptoManagerFactory.getCryptoManager();
	private AbstractTransactionProcessorFactory transactionProcessorFactory = new EPayLaterTransactionProcessorFactory();

	public void process(Fields fields) throws SystemException {

		addDefaultFields(fields);
		
		send(fields);	
				
		cryptoManager.secure(fields);
	}//process

	private void send(Fields fields) throws SystemException {
		TransactionProcessor transactionProcessor = transactionProcessorFactory.getInstance(fields);
		transactionProcessor.transact(fields);
		
	}

	public static void addDefaultFields(Fields fields) {

	}
}
