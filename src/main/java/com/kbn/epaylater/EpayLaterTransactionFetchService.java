package com.kbn.epaylater;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;

public class EpayLaterTransactionFetchService {

	private static Logger logger = Logger.getLogger(EpayLaterTransactionFetchService.class.getName());
	private static final String ipAddressCodeQuery = "select INTERNAL_CUST_IP from TRANSACTION where TXN_ID=?";

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public String fetchIpAddress(String txn_id) throws SystemException {
		String ipAddress = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(ipAddressCodeQuery)) {
				preparedStatement.setString(1, txn_id);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						ipAddress = resultSet.getString("INTERNAL_CUST_IP");
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching Auth Code for ISGPAY Refund Request " + sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return ipAddress;
	}

}
