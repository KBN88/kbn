package com.kbn.epaylater;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class SaleTransactionProcessor  implements TransactionProcessor{
	private static Logger logger = Logger.getLogger(SaleTransactionProcessor.class.getName());
	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();		
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createSaleTransaction(fields);
		logger.info("Request to EpayLater: " + request);
		communicator.sendAuthorization(request, fields);
		
	}

}
