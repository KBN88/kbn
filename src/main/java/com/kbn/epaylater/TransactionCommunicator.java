package com.kbn.epaylater;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.SystemConstants;

public class TransactionCommunicator {
	private static Logger logger = Logger.getLogger(TransactionCommunicator.class.getName());

	public void sendAuthorization(String request, Fields fields) throws SystemException {
		PrintWriter out;
		logger.info("Request sent to EpayLater: " + request);
		try {
			out = ServletActionContext.getResponse().getWriter();

			out.write(request);
			// Set response
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.SENT_TO_BANK.getName());

		} catch (IOException iOException) {
			logger.error(iOException);
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.INTERNAL_SYSTEM_ERROR.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.ERROR.getName());
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, iOException, "Network Exception with EpayLater");
		}
	}

	// Method to Post request to URL shortener API
	public String transactRefund(String request, String hostUrl) throws SystemException {
		String response = null;
		try {

			StringRequestEntity requestEntity = new StringRequestEntity(request, "application/json",
					SystemConstants.DEFAULT_ENCODING_UTF_8);

			PutMethod putMethod = new PutMethod(hostUrl+"/returned");

			putMethod.setRequestHeader("Content-Type", "application/json");
			putMethod.setRequestHeader("Authorization",ConfigurationConstants.EPAYLATER_AUTH_KEY.getValue());
			putMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			putMethod.setRequestEntity(requestEntity);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(putMethod);
			response = putMethod.getResponseBodyAsString();
		} catch (IOException ioException) {
			logger.error(ioException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with EpayLater for " + hostUrl.toString());
		}

		return response;
	}

	public String transactConfirmed(String request, String hostUrl) throws SystemException {
		String response = null;
		try {

			StringRequestEntity requestEntity = new StringRequestEntity(request, "application/json",
					SystemConstants.DEFAULT_ENCODING_UTF_8);

			PutMethod putMethod = new PutMethod(hostUrl);

			putMethod.setRequestHeader("Content-Type", "application/json");
			putMethod.setRequestHeader("Authorization",ConfigurationConstants.EPAYLATER_AUTH_KEY.getValue());
			putMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			putMethod.setRequestEntity(requestEntity);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(putMethod);
			response = putMethod.getResponseBodyAsString();
		} catch (IOException ioException) {
			logger.error(ioException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with EpayLater for " + hostUrl.toString());
		}

		return response;
	}

	public String transactSatusApi(String request, String hostUrl) throws SystemException {
		String response = null;
		try {
			GetMethod getMethod = new GetMethod(hostUrl+request);

			getMethod.setRequestHeader("Content-Type", "application/json");
			getMethod.setRequestHeader("Authorization",ConfigurationConstants.EPAYLATER_AUTH_KEY.getValue());
			getMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(getMethod);
			response = getMethod.getResponseBodyAsString();

		} catch (IOException ioException) {
			logger.error(ioException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with EpayLater for " + hostUrl.toString());
		}

		return response;
	}
}
