package com.kbn.epaylater;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Currency;
import com.kbn.pg.core.pageintegrator.Transaction;

public class TransactionConverter extends Transaction {
	UserDao userDao = new UserDao();

	public String createSaleTransaction(Fields fields) {
		String requestUrl = ConfigurationConstants.EPAYLATER_TRANSACTION_URL.getValue();
		String aeskey = ConfigurationConstants.EPAYLATER_AES_KEY.getValue();
		String iv = ConfigurationConstants.EPAYLATER_IV.getValue();
		StringBuilder request = new StringBuilder();
		appendMerchantInfo(fields, request);
		appendCustomerInfo(fields, request);
		deviceInfo(fields, request);
		// appendCustomerAddress(fields, request);
		appendMerchantrmation(fields, request);
		appendMarketplaceSpecificSection(fields, request);
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);
		String checkSumString = null;
		String encdata = null;
		try {
			checkSumString = EpayLaterEncryptDecryptUtil.createChecksum(jsonRequest.toString());
		} catch (NoSuchAlgorithmException | IOException exception) {
			exception.printStackTrace();
		}
		try {
			encdata = EpayLaterEncryptDecryptUtil.encryptAES256AndBase64(aeskey, iv, jsonRequest.toString());
		} catch (Exception exception) {

			exception.printStackTrace();
		}
		// Request
		StringBuilder request1 = new StringBuilder();
		request1.append("<HTML>");
		request1.append("<BODY OnLoad=\"OnLoadEvent();\" >");
		request1.append("<form name=\"form1\" action=\"");
		request1.append(requestUrl);
		request1.append("\" method=\"post\">");
		request1.append("<input type=\"hidden\" name=\"mcode\" value=\"");
		request1.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request1.append("\">");
		request1.append("<input type=\"hidden\" name=\"checksum\" value=\"");
		request1.append(checkSumString);
		request1.append("\">");
		request1.append("<input type=\"hidden\" name=\"encdata\" value=\"");
		request1.append(encdata);
		request1.append("\">");
		request1.append("</form>");
		request1.append("<script language=\"JavaScript\">");
		request1.append("function OnLoadEvent()");
		request1.append("{document.form1.submit();}");
		request1.append("</script>");
		request1.append("</BODY>");
		request1.append("</HTML>");
		return request1.toString();

		// return jsonRequest.toString();
	}

	private void appendMarketplaceSpecificSection(Fields fields, StringBuilder request) {
		Date date = new Date();
		Timestamp ts = new Timestamp(date.getTime());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		appendJsonField(Constants.MERCHANT_PALCE_SPECIFIC_SECTION, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.MARKET_PLACE_CUSTOMER_ID, fields.get(FieldType.CUST_PHONE.getName()), request);
		appendJsonField(Constants.MEMBER_SINCE, formatter.format(ts), request);
		request.append(Constants.CLOSING_BRACE);
	}

	private void appendMerchantrmation(Fields fields, StringBuilder request) {
		User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
		appendJsonField(Constants.MERCHANT, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.MARKET_PALCE_MERCHANT_ID, fields.get(FieldType.PAY_ID.getName()), request);
		appendJsonField(Constants.NAME, user.getBusinessName(), request);
		appendJsonField(Constants.TELEPHONE_NUMBER, user.getMobile(), request);
		request.append(",");
		appendJsonField(Constants.ADDRESS, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.LINE1, user.getAddress(), request);
		appendJsonField(Constants.CITY, user.getCity(), request);
		appendJsonField(Constants.POST_CODE, user.getPostalCode(), request);
		request.append(Constants.CLOSING_BRACE);
		request.append(Constants.CLOSING_BRACE);
		request.append(",");
	}

	private void deviceInfo(Fields fields, StringBuilder request) {
		EpayLaterTransactionFetchService epayLaterTransactionFetchService = new EpayLaterTransactionFetchService();
		String ip = null;
		try {
			ip = epayLaterTransactionFetchService.fetchIpAddress(fields.get(FieldType.INTERNAL_ORIG_TXN_ID.getName()));
		} catch (SystemException exception) {
			exception.printStackTrace();
		}
		appendJsonField(Constants.DEVICE, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.DEVICE_TYPE, "DESKTOP", request);
		appendJsonField(Constants.DEVICE_CLIENT, "Chrome/41.0.2228.0", request);
		appendJsonField(Constants.DEVICE_NUMBER, ip, request);
		request.append(Constants.CLOSING_BRACE);
		request.append(",");
	}

	private void appendCustomerInfo(Fields fields, StringBuilder request) {
		appendJsonField(Constants.CUSTOMER, request);
		request.append(Constants.OPENING_BRACE);
		appendJsonField1(Constants.FIRST_NAME, fields.get(FieldType.CUST_NAME.getName()), request);
		appendJsonField(Constants.EMAIL_ADDRESS, fields.get(FieldType.CUST_EMAIL.getName()), request);
		appendJsonField(Constants.TELEPHONE_NUMBER, fields.get(FieldType.CUST_PHONE.getName()), request);
		request.append(Constants.CLOSING_BRACE);
		request.append(",");

	}

	private void appendMerchantInfo(Fields fields, StringBuilder request) {
		Date now = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		// request.append(Constants.OPENING_BRACE);
		appendJsonField(Constants.REDIRECT_TYPE, "WEBPAGE", request);
		appendJsonField(Constants.MARKETPALACE_ORDERID, fields.get(FieldType.TXN_ID.getName()), request);
		appendJsonField(Constants.M_CODE, fields.get(FieldType.MERCHANT_ID.getName()), request);
		appendJsonField(Constants.CALL_BACK_URL, ConfigurationConstants.EPAYLATER_RETURN_URL.getValue(), request);
		appendJsonField(Constants.CUSTOMER_EMAIL_VERIFIED, false, request);
		appendJsonField(Constants.CUSTOMER_TELEPHONE_NUMBER_VERIFIED, true, request);
		appendJsonField(Constants.CUSTOMER_LOGGEDIN, true, request);
		appendJsonField(Constants.AMOUNT, fields.get(FieldType.AMOUNT.getName()), request);
		appendJsonField(Constants.CURRENCY_CODE,
				Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName())), request);
		appendJsonField(Constants.DATE, dateFormat.format(now), request);
		appendJsonField(Constants.CATEGORY, "PG", request);
		request.append(",");

	}

	public String createRefundTransaction(Fields fields) {
		StringBuilder request = new StringBuilder();
		appendRefundDeatils(fields, request);
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);

		return jsonRequest.toString();
	}

	private void appendRefundDeatils(Fields fields, StringBuilder request) {
		Date now = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		appendJsonField(Constants.MARKETPALACE_ORDERID, fields.get(FieldType.ORIG_TXN_ID.getName()), request);
		appendJsonField(Constants.RETURN_AMOUNT, fields.get(FieldType.AMOUNT.getName()), request);
		appendJsonField(Constants.RETURN_ACCEPTED_DATE, dateFormat.format(now), request);
		appendJsonField(Constants.RETURN_SHIPMENT_RECEIVED_DATE, dateFormat.format(now), request);
		appendJsonField(Constants.REFUND_DATE, dateFormat.format(now), request);
		appendJsonField(Constants.RETURN_TYPE, "full", request);
	}

// EpayLater Satus Create Request
	public String createStatusRequest(Fields fields) {
		StringBuilder request = new StringBuilder();
		appendStatusInfo(fields, request);

		return null;
	}

	private void appendStatusInfo(Fields fields, StringBuilder request) {

	}

}
