package com.kbn.epaylater;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class RefundTransactionProcessor implements TransactionProcessor{
	private static Logger logger = Logger.getLogger(RefundTransactionProcessor.class.getName());


	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createRefundTransaction(fields);
		logger.info("Request to EPayLAter: " + request);
		String response = communicator.transactRefund(request, ConfigurationConstants.EPAYLATER_REFUND_URL.getValue()+fields.get(FieldType.ORIG_TXN_ID.getName()));
		EPayLaterTransformer transformer = new EPayLaterTransformer(response);
		transformer.updateEpayLaterRefundResponse(fields);

	}

}
