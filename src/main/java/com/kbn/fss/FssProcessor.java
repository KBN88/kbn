package com.kbn.fss;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Processor;

public class FssProcessor implements Processor {
	public void preProcess(Fields fields) throws SystemException {
	}

	public void process(Fields fields) throws SystemException {
		
		if (fields.get(FieldType.TXNTYPE.getName()).equals(
				TransactionType.NEWORDER.getName())) {
			// New Order Transactions are not processed by FSS
			return;
		}


		if(!fields.get(FieldType.ACQUIRER_TYPE.getName()).equals(AcquirerType.FSS.getCode())){
			return;
		}
		
		(new FssIntegrator()).process(fields);
	}

	public void postProcess(Fields fields) throws SystemException {
	}
}
