package com.kbn.fss;

import com.kbn.pg.core.Processor;

public class FssProcessorFactory {
	public static Processor getInstance(){
		return new FssProcessor();
	}
}
