package com.kbn.fss;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;

public class TransactionFactory {
	@SuppressWarnings("incomplete-switch")
	public static Transaction getInstance(Fields fields){
		Transaction transaction = new Transaction();
		
		switch(TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))){
		case AUTHORISE:
			transaction.setAuthorization(fields);
			break;
		case ENROLL:
			transaction.setEnrollment(fields);
			break;
		case REFUND:
			transaction.setRefund(fields);
			break;
		case SALE:
			//Authorization and Sale messaging format is same, just action code changes
			transaction.setAuthorization(fields); 
			break;
		case CAPTURE:
			transaction.setCapture(fields);
			break;
		case STATUS:
			transaction.setStatusEnquiry(fields);
			break;
		}
		
		return transaction;
	}
}
