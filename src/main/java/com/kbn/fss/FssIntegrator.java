package com.kbn.fss;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;

public class FssIntegrator {
	private Transaction transactionRequest;
	private Transaction transactionResponse;
	private TransactionConverter converter = new TransactionConverter();
	private TransactionCommunicator communicator = new TransactionCommunicator();
	private FssTransformer fssTransformer = null;
	private CryptoManager cryptoManager = CryptoManagerFactory.getCryptoManager();

	public void process(Fields fields) throws SystemException {

		send(fields);

		resend(fields);		
				
		cryptoManager.secure(fields);
	}//process
	
	public void resend(Fields fields) throws SystemException{
		
		//TODO: Put a merchant specific flag
		
		// If card was not enrolled, FSS suggests to perform authorization
		String txnType = fields.get(FieldType.TXNTYPE.getName());
		if (txnType.equals(TransactionType.ENROLL.getName())) {
			String status = fields.get(FieldType.STATUS.getName());
			if (null != status && status.equals(StatusType.PENDING.getName())) {
				fields.put(FieldType.TXNTYPE.getName(), fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()));
				
				send(fields);

				// If transaction not authorized
				if (fields.get(FieldType.STATUS.getName()).equals(StatusType.PENDING.getName())) {
					fields.put(FieldType.STATUS.getName(),	StatusType.DECLINED.getName());
					fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.DECLINED.getResponseMessage());
				}//if
			}//if
		}//if
	}

	public void send(Fields fields) throws SystemException {
		transactionRequest = TransactionFactory.getInstance(fields);

		String request = converter.toXml(transactionRequest);

		String response = communicator.getResponse(request, fields);

		transactionResponse = converter.toTransaction(response);

		fssTransformer = new FssTransformer(transactionResponse);
		fssTransformer.updateResponse(fields);
	}

	public TransactionConverter getConverter() {
		return converter;
	}

	public void setConverter(TransactionConverter converter) {
		this.converter = converter;
	}

	public TransactionCommunicator getCommunicator() {
		return communicator;
	}

	public void setCommunicator(TransactionCommunicator communicator) {
		this.communicator = communicator;
	}

	public Transaction getTransactionRequest() {
		return transactionRequest;
	}

	public void setTransactionRequest(Transaction transactionRequest) {
		this.transactionRequest = transactionRequest;
	}

	public Transaction getTransactionResponse() {
		return transactionResponse;
	}

	public void setTransactionResponse(Transaction transactionResponse) {
		this.transactionResponse = transactionResponse;
	}

	public FssTransformer getFssTransformer() {
		return fssTransformer;
	}

	public void setFssTransformer(FssTransformer fssTransformer) {
		this.fssTransformer = fssTransformer;
	}
}
