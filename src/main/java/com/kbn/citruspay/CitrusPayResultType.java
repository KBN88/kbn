package com.kbn.citruspay;

public enum CitrusPayResultType {
	APPROVED		("APPROVED"),
	CAPTURED		("CAPTURED"),
	REJECTED		("REJECTED"),
	FAILED			("FAILED"),
	ENROLLED		("ENROLLED");
	
	private CitrusPayResultType(String name){
		this.name = name;
	}
	
	private final String name;

	public String getName() {
		return name;
	}
	
	public static CitrusPayResultType getInstance(String name){
		if(null == name){
			return REJECTED;
		}
		
		CitrusPayResultType[] fssResultTypes = CitrusPayResultType.values();
		
		for(CitrusPayResultType fssResultType : fssResultTypes){
			if(fssResultType.getName().startsWith(name)){
				return fssResultType;
			}
		}
		
		//Return error if unexpected value is returned in parameter "respCode"
		return REJECTED;
	}
}
