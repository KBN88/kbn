package com.kbn.citruspay;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

public class SignatureComparator {

	public void compare(Fields fields) throws SystemException {
		String generatedsignature = generateSig(fields);
		String recievedSignature = fields.get(Constants.SIGNATURE);

		if (!(generatedsignature.equals(recievedSignature))) {
			throw new SystemException(ErrorType.SIGNATURE_MISMATCH,	"Invalid signature");
		}
	}

	public String generateSig(Fields fields) throws SystemException {

		String TxId = fields.get(Constants.TRANSACTION_ID_CITRUS);
		String TxStatus = fields.get(Constants.TRANSACTION_STATUS_CITRUS);
		String amount = fields.get(Constants.AMOUNT);
		String pgTxnId = fields.get(Constants.PG_TRANSACTION_NO);
		String issuerRefNo = fields.get(Constants.ISSUER_REF_NO);
		String authIdCode = fields.get(Constants.AUTH_ID_CODE);
		String firstName = fields.get(Constants.FIRST_NAME);
		String lastName = fields.get(Constants.LAST_NAME);
		String pgRespCode = fields.get(Constants.PG_RESPONSE_CODE);
		String zipCode = fields.get(Constants.ADDRESS_ZIP);

		StringBuilder signatureText = new StringBuilder();
		if (TxId != null) {
			signatureText.append(TxId);
		}
		if (TxStatus != null) {
			signatureText.append(TxStatus);
		}
		if (amount != null) {
			signatureText.append(amount);
		}
		if (pgTxnId != null) {
			signatureText.append(pgTxnId);
		}
		if (issuerRefNo != null) {
			signatureText.append(issuerRefNo);
		}
		if (authIdCode != null) {
			signatureText.append(authIdCode);
		}
		if (firstName != null) {
			signatureText.append(firstName);
		}
		if (lastName != null) {
			signatureText.append(lastName);
		}
		if (pgRespCode != null) {
			signatureText.append(pgRespCode);
		}
		if (zipCode != null) {
			signatureText.append(zipCode);
		}

		CitrusPayIntegrator.addDefaultFields(fields);
		
		return CitrusPayUtil.generateHMAC(signatureText.toString(),	fields.get(FieldType.MERCHANT_ID.getName()));
	}
}
