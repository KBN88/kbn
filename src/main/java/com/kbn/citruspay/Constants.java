/**
 * 
 */
package com.kbn.citruspay;

/**
 * @author Surender
 *
 */
public class Constants {
	public static final String EQUATOR = "=";
	public static final String SEPARATOR = "&";
	public static final String OPENING_BRACE = "{";
	public static final String CLOSING_BRACE = "}";
	public static final String OPENING_SQUARE_BRACE = "[";
	public static final String CLOSING_SQUARE_BRACE = "]";
	
	public static final String MERCHANT_ACCESS_KEY = "merchantAccessKey";	
	public static final String MERCHANT_TXN_ID = "merchantTxnId";
	public static final String TRANSACTION_ID = "transactionId";
	public static final String PG_TRANSACTION_NO = "pgTxnNo";
	public static final String PG_RESPONSE_CODE = "pgRespCode";
	public static final String TRANSACTION_ID_CITRUS = "TxId";
	public static final String TRANSACTION_STATUS_CITRUS = "TxStatus";
	public static final String AMOUNT = "amount";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String ADDRESS = "address";
	public static final String ADDRESS_CITY = "addressCity";
	public static final String ADDRESS_STATE = "addressState";
	public static final String ADDRESS_ZIP = "addressZip";
	public static final String EMAIL = "email";
	public static final String MOBILE = "mobile";
	public static final String PAYMENT_MODE = "paymentMode";
	public static final String CARD_TYPE = "cardType";
	public static final String CARD_NUMBER = "cardNumber";
	public static final String CARD_HOLDER_NAME = "cardHolderName";
	public static final String EXPIRY_MONTH = "expiryMonth";
	public static final String EXPIRY_YEAR = "expiryYear";
	public static final String CVV_NUMBER = "cvvNumber";
	public static final String RETURN_URL = "returnUrl";
	public static final String REDIRECT_URL = "redirectUrl";
	public static final String SIGNATURE = "signature";
	public static final String ISSUER_REF_NO = "issuerRefNo";
	public static final String AUTH_ID_CODE = "authIdCode";
	public static final String TX_MSG = "TxMsg";
	public static final String TXN_DATE_TIME = "txnDateTime";
	public static final String TX_GATEWAY = "TxGateway";
	public static final String TXN_TYPE = "txnType";
	public static final String CURRENCY_CODE = "currencyCode";
	public static final String RRN = "RRN";
	public static final String PG_TXN_ID = "pgTxnId";
	public static final String KEY = "key";
	
	// Added for Net banking Transaction
	public static final String BANK_NAME = "bankName";
	public static final String BANK_CODE = "issuerCode";
	
	public static final String RESP_CODE = "respCode";
	public static final String RESP_MSG = "respMsg";	
	public static final String NAME_STRING = "name";
	public static final String CUSTOM_PARAM_NAME = "UDF1";
	public static final String VALUE_STRING = "value";
	public static final String R_STRING ="R";
	public static final String SLASH = "/";
	public static final String ACCESS_KEY = "access_key";
	public static final String SECRET_KEY = "secret_key";
	public static final String ENQUIRY_RESPONSE = "enquiryResponse";
	public static final String CUSTOM_PARAMS = "customParams";
	
	//fields for recurring payments
	public static final String BILLING_PERIOD_CYCLE = "billingPeriodCycle";
	public static final String BILLING_PERIOD_UNIT = "billingPeriodUnit";
	public static final String SCHEDULE = "schedule";
	public static final String VANITY = "vanity";
	public static final String MTX = "mtx";
	public static final String HINT = "hint";
	public static final String TOKEN = "token";
	public static final String TYPE = "type";
	public static final String CARD = "card";
	public static final String AUTH_REF_ID = "authRefId";
	public static final String AUTHORIZED = "authorized";
	public static final String SUBSCRIBER_EMAIL = "subscriberEmail";
	public static final String MERCHANT_ID = "merchantId";
	public static final String ONE = "1";
	public static final String CURRENCY = "currency";
	public static final String PAN = "pan";
	public static final String EXPIRY = "expiry";
	public static final String HOLDER = "holder";
	public static final String CREDIT_CARD = "creditcard";
	public static final String SUBSCRIPTION_ID = "subscriptionId";
	public static final String NEXT_BILLING_DATE = "nextBillingDate";
	public static final String LAST_PAYMENT_DATE = "lastPaymentDate";
	public static final String CODE = "code";
	public static final String MESSAGE = "message";
	public static final String STATUS = "status";
}
