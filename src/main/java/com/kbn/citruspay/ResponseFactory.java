package com.kbn.citruspay;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.pg.core.Currency;
/**
 * @author Surender
 *
 */
public class ResponseFactory {
	private static Logger logger = Logger.getLogger(ResponseFactory.class.getName());
	
	public static Response createResponse(String responseString) throws SystemException{
		
		Response response = new Response();
		JSONParser parser = new JSONParser();
		Object obj;
		try {
			obj = parser.parse(responseString);
			
			JSONObject jObject = (JSONObject)obj;
			
			//********************************************************
			//simualtor
			
			Object respCode1 = jObject.get("pgRespCode");
			if(null != respCode1){
				if(respCode1.getClass().equals(Long.class)){
					response.setRespCode(((Long)respCode1).toString());
				}else{
					response.setRespCode((String)respCode1);
				}
			}
			
			Object respMsg1 = jObject.get(Constants.RESP_MSG);
			if(null != respMsg1){
				response.setRespMsg((String)respMsg1);
			}
			
			Object pgTxnId1 = jObject.get(Constants.PG_TXN_ID);
			if(null != pgTxnId1){
				response.setPgTxnId((String)pgTxnId1);
			}
			
			
			
			//********************************************************
			Object enquiryResponse = jObject.get(Constants.ENQUIRY_RESPONSE);
            if(null != enquiryResponse){
            	JSONArray jSonArray = (JSONArray) enquiryResponse;
                   jObject =(JSONObject) jSonArray.get(0);
            }

			Object redirectUrl = jObject.get(Constants.REDIRECT_URL);
			if(null != redirectUrl){
				response.setRedirectUrl((String)redirectUrl);	
			}
			
			Object respCode = jObject.get(Constants.RESP_CODE);
			if(null != respCode){
				if(respCode.getClass().equals(Long.class)){
					response.setRespCode(((Long)respCode).toString());
				}else{
					response.setRespCode((String)respCode);
				}
			}
			
			Object respMsg = jObject.get(Constants.RESP_MSG);
			if(null != respMsg){
				response.setRespMsg((String)respMsg);
			}
			
			Object transactionId = jObject.get(Constants.TRANSACTION_ID);
			if(null != transactionId){
				response.setTransactionId((String)transactionId);
			}
			
			Object pgTxnId = jObject.get(Constants.PG_TXN_ID);
			if(null != pgTxnId){
				response.setPgTxnId((String)pgTxnId);
			}
			
			Object authIdCode = jObject.get(Constants.AUTH_ID_CODE);
			if(null != authIdCode){
				response.setAuthIdCode((String)authIdCode);
			}
			
			Object rrn = jObject.get(Constants.RRN.toLowerCase());
			if(null != rrn){
				response.setRrn((String)rrn);
			}
		} catch (ParseException parseException) {
			logger.error("Unable to parse citruspay response", parseException);		
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, parseException, "Unable to parse CitrusResponse ");
		}		
					
		return response;
	}
	
	public static Response createSubscriptionResponse(String responseString) throws SystemException{
		Response response = new Response();
		Object obj;
		try {
			JSONParser parser = new JSONParser();
			obj = parser.parse(responseString);
			JSONObject jObject = (JSONObject) obj;

			Object amount = jObject.get(Constants.AMOUNT);
			if (null != amount) {
				JSONObject jSonArray = (JSONObject) amount;
				Object amt = (Object) jSonArray.get(Constants.VALUE_STRING);
				if(null != amt){
					response.setAmount(String.valueOf(amt));
				}
				Object currencyObj = jSonArray.get(Constants.CURRENCY);
				if(null!=currencyObj){
					response.setCurrency(Currency.getNumericCode(String.valueOf(currencyObj)));
				}
			}
			Object schedule = jObject.get(Constants.SCHEDULE);
			if (null != schedule) {
				JSONObject jSonArray = (JSONObject) schedule;
				Object billingPeriodUnitObj = jSonArray.get(Constants.BILLING_PERIOD_UNIT);
				if(null!=billingPeriodUnitObj){					
						response.setBillingPeriodUnit(String.valueOf(billingPeriodUnitObj));					
				}
				Object billingPeriodCycleObj = jSonArray.get(Constants.BILLING_PERIOD_CYCLE);
				if(null!=billingPeriodCycleObj){
					response.setBillingPeriodCycle(String.valueOf(billingPeriodCycleObj));
				}
			}
			Object subscriptionId = jObject.get(Constants.SUBSCRIPTION_ID);
			if (null != subscriptionId) {
				response.setSubscriptionId((String) subscriptionId);
			}
			Object nextBillingDate = jObject.get(Constants.NEXT_BILLING_DATE);
			if (null != nextBillingDate) {
				//parse date first
				response.setNextBillingDate((String) nextBillingDate);
			}
			Object lastPaymentDateObj = jObject.get(Constants.LAST_PAYMENT_DATE);
			if (null != lastPaymentDateObj) {
				//parse date first
				response.setLastPaymentDate((String) lastPaymentDateObj);
			}
			Object merchantIdObj = jObject.get(Constants.MERCHANT_ID);
			if (null != merchantIdObj) {
				//parse date first
				response.setMerchantId((String) merchantIdObj);
			}
			Object respCode = jObject.get(Constants.CODE);
			if(null != respCode){
				if(respCode.getClass().equals(Long.class)){
					response.setRespCode(((Long)respCode).toString());
				}else{
					response.setRespCode((String)respCode);
				}
			}
			Object respMsg = jObject.get(Constants.MESSAGE);
			if(null != respMsg){
				response.setRespMsg((String)respMsg);
			}
	   } catch (ParseException parseException) {
		  logger.error("Unable to parse citruspay response", parseException);		
		  throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, parseException, "Unable to parse CitrusResponse ");
	   }	
	 return response;
    }
	
	public static String parseVaultToken(String responseString) throws SystemException{
		Object obj;
		JSONParser parser = new JSONParser();
		try {
			obj = parser.parse(responseString);
		} catch (ParseException parseException) {
			 logger.error("Unable to parse citruspay response", parseException);
			  throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, parseException, "Unable to parse CitrusResponse ");
		}
		JSONObject jObject = (JSONObject) obj;

		Object token = jObject.get(Constants.TOKEN);
		if (null != token) {
			//parse date first
			return (String) token;
		}
		return null;
	}
	public static String parseAlterTokenResponse(String responseString) throws SystemException{
		Object obj;
		JSONParser parser = new JSONParser();
		try {
			obj = parser.parse(responseString);
		} catch (ParseException parseException) {
			 logger.error("Unable to parse citruspay response", parseException);
			  throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, parseException, "Unable to parse CitrusResponse ");
		}
		JSONObject jObject = (JSONObject) obj;

		Object status = jObject.get(Constants.STATUS);
		if (null != status) {
			//parse date first
			return (String) status;
		}
		return null;
	}
}