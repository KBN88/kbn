/**
 * 
 */
package com.kbn.citruspay;

import java.io.PrintWriter;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.opensymphony.xwork2.Action;

/**
 * @author Sunil
 * 
 */
public class CitrusPayForwarder {
	private static Logger logger = Logger.getLogger(CitrusPayForwarder.class.getName());
	private PropertiesManager propertiesManager = new PropertiesManager();
	
	public String forward(Fields fields, String signature, String url, String request) {
		try {
			/************* Enrolled card condition starts here ************/
			String acsurl = fields.get(FieldType.ACS_URL.getName());
			String PAReq = fields.get(FieldType.PAREQ.getName());
			String paymentid = fields.get(FieldType.PAYMENT_ID.getName());
			String termURL = propertiesManager.getSystemProperty("Request3DSURL");

			PrintWriter out = ServletActionContext.getResponse().getWriter();

			StringBuilder httpRequest = new StringBuilder();
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(acsurl);
			httpRequest.append("\" method=\"post\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"PaReq\" value=\"");
			httpRequest.append(PAReq);
			httpRequest.append("\">");
			httpRequest.append("<input type=\"hidden\" name=\"MD\" value=\"");
			httpRequest.append(paymentid);
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"TermUrl\" value=\"");
			httpRequest.append(termURL);
			httpRequest.append("\">");
			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");

			out.write(httpRequest.toString());
			/************* Enrolled card condition Ends here ************/
		} catch (Exception exception) {
			MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(), fields.getCustomMDC());
			logger.error("Unable to forward transaction", exception);
		}
		return Action.NONE;
	}
}
