package com.kbn.citruspay;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.CitrusPaySubscription;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.ReccuringCycleType;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.SubscriptionStatus;
import com.kbn.commons.util.TransactionManager;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.Amount;

public class CitrusPayTransformer {
	private Response response;

	public CitrusPayTransformer(Response response){
		this.response = response;
	}

	public void updateResponse(Fields fields){

		if(fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.SALE.getName())){
			String url = response.getRedirectUrl();
			if(!StringUtils.isEmpty(url)){
				fields.put(FieldType.ACS_URL.getName(),url);
				fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
				fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getResponseCode());
				fields.put(FieldType.STATUS.getName(),StatusType.SENT_TO_BANK.getName());
				return;
			}
		}			
		fields.put(FieldType.STATUS.getName(), getStatus());

		ErrorType errorType = getResponseCode();
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());

		fields.put(FieldType.PG_RESP_CODE.getName(), response.getRespCode());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), response.getRespMsg());
		fields.put(FieldType.PG_REF_NUM.getName(), response.getTransactionId());
		fields.put(FieldType.AUTH_CODE.getName(), response.getAuthIdCode());
		fields.put(FieldType.RRN.getName(), response.getRrn());
		fields.put(FieldType.ACQ_ID.getName(), response.getPgTxnId());
	}
	
	public ErrorType getResponseCode(){
		String respCode = response.getRespCode();
		ErrorType errorType = null;

		if(null == respCode){
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if(respCode.equals("0")){
			errorType = ErrorType.SUCCESS;
		} else if(respCode.equals("4")){
			errorType = ErrorType.CANCELLED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;;
		}

		return errorType;
	}

	public String getStatus(){
		String respCode = response.getRespCode();
		String status = "";

		if(null == respCode){
			status = StatusType.ERROR.getName();
		} else if(respCode.equals("0")){
			status = StatusType.CAPTURED.getName();
		} else if(respCode.equals("1")){
			status = StatusType.ERROR.getName();
		} else if(respCode.equals("2")){
			status = StatusType.FAILED.getName();
		}else if(respCode.equals("4")){
			status = StatusType.CANCELLED.getName();
		}else {
			status = StatusType.ERROR.getName();
		}
		return status;
	}

	public CitrusPaySubscription createSubscription(Fields fields, String merchantId){
		CitrusPaySubscription citrusPaySubscription= new CitrusPaySubscription();

		citrusPaySubscription.setPayId(fields.get(FieldType.PAY_ID.getName()));
		citrusPaySubscription.setCustomerEmail(fields.get(FieldType.CUST_EMAIL.getName()));
		citrusPaySubscription.setMerchantId(merchantId);
		citrusPaySubscription.setTransactionOid(fields.get(FieldType.INTERNAL_ORIG_TXN_ID.getName()));
		citrusPaySubscription.setCurrency(fields.get(FieldType.CURRENCY_CODE.getName()));
		citrusPaySubscription.setAmount(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()), fields.get(FieldType.CURRENCY_CODE.getName())));
		citrusPaySubscription.setCardHolderName(fields.get(FieldType.CUST_NAME.getName()));
		citrusPaySubscription.setRecurringTransactionId(TransactionManager.getNewTransactionId());
		citrusPaySubscription.setOrderId(fields.get(FieldType.ORDER_ID.getName()));

		if(response.getSubscriptionId()!=null){
			//update DB for success
			citrusPaySubscription.setSubscriptionId(response.getSubscriptionId());
			citrusPaySubscription.setNextBillingDate(response.getNextBillingDate());
			citrusPaySubscription.setLastPaymentDate(response.getLastPaymentDate());
			citrusPaySubscription.setStatus(SubscriptionStatus.ACTIVE);
			citrusPaySubscription.setRecurringTransactionCount(Long.valueOf(fields.get(FieldType.RECURRING_TRANSACTION_COUNT.getName())));
			citrusPaySubscription.setRecurringTransactionsProcessed(1L);
			if (null!=response.getBillingPeriodCycle()){
				citrusPaySubscription.setRecurringTransactionInterval(ReccuringCycleType.getInstanceFromName(response.getBillingPeriodCycle()));
			}
			if (null!=response.getBillingPeriodUnit()){
				citrusPaySubscription.setBillingPeriodUnit(Long.valueOf(response.getBillingPeriodUnit()));
			}
			citrusPaySubscription.setResponseCode(ErrorType.SUCCESS.getResponseCode());
			citrusPaySubscription.setResponseMessage(ErrorType.SUCCESS.getResponseMessage());

			fields.put(FieldType.RECURRING_TRANSACTION_ID.getName(),citrusPaySubscription.getRecurringTransactionId());
		}else{
			//failed to create subscription
			citrusPaySubscription.setStatus(SubscriptionStatus.ERROR);
			citrusPaySubscription.setResponseCode(response.getRespCode());
			citrusPaySubscription.setResponseMessage(response.getRespMsg());
			fields.put(FieldType.IS_RECURRING.getName(),com.kbn.commons.util.Constants.N_FLAG.getValue());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.RECURRING_PAYMENT_UNSUCCESSFULL.getResponseMessage());
		}
		return citrusPaySubscription;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
}
