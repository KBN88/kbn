package com.kbn.citruspay;

import java.io.IOException;
import java.util.regex.Pattern;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.SystemConstants;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.Amount;

public class TransactionCommunicator {
	private static Logger logger = Logger.getLogger(TransactionCommunicator.class.getName());
	
	public String getResponseString(String request, Fields fields)
			throws SystemException {

			//Get transaction URL from configurations - based on type of transaction
			String url = Mapper.getTransactionUrl(fields);

			String signature = createSignature(fields);
	
			//Perform transaction with CitrusPay
			String response = transact(fields, request, url, signature);

			//Return response
			return response;
	}
	
	public String getStatusEnquryResponseString(String request, Fields fields)
			throws SystemException {

			//Get transaction URL from configurations - based on type of transaction
			String url = Mapper.getTransactionUrl(fields);
			url = url+Constants.SLASH + fields.get(FieldType.TXN_ID.getName());
			//Perform status enquiry with CitrusPay
			String response = transact(fields, request, url);

			//Return response
			return response;
	}
	
	@SuppressWarnings("incomplete-switch")
	public Response getResponseObject(String request, Fields fields) throws SystemException{
		
		String response = "";
		
		switch (TransactionType.getInstance(fields.get(FieldType.TXNTYPE
				.getName()))) {
		case AUTHORISE:
		case ENROLL:
		case SALE:
		case REFUND:
			response = getResponseString(request, fields);
			break;
		case STATUS:
			response = getStatusEnquryResponseString(request, fields);
			break;
		}
		return ResponseFactory.createResponse(response);
	}
	
	public Response getEnquiryResponseObject(String request, Fields fields) throws SystemException{
		String response = getStatusEnquryResponseString(request, fields);
		return  ResponseFactory.createResponse(response);
	}
	
	public String createSignature(Fields fields) throws SystemException{
		String signatureText = getSignatureText(fields);
		
		return CitrusPayUtil.generateHMAC(signatureText, fields.get(FieldType.MERCHANT_ID.getName()));		
	}

	public String getSignatureText(Fields fields){
		StringBuilder signature = new StringBuilder();

		String transactionId = "";
		if(fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.REFUND.getName())){
			transactionId = fields.get(FieldType.ORIG_TXN_ID.getName());
		}else{
			transactionId = fields.get(FieldType.TXN_ID.getName());	
		}
		signature.append(Constants.MERCHANT_ACCESS_KEY);
		signature.append(Constants.EQUATOR);
		signature.append(fields.get(FieldType.PASSWORD.getName())); //This is access key in CitrusPay
		signature.append(Constants.SEPARATOR);
		signature.append(Constants.TRANSACTION_ID);
		signature.append(Constants.EQUATOR);
		signature.append(transactionId);
		signature.append(Constants.SEPARATOR);
		signature.append(Constants.AMOUNT);
		signature.append(Constants.EQUATOR);
		String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()), fields.get(FieldType.CURRENCY_CODE.getName()));
		signature.append(amount);
		
		return signature.toString();
	}

    public String transact(Fields fields, String request, String hostUrl, String signature) throws SystemException{
		String response = "";

		logRequest(request, hostUrl, fields, signature);
		
		//temp code for NB simulation
		if(fields.get(FieldType.PAYMENT_TYPE.getName()).equals(PaymentType.NET_BANKING.getCode())){
			
		}

		try {

			StringRequestEntity requestEntity = new StringRequestEntity(  request, "application/json", SystemConstants.DEFAULT_ENCODING_UTF_8);

			PostMethod postMethod = new PostMethod(hostUrl);

			postMethod.setRequestHeader("Accept", "application/json");
			postMethod.setRequestHeader(Constants.ACCESS_KEY, fields.get(FieldType.PASSWORD.getName()));
			postMethod.setRequestHeader("signature", signature);
			postMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			postMethod.setRequestEntity(requestEntity);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(postMethod);

			response = postMethod.getResponseBodyAsString();

		} catch (IOException ioException) {
			fields.put(FieldType.STATUS.getName(),StatusType.ERROR.getName());
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					ioException, "Network Exception with CitrusPay "
							+ hostUrl.toString());
		}
		logResponse(response, fields);
		return response;
	}

	//for status enquiry transaction
   public String transact(Fields fields, String request, String hostUrl) throws SystemException{
	    String response = "";
		logRequest(request, hostUrl, fields, "");
		try{
			GetMethod getMethod = new GetMethod(hostUrl);

			getMethod.setRequestHeader("Accept", "application/json");
			getMethod.setRequestHeader(Constants.ACCESS_KEY, fields.get(FieldType.PASSWORD.getName()));
			getMethod.setRequestHeader(Constants.SECRET_KEY, fields.get(FieldType.MERCHANT_ID.getName()));
			getMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(getMethod);
			response = getMethod.getResponseBodyAsString();

		}catch (IOException ioException) {
			fields.put(FieldType.STATUS.getName(),StatusType.ERROR.getName());
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					ioException, "Network Exception with CitrusPay "
							+ hostUrl.toString());
		}
		logResponse(response, fields);
		return response;
	}

   public void logRequest(String requestMessage, String url,Fields fields, String signature){
		log("Request message to Citrus: Url= "+url +" "+ requestMessage, fields);
	}

   public void logResponse(String requestMessage, Fields fields){
		log("Response message from Citrus: "+ requestMessage, fields);
	}

   private void log(String message, Fields fields){
		message = Pattern.compile("(\"cardNumber\":\")([\\s\\S]*?)(\")").matcher(message).replaceAll("$1$3");
		message = Pattern.compile("(\"cvvNumber\":\")([\\s\\S]*?)(\")").matcher(message).replaceAll("$1$3");
		message = Pattern.compile("(\"expiryMonth\":\")([\\s\\S]*?)(\")").matcher(message).replaceAll("$1$3");
		message = Pattern.compile("(\"expiryYear\":\")([\\s\\S]*?)(\")").matcher(message).replaceAll("$1$3");
		MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(), fields.getCustomMDC());
		logger.info(message);
	}
}
