/**
 * 
 */
package com.kbn.citruspay;

/**
 * @author Surender
 *
 */
public enum PaymentMode
{
  NET_BANKING,  CREDIT_CARD,  DEBIT_CARD;
  
  private PaymentMode() {}
}
