package com.kbn.citruspay;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

public class CitrusPayIntegrator {
	private TransactionConverter converter = new TransactionConverter();
	private TransactionCommunicator communicator = new TransactionCommunicator();
	private CitrusPayTransformer citrusPayTransformer = null;
	private CryptoManager cryptoManager = CryptoManagerFactory.getCryptoManager();

	public void process(Fields fields) throws SystemException {

		addDefaultFields(fields);
		
		send(fields);	
				
		cryptoManager.secure(fields);
	}//process

	public void send(Fields fields) throws SystemException {
		String request = TransactionFactory.getInstance(fields);
		
		Response response = communicator.getResponseObject(request, fields);

		citrusPayTransformer = new CitrusPayTransformer(response);

		citrusPayTransformer.updateResponse(fields);
	}
	
	public static void addDefaultFields(Fields fields){
		String merchantId = fields.get(FieldType.MERCHANT_ID.getName());
		if(StringUtils.isEmpty(merchantId)){
			fields.put(FieldType.MERCHANT_ID.getName(), ConfigurationConstants.CITRUSPAY_MERCHANT_KEY.getValue());
		}
		
		String password = fields.get(FieldType.PASSWORD.getName());
		if(StringUtils.isEmpty(password)){
			fields.put(FieldType.PASSWORD.getName(), ConfigurationConstants.CITRUSPAY_ACCESS_KEY.getValue());
		}
	}

	public TransactionConverter getConverter() {
		return converter;
	}

	public void setConverter(TransactionConverter converter) {
		this.converter = converter;
	}

	public TransactionCommunicator getCommunicator() {
		return communicator;
	}

	public void setCommunicator(TransactionCommunicator communicator) {
		this.communicator = communicator;
	}

	public CitrusPayTransformer getCitrusPayTransformer() {
		return citrusPayTransformer;
	}

	public void setCitrusPayTransformer(CitrusPayTransformer citrusPayTransformer) {
		this.citrusPayTransformer = citrusPayTransformer;
	}
}
