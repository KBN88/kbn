package com.kbn.citruspay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionSearchService;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.security.SecurityProcessor;

public class CitrusPayStatusEnquirer {
	private static Logger logger = Logger
			.getLogger(CitrusPayStatusEnquirer.class.getName());

	public void citrusStatusEnquirer() {
		TransactionSearchService transactionSearchService = new TransactionSearchService();
		try {
			List<TransactionSummaryReport> transactionList = transactionSearchService
					.getTransactionsForStatusUpdate(AcquirerType.CITRUS_PAY
							.getCode());
			for (TransactionSummaryReport transaction : transactionList) {

				Fields fields = new Fields(prepareFields(transaction));

				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);

				String request = TransactionFactory.getInstance(fields);
				TransactionCommunicator txnCon = new TransactionCommunicator();

				Response response = txnCon.getEnquiryResponseObject(request, fields);
				CitrusPayTransformer transformer = new CitrusPayTransformer(response);
				transformer.updateResponse(fields);

				// update new order transaction
				fields.updateStatus();
				//Update sale txn
				fields.updateTransactionDetails();
			}
		} catch (SystemException systemException) {
			logger.error("Error updating status for citruspay", systemException);
		}
	}

	public Map<String, String> prepareFields(TransactionSummaryReport transaction){
		Map<String, String> requestMap = new HashMap<String, String>();

		requestMap.put(FieldType.PAY_ID.getName(),
				transaction.getPayId());
		requestMap.put(FieldType.TXN_ID.getName(),
				transaction.getTransactionId());
		requestMap.put(FieldType.TXNTYPE.getName(),
				TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(),
				AcquirerType.CITRUS_PAY.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(),
				TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(),
				transaction.getCurrencyCode());
		requestMap.put(FieldType.OID.getName(),
				transaction.getOid());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(),
				transaction.getOid());
		return requestMap;
	}
}