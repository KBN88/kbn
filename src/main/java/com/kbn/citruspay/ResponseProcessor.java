package com.kbn.citruspay;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.Processor;

/**
 * @author Surender
 *
 */
public class ResponseProcessor implements Processor {

	public void preProcess(Fields fields) throws SystemException {
		SignatureComparator compare = new SignatureComparator();
	//	compare.compare(fields); For simulator
	}

	public void process(Fields fields) throws SystemException {		

		mapCitrusResponse(fields);

		if(null == fields.get(Constants.TRANSACTION_ID_CITRUS)){
			fields.put(FieldType.STATUS.getName(), StatusType.REJECTED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.REJECTED.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.REJECTED.getResponseMessage());
		}
		fields.updateTransactionDetails();
		fields.updateNewOrderDetails();
	}

	public void postProcess(Fields fields) throws SystemException {
	}

	public void mapCitrusResponse(Fields fields){
		//Note: Not all response parameters from citruspay are needed to store/processed in response
		//Consider mapping only those parameters which we need to process for subsequent transactions like refund

		//pgRespCode is part of Signature check, no need to validate
		String pgResponseCode = fields.get(Constants.PG_RESPONSE_CODE);		
		if(!StringUtils.isBlank(pgResponseCode)){
			fields.put(FieldType.PG_RESP_CODE.getName(), pgResponseCode);
		}

		String txMessage = fields.get(Constants.TX_MSG);
		if(!StringUtils.isBlank(txMessage)){
			//TODO: Validate and then assign
			fields.put(FieldType.PG_TXN_MESSAGE.getName(), txMessage);
		}

		String authCode = fields.get(Constants.AUTH_ID_CODE);
		if(!StringUtils.isBlank(authCode)){
			fields.put(FieldType.AUTH_CODE.getName(), authCode);
		}

		String txStatus = fields.get(Constants.TRANSACTION_STATUS_CITRUS);
		if(!StringUtils.isBlank(txStatus)){
			fields.put(FieldType.PG_TXN_STATUS.getName(), txStatus);
		}

		//This field is being stored to compare success rate of different gateways
		String txGateway = fields.get(Constants.TX_GATEWAY);
		if(!StringUtils.isBlank(txGateway)){
			//TODO: Validate and then assign
			fields.put(FieldType.PG_GATEWAY.getName(), txGateway);
		}

		String transactionId = fields.get(Constants.TRANSACTION_ID);
		if(!StringUtils.isBlank(transactionId)){
			//TODO: Validate and then assign
			fields.put(FieldType.PG_REF_NUM.getName(), transactionId);
		}

		String pgTxnNo = fields.get(Constants.PG_TRANSACTION_NO);
		if(!StringUtils.isBlank(pgTxnNo)){
			fields.put(FieldType.ACQ_ID.getName(), pgTxnNo);
		}

		String issuerRefNum = fields.get(Constants.ISSUER_REF_NO);
		if(!StringUtils.isBlank(issuerRefNum)){
			fields.put(FieldType.RRN.getName(), issuerRefNum);
		}
		updateInternalFields(fields);
	}

	public void updateInternalFields(Fields fields){
		String pgResponseCode = fields.get(Constants.PG_RESPONSE_CODE);	
		if(null == pgResponseCode){
			//By default "rejected by bank" - Ideally this should be a non-reachable code
			pgResponseCode = "1"; 
		}

		ErrorType error = Mapper.getErrorType(pgResponseCode);
		fields.put(FieldType.RESPONSE_CODE.getName(), error.getCode());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), error.getResponseMessage());
		
		StatusType status = Mapper.getStatusType(pgResponseCode);
		fields.put(FieldType.STATUS.getName(), status.getName());
	}
}
