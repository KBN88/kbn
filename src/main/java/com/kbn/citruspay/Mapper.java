package com.kbn.citruspay;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;

/**
 * @author Surender
 *
 */
public class Mapper {
	public static PaymentMode  getPaymentMode(Fields fields){
		PaymentMode paymentMode = null;
		
		String paymentType = fields.get(FieldType.PAYMENT_TYPE.getName());
		if(paymentType.equals(PaymentType.CREDIT_CARD.getCode())){
			paymentMode = PaymentMode.CREDIT_CARD;
		} else if(paymentType.equals(PaymentType.DEBIT_CARD.getCode())){
			paymentMode = PaymentMode.DEBIT_CARD;
		} else if(paymentType.equals(PaymentType.NET_BANKING.getCode())){
			paymentMode = PaymentMode.NET_BANKING;
		}
		
		return paymentMode;
	}
	
	public static String getCardType(Fields fields){
		String cardType = "";
		
		String mopType = fields.get(FieldType.MOP_TYPE.getName());
		if(mopType.equals(MopType.VISA.getCode())){
			cardType = "VISA";
		} else if(mopType.equals(MopType.MASTERCARD.getCode())){
			cardType = "MCRD";
		} else if(mopType.equals(MopType.MAESTRO.getCode())){
			cardType = "MTRO";
		}
		
		return cardType;
	}
	
	@SuppressWarnings("incomplete-switch")
	public static String getTransactionUrl(Fields fields){
		String url = "";
		
		switch(TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))){

		case REFUND:
			url = ConfigurationConstants.CITRUSPAY_REFUND_URL.getValue();
			break;
		case SALE:
			url = ConfigurationConstants.CITRUSPAY_TRANSACTION_URL.getValue();
			break;	
		
		case STATUS:
			url = ConfigurationConstants.CITRUSPAY_ENQUIRY_URL.getValue();
			break;
		}
	
		return url;
	}
	
	public static ErrorType getErrorType(String citrusResponseCode){
		ErrorType error = null;
		
		if(citrusResponseCode.equals("0")){
			error = ErrorType.SUCCESS;
		} else if(citrusResponseCode.equals("1")){
			error = ErrorType.DECLINED;
		} else if(citrusResponseCode.equals("2")){
			error = ErrorType.REJECTED;
		} else if(citrusResponseCode.equals("3")){
			error = ErrorType.CANCELLED;
		} else {
			error = ErrorType.DECLINED;
		}
		
		return error;
	}
	
	public static StatusType getStatusType(String citrusResponseCode){
		StatusType status = null;
		
		if(citrusResponseCode.equals("0")){
			status = StatusType.CAPTURED;
		} else if(citrusResponseCode.equals("1")){
			status = StatusType.DECLINED;
		} else if(citrusResponseCode.equals("2")){
			status = StatusType.REJECTED;
		} else if(citrusResponseCode.equals("3")){
			status = StatusType.CANCELLED;
		} else {
			status = StatusType.DECLINED;
		}
		
		return status;
	}
}
