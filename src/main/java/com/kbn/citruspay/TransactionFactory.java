package com.kbn.citruspay;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;

public class TransactionFactory {
	@SuppressWarnings("incomplete-switch")
	public static String getInstance(Fields fields){
		CitrusPayTransaction transaction = new CitrusPayTransaction();
		String request = "";
		
		switch(TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))){
		case AUTHORISE:
			request = transaction.createAuthorization(fields);
			break;
		case ENROLL:
			request = transaction.createEnrollment(fields);
			break;
		case REFUND:
			request = transaction.createRefund(fields);
			break;
		case SALE:
			//Authorization and Sale messaging format is same, just action code changes
			request = transaction.createAuthorization(fields); 
			break;
		case STATUS:
			request = transaction.createEnquiry(fields);
			break;
		}
		
		return request;
	}
}
