package com.kbn.citruspay;

import com.kbn.pg.core.Processor;

public class CitrusPayProcessorFactory {
	public static Processor getInstance(){
		return new CitrusPayProcessor();
	}
}
