/**
 * 
 */
package com.kbn.citruspay;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.SystemConstants;

/**
 * @author Surender
 * 
 */
public class CitrusPayUtil {
	private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

	public static String generateHMAC(String data, String hexEncodedKey) throws SystemException {
		String result = "";
		try {
			byte[] keyBytes = hexEncodedKey.getBytes();
			SecretKeySpec signingKey = new SecretKeySpec(keyBytes, HMAC_SHA1_ALGORITHM);

			Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
			mac.init(signingKey);
			byte[] rawHmac = mac.doFinal(data.getBytes());

			byte[] hexBytes = new Hex().encode(rawHmac);
			result = new String(hexBytes, SystemConstants.DEFAULT_ENCODING_UTF_8);
		} catch (NoSuchAlgorithmException noSuchAlgorithmException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					noSuchAlgorithmException, "No such Hashing algoritham with Citrus");
		} catch (InvalidKeyException invalidKeyException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					invalidKeyException, "No such key with Citrus");
		} catch (UnsupportedEncodingException unsupportedEncodingException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					unsupportedEncodingException, "No such encoding supported with Citrus");			
		}
		return result;
	}

	public static String parseCardExpDate(String expDateDate){
		StringBuilder formattedDate = new StringBuilder(expDateDate);
		formattedDate.insert(2, '/');
		return formattedDate.toString();
	}
}
