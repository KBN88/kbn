package com.kbn.citruspay;

import com.kbn.commons.user.CitrusPaySubscription;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.NetBankingType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.ReccuringCycleType;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Currency;
import com.kbn.pg.core.pageintegrator.Transaction;

public class CitrusPayTransaction extends Transaction {

	public String createAuthorization(Fields fields) {
		return createRequest(fields);
	}
	public String createEnrollment(Fields fields) {
		return createRequest(fields);
	}

	public String createRefund(Fields fields) {
		return createCommand(fields);	
	}

	public String createEnquiry(Fields fields) {
		return createEnquiryRequest(fields);
	}

	public String createCommand(Fields fields){
		StringBuilder request = new StringBuilder();
		
		//Append amount
		String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()), fields.get(FieldType.CURRENCY_CODE.getName()));		
		appendJsonField(Constants.AMOUNT, amount, request);	
		
		//Append txnType - R for Refund
		appendJsonField(Constants.TXN_TYPE, Constants.R_STRING, request);

		Fields previous = fields.getPrevious();

		//Append authCode
		appendJsonField(Constants.AUTH_ID_CODE, previous.get(FieldType.AUTH_CODE.getName()), request);

		//Append RRN
		appendJsonField(Constants.RRN.toLowerCase(), previous.get(FieldType.RRN.getName()), request);

		//Append pgTxnId
		appendJsonField(Constants.PG_TXN_ID, previous.get(FieldType.ACQ_ID.getName()), request);

		String alphaCurrency = Currency.getAlphabaticCode(previous.get(FieldType.CURRENCY_CODE.getName()));
		appendJsonField(Constants.CURRENCY_CODE, alphaCurrency, request);

		appendJsonField(Constants.MERCHANT_TXN_ID, fields.get(FieldType.ORIG_TXN_ID.getName()), request);

		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);		
		return jsonRequest.toString();		
	}

	public String createRequest(Fields fields){
		StringBuilder request = new StringBuilder();		
		appendMerchantInfo(fields, request);
		appendCustomerInfo(fields, request);
		if(PaymentType.NET_BANKING.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))){
			appendBankInfo(fields, request);
		}else{
			appendCardInfo(fields, request);
		}
		appendOrderInfo(fields, request);
		appendCustomParams(fields,request);
		
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);		
		return jsonRequest.toString();
	}

	//draft code for future use
	public String createEnquiryRequest(Fields fields){			
		return "";
	}

	public void appendCardInfo(Fields fields, StringBuilder request){
		appendJsonField(Constants.PAYMENT_MODE, Mapper.getPaymentMode(fields).toString(), request);
		appendJsonField(Constants.CARD_TYPE, Mapper.getCardType(fields), request);		
		appendJsonField(Constants.CARD_HOLDER_NAME, fields.get(FieldType.CUST_NAME.getName()), request);
		appendJsonField(Constants.CARD_NUMBER, fields.get(FieldType.CARD_NUMBER.getName()), request);
		appendJsonField(Constants.CVV_NUMBER, fields.get(FieldType.CVV.getName()), request);
		
		//Setting expiring year and month
		String expDate = fields.get(FieldType.CARD_EXP_DT.getName());			
		appendJsonField(Constants.EXPIRY_MONTH, expDate.substring(0, 2), request);
		appendJsonField(Constants.EXPIRY_YEAR, expDate.substring(2, 6), request);
	}

	public void appendMerchantInfo(Fields fields, StringBuilder request){
		appendJsonField(Constants.RETURN_URL, ConfigurationConstants.CITRUSPAY_RETURN_URL.getValue(), request);
	}

	public void appendOrderInfo(Fields fields, StringBuilder request){
		appendJsonField(Constants.MERCHANT_TXN_ID, fields.get(FieldType.TXN_ID.getName()), request);
		String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()), fields.get(FieldType.CURRENCY_CODE.getName()));
		appendJsonField(Constants.AMOUNT, amount, request);		
	}

	public void appendCustomerInfo(Fields fields, StringBuilder request){
		appendJsonField(Constants.MOBILE, fields.get(FieldType.CUST_PHONE.getName()), request);
		appendJsonField(Constants.FIRST_NAME, fields.get(FieldType.CUST_FIRST_NAME.getName()), request);
		appendJsonField(Constants.LAST_NAME, fields.get(FieldType.CUST_LAST_NAME.getName()), request);
		appendJsonField(Constants.ADDRESS, fields.get(FieldType.CUST_STREET_ADDRESS1.getName()), request);
		appendJsonField(Constants.ADDRESS_CITY, fields.get(FieldType.CUST_CITY.getName()), request);
		appendJsonField(Constants.ADDRESS_STATE, fields.get(FieldType.CUST_STATE.getName()), request);
		appendJsonField(Constants.ADDRESS_ZIP, fields.get(FieldType.CUST_ZIP.getName()), request);
		appendJsonField(Constants.EMAIL, fields.get(FieldType.CUST_EMAIL.getName()), request);
	}

	public void appendCustomParams(Fields fields, StringBuilder request){

		request.append(COMMA);
		request.append(QUOTE);
		request.append(Constants.CUSTOM_PARAMS);
		request.append(QUOTE);
		request.append(COLON);
		request.append(Constants.OPENING_SQUARE_BRACE);
		request.append(Constants.OPENING_BRACE);
		StringBuilder customParamArray = new StringBuilder();
		appendJsonField(Constants.NAME_STRING, Constants.CUSTOM_PARAM_NAME, customParamArray);
		appendJsonField(Constants.VALUE_STRING, fields.get(FieldType.PAY_ID.getName()), customParamArray);
		request.append(customParamArray);
		request.append(Constants.CLOSING_BRACE);
		request.append(Constants.CLOSING_SQUARE_BRACE);
	}

	public void appendBankInfo(Fields fields, StringBuilder request){
		appendJsonField(Constants.PAYMENT_MODE, Mapper.getPaymentMode(fields).toString(), request);		
		appendJsonField(Constants.BANK_CODE, NetBankingType.getCitruscode(fields.get(FieldType.MOP_TYPE.getName())), request);
	}

	public String createSubscriptionRequest(Fields fields , String subscriptionMerchantId,String vanity, String vaultToken){
		StringBuilder request = new StringBuilder();

		appendJsonField(Constants.SUBSCRIBER_EMAIL, fields.get(FieldType.CUST_EMAIL.getName()),request);
		appendJsonField(Constants.AUTHORIZED,true,request);
		appendJsonField(Constants.AUTH_REF_ID,fields.get(FieldType.TXN_ID.getName()),request);
		appendJsonField(Constants.MERCHANT_ID, subscriptionMerchantId,request);

		appendSubscriptionAmount(fields,request);
		//append card block
	//	appendSubscriptionCardInfo(fields,request, vanity);
		appendSubscriptionCardToken(fields, request, vanity, vaultToken); //applicable for card details at citrus end

		appendSubscriptionSchedule(fields,request); //optional only if cycle time is supported by merchant

		StringBuilder jsonRequest = new StringBuilder();

		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);
		return jsonRequest.toString();
	}

	public void appendSubscriptionAmount(Fields fields, StringBuilder request){

		String currencyCode = fields.get(FieldType.CURRENCY_CODE.getName());
		request.append(COMMA);
		request.append(QUOTE);
		request.append(Constants.AMOUNT);
		request.append(QUOTE);
		request.append(COLON);
		request.append(Constants.OPENING_BRACE);
		StringBuilder customAmountString = new StringBuilder();
		appendJsonField(Constants.VALUE_STRING, Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),currencyCode), customAmountString);
		appendJsonField(Constants.CURRENCY, Currency.getAlphabaticCode(currencyCode), customAmountString);
		request.append(customAmountString);
		request.append(Constants.CLOSING_BRACE);
	}

	//used when card details stored in PG DB
	public void appendSubscriptionCardInfo(Fields fields, StringBuilder request, String vanity){
		request.append(COMMA);
		request.append(QUOTE);
		request.append(Constants.CARD);
		request.append(QUOTE);
		request.append(COLON);

		request.append(Constants.OPENING_BRACE);
		StringBuilder cardObjString = new StringBuilder();
		appendJsonField(Constants.TYPE, Constants.TOKEN, cardObjString);
		//append card
		cardObjString.append(COMMA);
		cardObjString.append(QUOTE);
		cardObjString.append(Constants.CARD);
		cardObjString.append(QUOTE);
		cardObjString.append(COLON);
		cardObjString.append(Constants.OPENING_BRACE);		
		StringBuilder cardString = new StringBuilder();
		//append card
		appendJsonField(Constants.TYPE, Constants.CREDIT_CARD, cardString);
		appendJsonField(Constants.PAN, fields.get(FieldType.CARD_NUMBER.getName()), cardString);
		appendJsonField(Constants.EXPIRY, CitrusPayUtil.parseCardExpDate(fields.get(FieldType.CARD_EXP_DT.getName())), cardString);
		appendJsonField(Constants.HOLDER, fields.get(FieldType.CUST_NAME.getName()), cardString);
		cardObjString.append(cardString);
		cardObjString.append(Constants.CLOSING_BRACE);
		//append card
		request.append(cardObjString);
		appendHintBlock(fields, request, vanity);
		request.append(Constants.CLOSING_BRACE);
	}

	public void appendSubscriptionSchedule(Fields fields, StringBuilder request){
		String interval = fields.get(FieldType.RECURRING_TRANSACTION_INTERVAL.getName());
		ReccuringCycleType reccuringCycleType = ReccuringCycleType.getInstanceFromName(interval);
		if(null==reccuringCycleType){
			return;
			//TODO.. create job later or throw exception
		}
		request.append(COMMA);
		request.append(QUOTE);
		request.append(Constants.SCHEDULE);
		request.append(QUOTE);
		request.append(COLON);
		request.append(Constants.OPENING_BRACE);
		StringBuilder scheduleString = new StringBuilder();
		appendJsonField(Constants.BILLING_PERIOD_UNIT, Constants.ONE, scheduleString);
		appendJsonField(Constants.BILLING_PERIOD_CYCLE, fields.get(FieldType.RECURRING_TRANSACTION_INTERVAL.getName()), scheduleString);
		request.append(scheduleString);
		request.append(Constants.CLOSING_BRACE);
	}

	//used when card details stored at citrus end
	public void appendSubscriptionCardToken(Fields fields, StringBuilder request, String vanity, String vaultToken){
		request.append(COMMA);
		request.append(QUOTE);
		request.append(Constants.CARD);
		request.append(QUOTE);
		request.append(COLON);
		request.append(Constants.OPENING_BRACE);
		StringBuilder cardTokenString = new StringBuilder();
		appendJsonField(Constants.TYPE, Constants.TOKEN, cardTokenString);
		//append token
		appendJsonField(Constants.TOKEN, vaultToken, cardTokenString);
		request.append(cardTokenString);
		appendHintBlock(fields, request, vanity);
		request.append(Constants.CLOSING_BRACE);
	}

	public String createVaultTokenRequest(Fields fields, String vanity){
		StringBuilder request = new StringBuilder();
		request.append(QUOTE);
		request.append(Constants.CARD);
		request.append(QUOTE);
		request.append(COLON);
		request.append(Constants.OPENING_BRACE);
		StringBuilder cardString = new StringBuilder();
		//append card
		appendJsonField(Constants.PAN, fields.get(FieldType.CARD_NUMBER.getName()), cardString);
		appendJsonField(Constants.EXPIRY, CitrusPayUtil.parseCardExpDate(fields.get(FieldType.CARD_EXP_DT.getName())), cardString);
		appendJsonField(Constants.HOLDER, fields.get(FieldType.CUST_NAME.getName()), cardString);
		request.append(cardString);
		request.append(Constants.CLOSING_BRACE);
		appendHintBlock(fields, request, vanity);
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);
		return jsonRequest.toString();
	}

	public void appendHintBlock(Fields fields, StringBuilder request, String vanity){
		StringBuilder hintString = new StringBuilder();
		hintString.append(COMMA);
		hintString.append(QUOTE);
		hintString.append(Constants.HINT);
		hintString.append(QUOTE);
		hintString.append(COLON);
		hintString.append(Constants.OPENING_BRACE);
		StringBuilder hintObj = new StringBuilder();
		appendJsonField(Constants.MTX, fields.get(FieldType.TXN_ID.getName()), hintObj); //append txn ID
		appendJsonField(Constants.VANITY, vanity, hintObj);
		appendJsonField(Constants.EMAIL, fields.get(FieldType.CUST_EMAIL.getName()), hintObj);		
		hintString.append(hintObj);
		hintString.append(Constants.CLOSING_BRACE);
		request.append(hintString);
	}
	
	public String createAlterSubscriptionRequest(CitrusPaySubscription subscription, String status){
		StringBuilder request = new StringBuilder();

		appendJsonField(Constants.STATUS, status, request);
		appendJsonField(Constants.AUTH_REF_ID, subscription.getTransactionOid(), request);

		//append amount
		String currencyCode = Currency.getAlphabaticCode(subscription.getCurrency());
		request.append(COMMA);
		request.append(QUOTE);
		request.append(Constants.AMOUNT);
		request.append(QUOTE);
		request.append(COLON);
		request.append(Constants.OPENING_BRACE);
		StringBuilder customAmountString = new StringBuilder();
		appendJsonField(Constants.VALUE_STRING, subscription.getAmount(), customAmountString);
		appendJsonField(Constants.CURRENCY, currencyCode, customAmountString);
		request.append(customAmountString);
		request.append(Constants.CLOSING_BRACE);

		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);
		return jsonRequest.toString();
	}
}
