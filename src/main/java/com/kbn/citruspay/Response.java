/**
 * 
 */
package com.kbn.citruspay;

/**
 * @author Surender
 *
 */
public class Response {
	private String redirectUrl;
	private String respCode;
	private String respMsg;
	private String txnId;
	private String pgTxnId;
	private String authIdCode;
	private String rrn;
	private String txnType;
	private String txnDateTime;
	private String amount;
	private String transactionId;
	private String cardHolderName;
	private String currency;
	private String eciFlag;	
	
	//for recurring payments
	private String subscriptionId;
	private String billingPeriodUnit;
	private String billingPeriodCycle;
	private String lastPaymentDate;
	private String nextBillingDate;
	private String merchantId;
	
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public String getRespCode() {
		return respCode;
	}
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}
	public String getRespMsg() {
		return respMsg;
	}
	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getPgTxnId() {
		return pgTxnId;
	}
	public void setPgTxnId(String pgTxnId) {
		this.pgTxnId = pgTxnId;
	}
	public String getAuthIdCode() {
		return authIdCode;
	}
	public void setAuthIdCode(String authIdCode) {
		this.authIdCode = authIdCode;
	}
	public String getRrn() {
		return rrn;
	}
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getTxnDateTime() {
		return txnDateTime;
	}
	public void setTxnDateTime(String txnDateTime) {
		this.txnDateTime = txnDateTime;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getCardHolderName() {
		return cardHolderName;
	}
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getEciFlag() {
		return eciFlag;
	}
	public void setEciFlag(String eciFlag) {
		this.eciFlag = eciFlag;
	}
	public String getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	public String getBillingPeriodUnit() {
		return billingPeriodUnit;
	}
	public void setBillingPeriodUnit(String billingPeriodUnit) {
		this.billingPeriodUnit = billingPeriodUnit;
	}
	public String getBillingPeriodCycle() {
		return billingPeriodCycle;
	}
	public void setBillingPeriodCycle(String billingPeriodCycle) {
		this.billingPeriodCycle = billingPeriodCycle;
	}
	public String getLastPaymentDate() {
		return lastPaymentDate;
	}
	public void setLastPaymentDate(String lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}
	public String getNextBillingDate() {
		return nextBillingDate;
	}
	public void setNextBillingDate(String nextBillingDate) {
		this.nextBillingDate = nextBillingDate;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
}
