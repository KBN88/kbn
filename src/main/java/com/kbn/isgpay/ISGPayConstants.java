package com.kbn.isgpay;

public interface ISGPayConstants {
	
	public static final String EQUATOR = "=";
	public static final String SEPARATOR = "&";
	public static final String BANK_ID = "000004";
	public static final String VERSION = "1";

}
