package com.kbn.isgpay;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;


public class ISGPayRefundTransactionProcessor implements TransactionProcessor {
	
	private Logger logger = Logger.getLogger(ISGPayRefundTransactionProcessor.class.getName());
		
		private ISGPayTransactionConverter transactionConverter = new ISGPayTransactionConverter();
		private ISGPayTransactionCommunicator communicator = new ISGPayTransactionCommunicator();
		
		
		@Override
		public void transact(Fields fields) throws SystemException {
			String request = "";
			try {
				request = transactionConverter.createRefundRequest(fields);
			} catch (Exception exception) {
				logger.error("ISGPAY Refund Converter "+exception);
			}
			String refundResponse = "";
			try {
				refundResponse = communicator.transact(request, ConfigurationConstants.ISGPAY_REFUND_URL.getValue());
				logger.info("ISGPay Refund Transaction Response:-" + refundResponse);
			} catch (Exception exception) {
				logger.error("ISGPAY Refund "+exception);
			}
			
			//update refund response
			ISGPayTransformer transformer = new ISGPayTransformer(refundResponse);
				transformer.updateISGPayRefundResponse(fields);
												
	}

}