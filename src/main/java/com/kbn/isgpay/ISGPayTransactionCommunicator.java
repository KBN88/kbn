package com.kbn.isgpay;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.SystemConstants;

public class ISGPayTransactionCommunicator {
	private static Logger logger = Logger.getLogger(ISGPayTransactionCommunicator.class.getName());

	public String sendAuthorization(String request, Fields fields) throws SystemException {
		PrintWriter out;
		String response = "";
		logger.info("Request sent to ISG Pay: " + request);
		try {
			out = ServletActionContext.getResponse().getWriter();

			out.write(request);
			// Return response
			response = ErrorType.SUCCESS.getCode();
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());

		} catch (IOException iOException) {
			logger.error(iOException);
			response = ErrorType.UNKNOWN.getCode();
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, iOException, "Network Exception with ISGPAY");
		}
		return response;
	}

	// sending Refund/Status Request as form POST
	public String transact(String request, String hostUrl) throws SystemException {

		String response = "";
		try {

			URL url = new URL(hostUrl + request);

			logger.info("*************************Request String******" + request);
			logger.info("***********************URL*************" + url);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setRequestProperty("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestMethod("POST");

			DataOutputStream dataoutputstream = new DataOutputStream(connection.getOutputStream());

			dataoutputstream.write(request.getBytes(SystemConstants.DEFAULT_ENCODING_UTF_8));
			dataoutputstream.flush();
			dataoutputstream.close();

			BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String decodedString;

			while ((decodedString = bufferedreader.readLine()) != null) {
				response = response + decodedString;
			}
			bufferedreader.close();
			connection.disconnect();
		} catch (IOException ioException) {
			logger.error(ioException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with ISGPAY for " + hostUrl.toString());
		}

		return response;
	}
}