package com.kbn.isgpay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionSearchService;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;

public class ISGPayStatusEnquirer {
	private static Logger logger = Logger.getLogger(ISGPayStatusEnquirer.class.getName());

	public void statusEnquirer() {

		TransactionSearchService transactionSearchService = new TransactionSearchService();
		try {
			List<TransactionSummaryReport> transactionList = transactionSearchService
					.getTransactionsForStatusUpdate(AcquirerType.ISGPAY.getCode());
			for (TransactionSummaryReport transaction : transactionList) {

				Map<String, String> reqMap = prepareFields(transaction);
				Fields fields = new Fields(reqMap);

				ISGPayStatusTransactionProcessor processor = new ISGPayStatusTransactionProcessor();
				processor.transact(fields);
				
					// update new order transaction
					fields.updateStatus();
					// Update sale transaction
					fields.updateTransactionDetails();

			}
		} catch (SystemException systemException) {
			logger.error("Error updating status for ISGPAY", systemException);
		}
	}

	private Map<String, String> prepareFields(TransactionSummaryReport transaction) {
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put(FieldType.PAY_ID.getName(), transaction.getPayId());
		requestMap.put(FieldType.TXN_ID.getName(), transaction.getTransactionId());
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), transaction.getOrigTransactionId());
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.ISGPAY.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), transaction.getCurrencyCode());
		return requestMap;
	}
	
	public String manualStatusEnquirer(String orderId) {
		String response = "";
		try {
				
				Map<String, String> reqMap = prepareFields(orderId);
				Fields fields = new Fields(reqMap);
				
				ISGPayStatusTransactionProcessor processor = new ISGPayStatusTransactionProcessor();
				processor.transact(fields);
				
				// update new order transaction
				fields.updateStatus();
				// Update sale transaction
				fields.updateTransactionDetails();
					
				response = fields.get(FieldType.STATUS.getName());

		} catch (SystemException systemException) {
			logger.error("Error updating manual status for Payplutus", systemException);
		}
		
		return response;
	}

	private Map<String, String> prepareFields(String txnID) throws SystemException {
		
		//Fetching PayId and Original transaction id
		ISGPayTransactionFetchService isgPayTransactionFetchService = new ISGPayTransactionFetchService();
		Map<String, String> responseMap = isgPayTransactionFetchService.fetchISGPayOrigTxn(txnID);
		
		
		Map<String, String> requestMap = new HashMap<String, String>();
		
		requestMap.put(FieldType.TXN_ID.getName(), txnID);
		requestMap.put(FieldType.PAY_ID.getName(), responseMap.get("payId"));
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), responseMap.get("origTxnId"));
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.ISGPAY.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());

		return requestMap;
	}

}
