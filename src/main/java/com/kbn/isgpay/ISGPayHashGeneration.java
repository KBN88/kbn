package com.kbn.isgpay;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class ISGPayHashGeneration {
	public ISGPayHashGeneration() {
	}

	protected String generateHash(LinkedHashMap<String, String> hmReqFields, String sSecureSecret) throws Exception {
		StringBuffer hexString = new StringBuffer();

		try {
			hmReqFields.remove("inprocess");

			ArrayList<String> fieldNames = new ArrayList(hmReqFields.keySet());
			Collections.sort(fieldNames);

			StringBuffer buf = new StringBuffer();

			buf.append(sSecureSecret);

			Iterator<String> itr = fieldNames.iterator();
			String hashKeys = "";

			while (itr.hasNext()) {
				String fieldName = (String) itr.next();
				String fieldValue = (String) hmReqFields.get(fieldName);
				hashKeys = hashKeys + fieldName + ", ";
				if ((fieldValue != null) && (fieldValue.length() > 0)) {
					buf.append(fieldValue);
				}
			}

			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(buf.toString().getBytes("UTF-8"));
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xFF & hash[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}

			hmReqFields.put("SecureHash", hexString.toString());
		} catch (Exception e) {
			System.out.println("Exception Occured in ISGPayHashGeneration.generateHash():" + e);
			throw e;
		}

		return hexString.toString();
	}
}