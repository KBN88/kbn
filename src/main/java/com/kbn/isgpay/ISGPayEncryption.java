package com.kbn.isgpay;

import java.security.Key;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;

public class ISGPayEncryption extends ISGPayHashGeneration {
	private static String[] parameters = { "MerchantId", "TerminalId", "BankId", "Version", "ReturnURL", "TxnType",
			"PassCode", "MCC", "Currency", "Amount", "TxnRefNo" };

	private String MERCHANT_ID;

	private String BANK_ID;

	private String TERMINAL_ID;

	private String VERSION;
	private String ENC_DATA;

	public ISGPayEncryption() {
	}

	public void encrypt(HttpServletRequest request, String sEncryptionKey, String sSecureSecret) throws Exception {
		try {
			if (request != null) {
				for (int i = 0; i < parameters.length; i++) {
					if ((request.getParameter(parameters[i]) == null)
							|| ("".equals(request.getParameter(parameters[i])))
							|| ("null".equalsIgnoreCase(request.getParameter(parameters[i])))) {
						throw new Exception("Missing Parameters:::" + parameters[i]);
					}
				}

				if ((sSecureSecret != null) && (!"".equals(sSecureSecret))) {
					if ((sEncryptionKey != null) && (!"".equals(sEncryptionKey))) {
						LinkedHashMap<String, String> hmReqFields = new LinkedHashMap();
						Enumeration e = request.getParameterNames();

						while (e.hasMoreElements()) {
							String fieldName = (String) e.nextElement();
							String fieldValue = request.getParameter(fieldName);

							if ((fieldValue != null) && (fieldValue.length() > 0)) {
								hmReqFields.put(fieldName, fieldValue);
							}
						}

						generateHash(hmReqFields, sSecureSecret);

						doISGPayEncrypt(hmReqFields, sEncryptionKey);

						if ("".equals(ENC_DATA)) {
							throw new Exception("Problem during encryption....");
						}
						MERCHANT_ID = ((String) hmReqFields.get("MerchantId"));
						BANK_ID = ((String) hmReqFields.get("BankId"));
						TERMINAL_ID = ((String) hmReqFields.get("TerminalId"));
						VERSION = ((String) hmReqFields.get("Version"));
					} else {
						throw new Exception("Encryption Key can not be null or empty!!!");
					}
				} else {
					throw new Exception("SecureSecret can not be null or empty!!!");
				}
			} else {
				throw new Exception("Error Parsing Request!!!");
			}
		} catch (Exception e) {
			System.out.println("Exception Occured in ISGPayEncryption.encrypt(request):" + e);
			throw e;
		}
	}

	public void encrypt(LinkedHashMap<String, String> hmReqFields, String sMerchantID, String sTerminalID,
			String sBankID, String sVersionNum, String sEncryptionKey, String sSecureSecret) throws Exception {
		try {
			if ((sMerchantID == null) || ("".equals(sMerchantID))) {
				throw new Exception("Merchant ID Can Not Be Empty or Null!!!");
			}
			if ((sTerminalID == null) || ("".equals(sTerminalID))) {
				throw new Exception("Terminal ID Can Not Be Empty or Null!!!");
			}
			if ((sBankID == null) || ("".equals(sBankID))) {
				throw new Exception("Bank ID Can Not Be Empty or Null!!!");
			}
			if ((sVersionNum == null) || ("".equals(sVersionNum))) {
				throw new Exception("Version Number Can Not Be Empty or Null!!!");
			}

			for (int i = 0; i < parameters.length; i++) {
				if ((!parameters[i].equals("MerchantId")) && (!parameters[i].equals("TerminalId"))
						&& (!parameters[i].equals("BankId")) && (!parameters[i].equals("Version"))) {

					if ((hmReqFields.get(parameters[i]) == null) || ("".equals(hmReqFields.get(parameters[i])))
							|| ("null".equalsIgnoreCase((String) hmReqFields.get(parameters[i])))) {
						throw new Exception("Missing Parameters:::" + parameters[i]);
					}
				}
			}

			if ((sSecureSecret != null) && (!"".equals(sSecureSecret))) {
				if ((sEncryptionKey != null) && (!"".equals(sEncryptionKey))) {
					hmReqFields.put("MerchantId", sMerchantID);
					hmReqFields.put("TerminalId", sTerminalID);
					hmReqFields.put("BankId", sBankID);
					hmReqFields.put("Version", sVersionNum);

					generateHash(hmReqFields, sSecureSecret);

					doISGPayEncrypt(hmReqFields, sEncryptionKey);

					if ("".equals(ENC_DATA)) {
						throw new Exception("Problem during encryption....");
					}

					MERCHANT_ID = sMerchantID;
					BANK_ID = sBankID;
					TERMINAL_ID = sTerminalID;
					VERSION = sVersionNum;
				} else {
					throw new Exception("Encryption Key can not be null or empty!!!");
				}
			} else {
				throw new Exception("SecureSecret can not be null or empty!!!");
			}
		} catch (Exception e) {
			System.out.println("Exception Occured in ISGPayEncryption.encrypt(String):" + e);
			throw e;
		}
	}

	public void encrypt(LinkedHashMap<String, String> hmReqFields, String sEncryptionKey, String sSecureSecret)
			throws Exception {
		try {
			for (int i = 0; i < parameters.length; i++) {
				if ((hmReqFields.get(parameters[i]) == null) || ("".equals(hmReqFields.get(parameters[i])))
						|| ("null".equalsIgnoreCase((String) hmReqFields.get(parameters[i])))) {
					throw new Exception("Missing Parameters:::" + parameters[i]);
				}
			}

			if ((sSecureSecret != null) && (!"".equals(sSecureSecret))) {
				if ((sEncryptionKey != null) && (!"".equals(sEncryptionKey))) {
					generateHash(hmReqFields, sSecureSecret);

					doISGPayEncrypt(hmReqFields, sEncryptionKey);

					if ("".equals(ENC_DATA)) {
						throw new Exception("Problem during encryption....");
					}

					MERCHANT_ID = ((String) hmReqFields.get("MerchantId"));
					BANK_ID = ((String) hmReqFields.get("BankId"));
					TERMINAL_ID = ((String) hmReqFields.get("TerminalId"));
					VERSION = ((String) hmReqFields.get("Version"));
				} else {
					throw new Exception("Encryption Key can not be null or empty!!!");
				}
			} else {
				throw new Exception("SecureSecret can not be null or empty!!!");
			}
		} catch (Exception e) {
			System.out.println("Exception Occured in ISGPayEncryption.encrypt(Collection):" + e);
			throw e;
		}
	}

	private void doISGPayEncrypt(LinkedHashMap<String, String> hmReqFields, String sEncryptionKey) throws Exception {
		try {
			StringBuffer sb = new StringBuffer();
			for (Iterator<String> itr = hmReqFields.keySet().iterator(); itr.hasNext();) {
				String fieldName = (String) itr.next();
				sb.append(fieldName);
				sb.append("||");
				sb.append((String) hmReqFields.get(fieldName));
				sb.append("::");
			}

			byte[] keyByte = sEncryptionKey.getBytes();
			Key key = new SecretKeySpec(keyByte, "AES");
			Cipher c = Cipher.getInstance("AES");
			c.init(1, key);
			byte[] encVal = c.doFinal(sb.toString().getBytes());
			byte[] encryptedByteValue = new Base64().encode(encVal);

			ENC_DATA = new String(encryptedByteValue);
		} catch (Exception e) {
			System.out.println("Exception Occured in ISGPayHashGeneration.generateHash():" + e);
			throw e;
		}
	}

	public String getMERCHANT_ID() {
		return MERCHANT_ID;
	}

	public String getBANK_ID() {
		return BANK_ID;
	}

	public String getTERMINAL_ID() {
		return TERMINAL_ID;
	}

	public String getVERSION() {
		return VERSION;
	}

	public String getENC_DATA() {
		return ENC_DATA;
	}
}
