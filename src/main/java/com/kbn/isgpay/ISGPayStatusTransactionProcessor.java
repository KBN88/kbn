package com.kbn.isgpay;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class ISGPayStatusTransactionProcessor implements TransactionProcessor {

	private Logger logger = Logger.getLogger(ISGPayStatusTransactionProcessor.class.getName());

	private ISGPayTransactionConverter transactionConverter = new ISGPayTransactionConverter();
	private ISGPayTransactionCommunicator communicator = new ISGPayTransactionCommunicator();

	@Override
	public void transact(Fields fields) throws SystemException {
		String request = "";
		try {
			request = transactionConverter.createStatusRequest(fields);
		} catch (Exception exception) {
			logger.error("ISGPAY Status Converter " + exception);
		}
		String statusResponse = "";
		try {
			statusResponse = communicator.transact(request, ConfigurationConstants.ISGPAY_STATUS_URL.getValue());
			logger.info("ISGPay Status Enquiry Response:-" + statusResponse);
		} catch (Exception exception) {
			logger.error("ISGPAY Status Communicator " + exception);
		}

		try {
			ISGPayTransformer transformer = new ISGPayTransformer(statusResponse);
			transformer.updateISGPayStatusResponse(fields);
			fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));

		} catch (Exception exception) {
			logger.error("ISGPAY Status Transformer " + exception);
		}

	}

}
