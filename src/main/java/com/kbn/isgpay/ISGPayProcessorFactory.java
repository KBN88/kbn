package com.kbn.isgpay;

import com.kbn.pg.core.Processor;

public class ISGPayProcessorFactory {
	public static Processor getInstance() {
		return new ISGPayProcessor();
	}

}