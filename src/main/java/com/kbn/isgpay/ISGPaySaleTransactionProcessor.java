package com.kbn.isgpay;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.TransactionProcessor;

public class ISGPaySaleTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(ISGPaySaleTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {

		ISGPayTransactionConverter converter = new ISGPayTransactionConverter();
		ISGPayTransactionCommunicator communicator = new ISGPayTransactionCommunicator();

		String request = converter.createSaleRequest(fields);
		logger.info(" Sale Request to ISG Pay: " + request);

		communicator.sendAuthorization(request, fields);
		fields.put(FieldType.STATUS.getName(), StatusType.SENT_TO_BANK.getName());
	}

}