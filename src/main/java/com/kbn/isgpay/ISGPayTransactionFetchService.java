package com.kbn.isgpay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;

public class ISGPayTransactionFetchService {

	private static Logger logger = Logger.getLogger(ISGPayTransactionFetchService.class.getName());

	private User user = new User();
	private static final String isgPayAuthCodeQuery = "select AUTH_CODE from TRANSACTION where TXN_ID=?";
	private static final String origTxnQuery = "select ORIG_TXN_ID, PAY_ID  from TRANSACTION where TXN_ID=?";
	private static final String isgPayQuery = "select PG_GATEWAY, PAY_ID from TRANSACTION where TXN_ID=?";
	private static final String isgPayQueryForUser = "select encryptionKey, secureKey from SIGN_USER U where U.terminalId = ?";

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public String fetchISGPayAuthCode(String txn_id) throws SystemException {
		String authCode = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(isgPayAuthCodeQuery)) {
				preparedStatement.setString(1, txn_id);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						authCode = resultSet.getString("AUTH_CODE");
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching Auth Code for ISGPAY Refund Request " + sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return authCode;
	}// fetchISGPayAuthCode

	public Map<String, String> fetchISGPayOrigTxn(String txnId) throws SystemException {
		Map<String, String> requestMap = new HashMap<String, String>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(origTxnQuery)) {
				preparedStatement.setString(1, txnId);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						requestMap.put("origTxnId", resultSet.getString("ORIG_TXN_ID"));
						requestMap.put("payId", resultSet.getString("PAY_ID"));
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching Original Transaction Id for ISGPay Manual status enquery "
					+ sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return requestMap;
	}// fetchISGPayOrigTxn

	public Map<String, String> fetchISGPayTxn(String txn_id) throws SystemException {
		Map<String, String> requestMap = new HashMap<String, String>();
		UserDao userDao = new UserDao();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(isgPayQuery)) {
				preparedStatement.setString(1, txn_id);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						requestMap.put("merchantId", resultSet.getString("PG_GATEWAY"));
						requestMap.put("payId", resultSet.getString("PAY_ID"));

						// get Decrypted password
						setUser(userDao.findPayId(requestMap.get("payId")));
						String decryptedPassword = retrieveAndDecryptPass(user, requestMap.get("merchantId"));

						requestMap.put("passCode", decryptedPassword);
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error(
					"Database error while fetching Merchant Id and PassCode for ISGPAY Status Request " + sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return requestMap;
	}// fetchISGPayTxn

	public Map<String, String> fetchISGPayFields(String terminal_id) throws SystemException {
		Map<String, String> requestMap = new HashMap<String, String>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(isgPayQueryForUser)) {
				preparedStatement.setString(1, terminal_id);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						requestMap.put("encryptionKey", resultSet.getString("encryptionKey"));
						requestMap.put("secureKey", resultSet.getString("secureKey"));
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching Secure Key and Encryption Key for ISGPAY Response Handling "
					+ sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return requestMap;
	}// fetchISGPayFields

	// password Decryption
	public String retrieveAndDecryptPass(User user, String merchantId) {
		String decryptedPassword = "";
		Set<Account> accounts = user.getAccounts();
		for (Account account : accounts) {
			Set<AccountCurrency> accountCurrencySet = account.getAccountCurrencySet();
			for (AccountCurrency accountCurrency : accountCurrencySet) {
				if (!StringUtils.isAnyEmpty(accountCurrency.getPassword())
						&& (accountCurrency.getMerchantId().equals(merchantId))) {
					decryptedPassword = AccountPasswordScrambler.decryptPassword(accountCurrency.getPassword());

				}
			}
		}
		return decryptedPassword;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}