package com.kbn.isgpay;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.NetBankingType;
import com.kbn.commons.util.PaymentType;

public class ISGPayTransactionConverter implements ISGPayConstants {

	private Logger logger = Logger.getLogger(ISGPayTransactionConverter.class.getName());
	private User user = new User();

	public String createSaleRequest(Fields fields) {
		UserDao userDao = new UserDao();
		setUser(userDao.findPayId(fields.get(FieldType.PAY_ID.getName())));

		String payOpt = fields.get(FieldType.PAYMENT_TYPE.getName());
		if (payOpt.equals("CC")) {
			payOpt = "cc";
		} else if (payOpt.equals("DC")) {
			payOpt = "dc";
		} else if (payOpt.equals("NB")) {
			payOpt = "nb";
		}

		ISGPayEncryption encObj = new ISGPayEncryption();

		String ISGPayTransactionURL = ConfigurationConstants.ISGPAY_TRANSACTION_URL.getValue();

		// Fetching ISGPay additional fields from User table
		String secureSecret = user.getSecureKey();
		String encrptionKey = user.getEncryptionKey();
		String mccCode = user.getMccCode();
		String terminalId = user.getTerminalId();

		LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();

		// Net Banking Request
		if (PaymentType.NET_BANKING.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {

			linkedHashMap.put("Amount", fields.get(FieldType.AMOUNT.getName()));
			linkedHashMap.put("bankCode", NetBankingType.getISGPayCode(fields.get(FieldType.MOP_TYPE.getName())));
			linkedHashMap.put("BankId", BANK_ID);
			linkedHashMap.put("Currency", fields.get(FieldType.CURRENCY_CODE.getName()));
			linkedHashMap.put("MCC", mccCode);
			linkedHashMap.put("MerchantId", fields.get(FieldType.MERCHANT_ID.getName()));
			linkedHashMap.put("PassCode", fields.get(FieldType.PASSWORD.getName()));
			linkedHashMap.put("payOpt", payOpt);
			linkedHashMap.put("ReturnURL", ConfigurationConstants.ISGPAY_RETURN_URL.getValue());
			linkedHashMap.put("TerminalId", terminalId);
			linkedHashMap.put("TxnType", "Pay");
			linkedHashMap.put("TxnRefNo", fields.get(FieldType.TXN_ID.getName()));
			linkedHashMap.put("Version", VERSION);
		} // Credit/Debit Card Request
		else {
			linkedHashMap.put("Amount", fields.get(FieldType.AMOUNT.getName()));
			linkedHashMap.put("BankId", BANK_ID);
			linkedHashMap.put("Currency", fields.get(FieldType.CURRENCY_CODE.getName()));
			linkedHashMap.put("CardNumber", fields.get(FieldType.CARD_NUMBER.getName()));
			linkedHashMap.put("CardSecurityCode", fields.get(FieldType.CVV.getName()));
			linkedHashMap.put("ExpiryDate", fields.get(FieldType.CARD_EXP_DT.getName()));
			linkedHashMap.put("MCC", mccCode);
			linkedHashMap.put("MerchantId", fields.get(FieldType.MERCHANT_ID.getName()));
			linkedHashMap.put("PassCode", fields.get(FieldType.PASSWORD.getName()));
			linkedHashMap.put("payOpt", payOpt);
			linkedHashMap.put("ReturnURL", ConfigurationConstants.ISGPAY_RETURN_URL.getValue());
			linkedHashMap.put("TerminalId", terminalId);
			linkedHashMap.put("TxnType", "Pay");
			linkedHashMap.put("TxnRefNo", fields.get(FieldType.TXN_ID.getName()));
			linkedHashMap.put("Version", VERSION);
		}

		// hashing and encryption of request parameters
		try {
			encObj.encrypt(linkedHashMap, encrptionKey, secureSecret);
		} catch (Exception exception) {
			logger.error("ISGPAY Sale Request Encryption/Hashing " + exception);
		}

		StringBuilder httpRequest = new StringBuilder();

		// Net Banking Request/CC/DC Form Post Request
		httpRequest.append("<HTML>");
		httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
		httpRequest.append("<form name=\"form1\" action=\"");
		httpRequest.append(ISGPayTransactionURL);
		httpRequest.append("\" method=\"post\">");
		httpRequest.append("<input type=\"hidden\" name=\"MerchantId\" id=\"MerchantId\" value=\"");
		httpRequest.append(encObj.getMERCHANT_ID());
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"TerminalId\" id=\"TerminalId\" value=\"");
		httpRequest.append(encObj.getTERMINAL_ID());
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"BankId\" id=\"BankId\" value=\"");
		httpRequest.append(encObj.getBANK_ID());
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"Version\" id=\"Version\" value=\"");
		httpRequest.append(encObj.getVERSION());
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"EncData\" id=\"EncData\" value=\"");
		httpRequest.append(encObj.getENC_DATA());
		httpRequest.append("\">");
		httpRequest.append("</form>");
		httpRequest.append("<script language=\"JavaScript\">");
		httpRequest.append("function OnLoadEvent()");
		httpRequest.append("{document.form1.submit();}");
		httpRequest.append("</script>");
		httpRequest.append("</BODY>");
		httpRequest.append("</HTML>");

		return httpRequest.toString();
	}// createSaleRequest

	// create Refund Request
	public String createRefundRequest(Fields fields) throws Exception {

		// Fetching Auth Code from database
		ISGPayTransactionFetchService isgPayTransactionFetchService = new ISGPayTransactionFetchService();
		String authCode = isgPayTransactionFetchService
				.fetchISGPayAuthCode(fields.get(FieldType.ORIG_TXN_ID.getName()));

		// Fetching ISGPay additional fields from User table
		UserDao userDao = new UserDao();
		setUser(userDao.findPayId(fields.get(FieldType.PAY_ID.getName())));

		LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();

		if (authCode != null) {
			linkedHashMap.put("AuthCode", authCode);
		}
		linkedHashMap.put("BankId", BANK_ID);
		linkedHashMap.put("MerchantId", fields.get(FieldType.MERCHANT_ID.getName()));
		linkedHashMap.put("PassCode", fields.get(FieldType.PASSWORD.getName()));
		linkedHashMap.put("RefundAmount", fields.get(FieldType.AMOUNT.getName()));
		linkedHashMap.put("RetRefNo", fields.get(FieldType.PG_REF_NUM.getName()));
		linkedHashMap.put("TerminalId", user.getTerminalId());
		linkedHashMap.put("TxnType", "Refund");
		linkedHashMap.put("TxnRefNo", fields.get(FieldType.ORIG_TXN_ID.getName()));

		// hash generated
		ISGPayHashGeneration hashObj = new ISGPayHashGeneration();
		String secureHash = hashObj.generateHash(linkedHashMap, user.getSecureKey());

		// creating request for form POST
		StringBuilder httpRequest = new StringBuilder();

		if (authCode != null) {
			httpRequest.append("AuthCode");
			httpRequest.append(EQUATOR);
			httpRequest.append(authCode);
			httpRequest.append(SEPARATOR);
		}
		httpRequest.append("BankId");
		httpRequest.append(EQUATOR);
		httpRequest.append(BANK_ID);
		httpRequest.append(SEPARATOR);
		httpRequest.append("MerchantId");
		httpRequest.append(EQUATOR);
		httpRequest.append(fields.get(FieldType.MERCHANT_ID.getName()));
		httpRequest.append(SEPARATOR);
		httpRequest.append("PassCode");
		httpRequest.append(EQUATOR);
		httpRequest.append(fields.get(FieldType.PASSWORD.getName()));
		httpRequest.append(SEPARATOR);
		httpRequest.append("RefundAmount");
		httpRequest.append(EQUATOR);
		httpRequest.append(fields.get(FieldType.AMOUNT.getName()));
		httpRequest.append(SEPARATOR);
		httpRequest.append("RetRefNo");
		httpRequest.append(EQUATOR);
		httpRequest.append(fields.get(FieldType.PG_REF_NUM.getName()));
		httpRequest.append(SEPARATOR);
		httpRequest.append("SecureHash");
		httpRequest.append(EQUATOR);
		httpRequest.append(secureHash);
		httpRequest.append(SEPARATOR);
		httpRequest.append("TerminalId");
		httpRequest.append(EQUATOR);
		httpRequest.append(user.getTerminalId());
		httpRequest.append(SEPARATOR);
		httpRequest.append("TxnType");
		httpRequest.append(EQUATOR);
		httpRequest.append("Refund");
		httpRequest.append(SEPARATOR);
		httpRequest.append("TxnRefNo");
		httpRequest.append(EQUATOR);
		httpRequest.append(fields.get(FieldType.ORIG_TXN_ID.getName()));

		return httpRequest.toString();

	}// createRefundRequest

	// create Status Request
	public String createStatusRequest(Fields fields) throws Exception {

		// Fetching merchant ID and Pass Code from database
		ISGPayTransactionFetchService isgPayTransactionFetchService = new ISGPayTransactionFetchService();
		Map<String, String> requestMap = isgPayTransactionFetchService
				.fetchISGPayTxn(fields.get(FieldType.TXN_ID.getName()));

		// Fetching ISGPay additional fields from User table
		UserDao userDao = new UserDao();
		setUser(userDao.findPayId(fields.get(FieldType.PAY_ID.getName())));

		LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();

		linkedHashMap.put("BankId", BANK_ID);
		linkedHashMap.put("MerchantId", requestMap.get("merchantId"));
		linkedHashMap.put("PassCode", requestMap.get("passCode"));
		linkedHashMap.put("TerminalId", user.getTerminalId());
		linkedHashMap.put("TxnType", "Status");
		linkedHashMap.put("TxnRefNo", fields.get(FieldType.TXN_ID.getName()));

		// hash generated
		ISGPayHashGeneration hashObj = new ISGPayHashGeneration();
		String secureHash = hashObj.generateHash(linkedHashMap, user.getSecureKey());

		// creating request for form POST
		StringBuilder httpRequest = new StringBuilder();

		httpRequest.append("BankId");
		httpRequest.append(EQUATOR);
		httpRequest.append(BANK_ID);
		httpRequest.append(SEPARATOR);
		httpRequest.append("MerchantId");
		httpRequest.append(EQUATOR);
		httpRequest.append(requestMap.get("merchantId"));
		httpRequest.append(SEPARATOR);
		httpRequest.append("PassCode");
		httpRequest.append(EQUATOR);
		httpRequest.append(requestMap.get("passCode"));
		httpRequest.append(SEPARATOR);
		httpRequest.append("SecureHash");
		httpRequest.append(EQUATOR);
		httpRequest.append(secureHash);
		httpRequest.append(SEPARATOR);
		httpRequest.append("TerminalId");
		httpRequest.append(EQUATOR);
		httpRequest.append(user.getTerminalId());
		httpRequest.append(SEPARATOR);
		httpRequest.append("TxnType");
		httpRequest.append(EQUATOR);
		httpRequest.append("Status");
		httpRequest.append(SEPARATOR);
		httpRequest.append("TxnRefNo");
		httpRequest.append(EQUATOR);
		httpRequest.append(fields.get(FieldType.TXN_ID.getName()));

		return httpRequest.toString();

	}// createStatusRequest

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}