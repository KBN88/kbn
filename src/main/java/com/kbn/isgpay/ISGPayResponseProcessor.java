package com.kbn.isgpay;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;

public class ISGPayResponseProcessor implements Processor {
	public void preProcess(Fields fields) throws SystemException {

	}

	public void process(Fields fields) throws SystemException {
		String TxanId = fields.get(FieldType.TXN_ID.getName());
		fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), fields.get(FieldType.TXNTYPE.getName()));
		if (null != fields.get(FieldType.TXN_ID.getName()) && !TxanId.equals("NA")) {
			fields.updateNewOrderDetails();
			fields.updateTransactionDetails();
		}

	}

	public void postProcess(Fields fields) throws SystemException {
		fields.removeInternalFields();
		new ResponseCreator().create(fields);

	}

}
