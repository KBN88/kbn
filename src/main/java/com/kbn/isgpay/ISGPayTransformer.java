package com.kbn.isgpay;

import java.util.LinkedHashMap;
import java.util.Map;

import com.google.common.base.Splitter;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class ISGPayTransformer {

	private LinkedHashMap<String, String> responseMap;
	private String responseMapISGPay;

	public ISGPayTransformer(LinkedHashMap<String, String> hmDecryptedValue) {
		this.responseMap = hmDecryptedValue;
	}

	public ISGPayTransformer(String response) {
		this.responseMapISGPay = response;
	}

	public ErrorType getSaleResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("00")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("NBF")) {
			errorType = ErrorType.TXN_FAILED;
		} else if (respCode.equals("VER") || respCode.equals("IVR")) {
			errorType = ErrorType.VALIDATION_FAILED;
		} else if (respCode.equals("HNM")) {
			errorType = ErrorType.INVALID_HASH;
		} else if (respCode.equals("MNE")) {
			errorType = ErrorType.AUTHENTICATION_UNAVAILABLE;
		} else if (respCode.equals("STO")) {
			errorType = ErrorType.TIMEOUT;
		} else if (respCode.equals("CAN")) {
			errorType = ErrorType.CANCELLED;
		} else if (respCode.equals("RAE")) {
			errorType = ErrorType.INVALID_AMOUNT;
		} else if (respCode.equals("RDF") || respCode.equals("DOI")) {
			errorType = ErrorType.DUPLICATE;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getSaleStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("00")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("VER") || respCode.equals("IVR") || respCode.equals("RAE")) {
			status = StatusType.INVALID;
		} else if (respCode.equals("HNM") || respCode.equals("MNE")) {
			status = StatusType.AUTHENTICATION_FAILED;
		} else if (respCode.equals("STO")) {
			status = StatusType.TIMEOUT;
		} else if (respCode.equals("CAN")) {
			status = StatusType.CANCELLED;
		} else if (respCode.equals("NBF")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("RDF") || respCode.equals("DOI")) {
			status = StatusType.DUPLICATE;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public ErrorType getRefundResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("00")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("VER") || respCode.equals("IVR")) {
			errorType = ErrorType.VALIDATION_FAILED;
		} else if (respCode.equals("HNM")) {
			errorType = ErrorType.INVALID_HASH;
		} else if (respCode.equals("MNE")) {
			errorType = ErrorType.AUTHENTICATION_UNAVAILABLE;
		} else if (respCode.equals("STO")) {
			errorType = ErrorType.TIMEOUT;
		} else if (respCode.equals("CAN")) {
			errorType = ErrorType.CANCELLED;
		} else if (respCode.equals("RAE")) {
			errorType = ErrorType.INVALID_AMOUNT;
		} else if (respCode.equals("RDF") || respCode.equals("DOI")) {
			errorType = ErrorType.DUPLICATE;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getRefundStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("00")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("VER") || respCode.equals("IVR") || respCode.equals("RAE")) {
			status = StatusType.INVALID;
		} else if (respCode.equals("HNM") || respCode.equals("MNE")) {
			status = StatusType.AUTHENTICATION_FAILED;
		} else if (respCode.equals("STO")) {
			status = StatusType.TIMEOUT;
		} else if (respCode.equals("CAN")) {
			status = StatusType.CANCELLED;
		} else if (respCode.equals("RDF") || respCode.equals("DOI")) {
			status = StatusType.DUPLICATE;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public ErrorType getStatusResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("00")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("VER") || respCode.equals("IVR")) {
			errorType = ErrorType.VALIDATION_FAILED;
		} else if (respCode.equals("HNM")) {
			errorType = ErrorType.INVALID_HASH;
		} else if (respCode.equals("MNE")) {
			errorType = ErrorType.AUTHENTICATION_UNAVAILABLE;
		} else if (respCode.equals("STO")) {
			errorType = ErrorType.TIMEOUT;
		} else if (respCode.equals("CAN")) {
			errorType = ErrorType.CANCELLED;
		} else if (respCode.equals("RAE")) {
			errorType = ErrorType.INVALID_AMOUNT;
		} else if (respCode.equals("RDF") || respCode.equals("DOI")) {
			errorType = ErrorType.DUPLICATE;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("00")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("VER") || respCode.equals("IVR") || respCode.equals("RAE")) {
			status = StatusType.INVALID;
		} else if (respCode.equals("HNM") || respCode.equals("MNE")) {
			status = StatusType.AUTHENTICATION_FAILED;
		} else if (respCode.equals("STO")) {
			status = StatusType.TIMEOUT;
		} else if (respCode.equals("CAN")) {
			status = StatusType.CANCELLED;
		} else if (respCode.equals("RDF") || respCode.equals("DOI")) {
			status = StatusType.DUPLICATE;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	// Sale Transaction
	public void updateISGPayResponse(Fields fields) throws SystemException {

		StatusType status = getSaleStatus(responseMap.get("ResponseCode"));
		ErrorType errorType = getRefundResponse(responseMap.get("ResponseCode"));

		fields.put(FieldType.MERCHANT_ID.getName(), responseMap.get("MerchantId"));
		fields.put(FieldType.PG_GATEWAY.getName(), responseMap.get("MerchantId"));
		fields.put(FieldType.TXN_ID.getName(), responseMap.get("TxnRefNo"));
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.PG_REF_NUM.getName(), responseMap.get("RetRefNo"));
		fields.put(FieldType.RRN.getName(), responseMap.get("RetRefNo"));
		fields.put(FieldType.PG_RESP_CODE.getName(), responseMap.get("ResponseCode"));
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), responseMap.get("Message"));
		fields.put(FieldType.AUTH_CODE.getName(), responseMap.get("AuthCode"));
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());

	}

	// Refund transaction
	public void updateISGPayRefundResponse(Fields fields) throws SystemException {

		Map<String, String> responseMap = Splitter.on("&").withKeyValueSeparator('=').split(responseMapISGPay);

		String responseCode = responseMap.get("Status");
		if (responseCode == null) {
			responseCode = responseMap.get("ResponseCode");
		}

		StatusType status = getRefundStatus(responseCode);
		ErrorType errorType = getRefundResponse(responseCode);

		fields.put(FieldType.MERCHANT_ID.getName(), responseMap.get("MerchantId"));
		fields.put(FieldType.PG_GATEWAY.getName(), responseMap.get("MerchantId"));
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.RRN.getName(), responseMap.get("RetRefNo"));
		fields.put(FieldType.PG_REF_NUM.getName(), responseMap.get("RetRefNo"));
		fields.put(FieldType.PG_RESP_CODE.getName(), responseMap.get("Status"));
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), responseMap.get("Message"));
		fields.put(FieldType.AUTH_CODE.getName(), responseMap.get("AuthCode"));
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());

	}

	// Status transaction
	public void updateISGPayStatusResponse(Fields fields) throws SystemException {

		Map<String, String> responseMap = Splitter.on("&").withKeyValueSeparator('=').split(responseMapISGPay);

		StatusType status = getStatus(responseMap.get("ResponseCode"));
		ErrorType errorType = getStatusResponse(responseMap.get("ResponseCode"));

		fields.put(FieldType.MERCHANT_ID.getName(), responseMap.get("MerchantId"));
		fields.put(FieldType.PG_GATEWAY.getName(), responseMap.get("MerchantId"));
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.RRN.getName(), responseMap.get("RetRefNo"));
		fields.put(FieldType.PG_REF_NUM.getName(), responseMap.get("RetRefNo"));
		fields.put(FieldType.PG_RESP_CODE.getName(), responseMap.get("ResponseCode"));
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), responseMap.get("Message"));
		fields.put(FieldType.AUTH_CODE.getName(), responseMap.get("AuthCode"));
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());

	}

}
