package com.kbn.isgpay;

import java.net.URLDecoder;
import java.security.Key;
import java.util.Enumeration;
import java.util.LinkedHashMap;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

public class ISGPayDecryption extends ISGPayHashGeneration {
	public ISGPayDecryption() {
	}

	public LinkedHashMap<String, String> decrypt(HttpServletRequest request, String sDecryptionKey,
			String sSecureSecret) throws Exception {
		LinkedHashMap<String, String> hmDecryptedValue = new LinkedHashMap();
		try {
			if (request != null) {
				if ((sSecureSecret != null) && (!"".equals(sSecureSecret))) {
					if ((sDecryptionKey != null) && (!"".equals(sDecryptionKey))) {
						Enumeration e = request.getParameterNames();

						while (e.hasMoreElements()) {
							String fieldName = (String) e.nextElement();
							String fieldValue = request.getParameter(fieldName);

							if ((fieldValue != null) && (fieldValue.length() > 0)) {
								hmDecryptedValue.put(fieldName, fieldValue);
							}
						}

						doISGPayDecrypt(hmDecryptedValue, sDecryptionKey);

						String sResponseHash = hmDecryptedValue.get("SecureHash") == null ? ""
								: (String) hmDecryptedValue.get("SecureHash");

						if (!"".equals(sResponseHash)) {
							hmDecryptedValue.remove("SecureHash");

							StringBuffer hexString = new StringBuffer();
							hexString.append(generateHash(hmDecryptedValue, sSecureSecret));

							if (hexString.length() != 0) {
								if (sResponseHash.equals(hexString.toString())) {
									hmDecryptedValue.put("hashValidated", "CORRECT");
								} else {
									hmDecryptedValue.put("hashValidated", "INVALID HASH");
								}
							} else {
								hmDecryptedValue.put("ErrorMessage", "Problem During Generating Hash...!!!");
							}
						} else {
							hmDecryptedValue.put("ErrorMessage", "Securehash Not Found In Response...!!!");
						}
					} else {
						hmDecryptedValue.put("ErrorMessage", "Decryption Key can not be null or empty!!!");
					}
				} else {
					hmDecryptedValue.put("ErrorMessage", "SecureSecret can not be null or empty!!!");
				}
			} else {
				hmDecryptedValue.put("ErrorMessage", "Can Not Process Empty or Null Request!!!");
			}
		} catch (Exception e) {
			System.out.println("Exception Occured in ISGPayDecryption.decrypt(request): " + e);
			hmDecryptedValue.put("ErrorMessage", e.getMessage());
		}

		return hmDecryptedValue;
	}

	public void decrypt(LinkedHashMap<String, String> hmDecryptedValue, String sDecryptionKey, String sSecureSecret)
			throws Exception {
		try {
			if ((sSecureSecret != null) && (!"".equals(sSecureSecret))) {
				if ((sDecryptionKey != null) && (!"".equals(sDecryptionKey))) {
					doISGPayDecrypt(hmDecryptedValue, sDecryptionKey);

					String sResponseHash = hmDecryptedValue.get("SecureHash") == null ? ""
							: (String) hmDecryptedValue.get("SecureHash");

					if (!"".equals(sResponseHash)) {
						hmDecryptedValue.remove("SecureHash");

						StringBuffer hexString = new StringBuffer();
						hexString.append(generateHash(hmDecryptedValue, sSecureSecret));

						if (hexString.length() != 0) {
							if (sResponseHash.equals(hexString.toString())) {
								hmDecryptedValue.put("hashValidated", "CORRECT");
							} else {
								hmDecryptedValue.put("hashValidated", "INVALID HASH");
							}
						} else {
							hmDecryptedValue.put("ErrorMessage", "Problem During Generating Hash...!!!");
						}
					} else {
						hmDecryptedValue.put("ErrorMessage", "Securehash Not Found In Response...!!!");
					}
				} else {
					hmDecryptedValue.put("ErrorMessage", "Decryption Key can not be null or empty!!!");
				}
			} else {
				hmDecryptedValue.put("ErrorMessage", "SecureSecret can not be null or empty!!!");
			}
		} catch (Exception e) {
			System.out.println("Exception Occured in ISGPayDecryption.decrypt(Collection): " + e);
			hmDecryptedValue.put("ErrorMessage", e.getMessage());
		}
	}

	private void doISGPayDecrypt(LinkedHashMap<String, String> hmDecryptedValue, String sDecryptionKey)
			throws Exception {
		try {
			String decryptedValue = "";

			if ((hmDecryptedValue.get("EncData") != null) && (!"".equals(hmDecryptedValue.get("EncData")))) {
				byte[] keyByte = sDecryptionKey.getBytes();
				Key key = new SecretKeySpec(keyByte, "AES");
				Cipher c = Cipher.getInstance("AES");
				c.init(2, key);
				byte[] decryptedByteValue = new Base64().decode(((String) hmDecryptedValue.get("EncData")).getBytes());
				byte[] decValue = c.doFinal(decryptedByteValue);
				decryptedValue = new String(decValue);

				hmDecryptedValue.remove("EncData");
			} else {
				throw new Exception("Encrypted Data Not Found In Request!!!");
			}

			if (!"".equals(decryptedValue)) {
				String[] pipeSplit = decryptedValue.split("::");

				for (int i = 0; i < pipeSplit.length; i++) {
					String[] pareValues = pipeSplit[i].split("\\|\\|");
					if ((pareValues[1] != null) && (pareValues[1].toString().length() > 0)
							&& (!pareValues[1].toString().equalsIgnoreCase("null"))) {
						hmDecryptedValue.put(pareValues[0], URLDecoder.decode(pareValues[1], "UTF-8"));
					}
				}
			} else {
				throw new Exception("Problem In Decryption!!!");
			}
		} catch (Exception localException) {
		}
	}
}