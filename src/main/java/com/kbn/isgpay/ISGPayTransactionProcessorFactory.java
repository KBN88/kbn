package com.kbn.isgpay;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AbstractTransactionProcessorFactory;
import com.kbn.pg.core.TransactionProcessor;

public class ISGPayTransactionProcessorFactory implements AbstractTransactionProcessorFactory {

	private TransactionProcessor transactionProcessor;

	public TransactionProcessor getInstance(Fields fields) throws SystemException {

		switch (TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))) {

		case REFUND:
			transactionProcessor = new ISGPayRefundTransactionProcessor();
			break;
		case ENROLL:
		case SALE:
		case AUTHORISE:
			transactionProcessor = new ISGPaySaleTransactionProcessor();
			break;
		case STATUS:
			transactionProcessor = new ISGPayStatusTransactionProcessor();
			break;
		default:
			throw new SystemException(ErrorType.ACQUIRER_ERROR, "Unsupported transaction type for ISG Pay");
		}

		return transactionProcessor;
	}

}