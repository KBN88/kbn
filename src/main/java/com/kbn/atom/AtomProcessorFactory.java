package com.kbn.atom;

import com.kbn.pg.core.Processor;

public class AtomProcessorFactory {
	
	public static Processor getInstance() {
		return new AtomProcessor();
	}

}
