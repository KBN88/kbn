package com.kbn.atom;

import java.util.Map;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class AtomTransformer {
	private Map<String, String> responseString;

	public AtomTransformer(Map<String, String> map) {
		this.responseString = map;

	}

	
	private ErrorType getSaleResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("Ok")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("F")) {
			errorType = ErrorType.REJECTED;
		} else if (respCode.equals("C")) {
			errorType = ErrorType.CANCELLED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	private StatusType getSaleStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Ok")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("F")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("C")) {
			status = StatusType.CANCELLED;
		} else {
			status = StatusType.ERROR;
		}
		
		return status;
	}

	public void updateAtomResponse(Fields fields) throws SystemException {
		StatusType status = getSaleStatus(responseString.get("f_code").toString());
		ErrorType errorType = getSaleResponse(responseString.get("f_code").toString());
		fields.put(FieldType.PG_DATE_TIME.getName(),responseString.get("date").toString());
		fields.put(FieldType.PG_GATEWAY.getName(),responseString.get("bank_txn").toString());
		fields.put(FieldType.TXN_ID.getName(),responseString.get("mer_txn").toString());
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.PG_REF_NUM.getName(), responseString.get("mmp_txn").toString());
		fields.put(FieldType.ACQ_ID.getName(),responseString.get("merchant_id").toString() );
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());

	}

}
