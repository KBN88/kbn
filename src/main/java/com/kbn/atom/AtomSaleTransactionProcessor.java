package com.kbn.atom;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.TransactionProcessor;

public class AtomSaleTransactionProcessor  implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(AtomSaleTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createSaleTransaction(fields);
		logger.info("Request to Atom: " + request);
		communicator.sendAuthorization(request, fields);
		fields.put(FieldType.STATUS.getName(), StatusType.SENT_TO_BANK.getName());
		
	}

}
