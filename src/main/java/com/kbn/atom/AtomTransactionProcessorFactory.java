package com.kbn.atom;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AbstractTransactionProcessorFactory;
import com.kbn.pg.core.TransactionProcessor;

public class AtomTransactionProcessorFactory implements AbstractTransactionProcessorFactory {
	private TransactionProcessor transactionProcessor;

	@Override
	public TransactionProcessor getInstance(Fields fields) throws SystemException {
		switch (TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))) {
		case REFUND:
			transactionProcessor = new AtomRefundTransactionProcessor();
			break;
		case SALE:
		case AUTHORISE:
		case ENROLL:
			transactionProcessor = new AtomSaleTransactionProcessor();
			break;
		case STATUS:
			transactionProcessor = new AtomStatusTransactionProcessor();
			break;
		default:
			throw new SystemException(ErrorType.ACQUIRER_ERROR, "Unsupported transaction type for ATOM");
		}
		return transactionProcessor;
	}

}
