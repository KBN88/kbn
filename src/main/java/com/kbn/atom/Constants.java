package com.kbn.atom;

public interface Constants {
	public static final String EQUATOR = "=";
	public static final String SEPARATOR = "&";
	public static final String LOGIN = "login";
	public static final String PASS = "pass";
	public static final String TTYPE = "ttype";
	public static final String PRODID = "prodid";
	public static final String AMOUNT = "amt";
	public static final String TXNCURR = "txncurr";
	public static final String TXNSCAMT = "txnscamt";
	public static final String CLIENTCODE = "clientcode";
	public static final String TXNID = "txnid";
	public static final String DATE = "date";
	public static final String CUSTACC = "custacc";
	public static final String RETURN_ULR = "ru";
	public static final String CHECK_SUM = "signature";
	public static final String UDF1 = "udf1";
	public static final String UDF2 = "udf2";
	public static final String UDF3 = "udf3";
	public static final String BANK_ID = "bankid";
	//Refund
	public static final String MERCHANTID = "merchantid";
	public static final String ATOM_TXNID = "atomtxnid";
	public static final String REFUND_AMT = "refundamt";
	public static final String TXN_DATE = "txndate";
	public static final String MEREFUNDREF = "merefundref";
	public static final String REFUND_PASS = "pwd";
	
	// Status
	public static final String MERCHANT_ID= "merchantid";
	public static final String MERCHANT_TXNID = "merchanttxnid";
	public static final String AMT= "amt";
	public static final String T_DATE= "tdate";
	 
	

}
