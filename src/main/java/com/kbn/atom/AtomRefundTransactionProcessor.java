package com.kbn.atom;

import org.apache.log4j.Logger;
import org.json.XML;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class AtomRefundTransactionProcessor  implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(AtomRefundTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createRefundTransaction(fields);
		logger.info("Refund Request to Atom:" +request);
		String response = communicator.refundTransaction(fields,request,ConfigurationConstants.ATOM_REFUND_URL.getValue());
	    String jsonresponse = XML.toJSONObject(response).toString();
		AtomRefundTransformer transformer = new AtomRefundTransformer(jsonresponse);
		transformer.updateAtomRefundResponse(fields);
		
	}

}
