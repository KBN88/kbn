package com.kbn.atom;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.pageintegrator.Customizer;
import com.opensymphony.xwork2.Action;

public class AtomCustomizer implements Customizer {

	@Override
	public String integrate(Fields fields) throws SystemException {
		fields.put(FieldType.TXNTYPE.getName(), TransactionType.SALE.getName());
		fields.logAllFields("All Response fields Recieved");
		return Action.NONE;
	}
}
