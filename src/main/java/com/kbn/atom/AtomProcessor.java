package com.kbn.atom;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Processor;

public class AtomProcessor implements Processor {

	public void preProcess(Fields fields) throws SystemException {

	}

	public void process(Fields fields) throws SystemException {
		String transactionType = fields.get(FieldType.TXNTYPE.getName());

		if (transactionType.equals(TransactionType.NEWORDER.getName())) {
			// New Order Transactions are not processed by acquirer
			return;
		}
		if (!fields.get(FieldType.ACQUIRER_TYPE.getName()).equals(AcquirerType.ATOM.getCode())) {
			return;
		}

		if (transactionType.equals(TransactionType.ENROLL.getName())) {
			// Atom pay does not have enrollment
			fields.put(FieldType.TXNTYPE.getName(), TransactionType.SALE.getName());
		}

		(new AtomIntegrator()).process(fields);
	}

	public void postProcess(Fields fields) throws SystemException {

	}
}
