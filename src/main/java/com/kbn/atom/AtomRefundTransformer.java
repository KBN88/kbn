package com.kbn.atom;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class AtomRefundTransformer {

	private String responseString;

	public AtomRefundTransformer(String response) {
		this.responseString = response;

	}

	public ErrorType getRefundResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("00") || respCode.equals("01")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("M0")) {
			errorType = ErrorType.INVALID_REQUEST;
		} else if (respCode.equals("M1")) {
			errorType = ErrorType.REFUND_NOT_SUCCESSFULL;
		} else if (respCode.equals("M4")) {
			errorType = ErrorType.REFUND_INSUFFICIENT_BALANCE;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getRefundStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("00") || respCode.equals("01")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("M0")) {
			status = StatusType.INVALID;
		} else if (respCode.equals("M1")) {
			status = StatusType.REJECTED;
		} else if (respCode.equals("M1")) {
			status = StatusType.REJECTED;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public void updateAtomRefundResponse(Fields fields) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(responseString);
		} catch (ParseException exception) {
			exception.printStackTrace();
		}
		JSONObject jObject = (JSONObject) obj;
		JSONObject refundjObject = null;
		String refundresponse = jObject.get("REFUND").toString();
		JSONParser refundparser = new JSONParser();
		try {
			Object refundobj = refundparser.parse(refundresponse);
			refundjObject = (JSONObject) refundobj;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		StatusType status = getRefundStatus(refundjObject.get("STATUSCODE").toString());
		ErrorType errorType = getRefundResponse(refundjObject.get("STATUSCODE").toString());
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
		fields.put(FieldType.PG_REF_NUM.getName(), refundjObject.get("TXNID").toString());
		fields.put(FieldType.ACQ_ID.getName(), refundjObject.get("MERCHANTID").toString());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), refundjObject.get("STATUSMESSAGE").toString());
	}

}
