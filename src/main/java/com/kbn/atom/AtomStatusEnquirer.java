package com.kbn.atom;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionSearchService;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;

public class AtomStatusEnquirer {

	private static Logger logger = Logger.getLogger(AtomStatusEnquirer.class.getName());

	public void statusEnquirer() {
		TransactionSearchService transactionSearchService = new TransactionSearchService();
		try {
			List<TransactionSummaryReport> transactionList = transactionSearchService
					.getTransactionsForStatusUpdate(AcquirerType.ATOM.getCode());
			for (TransactionSummaryReport transaction : transactionList) {
				Map<String, String> reqMap = prepareFields(transaction);
				Fields fields = new Fields(reqMap);
				AtomStatusTransactionProcessor processor = new AtomStatusTransactionProcessor();
				processor.transact(fields);
				String response = fields.get(FieldType.STATUS.getName());
				if (!response.equals("Error")) {
					// update new order transaction
					fields.updateStatus();
					// Update sale txn
					fields.updateTransactionDetails();
				}

			}
		} catch (SystemException systemException) {
			logger.error("Error updating status for ATOM :", systemException);
		}
	}

	private Map<String, String> prepareFields(TransactionSummaryReport transaction) {
		UserDao userDao = new UserDao();
		User user = userDao.findBySafexpayUser(transaction.getPayId());
		Account account = user.getAccountUsingAcquirerCode(AcquirerType.ATOM.getCode());
		String currencyCode = transaction.getCurrencyCode();
		AccountCurrency accountCurrency = null;
		try {
			accountCurrency = account.getAccountCurrency(currencyCode);
		} catch (SystemException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		// Get key
		String merchantKey = accountCurrency.getTxnKey();
		String merchantID = accountCurrency.getMerchantId();

		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put(FieldType.PAY_ID.getName(), transaction.getPayId());
		requestMap.put(FieldType.TXN_ID.getName(), transaction.getTransactionId());
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), transaction.getOrigTransactionId());
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.ATOM.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), transaction.getCurrencyCode());
		requestMap.put(FieldType.MERCHANT_ID.getName(), merchantID);
		requestMap.put(FieldType.TXN_KEY.getName(), merchantKey);
		requestMap.put(FieldType.AMOUNT.getName(), transaction.getNetAmount());
		requestMap.put(FieldType.PG_DATE_TIME.getName(), transaction.getTxnDate());
		return requestMap;

	}

	private Map<String, String> prepareFields(String txnID) throws SystemException {
		AtomTransactionFetchService fetchTxn = new AtomTransactionFetchService();
		Map<String, String> responseMap = fetchTxn.fetchTransaction(txnID);

		UserDao userDao = new UserDao();
		User user = userDao.findBySafexpayUser(responseMap.get("payID"));
		Account account = user.getAccountUsingAcquirerCode(AcquirerType.GPAY.getCode());
		String currencyCode = responseMap.get("currencyCode");
		AccountCurrency accountCurrency = null;
		try {
			accountCurrency = account.getAccountCurrency(currencyCode);
		} catch (SystemException exception) {
			exception.printStackTrace();
		}
		// Get key
		String merchantKey = accountCurrency.getTxnKey();
		String merchantID = accountCurrency.getMerchantId();

		Map<String, String> requestMap = new HashMap<String, String>();

		requestMap.put(FieldType.PAY_ID.getName(), responseMap.get("payID"));
		requestMap.put(FieldType.TXN_ID.getName(), txnID);
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), responseMap.get("origTxnID"));
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.ATOM.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), responseMap.get("currencyCode"));
		requestMap.put(FieldType.MERCHANT_ID.getName(), merchantID);
		requestMap.put(FieldType.TXN_KEY.getName(), merchantKey);

		return requestMap;
	}

	public String manualStatusEnquirer(String orderId) {

		String response = null;

		try {

			Map<String, String> reqMap = prepareFields(orderId);
			Fields fields = new Fields(reqMap);

			AtomStatusTransactionProcessor processor = new AtomStatusTransactionProcessor();
			processor.transact(fields);

			response = fields.get(FieldType.STATUS.getName());
			logger.info("ATOM Status Response:" + response);
			if (!response.equals("Error")) {
				// update new order transaction
				fields.updateStatus();
				// Update sale txn
				fields.updateTransactionDetails();
			}

		} catch (SystemException systemException) {
			logger.error("Error updating manual status for ATOM:", systemException);
		}
		logger.info("ATOM Status Response before return Statement:");
		return response;
	}

}
