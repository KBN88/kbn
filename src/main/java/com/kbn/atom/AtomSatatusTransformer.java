package com.kbn.atom;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class AtomSatatusTransformer {

	private String responseString;

	public AtomSatatusTransformer(String response) {
		this.responseString = response;

	}

	public ErrorType getRefundResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("001") | respCode.equals("005")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("002")) {
			errorType = ErrorType.TXN_FAILED;
		} else if (respCode.equals("007")) {
			errorType = ErrorType.PENDING_FROM_BANK;
		} else if (respCode.equals("004")) {
			errorType = ErrorType.AUTO_REVERSAL;
		} else if (respCode.equals("006")) {
			errorType = ErrorType.INVALID_REQUEST;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getRefundStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("001") | respCode.equals("005")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("002")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("007")) {
			status = StatusType.PENDING;
		} else if (respCode.equals("004")) {
			status = StatusType.AUTO_REVERSAL;
		} else if (respCode.equals("006")) {
			status = StatusType.INVALID;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public void updateBillDeskStatusResponse(Fields fields) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(responseString);
		} catch (ParseException exception) {
			exception.printStackTrace();
		}
		JSONObject jObject = (JSONObject) obj;
		StatusType status = getRefundStatus(jObject.get("statusCode").toString());
		ErrorType errorType = getRefundResponse(jObject.get("statusCode").toString());
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
		fields.put(FieldType.PG_REF_NUM.getName(), jObject.get("merchantTxnID").toString());
		fields.put(FieldType.ACQ_ID.getName(), jObject.get("merchantID").toString());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), jObject.get("verified").toString());
	}
}
