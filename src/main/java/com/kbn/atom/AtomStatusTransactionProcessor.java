package com.kbn.atom;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class AtomStatusTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(AtomStatusTransactionProcessor.class.getName());
    
	@Override
	public void transact(Fields fields) throws SystemException {
		UserDao dao = new UserDao();
		User user = dao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
		String mid = user.getTerminalId();// Request Decrypted Key/ Salt
		String key = user.getTerminalId();// Request Decrypted Key/ Salt
		String decyptiion = null;
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.prepareFieldsForStatus(fields);
		logger.info("Satus Request to ATOM: " + request);
		String responseTrimmed = communicator.transactStatus(fields,request,ConfigurationConstants.ATOM_ENQUIRY_URL.getValue());
		try {
			decyptiion = new AtomAES().decrypt(responseTrimmed, key, mid);
			logger.error("Encrypted String is :: " + decyptiion);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		AtomSatatusTransformer transformer = new AtomSatatusTransformer(decyptiion.substring(1, decyptiion.length()-1));
		transformer.updateBillDeskStatusResponse(fields);
		fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));
	}

}
