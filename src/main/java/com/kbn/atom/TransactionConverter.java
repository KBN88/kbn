package com.kbn.atom;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.NetBankingType;
import com.kbn.commons.util.PaymentType;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Currency;

public class TransactionConverter {
	private static Logger logger = Logger.getLogger(TransactionConverter.class.getName());

	UserDao dao = new UserDao();

	public String createSaleTransaction(Fields fields) {
		
		User user = dao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
		String mid = user.getMccCode();// Request Key/ Salt
		String key = user.getMccCode();// Request Key/ Salt
		String atomTransactionURL = ConfigurationConstants.ATOM_TRANSACTION_URL.getValue();
		String signature = null;
		String enc = null;

		StringBuilder request = new StringBuilder();
		String login = fields.get(FieldType.MERCHANT_ID.getName());
		String pass = fields.get(FieldType.TXN_KEY.getName());
		String ttype = "NBFundTransfer";
		String prodid = fields.get(FieldType.PASSWORD.getName());
		String txnid = fields.get(FieldType.TXN_ID.getName());
		String amt = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));
		String txncurr = Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName()));
		try {
			signature = SignatureGenerate.getEncodedValueWithSha2(user.getEncryptionKey(), login, pass, ttype, prodid,
					txnid, amt, txncurr);
			logger.info("Craete Atom Signature:" + signature);
		} catch (Exception exception) {

		}

		if (PaymentType.NET_BANKING.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
			Date now = new Date();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			String date = df.format(now);
			System.out.println(date);
			request.append(Constants.LOGIN);
			request.append(Constants.EQUATOR);
			request.append(login);
			request.append(Constants.SEPARATOR);

			request.append(Constants.PASS);
			request.append(Constants.EQUATOR);
			request.append(pass);
			request.append(Constants.SEPARATOR);

			request.append(Constants.TTYPE);
			request.append(Constants.EQUATOR);
			request.append("NBFundTransfer");
			request.append(Constants.SEPARATOR);

			request.append(Constants.PRODID);
			request.append(Constants.EQUATOR);
			request.append(prodid);
			request.append(Constants.SEPARATOR);

			request.append(Constants.AMOUNT);
			request.append(Constants.EQUATOR);
			request.append(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
					fields.get(FieldType.CURRENCY_CODE.getName())));
			request.append(Constants.SEPARATOR);

			request.append(Constants.TXNCURR);
			request.append(Constants.EQUATOR);
			request.append(Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName())));
			request.append(Constants.SEPARATOR);

			request.append(Constants.TXNSCAMT);
			request.append(Constants.EQUATOR);
			request.append("0");
			request.append(Constants.SEPARATOR);

			request.append(Constants.CLIENTCODE);
			request.append(Constants.EQUATOR);
			request.append("001");
			request.append(Constants.SEPARATOR);

			request.append(Constants.TXNID);
			request.append(Constants.EQUATOR);
			request.append(fields.get(FieldType.TXN_ID.getName()));
			request.append(Constants.SEPARATOR);

			request.append(Constants.DATE);
			request.append(Constants.EQUATOR);
			request.append(date);
			request.append(Constants.SEPARATOR);

			request.append(Constants.CUSTACC);
			request.append(Constants.EQUATOR);
			request.append("100000036600");
			request.append(Constants.SEPARATOR);

			request.append(Constants.UDF1);
			request.append(Constants.EQUATOR);
			request.append(fields.get(FieldType.CUST_NAME.getName()));
			request.append(Constants.SEPARATOR);

			request.append(Constants.UDF2);
			request.append(Constants.EQUATOR);
			request.append(fields.get(FieldType.CUST_EMAIL.getName()));
			request.append(Constants.SEPARATOR);

			request.append(Constants.UDF3);
			request.append(Constants.EQUATOR);
			request.append(fields.get(FieldType.CUST_PHONE.getName()));
			request.append(Constants.SEPARATOR);

			request.append(Constants.RETURN_ULR);
			request.append(Constants.EQUATOR);
			request.append(ConfigurationConstants.ATOM_RETURN_URL.getValue());
			request.append(Constants.SEPARATOR);

			request.append(Constants.BANK_ID);
			request.append(Constants.EQUATOR);
			request.append(NetBankingType.getAtomBankCode(fields.get(FieldType.MOP_TYPE.getName())));
			request.append(Constants.SEPARATOR);

			request.append(Constants.CHECK_SUM);
			request.append(Constants.EQUATOR);
			request.append(signature);
			logger.info("Craete Atom PlainText Request:" + request.toString());
			try {
				enc = new AtomAES().encrypt(request.toString(), key, mid);
				logger.info("Encrypted Atom String is:" + request.toString());
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
		StringBuilder httpRequest = new StringBuilder();
		httpRequest.append("<HTML>");
		httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
		httpRequest.append("<form name=\"form1\"action=\"");
		httpRequest.append(atomTransactionURL);
		httpRequest.append("\"method=\"post\">");
		httpRequest.append("<input type=\"hidden\"name=\"login\"value=\"");
		httpRequest.append(login);
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\"name=\"encdata\"value=\"");
		httpRequest.append(enc);
		httpRequest.append("\">");
		httpRequest.append("</form>");
		httpRequest.append("<script language=\"JavaScript\">");
		httpRequest.append("function OnLoadEvent()");
		httpRequest.append("{document.form1.submit();}");
		httpRequest.append("</script>");
		httpRequest.append("</BODY>");
		httpRequest.append("</HTML>");
		return httpRequest.toString();
	}

	public String createRefundTransaction(Fields fields) {
		String date = fields.get(FieldType.PG_DATE_TIME.getName());

		StringBuilder request = new StringBuilder();
		String pass = fields.get(FieldType.TXN_KEY.getName());
		request.append(Constants.MERCHANTID);
		request.append(Constants.EQUATOR);
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append(Constants.SEPARATOR);
		request.append(Constants.REFUND_PASS);
		request.append(Constants.EQUATOR);
		request.append(Base64.getEncoder().encodeToString(pass.getBytes()));
		request.append(Constants.SEPARATOR);
		request.append(Constants.ATOM_TXNID);
		request.append(Constants.EQUATOR);
		request.append(fields.get(FieldType.PG_REF_NUM.getName()));
		request.append(Constants.SEPARATOR);

		request.append(Constants.REFUND_AMT);
		request.append(Constants.EQUATOR);
		request.append(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName())));
		request.append(Constants.SEPARATOR);

		request.append(Constants.TXN_DATE);
		request.append(Constants.EQUATOR);
		request.append(date.substring(0, 10));
		request.append(Constants.SEPARATOR);

		request.append(Constants.MEREFUNDREF);
		request.append(Constants.EQUATOR);
		request.append(fields.get(FieldType.TXN_ID.getName()));
		logger.info("Craete Atom Refund Request:" + request.toString());
		return request.toString();
	}

	public String prepareFieldsForStatus(Fields fields) {
		// merchantid=192&merchanttxnid=abc123&amt=100.00&tdate=2018-10-31
		User user = dao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
		String mid = user.getMccCode();// Request Key/ Salt
		String key = user.getMccCode();// Request Key/ Salt
		String enc = null;
		String date = fields.get(FieldType.PG_DATE_TIME.getName());
		StringBuilder request = new StringBuilder();
		
		request.append(Constants.MERCHANT_ID);
		request.append(Constants.EQUATOR);
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append(Constants.SEPARATOR);
		
		request.append(Constants.MERCHANT_TXNID);
		request.append(Constants.EQUATOR);
		request.append(fields.get(FieldType.TXN_ID.getName()));
		request.append(Constants.SEPARATOR);
		
		request.append(Constants.AMT);
		request.append(Constants.EQUATOR);
		request.append(fields.get(FieldType.AMOUNT.getName()));
		request.append(Constants.SEPARATOR);
		
		request.append(Constants.T_DATE);
		request.append(Constants.EQUATOR);
		request.append(date.substring(0, 10));
		//request.append(Constants.SEPARATOR);
		try {
			enc = new AtomAES().encrypt(request.toString(), key, mid);
			System.out.println("Encrypted String is :: " + enc);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return enc;
	}

}
