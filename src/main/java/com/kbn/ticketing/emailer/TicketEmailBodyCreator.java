package com.kbn.ticketing.emailer;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.util.EmailerConstants;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.ticketing.core.TicketComment;

/**
 * @author shashi
 *
 */
public class TicketEmailBodyCreator {
	private static Logger logger = Logger.getLogger(TicketEmailBodyCreator.class.getName());
	private String body;
	private PropertiesManager propertiesManager = new PropertiesManager();
	private StringBuilder contant = new StringBuilder();

	public String bodyCreateTicketEmail(Fields responseMap, String heading, String message) {
		try {
			makeHeader();
			contant.append("<tr>");
			contant.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear  " + heading + ",</td>");
			contant.append("<br /><br />" + message + "</em></p>");
			contant.append(
					"<p><table width='96%' border='0' align='center' cellpadding='0' cellspacing='0' class='product-spec'>");
			contant.append("<tr>");
			contant.append("<td>TicketId:</td>");
			contant.append("<td>" + responseMap.get("ticketId") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>TicketType:</td>");
			contant.append("<td>" + responseMap.get("ticketType") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>Subject:</td>");
			contant.append("<td>" + responseMap.get("subject") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>Ticketbody:</td>");
			contant.append("<td>" + responseMap.get("ticketbody") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>PayId:</td>");
			contant.append("<td>" + responseMap.get("payId") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>Response:</td>");
			contant.append("<td>" + responseMap.get("response") + "</td>");
			contant.append("</tr>");

			makeFooter();
			body = contant.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String bodyCreateTicketEmail(Fields responseMap) {
		try {
			makeHeader();
			contant.append("<tr>");
			contant.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear Merchant,<br />");
			contant.append("<br />");
			contant.append(
					"Thank you for createing ticket . Your Ticket has been Failled. If you wish to Create Ticket again for the services please click on the below given button, which will redirect you to our payment page.</em></p>");
			contant.append(
					"<p><table width='20%' border='0' align='center' cellpadding='0.' cellspacing='0' class='product-spec1'>");

			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>TicketId:</td>");
			contant.append("<td>" + responseMap.get("ticketId") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>TicketType:</td>");
			contant.append("<td>" + responseMap.get("ticketType") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>Subject:</td>");
			contant.append("<td>" + responseMap.get("subject") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>Ticketbody:</td>");
			contant.append("<td>" + responseMap.get("ticketbody") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>PayId:</td>");
			contant.append("<td>" + responseMap.get("payId") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>Response:</td>");
			contant.append("<td>" + responseMap.get("response") + "</td>");
			contant.append("</tr>");
			makeFooter();
			body = contant.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String bodyCreateTicketUpdateEmail(Fields responseMap, String heading, String message) {
		try {
			makeHeader();
			contant.append("<tr>");
			contant.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear  " + heading + ",</td>");
			contant.append("<br /><br />" + message + "</em></p>");
			contant.append(
					"<p><table width='96%' border='0' align='center' cellpadding='0' cellspacing='0' class='product-spec'>");
			contant.append("<tr>");
			contant.append("<td>TicketId:</td>");
			contant.append("<td>" + responseMap.get("ticketId") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>ticketTypes:</td>");
			contant.append("<td>" + responseMap.get("ticketTypes") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>subject:</td>");
			contant.append("<td>" + responseMap.get("subject") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>assignedAgentPayId:</td>");
			contant.append("<td>" + responseMap.get("assignedAgentPayId") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>ticketStatus:</td>");
			contant.append("<td>" + responseMap.get("ticketStatus") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>ticketbody:</td>");
			contant.append("<td>" + responseMap.get("ticketbody") + "</td>");
			contant.append("</tr>");
			contant.append("<tr>");
			contant.append("<td>response:</td>");
			contant.append("<td>" + responseMap.get("response") + "</td>");
			contant.append("</tr>");
			makeFooter();
			body = contant.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String bodyCreateMessageSendEmail(List<TicketComment> commentList, String heading, String message) {
		try {
			makeHeader();
			contant.append("<tr>");
			contant.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear  " + heading + ",</td>");
			contant.append("<br /><br />" + message + "</em></p>");
			contant.append(
					"<p><table width='96%' border='0' align='center' cellpadding='0' cellspacing='0' class='product-spec'>");
			contant.append("<tr>");
			contant.append("<tr>");
			contant.append("<td>commentList:</td>");
			Iterator<TicketComment> iterator = commentList.iterator();
			while (iterator.hasNext()) {
				TicketComment comments = iterator.next();
				String commentBody = comments.getCommentBody();
				String commentSenderEmail = comments.getCommentSenderEmailId();
				Date commentTime = comments.getCreateDate();

				contant.append("<td>" + commentBody + commentSenderEmail + commentTime + "</td>");
			}
			contant.append("</tr>");
			makeFooter();
			body = contant.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	private StringBuilder makeHeader() {

		contant.append("<html xmlns='http://www.w3.org/1999/xhtml'>");
		contant.append("<head>");
		contant.append("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>");
		contant.append("<title>Payment Acknowledgement</title>");
		contant.append("<style>");
		contant.append("table.product-spec{border:1px solid #eaeaea; border-bottom:1px solid");
		contant.append(
				"#dedede;font-family:Arial,Helvetica,sans-serif;background:#eeeeee}table.product-spec th{font-size:16px;");
		contant.append(
				"font-weight:bold;padding:5px;border-right:1px solid #dedede; border-bottom:1px solid #dedede; background:#0271bb;");
		contant.append(
				"color:#ffffff;}table.product-spec td{font-size:12px;padding:6px; border-right:1px solid #dedede; border-bottom:1px solid");
		contant.append("#dedede;background-color:#ffffff;}");
		contant.append(
				"table.product-spec td p { font:normal 12px Arial; color:#6f6f6f; padding:0px; margin:0px; line-height:12px; }");
		contant.append("</style>");
		contant.append("</head>");
		contant.append("<body>");
		contant.append("<br /><br />");
		contant.append(
				"<table width='700' border='0' align='center' cellpadding='7' cellspacing='0' style='border:1px solid #d4d4d4;");
		contant.append("background-color:#ffffff; border-radius:10px;'>");
		contant.append("<tr>");
		contant.append(
				"<td height='60' align='center' valign='middle' bgcolor='#fbfbfb' style='border-bottom:1px solid #d4d4d4;");
		contant.append("border-top-left-radius:10px; border-top-right-radius:10px;'><img src='"
				+ propertiesManager.getSystemProperty("emailerLogoURL") + "' width='277' height='45' /></td>");
		contant.append("</tr>");
		return contant;

	}

	private StringBuilder makeFooter() {

		contant.append("<p><em>Assuring you of our best services at all times.</em></p>");
		contant.append(
				"<p><em>If you have any questions about your transaction or any other matter, please feel free to contact us at ");
		contant.append(EmailerConstants.CONTACT_US_EMAIL.getValue());
		contant.append(" or by phone at ");
		contant.append(EmailerConstants.PHONE_NO.getValue());
		contant.append(".</em></p>");
		contant.append("<p></p></td>");
		contant.append("</tr>");
		contant.append("<tr>");
		contant.append(
				"<td align='left' valign='middle' bgcolor='#fbfbfb' style='border-top:1px solid #d4d4d4; border-bottom-left-radius:10px;");
		contant.append(
				"border-bottom-right-radius:10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0.'>");
		contant.append("<tr>");
		contant.append("<td align='left' valign='middle' style='font-family:Arial;'><strong>We Care!<br />");
		contant.append(EmailerConstants.COMPANY.getValue());
		contant.append("</strong></td>");
		contant.append("<td align='right' valign='bottom' style='font:normal 12px Arial;'>© 2016 ");
		contant.append(EmailerConstants.WEBSITE.getValue());
		contant.append(", All rights reserved.</td>");
		contant.append("</tr>");
		contant.append("</table></td>");
		contant.append("</tr>");
		contant.append("</table>");
		contant.append("</body>");
		contant.append("</html>");
		return contant;
	}
}
