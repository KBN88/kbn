package com.kbn.ticketing.emailer;

import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.EmailerConstants;
import com.kbn.commons.util.Fields;
import com.kbn.emailer.Emailer;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketComment;
import com.kbn.ticketing.core.TicketDao;

/**
 * @author shashi
 *
 */
public class TicketEmailBuilder {
	private static Logger logger = Logger.getLogger(TicketEmailBuilder.class.getName());
	private String body;
	private String subject;
	private String toEmail;
	private String emailToBcc;
	private boolean emailExceptionHandlerFlag;

	public void helpTicketEmailer(Fields fields, String senderType, String sessionUser) {
		String heading = null;
		String message = null;
		String sendToEmail = null;

		try {
			if (sessionUser.equals(UserType.MERCHANT.toString())) {
				heading = senderType;
				message = CrmFieldConstants.MERCHANT_TICKET_MESSAGE.getValue();
				sendToEmail = senderType;
			} else if (sessionUser.equals(UserType.ADMIN.toString())) {
				heading = senderType;
				message = CrmFieldConstants.ADMIN_TICKET_MESSAGE.getValue();
				sendToEmail = senderType;
			}

			if (sessionUser.equals(UserType.MERCHANT.toString())) {
				createEmail(fields, sendToEmail, heading, message);

			} else if (sessionUser.equals(UserType.ADMIN.toString())) {

				createEmail(fields, sendToEmail, heading, message);
			}
		} catch (Exception e) {
			logger.error("Exception" + e);
		}
	}

	private void createEmail(Fields fields, String sendToEmail, String heading, String message) throws Exception {
		Emailer emailer = new Emailer();
		TicketEmailBodyCreator ticketEmailBodyCreator = new TicketEmailBodyCreator();
		if (fields.get("response").equals("Ticket create successfully!!")) {
			body = ticketEmailBodyCreator.bodyCreateTicketEmail(fields, heading, message);
			subject = EmailerConstants.GATEWAY.getValue() + " HELP TICKET: help ticket Acknowledgement";
			toEmail = sendToEmail;
			setEmailExceptionHandlerFlag(false);
			emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
		} else {
			body = ticketEmailBodyCreator.bodyCreateTicketEmail(fields);
			subject = EmailerConstants.GATEWAY.getValue() + " HELP TICKET: help ticket Acknowledgement";
			toEmail = sendToEmail;
			setEmailExceptionHandlerFlag(false);
			emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
		}
	}

	public void updateTicket(Fields fields, String senderType) {
		String heading = null;
		String message = null;
		String sendToEmail = null;
		Emailer emailer = new Emailer();
		TicketEmailBodyCreator ticketEmailBodyCreator = new TicketEmailBodyCreator();
		TicketDao ticketDao = new TicketDao();
		try {
			HelpTicket ticketDetails = ticketDao.findTicketId(fields.get("ticketId"));
			if (senderType.equals(UserType.ADMIN.toString())) {
				heading = CrmFieldConstants.ADMIN_TICKET_HEADING.getValue();
				message = CrmFieldConstants.ADMIN_UPDATE_TICKET.getValue();
				sendToEmail = ticketDetails.getContactEmailId();
			}
			if (fields.get("response").equals("Ticket details updated successfully")) {
				body = ticketEmailBodyCreator.bodyCreateTicketUpdateEmail(fields, heading, message);
				subject = EmailerConstants.GATEWAY.getValue() + " HELP TICKET: help ticket Acknowledgement";
				toEmail = sendToEmail;
				setEmailExceptionHandlerFlag(false);

				emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(),
						isEmailExceptionHandlerFlag());
			}
		} catch (Exception e) {
			logger.error("Exception" + e);
		}
	}

	public void sendCommentEmail(List<TicketComment> commentList, String ticketId, String userType) {
		String heading = null;
		String message = null;
		String sendToEmail = null;
		Emailer emailer = new Emailer();
		TicketEmailBodyCreator ticketEmailBodyCreator = new TicketEmailBodyCreator();
		TicketDao ticketDao = new TicketDao();
		try {
			HelpTicket ticketDetails = ticketDao.findTicketId(ticketId);

			if (userType.equals(UserType.ADMIN.toString())) {
				heading = CrmFieldConstants.MERCHANT_HEADING.getValue();
				message = CrmFieldConstants.MESSAGE_SEND_SUCCESSFULLY.getValue();
				sendToEmail = ticketDetails.getContactEmailId();
			} else if (userType.equals(UserType.MERCHANT.toString())) {
				heading = CrmFieldConstants.MERCHANT_HEADING.getValue();
				message = CrmFieldConstants.MESSAGE_SEND_SUCCESSFULLY.getValue();
				sendToEmail = ticketDetails.getContactEmailId();
			} else if (userType.equals(UserType.SUBADMIN.toString())) {
				heading = CrmFieldConstants.AGENT_TICKET_HEADING.getValue();
				message = CrmFieldConstants.MESSAGE_SEND_SUCCESSFULLY.getValue();
				sendToEmail = ticketDetails.getContactEmailId();
			}
			body = ticketEmailBodyCreator.bodyCreateMessageSendEmail(commentList, heading, message);
			subject = EmailerConstants.GATEWAY.getValue() + " HELP TICKET: help ticket Acknowledgement";
			toEmail = sendToEmail;
			setEmailExceptionHandlerFlag(false);
			emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
		} catch (Exception e) {
			logger.error("Exception" + e);
		}
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public boolean isEmailExceptionHandlerFlag() {
		return emailExceptionHandlerFlag;
	}

	public void setEmailExceptionHandlerFlag(boolean emailExceptionHandlerFlag) {
		this.emailExceptionHandlerFlag = emailExceptionHandlerFlag;
	}

	public String getEmailToBcc() {
		return emailToBcc;
	}

	public void setEmailToBcc(String emailToBcc) {
		this.emailToBcc = emailToBcc;
	}

}
