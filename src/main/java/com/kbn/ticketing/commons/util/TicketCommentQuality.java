package com.kbn.ticketing.commons.util;

public enum TicketCommentQuality {
	GOOD		(211,"GOOD"),
	BAD			(212,"BAD"),
	FINE		(213,"FINE"),
	POOR		(214,"POOR");

	private final int commentCode;
	private final String commentQuality;
	
	
	private TicketCommentQuality(int commentCode, String commentQuality) {
		this.commentCode = commentCode;
		this.commentQuality = commentQuality;
	}
	public int getCommentCode() {
		return commentCode;
	}
	public String getCommentQuality() {
		return commentQuality;
	}
	
	
}
