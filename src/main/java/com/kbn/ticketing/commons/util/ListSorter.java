package com.kbn.ticketing.commons.util;

import java.util.LinkedList;
import java.util.List;

import com.kbn.ticketing.core.TicketDao;

public class ListSorter {
	private List<SubAdmin> templist = new LinkedList<>();

	public List<SubAdmin> sortingList(SubAdmin assignedAgent) {
		
		List<SubAdmin> agentsList = new LinkedList<>();
		agentsList = new TicketDao().getAgentsList();
		for(SubAdmin agent : agentsList){
			if(agent.getAgentEmailId().equals(assignedAgent.getAgentEmailId())){
				agentsList.remove(agent);
				break;
			}
		}
		templist.add(assignedAgent);
		for (SubAdmin assigned : agentsList) {
			templist.add(assigned);
		}
		return templist;
	}

}
