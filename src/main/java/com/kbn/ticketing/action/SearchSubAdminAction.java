package com.kbn.ticketing.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.SearchUserService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.commons.util.SubAdmin;

public class SearchSubAdminAction extends AbstractSecureAction {

	/**
	 * @author Neeraj
	 */
	private static final long serialVersionUID = 5929390136391392637L;
	private static Logger logger = Logger.getLogger(SearchSubAdminAction.class.getName());
	private List<SubAdmin> aaData = new ArrayList<SubAdmin>();
	private String emailId;
	private String phoneNo;

	public String execute() {
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		SearchUserService searchUserService = new SearchUserService();
		DataEncoder encoder = new DataEncoder();
		try {
			// TODO resolve error
			if ((sessionUser.getUserType().equals(UserType.ADMIN)) || (sessionUser.getUserType().equals(UserType.SUBADMIN))) {
				setAaData(encoder.encodeAgentsObj(searchUserService.getAgentsList(sessionUser.getPayId())));

			}

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if (validator.validateBlankField(getEmailId())) {
		} else if (!validator.validateField(CrmFieldType.EMAILID, getEmailId())) {
			addFieldError(CrmFieldType.EMAILID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getPhoneNo())) {
		} else if (!validator.validateField(CrmFieldType.MOBILE, getPhoneNo())) {
			addFieldError("phoneNo", ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public List<SubAdmin> getAaData() {
		return aaData;
	}

	public void setAaData(List<SubAdmin> aaData) {
		this.aaData = aaData;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

}
