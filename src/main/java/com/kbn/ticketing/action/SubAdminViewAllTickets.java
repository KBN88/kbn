package com.kbn.ticketing.action;

import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketDao;

/**
 * @author shashi
 *
 */
public class SubAdminViewAllTickets extends AbstractSecureAction {

	private static final long serialVersionUID = 8332475848385693601L;

	private static Logger logger = Logger.getLogger(SubAdminViewAllTickets.class.getName());
	private List<HelpTicket> aaData;

	public String execute() {
		User sessionUser = new User();
		TicketDao ticketDao = new TicketDao();
		try {
			sessionUser = (User) sessionMap.get(Constants.USER.getValue());

			if (sessionUser.getUserType().equals(UserType.SUBADMIN)) {
				String assignedAgentEmailId = sessionUser.getEmailId();
				setAaData(ticketDao.getAgentAssignedTickets(assignedAgentEmailId));
			}
			return SUCCESS;

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public List<HelpTicket> getAaData() {

		return aaData;
	}

	public void setAaData(List<HelpTicket> aaData) {
		this.aaData = aaData;
	}

}
