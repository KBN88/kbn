package com.kbn.ticketing.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionManager;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.actionBeans.TicketCreater;
import com.kbn.ticketing.commons.util.TicketStatus;
import com.kbn.ticketing.commons.util.TicketType;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.emailer.TicketEmailBuilder;

/**
 * @author shashi
 *
 */
public class HelpTicketCreater extends AbstractSecureAction {
	private static Logger logger = Logger.getLogger(HelpTicketCreater.class.getName());

	private static final long serialVersionUID = -2461765620561668764L;
	private String ticketId;
	private TicketType ticketType;
	private String contactEmail;
	private String contactMobile;
	private String subject;
	private String merchant;
	private TicketStatus ticketStatus;
	private String ticketbody;
	private String response;
	private String payId;

	public String execute() {
		TicketCreater ticketCreater = new TicketCreater();
		ResponseObject responseObject = new ResponseObject();
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());

		try {

			responseObject = ticketCreater.createTicket(getHelpTicketInstance(), sessionUser.getUserType());

			response = responseObject.getResponseMessage();
			Fields fields = new Fields();
			fields.put("ticketId", ticketId);
			fields.put("ticketType", ticketType.toString());
			fields.put("subject", subject);
			fields.put("ticketbody", ticketbody);
			fields.put("payId", payId);
			fields.put("response", response);
			TicketEmailBuilder ticketEmailBuilder = new TicketEmailBuilder();

			if (sessionUser.getUserType().toString().equals(UserType.ADMIN.toString())) {
				ticketEmailBuilder.helpTicketEmailer(fields, contactEmail, sessionUser.getUserType().toString());

			} else {
				ticketEmailBuilder.helpTicketEmailer(fields, sessionUser.getUserType().toString(),
						sessionUser.getUserType().toString());
			}

		} catch (SystemException exception) {
			// TODO Auto-generated catch block
			logger.error("SystemException" + exception);
		}
		if (!ErrorType.SUCCESS.getResponseCode().equals(responseObject.getResponseCode())) {
			return INPUT;
		}
		return SUCCESS;

	}

	public void validate() {
		CrmValidator validator = new CrmValidator();
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());

		if (validator.validateBlankField(getContactEmail())) {
			addFieldError(CrmFieldType.MERCHANT_EMAIL_ID.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!validator.validateField(CrmFieldType.MERCHANT_EMAIL_ID, getContactEmail())) {
			addFieldError(CrmFieldType.MERCHANT_EMAIL_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getContactMobile())) {
			addFieldError(CrmFieldType.TICKET_MOBILE.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!validator.validateField(CrmFieldType.TICKET_MOBILE, getContactMobile())) {
			addFieldError(CrmFieldType.TICKET_MOBILE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getSubject())) {
			addFieldError(CrmFieldType.TICKET_SUBJECT.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!validator.validateField(CrmFieldType.TICKET_SUBJECT, getSubject())) {
			addFieldError(CrmFieldType.TICKET_SUBJECT.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (sessionUser.getUserType().equals(UserType.ADMIN)) {
			if (validator.validateBlankField(getMerchant())) {
				addFieldError(CrmFieldType.MERCHANT_ID.getName(), validator.getResonseObject().getResponseMessage());
			} else if (!validator.validateField(CrmFieldType.MERCHANT_ID, getMerchant())) {
				addFieldError(CrmFieldType.MERCHANT_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		}
		if (validator.validateBlankField(getTicketbody())) {
			addFieldError(CrmFieldType.TICKET_MESSAGE.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!validator.validateField(CrmFieldType.TICKET_MESSAGE, getTicketbody())) {
			addFieldError(CrmFieldType.TICKET_MESSAGE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

	}

	private HelpTicket getHelpTicketInstance() {
		// TODO Auto-generated method stub
		UserDao userDao = new UserDao();
		String concernedUserEmailId = null;
		if (merchant != null) {
			User concernedUserPayId = userDao.getUserClass(merchant);
			concernedUserEmailId = concernedUserPayId.getEmailId();
		} else {
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			concernedUserEmailId = sessionUser.getEmailId();
		}
		HelpTicket helpTicket = new HelpTicket();
		helpTicket.setSubject(getSubject());
		helpTicket.setContactMobileNo(getContactMobile());
		helpTicket.setSessionUserPayId(payId);
		helpTicket.setConcernedUserEmailId(concernedUserEmailId);
		helpTicket.setTicketId(TransactionManager.getNewTransactionId());
		setTicketId(helpTicket.getTicketId());
		helpTicket.setContactEmailId(getContactEmail());
		helpTicket.setConcernedUser(getMerchant());
		helpTicket.setTicketStatus(getTicketStatus());
		helpTicket.setTicketType(getTicketType());
		helpTicket.setMessageBody(getTicketbody());
		return helpTicket;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public TicketType getTicketType() {
		return ticketType;
	}

	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactMobile() {
		return contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	public String getTicketbody() {
		return ticketbody;
	}

	public void setTicketbody(String ticketbody) {
		this.ticketbody = ticketbody;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

}
