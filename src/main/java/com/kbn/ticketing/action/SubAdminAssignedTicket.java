package com.kbn.ticketing.action;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketComment;
import com.kbn.ticketing.core.TicketDao;

public class SubAdminAssignedTicket extends AbstractSecureAction{
	/**
	 * @Neeraj
	 */
	private static final long serialVersionUID = -8165630468852686143L;
	private static Logger logger = Logger.getLogger(SubAdminAssignedTicket.class.getName());
	private String ticketId;
	private String ticketType;
	private String ticketStatus;
	private String subject;
	private List<TicketComment> commentList = new LinkedList<>();
	private HelpTicket helpTicket = new HelpTicket();
	public String execute(){
		TicketDao ticketDao = new TicketDao();
		setHelpTicket(ticketDao.findTicketId(ticketId));
		try{
			Set<TicketComment> ticketCommentSet = helpTicket
					.getTicketComments();
			Iterator<TicketComment> iterator = ticketCommentSet.iterator();
			while (iterator.hasNext()) {
				TicketComment ticketComment = iterator.next();
				commentList.add(ticketComment);
			}
			/* date$time wise sort */
			Collections.sort(commentList, new Comparator<TicketComment>() {
				@Override
				public int compare(TicketComment o1, TicketComment o2) {
					if (o1.getCreateDate() == null
							|| o2.getCreateDate() == null) {
						return 0;
					}
					return o1.getCreateDate().compareTo(o2.getCreateDate());
				}
			});
			
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	public String getTicketStatus() {
		return ticketStatus;
	}
	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public List<TicketComment> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<TicketComment> commentList) {
		this.commentList = commentList;
	}
	public HelpTicket getHelpTicket() {
		return helpTicket;
	}
	public void setHelpTicket(HelpTicket helpTicket) {
		this.helpTicket = helpTicket;
	}
	
	}


