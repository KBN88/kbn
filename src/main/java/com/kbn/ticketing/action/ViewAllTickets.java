package com.kbn.ticketing.action;

import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketDao;

/**
 * @author shashi
 *
 */
public class ViewAllTickets extends AbstractSecureAction {

	private static final long serialVersionUID = 333796010880164475L;
	private static Logger logger = Logger.getLogger(ViewAllTickets.class.getName());
	private List<HelpTicket> aaData;

	public String execute() {
		User sessionUser = new User();
		TicketDao ticketDao = new TicketDao();

		try {
			sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			SessionUserIdentifier sessionUserIdentifier = new SessionUserIdentifier();
			String sessionUserPayId = sessionUserIdentifier.getMerchantPayId(sessionUser, sessionUser.getEmailId());
			if (sessionUser.getUserType().equals(UserType.ADMIN)) {

				setAaData(ticketDao.getAllTicketDetails());
				return SUCCESS;
			} else if (sessionUser.getUserType().equals(UserType.MERCHANT)
					|| sessionUser.getUserType().equals(UserType.SUBUSER)
					|| sessionUser.getUserType().equals(UserType.SUBADMIN)) {
				setAaData(ticketDao.getTicketByPayId(sessionUserPayId));
				return SUCCESS;
			}

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public List<HelpTicket> getAaData() {
		return aaData;
	}

	public void setAaData(List<HelpTicket> aaData) {
		this.aaData = aaData;
	}

}
