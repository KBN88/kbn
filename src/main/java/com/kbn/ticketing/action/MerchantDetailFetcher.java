package com.kbn.ticketing.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author shashi
 *
 */
public class MerchantDetailFetcher extends AbstractSecureAction {

	private static final long serialVersionUID = 2146118316752042935L;
	private static Logger logger = Logger.getLogger(MerchantDetailFetcher.class.getName());

	private String mobileNo;
	private String email;
	private String payId;
	private String merchants;

	public String execute() {
		try {

			UserDao userDao = new UserDao();
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			User user = userDao.getUserClass(SessionUserIdentifier.getUserPayId(sessionUser, payId));
			setEmail(user.getEmailId());
			setMobileNo(user.getMobile());
			return SUCCESS;
		} catch (Exception exception) {
			ErrorType.INVALID_DEFAULT_CURRENCY.getResponseMessage();
			logger.error("Exception" + exception);
			return ERROR;
		}
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if (validator.validateBlankField(getPayId())) {
			addFieldError(CrmFieldType.MERCHANT_ID.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!validator.validateField(CrmFieldType.MERCHANT_ID, getPayId())) {
			addFieldError(CrmFieldType.MERCHANT_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMerchants() {
		return merchants;
	}

	public void setMerchants(String merchants) {
		this.merchants = merchants;
	}

}
