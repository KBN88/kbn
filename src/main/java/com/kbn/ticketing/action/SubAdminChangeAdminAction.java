
package com.kbn.ticketing.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.UserStatusType;
import com.kbn.crm.commons.action.AbstractSecureAction;
/**
 * @author Rahul
 *
 */
public class SubAdminChangeAdminAction extends AbstractSecureAction{

	private static final long serialVersionUID = 3702199787764289745L;
	private static Logger logger = Logger.getLogger(SubAdminChangeAdminAction.class.getName());
	
	private String firstName;
	private String lastName;
	private String emailId;
	private String adminEmailId;
	private String response;
	
	public String execute() {
		
		if(firstName != null && lastName != null && emailId != null){
			return INPUT;
		} else {
			return ERROR;
		}
	}
	
	public String changeAdmin(){
		UserDao userDao = new UserDao();
		User user = userDao.find(adminEmailId);
		if(user != null){
		UserStatusType userStatus = user.getUserStatus();
		if((user.getUserType().equals(UserType.ADMIN)) && (userStatus.equals(UserStatusType.ACTIVE))){
			String payId = user.getPayId();
			User subAdmin = userDao.find(emailId);
			subAdmin.setParentPayId(payId);
			userDao.update(subAdmin);
			setResponse("Admin Changed Successfully");
			logger.error(response + "for " + adminEmailId );
		} else {
			setResponse("User Inactive");
			logger.error( adminEmailId + response);
		}
		} else {
			setResponse("User doesn't exist!");
			logger.error(response + "with" + adminEmailId);
			
		}
		return SUCCESS;
		
	}
	
	/*public void validate() {
		CrmValidator validator = new CrmValidator();
		if (validator.validateBlankField(getEmailId())) {
			addFieldError(CrmFieldType.EMAILID.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.isValidEmailId(getEmailId()))) {
			addFieldError(CrmFieldType.EMAILID.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		if (validator.validateBlankField(getAdminEmailId())) {
			addFieldError(CrmFieldType.EMAILID.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.isValidEmailId(getAdminEmailId()))) {
			addFieldError(CrmFieldType.EMAILID.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		if ((validator.validateBlankField(getFirstName()))) {
			addFieldError(CrmFieldType.FIRSTNAME.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.FIRSTNAME, getFirstName()))) {
			addFieldError(CrmFieldType.FIRSTNAME.getName(), validator.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(getLastName()))) {
			addFieldError(CrmFieldType.LASTNAME.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.LASTNAME, getLastName()))) {
			addFieldError(CrmFieldType.LASTNAME.getName(), validator.getResonseObject().getResponseMessage());
		}
	}*/

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getAdminEmailId() {
		return adminEmailId;
	}

	public void setAdminEmailId(String adminEmailId) {
		this.adminEmailId = adminEmailId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
