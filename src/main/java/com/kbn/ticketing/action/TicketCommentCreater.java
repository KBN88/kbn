package com.kbn.ticketing.action;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.TransactionManager;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketComment;
import com.kbn.ticketing.core.TicketDao;
import com.kbn.ticketing.emailer.TicketEmailBuilder;

/**
 * @author shashi
 *
 */
public class TicketCommentCreater extends AbstractSecureAction {

	private static final long serialVersionUID = -9153381150076597756L;
	private static Logger logger = Logger.getLogger(TicketCommentCreater.class.getName());
	private String comment;
	private String ticketId;
	private String response;

	public String execute() {
		String commentSenderEmailId;
		HelpTicket helpTicket = new HelpTicket();
		TicketDao dao = new TicketDao();
		try {
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			commentSenderEmailId = sessionUser.getEmailId();
			helpTicket = dao.find(ticketId);
			TicketComment ticketComment = new TicketComment();
			ticketComment.setCommentId(TransactionManager.getNewTransactionId());
			ticketComment.setCommentBody(getComment());
			ticketComment.setCommentSenderEmailId(commentSenderEmailId);
			Set<TicketComment> commentSet = helpTicket.getTicketComments();
			commentSet.add(ticketComment);
			helpTicket.setTicketComments(commentSet);
			dao.update(helpTicket);
			setResponse(ErrorType.SUCCESS.getResponseMessage());
			Iterator<TicketComment> iterator = commentSet.iterator();
			List<TicketComment> commentList = new LinkedList<>();
			while (iterator.hasNext()) {
				TicketComment comments = iterator.next();
				commentList.add(comments);

			}
			TicketEmailBuilder ticketEmailBuilder = new TicketEmailBuilder();

			ticketEmailBuilder.sendCommentEmail(commentList, ticketId, sessionUser.getUserType().toString());

			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}

	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if (validator.validateBlankField(getTicketId())) {
			addFieldError(CrmFieldType.TICKET_ID.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!validator.validateField(CrmFieldType.TICKET_ID, getTicketId())) {
			addFieldError(CrmFieldType.TICKET_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
