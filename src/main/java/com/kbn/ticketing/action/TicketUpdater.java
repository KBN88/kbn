package com.kbn.ticketing.action;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.actionBeans.TicketRecordUpdater;
import com.kbn.ticketing.commons.util.SubAdmin;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketComment;
import com.kbn.ticketing.emailer.TicketEmailBuilder;

public class TicketUpdater  extends AbstractSecureAction {

	private static final long serialVersionUID = -2525358537565354762L;
	private static Logger logger = Logger.getLogger(TicketUpdater.class
			.getName());
	private String ticketId;
	private String assignedAgentPayId;

	private String ticketType;
	private String ticketStatus;
	private String contactEmailId;
	private String contantMobileNo;
	private String subject;
	private String createDate;
	private String messageBody;
	private String response;
	private List<TicketComment> commentList = new LinkedList<>();
	public List<SubAdmin> agentList = new LinkedList<>();

	private HelpTicket helpTicket = new HelpTicket();

	public String updateData() {

		TicketRecordUpdater ticketRecordUpdater = new TicketRecordUpdater();
		try {
			setHelpTicket(ticketRecordUpdater.updateTicketDetails(
					assignedAgentPayId, ticketStatus, contactEmailId, subject,
					messageBody, contantMobileNo, ticketType, ticketId));
			response = CrmFieldConstants.TICKET_DETAILS_UPDATE.getValue();
			addActionMessage(response);
			
			Fields fields = new Fields();
			fields.put("ticketId",ticketId);
			fields.put("ticketTypes", ticketType);
			fields.put("subject", subject);
			fields.put("assignedAgentPayId", assignedAgentPayId);	
			fields.put("ticketStatus", ticketStatus);	
			fields.put("ticketbody", messageBody);	
			fields.put("response", response);
			
			
			TicketEmailBuilder ticketEmailBuilder = new TicketEmailBuilder();
			ticketEmailBuilder.updateTicket(fields, UserType.ADMIN.toString());
			return SUCCESS;

		} catch (Exception exception) {
			logger.error("DefaultCurrencyUpdater", exception);
			CrmFieldConstants.ASSIGNED_TICKET_FAILED.getValue();
			return ERROR;
		}
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getAssignedAgentPayId() {
		return assignedAgentPayId;
	}

	public void setAssignedAgentPayId(String assignedAgentPayId) {
		this.assignedAgentPayId = assignedAgentPayId;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getContactEmailId() {
		return contactEmailId;
	}

	public void setContactEmailId(String contactEmailId) {
		this.contactEmailId = contactEmailId;
	}

	public String getContantMobileNo() {
		return contantMobileNo;
	}

	public void setContantMobileNo(String contantMobileNo) {
		this.contantMobileNo = contantMobileNo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<TicketComment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<TicketComment> commentList) {
		this.commentList = commentList;
	}

	public HelpTicket getHelpTicket() {
		return helpTicket;
	}

	public void setHelpTicket(HelpTicket helpTicket) {
		this.helpTicket = helpTicket;
	}
}
