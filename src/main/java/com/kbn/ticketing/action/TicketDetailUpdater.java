package com.kbn.ticketing.action;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.commons.util.ListSorter;
import com.kbn.ticketing.commons.util.SubAdmin;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketComment;
import com.kbn.ticketing.core.TicketDao;

/**
 * @author shashi
 *
 */
public class TicketDetailUpdater extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(TicketDetailUpdater.class.getName());
	private static final long serialVersionUID = 8034768427830717325L;
	private String ticketId;
	private String assignedAgentPayId;
	private String ticketType;
	private String ticketStatus;
	private String contactEmailId;
	private String contantMobileNo;
	private String subject;
	private String createDate;
	private String messageBody;
	private String response;
	private List<TicketComment> commentList = new LinkedList<>();
	public List<SubAdmin> agentList = new LinkedList<>();

	private HelpTicket helpTicket = new HelpTicket();

	public String execute() {
		TicketDao ticketDao = new TicketDao();

		try {
			setHelpTicket(ticketDao.findTicketId(ticketId));
			UserDao userDao = new UserDao();
			if (!(assignedAgentPayId.equals(""))) {
				User assignedAgent = userDao.getUserClass(assignedAgentPayId);
				SubAdmin agent = new SubAdmin();
				agent.setSubAdmin(assignedAgent);
				/* DropDown List Sorting */
				ListSorter defaultListSort = new ListSorter();
				agentList = defaultListSort.sortingList(agent);
			} else {
				agentList = new TicketDao().getAgentsList();
			}
			Set<TicketComment> ticketCommentSet = helpTicket.getTicketComments();
			Iterator<TicketComment> iterator = ticketCommentSet.iterator();
			while (iterator.hasNext()) {
				TicketComment ticketComment = iterator.next();
				commentList.add(ticketComment);
			}
			/* date$time wise sort */
			Collections.sort(commentList, new Comparator<TicketComment>() {
				@Override
				public int compare(TicketComment o1, TicketComment o2) {
					if (o1.getCreateDate() == null || o2.getCreateDate() == null) {
						return 0;
					}
					return o1.getCreateDate().compareTo(o2.getCreateDate());
				}
			});

			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public HelpTicket getHelpTicket() {
		return helpTicket;
	}

	public void setHelpTicket(HelpTicket helpTicket) {
		this.helpTicket = helpTicket;
	}

	public List<SubAdmin> getAgentList() {
		return agentList;
	}

	public void setAgentList(List<SubAdmin> agentList) {
		this.agentList = agentList;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getContactEmailId() {
		return contactEmailId;
	}

	public void setContactEmailId(String contactEmailId) {
		this.contactEmailId = contactEmailId;
	}

	public String getContantMobileNo() {
		return contantMobileNo;
	}

	public void setContantMobileNo(String contantMobileNo) {
		this.contantMobileNo = contantMobileNo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCreateDate() {
		return createDate;
	}

	public String getAssignedAgentPayId() {
		return assignedAgentPayId;
	}

	public void setAssignedAgentPayId(String assignedAgentPayId) {
		this.assignedAgentPayId = assignedAgentPayId;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public List<TicketComment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<TicketComment> commentList) {
		this.commentList = commentList;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
