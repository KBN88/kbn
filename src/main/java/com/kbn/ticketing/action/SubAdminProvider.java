package com.kbn.ticketing.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.commons.util.SubAdmin;
import com.kbn.ticketing.core.TicketDao;

/**
 * @author shashi
 *
 */
public class SubAdminProvider extends AbstractSecureAction {

	private static final long serialVersionUID = 1827159471593360896L;

	private static Logger logger = Logger.getLogger(SubAdminProvider.class
			.getName());

	private List<SubAdmin> agentList = new ArrayList<SubAdmin>();

	public String execute() {
		try {
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			if (sessionUser.getUserType().equals(UserType.ADMIN)) {
				agentList = new TicketDao().getAgentsList();

			} else {
				return INPUT;
			}

			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public List<SubAdmin> getAgentList() {
		return agentList;
	}

	public void setAgentList(List<SubAdmin> agentList) {
		this.agentList = agentList;
	}
}
