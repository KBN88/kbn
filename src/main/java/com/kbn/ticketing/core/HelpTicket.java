package com.kbn.ticketing.core;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.kbn.ticketing.commons.util.TicketStatus;
import com.kbn.ticketing.commons.util.TicketType;

/**
 * @author shashi
 *
 */
@Entity
@Proxy(lazy = false)
@Table
public class HelpTicket implements Serializable {

	private static final long serialVersionUID = -5897774594345351594L;

	@Id
	@Column(nullable = false, unique = true)
	private String ticketId;
	private String sessionUserPayId;
	@Enumerated(EnumType.STRING)
	private TicketType ticketType;
	private String contactEmailId;
	private String contactMobileNo;
	private String concernedUser;
	private String concernedUserEmailId;
	private String messageBody;
	private String subject;
	private String assignedTo;
	private String agentEmailId;
	@Enumerated(EnumType.STRING)
	private TicketStatus ticketStatus;

	@OneToMany(targetEntity = TicketComment.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<TicketComment> ticketComment = new HashSet<TicketComment>();

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getSessionUserPayId() {
		return sessionUserPayId;
	}

	public void setSessionUserPayId(String sessionUserPayId) {
		this.sessionUserPayId = sessionUserPayId;
	}

	public String getContactEmailId() {
		return contactEmailId;
	}

	public void setContactEmailId(String contactEmailId) {
		this.contactEmailId = contactEmailId;
	}

	public String getContactMobileNo() {
		return contactMobileNo;
	}

	public void setContactMobileNo(String contactMobileNo) {
		this.contactMobileNo = contactMobileNo;
	}

	public Set<TicketComment> getTicketComment() {
		return ticketComment;
	}

	public void setTicketComment(Set<TicketComment> ticketComment) {
		this.ticketComment = ticketComment;
	}

	public String getConcernedUser() {
		return concernedUser;
	}

	public void setConcernedUser(String concernedUser) {
		this.concernedUser = concernedUser;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public TicketType getTicketType() {
		return ticketType;
	}

	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public Set<TicketComment> getTicketComments() {
		return ticketComment;
	}

	public void setTicketComments(Set<TicketComment> ticketComment) {
		this.ticketComment = ticketComment;
	}

	public void addTicketComments(TicketComment ticketComment) {
		this.ticketComment.add(ticketComment);
	}

	public void removeTicketComments(TicketComment ticketComment) {
		this.ticketComment.remove(ticketComment);
	}

	public String getConcernedUserEmailId() {
		return concernedUserEmailId;
	}

	public void setConcernedUserEmailId(String concernedUserEmailId) {
		this.concernedUserEmailId = concernedUserEmailId;
	}

	public String getAgentEmailId() {
		return agentEmailId;
	}

	public void setAgentEmailId(String agentEmailId) {
		this.agentEmailId = agentEmailId;
	}

}
