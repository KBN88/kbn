package com.kbn.ticketing.core;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.UserStatusType;
import com.kbn.ticketing.commons.util.SubAdmin;
import com.kbn.ticketing.commons.util.TicketStatus;
import com.kbn.ticketing.commons.util.TicketType;

public class TicketDao extends HibernateAbstractDao {
	private static Logger logger = Logger.getLogger(TicketDao.class.getName());

	public TicketDao() {
		super();
	}

	private static final String getCompleteTicket = "from HelpTicket ht  where ht.ticketId = :ticketId ";

	public void create(HelpTicket helpTicket) throws DataAccessLayerException {
		super.save(helpTicket);
	}

	public void delete(HelpTicket helpTicket) throws DataAccessLayerException {
		super.delete(helpTicket);
	}

	public HelpTicket find(Long id) throws DataAccessLayerException {
		return (HelpTicket) super.find(HelpTicket.class, id);
	}

	public HelpTicket find(String name) throws DataAccessLayerException {
		return (HelpTicket) super.find(HelpTicket.class, name);
	}

	@SuppressWarnings("rawtypes")
	public List findAll() throws DataAccessLayerException {
		return super.findAll(HelpTicket.class);
	}

	public void update(HelpTicket helpTicket) throws DataAccessLayerException {
		super.saveOrUpdate(helpTicket);
	}

	@SuppressWarnings("unchecked")
	public static void main(String s[]) {
		TicketDao dao = new TicketDao();
		HelpTicket ticket = new HelpTicket();
		// ticket.setTicketId("121212");

		ticket = dao.find("121212");

		System.out.println(ticket.getAssignedTo());
		Set<TicketComment> comments = ticket.getTicketComments();
		Iterable<TicketComment> it = (Iterable<TicketComment>) comments.iterator();
	}

	/* get all Tickets from HelpTicket */
	@SuppressWarnings("unchecked")
	public List<HelpTicket> getAllTicketDetails() {
		List<HelpTicket> ticketDetails = new ArrayList<HelpTicket>();
		try {
			startOperation();
			ticketDetails = getSession().createQuery("from HelpTicket").list();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return ticketDetails;

	}

	/* using TicketId get all Tickets from HelpTicket */
	public HelpTicket findTicketId(String ticketId) {
		HelpTicket helpTicket = findByTicketId(ticketId);
		return helpTicket;

	}

	/* using TicketId get all Tickets from HelpTicket */
	protected HelpTicket findByTicketId(String ticketId) {
		HelpTicket ticketData = null;
		try {
			startOperation();
			ticketData = (HelpTicket) getSession().createQuery(getCompleteTicket).setParameter("ticketId", ticketId)
					.setCacheable(true).getSingleResult();
			getTx().commit();

			return ticketData;
		} catch (NoResultException noResultException) {
			return null;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			logger.error(hibernateException);
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return ticketData;

	}

	/* using PayId get some Ticket Details from HelpTicket */
	@SuppressWarnings("unchecked")
	public List<HelpTicket> getTicketByPayId(String payId) {
		List<HelpTicket> ticketDetails = new ArrayList<HelpTicket>();
		try {
			startOperation();
			List<Object[]> selectedTickets = getSession().createQuery(
					"Select ticketId,sessionUserPayId, ticketType, contactEmailId, contactMobileNo,concernedUser,subject,ticketStatus,messageBody, assignedTo ,agentEmailId from  HelpTicket ht where ht.sessionUserPayId='"
							+ payId + "'")
					.getResultList();

			for (Object[] objects : selectedTickets) {
				HelpTicket helpTicket = new HelpTicket();
				helpTicket.setTicketId((String) objects[0]);
				helpTicket.setSessionUserPayId((String) objects[1]);
				helpTicket.setTicketType((TicketType) objects[2]);
				helpTicket.setContactEmailId((String) objects[3]);
				helpTicket.setContactMobileNo((String) objects[4]);
				helpTicket.setConcernedUser((String) objects[5]);
				helpTicket.setSubject((String) objects[6]);
				helpTicket.setTicketStatus((TicketStatus) objects[7]);
				helpTicket.setMessageBody((String) objects[8]);

				helpTicket.setAssignedTo((String) objects[9]);
				helpTicket.setAgentEmailId((String) objects[10]);

				ticketDetails.add(helpTicket);
			}
			getTx().commit();
			return ticketDetails;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return ticketDetails;

	}

	/* get Agents emailId and BusinessName from User */
	@SuppressWarnings({ "unchecked" })
	public List<SubAdmin> getAgentsList() {
		List<SubAdmin> agentsDetail = new LinkedList<>();
		try {
			startOperation();

			Query query = getSession().createQuery("Select emailId, payId from User U where U.userType = '"
					+ UserType.SUBADMIN + "' and U.userStatus='" + UserStatusType.ACTIVE + "'");
			@SuppressWarnings("deprecation")
			List<Object[]> selectedAgents = query.list();
			for (Object[] objects : selectedAgents) {
				SubAdmin agents = new SubAdmin();
				agents.setAgentEmailId((String) objects[0]);
				agents.setPayId((String) objects[1]);

				agentsDetail.add(agents);
			}
			getTx().commit();
			return agentsDetail;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return agentsDetail;

	}

	/* get selected agent from user */
	@SuppressWarnings("unchecked")
	public List<SubAdmin> getSelectedAgent(String assignedAgent) {
		List<SubAdmin> agentList = new ArrayList<SubAdmin>();
		try {
			startOperation();

			agentList = getSession()
					.createQuery("from User U where U.userType='SUBADMIN' and U.emailId = :assignedAgent")
					.setParameter("assignedAgent", assignedAgent).getResultList();
			/*
			 * for(Agents agents : agentsList){ selectedAgent = agents; break; }
			 */

			getTx().commit();
			return agentList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return agentList;

	}

	/* get Assigned ticket using agent email Id */
	@SuppressWarnings("unchecked")
	public List<HelpTicket> getAgentAssignedTickets(String assignedAgentEmailId) {
		List<HelpTicket> agentAssignedTickets = new ArrayList<HelpTicket>();
		try {
			startOperation();
			List<Object[]> query = getSession().createQuery(
					"Select ticketId, subject, ticketType,messageBody ,ticketStatus from HelpTicket ht where ht.agentEmailId='"
							+ assignedAgentEmailId + "'")
					.getResultList();
			for (Object[] objects : query) {
				HelpTicket helpTicket = new HelpTicket();
				helpTicket.setTicketId((String) objects[0]);
				helpTicket.setSubject((String) objects[1]);
				helpTicket.setTicketType((TicketType) objects[2]);
				helpTicket.setMessageBody((String) objects[3]);
				helpTicket.setTicketStatus((TicketStatus) objects[4]);

				agentAssignedTickets.add(helpTicket);
			}
			getTx().commit();
			return agentAssignedTickets;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return agentAssignedTickets;

	}

	/* get comments using Ticket Id */
	@SuppressWarnings("unchecked")
	public List<TicketComment> fetchAllCommentList(String ticketId) {
		List<TicketComment> commentList = new ArrayList<TicketComment>();
		try {
			startOperation();
			List<Object[]> query = getSession()
					.createQuery("Select commentBody from ticketcomment where ticketId  = '" + ticketId + "' ")
					.getResultList();
			for (Object[] objects : query) {
				TicketComment ticketComment = new TicketComment();
				ticketComment.setCommentBody((String) objects[0]);
				commentList.add(ticketComment);
			}
			getTx().commit();
			return commentList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return commentList;

	}
}
