package com.kbn.ticketing.actionBeans;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.UserType;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketDao;

/**
 * @author Neeraj
 *
 */
public class TicketCreater {

	public ResponseObject createTicket(HelpTicket helpTicket, UserType userType) throws SystemException {

		TicketDao ticketDao = new TicketDao();
		ResponseObject responseActionObject = new ResponseObject();
		if (userType.equals(UserType.ADMIN) || userType.equals(UserType.MERCHANT)
				|| userType.equals(UserType.SUBADMIN)) {
			ticketDao.create(helpTicket);

			responseActionObject.setResponseCode(ErrorType.SUCCESS.getResponseCode());
			responseActionObject.setResponseMessage(ErrorType.TICKET_SUCCESSFULLY_SAVED.getResponseMessage());

		} else {
			// TODO
			responseActionObject.setResponseCode(ErrorType.INTERNAL_SYSTEM_ERROR.getResponseCode());
			responseActionObject.setResponseMessage(ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
		}
		return responseActionObject;

	}

}
