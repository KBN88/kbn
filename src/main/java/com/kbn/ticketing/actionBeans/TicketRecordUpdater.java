package com.kbn.ticketing.actionBeans;

import org.apache.commons.lang.SerializationUtils;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.ticketing.commons.util.TicketStatus;
import com.kbn.ticketing.commons.util.TicketType;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketDao;

/**
 * @author shashi
 *
 */
public class TicketRecordUpdater {
	private HelpTicket helpTicketDB = null;
	private TicketDao ticketDao = new TicketDao();

	public HelpTicket updateTicketDetails(String agent, String ticketStatus, String contactEmailId, String subject,
			String messageBody, String contantMobileNo, String ticketType, String ticketId) {
		helpTicketDB = ticketDao.findTicketId(ticketId);
		HelpTicket ticketDB = (HelpTicket) SerializationUtils.clone(helpTicketDB);
		UserDao userDao = new UserDao();
		User agentPayId = userDao.getUserClass(agent);
		String agentEmailId = agentPayId.getEmailId();
		helpTicketDB.setAssignedTo(agent);
		helpTicketDB.setContactEmailId(contactEmailId);
		helpTicketDB.setContactMobileNo(contantMobileNo);
		helpTicketDB.setAgentEmailId(agentEmailId);
		helpTicketDB.setSubject(subject);
		helpTicketDB.setTicketStatus(TicketStatus.getInstance(ticketStatus));
		helpTicketDB.setTicketType(TicketType.getInstance(ticketType));

		ticketDao.update(helpTicketDB);
		return helpTicketDB;

	}

}
