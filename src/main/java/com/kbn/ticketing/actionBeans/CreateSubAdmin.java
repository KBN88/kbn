package com.kbn.ticketing.actionBeans;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Permissions;
import com.kbn.commons.user.Roles;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.SaltFactory;
import com.kbn.commons.util.SaltFileManager;
import com.kbn.commons.util.TransactionManager;
import com.kbn.commons.util.UserStatusType;
import com.kbn.crm.actionBeans.CheckExistingUser;
import com.kbn.crm.actionBeans.ResponseObject;

public class CreateSubAdmin {
	public ResponseObject createNewSubAdmin(User user, UserType userType, String parentPayId, Set<Permissions> permissions)
			throws SystemException {

		SaltFileManager saltFileManager = new SaltFileManager();
		UserDao userDao = new UserDao();
		ResponseObject responseObject = new ResponseObject();
		ResponseObject responseActionObject = new ResponseObject();
		CheckExistingUser checkExistingUser = new CheckExistingUser();
		Date date = new Date();
		String salt = SaltFactory.generateRandomSalt();

		responseObject = checkExistingUser.checkuser(user.getEmailId());
		if (ErrorType.USER_AVAILABLE.getResponseCode().equals(responseObject.getResponseCode())) {
			user.setUserType(userType);
			user.setUserStatus(UserStatusType.ACTIVE);
			user.setPayId(getpayId());
			user.setAccountValidationKey(TransactionManager.getNewTransactionId());
			user.setEmailValidationFlag(false);
			user.setExpressPayFlag(false);
			user.setRegistrationDate(date);
			// This condition is created for Agent
			if (null != user.getPassword()) {
				user.setPassword(Hasher.getHash(user.getPassword().concat(salt)));
			} else {
				user.setPassword("");// tp prevent password from being set null
			}
			user.setParentPayId(parentPayId);
			Set<Roles> roles = new HashSet<Roles>();
			Roles role = new Roles();

			role.setPermissions(permissions);
			role.setName(UserType.ADMIN.name());
			roles.add(role);

			user.setRoles(roles);

			userDao.create(user);

			// Insert salt in salt.properties
			boolean isSaltInserted = saltFileManager.insertSalt(user.getPayId(), salt);

			if (!isSaltInserted) {
				// Rollback user creation
				userDao.delete(user);
				throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
						ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
			}
			responseActionObject.setResponseCode(ErrorType.SUCCESS.getResponseCode());
			responseActionObject.setAccountValidationID(user.getAccountValidationKey());
			responseActionObject.setEmail(user.getEmailId());
		} else {
			responseActionObject.setResponseCode(ErrorType.USER_UNAVAILABLE.getResponseCode());
			responseActionObject.setResponseMessage(ErrorType.USER_UNAVAILABLE.getResponseMessage());
		}
		return responseActionObject;
	}

	private String getpayId() {
		return TransactionManager.getNewTransactionId();
	}

}
