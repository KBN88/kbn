package com.kbn.sbi;

import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class SBINetbankingTransformer {
	
	public void updateSBINetbankingResponse(Fields fields, Map<String, String> responseMap) throws SystemException {
		Logger logger = Logger.getLogger(SBISaleTransactionProcessor.class.getName());
		logger.info("SBI NetBanking Transformer Response Map:" + responseMap);

		StatusType status = getNetBankingStatusType(responseMap.get("Status"));
		ErrorType errorType = getNetBankingErorType(responseMap.get("Status"));

		fields.put(FieldType.TXN_ID.getName(), responseMap.get("Ref_no"));
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.PG_REF_NUM.getName(), responseMap.get("sbi_ref_no"));
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), responseMap.get("Status_desc"));
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());

	}
	
	public ErrorType getNetBankingErorType(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("Success")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("Failure")) {
			errorType = ErrorType.TXN_FAILED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getNetBankingStatusType(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Success")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("Failure")) {
			status = StatusType.FAILED;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

}
