package com.kbn.sbi;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.TransactionProcessor;

public class SBIStatusTransactionProcessor  implements TransactionProcessor{
	private static Logger logger = Logger.getLogger(SBIStatusTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()), fields.get(FieldType.CURRENCY_CODE.getName()));
		String referenceNumber = fields.get(FieldType.PG_GATEWAY.getName());
		try {
			Map<String, String> request = converter.createDoubleVerificationRequest(amount,referenceNumber);
			logger.error("SBI Status API Request:" +request);
		} catch (IOException exception) {
			logger.error("SBI Status API Request:" +exception);
		}
		
	}

}
