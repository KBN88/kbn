package com.kbn.sbi;

public class AESAlgorithm {
	public AESAlgorithm() {
	}

	public String binary2hex(String binaryString) {
		if (binaryString == null) {
			return null;
		}
		StringBuilder hexString = new StringBuilder();
		for (int i = 0; i < binaryString.length(); i += 8) {
			String temp = binaryString.substring(i, i + 8);
			int intValue = 0;
			int k = 0;
			for (int j = temp.length() - 1; j >= 0; k++) {
				intValue = (int) (intValue + Integer.parseInt(temp, temp.charAt(j)) * Math.pow(2.0D, k));
				j--;
			}
			temp = "0" + Integer.toHexString(intValue);
			hexString.append(temp.substring(temp.length() - 2));
		}
		return hexString.toString();
	}

	public String asciiChar2binary(String asciiString) {
		if (asciiString == null) {
			return null;
		}
		StringBuilder binaryString = new StringBuilder();
		for (int i = 0; i < asciiString.length(); i++) {
			int intValue = asciiString.charAt(i);
			String temp = "00000000" + Integer.toBinaryString(intValue);
			binaryString.append(temp.substring(temp.length() - 8));
		}
		return binaryString.toString();
	}

	public String toString(byte[] temp) {
		char[] ch = new char[temp.length];
		for (int i = 0; i < temp.length; i++) {
			ch[i] = ((char) temp[i]);
		}
		String s = new String(ch);
		return s;
	}

	public static String rightPadZeros(String Str) {
		if (Str == null) {
			return null;
		}
		StringBuilder padStr = new StringBuilder(Str);
		for (int i = Str.length(); i % 8 != 0; i++) {
			padStr.append('^');
		}
		return padStr.toString();
	}

	public static String alpha2Hex(String data) {
		char[] alpha = data.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < alpha.length; i++) {
			int count = Integer.toHexString(alpha[i]).toUpperCase().length();
			if (count <= 1) {
				sb.append("0").append(Integer.toHexString(alpha[i]).toUpperCase());
			} else {
				sb.append(Integer.toHexString(alpha[i]).toUpperCase());
			}
		}
		return sb.toString();
	}

	public String encryptText(String key, String valueToBeEncrypted) throws Exception {
		MAC mac = null;
		String enc1 = null;
		String encadd = "";
		try {
			mac = new MAC();
			if (valueToBeEncrypted.length() % 8 != 0) {
				valueToBeEncrypted = rightPadZeros(valueToBeEncrypted);
			}
			enc1 = mac.getHexValue(alpha2Hex(valueToBeEncrypted), key);
			encadd = encadd + enc1;
			return encadd;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			mac = null;
			enc1 = null;
			encadd = null;
		}
	}

	public static String decryptText(String key, String valueToBeDecrypted) throws Exception {
		MAC mac = null;
		try {
			mac = new MAC();
			return mac.getAESValue(valueToBeDecrypted, key);
		} catch (Exception e) {
			return null;
		} finally {
			mac = null;
		}
	}
}