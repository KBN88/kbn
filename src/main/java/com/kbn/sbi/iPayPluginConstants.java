package com.kbn.sbi;

public class iPayPluginConstants
{
  public static final String DEK_KEY = "DEK_KEY";
  public static final String RESOURCE_ID = "RESOURCE_ID";
  public static final String KEYSTORE_ID = "KEYSTORE_ID";
  
  public iPayPluginConstants() {}
}