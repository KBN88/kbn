package com.kbn.sbi;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public final class iPayPipe {
	static Map<String, Object> trxnParameters = new HashMap();
	String id;
	String action;
	String transId;
	String amt;
	String responseURL;
	String trackId;
	String udf1;
	String udf2;
	String udf3;
	String udf4;
	String udf5;
	String paymentPage;
	String paymentId;
	String result;
	String auth;
	String ref;
	String avr;
	String date;
	String currency;
	String errorURL;
	String language;
	String error;
	String error_text;
	String rawResponse;
	String alias;
	StringBuffer debugMsg;
	String responseCode;
	String zip;
	String addr;
	String member;
	String cvv2;
	String cvv2Verification;
	String type;
	String card;
	String expDay;
	String expMonth;
	String expYear;
	String eci;
	String cavv;
	String xid;
	String resourcePath;
	String acsurl;
	String pareq;
	String pares;
	String error_service_tag;
	String keystorePath;
	String seperator = "\\";
	String sep = "/";
	String webAddress;
	String key = "";
	String initializationVector = "";
	String ivrFlag;
	String npc356chphoneidformat;
	String npc356chphoneid;
	String npc356shopchannel;
	String npc356availauthchannel;
	String npc356pareqchannel;
	String npc356itpcredential;
	String authDataName;
	String authDataLength;
	String authDataType;
	String authDataLabel;
	String authDataPrompt;
	String authDataEncryptKey;
	String authDataEncryptType;
	String authDataEncryptMandatory;
	String ivrPasswordStatus;
	String ivrPassword;
	String itpauthtran;
	String itpauthiden;
	String url;
	String savedcard;
	String paymentdebitId;
	String paymentUrl;
	String absoluteKeystorePath;
	String absoluteResourcePath;

	public iPayPipe() {
		action = "";
		transId = "";
		amt = "";
		responseURL = "";
		trackId = "";
		udf1 = "";
		udf2 = "";
		udf3 = "";
		udf4 = "";
		udf5 = "";
		paymentPage = "";
		paymentId = "";
		result = "";
		auth = "";
		ref = "";
		avr = "";
		date = "";
		currency = "";
		errorURL = "";
		language = "";
		error = "";
		rawResponse = "";
		resourcePath = "";
		keystorePath = "";
		debugMsg = new StringBuffer();
		ivrFlag = "";
		npc356chphoneidformat = "";
		npc356chphoneid = "";
		npc356shopchannel = "";
		npc356availauthchannel = "";
		npc356pareqchannel = "";
		npc356itpcredential = "";
		authDataName = "";
		authDataLength = "";
		authDataType = "";
		authDataLabel = "";
		authDataPrompt = "";
		authDataEncryptKey = "";
		authDataEncryptType = "";
		authDataEncryptMandatory = "";
		ivrPasswordStatus = "";
		ivrPassword = "";
		itpauthtran = "";
		itpauthiden = "";
		id = "";
		Security.addProvider(new BouncyCastleProvider());
	}

	public int performPaymentInitializationHTTP() {
		String request = null;
		StringBuilder requestbuffer = null;
		String xmlData = null;
		HashMap<String, String> hm = null;
		try {
			isUpdatedResourceFile();
			if ((error != null) && (error.length() > 0)) {
				return -1;
			}
			xmlData = getTrxnParamater();
			if (xmlData != null) {
				hm = parseXMLRequest(xmlData);
			} else {
				error = "Alias name does not exits";
				return -1;
			}
			key = ((String) hm.get("resourceKey"));
			requestbuffer = buildHostRequest();
			requestbuffer.append("id=" + (String) hm.get("id") + "&");
			requestbuffer.append("password=" + (String) hm.get("password") + "&");
			webAddress = ((String) hm.get("webaddress"));
			request = "&trandata=" + new AESAlgorithm().encryptText(key, requestbuffer.toString());
			request = request + "&errorURL=" + errorURL + "&responseURL=" + responseURL + "&tranportalId="
					+ (String) hm.get("id");
			webAddress = (webAddress + "/PaymentHTTP.htm?param=paymentInit" + request);
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - performPaymentInitializationHTTP : " + e.getMessage());
			e.printStackTrace();
			error = "Problem while encrypting request data";
			return -1;
		} finally {
			request = null;
			requestbuffer = null;
			xmlData = null;
			hm = null;
		}
		request = null;
		requestbuffer = null;
		xmlData = null;
		hm = null;

		return 0;
	}

	private StringBuilder buildHostRequest() {
		StringBuilder requestbuffer = null;
		try {
			requestbuffer = new StringBuilder();
			if (amt.length() > 0) {
				requestbuffer.append("amt=" + amt + "&");
			}
			if (action.length() > 0) {
				requestbuffer.append("action=" + action + "&");
			}
			if (responseURL.length() > 0) {
				requestbuffer.append("responseURL=" + responseURL + "&");
			}
			if (errorURL.length() > 0) {
				requestbuffer.append("errorURL=" + errorURL + "&");
			}
			if (trackId.length() > 0) {
				requestbuffer.append("trackId=" + trackId + "&");
			}
			if (udf1.length() > 0) {
				requestbuffer.append("udf1=" + udf1 + "&");
			}
			if (udf2.length() > 0) {
				requestbuffer.append("udf2=" + udf2 + "&");
			}
			if (udf3.length() > 0) {
				requestbuffer.append("udf3=" + udf3 + "&");
			}
			if (udf4.length() > 0) {
				requestbuffer.append("udf4=" + udf4 + "&");
			}
			if (udf5.length() > 0) {
				requestbuffer.append("udf5=" + udf5 + "&");
			}
			if (currency.length() > 0) {
				requestbuffer.append("currencycode=" + currency + "&");
			}
			if ((language != null) && (language.length() > 0)) {
				requestbuffer.append("langid=" + language + "&");
			}
			return requestbuffer;
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - buildHostRequest : " + e.getMessage());
		}
		return null;
	}

	public short performTransactionHTTP() {
		StringBuilder request = null;
		StringBuilder requestbuffer = null;
		String xmlData = null;
		HashMap<String, String> hm = null;
		try {
			requestbuffer = buildXMLRequest();
			isUpdatedResourceFile();
			if ((error != null) && (error.length() > 0)) {
				return -1;
			}
			xmlData = getTrxnParamater();
			request = new StringBuilder();
			if (xmlData == null) {
				error = "Alias name does not exits";
				return -1;
			}
			hm = parseXMLRequest(xmlData);

			requestbuffer.append("<id>" + (String) hm.get("id") + "</id>");
			requestbuffer.append("<password>" + (String) hm.get("password") + "</password>");
			requestbuffer.append("</request>");
			if ((responseURL == null) || (responseURL.trim().equals(""))) {
				error = "Response URL is Invalid or NULL";
				return -1;
			}
			key = ((String) hm.get("resourceKey"));
			request.append("&trandata=" + new AESAlgorithm().encryptText(key, requestbuffer.toString()));
			request.append("&errorURL=" + errorURL);
			request.append("&responseURL=" + responseURL);
			request.append("&tranportalId=" + (String) hm.get("id"));
			webAddress = ((String) hm.get("webaddress"));
			setId((String) hm.get("id"));
			webAddress = (webAddress + "/tranPipeHTTP.htm?param=tranInit" + request);
			return 0;
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - performTransactionHTTP : " + e.getMessage());
			e.printStackTrace();
			error = "Error :(";
			return -1;
		} finally {
			request = null;
			requestbuffer = null;
			xmlData = null;
			hm = null;
		}
	}

	public short performTransaction() {
		String xmlData = null;

		HashMap<String, String> hm = null;
		String response = null;
		String webaddr = null;
		HashMap<String, String> resultMap = null;
		try {
			isUpdatedResourceFile();
			if ((error != null) && (error.length() > 0)) {
				StringBuilder requestbuffer;
				return -1;
			}
			xmlData = getTrxnParamater();
			if (xmlData == null) {
				System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
						+ " - performTransaction - Empty XML");
				error = "Alias name does not exits";
				StringBuilder requestbuffer;
				return -1;
			}
			hm = parseXMLRequest(xmlData);

			StringBuilder requestbuffer = buildXMLRequest();
			requestbuffer.append("<id>" + (String) hm.get("id") + "</id>");
			setId((String) hm.get("id"));
			requestbuffer.append("<password>" + (String) hm.get("password") + "</password>");
			webaddr = (String) hm.get("webaddress");
			requestbuffer.append("</request>");

			if ((getAction() != null)
					&& (("1".equalsIgnoreCase(getAction().trim())) || ("4".equalsIgnoreCase(getAction().trim())))) {
				error = "Invalid action";
				return -1;
			}

			response = performTranPortalTransaction(requestbuffer.toString(), webaddr);
			if ((response == null) || ("".equalsIgnoreCase(response))) {
				error = "Invalid response";
				return -1;
			}
			resultMap = parseResponse(response);
			return setTransactionData(resultMap);
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - performTransaction : " + e.getMessage());
			e.printStackTrace();
			error = ("Error while processing request! " + e.getMessage());
			StringBuilder requestbuffer;
			return -1;
		} finally {
			xmlData = null;
			StringBuilder requestbuffer = null;
			hm = null;
			response = null;
			webaddr = null;
		}
	}

	private String performTranPortalTransaction(String request, String webAddress) {
		webAddress = webAddress + "/tranPipe.htm?param=tranInit";
		String tranType = "tran";
		String response = sendMessage(request, webAddress, tranType);
		return response;
	}

	private String sendMessage(String request, String webAddress, String tranType) {
		String rawResponse = "";
		if (webAddress.indexOf("https") != -1)
			System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
		URL url = null;
		URLConnection urlconnection = null;
		try {
			if (webAddress.length() <= 0) {
				return null;
			}
			url = new URL(webAddress);
			urlconnection = url.openConnection();
			urlconnection.setDoInput(true);
			urlconnection.setDoOutput(true);
			urlconnection.setUseCaches(false);
			if (tranType.equals("host"))
				urlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			if (tranType.equals("tran"))
				urlconnection.setRequestProperty("Content-Type", "application/xml");
			if (request.length() > 0) {
				DataOutputStream dataoutputstream = new DataOutputStream(urlconnection.getOutputStream());
				dataoutputstream.writeBytes(request);
				dataoutputstream.flush();
				dataoutputstream.close();
				BufferedReader bufferedreader = new BufferedReader(
						new InputStreamReader(urlconnection.getInputStream()));
				if (tranType.equals("host")) {
					rawResponse = bufferedreader.readLine();
				} else if (tranType.equals("tran")) {
					StringBuffer buf = new StringBuffer();
					for (;;) {
						String s = bufferedreader.readLine();
						if (s == null)
							break;
						buf.append(s);
					}
					rawResponse = buf.toString();
				}
				return rawResponse;
			}
			return null;
		} catch (Exception exception) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - sendMessage : " + exception.getMessage());
			exception.printStackTrace();
			return null;
		} finally {
			url = null;
			urlconnection = null;
		}
	}

	public short performVbVTransaction() {
		String request = null;
		String xmlData = null;
		StringBuilder requestbuffer = null;
		HashMap<String, String> hm = null;
		try {
			requestbuffer = buildXMLRequest();
			isUpdatedResourceFile();
			if ((error != null) && (error.length() > 0)) {
				return -1;
			}
			xmlData = getTrxnParamater();
			if (xmlData == null) {
				error = "Alias name does not exits";
				return -1;
			}
			hm = parseXMLRequest(xmlData);

			requestbuffer.append("<id>" + (String) hm.get("id") + "</id>");
			requestbuffer.append("<password>" + (String) hm.get("password") + "</password>");
			requestbuffer.append("</request>");
			if ((responseURL == null) || (responseURL.trim().equals(""))) {
				error = "Response URL is Invalid or NULL";
				return -1;
			}
			key = ((String) hm.get("resourceKey"));
			request = "&trandata=" + new AESAlgorithm().encryptText(key, requestbuffer.toString()) + "&errorURL="
					+ errorURL + "&responseURL=" + responseURL + "&tranportalId=" + (String) hm.get("id");
			webAddress = ((String) hm.get("webaddress"));
			webAddress = (webAddress + "/VPAS.htm?actionVPAS=VbvVEReqProcessHTTP" + request);
			return 0;
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - performVbVTransaction : " + e.getMessage());
			e.printStackTrace();
			error = ("Error! " + e.getMessage());
			return -1;
		} finally {
			request = null;
			xmlData = null;
			requestbuffer = null;
			hm = null;
		}
	}

	private StringBuilder buildXMLRequest() {
		StringBuilder requestbuffer = null;
		try {
			requestbuffer = new StringBuilder();
			requestbuffer.append("<request>");
			if ((card != null) && (card.length() > 0)) {
				requestbuffer.append("<card>" + card + "</card>");
			}
			if ((cvv2 != null) && (cvv2.length() > 0)) {
				requestbuffer.append("<cvv2>" + cvv2 + "</cvv2>");
			}
			if ((currency != null) && (currency.length() > 0)) {
				requestbuffer.append("<currencycode>" + currency + "</currencycode>");
			}
			if ((expYear != null) && (expYear.length() > 0)) {
				requestbuffer.append("<expyear>" + expYear + "</expyear>");
			}
			if ((expMonth != null) && (expMonth.length() > 0)) {
				requestbuffer.append("<expmonth>" + expMonth + "</expmonth>");
			}
			if ((expDay != null) && (expDay.length() > 0)) {
				requestbuffer.append("<expday>01</expday>");
			}
			if ((type != null) && (type.length() > 0)) {
				requestbuffer.append("<type>" + type + "</type>");
			}
			if ((transId != null) && (transId.length() > 0)) {
				requestbuffer.append("<transid>" + transId + "</transid>");
			}
			if ((zip != null) && (zip.length() > 0)) {
				requestbuffer.append("<zip>" + zip + "</zip>");
			}
			if ((addr != null) && (addr.length() > 0)) {
				requestbuffer.append("<addr>" + addr + "</addr>");
			}
			if ((member != null) && (member.length() > 0)) {
				requestbuffer.append("<member>" + member + "</member>");
			}
			if ((amt != null) && (amt.length() > 0)) {
				requestbuffer.append("<amt>" + amt + "</amt>");
			}
			if ((action != null) && (action.length() > 0)) {
				requestbuffer.append("<action>" + action + "</action>");
			}
			if ((trackId != null) && (trackId.length() > 0)) {
				requestbuffer.append("<trackid>" + trackId + "</trackid>");
			}
			if ((udf1 != null) && (udf1.length() > 0)) {
				requestbuffer.append("<udf1>" + udf1 + "</udf1>");
			}
			if ((udf2 != null) && (udf2.length() > 0)) {
				requestbuffer.append("<udf2>" + udf2 + "</udf2>");
			}
			if ((udf3 != null) && (udf3.length() > 0)) {
				requestbuffer.append("<udf3>" + udf3 + "</udf3>");
			}
			if ((udf4 != null) && (udf4.length() > 0)) {
				requestbuffer.append("<udf4>" + udf4 + "</udf4>");
			}
			if ((udf5 != null) && (udf5.length() > 0)) {
				requestbuffer.append("<udf5>" + udf5 + "</udf5>");
			}
			if ((currency != null) && (currency.length() > 0)) {
				requestbuffer.append("<currencycode>" + currency + "</currencycode>");
			}
			if ((eci != null) && (eci.length() > 0)) {
				requestbuffer.append("<eci>" + eci + "</eci>");
			}
			if ((errorURL != null) && (errorURL.length() > 0)) {
				requestbuffer.append("<errorURL>" + errorURL + "</errorURL>");
			}
			if ((responseURL != null) && (responseURL.length() > 0)) {
				requestbuffer.append("<responseURL>" + responseURL + "</responseURL>");
			}
			if ((ivrFlag != null) && (ivrFlag.length() > 0)) {
				requestbuffer.append("<ivrFlag>" + ivrFlag + "</ivrFlag>");
			}
			if ((npc356chphoneidformat != null) && (npc356chphoneidformat.length() > 0)) {
				requestbuffer.append("<npc356chphoneidformat>" + npc356chphoneidformat + "</npc356chphoneidformat>");
			}
			if ((npc356chphoneid != null) && (npc356chphoneid.length() > 0)) {
				requestbuffer.append("<npc356chphoneid>" + npc356chphoneid + "</npc356chphoneid>");
			}
			if ((npc356shopchannel != null) && (npc356shopchannel.length() > 0)) {
				requestbuffer.append("<npc356shopchannel>" + npc356shopchannel + "</npc356shopchannel>");
			}
			if ((npc356availauthchannel != null) && (npc356availauthchannel.length() > 0)) {
				requestbuffer.append("<npc356availauthchannel>" + npc356availauthchannel + "</npc356availauthchannel>");
			}
			if ((npc356pareqchannel != null) && (npc356pareqchannel.length() > 0)) {
				requestbuffer.append("<npc356pareqchannel>" + npc356pareqchannel + "</npc356pareqchannel>");
			}
			if ((npc356itpcredential != null) && (npc356itpcredential.length() > 0)) {
				requestbuffer.append("<npc356itpcredential>" + npc356itpcredential + "</npc356itpcredential>");
			}
			if ((ivrPasswordStatus != null) && (ivrPasswordStatus.length() > 0))
				requestbuffer.append("<ivrPasswordStatus>" + ivrPasswordStatus + "</ivrPasswordStatus>");
			if ((ivrPassword != null) && (ivrPassword.length() > 0)) {
				requestbuffer.append("<ivrPassword>" + ivrPassword + "</ivrPassword>");
			}
			if (savedcard != null) {
				requestbuffer.append("<savedcard>" + savedcard + "</savedcard>");
			}
			return requestbuffer;
		} catch (Exception e) {
		}
		return null;
	}

	public int parseEncryptedRequest(String trandata) {
		int result = 0;
		String xmlData = null;
		HashMap<String, String> hm = null;
		HashMap<String, String> resultMap = null;
		try {
			if (trandata == null) {
				return 0;
			}
			isUpdatedResourceFile();
			if ((error != null) && (error.length() > 0)) {
				return -1;
			}
			xmlData = getTrxnParamater();
			if (xmlData != null) {
				hm = parseXMLRequest(xmlData);
			} else {
				error = "Alias name does not exits";
			}
			key = ((String) hm.get("resourceKey"));
			trandata = new AESAlgorithm().decryptText(key, trandata);
			resultMap = parseResponse(trandata);
			setTransactionData(resultMap);
			result = parsetrandata(trandata);
			return result;
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - parseEncryptedRequest : " + e.getMessage());
			e.printStackTrace();
			return -1;
		} finally {
			xmlData = null;
			hm = null;
			resultMap = null;
		}
	}

	private int parsetrandata(String trandata) {
		Scanner s = null;
		String[] sObj = null;
		try {
			if ((trandata == null) || (trandata.indexOf("&") == -1))
				return 0;
			s = new Scanner(trandata).useDelimiter("&");
			while (s.hasNext()) {
				sObj = s.next().split("=");
				if (sObj[0].equals("paymentid")) {
					paymentId = sObj[1];
				} else if (sObj[0].equals("result")) {
					result = sObj[1];
				} else if (sObj[0].equals("udf1")) {
					udf1 = sObj[1];
				} else if (sObj[0].equals("udf2")) {
					udf2 = sObj[1];
				} else if (sObj[0].equals("udf3")) {
					udf3 = sObj[1];
				} else if (sObj[0].equals("udf4")) {
					udf4 = sObj[1];
				} else if (sObj[0].equals("udf5")) {
					udf5 = sObj[1];
				} else if (sObj[0].equals("amt")) {
					amt = sObj[1];
				} else if (sObj[0].equals("auth")) {
					auth = sObj[1];
				} else if (sObj[0].equals("ref")) {
					ref = sObj[1];
				} else if (sObj[0].equals("tranid")) {
					transId = sObj[1];
				} else if (sObj[0].equals("postdate")) {
					date = sObj[1];
				} else if (sObj[0].equals("trackId")) {
					trackId = sObj[1];
				} else if (sObj[0].equals("trackid")) {
					trackId = sObj[1];
				} else if (sObj[0].equals("action")) {
					action = sObj[1];
				} else if (sObj[0].equals("Error")) {
					error = sObj[1];
				} else if (sObj[0].equals("ErrorText")) {
					error_text = sObj[1];
				} else if (sObj[0].equals("error_text")) {
					error_text = sObj[1];
				}
			}
			return 0;
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - parsetrandata : " + e.getMessage());
			e.printStackTrace();
			return -1;
		} finally {
			s = null;
			sObj = null;
		}
	}

	public int parseEncryptedResult(String response) {
		String xmlData = null;
		HashMap<String, String> hm = null;
		HashMap<String, String> resultMap = null;
		try {
			isUpdatedResourceFile();
			if ((error != null) && (error.length() > 0)) {
				return -1;
			}
			xmlData = getTrxnParamater();
			if (xmlData != null) {
				hm = parseXMLRequest(xmlData);
			} else {
				error = "Alias name does not exits";
			}
			key = ((String) hm.get("resourceKey"));
			response = new AESAlgorithm().decryptText(key, response);
			parsetrandata(response);
			resultMap = parseResponse(response);
			return setTransactionData(resultMap);
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - parseEncryptedResult : " + e.getMessage());
			e.printStackTrace();
			error = ("Internal Error: " + e.getMessage());
			return -1;
		} finally {
			xmlData = null;
			hm = null;
			resultMap = null;
		}
	}

	private short setTransactionData(HashMap<String, String> resultMap) {
		if ((resultMap == null) || (resultMap.isEmpty()))
			return 0;
		String responseTemp = (String) resultMap.get("error");
		if (responseTemp != null)
			error = responseTemp.trim();
		responseTemp = (String) resultMap.get("url");
		if (responseTemp != null)
			acsurl = responseTemp.trim();
		responseTemp = (String) resultMap.get("PAReq");
		if (responseTemp != null)
			pareq = responseTemp.trim();
		responseTemp = (String) resultMap.get("paymentid");
		if (responseTemp != null)
			paymentId = responseTemp.trim();
		responseTemp = (String) resultMap.get("payid");
		if (responseTemp != null)
			paymentId = responseTemp.trim();
		responseTemp = (String) resultMap.get("eci");
		if (responseTemp != null)
			eci = responseTemp.trim();
		responseTemp = (String) resultMap.get("result");
		if (responseTemp != null)
			result = responseTemp.trim();
		responseTemp = (String) resultMap.get("auth");
		if (responseTemp != null)
			auth = responseTemp.trim();
		responseTemp = (String) resultMap.get("ref");
		if (responseTemp != null)
			ref = responseTemp.trim();
		responseTemp = (String) resultMap.get("avr");
		if (responseTemp != null)
			avr = responseTemp.trim();
		responseTemp = (String) resultMap.get("postdate");
		if (responseTemp != null)
			date = responseTemp.trim();
		responseTemp = (String) resultMap.get("tranid");
		if (responseTemp != null)
			transId = responseTemp.trim();
		responseTemp = (String) resultMap.get("amt");
		if (responseTemp != null)
			amt = responseTemp.trim();
		responseTemp = (String) resultMap.get("trackid");
		if (responseTemp != null)
			trackId = responseTemp.trim();
		responseTemp = (String) resultMap.get("udf1");
		if (responseTemp != null)
			udf1 = responseTemp.trim();
		responseTemp = (String) resultMap.get("udf2");
		if (responseTemp != null)
			udf2 = responseTemp.trim();
		responseTemp = (String) resultMap.get("udf3");
		if (responseTemp != null)
			udf3 = responseTemp.trim();
		responseTemp = (String) resultMap.get("udf4");
		if (responseTemp != null)
			udf4 = responseTemp.trim();
		responseTemp = (String) resultMap.get("udf5");
		if (responseTemp != null)
			udf5 = responseTemp.trim();
		responseTemp = (String) resultMap.get("error_code_tag");
		if (responseTemp != null)
			error = responseTemp.trim();
		responseTemp = (String) resultMap.get("error_service_tag");
		if (responseTemp != null)
			error_service_tag = responseTemp.trim();
		responseTemp = (String) resultMap.get("error_text");
		if (responseTemp != null)
			error_text = responseTemp.trim();
		responseTemp = (String) resultMap.get("responsecode");
		if (responseTemp != null)
			responseCode = responseTemp.trim();
		responseTemp = (String) resultMap.get("cvv2response");
		if (responseTemp != null) {
			cvv2Verification = responseTemp.trim();
		}
		responseTemp = (String) resultMap.get("paymentId");
		if (responseTemp != null)
			paymentdebitId = responseTemp.trim();
		responseTemp = (String) resultMap.get("paymenturl");
		if (responseTemp != null) {
			paymentUrl = responseTemp.trim();
			return 2;
		}
		return 0;
	}

	public int parseEncryptedResultHttp(String response) {
		String xmlData = null;
		HashMap<String, String> hm = null;
		String ds = null;
		HashMap<String, String> resultMap = null;
		try {
			isUpdatedResourceFile();
			if ((error != null) && (error.length() > 0)) {
				return -1;
			}
			xmlData = getTrxnParamater();
			if (xmlData != null) {
				hm = parseXMLRequest(xmlData);
			} else {
				error = "Alias name does not exits";
			}
			key = ((String) hm.get("resourceKey"));
			ds = new AESAlgorithm().decryptText(key, response);
			if (ds == null) {
				error = "Invalid response";
				return -1;
			}
			resultMap = parseResponse(ds);
			setTransactionData(resultMap);
			return parsetrandata(ds);
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - parseEncryptedResultHttp : " + e.getMessage());
			e.printStackTrace();
			error = ("Internal Error: " + e.getMessage());
			return -1;
		} finally {
			xmlData = null;
			hm = null;
			ds = null;
			resultMap = null;
		}
	}

	private static HashMap<String, String> parseResponse(String response) {
		HashMap<String, String> responseMap = null;
		int begin = 0;
		int end = 0;
		String beginString = null;
		String value = null;
		try {
			responseMap = new HashMap();
			HashMap<String, String> localHashMap1;
			if ((response == null) || (!response.trim().startsWith("<")) || (response.trim().length() < 0)) {
				return responseMap;
			}
			response = response.trim();
			try {
				do {
					if ((!response.contains("<")) || (!response.contains(">")))
						break;
					beginString = response.substring(response.indexOf("<") + 1, response.indexOf(">"));
					begin = response.indexOf("<") + beginString.length() + 2;
					end = response.indexOf("</" + beginString);
					value = response.substring(begin, end);
					end = end + beginString.length() + 3;
					response = response.substring(end, response.length());
					responseMap.put(beginString, value);
					begin = 0;
					end = 0;
					beginString = "";
				} while (

				response.length() > 0);
			} catch (Exception ex) {
				System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
						+ "; Exception - parseResponse : " + response + " : " + ex.getMessage());
				ex.printStackTrace();
			}

			return responseMap;
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - parseResponse : " + response + " : " + e.getMessage());
			e.printStackTrace();
			return null;
		} finally {
			beginString = null;
			value = null;
		}
	}

	private static HashMap<String, String> parseXMLRequest(String request) {
		HashMap<String, String> responseMap = null;
		try {
			if ((request == null) || (!request.startsWith("<")) || (request.trim().length() < 0)) {
				return null;
			}
			request = request.trim();
			request = request.substring(request.indexOf("<id>"), request.length());
			request = request.replaceAll("</terminal>", "");
			try {
				responseMap = parseResponse(request);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			return responseMap;
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - parseXMLRequest : " + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	private void isUpdatedResourceFile() {
		File keystoreFile = null;
		File resourceFile = null;
		String keystoreID = null;
		String resourceID = null;
		try {
			keystoreFile = new File(keystorePath + File.separator + "keystore.bin");
			keystoreID = keystoreFile.lastModified() + "-" + keystoreFile.length();
			absoluteKeystorePath = keystoreFile.getAbsolutePath();
			resourceFile = new File(resourcePath + File.separator + "resource.cgn");
			resourceID = resourceFile.lastModified() + "-" + resourceFile.length();
			absoluteResourcePath = resourceFile.getAbsolutePath();
			if ((!keystoreFile.exists()) || (!resourceFile.exists())) {
				trxnParameters.clear();
				error = "Transaction supporting files does not exists.";
			}
			if (trxnParameters.get("KEYSTORE_ID_" + absoluteKeystorePath) == null) {
				trxnParameters.put("KEYSTORE_ID_" + absoluteKeystorePath, keystoreID);
			}
			if (trxnParameters.get("RESOURCE_ID_" + absoluteResourcePath) == null) {
				trxnParameters.put("RESOURCE_ID_" + absoluteResourcePath, resourceID);
			}
			if ((!trxnParameters.get("KEYSTORE_ID_" + absoluteKeystorePath).equals(keystoreID))
					|| (!trxnParameters.get("RESOURCE_ID_" + absoluteResourcePath).equals(resourceID))) {
				trxnParameters.clear();
			}
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - isUpdatedResourceFile : " + e.getMessage());
			e.printStackTrace();
			trxnParameters.clear();
			error = "Problem occurred while building transaction request.";
		} finally {
			keystoreFile = null;
			resourceFile = null;
			keystoreID = null;
			resourceID = null;
		}
	}

	private String getTrxnParamater() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			ShortBufferException, IllegalBlockSizeException, BadPaddingException {
		ParseResouce pr = null;
		try {
			pr = new ParseResouce();
			String str;
			if ((trxnParameters.get(alias + "_" + absoluteResourcePath) != null)
					&& (trxnParameters.get("DEK_KEY_" + absoluteKeystorePath) != null)) {
				byte[] xmlData = pr.decrypt((byte[]) trxnParameters.get(alias + "_" + absoluteResourcePath),
						(Key) trxnParameters.get("DEK_KEY_" + absoluteKeystorePath));
				if (xmlData == null) {
					return null;
				}
				return new String(xmlData);
			}
			return pr.parseResource(keystorePath, resourcePath, alias, absoluteKeystorePath, absoluteResourcePath);
		} finally {
			pr = null;
		}
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getAvr() {
		return avr;
	}

	public void setAvr(String avr) {
		this.avr = avr;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public StringBuffer getDebugMsg() {
		return debugMsg;
	}

	public void setDebugMsg(StringBuffer debugMsg) {
		this.debugMsg = debugMsg;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorURL() {
		return errorURL;
	}

	public void setErrorURL(String errorURL) {
		this.errorURL = errorURL;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentPage() {
		return paymentPage;
	}

	public void setPaymentPage(String paymentPage) {
		this.paymentPage = paymentPage;
	}

	public String getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getResponseURL() {
		return responseURL;
	}

	public void setResponseURL(String responseURL) {
		this.responseURL = responseURL;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getTrackId() {
		return trackId;
	}

	public void setTrackId(String trackId) {
		this.trackId = trackId;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public String getCavv() {
		return cavv;
	}

	public void setCavv(String cavv) {
		this.cavv = cavv;
	}

	public String getCvv2() {
		return cvv2;
	}

	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}

	public String getCvv2Verification() {
		return cvv2Verification;
	}

	public void setCvv2Verification(String cvv2Verification) {
		this.cvv2Verification = cvv2Verification;
	}

	public String getEci() {
		return eci;
	}

	public void setEci(String eci) {
		this.eci = eci;
	}

	public String getExpDay() {
		return expDay;
	}

	public void setExpDay(String expDay) {
		this.expDay = expDay;
	}

	public String getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	public String getExpYear() {
		return expYear;
	}

	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	public String getMember() {
		return member;
	}

	public void setMember(String member) {
		this.member = member;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getAlias() {
		return alias;
	}

	public String getError_text() {
		return error_text;
	}

	public void setError_text(String error_text) {
		this.error_text = error_text;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public String getKeystorePath() {
		return keystorePath;
	}

	public void setKeystorePath(String keystorePath) {
		this.keystorePath = keystorePath;
	}

	public String getAcsurl() {
		return acsurl;
	}

	public void setAcsurl(String acsurl) {
		this.acsurl = acsurl;
	}

	public String getPareq() {
		return pareq;
	}

	public void setPareq(String pareq) {
		this.pareq = pareq;
	}

	public String getPares() {
		return pares;
	}

	public void setPares(String pares) {
		this.pares = pares;
	}

	public String getError_service_tag() {
		return error_service_tag;
	}

	public void setError_service_tag(String error_service_tag) {
		this.error_service_tag = error_service_tag;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public String getXid() {
		return xid;
	}

	public void setXid(String xid) {
		this.xid = xid;
	}

	public String getIvrFlag() {
		return ivrFlag;
	}

	public void setIvrFlag(String ivrFlag) {
		this.ivrFlag = ivrFlag;
	}

	public String getNpc356chphoneidformat() {
		return npc356chphoneidformat;
	}

	public void setNpc356chphoneidformat(String npc356chphoneidformat) {
		this.npc356chphoneidformat = npc356chphoneidformat;
	}

	public String getNpc356chphoneid() {
		return npc356chphoneid;
	}

	public void setNpc356chphoneid(String npc356chphoneid) {
		this.npc356chphoneid = npc356chphoneid;
	}

	public String getNpc356shopchannel() {
		return npc356shopchannel;
	}

	public void setNpc356shopchannel(String npc356shopchannel) {
		this.npc356shopchannel = npc356shopchannel;
	}

	public String getNpc356availauthchannel() {
		return npc356availauthchannel;
	}

	public void setNpc356availauthchannel(String npc356availauthchannel) {
		this.npc356availauthchannel = npc356availauthchannel;
	}

	public String getNpc356pareqchannel() {
		return npc356pareqchannel;
	}

	public void setNpc356pareqchannel(String npc356pareqchannel) {
		this.npc356pareqchannel = npc356pareqchannel;
	}

	public String getNpc356itpcredential() {
		return npc356itpcredential;
	}

	public void setNpc356itpcredential(String npc356itpcredential) {
		this.npc356itpcredential = npc356itpcredential;
	}

	public String getAuthDataName() {
		return authDataName;
	}

	public void setAuthDataName(String authDataName) {
		this.authDataName = authDataName;
	}

	public String getAuthDataLength() {
		return authDataLength;
	}

	public void setAuthDataLength(String authDataLength) {
		this.authDataLength = authDataLength;
	}

	public String getAuthDataType() {
		return authDataType;
	}

	public void setAuthDataType(String authDataType) {
		this.authDataType = authDataType;
	}

	public String getAuthDataLabel() {
		return authDataLabel;
	}

	public void setAuthDataLabel(String authDataLabel) {
		this.authDataLabel = authDataLabel;
	}

	public String getAuthDataPrompt() {
		return authDataPrompt;
	}

	public void setAuthDataPrompt(String authDataPrompt) {
		this.authDataPrompt = authDataPrompt;
	}

	public String getAuthDataEncryptKey() {
		return authDataEncryptKey;
	}

	public void setAuthDataEncryptKey(String authDataEncryptKey) {
		this.authDataEncryptKey = authDataEncryptKey;
	}

	public String getAuthDataEncryptType() {
		return authDataEncryptType;
	}

	public void setAuthDataEncryptType(String authDataEncryptType) {
		this.authDataEncryptType = authDataEncryptType;
	}

	public String getAuthDataEncryptMandatory() {
		return authDataEncryptMandatory;
	}

	public void setAuthDataEncryptMandatory(String authDataEncryptMandatory) {
		this.authDataEncryptMandatory = authDataEncryptMandatory;
	}

	public String getIvrPasswordStatus() {
		return ivrPasswordStatus;
	}

	public void setIvrPasswordStatus(String ivrPasswordStatus) {
		this.ivrPasswordStatus = ivrPasswordStatus;
	}

	public String getIvrPassword() {
		return ivrPassword;
	}

	public void setIvrPassword(String ivrPassword) {
		this.ivrPassword = ivrPassword;
	}

	public String getItpauthtran() {
		return itpauthtran;
	}

	public void setItpauthtran(String itpauthtran) {
		this.itpauthtran = itpauthtran;
	}

	public String getItpauthiden() {
		return itpauthiden;
	}

	public void setItpauthiden(String itpauthiden) {
		this.itpauthiden = itpauthiden;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPaymentdebitId() {
		return paymentdebitId;
	}

	public void setPaymentdebitId(String paymentdebitId) {
		this.paymentdebitId = paymentdebitId;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	public String getSavedcard() {
		return savedcard;
	}

	public void setSavedcard(String savedcard) {
		this.savedcard = savedcard;
	}

	public String getAbsoluteKeystorePath() {
		return absoluteKeystorePath;
	}

	public void setAbsoluteKeystorePath(String absoluteKeystorePath) {
		this.absoluteKeystorePath = absoluteKeystorePath;
	}

	public String getAbsoluteResourcePath() {
		return absoluteResourcePath;
	}

	public void setAbsoluteResourcePath(String absoluteResourcePath) {
		this.absoluteResourcePath = absoluteResourcePath;
	}
}