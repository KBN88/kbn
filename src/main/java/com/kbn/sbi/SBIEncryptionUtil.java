package com.kbn.sbi;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.Logger;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import java.util.Arrays;

public class SBIEncryptionUtil {
	private static Logger logger = Logger.getLogger(SBIEncryptionUtil.class.getName());

	public static String encrypt(String data, String keyPath) throws Exception {
		byte[] key = null;
		try {
			key = Files.readAllBytes(Paths.get(keyPath));
		} catch (IOException iOException) {
			logger.error(iOException);
		}

		Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
		int blockSize = cipher.getBlockSize();

		byte[] iv = new byte[cipher.getBlockSize()];
		byte[] dataBytes = data.getBytes();
		int plaintextLength = dataBytes.length;
		int remainder = plaintextLength % blockSize;

		if (remainder != 0) {
			plaintextLength += (blockSize - remainder);
		}

		byte[] plaintext = new byte[plaintextLength];
		System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		SecureRandom randomSecureRandom = SecureRandom.getInstance("SHA1PRNG");
		randomSecureRandom.nextBytes(iv);
		GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
		cipher.init(Cipher.ENCRYPT_MODE, keySpec, parameterSpec);

		byte[] results = cipher.doFinal(plaintext);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		outputStream.write(iv);
		outputStream.write(results);
		byte[] encryptedRequest = outputStream.toByteArray();
		return Base64.encodeBase64String(encryptedRequest);
	}

	public static String decrypt(String encResponse, String keyPath) {
		String decryptedData = null;
		byte[] key = null;
		try {
			key = Files.readAllBytes(Paths.get(keyPath));
		} catch (IOException iOException) {
			logger.error(iOException);
		}

		try {
			Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
			SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
			byte[] results = Base64.decodeBase64(encResponse);
			byte[] iv = Arrays.copyOfRange(results, 0, cipher.getBlockSize());
			cipher.init(Cipher.DECRYPT_MODE, keySpec, new GCMParameterSpec(128, iv));
			byte[] results1 = Arrays.copyOfRange(results, cipher.getBlockSize(), results.length);
			byte[] ciphertext = cipher.doFinal(results1);
			decryptedData = new String(ciphertext).trim();
		} catch (Exception exception) {
			logger.error(exception);
		}
		return decryptedData;
	}

}