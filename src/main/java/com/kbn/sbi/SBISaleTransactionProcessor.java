package com.kbn.sbi;

import java.io.IOException;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.TransactionProcessor;

public class SBISaleTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(SBISaleTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = null;
		try {
			request = converter.createSaleTransaction(fields);
		} catch (IOException iOException) {
			logger.error(iOException);
		}
		logger.info("Request to SBI: " + request);
		communicator.sendAuthorization(request, fields);
		fields.put(FieldType.STATUS.getName(), StatusType.SENT_TO_BANK.getName());
	}

}
