package com.kbn.sbi;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SBIChecksumUtil {

	public static String generateHashvalue(String data) {
		MessageDigest md;
		StringBuffer hexString = new StringBuffer();
		try {
		md = MessageDigest.getInstance("SHA-256");
		md.update(data.getBytes());
		byte byteData[] = md.digest();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
		sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		for (int i=0;i<byteData.length;i++) {
		String hex=Integer.toHexString(0xff & byteData[i]);
		if(hex.length()==1) hexString.append('0');
		hexString.append(hex);

		}
		} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
		}
		return hexString.toString();
		}

}

