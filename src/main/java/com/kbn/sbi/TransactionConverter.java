package com.kbn.sbi;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.WhitelableBranding;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.pg.core.Amount;

public class TransactionConverter {
	private static Logger logger = Logger.getLogger(TransactionConverter.class.getName());

	public String createSaleTransaction(Fields fields) throws SystemException, IOException {
		UserDao userDao = new UserDao();
		// White-Label Response url
		HttpServletRequest httprequest = ServletActionContext.getRequest();
		String brandingURL = httprequest.getRequestURL().toString();
		URL urlTemp = null;
		try {
			urlTemp = new URL(brandingURL);
		} catch (MalformedURLException exception) {
			logger.error("WhitLabe Brand HostUrl error :" + exception);
		}
		String brandingHost = urlTemp.getHost();
		logger.info("WhitLabe Brand HostUrl :" + brandingHost);
		WhitelableBranding userReseller = userDao.findByWhitelabelBrandURL(brandingHost);
		if (PaymentType.NET_BANKING.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
			StringBuilder request = new StringBuilder();
			request.append("Amount");
			request.append("=");
			request.append(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
					fields.get(FieldType.CURRENCY_CODE.getName())));
			request.append(Constants.PIPE_SEPARATOR);
			request.append("Ref_no");
			request.append("=");
			request.append(fields.get(FieldType.TXN_ID.getName()));
			request.append(Constants.PIPE_SEPARATOR);
			request.append("MERCHANT NAME");
			request.append("=");
			request.append("1");
			request.append(Constants.PIPE_SEPARATOR);
			request.append("PAY ID");
			request.append("=");
			request.append("1");
			String hash = null;
			hash = SBIChecksumUtil.generateHashvalue(request.toString());
			request.append(Constants.PIPE_SEPARATOR);
			request.append("checkSum");
			request.append("=");
			request.append(hash);
			String keyPath = new PropertiesManager().getSystemProperty("SBINetbankingKeyPath");
			String encryptedRequest = null;
			try {
				encryptedRequest = SBIEncryptionUtil.encrypt(request.toString(), keyPath);
			} catch (Exception exception) {
				logger.error(exception);
			}
			StringBuilder finalRequest = new StringBuilder();
			finalRequest.append(encryptedRequest);
			StringBuilder httpRequest = new StringBuilder();

			// Net Banking Form Post Request
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(ConfigurationConstants.SBI_TRANSACTION_URL.getValue());
			httpRequest.append("\" method=\"post\">");
			httpRequest.append("<input type=\"hidden\" name=\"encdata\" value=\"");
			httpRequest.append(finalRequest);
			httpRequest.append("\">");
			httpRequest.append("<input type=\"hidden\" name=\"merchant_code\" value=\"");
			httpRequest.append(fields.get(FieldType.MERCHANT_ID.getName()));
			httpRequest.append("\">");
			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");
			return httpRequest.toString();

		} else {
			String request = null;
			iPayPipe pipe = new iPayPipe();
			String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
					fields.get(FieldType.CURRENCY_CODE.getName()));
			String cardType = fields.get(FieldType.PAYMENT_TYPE.getName());
			if (cardType.equals("DC")) {
				if (fields.get(FieldType.MOP_TYPE.getName()).equals(MopType.RUPAY.getCode())) {
					cardType = "RDC";
				} else {
					cardType = "D";
				}
			} else {
				cardType = "C";
			}
			pipe.setAction(Constants.ACTION);
			pipe.setCurrency(fields.get(FieldType.CURRENCY_CODE.getName()));
			pipe.setAmt(amount);
			String fssTrackId = fields.get(FieldType.ORIG_TXN_ID.getName());
			if (null == fssTrackId) {
				fssTrackId = fields.get(FieldType.TXN_ID.getName());
			}
			pipe.setTrackId(fssTrackId);
			//pipe.setAlias("BhartiPay");//live
			String folderName = fields.get(FieldType.MERCHANT_ID.getName());
			String driverImagePath = folderName;
			File FileLocation1 =null;
			String str ="";
			String file = new PropertiesManager().getSystemProperty("ResourcePath");        
		    FileLocation1 = new File(file + File.separator + driverImagePath + File.separator +"cgn");
		    str = FileLocation1.toString();
		
			
			pipe.setCard(fields.get(FieldType.CARD_NUMBER.getName()));
			pipe.setAlias(fields.get(FieldType.PASSWORD.getName()));//uat
			pipe.setResourcePath(str);
			//pipe.setResourcePath(new PropertiesManager().getSystemProperty("ResourcePath"));
			pipe.setCard(fields.get(FieldType.CARD_NUMBER.getName()));
			pipe.setCvv2(fields.get(FieldType.CVV.getName()));
			String expDate = fields.get(FieldType.CARD_EXP_DT.getName());
			pipe.setExpMonth(expDate.substring(0, 2));
			pipe.setExpYear(expDate.substring(2, 6));
			pipe.setMember(fields.get(FieldType.CUST_NAME.getName()));
			pipe.setType(cardType);
			pipe.setUdf1("");
			pipe.setUdf2("");
			pipe.setUdf3("");
			pipe.setUdf4("");
			pipe.setUdf5("");
			pipe.setKeystorePath(str);
			//pipe.setKeystorePath(new PropertiesManager().getSystemProperty("ResourcePath"));
			if (userReseller != null) {
			pipe.setResponseURL("https://"+userReseller.getBrandURL()+"/crm/jsp/sbiPaymentResponse");
			pipe.setErrorURL("https://"+userReseller.getBrandURL()+"/crm/jsp/sbiPaymentResponse");
					}else {
			pipe.setResponseURL(ConfigurationConstants.SBI_RETURN_URL.getValue());
			pipe.setErrorURL(ConfigurationConstants.SBI_RETURN_URL.getValue());
					}
			int result = pipe.performVbVTransaction();// Live
			// To redirect the web address.
			if (result == 0) {
				request = pipe.getWebAddress();
			} else {
				System.out.println(pipe.getError()); // Problem in connecting
														// Payment Gateway
			}
			StringBuilder httpRequest = new StringBuilder();
			// Net Banking Request/CC/DC Form Post Request
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\">");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(request);
			httpRequest.append("\" method=\"post\">");
			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");

			return httpRequest.toString();
		}

	}

	public Map<String, String> createDoubleVerificationRequest(String amount, String referenceNumber)
			throws SystemException, IOException {

		SBITransactionFetchService fetchTxn = new SBITransactionFetchService();
		String merchantId = fetchTxn.fetchMerchantId(referenceNumber);
		Map<String, String> requestMap = new HashMap<String, String>();
		StringBuilder request = new StringBuilder();
		request.append("Ref_no");
		request.append("=");
		request.append(referenceNumber);
		request.append(Constants.PIPE_SEPARATOR);
		request.append("Amount");
		request.append("=");
		request.append(amount);
		// Generate hash value
		String hash = null;
		hash = SBIChecksumUtil.generateHashvalue(request.toString());
		request.append(Constants.PIPE_SEPARATOR);
		request.append("checkSum");
		request.append("=");
		request.append(hash);
		String keyPath = new PropertiesManager().getSystemProperty("SBINetbankingKeyPath");
		String encryptedRequest = null;
		try {
			encryptedRequest = SBIEncryptionUtil.encrypt(request.toString(), keyPath);

		} catch (Exception exception) {
			logger.error(exception);
		}
		requestMap.put("encdata", encryptedRequest);
		requestMap.put("merchant_code", merchantId);
		return requestMap;
	}

//SBI Refund Transaction 
	public String createRefundTransaction(Fields fields) {
		UserDao userDao = new UserDao();
		// White-Label Response url
		HttpServletRequest httprequest = ServletActionContext.getRequest();
		String brandingURL = httprequest.getRequestURL().toString();
		URL urlTemp = null;
		try {
			urlTemp = new URL(brandingURL);
		} catch (MalformedURLException exception) {
			logger.error("WhitLabe Brand HostUrl error :" + exception);
		}
		String brandingHost = urlTemp.getHost();
		logger.info("WhitLabe Brand HostUrl :" + brandingHost);
		WhitelableBranding userReseller = userDao.findByWhitelabelBrandURL(brandingHost);
		String request = null;
		iPayPipe pipe = new iPayPipe();
		String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));
		String cardType = fields.get(FieldType.PAYMENT_TYPE.getName());
		if (cardType.equals("DC")) {
			if (fields.get(FieldType.MOP_TYPE.getName()).equals(MopType.RUPAY.getCode())) {
				cardType = "RDC";
			} else {
				cardType = "D";
			}
		} else {
			cardType = "C";
		}
		String fssTrackId = fields.get(FieldType.ORIG_TXN_ID.getName());
		if (null == fssTrackId) {
			fssTrackId = fields.get(FieldType.TXN_ID.getName());
		}
		
		String folderName = fields.get(FieldType.MERCHANT_ID.getName());
		String driverImagePath = folderName;
		File FileLocation1 =null;
		String str ="";
		String file = new PropertiesManager().getSystemProperty("ResourcePath");        
	    FileLocation1 = new File(file + File.separator + driverImagePath + File.separator +"cgn");
	    str = FileLocation1.toString();
	    
		pipe.setTrackId(fssTrackId);
		//pipe.setAlias("BhartiPay");//Live
		pipe.setAlias(fields.get(FieldType.PASSWORD.getName()));
		pipe.setResourcePath(str);
		//pipe.setResourcePath(new PropertiesManager().getSystemProperty("ResourcePath"));
		pipe.setAction(Constants.REFUNDACTION);
		pipe.setAmt(amount);
		pipe.setCurrency(fields.get(FieldType.CURRENCY_CODE.getName()));
		pipe.setType(cardType);
		pipe.setTransId(fssTrackId);
		pipe.setUdf1("");
		pipe.setUdf2("");
		pipe.setUdf3("");
		pipe.setUdf4("");
		pipe.setUdf5("");
		//pipe.setKeystorePath(new PropertiesManager().getSystemProperty("ResourcePath"));
		pipe.setKeystorePath(str);
		if (userReseller != null) {
			pipe.setResponseURL("https://"+userReseller.getBrandURL()+"/crm/jsp/sbiPaymentResponse");
			pipe.setErrorURL("https://"+userReseller.getBrandURL()+"/crm/jsp/sbiPaymentResponse");
					}else {
			pipe.setResponseURL(ConfigurationConstants.SBI_RETURN_URL.getValue());
			pipe.setErrorURL(ConfigurationConstants.SBI_RETURN_URL.getValue());
					}
	
		int result = pipe.performVbVTransaction();//Live
		// To redirect the web address.
		if (result == 0) {
			request = pipe.getWebAddress();
		} else {
			System.out.println(pipe.getError()); // Problem in connecting
													// Payment Gateway
		}

		return request;
	}
	
	
	//SBI SATATUS Transaction 
		public String createSatusTransaction(Fields fields) {
			UserDao userDao = new UserDao();
			// White-Label Response url
			HttpServletRequest httprequest = ServletActionContext.getRequest();
			String brandingURL = httprequest.getRequestURL().toString();
			URL urlTemp = null;
			try {
				urlTemp = new URL(brandingURL);
			} catch (MalformedURLException exception) {
				logger.error("WhitLabe Brand HostUrl error :" + exception);
			}
			String brandingHost = urlTemp.getHost();
			logger.info("WhitLabe Brand HostUrl :" + brandingHost);
			WhitelableBranding userReseller = userDao.findByWhitelabelBrandURL(brandingHost);
			String request = null;
			iPayPipe pipe = new iPayPipe();
			String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
					fields.get(FieldType.CURRENCY_CODE.getName()));
			String cardType = fields.get(FieldType.PAYMENT_TYPE.getName());
			if (cardType.equals("DC")) {
				if (fields.get(FieldType.MOP_TYPE.getName()).equals(MopType.RUPAY.getCode())) {
					cardType = "RDC";
				} else {
					cardType = "D";
				}
			} else {
				cardType = "C";
			}
			String fssTrackId = fields.get(FieldType.ORIG_TXN_ID.getName());
			if (null == fssTrackId) {
				fssTrackId = fields.get(FieldType.TXN_ID.getName());
			}
			
			String folderName = fields.get(FieldType.MERCHANT_ID.getName());
			String driverImagePath = folderName;
			File FileLocation1 =null;
			String str ="";
			String file = new PropertiesManager().getSystemProperty("ResourcePath");        
		    FileLocation1 = new File(file + File.separator + driverImagePath + File.separator +"cgn");
		    str = FileLocation1.toString();
		    
			pipe.setTrackId(fssTrackId);
			//pipe.setAlias("BhartiPay");//Live
			pipe.setAlias(fields.get(FieldType.PASSWORD.getName()));
			pipe.setResourcePath(str);
			//pipe.setResourcePath(new PropertiesManager().getSystemProperty("ResourcePath"));
			pipe.setAction(Constants.REFUNDACTION);
			pipe.setAmt(amount);
			pipe.setCurrency(fields.get(FieldType.CURRENCY_CODE.getName()));
			pipe.setType(cardType);
			pipe.setTransId(fssTrackId);
			pipe.setUdf1("");
			pipe.setUdf2("");
			pipe.setUdf3("");
			pipe.setUdf4("");
			pipe.setUdf5("");
			//pipe.setKeystorePath(new PropertiesManager().getSystemProperty("ResourcePath"));
			pipe.setKeystorePath(str);
			if (userReseller != null) {
				pipe.setResponseURL("https://"+userReseller.getBrandURL()+"/crm/jsp/sbiPaymentResponse");
				pipe.setErrorURL("https://"+userReseller.getBrandURL()+"/crm/jsp/sbiPaymentResponse");
						}else {
				pipe.setResponseURL(ConfigurationConstants.SBI_RETURN_URL.getValue());
				pipe.setErrorURL(ConfigurationConstants.SBI_RETURN_URL.getValue());
						}
		
			int result = pipe.performVbVTransaction();//Live
			// To redirect the web address.
			if (result == 0) {
				request = pipe.getWebAddress();
			} else {
				System.out.println(pipe.getError()); // Problem in connecting
														// Payment Gateway
			}

			return request;
		}

}