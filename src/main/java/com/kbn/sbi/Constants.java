package com.kbn.sbi;

public class Constants {
	public static final String EQUATOR = "=";
	public static final String SEPARATOR = "&";
	public static final String PIPE_SEPARATOR = "|";
	public static final String CHECK_SUM = "Checksum";
	public static final String ID = "id";
	public static final String PASSWORD = "password";
	public static final String ACTION = "1";
	public static final String REFUNDACTION = "2";
	public static final String STATUSACTION = "8";
	public static final String AMT = "amt";
	public static final String CURRENCYCODE = "currencycode";
	public static final String TRACKID = "trackId";
	public static final String CARD = "card";
	public static final String EXPMONTH = "expmonth";
	public static final String EXPYEAR = "expyear";
	public static final String CVV2 = "cvv2";
	public static final String MEMBER = "member";
	public static final String UDF1 = "udf1";
	public static final String UDF2 = "udf2";
	public static final String UDF3 = "udf3";
	public static final String UDF4 = "udf4";
	public static final String UDF5 = "udf5";
	public static final String RESULT = "result";
	public static final String URL = "url";
	public static final String PAREQ = "PAReq";
	public static final String PAYMENTID = "paymentid";
	public static final String ECI = "eci";
	public static final String PARES = "PaRes";
	public static final String AUTH = "auth";
	public static final String REF = "ref";
	public static final String AVR = "avr";
	public static final String POSTDATE = "postdate";
	public static final String TRANID = "tranid";
	public static final String MD = "MD";
	public static final String TERMURL = "TermURL";
	public static final String ERROR_TEXT = "error_text";
	public static final String ERROR_CODE_TAG = "error_code_tag";
	public static final String ERROR_SERVICE_TAG = "error_service_tag";
	public static final String ZIP = "zip";
	public static final String ADDR = "addr";
	public static final String TRANSID = "transid";
	

}
