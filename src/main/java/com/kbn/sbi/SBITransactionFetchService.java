package com.kbn.sbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;

public class SBITransactionFetchService extends HibernateAbstractDao {

	private static Logger logger = Logger.getLogger(SBITransactionFetchService.class.getName());

	private static final String merchantIDquery = "Select PG_GATEWAY from TRANSACTION WHERE TXN_ID=? ";

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}
	
	public String fetchMerchantId(String txnId) throws SystemException {
		String merchantID = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(merchantIDquery)) {
				preparedStatement.setString(1, txnId);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						merchantID = resultSet.getString("PG_GATEWAY");
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching MErchant Id for Manual status enquery " + sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}finally {
			autoClose();
		}
		return merchantID;
	}// fetchMerchantID

}