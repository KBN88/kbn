package com.kbn.sbi;

public enum SBIResultType {
	
	HOST_TIMEOUT	("HOST TIMEOUT"),
	DENIED_BY_RISK	("DENIED BY RISK"),
	NOT_APPROVED	("NOT APPROVED"),
	NOT_CAPTURED	("NOT CAPTURED"),
	APPROVED		("APPROVED"),
	CAPTURED		("CAPTURED"),
	FSS0001			("FSS0001"),
	NOT_ENROLED		("NOT ENROLLED"),
	REJECTED		("REJECTED"),
	INITIALIZED		("INITIALIZED"),
	ENROLLED		("ENROLLED");
	
	private SBIResultType(String name){
		this.name = name;
	}
	
	private final String name;

	public String getName() {
		return name;
	}
	
	public static SBIResultType getInstance(String name){
		if(null == name || name ==""){
			return REJECTED;
		}
		
		SBIResultType[] sbiResultTypes = SBIResultType.values();
		
		for(SBIResultType sbiResultType : sbiResultTypes){
			if(sbiResultType.getName().startsWith(name)){
				return sbiResultType;
			}
		}
		
		//Return error if unexpected value is returned in parameter "result"
		return REJECTED;
	}

}
