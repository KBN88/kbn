package com.kbn.sbi;

import com.kbn.pg.core.Processor;

public class SBIProcessorFactory {
	public static Processor getInstance(){
		return new SBIProcessor();
	}

}
