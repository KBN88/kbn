package com.kbn.sbi;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Processor;

public class SBIProcessor implements Processor {
	public void preProcess(Fields fields) throws SystemException {
	}

	public void process(Fields fields) throws SystemException {

		if (fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.NEWORDER.getName())) {
			// New Order Transactions are not processed by SBI
			return;
		}

		if (!fields.get(FieldType.ACQUIRER_TYPE.getName()).equals(AcquirerType.SBI.getCode())) {
			return;
		}

		(new SBIIntegrator()).process(fields);
	}

	public void postProcess(Fields fields) throws SystemException {
	}
}