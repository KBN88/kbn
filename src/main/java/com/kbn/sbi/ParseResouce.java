package com.kbn.sbi;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

public class ParseResouce {
	public ParseResouce() {
	}

	public static Key loadKeyStore(String keystorePath) {
		String keystorelocation = null;
		Key key = null;
		try {
			keystorelocation = keystorePath + File.separator + "keystore.bin";
			key = loadKeys(keystorelocation);
			return key;
		} catch (KeyStoreException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - loadKeyStore method : " + e.getMessage());
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - loadKeyStore method : " + e.getMessage());
			e.printStackTrace();
		} catch (CertificateException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - loadKeyStore method : " + e.getMessage());
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - loadKeyStore method : " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - loadKeyStore method : " + e.getMessage());
			e.printStackTrace();
		} finally {
			keystorelocation = null;
		}
		return key;
	}

	private static Key loadKeys(String keystorelocation) throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException, UnrecoverableKeyException {
		File file = null;
		Key key = null;
		KeyStore ks = null;
		char[] pass = null;
		InputStream is = null;
		try {
			file = new File(keystorelocation);
			if (!file.exists()) {
				return null;
			}
			ks = KeyStore.getInstance("JCEKS");
			pass = getCharArray("password");
			is = new java.io.FileInputStream(file);
			try {
				ks.load(is, pass);
				key = ks.getKey("pgkey", pass);
			} catch (EOFException ex) {
				ex.printStackTrace();
				ks.load(null, pass);
			} finally {
				if (is != null) {
					is.close();
				}
			}
			return key;
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - loadKeys method : " + e.getMessage());
			e.printStackTrace();
			return null;
		} finally {
			file = null;
			ks = null;
			pass = null;
			is = null;
		}
	}

	private static char[] getCharArray(String str) {
		return (str != null) && (!str.equalsIgnoreCase("")) ? str.toCharArray() : null;
	}

	public String getCGNData(String resourcePath, String aliasName, Key key, String absoluteResourcePath) {
		try {
			createCGZFileFromCGN(resourcePath, key);
		} catch (InvalidKeyException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData method : " + e.getMessage());
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData method : " + e.getMessage());
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData method : " + e.getMessage());
			e.printStackTrace();
		} catch (ShortBufferException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData method : " + e.getMessage());
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData method : " + e.getMessage());
			e.printStackTrace();
		} catch (BadPaddingException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData method : " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData method : " + e.getMessage());
			e.printStackTrace();
		}
		byte[] xmlData = null;
		byte[] decryptedText = null;
		xmlData = extractZIPAndReadXML(aliasName, resourcePath, absoluteResourcePath);
		if (xmlData == null) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ " - ParseResource - getCGNData - Empty XML data");
			return new String("Alias Name does not Exits.");
		}
		try {
			decryptedText = decrypt(xmlData, key);
		} catch (InvalidKeyException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData - Reading XML : " + e.getMessage());
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData - Reading XML : " + e.getMessage());
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData - Reading XML : " + e.getMessage());
			e.printStackTrace();
		} catch (ShortBufferException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData - Reading XML : " + e.getMessage());
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData - Reading XML : " + e.getMessage());
			e.printStackTrace();
		} catch (BadPaddingException e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - getCGNData - Reading XML : " + e.getMessage());
			e.printStackTrace();
		}
		if (decryptedText != null) {
			deleteFile(resourcePath + File.separator + "resource.cgz");
			return new String(decryptedText);
		}

		return null;
	}

	private void createCGZFileFromCGN(String resourcePath, Key key)
			throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			ShortBufferException, IllegalBlockSizeException, BadPaddingException {
		File cgzFile = null;
		File cgnFile = null;
		BufferedInputStream data = null;
		byte[] decryptedCgnFileData = null;
		OutputStream os = null;
		try {
			cgzFile = new File(resourcePath + File.separator + "resource.cgz");
			if (!cgzFile.exists())
				cgzFile.createNewFile();
			cgnFile = new File(resourcePath + File.separator + "resource.cgn");
			data = new BufferedInputStream(new java.io.FileInputStream(cgnFile));
			int cgnFileLength = (int) cgnFile.length();
			byte[] cgnFileData = new byte[cgnFileLength];
			while (data.read(cgnFileData) != -1) {
			}
			decryptedCgnFileData = decrypt(cgnFileData, key);
			os = new java.io.FileOutputStream(cgzFile);
			os.write(decryptedCgnFileData);
			os.flush();
			os.close();
			data.close();
		} catch (Exception e) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - createCGZFileFromCGN method : " + e.getMessage());
			e.printStackTrace();
		} finally {
			cgzFile = null;
			cgnFile = null;
			data = null;
			decryptedCgnFileData = null;
			os = null;
		}
	}

	private byte[] extractZIPAndReadXML(String entryName, String resourcePath, String absoluteResourcePath) {
		ZipFile zipFile = null;
		ByteArrayOutputStream os = null;
		java.util.Enumeration<? extends ZipEntry> zipEntries = null;
		try {
			zipFile = new ZipFile(resourcePath + File.separator + "resource.cgz");
			zipEntries = zipFile.entries();
			while (zipEntries.hasMoreElements()) {
				os = new ByteArrayOutputStream();
				ZipEntry entry = (ZipEntry) zipEntries.nextElement();
				if (entry != null) {
					InputStream is = zipFile.getInputStream(entry);
					copyInputStream(is, os);
				}
				String temp = entry.getName();
				if ((temp != null) && (temp.indexOf(".xml") != -1)) {
					temp = temp.substring(0, temp.indexOf(".xml"));
					if (os != null) {
						byte[] data = os.toByteArray();
						if ((data != null) && (data.length > 0))
							iPayPipe.trxnParameters.put(temp + "_" + absoluteResourcePath, data);
						os.close();
					}
				}
			}
		} catch (IOException ioe) {
			System.out.println(Thread.currentThread() + " - " + System.currentTimeMillis()
					+ "; Exception - ParseResource - extractZIPAndReadXML method : " + ioe.getMessage());
			ioe.printStackTrace();
		} finally {
			os = null;
			try {
				if (zipFile != null)
					zipFile.close();
				zipFile = null;
			} catch (Exception localException1) {
			}
			zipEntries = null;
		}
		return (byte[]) iPayPipe.trxnParameters.get(entryName + "_" + absoluteResourcePath);
	}

	public static final void copyInputStream(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte['Ѐ'];
		int len;
		while ((len = in.read(buffer)) >= 0) {// int len;
			out.write(buffer, 0, len);
		}
		in.close();
		out.close();
	}

	public byte[] decrypt(byte[] cipherText, Key key) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, ShortBufferException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(2, key);
			int plainTextLength = cipher.getOutputSize(cipherText.length);
			byte[] tmpPlainText = new byte[plainTextLength];
			int ptLength = cipher.update(cipherText, 0, cipherText.length, tmpPlainText, 0);
			ptLength += cipher.doFinal(tmpPlainText, ptLength);
			byte[] plainText = new byte[ptLength];
			System.arraycopy(tmpPlainText, 0, plainText, 0, ptLength);
			return plainText;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			cipher = null;
		}
	}

	private void deleteFile(String filePath) {
		File file = new File(filePath);
		file.delete();
	}

	public String parseResource(String keystorePath, String resourcePath, String alias, String absoluteKeystorePath,
			String absoluteResourcePath) {
		synchronized (ParseResouce.class) {
			String xmlData = null;
			Key key = null;
			try {
				if ((iPayPipe.trxnParameters.get(alias + "_" + absoluteResourcePath) != null)
						&& (iPayPipe.trxnParameters.get("DEK_KEY_" + absoluteKeystorePath) != null)) {
					byte[] trxnData = decrypt((byte[]) iPayPipe.trxnParameters.get(alias + "_" + absoluteResourcePath),
							(Key) iPayPipe.trxnParameters.get("DEK_KEY_" + absoluteKeystorePath));
					if (trxnData == null) {

						key = null;
						return null;
					}
					String str1 = new String(trxnData);

					key = null;
					return str1;
				}
				key = loadKeyStore(keystorePath);
				if (key != null)
					iPayPipe.trxnParameters.put("DEK_KEY_" + absoluteKeystorePath, key);
				xmlData = getCGNData(resourcePath, alias, key, absoluteResourcePath);
				String str1 = xmlData;

				key = null;
				return str1;
			} catch (Exception e) {
				e.printStackTrace();

				key = null;
				return null;
			} finally {
				key = null;
			}
		}
	}
}
