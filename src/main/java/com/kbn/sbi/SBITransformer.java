package com.kbn.sbi;

import com.fss.plugin.sbi.iPayPipe;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class SBITransformer {
	private iPayPipe pipe = null;

	public SBITransformer(iPayPipe pipe) {
		this.pipe = pipe;
	}

	public iPayPipe getTransaction() {
		return pipe;
	}

	public void setTransaction(iPayPipe pipe) {
		this.pipe = pipe;
	}

	public void updateSBIResponse(Fields fields) {

		String status = getStatus();
		ErrorType errorType = getResponseCode();
		if (StatusType.REJECTED.getName().equals(status)) {
			// This is applicable when we sent invalid request to server
			fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.REJECTED.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.REJECTED.getName());
		} else {
			fields.put(FieldType.STATUS.getName(), status);
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
			fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
		}
		fields.put(FieldType.PAYMENT_ID.getName(), pipe.getPaymentId());
		fields.put(FieldType.ECI.getName(), pipe.getEci());
		fields.put(FieldType.AUTH_CODE.getName(), pipe.getAuth());
		fields.put(FieldType.RRN.getName(), pipe.getRef());
		fields.put(FieldType.PG_REF_NUM.getName(), pipe.getTrackId());
		fields.put(FieldType.AVR.getName(), pipe.getAvr());
		fields.put(FieldType.PG_DATE_TIME.getName(), pipe.getDate());
		fields.put(FieldType.ACQ_ID.getName(), pipe.getTransId());
		fields.put(FieldType.PG_RESP_CODE.getName(), pipe.getError_text());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), pipe.getResult());
		fields.put(FieldType.PG_TXN_STATUS.getName(), pipe.getError());
	}

	public ErrorType getResponseCode() {
		String result = pipe.getResult();
		ErrorType errorType = null;

		SBIResultType sbiResulType = SBIResultType.getInstance(result);
		switch (sbiResulType) {
		case APPROVED:
			errorType = ErrorType.SUCCESS;
			break;
		case CAPTURED:
			errorType = ErrorType.SUCCESS;
			break;
		case DENIED_BY_RISK:
			errorType = ErrorType.DENIED;
			break;
		case ENROLLED:
			errorType = ErrorType.SUCCESS;
			break;
		case FSS0001:
			errorType = ErrorType.AUTHENTICATION_UNAVAILABLE;
			break;
		case HOST_TIMEOUT:
			errorType = ErrorType.TIMEOUT;
			break;
		case NOT_APPROVED:
			errorType = ErrorType.DECLINED;
			break;
		case NOT_CAPTURED:
			errorType = ErrorType.DECLINED;
			break;
		case REJECTED:
			errorType = ErrorType.REJECTED;
			break;
		case NOT_ENROLED:
			errorType = ErrorType.PROCESSING;
			break;
		case INITIALIZED:
			errorType = ErrorType.SUCCESS;
			break;
		default:
			errorType = ErrorType.ACQUIRER_ERROR;
			break;
		}

		return errorType;
	}

	public String getStatus() {
		String result = pipe.getResult();
		String status = "";

		SBIResultType sbiResulType = SBIResultType.getInstance(result);
		switch (sbiResulType) {
		case APPROVED:
			status = StatusType.APPROVED.getName();
			break;
		case CAPTURED:
			status = StatusType.CAPTURED.getName();
			break;
		case DENIED_BY_RISK:
			status = StatusType.DENIED.getName();
			break;
		case ENROLLED:
			status = StatusType.ENROLLED.getName();
			break;
		case FSS0001:
			status = StatusType.AUTHENTICATION_FAILED.getName();
			break;
		case HOST_TIMEOUT:
			status = StatusType.TIMEOUT.getName();
			break;
		case NOT_APPROVED:
			status = StatusType.DECLINED.getName();
			break;
		case NOT_CAPTURED:
			status = StatusType.DECLINED.getName();
			break;
		case NOT_ENROLED:
			status = StatusType.PENDING.getName();
			break;
		case REJECTED:
			status = StatusType.REJECTED.getName();
			break;
		default:
			status = StatusType.ENROLLED.getName();
			break;
		}

		return status;
	}

}
