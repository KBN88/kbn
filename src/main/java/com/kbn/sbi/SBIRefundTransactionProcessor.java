package com.kbn.sbi;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class SBIRefundTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(SBIRefundTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createRefundTransaction(fields);
		logger.info("Refund-Request to SBI:" + request);
		communicator.sendAuthorization(request, fields);
	}
}