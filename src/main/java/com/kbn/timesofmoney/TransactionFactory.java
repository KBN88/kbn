package com.kbn.timesofmoney;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;

/**
 * @author Sunil
 *
 */
public class TransactionFactory {
	
	@SuppressWarnings("incomplete-switch")
	public static String getInstance(Fields fields) throws SystemException{
		DirecpayTransaction transaction = new DirecpayTransaction();
		String request = "";
		switch(TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))){
		case REFUND:
			request = transaction.createRefund(fields);
			break;
		case SALE:
			request = transaction.createSale(fields);
			break;
		case STATUS:
			request = transaction.createEnquiry(fields);
			break;
		}
		return request;
	}
}
