package com.kbn.timesofmoney;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.Processor;

/**
 * @author Sunil
 *
 */
public class ResponseProcessor implements Processor {

	public void preProcess(Fields fields) throws SystemException {

	}

	public void process(Fields fields) throws SystemException {		
				
		String txnType = fields.get(FieldType.TXNTYPE.getName());
		if(txnType.equals(TransactionType.REFUND.getName())){
			fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(),txnType);
			fields.updateTransactionDetails();
		}else if(null != fields.get(FieldType.TXN_ID.getName())){
			updateInternalFields(fields);
			fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(),fields.get(FieldType.TXNTYPE.getName()));
			fields.updateNewOrderDetails();
			fields.updateTransactionDetails();
		}
	}

	public void postProcess(Fields fields) throws SystemException {
		fields.removeInternalFields();
	//	new ResponseCreator().create(fields);
	}

	public void updateInternalFields(Fields fields){
		String pgResponseCode = fields.get(FieldType.PG_TXN_STATUS.getName());
		
		if(pgResponseCode.equals("SUCCESS")){
			fields.put(FieldType.STATUS.getName(), StatusType.CAPTURED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
		} else {
			fields.put(FieldType.STATUS.getName(), StatusType.DECLINED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.DECLINED.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.DECLINED.getResponseMessage());					
		}
	}
}
