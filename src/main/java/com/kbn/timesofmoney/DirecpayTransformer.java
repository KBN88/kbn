package com.kbn.timesofmoney;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

/**
 * @author Sunil
 *
 */
public class DirecpayTransformer {
	private Response response;

	public DirecpayTransformer(Response response){
		this.response = response;
	}

	public void updateResponse(Fields fields){
		
/*		String status = getStatus();
		ErrorType errorType = getResponseCode();
		if(StatusType.REJECTED.getName().equals(status)){
			//This is applicable when we sent invalid request to server
			fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.REJECTED.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.REJECTED.getName());
			return;
		}
		
		fields.put(FieldType.STATUS.getName(), status);
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());*/
		
		fields.put(FieldType.STATUS.getName(), StatusType.CAPTURED.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), "SUCCESS");
		fields.put(FieldType.RESPONSE_CODE.getName(), "SUCCESS");
	}
	
	public ErrorType getResponseCode(){
		String respCode = response.getRespCode();
		ErrorType errorType = null;

		if(null == respCode){
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if(respCode.equals("0")){
			errorType = ErrorType.SUCCESS;
		}  else {
			errorType = ErrorType.ACQUIRER_ERROR;;
		}
		
		return errorType;
	}
	
	public String getStatus(){
		String respCode = response.getRespCode();
		String status = "";

		if(null == respCode){
			status = StatusType.ERROR.getName();
		} else if(respCode.equals("0")){
			status = StatusType.CAPTURED.getName();
		} else if(respCode.equals("1")){
			status = StatusType.ERROR.getName();
		} else if(respCode.equals("2")){
			status = StatusType.FAILED.getName();
		} else {
			status = StatusType.ERROR.getName();
		}
		
		return status;
	}
}
