package com.kbn.timesofmoney;

/**
 * @author Sunil
 *
 */
public enum PaymentMode
{
  NET_BANKING;
  
  private PaymentMode() {}
}
