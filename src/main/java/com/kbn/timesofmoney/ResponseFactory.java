package com.kbn.timesofmoney;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Currency;

/**
 * @author Sunil
 *
 */
public class ResponseFactory {
	private static Logger logger = Logger.getLogger(ResponseFactory.class
			.getName());

	public static Response createResponse(String responseString)
			throws SystemException {

		Response response = new Response();
		
		response.setRespCode("000");

		return response;
	}
	
	public static Map<String, String> parse(String request)  {
		logger.info("Response from direcpay: " + request);
		String [] values = request.split("\\|"); 
		Map<String, String> mapValues = new HashMap<String, String>();
		String currencyCode = "";
		int index = 1;
		for(String value : values){
			if(index > 7){
				break;
			}
			
			value = value.trim();
			value = value.toUpperCase();
			
			switch (index){
			case 1:
				mapValues.put(FieldType.ACQ_ID.getName(), value);
				break;
			case 2:
				mapValues.put(FieldType.PG_TXN_STATUS.getName(), value);
				if(value.equals("SUCCESS")){
					mapValues.put(FieldType.STATUS.getName(), StatusType.CAPTURED.getName());
					mapValues.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getResponseCode());
					mapValues.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
				} else {
					mapValues.put(FieldType.STATUS.getName(), StatusType.DECLINED.getName());
					mapValues.put(FieldType.RESPONSE_CODE.getName(), ErrorType.DECLINED.getResponseCode());
					mapValues.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.DECLINED.getResponseMessage());					
				}
				break;
			case 3:
				mapValues.put(FieldType.CUST_COUNTRY.getName(), value);
				break;
			case 4:
				currencyCode = Currency.getNumericCode(value);
				mapValues.put(FieldType.CURRENCY_CODE.getName(), currencyCode);
				break;
			case 5:
				if(null==value || value.equalsIgnoreCase("null")){
					break;
				}
				mapValues.put(FieldType.PRODUCT_DESC.getName(), value);
				break;
			case 6:
				mapValues.put(FieldType.TXN_ID.getName(), value);
				break;
			case 7:
				mapValues.put(FieldType.AMOUNT.getName(), Amount.formatAmount(value,currencyCode));
				break;			
			}
			
			index++;
		}
		
		return mapValues;
	}
	
	public static void parseRefundResponse(String request, Fields fields)  {
		String [] values = request.split("\\|"); 

		if (values.length == 3) {
			String response = values[1];

			fields.put(FieldType.ACQ_ID.getName(), values[0]);
			fields.put(FieldType.PG_TXN_MESSAGE.getName(), values[2]);

			if (response.equals("SUCCESS")) {
				fields.put(FieldType.STATUS.getName(),
						StatusType.CAPTURED.getName());
				fields.put(FieldType.RESPONSE_CODE.getName(),
						ErrorType.SUCCESS.getResponseCode());
				fields.put(FieldType.RESPONSE_MESSAGE.getName(),
						ErrorType.SUCCESS.getResponseMessage());
			}else{
				fields.put(FieldType.STATUS.getName(),
						StatusType.DECLINED.getName());
				fields.put(FieldType.RESPONSE_CODE.getName(),
						ErrorType.DECLINED.getResponseCode());
				fields.put(FieldType.RESPONSE_MESSAGE.getName(),
						ErrorType.DECLINED.getResponseMessage());
			}

		} else {
			fields.put(FieldType.STATUS.getName(),
					StatusType.ERROR.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.ACQUIRER_ERROR.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.ACQUIRER_ERROR.getResponseMessage());
		}
	}
	
}
