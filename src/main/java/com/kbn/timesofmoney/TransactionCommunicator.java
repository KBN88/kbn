package com.kbn.timesofmoney;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

public class TransactionCommunicator {
	
	private static Logger logger = Logger.getLogger(TransactionCommunicator.class
			.getName());
	
	public String getResponseString(String request, Fields fields)
			throws SystemException {

		PrintWriter out;
		String response = "";
		try {
			out = ServletActionContext.getResponse().getWriter();

			out.write(request);
			//Return response
			response = ErrorType.SUCCESS.getCode();
			fields.put(FieldType.RESPONSE_CODE.getName(),ErrorType.SUCCESS.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.SUCCESS.getResponseMessage());
			
		} catch (IOException iOException) {
			logger.error(iOException);
			response=ErrorType.UNKNOWN.getCode();
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					iOException, "Network Exception with Direcpay");		
		}			
			return response;
	}
	
	public Response getResponseObject(String request, Fields fields) throws SystemException{
		String response = getResponseString(request, fields);
		return ResponseFactory.createResponse(response);
	}
	
	public String transact(Fields fields, String request, String hostUrl) throws SystemException{
		
		String response = "000";

		return response;
	}
}
