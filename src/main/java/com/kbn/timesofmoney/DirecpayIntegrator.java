package com.kbn.timesofmoney;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class DirecpayIntegrator {
	private TransactionCommunicator communicator = new TransactionCommunicator();
	private CryptoManager cryptoManager = CryptoManagerFactory.getCryptoManager();

	public void process(Fields fields) throws SystemException {

		send(fields);

		cryptoManager.secure(fields);
	}//process

	public void send(Fields fields) throws SystemException {
		String request = TransactionFactory.getInstance(fields);
		
		String response = communicator.getResponseString(request, fields);
		
		if (null == response || !response.equals("000")) {
            //insert error transaction fields
			fields.put(FieldType.STATUS.getName(), StatusType.ERROR.getName());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.ACQUIRER_ERROR.getResponseMessage());
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.ACQUIRER_ERROR.getResponseCode());
		}else{
			fields.put(FieldType.STATUS.getName(), StatusType.SENT_TO_BANK.getName());
		}		
	}
}

