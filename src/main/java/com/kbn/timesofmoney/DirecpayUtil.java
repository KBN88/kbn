package com.kbn.timesofmoney;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;

public class DirecpayUtil {
	private static Logger logger = Logger.getLogger(DirecpayUtil.class
			.getName());

	private static Key generateKeyFromString(String secretKey) throws Exception {
		byte[] keyValue = Base64.decodeBase64(secretKey.getBytes());
		Key key = new SecretKeySpec(keyValue, "AES");
		return key;
	}

	public static String encrypt(String valueToEnc, String secretKey)
			throws SystemException {
		String encryptedValue = null;
		try {
			Key key = generateKeyFromString(secretKey);
			Cipher c = Cipher.getInstance("AES");
			c.init(1, key);
			byte[] encValue = c.doFinal(valueToEnc.getBytes());

			encryptedValue = new String(Base64.encodeBase64(encValue));
		} catch (Exception exception) {
			logger.error("Exception", exception);
			throw new SystemException(ErrorType.CRYPTO_ERROR,
					ErrorType.CRYPTO_ERROR.getResponseMessage());
		}
		return encryptedValue;
	}

	public static String decrypt(String encryptedValue, String secretKey)
			throws SystemException {
		String decryptedValue = null;

		try {
			Key key = generateKeyFromString(secretKey);
			Cipher c = Cipher.getInstance("AES");
			c.init(2, key);

			byte[] decordedValue = Base64.decodeBase64(encryptedValue
					.getBytes());
			byte[] decValue = c.doFinal(decordedValue);
			decryptedValue = new String(decValue);
		} catch (Exception exception) {
			logger.error("Exception", exception);
			throw new SystemException(ErrorType.CRYPTO_ERROR,
					ErrorType.CRYPTO_ERROR.getResponseMessage());
		}
		return decryptedValue;
	}
}
