package com.kbn.timesofmoney;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;

/**
 * @author Sunil
 *
 */
public class Mapper {
	public static PaymentMode  getPaymentMode(Fields fields){
		PaymentMode paymentMode = null;
		
		String paymentType = fields.get(FieldType.PAYMENT_TYPE.getName());
		if(paymentType.equals(PaymentType.NET_BANKING.getCode()))
			paymentMode = PaymentMode.NET_BANKING;
		
		return paymentMode;
	}
	
	@SuppressWarnings("incomplete-switch")
	public static String getTransactionUrl(Fields fields){
		String url = "";
		
		switch(TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))){

		case REFUND:
			url = ConfigurationConstants.DIRECPAY_REFUND_URL.getValue();
			break;
		case ENQUIRY:
			url = ConfigurationConstants.DIRECPAY_STATUS_ENQUIRY.getValue();
			break;
		}
		
		return url;
	}
	
	public static ErrorType getErrorType(String PaytmResponseCode){
		ErrorType error = null;
		
		if(PaytmResponseCode.equals("01")){
			error = ErrorType.SUCCESS;
		} else if(PaytmResponseCode.equals("03")){
			error = ErrorType.TIMEOUT;
		} else if(PaytmResponseCode.equals("10")){
			error = ErrorType.SUCCESS;
		} else if(PaytmResponseCode.equals("141")){
			error = ErrorType.CANCELLED;
		} else if(PaytmResponseCode.equals("142")){
			error = ErrorType.CANCELLED;
		} else {
			error = ErrorType.DECLINED;
		}
		
		return error;
	}
	
	public static StatusType getStatusType(String paytmResponseCode){
		StatusType status = null;
		
		if(paytmResponseCode.equals("01")){
			status = StatusType.CAPTURED;
		} else if(paytmResponseCode.equals("03")){
			status = StatusType.TIMEOUT;
		} else if(paytmResponseCode.equals("10")){
			status = StatusType.SETTLED;
		} else if(paytmResponseCode.equals("141")){
			status = StatusType.CANCELLED;
		} else if(paytmResponseCode.equals("142")){
			status = StatusType.CANCELLED;
		} else {
			status = StatusType.DECLINED;
		}
		
		return status;
	}
}
