/**
 * 
 */
package com.kbn.timesofmoney;


/**
 * @author Sunil
 *
 */
public enum DirecpayResultType {
	TXN_SUCCESS		("TXN_SUCCESS"),
	TXN_FAILURE		("TXN_FAILURE");
	
	private DirecpayResultType(String name){
		this.name = name;
	}
	
	private final String name;

	public String getName() {
		return name;
	}
	
	public static DirecpayResultType getInstance(String name){
		if(null == name){
			return TXN_FAILURE;
		}
		
		DirecpayResultType[] paytmResultTypes = DirecpayResultType.values();
		
		for(DirecpayResultType paytmResultType : paytmResultTypes){
			if(paytmResultType.getName().startsWith(name)){
				return paytmResultType;
			}
		}
		
		//Return error if unexpected value is returned in parameter "respCode"
		return TXN_FAILURE;
	}
}

