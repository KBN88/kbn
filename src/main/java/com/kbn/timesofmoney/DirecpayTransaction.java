package com.kbn.timesofmoney;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Amount;

/**
 * @author Sunil
 *
 */
public class DirecpayTransaction {

	private String requestparameter;
	
	public String createRefund(Fields fields) throws SystemException{

		PropertiesManager propertiesManager = new PropertiesManager();
		fields.put(FieldType.RETURN_URL.getName(),propertiesManager.getSystemProperty("DirecpayRefundResponse"));
		fields.put(FieldType.REQUEST_URL.getName(),propertiesManager.getSystemProperty("DirecpayRefundUrl"));

		requestparameter = fields.get(FieldType.TXN_ID.getName()) + 
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.ACQ_ID.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.MERCHANT_ID.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.ORIG_TXN_ID.getName()) + 
				Constants.DIRECPAY_SEPARATOR.getValue() + Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),fields.get(FieldType.CURRENCY_CODE.getName()))+ 
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.RETURN_URL.getName());
		requestparameter = requestparameter.replaceAll("\n","");

		// Request formation for Refund Transaction					
		StringBuilder request = new StringBuilder();
		request.append("<HTML>");
		request.append("<BODY OnLoad=\"OnLoadEvent();\" >");
		request.append("<form name=\"form1\" action=\"");
		request
		.append(fields.get(FieldType.REQUEST_URL.getName()));
		request.append("?");
		request.append("requestparams=");
		request.append(DirecpayUtil.encrypt(getRequestparameter(),fields.get(FieldType.PASSWORD.getName())));
		request.append("\" method=\"post\">");
		request
		.append("<input type=\"hidden\" name=\"merchantId\" value=\"");
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append("\">");
		request.append("</form>");
		request.append("<script language=\"JavaScript\">");
		request.append("function OnLoadEvent()");
		request.append("{document.form1.submit();}");
		request.append("</script>");
		request.append("</BODY>");
		request.append("</HTML>");
				
		fields.put(FieldType.ACQUIRER_TYPE.getName(),
				AcquirerType.DIREC_PAY.getCode());
		 
		return request.toString();
	}

	public String createSale(Fields fields) throws SystemException{
		
		String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));

		// Fetching Details from Property file
		PropertiesManager propertiesManager = new PropertiesManager();
		String direcpayTransactionUrl = propertiesManager.getSystemProperty("DirecpayRequestUrl");

		String direcpayMID = fields.get(FieldType.MERCHANT_ID.getName());
		String direcpayKey = fields.get(FieldType.PASSWORD.getName());
		requestparameter = direcpayMID + 
				Constants.DIRECPAY_SEPARATOR.getValue() + propertiesManager.getSystemProperty("DirecpayOperatingMode") +
				Constants.DIRECPAY_SEPARATOR.getValue() + propertiesManager.getSystemProperty("DirecpayCountry") +
				Constants.DIRECPAY_SEPARATOR.getValue() + propertiesManager.getSystemProperty("DirecpayCurrency") + 
				Constants.DIRECPAY_SEPARATOR.getValue() + amount +  
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.TXN_ID.getName()) + 
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.PRODUCT_DESC.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + propertiesManager.getSystemProperty("DirecpayTxnResponse") +
				Constants.DIRECPAY_SEPARATOR.getValue() + propertiesManager.getSystemProperty("DirecpayTxnResponse") +
				Constants.DIRECPAY_SEPARATOR.getValue() + propertiesManager.getSystemProperty("DirecpayCollaborator")+
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.MOP_TYPE.getName()) + "-" +  MopType.getmopName(fields.get(FieldType.MOP_TYPE.getName())) +
				Constants.DIRECPAY_SEPARATOR.getValue() + propertiesManager.getSystemProperty("DirecpayCardType");
		requestparameter = requestparameter.replaceAll("\n","");

		String billingDtls =  fields.get(FieldType.CUST_NAME.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_STREET_ADDRESS1.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_CITY.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_STATE.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_ZIP.getName()) + 
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_COUNTRY.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + "0" + // Phone number prefix
				Constants.DIRECPAY_SEPARATOR.getValue() + "22" + // Phone number prefix
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_PHONE.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_PHONE.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_EMAIL.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.PRODUCT_DESC.getName());
		billingDtls = billingDtls.replaceAll("\n","");

		String shippingDtls =  fields.get(FieldType.CUST_SHIP_NAME.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_SHIP_STREET_ADDRESS1.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_SHIP_CITY.getName()) + 
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_SHIP_STATE.getName()) +  
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_SHIP_ZIP.getName()) + 
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_SHIP_COUNTRY.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + "0" + // Phone number prefix
				Constants.DIRECPAY_SEPARATOR.getValue() + "22" + // Phone number prefix
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_SHIP_PHONE.getName()) +
				Constants.DIRECPAY_SEPARATOR.getValue() + fields.get(FieldType.CUST_SHIP_PHONE.getName());
		shippingDtls = shippingDtls.replaceAll("\n","");

		// Request formation for Sale Transaction		
		StringBuilder httpRequest = new StringBuilder();
		httpRequest.append("<HTML>");
		httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
		httpRequest.append("<form name=\"form1\" action=\"");
		httpRequest
				.append(direcpayTransactionUrl);
		httpRequest.append("\" method=\"post\">");
		httpRequest
				.append("<input type=\"hidden\" name=\"requestparameter\" value=\"");
		httpRequest.append(DirecpayUtil.encrypt(getRequestparameter(),direcpayKey));
		httpRequest.append("\">");
		httpRequest
				.append("<input type=\"hidden\" name=\"billingDtls\" value=\"");
		httpRequest.append(DirecpayUtil.encrypt(billingDtls,direcpayKey));
		httpRequest.append("\">");
		httpRequest
				.append("<input type=\"hidden\" name=\"shippingDtls\" value=\"");
		httpRequest.append(DirecpayUtil.encrypt(shippingDtls,direcpayKey));
		httpRequest.append("\">");
		httpRequest
				.append("<input type=\"hidden\" name=\"merchantId\" value=\"");
		httpRequest.append(direcpayMID);
		httpRequest.append("\">");
		httpRequest.append("</form>");
		httpRequest.append("<script language=\"JavaScript\">");
		httpRequest.append("function OnLoadEvent()");
		httpRequest.append("{document.form1.submit();}");
		httpRequest.append("</script>");
		httpRequest.append("</BODY>");
		httpRequest.append("</HTML>");		
		return httpRequest.toString();
	}

	public String createEnquiry(Fields fields) {
		StringBuilder request = new StringBuilder();

		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append("");
		jsonRequest.append(request);
		jsonRequest.append("");		
		return jsonRequest.toString();
	}

	public String getRequestparameter() {
		return requestparameter;
	}

	public void setRequestparameter(String requestparameter) {
		this.requestparameter = requestparameter;
	}

}
