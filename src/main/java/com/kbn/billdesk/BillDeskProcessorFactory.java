package com.kbn.billdesk;

import com.kbn.pg.core.Processor;

public class BillDeskProcessorFactory {

	public static Processor getInstance() {
		return new BillDeskPayProcessor();
	}
}
