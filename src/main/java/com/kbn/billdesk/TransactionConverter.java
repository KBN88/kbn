package com.kbn.billdesk;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.WhitelableBranding;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.NetBankingType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.WalletType;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Currency;

public class TransactionConverter {
	private static Logger logger = Logger.getLogger(TransactionConverter.class.getName());

	public String createSaleTransaction(Fields fields) throws SystemException {
		// White-Label Response url
		UserDao userDao = new UserDao();
		HttpServletRequest requesturl = ServletActionContext.getRequest();
		String brandingURL = requesturl.getRequestURL().toString();
		URL urlTemp = null;
		try {
			urlTemp = new URL(brandingURL);
		} catch (MalformedURLException exception) {
			logger.error("WhitLabe Brand HostUrl error :" + exception);
		}
		String brandingHost = urlTemp.getHost();
		logger.info("WhitLabe Brand Billdesk  HostUrl :" + brandingHost);
		WhitelableBranding userReseller = userDao.findByWhitelabelBrandURL(brandingHost);

		StringBuilder request = new StringBuilder();
		String BillDeskTransactionUrl = new PropertiesManager().getSystemProperty("BillDeskRequestUrl");
		String BillDeskNetBankingUrl = new PropertiesManager().getSystemProperty("BillDeskNetBankingUrl");
		String CardExperDate = fields.get(FieldType.CARD_EXP_DT.getName());
		if (PaymentType.NET_BANKING.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
			appendBankInfo(fields, request, userReseller);
		} else if (PaymentType.WALLET.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
			appendWalletInfo(fields, request, userReseller);
		} else {
			appendCardInfo(fields, request, userReseller);
		}
		String msg = BillDeskChecksumUtil.main(request.toString(),
				ConfigurationConstants.BILLDESK_CHECKSUM_KEY.getValue());
		StringBuilder httpRequest = new StringBuilder();
		request.append(Constants.PIPE_SEPARATOR);
		request.append(msg);

		// Request formation for seam less Net-bank transaction
		if (PaymentType.NET_BANKING.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {

			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(BillDeskNetBankingUrl);
			httpRequest.append("\" method=\"post\">");
			httpRequest.append("<input type=\"hidden\" name=\"msg\" value=\"");
			httpRequest.append(request);
			httpRequest.append("\">");
			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");

		} else if (PaymentType.WALLET.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
			// Request formation for seam less Wallet transaction
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(BillDeskNetBankingUrl);
			httpRequest.append("\" method=\"post\">");
			httpRequest.append("<input type=\"hidden\" name=\"msg\" value=\"");
			httpRequest.append(request);
			httpRequest.append("\">");
			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");

		} else {
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(BillDeskTransactionUrl);
			httpRequest.append("\" method=\"post\">");
			httpRequest.append("<input type=\"hidden\" name=\"msg\" value=\"");
			httpRequest.append(request);
			httpRequest.append("\">");
			httpRequest.append("<input type=\"hidden\" name=\"cnumber\" value=\"");
			httpRequest.append(fields.get(FieldType.CARD_NUMBER.getName()));
			httpRequest.append("\">");
			httpRequest.append("<input type=\"hidden\" name=\"expmon\" value=\"");
			httpRequest.append(CardExperDate.substring(0, 2));
			httpRequest.append("\">");
			httpRequest.append("<input type=\"hidden\" name=\"expyr\" value=\"");
			httpRequest.append(CardExperDate.substring(2, 6));
			httpRequest.append("\">");
			httpRequest.append("<input type=\"hidden\" name=\"cvv2\" value=\"");
			httpRequest.append(fields.get(FieldType.CVV.getName()));
			httpRequest.append("\">");
			httpRequest.append("<input type=\"hidden\" name=\"cardType\" value=\"");
			httpRequest.append(Constants.CARD_TYPE);
			httpRequest.append("\">");
			httpRequest.append("<input type=\"hidden\" name=\"cname2\" value=\"");
			httpRequest.append(fields.get(FieldType.CUST_NAME.getName()));
			httpRequest.append("\">");
			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");
		}
		return httpRequest.toString();
	}

	private void appendCardInfo(Fields fields, StringBuilder request, WhitelableBranding userReseller) {
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.TXN_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER1);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName())));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.BANK_ID);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER2);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER3);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName())));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ITEM_CODE);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.TYPE_FIELD1);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.PASSWORD.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER4);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER5);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.TYPE_FIELD2);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.ORDER_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.CUST_EMAIL.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.CUST_PHONE.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO4);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO5);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO6);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO7);
		request.append(Constants.PIPE_SEPARATOR);
		if (userReseller != null) {
			request.append("https://"+userReseller.getBrandURL()+"/crm/jsp/billdeskresponse");
		} else {
			request.append(ConfigurationConstants.BILLDESK_RETURN_URL.getValue());
		}

	}

	private void appendBankInfo(Fields fields, StringBuilder request, WhitelableBranding userReseller) {
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.TXN_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER1);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName())));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(NetBankingType.getBillDeskCode(fields.get(FieldType.MOP_TYPE.getName())));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER2);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER3);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName())));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ITEM_CODE);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.TYPE_FIELD1);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.PASSWORD.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER4);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER5);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.TYPE_FIELD2);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.ORDER_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.CUST_EMAIL.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.CUST_PHONE.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO4);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO5);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO6);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO7);
		request.append(Constants.PIPE_SEPARATOR);
		if (userReseller != null) {
			request.append("https://"+userReseller.getBrandURL()+"/crm/jsp/billdeskresponse");
		} else {
			request.append(ConfigurationConstants.BILLDESK_RETURN_URL.getValue());
		}

	}

	private void appendWalletInfo(Fields fields, StringBuilder request, WhitelableBranding userReseller) {
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.TXN_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER1);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName())));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(WalletType.getBillDeskCode(fields.get(FieldType.MOP_TYPE.getName())));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER2);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER3);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName())));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ITEM_CODE);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.TYPE_FIELD1);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.PASSWORD.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER4);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER5);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.TYPE_FIELD2);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.ORDER_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.CUST_EMAIL.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.CUST_PHONE.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO4);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO5);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO6);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.ADDITIONAL_INFO7);
		request.append(Constants.PIPE_SEPARATOR);
		if (userReseller != null) {
			request.append("https://"+userReseller.getBrandURL()+"/crm/jsp/billdeskresponse");
		} else {
			request.append(ConfigurationConstants.BILLDESK_RETURN_URL.getValue());
		}

	}

	public String createRefundTransaction(Fields fields) throws SystemException {
		// Fetching Key from Property file
		String checkSumKey = ConfigurationConstants.BILLDESK_CHECKSUM_KEY.getValue();
		RefundTransactionAmountService refundTransactionAmountService = new RefundTransactionAmountService();
		String originalAmount = refundTransactionAmountService
				.todayRefundAmountFatch(fields.get(FieldType.OID.getName()));
		String date = fields.get(FieldType.PG_DATE_TIME.getName());
		Date simpleDate = null;
		try {
			simpleDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException exception) {
			logger.error("date error" + exception);
		}
		SimpleDateFormat formatter = new SimpleDateFormat("YYYYMMdd");
		String pgDate = formatter.format(simpleDate);
		Date currentDateTime = new Date();
		SimpleDateFormat formatter1 = new SimpleDateFormat("YYYYMMddHHmmss");
		String systemDateTime = formatter1.format(currentDateTime);
		StringBuilder request = new StringBuilder();
		request.append("0400");
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.PG_REF_NUM.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(pgDate);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.ORIG_TXN_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(originalAmount);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName())));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(systemDateTime);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.TXN_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER1);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER2);
		request.append(Constants.PIPE_SEPARATOR);
		request.append(Constants.FILLER3);
		// Generating Hash
		String hash = BillDeskChecksumUtil.main(request.toString(), checkSumKey);
		// Generating Final Request with CheckSum
		StringBuilder finalrequest = new StringBuilder();
		finalrequest.append(request);
		finalrequest.append(Constants.PIPE_SEPARATOR);
		finalrequest.append(hash);
		return finalrequest.toString();

	}

	// for Automate Inquiry
	public String prepareFieldsForStatus(Fields fields) throws SystemException {

		String checkSumKey = ConfigurationConstants.BILLDESK_CHECKSUM_KEY.getValue();
		Date currentDateTime = new Date();
		SimpleDateFormat formatter1 = new SimpleDateFormat("YYYYMMddHHmmss");
		String systemDateTime = formatter1.format(currentDateTime);

		StringBuilder request = new StringBuilder();
		request.append("0122");
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.TXN_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(systemDateTime);
		String hash = BillDeskChecksumUtil.main(request.toString(), checkSumKey);

		// Generating Final Request with CheckSum
		StringBuilder finalrequest = new StringBuilder();
		finalrequest.append(request);
		finalrequest.append(Constants.PIPE_SEPARATOR);
		finalrequest.append(hash);
		return finalrequest.toString();

	}

	// for manualStatus Inquiry
	public String prepareFieldsForManualStatus(Fields fields) throws SystemException {

		String checkSumKey = ConfigurationConstants.BILLDESK_CHECKSUM_KEY.getValue();
		Date currentDateTime = new Date();
		SimpleDateFormat formatter1 = new SimpleDateFormat("YYYYMMddHHmmss");
		String systemDateTime = formatter1.format(currentDateTime);
		StringBuilder request = new StringBuilder();

		request.append("0122");
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(fields.get(FieldType.TXN_ID.getName()));
		request.append(Constants.PIPE_SEPARATOR);
		request.append(systemDateTime);
		String hash = BillDeskChecksumUtil.main(request.toString(), checkSumKey);

		// Generating Final Request with CheckSum
		StringBuilder finalrequest = new StringBuilder();
		finalrequest.append(request);
		finalrequest.append(Constants.PIPE_SEPARATOR);
		finalrequest.append(hash);
		return finalrequest.toString();

	}
}