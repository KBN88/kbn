package com.kbn.billdesk;

public class Constants {
	public static final String PIPE_SEPARATOR = "|";
	public static final String FILLER1 = "NA";
	public static final String FILLER2 = "NA";
	public static final String FILLER3 = "NA";
	public static final String FILLER4 = "NA";
	public static final String FILLER5 = "NA";
	public static final String TYPE_FIELD1 = "R";
	public static final String TYPE_FIELD2 = "F";
	public static final String ACCESS_KEY = "access_key";

	public static final String MERCHANT_ID = "MerchantID";
	public static final String TXN_ID = "CustomerID";
	public static final String AMOUNT = "TxnAmount";
	public static final String BANK_ID = "CARD";
	public static final String NETBANKING_BANK_ID = "BOB";
	public static final String CURRENCY = "CurrencyType";
	public static final String ITEM_CODE = "DIRECT";
	public static final String SECURITY_ID = "SecurityID";
	public static final String ADDITIONAL_INFO1 = "AdditionalInfo1";
	public static final String ADDITIONAL_INFO2 = "AdditionalInfo2";
	public static final String ADDITIONAL_INFO3 = "AdditionalInfo3";
	public static final String ADDITIONAL_INFO4 = "NA";
	public static final String ADDITIONAL_INFO5 = "NA";
	public static final String ADDITIONAL_INFO6 = "NA";
	public static final String ADDITIONAL_INFO7 = "NA";
	public static final String RETURN_URL = "RU";
	public static final String CARD_TYPE = "NA";
	public static final String CARD_NUMBER = "cnumber";
	public static final String CARD_HOLDER_NAME = "cname2";
	public static final String EXPIRY_MONTH = "expmon";
	public static final String EXPIRY_YEAR = "expyr";
	public static final String CVV_NUMBER = "cvv2";
	public static final String CHECK_SUM = "Checksum";
	public static final String QUESTION_MARK = "?";
	public static final String EQUATOR = "=";
	public static final String REFUNDTAYPEVALUE = "0400";
	public static final String STATUSTAYPEVALUE = "0122";

}
