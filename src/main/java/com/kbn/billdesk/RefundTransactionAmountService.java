package com.kbn.billdesk;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;

public class RefundTransactionAmountService extends HibernateAbstractDao {
	private static Logger logger = Logger.getLogger(RefundTransactionAmountService.class.getName());

	private static final String todayRefundAmountQuery = "select AMOUNT from TRANSACTION where OID=? AND txntype='SALE' AND STATUS='Captured'";

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	// today refunded AMOUNT
	public String todayRefundAmountFatch(String oid) throws SystemException {
		String amount = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(todayRefundAmountQuery)) {
				preparedStatement.setString(1, oid);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						amount = resultSet.getString(FieldType.AMOUNT.getName());
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching refunded amount " + sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}finally {
			autoClose();
		}
		return amount;
	}

}
