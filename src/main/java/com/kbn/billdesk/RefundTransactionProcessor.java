package com.kbn.billdesk;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class RefundTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(RefundTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createRefundTransaction(fields);
		logger.info("Request to BillDesk: " + request);
		String response = communicator.transactStatus(request, ConfigurationConstants.BILLDESK_REFUND_URL.getValue());
		BillDeskTransformer transformer = new BillDeskTransformer(response);
		transformer.updateBillDeskRefundResponse(fields);

	}

}
