package com.kbn.billdesk;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.SystemConstants;

public class BillDeskChecksumUtil {
	private static final String HASH_ALGORITHM = "HmacSHA256";

	// hash_hmac('sha256',$str,'Your-Checksum-Key-Here', false);
	public static String main(String msg2, String Checksumkey) throws SystemException {
		String result = "";
		try {
			byte[] keyBytes = Checksumkey.getBytes();
			SecretKeySpec signingKey = new SecretKeySpec(keyBytes, HASH_ALGORITHM);

			Mac mac = Mac.getInstance(HASH_ALGORITHM);
			mac.init(signingKey);
			byte[] rawHmac = mac.doFinal(msg2.getBytes());

			byte[] hexBytes = new Hex().encode(rawHmac);
			result = new String(hexBytes, SystemConstants.DEFAULT_ENCODING_UTF_8);
		} catch (NoSuchAlgorithmException noSuchAlgorithmException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, noSuchAlgorithmException,
					"No such Hashing algoritham with BillDeks");
		} catch (InvalidKeyException invalidKeyException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, invalidKeyException,
					"No such key with BillDeks");
		} catch (UnsupportedEncodingException unsupportedEncodingException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, unsupportedEncodingException,
					"No such encoding supported with BillDeks");
		}
		return result.toUpperCase();
	}

	public boolean compareResponseHash(Fields fields, String responseString) throws SystemException {
		String Checksumkey = ConfigurationConstants.BILLDESK_CHECKSUM_KEY.getValue();
		String responsewithouthash = (responseString.substring(0, responseString.lastIndexOf('|')));
		String calculatedHash = BillDeskChecksumUtil.main(responsewithouthash, Checksumkey);

		String[] responseArray = responseString.split("\\|");
		String receivedHash = responseArray[25];

		return calculatedHash.equals(receivedHash);

	}

	public boolean compareResponseStatusHash(Fields fields, String responseString) throws SystemException {
		String Checksumkey = ConfigurationConstants.BILLDESK_CHECKSUM_KEY.getValue();
		String responsewithouthash = (responseString.substring(0, responseString.lastIndexOf('|')));
		String calculatedHash = BillDeskChecksumUtil.main(responsewithouthash, Checksumkey);

		String[] responseArray = responseString.split("\\|");
		String receivedHash = responseArray[32];

		return calculatedHash.equals(receivedHash);

	}

	public boolean compareResponseRefundHash(Fields fields, String responseString) throws SystemException {
		String Checksumkey = ConfigurationConstants.BILLDESK_CHECKSUM_KEY.getValue();
		String responsewithouthash = (responseString.substring(0, responseString.lastIndexOf('|')));
		String calculatedHash = BillDeskChecksumUtil.main(responsewithouthash, Checksumkey);

		String[] responseArray = responseString.split("\\|");
		String receivedHash = responseArray[13];

		return calculatedHash.equals(receivedHash);

	}
}
