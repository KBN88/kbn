package com.kbn.billdesk;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.AccountCurrency;

public class BillDeskTransactionFetchService extends HibernateAbstractDao {

	private static Logger logger = Logger.getLogger(BillDeskTransactionFetchService.class.getName());

	private static final String acquirerPayIdQuery = "select acquirerPayId from Account where acquirerName=?";
	private static final String accountDetailsQuery = "select merchantId, password, txnKey, currencyCode from AccountCurrency where acqPayId=?";
	private static final String transactionQuery = "Select PG_GATEWAY, CURRENCY_CODE, ORIG_TXN_ID, PAY_ID from TRANSACTION WHERE TXN_ID=? ";

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public String fetchAcquirerPayId(String acquirerName) throws SystemException {
		String acquirerPayId = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(acquirerPayIdQuery)) {
				preparedStatement.setString(1, acquirerName);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						acquirerPayId = resultSet.getString("acquirerPayId");
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching Acquirer Pay Id for Manual status enquery " + sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		} finally {
			autoClose();
		}
		return acquirerPayId;
	}// fetchAcquirerPayId

	public List<AccountCurrency> fetchAccountDetails(String acquirerName) throws SystemException {

		List<AccountCurrency> accountSummaryList = new ArrayList<AccountCurrency>();

		String acquirerPayId = fetchAcquirerPayId(acquirerName);

		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(accountDetailsQuery)) {
				preparedStatement.setString(1, acquirerPayId);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						AccountCurrency accountCurrency = new AccountCurrency();

						accountCurrency.setMerchantId(resultSet.getString("merchantId"));
						accountCurrency.setPassword(resultSet.getString("password"));
						accountCurrency.setTxnKey(resultSet.getString("txnKey"));
						accountCurrency.setCurrencyCode(resultSet.getString("currencyCode"));

						accountSummaryList.add(accountCurrency);

					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching Account Details for Manual status enquery " + sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		} finally {
			autoClose();
		}
		return accountSummaryList;

	}// fetchAccountDetails

	public Map<String, String> fetchTransaction(String txnId) throws SystemException {
		Map<String, String> requestMap = new HashMap<String, String>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(transactionQuery)) {
				preparedStatement.setString(1, txnId);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						requestMap.put("merchantID", resultSet.getString("PG_GATEWAY"));
						requestMap.put("currencyCode", resultSet.getString("CURRENCY_CODE"));
						requestMap.put("origTxnID", resultSet.getString("ORIG_TXN_ID"));
						requestMap.put("payID", resultSet.getString("PAY_ID"));
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching MErchant Id for Manual status enquery " + sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		} finally {
			autoClose();
		}
		return requestMap;
	}// fetchTransaction

}