package com.kbn.billdesk;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class StatusTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(StatusTransactionProcessor.class.getName());

	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.prepareFieldsForStatus(fields);
		logger.info("Satus Request to BillDesk: " + request);
		String responseTrimmed = communicator.transactStatus(request,
				ConfigurationConstants.BILLDESK_ENQUIRY_URL.getValue());
		BillDeskTransformer transformer = new BillDeskTransformer(responseTrimmed);
		transformer.updateBillDeskStatusResponse(fields);
		fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));

	}

}
