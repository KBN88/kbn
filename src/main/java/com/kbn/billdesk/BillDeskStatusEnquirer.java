package com.kbn.billdesk;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionSearchService;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;

public class BillDeskStatusEnquirer {
	private static Logger logger = Logger.getLogger(BillDeskStatusEnquirer.class.getName());

	public void statusEnquirer() {

		TransactionSearchService transactionSearchService = new TransactionSearchService();
		try {
			List<TransactionSummaryReport> transactionList = transactionSearchService
					.getTransactionsForStatusUpdate(AcquirerType.BILLDESK.getCode());
			for (TransactionSummaryReport transaction : transactionList) {

				Map<String, String> reqMap = prepareFields(transaction);
				Fields fields = new Fields(reqMap);

				StatusTransactionProcessor processor = new StatusTransactionProcessor();
				processor.transact(fields);

				// transaction should not be updated in database if it is not SUCCESS/FAILED
				// txn, errors are logged in PG logs
				String response = fields.get(FieldType.STATUS.getName());
				if (!response.equals("Error")) {
					// update new order transaction
					fields.updateStatus();
					// Update sale txn
					fields.updateTransactionDetails();
				}

			}
		} catch (SystemException systemException) {
			logger.error("Error updating status for Bill Desk", systemException);
		}
	}

	private Map<String, String> prepareFields(TransactionSummaryReport transaction) {
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put(FieldType.MERCHANT_ID.getName(), transaction.getPgGateway());
		requestMap.put(FieldType.PAY_ID.getName(), transaction.getPayId());
		requestMap.put(FieldType.TXN_ID.getName(), transaction.getTransactionId());
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), transaction.getOrigTransactionId());
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.BILLDESK.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), transaction.getCurrencyCode());
		return requestMap;
	}

	public String manualStatusEnquirer(String orderId) {

		String response = null;

		try {

			Map<String, String> reqMap = prepareFields(orderId);
			Fields fields = new Fields(reqMap);

			BillDeskManualStatusTransactionProcessor processor = new BillDeskManualStatusTransactionProcessor();
			processor.transact(fields);

			response = fields.get(FieldType.STATUS.getName());
			logger.info("BIll Desk Status Response:" + response);

			// transaction should not be updated in database if it is not SUCCESS/FAILED
			// txn, errors are logged in PG logs
			if (!response.equals("Error")) {
				// update new order transaction
				fields.updateStatus();
				// Update sale txn
				fields.updateTransactionDetails();

			}

		} catch (SystemException systemException) {
			logger.error("Error updating manual status for Bill Desk", systemException);
		}
		logger.info("BIll Desk Status Response before return Statement:");
		return response;
	}

	private Map<String, String> prepareFields(String txnID) throws SystemException {

		BillDeskTransactionFetchService fetchTxn = new BillDeskTransactionFetchService();
		Map<String, String> responseMap = fetchTxn.fetchTransaction(txnID);

		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put(FieldType.PAY_ID.getName(), responseMap.get("payID"));
		requestMap.put(FieldType.MERCHANT_ID.getName(), responseMap.get("merchantID"));
		requestMap.put(FieldType.TXN_ID.getName(), txnID);
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), responseMap.get("origTxnID"));
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.BILLDESK.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), responseMap.get("currencyCode"));
		return requestMap;
	}
}
