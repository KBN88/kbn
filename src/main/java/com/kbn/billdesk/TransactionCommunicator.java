package com.kbn.billdesk;

import java.io.IOException;
import java.io.PrintWriter;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

public class TransactionCommunicator {
	private static Logger logger = Logger.getLogger(TransactionCommunicator.class.getName());

	public String sendAuthorization(String request, Fields fields) throws SystemException {
		PrintWriter out;
		String response = "";
		logger.info("Request sent to BillDesk: " + request);
		try {
			out = ServletActionContext.getResponse().getWriter();

			out.write(request);
			// Return response
			response = ErrorType.SUCCESS.getCode();
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
			fields.put(FieldType.PG_GATEWAY.getName(), fields.get(FieldType.MERCHANT_ID.getName()));

		} catch (IOException iOException) {
			logger.error(iOException);
			response = ErrorType.UNKNOWN.getCode();
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, iOException, "Network Exception with BillDesk");
		}
		return response;

	}

	public String transactStatus(String request, String hostUrl) throws SystemException {
		PostMethod postMethod = new PostMethod(hostUrl);
		postMethod.addParameter("msg", request);

		return transact(postMethod, hostUrl);

	}

	public String transact(HttpMethod httpMethod, String hostUrl) throws SystemException {
		String response = "";

		try {
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(httpMethod);

			if (httpMethod.getStatusCode() == HttpStatus.SC_OK) {
				response = httpMethod.getResponseBodyAsString();
				logger.info("Response from Bill Desk: " + response);
			} else {
				throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, "Network Exception with Bill Desk "
						+ hostUrl.toString() + "recieved response code" + httpMethod.getStatusCode());
			}
		} catch (IOException ioException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with Bill Desk " + hostUrl.toString());
		}
		return response;

	}

}
