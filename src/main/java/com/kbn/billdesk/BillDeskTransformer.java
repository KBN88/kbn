package com.kbn.billdesk;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.Amount;

public class BillDeskTransformer {

	private String[] billDeskResponse;
	private String responseString;
	PropertiesManager propertiesManager = new PropertiesManager();
	String billdeskkey = propertiesManager.getSystemProperty("Checksum_key");

	public BillDeskTransformer(String responseString) {
		this.responseString = responseString;
		this.billDeskResponse = responseString.split("\\|");
	}

	public ErrorType getSaleResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("0300")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("0399")) {
			errorType = ErrorType.REJECTED;
		} else if (respCode.equals("0001")) {
			errorType = ErrorType.INVALID_FIELD_VALUE;
		} else if (respCode.equals("0002")) {
			errorType = ErrorType.DENIED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getSaleStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("0300")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("0399")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("0001")) {
			status = StatusType.ERROR;
		} else if (respCode.equals("0002")) {
			status = StatusType.PENDING;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public ErrorType getRefundResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("Y")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("N")) {
			errorType = ErrorType.REJECTED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getRefundEnquriyStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Y")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("N")) {
			status = StatusType.REJECTED;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public ErrorType getResponse(String respCode, String authStatus) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("Y") && authStatus.equals("0300")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("Y") && authStatus.equals("0002")) {
			errorType = ErrorType.DECLINED;
		} else if (respCode.equals("Y") && authStatus.equals("0399")) {
			errorType = ErrorType.REJECTED;
		} else if (respCode.equals("Y") && authStatus.equals("0001")) {
			errorType = ErrorType.REJECTED;
		} else if (respCode.equals("N")) {
			errorType = ErrorType.REJECTED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getStatus(String respCode, String authStatus) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Y") && authStatus.equals("0300")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("Y") && authStatus.equals("0399")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("Y") && authStatus.equals("0002")) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Y") && authStatus.equals("0001")) {
			status = StatusType.ERROR;
		} else if (respCode.equals("N")) {
			status = StatusType.ERROR;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	// Sale Transaction
	public void updateBillDeskResponse(Fields fields) throws SystemException {
		BillDeskChecksumUtil billDeskChecksumUtil = new BillDeskChecksumUtil();
		if (!billDeskChecksumUtil.compareResponseHash(fields, responseString)) {
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SIGNATURE_MISMATCH.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.FAILED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SIGNATURE_MISMATCH.getResponseCode());
			return;
		}
		StatusType status = getSaleStatus(billDeskResponse[14]);
		ErrorType errorType = getSaleResponse(billDeskResponse[14]);
		fields.put(FieldType.PG_DATE_TIME.getName(), billDeskResponse[13]);
		fields.put(FieldType.MERCHANT_ID.getName(), billDeskResponse[0]);
		fields.put(FieldType.PG_GATEWAY.getName(), billDeskResponse[0]);
		fields.put(FieldType.TXN_ID.getName(), billDeskResponse[1]);
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.ACQ_ID.getName(), billDeskResponse[3]);
		fields.put(FieldType.PG_REF_NUM.getName(), billDeskResponse[2]);
		fields.put(FieldType.PG_RESP_CODE.getName(), billDeskResponse[23]);
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), billDeskResponse[24]);
		fields.put(FieldType.AUTH_CODE.getName(), billDeskResponse[6]);

		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());

	}

	// Status Transaction
	public void updateBillDeskStatusResponse(Fields fields) throws SystemException {

		BillDeskChecksumUtil billDeskChecksumUtil = new BillDeskChecksumUtil();

		if (!billDeskChecksumUtil.compareResponseStatusHash(fields, responseString)) {
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SIGNATURE_MISMATCH.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.FAILED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SIGNATURE_MISMATCH.getResponseCode());
			return;
		}

		StatusType status = getStatus(billDeskResponse[31], billDeskResponse[15]);
		ErrorType errorType = getResponse(billDeskResponse[31], billDeskResponse[15]);
		String amount = billDeskResponse[5];
		String refundAmount = billDeskResponse[28];

		fields.put(FieldType.PG_GATEWAY.getName(), billDeskResponse[1]);
		fields.put(FieldType.MERCHANT_ID.getName(), billDeskResponse[1]);
		fields.put(FieldType.TXN_ID.getName(), billDeskResponse[2]);
		fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), billDeskResponse[2]);

		fields.put(FieldType.ACQ_ID.getName(), billDeskResponse[4]);
		fields.put(FieldType.PG_REF_NUM.getName(), billDeskResponse[3]);
		if (!amount.equals("NA")) {
			fields.put(FieldType.AMOUNT.getName(),
					Amount.formatAmount(billDeskResponse[5], fields.get(FieldType.CURRENCY_CODE.getName())));
		}
		fields.put(FieldType.PG_DATE_TIME.getName(), billDeskResponse[14]);
		fields.put(FieldType.AUTH_CODE.getName(), billDeskResponse[15]);
		fields.put(FieldType.PG_RESP_CODE.getName(), billDeskResponse[24]);
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), billDeskResponse[25]);
		// Refund Transaction Status returned in Status Query
		fields.put(FieldType.PG_TXN_STATUS.getName(), billDeskResponse[27]);
		if (!refundAmount.equals("NA")) {
			fields.put(FieldType.REFUNDAMOUNT.getName(),
					Amount.formatAmount(billDeskResponse[28], fields.get(FieldType.CURRENCY_CODE.getName())));
		}

		fields.put(FieldType.REFUND_DATE_TIME.getName(), billDeskResponse[29]);
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());

	}

	// Refund transaction
	public void updateBillDeskRefundResponse(Fields fields) throws SystemException {

		BillDeskChecksumUtil billDeskChecksumUtil = new BillDeskChecksumUtil();

		if (!billDeskChecksumUtil.compareResponseRefundHash(fields, responseString)) {
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SIGNATURE_MISMATCH.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.FAILED.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SIGNATURE_MISMATCH.getResponseCode());
			return;
		}
		// [0410, BP8SUBZI, QIOB7825136795, NA, 1908021235161002, NA, NA, NA, NA, NA,
		// ERR_REF011, Invalid source, N,
		// E16A8E5F6594BF5BB8C41BDBD4FE1A6C991D21393F35091903EF1F57E55ADC94]
		StatusType status = getRefundEnquriyStatus(billDeskResponse[12]);
		ErrorType errorType = getRefundResponse(billDeskResponse[12]);

		fields.put(FieldType.PG_GATEWAY.getName(), billDeskResponse[1]);
		fields.put(FieldType.MERCHANT_ID.getName(), billDeskResponse[1]);
		fields.put(FieldType.PG_DATE_TIME.getName(), billDeskResponse[3]);
		fields.put(FieldType.REFUND_DATE_TIME.getName(), billDeskResponse[7]);
		// fields.put(FieldType.TXN_ID.getName(), billDeskResponse[4]);
		String amount = (billDeskResponse[6]).toString();
		if (amount.equals("NA")) {
		} else {
			fields.put(FieldType.AMOUNT.getName(),
					Amount.formatAmount(billDeskResponse[6], fields.get(FieldType.CURRENCY_CODE.getName())));
			fields.put(FieldType.REFUNDAMOUNT.getName(),
					Amount.formatAmount(billDeskResponse[6], fields.get(FieldType.CURRENCY_CODE.getName())));
		}
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.ACQ_ID.getName(), billDeskResponse[9]);
		fields.put(FieldType.PG_REF_NUM.getName(), billDeskResponse[2]);
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), billDeskResponse[11]);
		fields.put(FieldType.PG_RESP_CODE.getName(), billDeskResponse[10]);
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
	}
}
