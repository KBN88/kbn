package com.kbn.jmsscheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.StatusEnquiryParameters;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.emailer.Emailer;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class JmsQueueClient {
	private int count;
	private static void log(String message) {
		System.out.println(message);
		//Logger.getLogger(JmsQueueClient.class.getName()).debug(message);
	}

	//This method will be used in case of Reports only. String has been replaced by Object.
	// This method is dependant on Preport Processing
	public void fetchMsgFromJmsAndSendEmail(String mailTo,String mailBcc) throws IOException,Exception {

		mailBcc = null;
		String subject=null;
		String mailBody=null;
		String [] emailArr=null;
		
		PropertiesManager propertiesManager = new PropertiesManager();
		String url = propertiesManager.getSystemProperty("GetUrl");
		
		for (;;) {
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(url);
			CloseableHttpResponse httpResponse = httpClient.execute(httpGet);

			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + httpResponse.getStatusLine().getStatusCode());
			}

			BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
			String email = reader.readLine();

			if (StringUtils.isEmpty(email)) {
				log("::: No Message on JMS Queue :::");
				break;
			}

			while (email != null) {
				log("emailBody::: " + email);
				//emailArr=email.split(Constant.EMAIL_SEPERATOR);
				
				if(emailArr==null || emailArr.length==0){
					throw new SystemException(ErrorType.JMS_EMAIL_ERROR, "Email Array Is Empty");//TO-DO customize the exception
				}
				
				for(int i=0; i<emailArr.length; i++){
					switch(i) {
					   case 0 :
						   subject=emailArr[0];
					      break;
					   case 1 :
						   mailBody=emailArr[1];
					      break;
					}
				}
				if(StringUtils.isBlank(subject) || StringUtils.isEmpty(mailBody))
					throw new SystemException(ErrorType.EMAIL_ERROR,"Subject or Body is blank or null");//TO-DO Exception need to be customized
				
				Emailer.sendEmail(mailBody, subject, mailTo, mailBcc, true);
				 ++count;
				 log("Total Count of Msg Picked from JMS = "+count);
				 email = null;
			}
			reader.close();
			httpClient.close();
		}
		count=0;
	}
	
	
	/* In case of Schedular Email Id will be defined in Properties file
	 * We will have to pick Data from Database
	 * 
	 * */ 
	
	public static void postReportsToJms(String subject,String body,String toEmail,String bccEmail) 
			throws IOException,Exception {
		
		PropertiesManager propertiesManager = new PropertiesManager();
		String url = propertiesManager.getSystemProperty("PostUrlForScheduledMsg");
		
		HttpPost httpPost=null; 
		httpPost = new HttpPost(url);
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		httpPost.addHeader("subject", subject);
		httpPost.addHeader("body", body);
		
		CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
		if (httpResponse.getStatusLine().getStatusCode() != 200) {
			throw new SystemException(ErrorType.JMS_EMAIL_ERROR, "Response Code : "+httpResponse.getStatusLine().getStatusCode());
		}else{
			log("Message Sent on Jms Queue successfully");
		}
		httpClient.close();

	}
	
	public static void postMsgToJms(String toEmail,String bccEmail,String body,String subject,Boolean exceptionHandlerFlag) 
			throws IOException,Exception {
		
		PropertiesManager propertiesManager = new PropertiesManager();
		String url = propertiesManager.getSystemProperty("PostUrlForTransactionalMsg");
		String isExceptionHandlerFlag=exceptionHandlerFlag.toString();

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("toEmail", toEmail));
		urlParameters.add(new BasicNameValuePair("bccEmail", bccEmail));
		urlParameters.add(new BasicNameValuePair("body", body));
		urlParameters.add(new BasicNameValuePair("subject", subject));
		urlParameters.add(new BasicNameValuePair("isExceptionHandlerFlag", isExceptionHandlerFlag));

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse httpResponse = client.execute(post);
		if (httpResponse.getStatusLine().getStatusCode() != 200) 
			throw new SystemException(ErrorType.JMS_EMAIL_ERROR, "Response Code : "+httpResponse.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
		        new InputStreamReader(httpResponse.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		log(result.toString());
	}
	
	
	public static void postSmsToJms(String mobile,String message) 
			throws IOException,Exception {
		
		PropertiesManager propertiesManager = new PropertiesManager();
		String url = propertiesManager.getSystemProperty("PostUrlForSms");
		
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("mobile", mobile));
		urlParameters.add(new BasicNameValuePair("message", message));

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse httpResponse = client.execute(post);
		if (httpResponse.getStatusLine().getStatusCode() != 200) 
			throw new SystemException(ErrorType.JMS_EMAIL_ERROR, "Response Code : "+httpResponse.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
		        new InputStreamReader(httpResponse.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		log(result.toString());
	}

	
	public static void postSummaryReportUpdateToJms(StatusEnquiryParameters statusEnquiryParameters) throws IOException,Exception {

		try {
			
			PropertiesManager propertiesManager = new PropertiesManager();
			String url = propertiesManager.getSystemProperty("PostUrlForSummaryReportStatusUpdate");
			
				Client client = Client.create();
				WebResource webResource = client.resource(url);
				
				String inputParams = "{"
						+ "\"acqId\":\""+statusEnquiryParameters.getAcqId()+"\","
						+" \"acquirerType\":\""+statusEnquiryParameters.getAcquirerType()+"\","
						+"\"authCode\":\""+statusEnquiryParameters.getAuthCode()+"\","
						+"\"currencyCode\":\""+statusEnquiryParameters.getCurrencyCode()+"\","
						+"\"orderId\":\""+statusEnquiryParameters.getOrderId()+"\","
						+"\"origTxnId\":\""+statusEnquiryParameters.getOrigTxnId()+"\","
						+"\"payId\":\""+statusEnquiryParameters.getPayId()+"\","
						+"\"pgDateTime\":\""+statusEnquiryParameters.getPgDateTime()+"\","
						+"\"pgRefNum\":\""+statusEnquiryParameters.getPgRefNum()+"\","
						+"\"pgRespCode\":\""+statusEnquiryParameters.getPgRespCode()+"\","
						+"\"pgTxnMessage\":\""+statusEnquiryParameters.getPgTxnMessage()+"\","
						+"\"responseCode\":\""+statusEnquiryParameters.getResponseCode()+"\","
						+"\"responseMessage\":\""+statusEnquiryParameters.getResponseMessage()+"\","
						+"\"rfu1\":\""+statusEnquiryParameters.getRfu1()+"\","
						+"\"rfu2\":\""+statusEnquiryParameters.getRfu2()+"\","
						+"\"rrn\":\""+statusEnquiryParameters.getRrn()+"\","
						+"\"status\":\""+statusEnquiryParameters.getStatus()+"\","
						+"\"txnId\":\""+statusEnquiryParameters.getTxnId()+"\","
						+"\"txnType\":\""+statusEnquiryParameters.getTxnType()+"\","
						+"\"amount\":\""+statusEnquiryParameters.getAmount()+
						"\"}";
				
				System.out.println(inputParams);

				ClientResponse response = webResource.type("application/json")
				   .post(ClientResponse.class, inputParams);

				if (response.getStatus() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
					     + response.getStatus());
				}

				System.out.println("Output from Server .... \n");
				String output = response.getEntity(String.class);
				System.out.println(output);

			  } catch (Exception e) {

				e.printStackTrace();

			  }
}


}
	