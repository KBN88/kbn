package com.kbn.sms;

import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.kbn.commons.user.Invoice;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

/**
 * @author SunilYadav Description - SMS API
 */

public class SmsSender {

	private static Logger logger = Logger.getLogger(SmsSender.class.getName());

	// Dynamic Message Format
	public static void sendSMS(Fields fields) {
		try {
			User user = new UserDao().getUserClass(fields.get(FieldType.PAY_ID
					.getName()));
			if (user.isTransactionSmsFlag() == true) {
				UrlCreater urlCreater = new UrlCreater();
				// Bind on the basis of Merchant Contact Number
				String requestUrl = urlCreater.createURL(fields, user);

				URL url = new URL(requestUrl.toString());
				HttpURLConnection urlconnection = (HttpURLConnection) url
						.openConnection();

				if (urlconnection.getResponseMessage().equals("OK")) {
					MDC.put(Constants.CRM_LOG_USER_PREFIX.getValue(),
							user.getEmailId());
					logger.info("Transaction sms sent for TXN_ID:"
							+ fields.get(FieldType.TXN_ID.getName()));
				} else {
					MDC.put(Constants.CRM_LOG_USER_PREFIX.getValue(),
							user.getEmailId());
					logger.info("Error sending SMS for TXN_ID:"
							+ fields.get(FieldType.TXN_ID.getName()));
				}
				urlconnection.disconnect();
			}

		} catch (Exception exception) {
			logger.error(
					"Error sending SMS for TXN_ID:"
							+ fields.get(FieldType.TXN_ID.getName()), exception);
		}
	}

	public void sendPromoSMS(Invoice invoice, String url) {
		try {

			UrlCreater urlCreater = new UrlCreater();
			// Bind on the basis of Merchant Contact Number
			String requestUrl = urlCreater.createPromoURL(invoice, url);

			URL msgUrl = new URL(requestUrl.toString());
			HttpURLConnection urlconnection = (HttpURLConnection) msgUrl
					.openConnection();

			if (urlconnection.getResponseMessage().equals("OK")) {

				logger.info("Invoice sms sent:" + invoice.getInvoiceId());
			} else {

				logger.info("Error sending SMS for Invoice:"
						+ invoice.getInvoiceId());
			}
			urlconnection.disconnect();
		} catch (Exception exception) {
			logger.error("Error sending SMS for Invoice:"
					+ invoice.getInvoiceId());
		}
	}
}
