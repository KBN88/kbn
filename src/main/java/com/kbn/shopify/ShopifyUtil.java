/**
 * 
 */
package com.kbn.shopify;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.SystemConstants;

/**
 * @author Sunil
 * 
 */
public class ShopifyUtil {
	private static final String HMAC_SHA2_ALGORITHM = "HmacSHA256";
	private static Logger logger = Logger.getLogger(ShopifyUtil.class
			.getName());
	private static DateFormat inputDateFormatter = new SimpleDateFormat(CrmFieldConstants.OUTPUT_DATE_FORMAT_DB.getValue());
	private static DateFormat shopifyDateFormatter = new SimpleDateFormat(Constants.SHOPIFY_DATE_FORMAT);

	public static String calculatehash(String inputString, Fields fields) throws SystemException {

	//	String salt = (new PropertiesManager()).getSalt(fields.get(FieldType.PAY_ID.getName()));
		String result = calculatehash(inputString,fields.get(FieldType.PAY_ID.getName()));
		return result;
	}

	public static String calculatehash(String inputString, String payId) throws SystemException{
		String salt = (new PropertiesManager()).getSalt(payId);
	
		if (null == salt) {			  
			throw new SystemException(ErrorType.INVALID_PAYID_ATTEMPT,
					"Invalid " + FieldType.HASH.getName());
		}
		String result = "";
		try {
			/*byte[] keyBytes = Hex.decodeHex(salt.toCharArray());

			SecretKeySpec signingKey = new SecretKeySpec(keyBytes,HMAC_SHA2_ALGORITHM);
			Mac mac = Mac.getInstance(HMAC_SHA2_ALGORITHM);
			mac.init(signingKey);

			byte[] rawHmac = mac.doFinal(inputString.getBytes(Charset
					.forName(SystemConstants.DEFAULT_ENCODING_UTF_8)));
			byte[] hexBytes = new Hex().encode(rawHmac);

			result = new String(hexBytes, SystemConstants.DEFAULT_ENCODING_UTF_8);*/
			
			byte[] keyBytes = salt.getBytes(SystemConstants.DEFAULT_ENCODING_UTF_8);
			
			byte[] dataBytes = inputString.getBytes(SystemConstants.DEFAULT_ENCODING_UTF_8);
			
			SecretKeySpec signingKey = new SecretKeySpec(keyBytes,HMAC_SHA2_ALGORITHM);
			Mac mac = Mac.getInstance(HMAC_SHA2_ALGORITHM);
			mac.init(signingKey);

			byte[] rawHmac = mac.doFinal(dataBytes);
			byte[] hexBytes = new Hex().encode(rawHmac);
			
			result = new String(hexBytes, SystemConstants.DEFAULT_ENCODING_UTF_8);
			
		} catch (NoSuchAlgorithmException noSuchAlgorithmException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					noSuchAlgorithmException, "No such Hashing algoritham with Shopify");
		} catch (InvalidKeyException invalidKeyException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					invalidKeyException, "No such key with amex");
		} catch (UnsupportedEncodingException unsupportedEncodingException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					unsupportedEncodingException, "No such encoder with Shopify");
		}
		return result;
	}
	
	public static void matchHash(Map<String, String> requestMap) throws SystemException{
		Map<String, String> treeMap = new TreeMap<String, String>(requestMap);
		// Matching the hash string
		String signature = null;
		StringBuilder allFields = new StringBuilder();
			for (String key : treeMap.keySet()) {
				if (!(key.equals("x_signature"))){
					allFields.append(key);
					allFields.append(treeMap.get(key));
				}else if(key.equals("x_signature")){
					signature = treeMap.get(key);
				}
			}

			String calculatedSignature = calculatehash(allFields.toString(), treeMap.get(ShopifyFieldType.PAY_ID.getRequestName()));
			
			logger.info("recieved signature " + signature);
			logger.info("recieved signature string " + allFields.toString());
			logger.info("calculated Signature " + calculatedSignature);
			
			if (calculatedSignature.equals(signature)){
			//	fields.put(FieldType.HASH.getName(),Hasher.getHash(fields));
			}else{
				throw new SystemException(ErrorType.VALIDATION_FAILED, "Invalid "
					+ FieldType.HASH.getName());
			}
	}

	public static void putHash(Map<String,String> fields){
		String signature = null;
		Map<String, String> treeMap = new TreeMap<String, String>(fields);

		StringBuilder allFields = new StringBuilder();

			for (String key : treeMap.keySet()) {										
          				 allFields.append(key);
						 allFields.append(treeMap.get(key));
				   }

			logger.info("Response string" + allFields.toString());
		try {
			signature = calculatehash(allFields.toString(), fields.get(ShopifyFieldType.PAY_ID.getRequestName()));
		} catch (SystemException systemException) {			
			logger.error("Error preparing response hash" + systemException);
			fields.put(ShopifyFieldType.STATUS.getRequestName(),ShopifyResponseType.FAILED.getName());
		}
		logger.info("final response signature" + signature);
		fields.put(ShopifyFieldType.HASH.getRequestName(),signature);		
	}

	public static String formatDate(String date){		
		try{
			Date inputDate = inputDateFormatter.parse(date);
			return shopifyDateFormatter.format(inputDate);
		}catch(Exception exception){
			logger.error("Error formating date for shopify response", exception);
			return shopifyDateFormatter.format(new Date());
		}
	}
}
