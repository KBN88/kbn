package com.kbn.shopify;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

public class Mapper {

	public static String mapResponse(Fields fields){
		String response="";
		if(fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())){
			response=ShopifyResponseType.COMPLETED.getName();
		}else{
			response=ShopifyResponseType.FAILED.getName();
		}
		return response;
	}
}
