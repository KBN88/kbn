package com.kbn.shopify;

/**
 * @author Sunil
 *
 */
public class Constants {	
	 public static final String SHOPIFY_DATE_FORMAT ="yyyy-MM-dd'T'HH:mm:ssZ";
	 public static final String RESPONSE__RESPONSE_FIELDS = "ShopifyResponseFields";
	 public static final String EQUATOR = "=";
	 public static final String SEPRATOR = "&";
	 public static final String QUESTION_MARK = "?";
}
