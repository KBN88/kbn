package com.kbn.shopify;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.util.Fields;
import com.kbn.crm.actionBeans.SessionCleaner;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.action.RequestAction;
import com.opensymphony.xwork2.Action;

public class ShopifyRequestActon extends AbstractSecureAction implements ServletRequestAware{

	private static final long serialVersionUID = -6629146444578205862L;

	private static Logger logger = Logger.getLogger(RequestAction.class.getName());

	private HttpServletRequest request;
	@SuppressWarnings("unchecked")
	public String execute(){
		try {

			Map<String, Object> fieldMapObj = request.getParameterMap();
			Map<String, String> requestMap = new HashMap<String, String>();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					requestMap.put(entry.getKey(),((String[]) entry.getValue())[0]);

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			requestMap.remove("button");
			mapper(requestMap);
			
		}catch (Exception exception) {
			SessionCleaner.cleanSession(sessionMap);
			logger.error("Exception", exception);
			return Action.ERROR;
		}
		return SUCCESS;
	}
	
	public Fields mapper(Map<String, String> requestMap){
		Map<String, String> responseMap = new HashMap<String, String>();
		
		for (Entry<String, String> entry : requestMap.entrySet()) {
			ShopifyFieldType shopifyFieldType = ShopifyFieldType.getInstance(entry.getKey());
			responseMap.put(shopifyFieldType.getPgName(), entry.getValue());
		}
		Fields fields = new Fields(responseMap);
		
		return fields;
	}
	public void setServletRequest(HttpServletRequest request) {
		this.setRequest(request);
	}
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
}