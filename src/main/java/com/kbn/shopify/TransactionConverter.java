package com.kbn.shopify;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.Helper;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Currency;

public class TransactionConverter {

	public static Map<String, String> convertResponse(Fields fields) {
		Map<String, String> responseMap = new HashMap<String, String>();

		for (String key : fields.keySet()) {
			ShopifyFieldType shopifyFieldType = ShopifyFieldType
					.getInstancePgName(key);
			if(null == shopifyFieldType){
				continue;
			}
			String value = fields.get(key);
			responseMap.put(shopifyFieldType.getRequestName(), value);
		}
		return responseMap;
	}
	
	public Map<String,String> prepareResponse(Fields fields){

		String responseFields = new PropertiesManager().getSystemProperty(Constants.RESPONSE__RESPONSE_FIELDS);
		List<String> responseFieldNameList = (List<String>) Helper.parseFields(responseFields);
		Map<String,String> responseMap = new HashMap<String,String>();

		for(String varName:responseFieldNameList){
			ShopifyFieldType shopifyFieldType = ShopifyFieldType.getInstancePgName(varName);			
			switch(shopifyFieldType){
			case ORDER_ID:
			case PAY_ID:
			case TXN_ID:
				responseMap.put(shopifyFieldType.getRequestName(), fields.get(varName));
				break;
				case AMOUNT:
				responseMap.put(shopifyFieldType.getRequestName(), Amount.toDecimal(fields.get(varName),fields.get(FieldType.CURRENCY_CODE.getName())));
				break;
			case CURRENCY_CODE:
				responseMap.put(shopifyFieldType.getRequestName(),Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName())));
				break;
			case RESPONSE_DATE_TIME:
				responseMap.put(shopifyFieldType.getRequestName(),ShopifyUtil.formatDate(fields.get(FieldType.RESPONSE_DATE_TIME.getName())));
				break;
			case STATUS:
				responseMap.put(shopifyFieldType.getRequestName(),Mapper.mapResponse(fields));
				break;
			case TEST:
				responseMap.put(shopifyFieldType.getRequestName(),"false");
				break;
			default:
				break;
			}
		}

		ShopifyUtil.putHash(responseMap);
        return responseMap;
	}
}
