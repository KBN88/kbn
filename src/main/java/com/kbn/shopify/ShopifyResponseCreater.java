package com.kbn.shopify;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.SystemConstants;

/**
 * @author Puneet
 *
 */
public class ShopifyResponseCreater {
	private static Logger logger = Logger
			.getLogger(ShopifyResponseCreater.class.getName());

	public void ResponsePost(Fields fields, Map<String, String> responseMap) {

		try {
			PrintWriter out;

			out = ServletActionContext.getResponse().getWriter();

			StringBuilder url = new StringBuilder();
			url.append(fields.get(FieldType.RETURN_URL.getName()));
			url.append(Constants.QUESTION_MARK);
			for (String key : responseMap.keySet()) {
				url.append(key);
				url.append(Constants.EQUATOR);
				url.append(URLEncoder.encode(responseMap.get(key), SystemConstants.DEFAULT_ENCODING_UTF_8));
				url.append(Constants.SEPRATOR);
			}
			url.deleteCharAt(url.length() - 1);

			StringBuilder httpRequest = new StringBuilder();
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent() { ");
			httpRequest.append("window.location.assign('");
			httpRequest.append(url);
			httpRequest.append("') }");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");
			logger.info("final request sent " + httpRequest);
			out.write(httpRequest.toString());
		} catch (IOException ioException) {
			logger.error(ioException);
		}
	}
}
