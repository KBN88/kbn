package com.kbn.shopify;

public enum ShopifyResponseType {
	COMPLETED    ("completed"), 
	FAILED       ("failed"), 
	PENDING      ("pending");

	private final String name;

	private ShopifyResponseType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
