package com.kbn.shopify;

import com.kbn.commons.util.FieldType;

public enum ShopifyFieldType {

	PAY_ID						("x_account_id", FieldType.PAY_ID.getName()),
	CURRENCY_CODE 				("x_currency", FieldType.CURRENCY_CODE.getName()),
	AMOUNT						("x_amount", FieldType.AMOUNT.getName()),
	ORDER_ID					("x_reference", FieldType.ORDER_ID.getName()),
	RETURN_URL					("x_url_complete", FieldType.RETURN_URL.getName()),
	CANCEL_URL					("x_url_cancel", FieldType.CANCEL_URL.getName()),
	TXN_TYPE					("x_transaction_type", FieldType.TXNTYPE.getName()),
	PRODUCT_DESC				("x_description", FieldType.PRODUCT_DESC.getName()),
	CUST_FIRST_NAME				("x_customer_first_name", FieldType.CUST_FIRST_NAME.getName()),
	CUST_LAST_NAME				("x_customer_last_name", FieldType.CUST_LAST_NAME.getName()),
	CUST_EMAIL					("x_customer_email", FieldType.CUST_EMAIL.getName()),
	CUST_PHONE					("x_customer_phone", FieldType.CUST_PHONE.getName()),
	CUST_SHIP_CITY				("x_customer_shipping_city", FieldType.CUST_SHIP_CITY.getName()),
	CUST_SHIP_STREET_ADDRESS1	("x_customer_shipping_address1", FieldType.CUST_SHIP_STREET_ADDRESS1.getName()),
	CUST_SHIP_STREET_ADDRESS2	("x_customer_shipping_address2", FieldType.CUST_SHIP_STREET_ADDRESS2.getName()),
	CUST_SHIP_STATE				("x_customer_shipping_state", FieldType.CUST_SHIP_STATE.getName()),
	CUST_SHIP_ZIP				("x_customer_shipping_zip", FieldType.CUST_SHIP_ZIP.getName()),
	CUST_SHIP_COUNTRY			("x_customer_shipping_country", FieldType.CUST_SHIP_COUNTRY.getName()),
	CUST_SHIP_PHONE				("x_customer_shipping_phone", FieldType.CUST_SHIP_PHONE.getName()),
	STATUS						("x_result", FieldType.STATUS.getName()),
	HASH						("x_signature", FieldType.HASH.getName()),
	TXN_ID						("x_gateway_reference", FieldType.TXN_ID.getName()),
	RESPONSE_DATE_TIME			("x_timestamp", "RESPONSE_DATE_TIME" ),

	CALLBACK_URL				("x_url_callback", "CALLBACK_URL" ),
	SHIP_AMOUNT					("x_amount_shipping", "SHIP_AMOUNT" ),
	TAX_AMOUNT					("x_amount_tax", "TAX_AMOUNT" ),
	SHOP_COUNTRY 				("x_shop_country", "SHOP_COUNTRY" ),
	MERCHANT_NAME				("x_shop_name", "MERCHANT_NAME" ),
	INVOICE_ID					("x_invoice", "INVOICE_ID" ),
	TEST						("x_test", "TEST" ),
	SHIP_COMPANY				("x_customer_shipping_company", "SHIP_COMPANY" );

	private final String requestName;
	private final String pgName; 

	private ShopifyFieldType(String requestName,String pgName){
		this.requestName = requestName;
		this.pgName = pgName;
	}

	public String getRequestName() {
		return requestName;
	}

	public String getPgName() {
		return pgName;
	}

	public static ShopifyFieldType getInstance(String requestName){
		ShopifyFieldType[] shopifyFieldTypes = ShopifyFieldType.values();
		for(ShopifyFieldType shopifyFieldType : shopifyFieldTypes){
			if(shopifyFieldType.getRequestName().toString().equals(requestName)){
				return shopifyFieldType;
			}
		}
		return null;
	}
		public static ShopifyFieldType getInstancePgName(String pgName){
			ShopifyFieldType[] shopifyFieldTypes = ShopifyFieldType.values();
			for(ShopifyFieldType shopifyFieldType : shopifyFieldTypes){
				if(shopifyFieldType.getPgName().toString().equals(pgName)){
					return shopifyFieldType;
				}
			}
			return null;
	}
}
