package com.kbn.cybersource;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;

public class CyberSourceIntegrator {
	private TransactionConverter converter = new TransactionConverter();
	private TransactionCommunicator communicator = new TransactionCommunicator();
	private TransactionFactory TransactionFactory = new TransactionFactory();
	private CyberSourceTransformer cyberSourceTransformer = null;

	public void process(Fields fields) throws SystemException, SOAPException {

		send(fields);

		 //resend(fields);

		// cryptoManager.secure(fields);
	}// process

	public void resend(Fields fields) throws SystemException {

		// TODO: Put a merchant specific flag

		// If card was not enrolled, CyberSource suggests to perform authorization
		String txnType = fields.get(FieldType.TXNTYPE.getName());
		if (txnType.equals(TransactionType.ENROLL.getName())) {
			String status = fields.get(FieldType.STATUS.getName());
			if (null != status && status.equals(StatusType.PENDING.getName())) {
				fields.put(FieldType.TXNTYPE.getName(), fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()));

				// send(fields);

				// If transaction not authorized
				if (fields.get(FieldType.STATUS.getName()).equals(StatusType.PENDING.getName())) {
					fields.put(FieldType.STATUS.getName(), StatusType.DECLINED.getName());
					fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.DECLINED.getResponseMessage());
				} // if
			} // if
		} // if
	}

	public void send(Fields fields) throws SystemException, SOAPException {

		Transaction transactionRequest = new Transaction();
		Transaction transactionResponse = new Transaction();

		transactionRequest = TransactionFactory.getInstance(fields);

		SOAPMessage request = converter.perpareRequest(fields, transactionRequest);

		String response = communicator.sendSoapMessage(request, fields);

		transactionResponse = converter.toTransaction(response);

		cyberSourceTransformer = new CyberSourceTransformer(transactionResponse);
		cyberSourceTransformer.updateResponse(fields);
	}

	public TransactionConverter getConverter() {
		return converter;
	}

	public void setConverter(TransactionConverter converter) {
		this.converter = converter;
	}

	public TransactionCommunicator getCommunicator() {
		return communicator;
	}

	public void setCommunicator(TransactionCommunicator communicator) {
		this.communicator = communicator;
	}

}
