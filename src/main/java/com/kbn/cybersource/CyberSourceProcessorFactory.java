package com.kbn.cybersource;

import com.kbn.pg.core.Processor;

public class CyberSourceProcessorFactory {

	public static Processor getInstance() {
		return new CyberSourceProcessor();
	}
}
