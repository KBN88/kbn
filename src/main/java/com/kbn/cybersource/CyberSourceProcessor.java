package com.kbn.cybersource;
import javax.xml.soap.SOAPException;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Processor;

public class CyberSourceProcessor implements Processor {

	//private YesBankCbIntegrator yesBankCbIntegrator;

	 CyberSourceIntegrator cyberSourceIntegrator = new CyberSourceIntegrator();

	public void preProcess(Fields fields) throws SystemException {
	}

	public void process(Fields fields) throws SystemException {

		if ((fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.NEWORDER.getName())
				|| fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()).equals(TransactionType.STATUS.getName())
				|| fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()).equals(TransactionType.VERIFY.getName())
				|| fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()).equals(TransactionType.RECO.getName())
				|| fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()).equals(TransactionType.REFUNDRECO.getName()))) {
			// New Order Transactions are not processed by FSS
			return;
		}

		if (!fields.get(FieldType.ACQUIRER_TYPE.getName()).equals(AcquirerType.CYBER_SOURCE.getCode())) {
			return;
		}
		
		try {
			cyberSourceIntegrator.process(fields);
		} catch (SOAPException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
	/*	
	 //if the MOP Type is GPay
		String paymentType = fields.get(FieldType.PAYMENT_TYPE.getName());
		String mopType = fields.get(FieldType.MOP_TYPE.getName());
	  if (paymentType.equals(PaymentType.UPI.getCode()) && mopType.equals(MopType.GOOGLEPAY.getCode()) ) {
			return;
		}
		else if (paymentType.equals(PaymentType.UPI.getCode())) {
			yesBankCbIntegrator.process(fields);
		} else {
			cyberSourceIntegrator.process(fields);
		}*/
	}

	public void postProcess(Fields fields) throws SystemException {
	}

}

