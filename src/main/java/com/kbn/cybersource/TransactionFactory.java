package com.kbn.cybersource;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;

/**
 * @author neeraj
 *
 */
public class TransactionFactory {
	
	@SuppressWarnings("incomplete-switch")
	public Transaction getInstance(Fields fields) throws SystemException {

		Transaction transaction = new Transaction();
		switch (TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))) {
		case AUTHORISE:
			break;
		case ENROLL:
			transaction.setCardDetails(fields);
			fields.put(FieldType.PG_REF_NUM.getName(), fields.get(FieldType.TXN_ID.getName()));
			fields.put(FieldType.ORIG_TXN_ID.getName(), fields.get(FieldType.TXN_ID.getName()));
			break;
		case REFUND:
			//transaction.setRefund(fields);
			fields.put(FieldType.PG_REF_NUM.getName(), fields.get(FieldType.TXN_ID.getName()));
			//fields.put(FieldType.TOTAL_AMOUNT.getName(), fields.get(FieldType.AMOUNT.getName()));
			break;
		case SALE:
			// Authorization and Sale messaging format is same, just action code
			// changes
			//transaction.setAuthorization(fields);
			transaction.setCardDetails(fields);
			// if ORGIN_TXN_ID null
			cyberSourceTransactionFetchService cyberSourceTransactionFetchService = new cyberSourceTransactionFetchService();
			String orignTxn_Id = fields.get(FieldType.ORIG_TXN_ID.getName());
			if(null == orignTxn_Id){
			String txn_id	= fields.get(FieldType.INTERNAL_ORIG_TXN_ID.getName());
			String origtxnId =cyberSourceTransactionFetchService.fetchOorginTxnId(txn_id);
			 fields.put(FieldType.PG_REF_NUM.getName(),origtxnId);
			}else {
				fields.put(FieldType.PG_REF_NUM.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));
			}
			
			break;
		case CAPTURE:
			//transaction.setCapture(fields);
			fields.put(FieldType.PG_REF_NUM.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));
			break;
		case STATUS:
			//transaction.setStatusEnquiry(fields);
			break;
		}

		return transaction;
	}
}
