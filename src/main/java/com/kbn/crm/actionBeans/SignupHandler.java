package com.kbn.crm.actionBeans;

import com.kbn.crm.commons.action.AbstractSecureAction;

public class SignupHandler extends AbstractSecureAction {

	/**
	 * @ISHA
	 */
	private static final long serialVersionUID = -6486588768712818495L;
	private String amount ;
	private String orderId ;
	public String execute(){
		
		return SUCCESS;
		
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
