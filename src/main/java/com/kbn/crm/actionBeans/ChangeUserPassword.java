package com.kbn.crm.actionBeans;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserRecordsDao;
import com.kbn.crm.action.CheckOldPassword;
import com.kbn.emailer.EmailBuilder;

/**
 * @author ISHA
 *
 */
public class ChangeUserPassword {
		
	public ResponseObject changePassword(String emailId, String oldPassword, String newPassword) throws Exception{
		UserDao userDao = new UserDao();
		User user = new User();
		UserRecordsDao userRecordsDao = new UserRecordsDao();
		ResponseObject responseObject = new ResponseObject();
		
		user = userDao.find(emailId);
		
		oldPassword = (PasswordHasher.hashPassword(oldPassword, user.getPayId()));
		newPassword = (PasswordHasher.hashPassword(newPassword,user.getPayId()));
		if (!oldPassword.equals(user.getPassword())){
			responseObject.setResponseCode(ErrorType.PASSWORD_MISMATCH.getResponseCode());
			return responseObject;
		}else if(newPassword.equals(oldPassword)){                                           // Match if new and old password is same and password is correct
			responseObject.setResponseCode(ErrorType.OLD_PASSWORD_MATCH.getResponseCode());
			return responseObject;
		}
		
		
		if(new CheckOldPassword().isUsedPassword(newPassword, user.getEmailId())) {
			responseObject.setResponseCode(ErrorType.OLD_PASSWORD_MATCH.getResponseCode());
			return responseObject;
		}				
				
		userRecordsDao.createDetails(emailId, oldPassword, user.getPayId());
		user.setPassword(newPassword);
		userDao.update(user);
		responseObject.setResponseCode(ErrorType.PASSWORD_CHANGED.getResponseCode());
		
		// Sending Email for CRM password change notification
		EmailBuilder emailBuilder = new EmailBuilder();
		emailBuilder.emailPasswordChange(responseObject,emailId);
		
		return responseObject;
	}
	
}
