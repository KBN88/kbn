package com.kbn.crm.actionBeans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kbn.commons.dao.ServiceTaxDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.SurchargeDetails;
import com.kbn.commons.user.SurchargeDetailsDao;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class SurchargeDetailsFactory extends AbstractSecureAction {

	private static final long serialVersionUID = 2500609617979420055L;

	public Map<String, List<SurchargeDetails>> getSurchargeDetails(String payId, String paymentTypeName) {
		SurchargeDetails detail = null;

		detail = new SurchargeDetailsDao().findDetails(payId, paymentTypeName);
		BigDecimal serviceTax = new ServiceTaxDao().findServiceTaxByPayId(payId);
		if (serviceTax == null) {
			addActionMessage(ErrorType.PERMISSION_DENIED.getResponseMessage());
		}

		ArrayList<SurchargeDetails> details = new ArrayList<SurchargeDetails>();

		Map<String, List<SurchargeDetails>> detailsMap = new HashMap<String, List<SurchargeDetails>>();
		if (detail != null) {
			detail.setServiceTax(serviceTax);
			details.add(detail);
		} else {
			SurchargeDetails blankDetail = new SurchargeDetails();
			if (serviceTax != null) {
				blankDetail.setServiceTax(serviceTax);
			} else {
				blankDetail.setServiceTax(BigDecimal.ZERO);
			}
			blankDetail.setSurchargeAmount(BigDecimal.ZERO);
			blankDetail.setSurchargePercentage(BigDecimal.ZERO);
			details.add(blankDetail);

		}

		detailsMap.put(paymentTypeName, details);
		return detailsMap;
	}

}
