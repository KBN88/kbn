package com.kbn.crm.actionBeans;

import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.PropertiesManager;

/**
 * @author Puneet
 *
 */
public class PasswordHasher {

	public static String hashPassword(String password,String payId) throws SystemException{
	
		String salt = (new PropertiesManager()).getSalt(payId);	
		if(null==salt){
			throw new SystemException(ErrorType.AUTHENTICATION_UNAVAILABLE, ErrorType.AUTHENTICATION_UNAVAILABLE.getResponseCode());
		}
		
		String hashedPassword = Hasher.getHash(password.concat(salt));		
		return hashedPassword;		
	}		
}

	
