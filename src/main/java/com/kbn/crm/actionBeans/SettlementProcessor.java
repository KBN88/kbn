package com.kbn.crm.actionBeans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

import com.kbn.commons.user.Settlement;
import com.kbn.commons.user.SettlementDao;
import com.kbn.commons.util.CrmFieldConstants;

/**
 * @author Puneet
 *
 */

public class SettlementProcessor {
	private static Logger logger = Logger.getLogger(SettlementProcessor.class.getName());

	private SettlementDao settlementDao = new SettlementDao(); 
	public void processAll(List<Settlement> settlementList){
		for(Settlement settlement:settlementList){
			try{
				process(settlement);
			}catch(Exception exception){
				logger.error("Error inserting settlement with txnId: " + settlement.getTxnId() + exception);
			}
		}
	}

	public void process(Settlement settlement) throws ParseException{
			settlement.setTxnDate(formatDate(settlement.getTxnDate()));
			settlement.setStatus(CrmFieldConstants.SETTLEMENT_PROCESSED.getValue());
			settlementDao.create(settlement);
	}

	//TODO... move to date creater
	public static String formatDate(String txnDate) throws ParseException{		
		SimpleDateFormat sdf = new SimpleDateFormat(CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		Date date =null;
		date = sdf.parse(txnDate);
		sdf.applyPattern(CrmFieldConstants.OUTPUT_DATE_FORMAT_DB.getValue());
		txnDate = sdf.format(date);
		return txnDate;
	}
}
