package com.kbn.crm.actionBeans;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;

public class SessionUserIdentifier {

	public String getMerchantPayId(User user, String merchantEmailId) {
		UserDao userDao = new UserDao();

		if (user.getUserType().equals(UserType.ADMIN) || user.getUserType().equals(UserType.SUBADMIN)
				|| user.getUserType().equals(UserType.SUPERADMIN)
				|| user.getUserType().equals(UserType.RESELLER) || user.getUserType().equals(UserType.ACQUIRER)
				|| user.getUserType().equals(UserType.SUBACQUIRER)) {
			if ((null == merchantEmailId) || merchantEmailId.equals(CrmFieldConstants.ALL.toString())) {
				return CrmFieldConstants.ALL.toString();
			} else {
				User merchant = userDao.find(merchantEmailId);
				return merchant.getPayId();
			}
		} else if (user.getUserType().equals(UserType.SUBUSER)) {
			return user.getParentPayId();
		} else {
			return user.getPayId();
		}
	}

	// to use payId securely for further operations
	public static String getUserPayId(User user, String payId) {
		switch (user.getUserType()) {
		case ADMIN:
		case SUPERADMIN:
		case ACQUIRER:
		case RESELLER:
		case SUBACQUIRER:
			return payId;
		case SUBADMIN:
		case MERCHANT:
			return user.getPayId();
		case SUBUSER:
			return user.getParentPayId();
		}
		return null;
	}
}