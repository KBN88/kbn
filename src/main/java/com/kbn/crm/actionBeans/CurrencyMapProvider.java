package com.kbn.crm.actionBeans;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.action.IndexAction;
import com.kbn.pg.core.Currency;

/**
 * @author shashi
 *
 */
public class CurrencyMapProvider{

	private static Logger logger = Logger
			.getLogger(IndexAction.class.getName());

	public Map<String, String> currencyMap(User user) {
		
		Map<String, String> tempMap;
		String currencyKey = user.getDefaultCurrency();
		Map<String, String> currencyMap = new LinkedHashMap<String, String>();
		if (user.getUserType().equals(UserType.ADMIN)
				|| user.getUserType().equals(UserType.RESELLER) || user.getUserType().equals(UserType.ACQUIRER)
				|| user.getUserType().equals(UserType.SUPERADMIN)||user.getUserType().equals(UserType.SUBADMIN)) {
			tempMap = Currency.getAllCurrency();
			// set currencies
			String strKey = tempMap.get(CrmFieldConstants.INR.getValue());
			if (StringUtils.isBlank(currencyKey)) {
				if (!StringUtils.isBlank(strKey)) {
					tempMap.remove(CrmFieldConstants.INR.getValue());
					currencyMap.put(CrmFieldConstants.INR.getValue(), strKey);
					for (Entry<String, String> entry : tempMap.entrySet()) {
						try {
							currencyMap.put(entry.getKey(), entry.getValue());
						} catch (ClassCastException classCastException) {
							logger.error("Exception", classCastException);
						}
					}
					return currencyMap;
				}
			}
		}else if(user.getUserType().equals(UserType.SUBUSER) || user.getUserType().equals(UserType.SUBACQUIRER)){
		//	User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			String parentPayId = user.getParentPayId();
			UserDao userDao = new UserDao();
			User parentUser = (User) userDao.findPayId(parentPayId);
			tempMap = Currency.getSupportedCurreny(parentUser);
			if (StringUtils.isBlank(currencyKey)) {
				return tempMap;
			}
		} 
		else {
			tempMap = Currency.getSupportedCurreny(user);
			if (StringUtils.isBlank(currencyKey)) {
				return tempMap;
			}
		}
		tempMap.remove(currencyKey);
		currencyMap.put(currencyKey, Currency.getAlphabaticCode(currencyKey));
		for (Entry<String, String> entry : tempMap.entrySet()) {
			try {
				currencyMap.put(entry.getKey(), entry.getValue());
			} catch (ClassCastException classCastException) {
				logger.error("Exception", classCastException);
			}
		}

		return currencyMap;
	}
}
