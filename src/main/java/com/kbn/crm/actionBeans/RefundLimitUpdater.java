package com.kbn.crm.actionBeans;

import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.FieldType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.Amount;

/**
 * @author Shashi
 *
 */
public class RefundLimitUpdater extends AbstractSecureAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6077024655866344650L;
	private static Logger logger = Logger.getLogger(RefundLimitUpdater.class.getName());
	public void extraRefundLimitUpdate(String currencyCode,
			Map<String, String> requestMap, User user, String origTxnId,
			String payId, float todayRefundedAmount,
			float todayTotalCapturedAmount) {
		float refundingAmount = 0;
		float newExtraRefundLimitUpdate = 0;
		float extraRefundLimit = 0;
		String refundingAmountDecimal;
		String refundingAmountRM = requestMap.get(FieldType.AMOUNT.getName());
		UserDao userDao = new UserDao();			
		User user_ = userDao.find(user.getEmailId());
		try{
		refundingAmountDecimal = Amount.toDecimal(refundingAmountRM,
				currencyCode);
		refundingAmount = Float.parseFloat(refundingAmountDecimal); // current refunding Amount
		extraRefundLimit = (user.getExtraRefundLimit()); // User add amount externally
	
			float refundedAmount = (todayRefundedAmount-todayTotalCapturedAmount);
			float totalRefundedLimit = refundedAmount+extraRefundLimit;
			float totalRefundableAmount = (todayTotalCapturedAmount + totalRefundedLimit)
					- todayRefundedAmount;
			if (refundingAmount <= totalRefundableAmount) {
			newExtraRefundLimitUpdate = totalRefundableAmount - refundingAmount;
			//in case any attribute of user is updated by another resource
			user_.setExtraRefundLimit(newExtraRefundLimitUpdate);
			userDao.update(user_);
		} else {
			if(refundingAmount  > (todayTotalCapturedAmount-todayRefundedAmount)){
				float deductFromExtraLimit = refundingAmount-(todayTotalCapturedAmount-todayRefundedAmount);
				if(deductFromExtraLimit <= extraRefundLimit){
					newExtraRefundLimitUpdate = extraRefundLimit - deductFromExtraLimit;
					user_.setExtraRefundLimit(newExtraRefundLimitUpdate);
					userDao.update(user_);
				} 
		}
	}
		}catch (Exception exception) {
			logger.error("Exception", exception);	
			}
}
}