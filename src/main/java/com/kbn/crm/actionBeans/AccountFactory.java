package com.kbn.crm.actionBeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.Mop;
import com.kbn.commons.user.MopTransaction;
import com.kbn.commons.user.Payment;
import com.kbn.commons.user.User;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.commons.util.TransactionType;

/**
 * @author Puneet
 *
 */
public class AccountFactory {

	private ChargingDetailsMaintainer maintainChargingDetails = new ChargingDetailsMaintainer();
	private Account account = new Account();
	private Set<Payment> savedPaymentSet = new HashSet<Payment>();
	private Payment presentPayment = null;
	private Mop presentMop = null;
	private MopTransaction presentMoptxn = null;

	public AccountFactory(){

	}

	public Account editAccount(Account oldAccount,String mapString,String payId){
		this.account = oldAccount;
		String acquirerName = oldAccount.getAcquirerName();
		List<String> mappedCurrency = getCurrencyList();
		savedPaymentSet = account.getPayments();
		if(mapString.equals("")){
			String[] dummytokens = {""};
			removeMapping(dummytokens,acquirerName,payId,account.getId());
			return account;
		}
		String[] tokens = mapString.split(",");
		//remove old mappings
		removeMapping(tokens,acquirerName,payId,account.getId());
		// new mappings added
		addMapping(tokens,acquirerName,payId,mappedCurrency);
		return account;
	}

	public void removeMapping(String[] tokens,String acquirer,String payId,Long accountId){
		Set<Payment> paymentSet = account.getPayments();
		String[] savedTokens = account.getMappedString().split(CrmFieldConstants.COMMA.getValue());

		for(String savedToken:savedTokens){
			boolean isPresent = false;
			for(String token:tokens){
					if(token.equals(savedToken)){
						isPresent = true;
					}
				}
			//disable charging detail
			if(!isPresent){
				removeToken(paymentSet,savedToken);
				account.disableChargingDetail(savedToken);
			}
		}
	}

	public void removeToken(Set<Payment> paymentSet, String savedToken){
		String[] splittedToken = savedToken.split("-");

		if(checkPaymentType(paymentSet, savedToken)){
			if(checkMopType(savedToken)){
				if(splittedToken.length==3){
				   if(checkTxnType(savedToken)){
						presentMop.removeTransactionType(presentMoptxn);
						if(presentMop.getMopTransactionTypes().isEmpty()){
								presentPayment.removeMop(presentMop);
							}
					}
				}else{
					// IN case of WL and NB
					presentPayment.removeMop(presentMop);
				}
			}
			if(presentPayment.getMops().isEmpty()){
				account.removePayment(presentPayment);
			}
		}
	}

	public void addMapping(String[] tokens,String acquirerName,String payId, List<String> mappedCurrency){
		for(String token:tokens){
			String[] splitedToken = token.split("-");
		    if(!checkPaymentType(savedPaymentSet,token)){

		    	Payment payment = new Payment();
		    	payment.setPaymentType(PaymentType.getInstance(splitedToken[0]));

		    	Mop newMop = new Mop();
		    	newMop.setMopType(MopType.getmop(splitedToken[1]));

		    	payment.addMop(newMop);

		    	if(splitedToken.length==3){
		    	MopTransaction newMopTxn = new MopTransaction();
		    	newMopTxn.setTransactionType(TransactionType.getInstanceFromCode(splitedToken[2]));
		    	newMop.addMopTransaction(newMopTxn);
		    	}
		    	account.addPayment(payment);
		    	addChargingDetail(acquirerName, payId, token,mappedCurrency);
		    	continue;
		    }
		    //if mop not present
		    if(!checkMopType(token)){

		    	Mop newMop = new Mop();
		    	if(splitedToken.length==3){
		    	MopTransaction newMopTxn = new MopTransaction();
		    	newMopTxn.setTransactionType(TransactionType.getInstanceFromCode(splitedToken[2]));
		    	newMop.addMopTransaction(newMopTxn);
		    	}

		    	newMop.setMopType(MopType.getmop(splitedToken[1]));
		    	presentPayment.addMop(newMop);
		    	addChargingDetail(acquirerName, payId, token,mappedCurrency);
		    	continue;
		    }

		    if(!checkTxnType(token)){   //if txntype not present
		    	if(splitedToken.length==2){
		    		continue;
		    	}
		    	MopTransaction newMopTxn = new MopTransaction();
		    	newMopTxn.setTransactionType(TransactionType.getInstanceFromCode(splitedToken[2]));
		    	presentMop.addMopTransaction(newMopTxn);
		    	addChargingDetail(acquirerName, payId, token,mappedCurrency);
		    }
	    }
	}

	public void addChargingDetail(String acquirerName,String payId,String token,List<String> selectedCurrency){
		for(String currencyCode:selectedCurrency){
    		account.addChargingDetail(maintainChargingDetails.createChargingDetail(acquirerName, payId, token,currencyCode));
    	}
	}

	public boolean checkPaymentType(Set<Payment> paymentSet,String token){
		boolean isPresent = false;

		Iterator<Payment> paymentItr = paymentSet.iterator();
		String[] splittedToken = token.split("-");
		while(paymentItr.hasNext()){
			Payment currentPayment = paymentItr.next();
			if(currentPayment.getPaymentType().getName().equals(splittedToken[0])){
				isPresent = true;
				this.presentPayment = currentPayment;
			}
		}
		return isPresent;
	}

	public boolean checkMopType(String token){
		boolean isPresent = false;
		String[] splittedToken = token.split("-");
		Set<Mop> presentMopSet = presentPayment.getMops();
		Iterator<Mop> mopItr = presentMopSet.iterator();

		while(mopItr.hasNext()){
			Mop currentMop = mopItr.next();
			if(currentMop.getMopType().getCode().equals(splittedToken[1])){
				isPresent = true;
				this.presentMop = currentMop;
			}
		}
		return isPresent;
	}

	public boolean checkTxnType(String token){
		boolean isPresent = false;
		String[] splittedToken = token.split("-");

		Set<MopTransaction> presentMopTxnSet = presentMop.getMopTransactionTypes();
		Iterator<MopTransaction> mopTxnItr = presentMopTxnSet.iterator();

		while(mopTxnItr.hasNext()){
			MopTransaction currentMopTxn = mopTxnItr.next();
			if(currentMopTxn.getTransactionType().getCode().equals(splittedToken[2])){
				isPresent = true;
				this.presentMoptxn = currentMopTxn;
			}
		}
		return isPresent;
	}

	public Account addAccountCurrency(Account account, AccountCurrency[] selectedAccountCurrency,User acquirer,String payId){
		if(null == selectedAccountCurrency || selectedAccountCurrency.length==0){
			return account;
		}
		Set<AccountCurrency> accountCurrencySet = account.getAccountCurrencySet();
		for(AccountCurrency accountCurrencyFE:selectedAccountCurrency){
			boolean flag = false;
			Iterator<AccountCurrency> accountCurrencySetItrator = account.getAccountCurrencySet().iterator();
			while(accountCurrencySetItrator.hasNext()){
				AccountCurrency accountCurrency = accountCurrencySetItrator.next();
				if(accountCurrency.getCurrencyCode().equals(accountCurrencyFE.getCurrencyCode())){
					flag=true;
					//edit password and other details
					accountCurrency.setMerchantId(accountCurrencyFE.getMerchantId());
					accountCurrency.setPassword(AccountPasswordScrambler.encrpytPwd(accountCurrencyFE.getPassword()));
					accountCurrency.setTxnKey(accountCurrencyFE.getTxnKey());
					accountCurrency.setDirectTxn(accountCurrencyFE.isDirectTxn());
				}
			}
			if(!flag){
				AccountCurrency newAccountCurrency = new AccountCurrency();
				newAccountCurrency.setCurrencyCode(accountCurrencyFE.getCurrencyCode());

				newAccountCurrency.setMerchantId(accountCurrencyFE.getMerchantId());
				newAccountCurrency.setPassword(AccountPasswordScrambler.encrpytPwd(accountCurrencyFE.getPassword()));
				newAccountCurrency.setTxnKey(accountCurrencyFE.getTxnKey());				
				newAccountCurrency.setDirectTxn(accountCurrencyFE.isDirectTxn());

				newAccountCurrency.setAcqPayId(acquirer.getPayId());
				accountCurrencySet.add(newAccountCurrency);
				//add charging detail
				//get existing charging details and add them to account
				Set<Payment> paymentSet = account.getPayments();	

				for (Payment payment : paymentSet) {
					Set<Mop> mops = payment.getMops();
					for (Mop mop : mops) {
						if (payment.getPaymentType().equals(PaymentType.NET_BANKING)
								|| (payment.getPaymentType().equals(PaymentType.WALLET))) {
							ChargingDetails newChargingDetails = new ChargingDetails();
							newChargingDetails = maintainChargingDetails.createChargingDetail(payment.getPaymentType(),
									mop.getMopType(), acquirer.getBusinessName(), payId,
									accountCurrencyFE.getCurrencyCode());
							account.addChargingDetail(newChargingDetails);
						} else {
							Set<MopTransaction> mopTxnSet = mop.getMopTransactionTypes();
							for (MopTransaction mopTxn : mopTxnSet) {
								ChargingDetails newChargingDetails = new ChargingDetails();
								newChargingDetails = maintainChargingDetails.createChargingDetail(
										payment.getPaymentType(), mop.getMopType(), acquirer.getBusinessName(), payId,
										accountCurrencyFE.getCurrencyCode());
								newChargingDetails.setTransactionType(mopTxn.getTransactionType());
								account.addChargingDetail(newChargingDetails);
							}
						}
					}
				}
			}
		}
		return account;
	}

	//remove code
		public Account removeAccountCurrency(Account account, AccountCurrency[] selectedAccountCurrency){
			Iterator<AccountCurrency> accountCurrencySetItrator = account.getAccountCurrencySet().iterator();
			Set<ChargingDetails> chargingDetails = account.getChargingDetails();

			if(null == selectedAccountCurrency || selectedAccountCurrency.length==0){
				selectedAccountCurrency = new AccountCurrency[]{};
			}
			while(accountCurrencySetItrator.hasNext()){
				boolean flag = false;
				AccountCurrency accountCurrency = accountCurrencySetItrator.next();
				for(AccountCurrency accountCurrencyFE:selectedAccountCurrency){
					if(accountCurrency.getCurrencyCode().equals(accountCurrencyFE.getCurrencyCode())){
						flag=true;
					}
				}
				if(!flag){
					for(ChargingDetails chargingDetail :chargingDetails){
						if(chargingDetail.getStatus().equals(TDRStatus.ACTIVE) && chargingDetail.getCurrency().equals(accountCurrency.getCurrencyCode())){
							chargingDetail.setStatus(TDRStatus.INACTIVE);
							chargingDetail.setUpdatedDate(new Date());
						}
					}
					accountCurrencySetItrator.remove();
				}
			}
			return account;
		}

		public List<String> getCurrencyList(){
			List<String> mappedCurrency = new ArrayList<String>();
			Set<AccountCurrency> accountCurrencySet = account.getAccountCurrencySet();
			for(AccountCurrency accountCurrency:accountCurrencySet){
				mappedCurrency.add(accountCurrency.getCurrencyCode());
			}
			return mappedCurrency;
		}
}
