package com.kbn.crm.actionBeans;

import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.StatusEnquiryParameters;
import com.kbn.commons.util.CommanCsvReader;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.security.SecurityProcessor;

public class StatusUpdateProcessor extends AbstractSecureAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8218606918605067755L;
	private static Logger logger = Logger.getLogger(StatusUpdateProcessor.class.getName());

	public BatchResponseObject statusUpdateProcess(List<StatusEnquiryParameters> statusEnquiryList) {
		StringBuilder responseMessage = new StringBuilder();
		BatchResponseObject batchResponseObject = new BatchResponseObject();
		CommanCsvReader commanCsvReader = new CommanCsvReader();
		for (StatusEnquiryParameters enquiryParameters : statusEnquiryList) {
			Fields fields = null;
			try {
				fields = new Fields(commanCsvReader.prepareFields(enquiryParameters));
				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);
				//Verifying the fields here
				if (validater(fields)) {
					fields.updateStatus();
					fields.updateTransactionDetails();
				} else {
					responseMessage.append(ErrorType.VALIDATION_FAILED.getResponseMessage()
							+" order id: "+ enquiryParameters.getOrderId());
					continue;
				}
			} catch (Exception exception) {
				responseMessage.append(ErrorType.VALIDATION_FAILED.getResponseMessage());
				responseMessage.append(" order id: "+enquiryParameters.getOrderId());
				responseMessage.append("\n");
				logger.error("Exception", exception);
			}
		}
		batchResponseObject.setResponseMessage(responseMessage.toString());

		return batchResponseObject;

	}

	public boolean validater(Fields fields) {
		CrmValidator validator = new CrmValidator();
		if ((validator.validateBlankField(fields.get(FieldType.PAY_ID.getName())))) {
		} else if (!validator.validateField(CrmFieldType.PAY_ID, fields.get(FieldType.PAY_ID.getName()))) {
			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.TXN_ID.getName())))) {
		} else if (!validator.validateField(CrmFieldType.TXN_ID, fields.get(FieldType.TXN_ID.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.ORIG_TXN_ID.getName())))) {
		} else if (!validator.validateField(CrmFieldType.TXN_ID, fields.get(FieldType.ORIG_TXN_ID.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.AMOUNT.getName())))) {
		} else if (!validator.validateField(CrmFieldType.AMOUNT, fields.get(FieldType.AMOUNT.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.AUTH_CODE.getName())))) {
		} else if (!validator.validateField(CrmFieldType.AUTH_CODE, fields.get(FieldType.AUTH_CODE.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.RRN.getName())))) {
		} else if (!validator.validateField(CrmFieldType.RRN, fields.get(FieldType.RRN.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.ACQ_ID.getName())))) {
		} else if (!validator.validateField(CrmFieldType.ACQ_PAYID, fields.get(FieldType.ACQ_ID.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.STATUS.getName())))) {
		} else if (!validator.validateField(CrmFieldType.STATUS, fields.get(FieldType.STATUS.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.RESPONSE_CODE.getName())))) {
		} else if (!validator.validateField(CrmFieldType.RESPONSE_CODE,
				fields.get(FieldType.RESPONSE_CODE.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.RESPONSE_MESSAGE.getName())))) {
		} else if (!validator.validateField(CrmFieldType.RESPONSE_MESSAGE,
				fields.get(FieldType.RESPONSE_MESSAGE.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.PG_TXN_MESSAGE.getName())))) {
		} else if (!validator.validateField(CrmFieldType.PG_TXN_MESSAGE,
				fields.get(FieldType.PG_TXN_MESSAGE.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.PG_RESP_CODE.getName())))) {
		} else if (!validator.validateField(CrmFieldType.PG_RESP_CODE, fields.get(FieldType.PG_RESP_CODE.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.PG_REF_NUM.getName())))) {
		} else if (!validator.validateField(CrmFieldType.PG_REF_NUM, fields.get(FieldType.PG_REF_NUM.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.ACQUIRER_TYPE.getName())))) {
		} else if (!validator.validateField(CrmFieldType.ACQUIRER, fields.get(FieldType.ACQUIRER_TYPE.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.ORDER_ID.getName())))) {
		} else if (!validator.validateField(CrmFieldType.ORDER_ID, fields.get(FieldType.ORDER_ID.getName()))) {

			return false;
		}
		if ((validator.validateBlankField(fields.get(FieldType.TXNTYPE.getName())))) {
		} else if (!validator.validateField(CrmFieldType.TXNTYPE, fields.get(FieldType.TXNTYPE.getName()))) {

			return false;
		}

		if ((validator.validateBlankField(fields.get(FieldType.CURRENCY_CODE.getName())))) {
		} else if (!validator.validateField(CrmFieldType.CURRENCY, fields.get(FieldType.CURRENCY_CODE.getName()))) {

			return false;
		}
		return true;
	}

}
