package com.kbn.crm.actionBeans;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.CaptureTransactionService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.BatchTransactionObj;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailBuilder;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.RequestRouter;
import com.kbn.sms.SmsSender;

/**
 * @author Rahul
 *
 */
public class CaptureProcessor extends AbstractSecureAction{
	
	private static final long serialVersionUID = 1962966867662739938L;
	
	private static Logger logger = Logger.getLogger(CaptureProcessor.class.getName());
	private StringBuilder responseMessage = new StringBuilder();

	public String processAll(List<BatchTransactionObj> transactionList){
		for(BatchTransactionObj BatchOperationObj:transactionList){
			try{
				process(BatchOperationObj);
			}catch(Exception exception){
				logger.error("Error while capturing transaction: " + exception);
			}
		}
		return responseMessage.deleteCharAt(responseMessage.length()-1).toString();
	}

	public void process(BatchTransactionObj batchOperationObj) throws ParseException{
		Fields responseMap = null;

		try {
			String capturedTxnId = confirmIfCaptured(batchOperationObj);
			if (capturedTxnId != null) {
				//TODO to prepare a list of failed capture txns
				responseMessage.append(ErrorType.ALREADY_CAPTURED_TRANSACTION.getResponseMessage());
				responseMessage.append(batchOperationObj.getOrderId());
				responseMessage.append("\n");
				return;
			}
			User user = new UserDao().getUserClass(batchOperationObj.getPayId());
			Map<String, String> requestMap = new HashMap<String, String>();

			// Put parameters in request map
			requestMap.put(FieldType.AMOUNT.getName(),
					Amount.formatAmount(batchOperationObj.getAmount(), batchOperationObj.getCurrencyCode()));
			requestMap.put(FieldType.ORIG_TXN_ID.getName(), batchOperationObj.getOrigTxnId());
			requestMap.put(FieldType.PAY_ID.getName(), batchOperationObj.getPayId());
			requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.CAPTURE.getName());
			requestMap.put(FieldType.CURRENCY_CODE.getName(), batchOperationObj.getCurrencyCode());
			requestMap.put(FieldType.HASH.getName(),
							"1234567890123456789012345678901234567890123456789012345678901234"); 
			requestMap.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(), "N"); 

			// Preparing fields
			Fields fields = new Fields(requestMap);
			fields.logAllFields("All request fields :");
			RequestRouter router = new RequestRouter(fields);
			// Process request
			responseMap = new Fields(router.route());
			//send sms  
			SmsSender.sendSMS(fields);

			String responseCode = responseMap.get(FieldType.RESPONSE_CODE.getName());
			// Sending Email for Transaction Status to merchant or customer
			if(responseCode.equals("000")) {
				if(user.isRefundTransactionCustomerEmailFlag()){
				EmailBuilder emailBuilder = new EmailBuilder();
				emailBuilder.transactionRefundEmail(responseMap,CrmFieldConstants.CUSTOMER.toString(),
						fields.get(FieldType.CUST_EMAIL.getName()),user.getBusinessName());
				}
				if (user.isRefundTransactionMerchantEmailFlag()){
					EmailBuilder emailBuilder = new EmailBuilder();
					emailBuilder.transactionRefundEmail(responseMap,UserType.MERCHANT.toString(),
							user.getTransactionEmailId(),user.getBusinessName());
				}
			}

			if (null == responseCode
					|| !responseCode.equals(ErrorType.SUCCESS.getCode())) {
				responseMessage.append(ErrorType.CAPTURE_NOT_SUCCESSFULL.getResponseMessage());
				responseMessage.append(fields.get(FieldType.ORDER_ID.getName()));
				responseMessage.append("\n");
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			responseMessage.append(ErrorType.CAPTURE_NOT_SUCCESSFULL.getResponseMessage());
			responseMessage.append(batchOperationObj.getOrderId());
			responseMessage.append("\n");
		}
	}

	private String confirmIfCaptured(BatchTransactionObj batchOperationObj) throws SystemException {
		CaptureTransactionService captureService = new CaptureTransactionService();
		return captureService.getCapturedTransaction(batchOperationObj.getOrigTxnId(), batchOperationObj.getPayId());
	}
}