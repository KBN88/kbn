package com.kbn.crm.actionBeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.dao.ServiceTaxDao;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.ChargingDetailsDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.pg.core.AcquirerType;

/**
 * @author Puneet
 *
 */

public class ChargingDetailsFactory {

	public Map<String, List<ChargingDetails>> getChargingDetailsMap(
			String merchantEmailId, String acquirerCode) {

		Map<String, List<ChargingDetails>> chargingDetailsMap = new HashMap<String, List<ChargingDetails>>();
		UserDao userDao = new UserDao();
		Session session = null;
		try {
			User user = userDao.find(merchantEmailId);
			Account account = user.getAccountUsingAcquirerCode(acquirerCode);
			// Get the charging details
			if (null != account) {
				session = HibernateSessionProvider.getSession();
				Transaction tx = session.beginTransaction();
				session.load(account, account.getId());
				Set<ChargingDetails> data = account.getChargingDetails();
				for (PaymentType paymentType : PaymentType.values()) {
					List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
					String paymentName = paymentType.getName();
					for (ChargingDetails cDetail : data) {
						if (cDetail.getStatus().equals(TDRStatus.ACTIVE) && cDetail.getPaymentType().getName()
										.equals(paymentName)) {
							cDetail.setMerchantServiceTax(new ServiceTaxDao().findServiceTaxByPayId(cDetail.getPayId()).longValue());
							//cDetail.setMerchantFixCharge(cDetail.getBankFixCharge() + cDetail.getPgFixCharge());
							//cDetail.setMerchantTDR(cDetail.);
							chargingDetailsList.add(cDetail);
						}
					}
					if (chargingDetailsList.size() != 0) {
						Collections.sort(chargingDetailsList);
						chargingDetailsMap.put(paymentName, chargingDetailsList);
					}
				}
				tx.commit();
			}
			return chargingDetailsMap;
		} finally {
			HibernateSessionProvider.closeSession(session);
		}
	}

	public ChargingDetails getSingleChargingDetail(Account account,Long charginDetailId){
		for(ChargingDetails cDetail:account.getChargingDetails()){
			   if(cDetail.getId().equals(charginDetailId)){
				   return cDetail;
			   }
		}
				   return null;				
	}
	
	// supply names of payment type and mops and code of acquirer 
		public ChargingDetails getChargingDetail(String date,String payId,String acquirer,String paymentType,String mopType,String txnType,String currencyCode){
			 ChargingDetails detail = null;
			 
			 List<ChargingDetails>  chargingDetailsList = new ChargingDetailsDao().findDetail(date, payId, AcquirerType.getInstancefromCode(acquirer).getName(), 
					                                                                PaymentType.getInstance(paymentType).toString(), MopType.getInstance(mopType).toString(),
					                                                                        currencyCode);
			 
			 Iterator<ChargingDetails> chargingDetailsItr = chargingDetailsList.iterator();
			 
			 while(chargingDetailsItr.hasNext()){				 
				 if(paymentType.equals(PaymentType.NET_BANKING.getName()) ||paymentType.equals(PaymentType.WALLET.getName()) 
						 || paymentType.equals(PaymentType.UPI.getName())){
					      detail = chargingDetailsItr.next();
					      break;
			      }else{
			    	  detail = chargingDetailsItr.next();
			    	  if(detail.getTransactionType().toString().equals(txnType)){
			    		 break;
			    	  }
			      }		
			 } 
			 return detail;
		}
		
}
