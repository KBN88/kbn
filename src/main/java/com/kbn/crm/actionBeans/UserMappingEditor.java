package com.kbn.crm.actionBeans;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;

/**
 * @author Puneet
 *
 */
public class UserMappingEditor {

	private boolean accountPresent = false;

	public void decideAccountChange(String userEmail, String mappingString,
			String acquirerCode, AccountCurrency[] accountCurrencySetFE) {
		
		Session session = null;
		try {
			UserDao userDao = new UserDao();
			User user = new User();
			Account account = new Account();
			Account savedAccount = new Account();
			AccountFactory accountFactory = new AccountFactory();
			String acquirerPayId;
			User acquirer;
			acquirer = userDao.findAcquirerByCode(acquirerCode);
			acquirerPayId = (acquirer.getPayId());
			user = userDao.find(userEmail);

			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			session.load(user, userEmail);
			savedAccount = user.getAccountUsingAcquirerCode(acquirerCode);
			if (savedAccount != null) {
				setAccountPresent(true);
			}

			// create account
			if (!accountPresent) {
				account.setAcquirerPayId(acquirerPayId);
				account.setAcquirerName(acquirer.getBusinessName());
				account = accountFactory.editAccount(account, mappingString,user.getPayId());
				// add currency
				account = accountFactory.addAccountCurrency(account, accountCurrencySetFE, acquirer, user.getPayId());
				user.addAccount(account);
			} else {
				// edit if the account present
				savedAccount = accountFactory.editAccount(savedAccount,	mappingString, user.getPayId());
				// edit currency
				savedAccount = accountFactory.addAccountCurrency(savedAccount, accountCurrencySetFE, acquirer, user.getPayId());
				savedAccount = accountFactory.removeAccountCurrency(savedAccount, accountCurrencySetFE);
			}
			session.update(user);
			tx.commit();
		} finally {
			HibernateSessionProvider.closeSession(session);
		}
	}

	public boolean isAccountPresent() {
		return accountPresent;
	}

	public void setAccountPresent(boolean accountPresent) {
		this.accountPresent = accountPresent;
	}
}
