package com.kbn.crm.actionBeans;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.dao.NotificationDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.Messages;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.PendingResellerMappingApproval;
import com.kbn.commons.user.PendingResellerMappingDao;
import com.kbn.commons.user.ServiceTax;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.NotificationStatusType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.pg.core.NotificationDetail;

/**
 * @author Rahul
 *
 */
public class MerchantMappingFactory {
	private String actionMessage;

	public Map<String, List<Merchants>> getMerchantMappingFactory(String merchantEmailId, String reseller,
			String userType, StringBuilder permissions, User sessionUser) {
		Date date = new Date();
		Map<String, List<Merchants>> resellerMapList = new HashMap<String, List<Merchants>>();
		UserDao userResellerDao = new UserDao();
		User userReseller = userResellerDao.find(reseller);
		// user.getResellerId();
		String[] merchant = merchantEmailId.split(", ");
		for (String emailId : merchant) {

			UserDao userDao = new UserDao();
			User user = userDao.find(emailId);
			user.setResellerId(userReseller.getPayId());
			user.setUpdatedBy(sessionUser.getEmailId());
			user.setUpdateDate(date);
			if (userType.equals(UserType.ADMIN.toString())
					|| permissions.toString().contains("Create Reseller Mapping")) {
				//Cancel existing request before force update
				PendingResellerMappingApproval existingPendingRequest = new PendingResellerMappingApproval();
				existingPendingRequest = new PendingResellerMappingDao().findExistingMappingRequest(emailId, reseller);
				if (existingPendingRequest != null){
					cancelExistingPendingRequest(existingPendingRequest , TDRStatus.CANCELLED);
				}
				userDao.update(user);
			} else if (sessionUser.getUserType().equals(UserType.SUBADMIN)) {
				PendingResellerMappingDao pendingResellerMappingDao = new PendingResellerMappingDao();
				PendingResellerMappingApproval request = pendingResellerMappingDao.find(merchantEmailId);
				if (request != null) {
					setActionMessage(CrmFieldConstants.PENDING_REQUEST_EXIST.getValue());
					return resellerMapList;
				}
				String businessName = user.getBusinessName();
				String payId = user.getPayId();
				PendingResellerMappingApproval newPendingResellerMappingApproval = createPendingApprovalFields(
						merchantEmailId, reseller, sessionUser,businessName,payId);
				pendingResellerMappingDao.create(newPendingResellerMappingApproval);
				setActionMessage(CrmFieldConstants.DETAILS_UPDATE_REQUEST.getValue());
			}
		}
		return resellerMapList;
	}

	public PendingResellerMappingApproval createPendingApprovalFields(String merchantEmailId, String reseller,
			User sessionUser, String businessName, String payId) {
		Date date = new Date();
		PendingResellerMappingApproval prma = new PendingResellerMappingApproval();
		prma.setCreateDate(date);
		prma.setMerchantEmailId(merchantEmailId);
		prma.setRequestedBy(sessionUser.getEmailId());
		prma.setRequestStatus(TDRStatus.PENDING.toString());
		prma.setResellerId(reseller);
		prma.setBusinessName(businessName);
		prma.setMerchantPayId(payId);
		
		NotificationDetail notificationDetail = new NotificationDetail();
		notificationDetail.setConcernedUser(merchantEmailId);
		notificationDetail.setCreateDate(date);
		notificationDetail.setSubmittedBy(sessionUser.getEmailId());
		notificationDetail.setStatus(NotificationStatusType.UNREAD.getName());
		notificationDetail.setMessage(Messages.RESELLER_MAPPING_MESSAGE.getResponseMessage());
		NotificationDao notificationDao = new NotificationDao();
		notificationDao.create(notificationDetail);
		return prma;

	}
	
	public void cancelExistingPendingRequest(PendingResellerMappingApproval existingPendingRequest, TDRStatus status) {

		try {
			Date currentDate = new Date();
			Session session = null;
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			Long id = existingPendingRequest.getId();
			session.load(existingPendingRequest, existingPendingRequest.getId());
			PendingResellerMappingApproval existingRequest = (PendingResellerMappingApproval) session.get(PendingResellerMappingApproval.class, id);
			existingRequest.setRequestStatus(status.toString());
			existingRequest.setUpdateDate(currentDate);
			//existingRequest.setProcessedBy(emailId);
			session.update(existingRequest);
			tx.commit();
			session.close();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {

		}

	}

	public String getActionMessage() {
		return actionMessage;
	}

	public void setActionMessage(String actionMessage) {
		this.actionMessage = actionMessage;
	}

}
