package com.kbn.crm.actionBeans;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionHistory;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.crm.charging.TdrCalculator;
import com.kbn.crm.charging.TransactionDetailFactory;
import com.kbn.pg.core.Currency;

/**
 * @author Puneet
 *
 */

public class RefundDetailsProvider implements Serializable{
	
	public RefundDetailsProvider(String orderId, String payId,String txnId) throws SystemException{
		// get transaction details
		 List<TransactionHistory> transactionList = (TransactionDetailFactory.getTransactionDetail().getAllTransactionsFromDb(txnId));
		 this.oldTransactions = transactionList;
	}
	
	private static final long serialVersionUID = -3732059786389209314L;
	private List<TransactionHistory> oldTransactions = null;
	private int decimalPlaces;
	private TransactionHistory transDetails = new TransactionHistory();	
	
	public void getAllTransactions(){	
		TdrCalculator tdrCalculater = new TdrCalculator();
		// set details to be displayed
		for (TransactionHistory transaction : oldTransactions) {
			TransactionType transactionType = TransactionType
					.getInstance(transaction.getTxnType());
			decimalPlaces = Currency.getNumberOfPlaces(transaction
					.getCurrencyCode());
			switch (transactionType) {
			case NEWORDER:// set details recieved at the time of new order
				transDetails.setNewOrderTransactionDetails(transaction);
				break;
			case AUTHORISE:
				if(transaction.getStatus().equals(StatusType.APPROVED.getName())){
					transDetails.setOriginalTransactionDetails(transaction);
				}
				break;
			case SALE:
				if(transaction.getStatus().equals(StatusType.CAPTURED.getName())){
					transDetails.setOriginalTransactionDetails(transaction);
				}
				break;
			case CAPTURE:
				if(transaction.getStatus().equals(StatusType.CAPTURED.getName())){
					transDetails.setCaptureTransactionDetails(transaction);
				}	
				break;
			case REFUND:
				transDetails.setRefundedTransactionDetails(transaction,decimalPlaces);
				break;
			default:
				break;
			}
		}
		
		removeOldTransactions();
		transDetails = tdrCalculater.setTdrRefundDetails(transDetails,decimalPlaces);
	}
	
	public void removeOldTransactions(){
		// remove enroll and new order transaction; if exists
				Iterator<TransactionHistory> transItrator = oldTransactions.iterator();
				while (transItrator.hasNext()) {
					TransactionHistory transactionInstance = transItrator.next();
					if (transactionInstance.getTxnType().equals(
							TransactionType.NEWORDER.getName())
							|| transactionInstance.getTxnType().equals(
									TransactionType.ENROLL.getName())) {
						transItrator.remove();
					}
			}
	}
	
	public List<TransactionHistory> getOldTransactions() {
		return oldTransactions;
	}

	public void setOldTransactions(List<TransactionHistory> oldTransactions) {
		this.oldTransactions = oldTransactions;
	}

	public TransactionHistory getTransDetails() {
		return transDetails;
	}

	public void setTransDetails(TransactionHistory transDetails) {
		this.transDetails = transDetails;
	}
}
