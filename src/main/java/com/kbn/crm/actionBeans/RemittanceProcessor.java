package com.kbn.crm.actionBeans;

import java.text.ParseException;
import java.util.List;

import javax.transaction.SystemException;

import org.apache.log4j.Logger;

import com.kbn.commons.user.Remittance;
import com.kbn.commons.user.RemittanceDao;
import com.kbn.commons.util.CrmFieldConstants;

public class RemittanceProcessor {
	private static Logger logger = Logger.getLogger(RemittanceProcessor.class
			.getName());
	
	private RemittanceDao remittanceDao = new RemittanceDao(); 
	public void processAll(List<Remittance> remittanceList){
		for(Remittance remittance:remittanceList){
			try{
				process(remittance);
			}catch(Exception exception){
				logger.error("Error while processing with utr no.: " + remittance.getUtr() + exception);
			}
		}
	}
	
	public void process(Remittance remittance) throws ParseException,SystemException {
		remittance.setStatus(CrmFieldConstants.SETTLEMENT_PROCESSED.getValue());
		remittanceDao.updateStatus(remittance.getUtr(), remittance.getStatus());
	}

}
