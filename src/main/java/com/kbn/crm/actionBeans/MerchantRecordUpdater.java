package com.kbn.crm.actionBeans;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.dao.NotificationDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.Messages;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.NotificationEmailer;
import com.kbn.commons.user.PendingUserApproval;
import com.kbn.commons.user.PendingUserApprovalDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.NotificationStatusType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.action.MerchantLogUpdater;
import com.kbn.pg.core.NotificationDetail;

/**
 * @author Rahul
 *
 */
public class MerchantRecordUpdater {

	private NotificationEmailer notificationEmailerDB = null;
	private Set<Account> accountSetDB = null;
	private UserDao userDao = new UserDao();
	private String actionMessage;
	private MerchantLogUpdater merchantLogUpdater = new MerchantLogUpdater();
	User dbuser = null;
	private User userFromDB = null;

	public Map<String, User> updateUserPendingDetails(User userFE, User sessionUser, List<Account> newAccounts,
			List<AccountCurrency> accountCurrencyList, StringBuilder permissions) {

		User user = updateUserDetails(userFE, sessionUser, newAccounts, accountCurrencyList, permissions);
		Map<String, User> returnMap = new HashMap<String, User>();
		returnMap.put(actionMessage, user);
		return returnMap;
	}

	public User updateUserDetails(User userFE, User sessionUser, List<Account> newAccounts,
			List<AccountCurrency> accountCurrencyList, StringBuilder permissions) {
		Date date = new Date();
		userFromDB = userDao.findPayId(userFE.getPayId());
		User userDB = (User) SerializationUtils.clone(userFromDB);
		// Set details of user for edit
		userFromDB.setModeType(userFE.getModeType());
		userFromDB.setComments(userFE.getComments());
		userFromDB.setWhiteListIpAddress(userFE.getWhiteListIpAddress());
		userFromDB.setUserStatus(userFE.getUserStatus());

		userFromDB.setBusinessName(userFE.getBusinessName());
		userFromDB.setFirstName(userFE.getFirstName());
		userFromDB.setLastName(userFE.getLastName());
		userFromDB.setCompanyName(userFE.getCompanyName());
		userFromDB.setWebsite(userFE.getWebsite());
		userFromDB.setContactPerson(userFE.getContactPerson());
		userFromDB.setEmailId(userFE.getEmailId());
		userFromDB.setRegistrationDate(userFE.getRegistrationDate());
		userFromDB.setActivationDate(userFE.getActivationDate());

		userFromDB.setMerchantType(userFE.getMerchantType());
		userFromDB.setNoOfTransactions(userFE.getNoOfTransactions());
		userFromDB.setAmountOfTransactions(userFE.getAmountOfTransactions());
		userFromDB.setResellerId(userFE.getResellerId());
		userFromDB.setProductDetail(userFE.getProductDetail());

		userFromDB.setMobile(userFE.getMobile());
		userFromDB.setTransactionSmsFlag(userFE.isTransactionSmsFlag());
		userFromDB.setTelephoneNo(userFE.getTelephoneNo());
		userFromDB.setFax(userFE.getFax());
		userFromDB.setAddress(userFE.getAddress());
		userFromDB.setCity(userFE.getCity());
		userFromDB.setState(userFE.getState());
		userFromDB.setCountry(userFE.getCountry());
		userFromDB.setPostalCode(userFE.getPostalCode());

		userFromDB.setBankName(userFE.getBankName());
		userFromDB.setIfscCode(userFE.getIfscCode());
		userFromDB.setAccHolderName(userFE.getAccHolderName());
		userFromDB.setCurrency(userFE.getCurrency());
		userFromDB.setBranchName(userFE.getBranchName());
		userFromDB.setPanCard(userFE.getPanCard());
		userFromDB.setAccountNo(userFE.getAccountNo());

		userFromDB.setOrganisationType(userFE.getOrganisationType());
		userFromDB.setWebsite(userFE.getWebsite());
		userFromDB.setMultiCurrency(userFE.getMultiCurrency());
		userFromDB.setBusinessModel(userFE.getBusinessModel());
		userFromDB.setOperationAddress(userFE.getOperationAddress());
		userFromDB.setOperationState(userFE.getOperationState());
		userFromDB.setOperationCity(userFE.getOperationCity());
		userFromDB.setOperationPostalCode(userFE.getOperationPostalCode());
		userFromDB.setDateOfEstablishment(userFE.getDateOfEstablishment());

		userFromDB.setCin(userFE.getCin());
		userFromDB.setPan(userFE.getPan());
		userFromDB.setPanName(userFE.getPanName());
		userFromDB.setNoOfTransactions(userFE.getNoOfTransactions());
		userFromDB.setAmountOfTransactions(userFE.getAmountOfTransactions());
		userFromDB.setTransactionEmailerFlag(userFE.isTransactionEmailerFlag());
		userFromDB.setTransactionEmailId(userFE.getTransactionEmailId());
		userFromDB.setExpressPayFlag(userFE.isExpressPayFlag());
		userFromDB.setMerchantHostedFlag(userFE.isMerchantHostedFlag());
		userFromDB.setIframePaymentFlag(userFE.isIframePaymentFlag());
		userFromDB.setSurchargeFlag(userFE.isSurchargeFlag());
		userFromDB.setTransactionAuthenticationEmailFlag(userFE.isTransactionAuthenticationEmailFlag());
		userFromDB.setTransactionCustomerEmailFlag(userFE.isTransactionCustomerEmailFlag());
		userFromDB.setRefundTransactionCustomerEmailFlag(userFE.isRefundTransactionCustomerEmailFlag());
		userFromDB.setRefundTransactionMerchantEmailFlag(userFE.isRefundTransactionMerchantEmailFlag());
		userFromDB.setRetryTransactionCustomeFlag(userFE.isRetryTransactionCustomeFlag());
		userFromDB.setAttemptTrasacation(userFE.getAttemptTrasacation());
		userFromDB.setExtraRefundLimit(userFE.getExtraRefundLimit());
		userFromDB.setUpdateDate(date);
		userFromDB.setDefaultCurrency(userFE.getDefaultCurrency());
		userFromDB.setMCC(userFE.getMCC());
		userFromDB.setAmexSellerId(userFE.getAmexSellerId());
		userFromDB.setDefaultLanguage(userFE.getDefaultLanguage());
		userFromDB.setIndustryCategory(userFE.getIndustryCategory());
		userFromDB.setIndustrySubCategory(userFE.getIndustrySubCategory());
		userFromDB.setUpdatedBy(sessionUser.getEmailId());
		// Update account details
		updateAccount(newAccounts, accountCurrencyList);

		if (sessionUser.getUserType().equals(UserType.ADMIN)
				|| permissions.toString().contains("Edit Merchant Details")) {
			userDao.update(userFromDB);
			merchantLogUpdater.updateValue(userFE, sessionUser, newAccounts, accountCurrencyList, userDB);
			setActionMessage(CrmFieldConstants.USER_DETAILS_UPDATED.getValue());
		} else if (sessionUser.getUserType().equals(UserType.SUBADMIN)) {
			PendingUserApprovalDao pendingUserApprovalDao = new PendingUserApprovalDao();
			PendingUserApproval request = pendingUserApprovalDao.find(userFE.getPayId());
			if (request != null) {
				setActionMessage(CrmFieldConstants.PENDING_REQUEST_EXIST.getValue());
				return userFromDB;
			}
			
			PendingUserApproval newPendingUserApproval = createPendingApprovalFields(userFE, sessionUser);
			pendingUserApprovalDao.create(newPendingUserApproval);
			setActionMessage(CrmFieldConstants.DETAILS_UPDATE_REQUEST.getValue());
			return userFromDB;
		}	
		return userFromDB;
	}

	public PendingUserApproval createPendingApprovalFields(User userFE, User sessionUser) {
		Date date = new Date();
		User dbuser = userDao.findPayId(userFE.getPayId());

		// Set details of user for edit
		PendingUserApproval pua = new PendingUserApproval();
		pua.setModeType(userFE.getModeType());
		pua.setComments(userFE.getComments());
		pua.setWhiteListIpAddress(userFE.getWhiteListIpAddress());
		pua.setUserStatus(userFE.getUserStatus());

		pua.setBusinessName(userFE.getBusinessName());
		pua.setFirstName(userFE.getFirstName());
		pua.setLastName(userFE.getLastName());
		pua.setCompanyName(userFE.getCompanyName());
		pua.setWebsite(userFE.getWebsite());
		pua.setContactPerson(userFE.getContactPerson());
		pua.setEmailId(userFE.getEmailId());
		pua.setRegistrationDate(userFE.getRegistrationDate());
		
		pua.setMerchantType(userFE.getMerchantType());
		pua.setNoOfTransactions(userFE.getNoOfTransactions());
		pua.setAmountOfTransactions(userFE.getAmountOfTransactions());
		pua.setResellerId(userFE.getResellerId());
		pua.setProductDetail(userFE.getProductDetail());

		pua.setMobile(userFE.getMobile());
		pua.setTransactionSmsFlag(userFE.isTransactionSmsFlag());
		pua.setTelephoneNo(userFE.getTelephoneNo());
		pua.setFax(userFE.getFax());
		pua.setAddress(userFE.getAddress());
		pua.setCity(userFE.getCity());
		pua.setState(userFE.getState());
		pua.setCountry(userFE.getCountry());
		pua.setPostalCode(userFE.getPostalCode());

		pua.setBankName(userFE.getBankName());
		pua.setIfscCode(userFE.getIfscCode());
		pua.setAccHolderName(userFE.getAccHolderName());
		pua.setCurrency(userFE.getCurrency());
		pua.setBranchName(userFE.getBranchName());
		pua.setPanCard(userFE.getPanCard());
		pua.setAccountNo(userFE.getAccountNo());

		pua.setOrganisationType(userFE.getOrganisationType());
		pua.setWebsite(userFE.getWebsite());
		pua.setMultiCurrency(userFE.getMultiCurrency());
		pua.setBusinessModel(userFE.getBusinessModel());
		pua.setOperationAddress(userFE.getOperationAddress());
		pua.setOperationState(userFE.getOperationState());
		pua.setOperationCity(userFE.getOperationCity());
		pua.setOperationPostalCode(userFE.getOperationPostalCode());
		pua.setDateOfEstablishment(userFE.getDateOfEstablishment());

		pua.setCin(userFE.getCin());
		pua.setPan(userFE.getPan());
		pua.setPanName(userFE.getPanName());
		pua.setNoOfTransactions(userFE.getNoOfTransactions());
		pua.setAmountOfTransactions(userFE.getAmountOfTransactions());
		pua.setTransactionEmailerFlag(userFE.isTransactionEmailerFlag());
		pua.setTransactionEmailId(userFE.getTransactionEmailId());
		pua.setExpressPayFlag(userFE.isExpressPayFlag());
		pua.setMerchantHostedFlag(userFE.isMerchantHostedFlag());
		pua.setIframePaymentFlag(userFE.isIframePaymentFlag());
		pua.setSurchargeFlag(userFE.isSurchargeFlag());
		pua.setTransactionAuthenticationEmailFlag(userFE.isTransactionAuthenticationEmailFlag());
		pua.setTransactionCustomerEmailFlag(userFE.isTransactionCustomerEmailFlag());
		pua.setRefundTransactionCustomerEmailFlag(userFE.isRefundTransactionCustomerEmailFlag());
		pua.setRefundTransactionMerchantEmailFlag(userFE.isRefundTransactionMerchantEmailFlag());
		pua.setRetryTransactionCustomeFlag(userFE.isRetryTransactionCustomeFlag());
		pua.setAttemptTrasacation(userFE.getAttemptTrasacation());
		pua.setExtraRefundLimit(userFE.getExtraRefundLimit());
		pua.setUpdateDate(date);
		pua.setDefaultCurrency(userFE.getDefaultCurrency());
		pua.setMCC(userFE.getMCC());
		pua.setAmexSellerId(userFE.getAmexSellerId());
		pua.setDefaultLanguage(userFE.getDefaultLanguage());
		pua.setIndustryCategory(userFE.getIndustryCategory());
		pua.setIndustrySubCategory(userFE.getIndustrySubCategory());
		pua.setRequestedBy(sessionUser.getEmailId());
		pua.setPayId(userFE.getPayId());
		pua.setRequestStatus(TDRStatus.PENDING.toString());
		
		NotificationDetail notificationDetail = new NotificationDetail();
		notificationDetail.setCreateDate(date);
		notificationDetail.setSubmittedBy(sessionUser.getEmailId());
		notificationDetail.setConcernedUser(userFE.getEmailId());
		notificationDetail.setStatus(NotificationStatusType.UNREAD.getName());
		notificationDetail.setMessage(Messages.USER_UPDATE_MESSAGE.getResponseMessage());
		NotificationDao notificationDao = new NotificationDao();
		notificationDao.create(notificationDetail);
		return pua;

	}

	// merchant end code
	// update profile from user end when merchant is nor activated
	public User updateUserProfile(User userFE) {

		userFromDB = userDao.findPayId(userFE.getPayId());

		// Set details of user for edit
		userFromDB.setBusinessName(userFE.getBusinessName());
		userFromDB.setFirstName(userFE.getFirstName());
		userFromDB.setLastName(userFE.getLastName());
		userFromDB.setEmailId(userFE.getEmailId());
		userFromDB.setCompanyName(userFE.getCompanyName());
		userFromDB.setTelephoneNo(userFE.getTelephoneNo());
		userFromDB.setAddress(userFE.getAddress());
		userFromDB.setCity(userFE.getCity());
		userFromDB.setState(userFE.getState());
		userFromDB.setCountry(userFE.getCountry());
		userFromDB.setPostalCode(userFE.getPostalCode());

		userFromDB.setOrganisationType(userFE.getOrganisationType());
		userFromDB.setWebsite(userFE.getWebsite());
		userFromDB.setMultiCurrency(userFE.getMultiCurrency());
		userFromDB.setBusinessModel(userFE.getBusinessModel());

		userFromDB.setAddress(userFE.getAddress());
		userFromDB.setState(userFE.getState());
		userFromDB.setCity(userFE.getCity());
		userFromDB.setPostalCode(userFE.getPostalCode());
		userFromDB.setOperationAddress(userFE.getOperationAddress());
		userFromDB.setOperationState(userFE.getOperationState());
		userFromDB.setOperationCity(userFE.getOperationCity());
		userFromDB.setOperationPostalCode(userFE.getOperationPostalCode());

		userFromDB.setBankName(userFE.getBankName());
		userFromDB.setIfscCode(userFE.getIfscCode());
		userFromDB.setAccHolderName(userFE.getAccHolderName());
		userFromDB.setCurrency(userFE.getCurrency());
		userFromDB.setBranchName(userFE.getBranchName());
		userFromDB.setPanCard(userFE.getPanCard());
		userFromDB.setAccountNo(userFE.getAccountNo());

		userFromDB.setDateOfEstablishment(userFE.getDateOfEstablishment());
		userFromDB.setCin(userFE.getCin());
		userFromDB.setPan(userFE.getPan());
		userFromDB.setPanName(userFE.getPanName());
		userFromDB.setNoOfTransactions(userFE.getNoOfTransactions());
		userFromDB.setAmountOfTransactions(userFE.getAmountOfTransactions());
		userFromDB.setTransactionEmailerFlag(userFE.isTransactionEmailerFlag());
		userFromDB.setTransactionEmailId(userFE.getTransactionEmailId());
		userFromDB.setDefaultCurrency(userFE.getDefaultCurrency());
		userDao.update(userFromDB);

		return userFromDB;
	}

	public void updateAccount(List<Account> newAccounts, List<AccountCurrency> accountCurrencyList) {
		accountSetDB = userFromDB.getAccounts();

		for (Account accountFE : newAccounts) {
			for (Account accountDB : accountSetDB) {
				if (accountFE.getAcquirerPayId().equals(accountDB.getAcquirerPayId())) {
					accountDB.setPrimaryStatus(accountFE.isPrimaryStatus());

					accountDB.setPrimaryNetbankingStatus(accountFE.isPrimaryNetbankingStatus());
					accountDB.setPrimaryWalletStatus(accountFE.isPrimaryWalletStatus());
					Set<AccountCurrency> accountCurrencySetDB = accountDB.getAccountCurrencySet();

					// accountDB.setNetbankingPrimaryStatus(accountFE.isNetbankingPrimaryStatus());
					for (AccountCurrency accountCurrencyDB : accountCurrencySetDB) {
						for (AccountCurrency accountCurrencyFE : accountCurrencyList) {
							if (accountCurrencyFE.getCurrencyCode().equals(accountCurrencyDB.getCurrencyCode())
									&& accountCurrencyFE.getAcqPayId().equals(accountCurrencyDB.getAcqPayId())) {
								accountCurrencyDB.setMerchantId(accountCurrencyFE.getMerchantId());
								accountCurrencyDB.setTxnKey(accountCurrencyFE.getTxnKey());
								accountCurrencyDB.setPassword(accountCurrencyFE.getPassword());
								accountCurrencyDB.setDirectTxn(accountCurrencyFE.isDirectTxn());

								if (!StringUtils.isAnyEmpty(accountCurrencyFE.getPassword())) {
									if (!StringUtils.isAnyEmpty(accountCurrencyFE.getPassword().trim())) {
										accountCurrencyDB.setPassword(
												AccountPasswordScrambler.encrpytPwd(accountCurrencyFE.getPassword()));
									}

								}
							}
						}
					}
				}
			}
		}
	}

	public User updateResellerDetails(User user) {
		Date date = new Date();
		userFromDB = userDao.findPayId(user.getPayId());
		userFromDB.setModeType(user.getModeType());
		userFromDB.setComments(user.getComments());
		userFromDB.setWhiteListIpAddress(user.getWhiteListIpAddress());
		userFromDB.setUserStatus(user.getUserStatus());

		userFromDB.setBusinessName(user.getBusinessName());
		userFromDB.setFirstName(user.getFirstName());
		userFromDB.setLastName(user.getLastName());
		userFromDB.setCompanyName(user.getCompanyName());
		userFromDB.setWebsite(user.getWebsite());
		userFromDB.setContactPerson(user.getContactPerson());
		userFromDB.setEmailId(user.getEmailId());
		userFromDB.setRegistrationDate(user.getRegistrationDate());
		userFromDB.setActivationDate(user.getActivationDate());

		userFromDB.setMerchantType(user.getMerchantType());
		userFromDB.setNoOfTransactions(user.getNoOfTransactions());
		userFromDB.setAmountOfTransactions(user.getAmountOfTransactions());
		userFromDB.setResellerId(user.getResellerId());
		userFromDB.setProductDetail(user.getProductDetail());

		userFromDB.setMobile(user.getMobile());
		userFromDB.setTransactionSmsFlag(user.isTransactionSmsFlag());
		userFromDB.setTelephoneNo(user.getTelephoneNo());
		userFromDB.setFax(user.getFax());
		userFromDB.setAddress(user.getAddress());
		userFromDB.setCity(user.getCity());
		userFromDB.setState(user.getState());
		userFromDB.setCountry(user.getCountry());
		userFromDB.setPostalCode(user.getPostalCode());

		userFromDB.setBankName(user.getBankName());
		userFromDB.setIfscCode(user.getIfscCode());
		userFromDB.setAccHolderName(user.getAccHolderName());
		userFromDB.setCurrency(user.getCurrency());
		userFromDB.setBranchName(user.getBranchName());
		userFromDB.setPanCard(user.getPanCard());
		userFromDB.setAccountNo(user.getAccountNo());

		userFromDB.setOrganisationType(user.getOrganisationType());
		userFromDB.setWebsite(user.getWebsite());
		userFromDB.setMultiCurrency(user.getMultiCurrency());
		userFromDB.setBusinessModel(user.getBusinessModel());
		userFromDB.setOperationAddress(user.getOperationAddress());
		userFromDB.setOperationState(user.getOperationState());
		userFromDB.setOperationCity(user.getOperationCity());
		userFromDB.setOperationPostalCode(user.getOperationPostalCode());
		userFromDB.setDateOfEstablishment(user.getDateOfEstablishment());

		userFromDB.setCin(user.getCin());
		userFromDB.setPan(user.getPan());
		userFromDB.setPanName(user.getPanName());
		userFromDB.setNoOfTransactions(user.getNoOfTransactions());
		userFromDB.setAmountOfTransactions(user.getAmountOfTransactions());
		userFromDB.setTransactionEmailerFlag(user.isTransactionEmailerFlag());
		userFromDB.setTransactionEmailId(user.getTransactionEmailId());
		userFromDB.setExpressPayFlag(user.isExpressPayFlag());
		userFromDB.setMerchantHostedFlag(user.isMerchantHostedFlag());
		userFromDB.setIframePaymentFlag(user.isIframePaymentFlag());
		userFromDB.setTransactionAuthenticationEmailFlag(user.isTransactionAuthenticationEmailFlag());
		userFromDB.setTransactionCustomerEmailFlag(user.isTransactionCustomerEmailFlag());
		userFromDB.setRetryTransactionCustomeFlag(user.isRetryTransactionCustomeFlag());
		userFromDB.setAttemptTrasacation(user.getAttemptTrasacation());
		userFromDB.setUpdateDate(date);
		userFromDB.setDefaultCurrency(user.getDefaultCurrency());
		userDao.update(userFromDB);

		return userFromDB;
	}

	public String getActionMessage() {
		return actionMessage;
	}

	public void setActionMessage(String actionMessage) {
		this.actionMessage = actionMessage;
	}

	public NotificationEmailer updateNotificationEmail(NotificationEmailer userFE, String payId) {
		notificationEmailerDB = userDao.findByEmailerByPayId(payId);
		if(notificationEmailerDB ==null){
			userFE.setPayId(payId);
			userDao.createEmailerFalg(userFE);
		}else{
		notificationEmailerDB.setTransactionEmailerFlag(userFE.isTransactionEmailerFlag());
		notificationEmailerDB.setRefundTransactionCustomerEmailFlag(userFE.isRefundTransactionCustomerEmailFlag());
		notificationEmailerDB.setTransactionCustomerEmailFlag(userFE.isTransactionCustomerEmailFlag());
		notificationEmailerDB.setRefundTransactionMerchantEmailFlag(userFE.isRefundTransactionMerchantEmailFlag());
		notificationEmailerDB.setSendMultipleEmailer(userFE.getSendMultipleEmailer());
		notificationEmailerDB.setTransactionAuthenticationEmailFlag(userFE.isTransactionAuthenticationEmailFlag());
		notificationEmailerDB.setTransactionCustomerEmailFlag(userFE.isTransactionCustomerEmailFlag());
		notificationEmailerDB.setTransactionSmsFlag(userFE.isTransactionSmsFlag());
		notificationEmailerDB.setSurchargeFlag(userFE.isSurchargeFlag());
		notificationEmailerDB.setExpressPayFlag(userFE.isExpressPayFlag());
		notificationEmailerDB.setIframePaymentFlag(userFE.isIframePaymentFlag());
		notificationEmailerDB.setMerchantHostedFlag(userFE.isMerchantHostedFlag());
		userDao.updateNotificationEamiler(notificationEmailerDB);
		
		}
		return notificationEmailerDB;
	}

	/*public NotificationEmailer updateNotificationEmail(NotificationEmailer[] userFE, String payId) {
		notificationEmailerDB = userDao.findByEmailerByPayId(payId);
		if(notificationEmailerDB ==null){
			for (NotificationEmailer notificationEmailer : userFE) {
				notificationEmailer.setPayId(payId);
				userDao.createEmailerFalg(userFE);
			}
		}else{
			for (NotificationEmailer notificationEmailer : userFE) {
				notificationEmailerDB.setTransactionEmailerFlag(notificationEmailer.isTransactionEmailerFlag());
				notificationEmailerDB.setRefundTransactionCustomerEmailFlag(notificationEmailer.isRefundTransactionCustomerEmailFlag());
				notificationEmailerDB.setTransactionCustomerEmailFlag(notificationEmailer.isTransactionCustomerEmailFlag());
				notificationEmailerDB.setSurchargeFlag(notificationEmailer.isSurchargeFlag());
				userDao.updateNotificationEamiler(notificationEmailerDB);
			}
		}
		return notificationEmailerDB;
	}*/

}
