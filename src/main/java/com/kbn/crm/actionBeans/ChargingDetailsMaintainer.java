package com.kbn.crm.actionBeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.ChargingDetailsDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.commons.util.TransactionType;

public class ChargingDetailsMaintainer {

	public ChargingDetailsMaintainer() {

	}

	public void editChargingDetail(String emailId, String acquirer,
			ChargingDetails chargingDetail,boolean isPermissionGranted , String userType, String loginUserEmailId) throws SystemException {
		UserDao userDao = new UserDao();
		Session session = null;
		ChargingDetailsFactory chargingDetailProvider = new ChargingDetailsFactory();
		try {
			User user = userDao.find(emailId);
			Account account = user.getAccountUsingAcquirerCode(acquirer);
			if (null == account) {
				throw new SystemException(ErrorType.ACQUIRER_NOT_FOUND,
						ErrorType.ACQUIRER_NOT_FOUND.getResponseMessage());
			}

			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			session.load(account, account.getId());

			ChargingDetails chargingDetailFromDb = chargingDetailProvider
					.getSingleChargingDetail(account, chargingDetail.getId());

			if (null == chargingDetailFromDb) {
				throw new SystemException(ErrorType.CHARGINGDETAIL_NOT_FETCHED,
						ErrorType.CHARGINGDETAIL_NOT_FETCHED
								.getResponseMessage());
			}
			
			if (!(chargingDetailFromDb.getMerchantTDR() == 0.0) || !(chargingDetailFromDb.getMerchantTDRAFC() == 0.0)
					|| !(chargingDetailFromDb.getMerchantFixCharge() == 0.0)
					|| !(chargingDetailFromDb.getMerchantFixChargeAFC() == 0.0)) {
				
				//deactivate current and add new charging details
				editExistingChargingDetails(account, chargingDetailFromDb,  chargingDetail ,isPermissionGranted ,userType, loginUserEmailId);
				session.saveOrUpdate(account);
				tx.commit();
			}else{ // Edit blank charging detail
				chargingDetail = editBlankChargingDetails(account, chargingDetailFromDb, chargingDetail , isPermissionGranted , userType, loginUserEmailId);
				new ChargingDetailsDao().update(chargingDetail);
			}
		} finally {
			HibernateSessionProvider.closeSession(session);
		}
	}

	//TODO.. refactor 
	public void editAllChargingDetails(String userEmail, String acquirer,
			ChargingDetails newChargingDetails , boolean isPermissionGranted , String userType) throws SystemException {
		UserDao userDao = new UserDao();
		Session session = null;
		Date currentDate = new Date();
		try {
			User user = userDao.find(userEmail);
			Account account = user.getAccountUsingAcquirerCode(acquirer);
			if (null == account) {
				throw new SystemException(ErrorType.ACQUIRER_NOT_FOUND,
						ErrorType.ACQUIRER_NOT_FOUND.getResponseMessage());
			}
			session = HibernateSessionProvider.getSession();
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			session.load(account, account.getId());
			Set<ChargingDetails> chargingDetailList = account
					.getChargingDetails();
			List<ChargingDetails> newChargingDetailsList = new ArrayList<ChargingDetails>();
			for (ChargingDetails chargingDetailFromDb : chargingDetailList) {
				if (chargingDetailFromDb.getStatus().getName().equals(TDRStatus.ACTIVE.getName())
						&& chargingDetailFromDb.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())) {
					// edit only net-banking charging details
				/*	if (!(chargingDetailFromDb.getMerchantTDR() == 0.0)) { //deactivate current and add new charging details
					//	editExistingChargingDetails(account, chargingDetailFromDb,  newChargingDetails);
*/						
					if (!(chargingDetailFromDb.getMerchantTDR() == 0.0)
							|| !(chargingDetailFromDb.getMerchantTDRAFC() == 0.0)
							|| !(chargingDetailFromDb.getMerchantFixCharge() == 0.0)
							|| !(chargingDetailFromDb.getMerchantFixChargeAFC() == 0.0)) { 
						
						if (userType.equals(UserType.ADMIN.toString()) || isPermissionGranted){
							chargingDetailFromDb.setStatus(TDRStatus.INACTIVE);
							chargingDetailFromDb.setUpdatedDate(currentDate);
						}

						ChargingDetails newChargingDetail = SerializationUtils.clone(newChargingDetails);
						newChargingDetail.setAcquirerName(chargingDetailFromDb
								.getAcquirerName());
						newChargingDetail.setPayId(chargingDetailFromDb.getPayId());
						newChargingDetail.setPgServiceTax(newChargingDetails
								.getMerchantServiceTax());
						newChargingDetail.setBankServiceTax(newChargingDetails
								.getMerchantServiceTax());
						
						if (userType.equals(UserType.ADMIN.toString()) || isPermissionGranted){
							newChargingDetail.setStatus(TDRStatus.ACTIVE);
						}
						else{
							newChargingDetail.setStatus(TDRStatus.PENDING);
						}
						newChargingDetail.setCreatedDate(currentDate);
						newChargingDetail.setMopType(chargingDetailFromDb.getMopType());
						newChargingDetail.setId(null);
						newChargingDetailsList.add(newChargingDetail);
					}else{// Edit blank charging detail
						chargingDetailFromDb.setAllowFixCharge(newChargingDetails.isAllowFixCharge());
						chargingDetailFromDb.setFixChargeLimit(newChargingDetails.getFixChargeLimit());
						chargingDetailFromDb.setBankFixCharge(newChargingDetails.getBankFixCharge());
						chargingDetailFromDb.setBankFixChargeAFC(newChargingDetails.getBankFixChargeAFC());
						chargingDetailFromDb.setBankServiceTax(newChargingDetails.getBankServiceTax());
						chargingDetailFromDb.setBankTDR(newChargingDetails.getBankTDR());
						chargingDetailFromDb.setBankTDRAFC(newChargingDetails.getBankTDRAFC());
						chargingDetailFromDb.setMerchantFixCharge(newChargingDetails.getMerchantFixCharge());
						chargingDetailFromDb.setMerchantFixChargeAFC(newChargingDetails.getMerchantFixChargeAFC());
						chargingDetailFromDb.setMerchantServiceTax(newChargingDetails.getMerchantServiceTax());
						chargingDetailFromDb.setMerchantTDR(newChargingDetails.getMerchantTDR());
						chargingDetailFromDb.setMerchantTDRAFC(newChargingDetails.getMerchantTDRAFC());
						chargingDetailFromDb.setPgFixCharge(newChargingDetails.getPgFixCharge());
						chargingDetailFromDb.setPgFixChargeAFC(newChargingDetails.getPgChargeAFC());
						chargingDetailFromDb.setPgServiceTax(newChargingDetails.getPgServiceTax());
						chargingDetailFromDb.setPgTDR(newChargingDetails.getPgTDR());
						chargingDetailFromDb.setPgTDRAFC(newChargingDetails.getPgTDRAFC());
						chargingDetailFromDb.setCreatedDate(new Date());
					}
				}
			}
			for(ChargingDetails newChargingDetailFromList:newChargingDetailsList){
				account.addChargingDetail(newChargingDetailFromList);
			}
			session.saveOrUpdate(account);
			tx.commit();
		} finally {
			HibernateSessionProvider.closeSession(session);
		}
	}

	public void editExistingChargingDetails(Account account, ChargingDetails chargingDetailFromDb, ChargingDetails newChargingDetails , boolean isPermissionGranted
			, String userType, String loginUserEmailId){
		Date currentDate = new Date();

		if (!(chargingDetailFromDb.getMerchantTDR() == 0.0)) {
			
			if (userType.equals(UserType.ADMIN.toString()) || isPermissionGranted){
				
				chargingDetailFromDb.setStatus(TDRStatus.INACTIVE);
				chargingDetailFromDb.setUpdatedDate(currentDate);
				chargingDetailFromDb.setUpdateBy(loginUserEmailId);
			}
			
			ChargingDetails pendingChargingDetail = new ChargingDetails();
			ChargingDetailsDao chargingDetailsDao = new ChargingDetailsDao();
			
			pendingChargingDetail = chargingDetailsDao.findPendingChargingDetail(chargingDetailFromDb.getMopType() , chargingDetailFromDb.getPaymentType() , chargingDetailFromDb.getTransactionType()
					, chargingDetailFromDb.getAcquirerName() , chargingDetailFromDb.getCurrency() , chargingDetailFromDb.getPayId());
			
			if (null != pendingChargingDetail){
				Session session = null;
				if (pendingChargingDetail != null) {
					Long id = pendingChargingDetail.getId();
					session = HibernateSessionProvider.getSession();
					Transaction tx = session.beginTransaction();
					session.load(pendingChargingDetail, pendingChargingDetail.getId());
					try {
						ChargingDetails pendingChargingDetails = (ChargingDetails) session.get(ChargingDetails.class, id);
						pendingChargingDetails.setStatus(TDRStatus.CANCELLED);
						pendingChargingDetails.setUpdatedDate(currentDate);
						pendingChargingDetail.setUpdateBy(loginUserEmailId);
						session.update(pendingChargingDetails);
						tx.commit();
					} catch (HibernateException e) {
						if (tx != null)
							tx.rollback();
						e.printStackTrace();
					} finally {
						session.close();
					}
				}
			}
			
			ChargingDetails newChargingDetail = SerializationUtils.clone(newChargingDetails);
			newChargingDetail.setAcquirerName(chargingDetailFromDb
					.getAcquirerName());
			newChargingDetail.setPayId(chargingDetailFromDb.getPayId());
			newChargingDetail.setPgServiceTax(newChargingDetails
					.getMerchantServiceTax());
			newChargingDetail.setBankServiceTax(newChargingDetails
					.getMerchantServiceTax());
			
			if (userType.equals(UserType.ADMIN.toString()) || isPermissionGranted){
				newChargingDetail.setStatus(TDRStatus.ACTIVE);
			}
			else{
				newChargingDetail.setStatus(TDRStatus.PENDING);
			}
			newChargingDetail.setCreatedDate(currentDate);
			newChargingDetail.setId(null);
			newChargingDetail.setUpdateBy(loginUserEmailId);
			account.addChargingDetail(newChargingDetail);

		} else { // Edit current detail
			newChargingDetails.setAcquirerName(chargingDetailFromDb.getAcquirerName());
			newChargingDetails.setPayId(chargingDetailFromDb.getPayId());
			newChargingDetails.setPgServiceTax(newChargingDetails.getMerchantServiceTax());
			newChargingDetails.setBankServiceTax(newChargingDetails.getMerchantServiceTax());
			if (userType.equals(UserType.ADMIN.toString()) || isPermissionGranted){
				newChargingDetails.setStatus(TDRStatus.ACTIVE);
			}
			else{
				newChargingDetails.setStatus(TDRStatus.PENDING);
			}
			newChargingDetails.setCreatedDate(currentDate);
		//	account.removeChargingDetail(chargingDetailFromDb);
			account.addChargingDetail(newChargingDetails);
			//chargingDetailFromDb = newChargingDetails;
		}
	}

	public ChargingDetails editBlankChargingDetails(Account account, ChargingDetails chargingDetailFromDb, ChargingDetails newChargingDetails , boolean isPermissionGranted,
			String userType, String loginUserEmailId){
		Date currentDate = new Date();
		newChargingDetails.setAcquirerName(chargingDetailFromDb.getAcquirerName());
		newChargingDetails.setPayId(chargingDetailFromDb.getPayId());
		newChargingDetails.setPgServiceTax(newChargingDetails.getMerchantServiceTax());
		newChargingDetails.setBankServiceTax(newChargingDetails.getMerchantServiceTax());
		if (userType.equals(UserType.ADMIN.toString()) || isPermissionGranted){
			newChargingDetails.setStatus(TDRStatus.ACTIVE);
		}
		else{
			newChargingDetails.setStatus(TDRStatus.PENDING);
		}
		newChargingDetails.setCreatedDate(currentDate);
		newChargingDetails.setUpdateBy(loginUserEmailId);
		return newChargingDetails;
	}

	public ChargingDetails createChargingDetail(String acquirerName,
			String payId, String token, String currencyCode) {

		ChargingDetails chargingDetail = new ChargingDetails();
		String[] splittedToken = token.split("-");

		chargingDetail.setAcquirerName(acquirerName);
		chargingDetail.setPayId(payId);
		chargingDetail.setMopType(MopType.getmop(splittedToken[1]));
		chargingDetail.setPaymentType(PaymentType.getInstance(splittedToken[0]));
		chargingDetail.setStatus(TDRStatus.ACTIVE);
		chargingDetail.setCurrency(currencyCode);
		if (splittedToken.length == 3) {
			chargingDetail.setTransactionType(TransactionType.getInstanceFromCode(splittedToken[2]));
		}

		return chargingDetail;
	}

	public ChargingDetails createChargingDetail(PaymentType paymentType,
			MopType mopType, String acquirerName, String payId,
			String accountCurrencyCode) {
		ChargingDetails newChargingDetails = new ChargingDetails();

		newChargingDetails.setPaymentType(paymentType);
		newChargingDetails.setMopType(mopType);
		newChargingDetails.setAcquirerName(acquirerName);
		newChargingDetails.setPayId(payId);
		newChargingDetails.setStatus(TDRStatus.ACTIVE);
		newChargingDetails.setCurrency(accountCurrencyCode);

		return newChargingDetails;
	}

	public void updateServiceTax(ChargingDetails oldChargingDetail, double newServiceTax){
		Date currentDate = new Date();

		ChargingDetails newChargingDetail = SerializationUtils.clone(oldChargingDetail);
		//create new TDR
		newChargingDetail.setPgServiceTax(newServiceTax);
		newChargingDetail.setBankServiceTax(newServiceTax);
		newChargingDetail.setMerchantServiceTax(newServiceTax);
		newChargingDetail.setStatus(TDRStatus.ACTIVE);
		newChargingDetail.setCreatedDate(currentDate);
		newChargingDetail.setId(null);
		//deactivate old TDR
		oldChargingDetail.setStatus(TDRStatus.INACTIVE);
		oldChargingDetail.setUpdatedDate(currentDate);
	}

	public void editAllWalletChargingDetails(String userEmail, String acquirer, ChargingDetails newChargingDetails) throws SystemException {
			UserDao userDao = new UserDao();
			Session session = null;
			Date currentDate = new Date();
			try {
				User user = userDao.find(userEmail);
				Account account = user.getAccountUsingAcquirerCode(acquirer);
				if (null == account) {
					throw new SystemException(ErrorType.ACQUIRER_NOT_FOUND,
							ErrorType.ACQUIRER_NOT_FOUND.getResponseMessage());
				}
				session = HibernateSessionProvider.getSession();
				session = HibernateSessionProvider.getSession();
				Transaction tx = session.beginTransaction();
				session.load(account, account.getId());
				Set<ChargingDetails> chargingDetailList = account
						.getChargingDetails();
				List<ChargingDetails> newChargingDetailsList = new ArrayList<ChargingDetails>();
				for (ChargingDetails chargingDetailFromDb : chargingDetailList) {
					if (chargingDetailFromDb.getStatus().getName().equals(TDRStatus.ACTIVE.getName())
							&& (chargingDetailFromDb.getPaymentType().getCode().equals(PaymentType.WALLET.getCode()))) {
						// edit only wallet charging details
						//deactivate current and add new charging details
						// GIT Issue #43(TDR updation issue), previous TDRs were not updating as Inactive, added 3 more conditions other than getMerchantTDR() == 0.0
						// Bug fixed on 20/09/2018, Developer-neeraj
						if (!(chargingDetailFromDb.getMerchantTDR() == 0.0) || !(chargingDetailFromDb.getMerchantTDRAFC() == 0.0) || !(chargingDetailFromDb.getMerchantFixCharge() == 0.0)
								|| !(chargingDetailFromDb.getMerchantFixChargeAFC() == 0.0)) { 
						//	editExistingChargingDetails(account, chargingDetailFromDb,  newChargingDetails);
							chargingDetailFromDb.setStatus(TDRStatus.INACTIVE);
							chargingDetailFromDb.setUpdatedDate(currentDate);

							ChargingDetails newChargingDetail = SerializationUtils.clone(newChargingDetails);
							newChargingDetail.setAcquirerName(chargingDetailFromDb
									.getAcquirerName());
							newChargingDetail.setPayId(chargingDetailFromDb.getPayId());
						
							newChargingDetail.setBankServiceTax(newChargingDetails
									.getMerchantServiceTax());
							// For Reseller Share Fields
						/*	newChargingDetail.setResellerBaseRate(newChargingDetails.getResellerBaseRate());
							newChargingDetail.setResellerBaseRateAFC(newChargingDetails.getResellerBaseRateAFC());
							newChargingDetail.setRollingReserveBaseRate(newChargingDetails.getRollingReserveBaseRate());
							newChargingDetail.setRollingReserveRateAFC(newChargingDetails.getRollingReserveRateAFC());*/
							newChargingDetail.setStatus(TDRStatus.ACTIVE);
							newChargingDetail.setCreatedDate(currentDate);
							newChargingDetail.setMopType(chargingDetailFromDb.getMopType());
							newChargingDetail.setId(null);
							newChargingDetailsList.add(newChargingDetail);
						}else{// Edit blank charging detail
							chargingDetailFromDb.setAllowFixCharge(newChargingDetails.isAllowFixCharge());
							chargingDetailFromDb.setFixChargeLimit(newChargingDetails.getFixChargeLimit());
							chargingDetailFromDb.setBankFixCharge(newChargingDetails.getBankFixCharge());
							chargingDetailFromDb.setBankFixChargeAFC(newChargingDetails.getBankFixChargeAFC());
							chargingDetailFromDb.setBankServiceTax(newChargingDetails.getBankServiceTax());
							chargingDetailFromDb.setBankTDR(newChargingDetails.getBankTDR());
							chargingDetailFromDb.setBankTDRAFC(newChargingDetails.getBankTDRAFC());
							chargingDetailFromDb.setMerchantFixCharge(newChargingDetails.getMerchantFixCharge());
							chargingDetailFromDb.setMerchantFixChargeAFC(newChargingDetails.getMerchantFixChargeAFC());
							chargingDetailFromDb.setMerchantServiceTax(newChargingDetails.getMerchantServiceTax());
							chargingDetailFromDb.setMerchantTDR(newChargingDetails.getMerchantTDR());
							chargingDetailFromDb.setMerchantTDRAFC(newChargingDetails.getMerchantTDRAFC());
							chargingDetailFromDb.setPgFixCharge(newChargingDetails.getPgFixCharge());
							chargingDetailFromDb.setPgFixChargeAFC(newChargingDetails.getPgChargeAFC());
							chargingDetailFromDb.setPgServiceTax(newChargingDetails.getPgServiceTax());
							chargingDetailFromDb.setPgTDR(newChargingDetails.getPgTDR());
							chargingDetailFromDb.setPgTDRAFC(newChargingDetails.getPgTDRAFC());
							chargingDetailFromDb.setCreatedDate(new Date());
							// For Reseller Share Fields
							/*chargingDetailFromDb.setResellerBaseRate(newChargingDetails.getResellerBaseRate());
							chargingDetailFromDb.setResellerBaseRateAFC(newChargingDetails.getResellerBaseRateAFC());
							chargingDetailFromDb.setRollingReserveBaseRate(newChargingDetails.getRollingReserveBaseRate());
							chargingDetailFromDb.setRollingReserveRateAFC(newChargingDetails.getRollingReserveRateAFC());*/
						}
					}
				}//for
				for(ChargingDetails newChargingDetailFromList:newChargingDetailsList){
					account.addChargingDetail(newChargingDetailFromList);
				}
				session.saveOrUpdate(account);
				tx.commit();
			} finally {
				HibernateSessionProvider.closeSession(session);
			}
		}
		
	}

