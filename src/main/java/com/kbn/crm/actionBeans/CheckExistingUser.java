package com.kbn.crm.actionBeans;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;

/**
 * @author Puneet
 * 
 */

public class CheckExistingUser {
	private UserDao userDao = new UserDao();
	private User checkedUser = new User();
	private ResponseObject responseObject= new ResponseObject();
	
     public ResponseObject checkuser(String emailId) {
			checkedUser = userDao.find(emailId);
			if (null != checkedUser){
				responseObject.setResponseCode(ErrorType.USER_UNAVAILABLE.getResponseCode());
				responseObject.setResponseMessage(ErrorType.USER_UNAVAILABLE.getResponseMessage());
			 } else {
				
				responseObject.setResponseCode(ErrorType.USER_AVAILABLE.getResponseCode());
				responseObject.setResponseMessage(ErrorType.USER_AVAILABLE.getResponseMessage());
				}
			return responseObject;
		}
}
