package com.kbn.crm.actionBeans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kbn.commons.user.Surcharge;
import com.kbn.commons.user.SurchargeDao;
import com.kbn.commons.user.SurchargeDetails;
import com.kbn.commons.user.SurchargeDetailsDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.pg.core.AcquirerType;

public class SurchargeAcquirerDetailsFactory {

	public Map<String, List<Surcharge>> getSurchargeAcquirerDetails(String payId, String paymentTypeName) {

		BigDecimal surchargeAmount = BigDecimal.ZERO;
		BigDecimal surchargePercentage = BigDecimal.ZERO;

		SurchargeDetails surchargeDetail = null;
		User user = null;
		surchargeDetail = new SurchargeDetailsDao().findDetails(payId, paymentTypeName);
		user = new UserDao().findPayId(payId);

		if (surchargeDetail != null) {
			surchargeAmount = surchargeDetail.getSurchargeAmount();
			surchargePercentage = surchargeDetail.getSurchargePercentage();
		}

		List<Surcharge> acquirerDetails = new ArrayList<Surcharge>();

		acquirerDetails = new SurchargeDao().findSurchargeListByPayid(payId, paymentTypeName);

		Map<String, List<Surcharge>> detailsMap = new HashMap<String, List<Surcharge>>();

		for (AcquirerType acquirerType : AcquirerType.values()) {
			List<Surcharge> surchargeDetailsList = new ArrayList<Surcharge>();
			String acquirerName = acquirerType.getName();
			for (Surcharge acquirerDetail : acquirerDetails) {

				if (acquirerDetail.getAcquirerName().equals(acquirerName)) {

					acquirerDetail
							.setPgSurchargeAmount(surchargeAmount.subtract(acquirerDetail.getBankSurchargeAmount()));
					acquirerDetail.setPgSurchargePercentage(
							surchargePercentage.subtract(acquirerDetail.getBankSurchargePercentage()));
					acquirerDetail.setMerchantIndustryType(user.getIndustryCategory());
					acquirerDetail.setMerchantSurchargeAmount(surchargeAmount);
					acquirerDetail.setMerchantSurchargePercentage(surchargePercentage);
					if (acquirerDetail.getOnOff().equalsIgnoreCase("2")) {
						acquirerDetail.setOnOff("OFF US");
					} else if (acquirerDetail.getOnOff().equalsIgnoreCase("1")) {
						acquirerDetail.setOnOff("ON US");
					} else {
						acquirerDetail.setOnOff("NONE");
					}

					surchargeDetailsList.add(acquirerDetail);
				}
			}
			if (surchargeDetailsList.size() != 0) {
				detailsMap.put(acquirerName, surchargeDetailsList);
			}
		}

		return detailsMap;
	}

}
