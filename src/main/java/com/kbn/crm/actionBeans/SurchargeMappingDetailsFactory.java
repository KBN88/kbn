package com.kbn.crm.actionBeans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.ChargingDetailsDao;
import com.kbn.commons.user.Surcharge;
import com.kbn.commons.user.SurchargeDao;
import com.kbn.commons.user.SurchargeMappingPopulator;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.pg.core.AcquirerType;

public class SurchargeMappingDetailsFactory {

	public Map<String, List<SurchargeMappingPopulator>> getSurchargeAcquirerDetails(String payId, PaymentType paymentType) {

		User user = null;
	    user = new UserDao().findPayId(payId);
	    SurchargeDao surchargeDao = new SurchargeDao();
	    List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
	    chargingDetailsList = new ChargingDetailsDao().getAllActiveSaleChargingDetails(payId , paymentType);
	    
	    List<SurchargeMappingPopulator> acquirerDetails1 = new ArrayList<SurchargeMappingPopulator>();
	    
	    for (ChargingDetails chargingDetail : chargingDetailsList){
	    	
		    SurchargeMappingPopulator surchargeMappingPopulator = new SurchargeMappingPopulator();

	    	if(surchargeDao.isChargingDetailsMappedWithSurcharge(chargingDetail.getPayId(),chargingDetail.getAcquirerName(),
	    			chargingDetail.getMopType(),chargingDetail.getPaymentType())){
	    	
	    		List<Surcharge> surchargeList = surchargeDao.findDetailsOnUsOffUs(chargingDetail.getPayId(),chargingDetail.getAcquirerName(),
	    				chargingDetail.getMopType(),chargingDetail.getPaymentType());
	    		
	    		if (surchargeList.size() == 1){
	    			
	    			for(Surcharge surcharge:surchargeList ){
	    			
	    			surchargeMappingPopulator.setAcquirerName(surcharge.getAcquirerName());
		    		surchargeMappingPopulator.setMopType(surcharge.getMopType().getName());
		    		surchargeMappingPopulator.setPaymentType(surcharge.getPaymentType().getName());
		    		surchargeMappingPopulator.setStatus(surcharge.getStatus().getName());
		    		surchargeMappingPopulator.setBankSurchargeAmountOff(BigDecimal.ZERO);
		    		surchargeMappingPopulator.setBankSurchargeAmountOn(surcharge.getBankSurchargeAmount());
		    		surchargeMappingPopulator.setBankSurchargePercentageOff(BigDecimal.ZERO);
		    		surchargeMappingPopulator.setBankSurchargePercentageOn(surcharge.getBankSurchargePercentage());
		    		surchargeMappingPopulator.setAllowOnOff(false);
		    		
		    		acquirerDetails1.add(surchargeMappingPopulator);
	    			}
	    			
	    		}
	    		else{
	    			
	    			for(Surcharge surcharge:surchargeList ){
	    				if (surcharge.getOnOff().equalsIgnoreCase("1")){
	    					
	    					surchargeMappingPopulator.setAcquirerName(surcharge.getAcquirerName());
	    		    		surchargeMappingPopulator.setMopType(surcharge.getMopType().getName());
	    		    		surchargeMappingPopulator.setPaymentType(surcharge.getPaymentType().getName());
	    		    		surchargeMappingPopulator.setStatus(surcharge.getStatus().getName());
	    		    		surchargeMappingPopulator.setBankSurchargeAmountOn(surcharge.getBankSurchargeAmount());
	    		    		surchargeMappingPopulator.setBankSurchargePercentageOn(surcharge.getBankSurchargePercentage());
	    		    		surchargeMappingPopulator.setAllowOnOff(true);
	    				}
	    				else if (surcharge.getOnOff().equalsIgnoreCase("2")){
	    					
	    					surchargeMappingPopulator.setAcquirerName(surcharge.getAcquirerName());
	    		    		surchargeMappingPopulator.setMopType(surcharge.getMopType().getName());
	    		    		surchargeMappingPopulator.setPaymentType(surcharge.getPaymentType().getName());
	    		    		surchargeMappingPopulator.setStatus(surcharge.getStatus().getName());
	    		    		surchargeMappingPopulator.setBankSurchargeAmountOff(surcharge.getBankSurchargeAmount());
	    		    		surchargeMappingPopulator.setBankSurchargePercentageOff(surcharge.getBankSurchargePercentage());
	    		    		surchargeMappingPopulator.setAllowOnOff(true);
	    				}
	    				else{
	    					
	    					surchargeMappingPopulator.setBankSurchargeAmountOff(BigDecimal.ZERO);
	    					surchargeMappingPopulator.setBankSurchargePercentageOff(BigDecimal.ZERO);
	    					surchargeMappingPopulator.setAllowOnOff(false);
	    				}
	    				
	    				
	    			}
	    			acquirerDetails1.add(surchargeMappingPopulator);
	    			
	    		}
	    	}
	    	else{
	    		surchargeMappingPopulator.setAcquirerName(chargingDetail.getAcquirerName());
	    		surchargeMappingPopulator.setMopType(chargingDetail.getMopType().getName());
	    		surchargeMappingPopulator.setPaymentType(chargingDetail.getPaymentType().getName());
	    		surchargeMappingPopulator.setStatus(TDRStatus.INACTIVE.getName());
	    		surchargeMappingPopulator.setBankSurchargeAmountOff(BigDecimal.ZERO);
	    		surchargeMappingPopulator.setBankSurchargeAmountOn(BigDecimal.ZERO);
	    		surchargeMappingPopulator.setBankSurchargePercentageOff(BigDecimal.ZERO);
	    		surchargeMappingPopulator.setBankSurchargePercentageOn(BigDecimal.ZERO);
	    		surchargeMappingPopulator.setAllowOnOff(false);
	    		
	    		acquirerDetails1.add(surchargeMappingPopulator);
	    		
	    	}
	    	
	    }
	    
		Map<String, List<SurchargeMappingPopulator>> detailsMap = new HashMap<String, List<SurchargeMappingPopulator>>();
		
		for (AcquirerType acquirerType : AcquirerType.values()) {
			List<SurchargeMappingPopulator> surchargeDetailsList = new ArrayList<SurchargeMappingPopulator>();
			String acquirerName = acquirerType.getName();
			for (SurchargeMappingPopulator acquirerDetail : acquirerDetails1) {
				
				if (acquirerDetail.getAcquirerName().equals(acquirerName)) {
					
					acquirerDetail.setMerchantIndustryType(user.getIndustryCategory());
					
					surchargeDetailsList.add(acquirerDetail);
				}
			}
			if (surchargeDetailsList.size() != 0) {
				//Collections.sort(surchargeDetailsList);
				detailsMap.put(acquirerName, surchargeDetailsList);
			}
		}
		

		return detailsMap;
	}

}
