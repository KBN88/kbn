package com.kbn.crm.charging;

import java.util.List;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionHistory;

public interface TransactionDetailProvider {

	public  List<TransactionHistory> getAllTransactionsFromDb(String txnId)  throws SystemException;
	public void getCapturedTransactionsFromDb(String orderId, String txnId);
}
