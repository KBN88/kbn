package com.kbn.crm.charging;

import java.util.List;

import com.kbn.commons.dao.TransactionDetailsService;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionHistory;

public class DefaultTransactionDetailProvider implements TransactionDetailProvider{

	public DefaultTransactionDetailProvider(){
		
	}

	@Override
	public List<TransactionHistory> getAllTransactionsFromDb(String txnId) throws SystemException {
		
		TransactionDetailsService transactionService = new TransactionDetailsService();
		return transactionService.getTransaction(txnId);		
	}
	
	@Override
	public void getCapturedTransactionsFromDb(String ordereId, String txnId) {
		
		
		
	}
}
