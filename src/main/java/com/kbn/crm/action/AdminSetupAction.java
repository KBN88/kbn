package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.opensymphony.xwork2.ModelDriven;

public class AdminSetupAction extends AbstractSecureAction implements ModelDriven<User>{
	/**
	 * @ Neeraj
	 */
	private static final long serialVersionUID = 8870322503068475573L;
	private static Logger logger = Logger.getLogger(AdminSetupAction.class.getName());
	private User user = new User();
	
	public String execute() {
		UserDao userDao = new UserDao();
		try {
		  setUser(userDao.findPayId(user.getPayId()));
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;     
		}
	}
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public User getModel() {
	
		return user;
	}

}
