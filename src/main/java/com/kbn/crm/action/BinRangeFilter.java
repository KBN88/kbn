package com.kbn.crm.action;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.BinRangeMopType;
import com.kbn.commons.util.CardsType;
import com.kbn.commons.util.CrmFieldConstants;

public class BinRangeFilter {

	public String getPaymentType(User sessionUser, String cardType) {
		if (sessionUser.getUserType().equals(UserType.ADMIN) || sessionUser.getUserType().equals(UserType.SUBADMIN)) {
			if (cardType.equals(CrmFieldConstants.ALL.toString())) {
				return CrmFieldConstants.ALL.toString();
			} else {
				CardsType cardsType = CardsType.getInstance(cardType);
				return cardsType.toString();
			}

		}
		return null;
	}

	public String getMopType(User sessionUser, String mopType) {
		if (sessionUser.getUserType().equals(UserType.ADMIN) || sessionUser.getUserType().equals(UserType.SUBADMIN)) {
			if (mopType.equals(CrmFieldConstants.ALL.toString())) {
				return CrmFieldConstants.ALL.toString();
			} else {
				BinRangeMopType binRangeMopType = BinRangeMopType.getInstance(mopType);
				return binRangeMopType.toString();
			}
		}
		return null;

	}
}
