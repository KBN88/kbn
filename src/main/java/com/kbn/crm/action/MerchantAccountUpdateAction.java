package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.ChargingDetailsDao;
import com.kbn.commons.user.MerchantComments;
import com.kbn.commons.user.MerchantCommentsDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.BinCountryMapperType;
import com.kbn.commons.util.BusinessType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.ModeType;
import com.kbn.commons.util.SaltFactory;
import com.kbn.commons.util.TDRStatus;
import com.kbn.commons.util.UserStatusType;
import com.kbn.crm.actionBeans.CurrencyMapProvider;
import com.kbn.crm.actionBeans.MerchantRecordUpdater;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.AcquirerType;
import com.opensymphony.xwork2.ModelDriven;

/**
 * @author  Neeraj
 *
 */
public class MerchantAccountUpdateAction extends AbstractSecureAction
		implements ServletRequestAware, ModelDriven<User> {

	private static Logger logger = Logger.getLogger(MerchantAccountUpdateAction.class.getName());
	private static final long serialVersionUID = -7290087594947995464L;
	private User user = new User();
	private ChargingDetailsDao chargingDetailsDao = new ChargingDetailsDao();
	private List<Account> accountList = new ArrayList<Account>();
	private Map<String, List<AccountCurrency>> accountCurrencyMap = new HashMap<String, List<AccountCurrency>>();
	private List<AccountCurrency> accountCurrencyList = new ArrayList<AccountCurrency>();
	private String salt;
	private HttpServletRequest request;
	private String defaultCurrency;
	private Map<String, String> currencyMap = new LinkedHashMap<String, String>();
	private Map<String, String> industryTypes = new LinkedHashMap<String, String>();

	public String saveAction() {
		Date date = new Date();

		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

		MerchantRecordUpdater merchantRecordUpdater = new MerchantRecordUpdater();
		AccountPasswordScrambler accPwdScrambler = new AccountPasswordScrambler();
		try {
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			setSalt(SaltFactory.getSaltProperty(user));
			setIndustryTypes(BusinessType.getIndustryCategoryList());
			if (sessionUser.getUserType().equals(UserType.ADMIN)
					|| sessionUser.getUserType().equals(UserType.SUBADMIN)) {
				CurrencyMapProvider currencyMapProvider = new CurrencyMapProvider();
					if (user.getUserStatus().toString().equals(UserStatusType.ACTIVE.getStatus().toString())) {
					user.setActivationDate(date);
					if (!chargingDetailsDao.isChargingDetailsSet(user.getPayId())) {
						addActionMessage(CrmFieldConstants.USER_CHARGINGDETAILS_NOT_SET_MSG.getValue());
						return CrmFieldConstants.ADMIN.getValue();
					}
				} else if (user.getUserStatus().toString().equals(UserStatusType.SUSPENDED.getStatus().toString())
						|| user.getUserStatus().toString()
								.equals(UserStatusType.TRANSACTION_BLOCKED.getStatus().toString())) {
					user.setActivationDate(null);
				}

				Map<String, User> tempMap = new HashMap<String, User>();
				tempMap = merchantRecordUpdater.updateUserPendingDetails(user, sessionUser, accountList,
						accountCurrencyList, permissions);
				for (Map.Entry<String, User> entry : tempMap.entrySet()) {
					setUser(entry.getValue());
					addActionMessage(entry.getKey());
				}
				setUser(accPwdScrambler.retrieveAndDecryptPass(user));
				currencyMap = currencyMapProvider.currencyMap(user);
				return CrmFieldConstants.ADMIN.getValue();
			} else {
				setUser(merchantRecordUpdater.updateUserProfile(user));
				sessionMap.put(Constants.USER.getValue(), user);
				return CrmFieldConstants.SIGNUP_PROFILE.getValue();
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	// to provide default country
	public String getDefaultCountry() {
		if (StringUtils.isBlank(user.getCountry())) {
			return BinCountryMapperType.INDIA.getName();
		} else {
			return user.getCountry();
		}
	}

	@SuppressWarnings("unused")
	private void insertComments(String comments, String emailId) {
		MerchantCommentsDao merchantCommentDao = new MerchantCommentsDao();

		MerchantComments comment = new MerchantComments();
		comment.setComment(comments);
		comment.setStatus(TDRStatus.ACTIVE);
		comment.setEmailId(emailId);
		comment.setCreatedDate(new Date());
		merchantCommentDao.create(comment);
	}

	public void validate() {
		// FSS
		boolean hdfcAcquirerFlag = false;
		boolean hdfcBankExistFlag = false;
		// CyberSource
		boolean cyberSourceAcquirerFlag = false;
		boolean cyberSourceExistFlag = false;
		// Directpay
		boolean direcpayAcquirerFlag = false;
		boolean direcpayNetbankingPrimaryFlag = false;
		boolean direcpayExistFlag = false;
		
		boolean modeTypeFlag = false;
		// Citrus yes Bank
		boolean yesBankNetbnkingPrimaryFlag = false;
		boolean yesBankAcquirerFlag = false;
		boolean yesBankExistFlag = false;
		// SafePay
		boolean safexPayAcquirerFlag = false;
		boolean safexPayExistFlag = false;
		boolean safexPayNetbankingPrimaryFlag = false;
		boolean safexPayWalletPrimaryFlag = false;
		// SBI
		boolean sbiBankAcquirerFlag = false;
		boolean sbiBankExistFlag = false;
		//boolean sbiBankNetbankingPrimaryFlag = false;
		// ISGPAY
		boolean isgPayAcquirerFlag = false;
		boolean isgPayExistFlag = false;
		boolean isgPayNetbankingPrimaryFlag = false;
		// Billdesk
		boolean billdeskAcquirerFlag = false;
		boolean billdeskExistFlag = false;
		boolean billdeskNetbankingPrimaryFlag = false;
		boolean billdeskWalletPrimaryFlag = false;
		// ATOM
		boolean atomAcquirerFlag = false;
		boolean atomExistFlag = false;
		boolean atomNetbankingPrimaryFlag = false;
		boolean atomWalletPrimaryFlag = false;


		CrmValidator crmValidator = new CrmValidator();
		if ((crmValidator.validateBlankField(user.getFirstName()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.FIRSTNAME, user.getFirstName()))) {
			addFieldError(CrmFieldType.FIRSTNAME.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getLastName()))) {

		} else if (!(crmValidator.validateField(CrmFieldType.LASTNAME, user.getLastName()))) {
			addFieldError(CrmFieldType.LASTNAME.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getCompanyName()))) {

		} else if (!(crmValidator.validateField(CrmFieldType.COMPANY_NAME, user.getCompanyName()))) {
			addFieldError(CrmFieldType.COMPANY_NAME.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if (crmValidator.validateBlankField(user.getIndustryCategory())) {
			addFieldError(CrmFieldType.INDUSTRY_CATEGORY.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		} else if (!(crmValidator.validateField(CrmFieldType.INDUSTRY_CATEGORY, user.getIndustrySubCategory()))) {
			addFieldError(CrmFieldType.INDUSTRY_CATEGORY.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		if (crmValidator.validateBlankField(user.getIndustrySubCategory())) {
			addFieldError(CrmFieldType.INDUSTRY_SUB_CATEGORY.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		} else if (!(crmValidator.validateField(CrmFieldType.INDUSTRY_SUB_CATEGORY, user.getIndustrySubCategory()))) {
			addFieldError(CrmFieldType.INDUSTRY_SUB_CATEGORY.getName(),
					ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getTelephoneNo()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.TELEPHONE_NO, user.getTelephoneNo()))) {
			addFieldError(CrmFieldType.TELEPHONE_NO.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getAddress()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.ADDRESS, user.getAddress()))) {
			addFieldError(CrmFieldType.ADDRESS.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getCity()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.CITY, user.getCity()))) {
			addFieldError(CrmFieldType.CITY.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getState()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.STATE, user.getState()))) {
			addFieldError(CrmFieldType.STATE.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getCountry()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.COUNTRY, user.getCountry()))) {
			addFieldError(CrmFieldType.COUNTRY.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getPostalCode()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.POSTALCODE, user.getPostalCode()))) {
			addFieldError(CrmFieldType.POSTALCODE.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getBankName()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.BANK_NAME, user.getBankName()))) {
			addFieldError(CrmFieldType.BANK_NAME.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getIfscCode()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.IFSC_CODE, user.getIfscCode()))) {
			addFieldError(CrmFieldType.IFSC_CODE.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getAccHolderName()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.ACC_HOLDER_NAME, user.getAccHolderName()))) {
			addFieldError(CrmFieldType.ACC_HOLDER_NAME.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getCurrency()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.CURRENCY, user.getCurrency()))) {
			addFieldError(CrmFieldType.CURRENCY.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getBranchName()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.BRANCH_NAME, user.getBranchName()))) {
			addFieldError(CrmFieldType.BRANCH_NAME.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getBusinessName()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.BUSINESS_NAME, user.getBusinessName()))) {
			addFieldError(CrmFieldType.BUSINESS_NAME.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getComments()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.COMMENTS, user.getComments()))) {
			addFieldError(CrmFieldType.COMMENTS.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getPanCard()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.PANCARD, user.getPanCard()))) {
			addFieldError(CrmFieldType.PANCARD.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getAccountNo()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.ACCOUNT_NO, user.getAccountNo()))) {
			addFieldError(CrmFieldType.ACCOUNT_NO.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getWebsite()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.WEBSITE, user.getWebsite()))) {
			addFieldError(CrmFieldType.WEBSITE.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getOrganisationType()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.ORGANIZATIONTYPE, user.getOrganisationType()))) {
			addFieldError(CrmFieldType.ORGANIZATIONTYPE.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getMultiCurrency()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.MULTICURRENCY, user.getMultiCurrency()))) {
			addFieldError(CrmFieldType.MULTICURRENCY.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getBusinessModel()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.BUSINESSMODEL, user.getBusinessModel()))) {
			addFieldError(CrmFieldType.BUSINESSMODEL.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getOperationAddress()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.OPERATIONADDRESS, user.getOperationAddress()))) {
			addFieldError(CrmFieldType.OPERATIONADDRESS.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getOperationCity()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.CITY, user.getOperationCity()))) {
			addFieldError(CrmFieldType.CITY.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getOperationState()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.STATE, user.getOperationState()))) {
			addFieldError(CrmFieldType.STATE.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getOperationPostalCode()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.OPERATION_POSTAL_CODE, user.getOperationPostalCode()))) {
			addFieldError(CrmFieldType.OPERATION_POSTAL_CODE.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getCin()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.CIN, user.getCin()))) {
			addFieldError(CrmFieldType.CIN.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getPan()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.PAN, user.getPan()))) {
			addFieldError(CrmFieldType.PAN.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getPanName()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.PANNAME, user.getPanName()))) {
			addFieldError(CrmFieldType.PANNAME.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getNoOfTransactions()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.NO_OF_TRANSACTIONS, user.getNoOfTransactions()))) {
			addFieldError(CrmFieldType.NO_OF_TRANSACTIONS.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}

		if ((crmValidator.validateBlankField(user.getAmountOfTransactions()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.AMOUNT_OF_TRANSACTIONS, user.getAmountOfTransactions()))) {
			addFieldError(CrmFieldType.AMOUNT_OF_TRANSACTIONS.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}

		if ((crmValidator.validateBlankField(user.getDateOfEstablishment()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.DATE_OF_ESTABLISHMENT, user.getDateOfEstablishment()))) {
			addFieldError(CrmFieldType.DATE_OF_ESTABLISHMENT.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}

		if ((crmValidator.validateBlankField(user.getAccountValidationKey()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.ACCOUNT_VALIDATION_KEY, user.getAccountValidationKey()))) {
			addFieldError(CrmFieldType.ACCOUNT_VALIDATION_KEY.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}

		if ((crmValidator.validateBlankField(user.getUploadePhoto()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.UPLOADE_PHOTO, user.getUploadePhoto()))) {
			addFieldError(CrmFieldType.UPLOADE_PHOTO.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getUploadedPanCard()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.UPLOADE_PAN_CARD, user.getUploadedPanCard()))) {
			addFieldError(CrmFieldType.UPLOADE_PAN_CARD.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getUploadedPhotoIdProof()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.UPLOADE_PHOTOID_PROOF, user.getUploadedPhotoIdProof()))) {
			addFieldError(CrmFieldType.UPLOADE_PHOTOID_PROOF.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getUploadedContractDocument()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.UPLOADE_CONTRACT_DOCUMENT,
				user.getUploadedContractDocument()))) {
			addFieldError(CrmFieldType.UPLOADE_CONTRACT_DOCUMENT.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}
		if (crmValidator.validateBlankField(user.getTransactionEmailId())) {
		} else if (!(crmValidator.isValidBatchEmailId(user.getTransactionEmailId())
				|| (crmValidator.isValidEmailId(user.getTransactionEmailId())))) {
			addFieldError(CrmFieldType.TRANSACTION_EMAIL_ID.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}

		if (crmValidator.validateBlankField(user.getEmailId())) {
			addFieldError(CrmFieldType.EMAILID.getName(), crmValidator.getResonseObject().getResponseMessage());
		} else if (!(crmValidator.isValidEmailId(user.getEmailId()))) {
			addFieldError(CrmFieldType.EMAILID.getName(), crmValidator.getResonseObject().getResponseMessage());
		}

		if ((crmValidator.validateBlankField(user.getContactPerson()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.CONTACT_PERSON, user.getContactPerson()))) {
			addFieldError(CrmFieldType.CONTACT_PERSON.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getPayId()))) {
			addFieldError(CrmFieldType.PAY_ID.getName(), crmValidator.getResonseObject().getResponseMessage());
		} else if (!(crmValidator.validateField(CrmFieldType.PAY_ID, user.getPayId()))) {
			addFieldError(CrmFieldType.PAY_ID.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getPassword()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.PASSWORD, user.getPassword()))) {
			addFieldError(CrmFieldType.PASSWORD.getName(), crmValidator.getResonseObject().getResponseMessage());
		}

		if ((crmValidator.validateBlankField(user.getParentPayId()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.PARENT_PAY_ID, user.getParentPayId()))) {
			addFieldError(CrmFieldType.PARENT_PAY_ID.getName(), crmValidator.getResonseObject().getResponseMessage());
		}

		if ((crmValidator.validateBlankField(user.getWhiteListIpAddress()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.WHITE_LIST_IPADDRES, user.getWhiteListIpAddress()))) {
			addFieldError(CrmFieldType.WHITE_LIST_IPADDRES.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getFax()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.FAX, user.getFax()))) {
			addFieldError(CrmFieldType.FAX.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getMobile()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.MOBILE, user.getMobile()))) {
			addFieldError(CrmFieldType.MOBILE.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getProductDetail()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.PRODUCT_DETAIL, user.getProductDetail()))) {
			addFieldError(CrmFieldType.PRODUCT_DETAIL.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getResellerId()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.RESELLER_ID, user.getResellerId()))) {
			addFieldError(CrmFieldType.RESELLER_ID.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getMerchantType()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.MERCHANT_TYPE, user.getMerchantType()))) {
			addFieldError(CrmFieldType.MERCHANT_TYPE.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getBranchName()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.BRANCH_NAME, user.getBranchName()))) {
			addFieldError(CrmFieldType.BRANCH_NAME.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getDefaultCurrency()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.DEFAULT_CURRENCY, user.getDefaultCurrency()))) {
			addFieldError(CrmFieldType.DEFAULT_CURRENCY.getName(),
					crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getMCC()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.MCC, user.getMCC()))) {
			addFieldError(CrmFieldType.MCC.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		if ((crmValidator.validateBlankField(user.getAmexSellerId()))) {
		} else if (!(crmValidator.validateField(CrmFieldType.AMEX_SELLER_ID, user.getAmexSellerId()))) {
			addFieldError(CrmFieldType.AMEX_SELLER_ID.getName(), crmValidator.getResonseObject().getResponseMessage());
		}
		for (Account accountFE : accountList) {
			if ((crmValidator.validateBlankField(accountFE.getMerchantId()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.MERCHANTID, accountFE.getMerchantId()))) {
				addFieldError(CrmFieldType.MERCHANTID.getName(), crmValidator.getResonseObject().getResponseMessage());
			}

			if ((crmValidator.validateBlankField(accountFE.getAcquirerPayId()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.ACQUIRER_PAYID, accountFE.getAcquirerPayId()))) {
				addFieldError(CrmFieldType.ACQUIRER_PAYID.getName(),
						crmValidator.getResonseObject().getResponseMessage());
			}
			if ((crmValidator.validateBlankField(accountFE.getAcquirerName()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.AQCQUIRER_NAME, accountFE.getAcquirerName()))) {
				addFieldError(CrmFieldType.AQCQUIRER_NAME.getName(),
						crmValidator.getResonseObject().getResponseMessage());
			}
			if ((crmValidator.validateBlankField(accountFE.getPassword()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.ACCOUNT_PASSWORD, accountFE.getPassword()))) {
				addFieldError(CrmFieldType.ACCOUNT_PASSWORD.getName(),
						crmValidator.getResonseObject().getResponseMessage());
			}
			if ((crmValidator.validateBlankField(accountFE.getTxnKey()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.TXN_KEY, accountFE.getTxnKey()))) {
				addFieldError(CrmFieldType.TXN_KEY.getName(), crmValidator.getResonseObject().getResponseMessage());
			}
		}
		for (AccountCurrency accountCurrency : accountCurrencyList) {
			if ((crmValidator.validateBlankField(accountCurrency.getAcqPayId()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.ACQ_PAYID, accountCurrency.getAcqPayId()))) {
				addFieldError(CrmFieldType.ACQ_PAYID.getName(), crmValidator.getResonseObject().getResponseMessage());
			}
			if ((crmValidator.validateBlankField(accountCurrency.getCurrencyCode()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.CURRENCY, accountCurrency.getCurrencyCode()))) {
				addFieldError(CrmFieldType.CURRENCY.getName(), crmValidator.getResonseObject().getResponseMessage());
			}
			if ((crmValidator.validateBlankField(accountCurrency.getMerchantId()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.MERCHANTID, accountCurrency.getMerchantId()))) {
				addFieldError(CrmFieldType.MERCHANTID.getName(), crmValidator.getResonseObject().getResponseMessage());
			}
			if ((crmValidator.validateBlankField(accountCurrency.getPassword()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.ACCOUNT_PASSWORD, accountCurrency.getPassword()))) {
				addFieldError(CrmFieldType.ACCOUNT_PASSWORD.getName(),
						crmValidator.getResonseObject().getResponseMessage());
			}
			if ((crmValidator.validateBlankField(accountCurrency.getTxnKey()))) {
			} else if (!(crmValidator.validateField(CrmFieldType.TXN_KEY, accountCurrency.getTxnKey()))) {
				addFieldError(CrmFieldType.TXN_KEY.getName(), crmValidator.getResonseObject().getResponseMessage());
			}
		}

		// Validation between check boxes
		for (Account accountFE : accountList) {
			//CITRUS
			if (accountFE.getAcquirerName().equalsIgnoreCase(AcquirerType.CITRUS_PAY.getName())) {
				yesBankExistFlag = true;
				if (accountFE.isPrimaryStatus() == true) {
					yesBankAcquirerFlag = true;
				}
				if (accountFE.isPrimaryNetbankingStatus() == true) {
					yesBankNetbnkingPrimaryFlag = true;
				}

				if (yesBankAcquirerFlag
						&& user.getModeType().getName().equalsIgnoreCase(ModeType.AUTH_CAPTURE.getName())) {
					modeTypeFlag = true;
				}
			}//FSS
			if (accountFE.getAcquirerName().equalsIgnoreCase(AcquirerType.FSS.getName())) {
				hdfcBankExistFlag = true;
				if (accountFE.isPrimaryStatus() == true) {
					hdfcAcquirerFlag = true;
				}
			}// DIRECT_PAY
			if (accountFE.getAcquirerName().equalsIgnoreCase(AcquirerType.DIREC_PAY.getName())) {
				direcpayExistFlag = true;
				if (accountFE.isPrimaryStatus() == true) {
					direcpayAcquirerFlag = true;
				}
				if (accountFE.isPrimaryNetbankingStatus() == true) {
					direcpayNetbankingPrimaryFlag = true;
				}
			}
			//  CyberSource	
			if (accountFE.getAcquirerName().equalsIgnoreCase(AcquirerType.CYBER_SOURCE.getName())) {
				cyberSourceExistFlag = true;
				if (accountFE.isPrimaryStatus() == true) {
					cyberSourceAcquirerFlag = true;
				}
			}// BIllDESK
			if (accountFE.getAcquirerName().equalsIgnoreCase(AcquirerType.BILLDESK.getName())) {
				billdeskExistFlag = true;
				if (accountFE.isPrimaryStatus() == true) {
					billdeskAcquirerFlag = true;
				}
				if (accountFE.isPrimaryNetbankingStatus() == true) {
					billdeskNetbankingPrimaryFlag = true;
				}if (accountFE.isPrimaryWalletStatus() == true) {
					billdeskWalletPrimaryFlag = true;
				}
			}//SafexPay
			if (accountFE.getAcquirerName().equalsIgnoreCase(AcquirerType.SAFEX_PAY.getName())) {
				safexPayExistFlag = true;
				if (accountFE.isPrimaryStatus() == true) {
					safexPayAcquirerFlag = true;
				}
				if (accountFE.isPrimaryNetbankingStatus() == true) {
					safexPayNetbankingPrimaryFlag = true;
				}if (accountFE.isPrimaryWalletStatus() == true) {
					safexPayWalletPrimaryFlag = true;
				}
			}
			// SBI 
			if (accountFE.getAcquirerName().equalsIgnoreCase(AcquirerType.SBI.getName())) {
				sbiBankExistFlag = true;
				if (accountFE.isPrimaryStatus() == true) {
					sbiBankAcquirerFlag = true;
				}
			/*	if (accountFE.isPrimaryNetbankingStatus() == true) {
					sbiBankNetbankingPrimaryFlag = true;
				}
*/
			}//ISGPAY
			if (accountFE.getAcquirerName().equalsIgnoreCase(AcquirerType.ISGPAY.getName())) {
				isgPayExistFlag = true;
				if (accountFE.isPrimaryStatus() == true) {
					isgPayAcquirerFlag = true;
				}
				if (accountFE.isPrimaryNetbankingStatus() == true) {
					isgPayNetbankingPrimaryFlag = true;
				}

			}
			//ATOM
			if (accountFE.getAcquirerName().equalsIgnoreCase(AcquirerType.ATOM.getName())) {
				atomExistFlag = true;
				if (accountFE.isPrimaryStatus() == true) {
					atomAcquirerFlag = true;
				}
				if (accountFE.isPrimaryNetbankingStatus() == true) {
					atomNetbankingPrimaryFlag = true;
				}if (accountFE.isPrimaryWalletStatus() == true) {
					atomWalletPrimaryFlag = true;
				}
			}
		}
		if ((yesBankExistFlag == true || hdfcBankExistFlag == true || cyberSourceExistFlag ==true || billdeskExistFlag ==true 
			|| safexPayExistFlag ==true || sbiBankExistFlag ==true || isgPayExistFlag ==true  || atomExistFlag ==true )
		&& (!yesBankAcquirerFlag && !hdfcAcquirerFlag  && !cyberSourceAcquirerFlag && !billdeskAcquirerFlag && !safexPayAcquirerFlag 
		&& !sbiBankAcquirerFlag  && !isgPayAcquirerFlag && !atomAcquirerFlag )) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_PRIMARY_CARD.getValue());
			addActionMessage(CrmFieldConstants.SELECT_PRIMARY_CARD.getValue());
		}// NetBanking Checked Flag
		if ((direcpayExistFlag == true || yesBankExistFlag == true || billdeskExistFlag == true || isgPayExistFlag == true || atomExistFlag == true || safexPayExistFlag == true)
				&& (!direcpayNetbankingPrimaryFlag && !yesBankNetbnkingPrimaryFlag && !billdeskNetbankingPrimaryFlag && !safexPayNetbankingPrimaryFlag && !isgPayNetbankingPrimaryFlag
				&& !atomNetbankingPrimaryFlag)) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_ONE_NETBANKING.getValue());
			addActionMessage(CrmFieldConstants.SELECT_ONE_NETBANKING.getValue());
		}
		// Wallet Checked Flag
		if ((billdeskExistFlag == true || atomExistFlag == true || safexPayExistFlag == true)
				&& (!billdeskWalletPrimaryFlag && !atomWalletPrimaryFlag && !safexPayWalletPrimaryFlag )) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_ONE_WALLET.getValue());
			addActionMessage(CrmFieldConstants.SELECT_ONE_WALLET.getValue());
		}
		// SALE/AUTH
		if (modeTypeFlag) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_SALE.getValue());
			addActionMessage(CrmFieldConstants.SELECT_SALE.getValue());
		}
		// Atleast one Netbanking Select
		if (yesBankNetbnkingPrimaryFlag && direcpayNetbankingPrimaryFlag 
				|| direcpayAcquirerFlag && yesBankAcquirerFlag) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_DIRECPAY_YES.getValue());
			addActionMessage(CrmFieldConstants.SELECT_DIRECPAY_YES.getValue());
		}
		if (billdeskNetbankingPrimaryFlag && safexPayNetbankingPrimaryFlag 
				|| billdeskAcquirerFlag && safexPayAcquirerFlag) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_DIRECPAY_BILLDESK.getValue());
			addActionMessage(CrmFieldConstants.SELECT_DIRECPAY_BILLDESK.getValue());
			
		}if (isgPayNetbankingPrimaryFlag && atomNetbankingPrimaryFlag 
				|| isgPayAcquirerFlag && atomAcquirerFlag) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_DIRECPAY_ATOM.getValue());
			addActionMessage(CrmFieldConstants.SELECT_DIRECPAY_ATOM.getValue());
		}
		
		// Atleast one Wallet Select
		if (billdeskWalletPrimaryFlag && safexPayWalletPrimaryFlag 
			|| billdeskAcquirerFlag && safexPayAcquirerFlag) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_DIRECPAY_SAFEXPAY.getValue());
			addActionMessage(CrmFieldConstants.SELECT_DIRECPAY_SAFEXPAY.getValue());
				}
		
		if (hdfcAcquirerFlag && yesBankAcquirerFlag) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_ONLY_ONE.getValue());
			addActionMessage(CrmFieldConstants.SELECT_ONLY_ONE.getValue());
		}
		if (cyberSourceAcquirerFlag && sbiBankAcquirerFlag) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_ONLY_ONE.getValue());
			addActionMessage(CrmFieldConstants.SELECT_ONLY_ONE.getValue());
		}
		if (safexPayAcquirerFlag && isgPayAcquirerFlag) {
			addFieldError(CrmFieldType.PAY_ID.getName(), CrmFieldConstants.SELECT_ONLY_ONE.getValue());
			addActionMessage(CrmFieldConstants.SELECT_ONLY_ONE.getValue());
		}

		// TODO validation field password encry
		if (!getFieldErrors().isEmpty()) {
			accountList = updateAccount(accountList, accountCurrencyList);
			Set<Account> set = new HashSet<Account>(accountList);
			user.setAccounts(set);
			UserDao userDao = new UserDao();
			User userFromDb = userDao.getUserClass(user.getPayId());
			float extraRefundLimitDB = userFromDb.getExtraRefundLimit();
			user.setExtraRefundLimit(extraRefundLimitDB);
		}
	}

	private List<Account> updateAccount(List<Account> newAccounts, List<AccountCurrency> accountCurrencyList) {
		List<Account> accountList = new ArrayList<Account>();
		for (Account accountFE : newAccounts) {
			for (AccountCurrency accountCurrencyFE : accountCurrencyList) {
				if (accountCurrencyFE.getAcqPayId().equals(accountFE.getAcquirerPayId())) {
					Set<AccountCurrency> accountCurrencySet = new HashSet<AccountCurrency>();
					accountCurrencySet.add(accountCurrencyFE);
					accountFE.addAccountCurrency(accountCurrencyFE);
				}
			}
			accountList.add(accountFE);
		}
		return accountList;
	}

	public List<Account> getAccountList() {
		return accountList;
	}

	public void setAccountList(List<Account> accountList) {
		this.accountList = accountList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getModel() {
		return user;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}

	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

	public Map<String, List<AccountCurrency>> getAccountCurrencyMap() {
		return accountCurrencyMap;
	}

	public void setAccountCurrencyMap(Map<String, List<AccountCurrency>> accountCurrencyMap) {
		this.accountCurrencyMap = accountCurrencyMap;
	}

	public List<AccountCurrency> getAccountCurrencyList() {
		return accountCurrencyList;
	}

	public void setAccountCurrencyList(List<AccountCurrency> accountCurrencyList) {
		this.accountCurrencyList = accountCurrencyList;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;

	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	public Map<String, String> getIndustryTypes() {
		return industryTypes;
	}

	public void setIndustryTypes(Map<String, String> industryTypes) {
		this.industryTypes = industryTypes;
	}

}