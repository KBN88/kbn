package com.kbn.crm.action;

import java.util.Map;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.Currency;

/**
 * @author shashi
 *
 */
public class MerchantCurrencyAction extends AbstractSecureAction {

	private static final long serialVersionUID = -4779558725256308048L;
	private String emailId;
	private String response;
	private Map<String, String> currencyMap;

	public String execute() {
		User userFromDb;
		UserDao userDao = new UserDao();
		userFromDb = userDao.findPayIdByEmail(emailId);
		
		currencyMap = Currency.getSupportedCurreny(userFromDb);
				 
		return SUCCESS;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}

	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

}
