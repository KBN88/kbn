package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.MapList;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.ServiceTax;
import com.kbn.commons.user.SurchargeDetails;
import com.kbn.commons.user.SurchargeMappingPopulator;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.crm.actionBeans.PendingDetailsFactory;
import com.kbn.crm.commons.action.AbstractSecureAction;
/**
 * @author Rahul, Shaiwal
 *
 */
public class PendingDetailsFormAction extends AbstractSecureAction {

	private static Logger logger = Logger
			.getLogger(PendingDetailsFormAction.class.getName());
	private static final long serialVersionUID = -6879974923614009981L;

	public List<Merchants> listMerchant = new ArrayList<Merchants>();
	private Map<String, List<SurchargeDetails>> surchargeMerchantData = new HashMap<String, List<SurchargeDetails>>();
	private Map<String, List<SurchargeMappingPopulator>> surchargePGData = new HashMap<String, List<SurchargeMappingPopulator>>();
	private Map<String, List<ServiceTax>> serviceTaxData = new HashMap<String, List<ServiceTax>>();
	private Map<String, List<ChargingDetails>> tdrData = new HashMap<String, List<ChargingDetails>>();
	private Map<String, User> userProfileData = new HashMap<String, User>();
	private Map<String, User> resellerMappingData = new HashMap<String, User>();
	
	private Map<String, List<MapList>> testData = new HashMap<String, List<MapList>>();

	@SuppressWarnings("unchecked")
	public String execute() {
		UserDao userDao = new UserDao();
		setListMerchant(userDao.getMerchantList());

		try {
				
				PendingDetailsFactory pendingDetailsFactory = new PendingDetailsFactory();
				setSurchargeMerchantData(pendingDetailsFactory.getPendingSurchargeDetails());
				setSurchargePGData(pendingDetailsFactory.getPendingPGSurchargeDetails());
				setServiceTaxData(pendingDetailsFactory.getPendingServiceTax());
				setTdrData(pendingDetailsFactory.getPendingChargingDetails());
				setUserProfileData(pendingDetailsFactory.getPendingUserProfile());
				setResellerMappingData(pendingDetailsFactory.getPendingResellerMapping());
				setTestData(pendingDetailsFactory.getTestData());
				
				if(surchargeMerchantData.size()==0 && surchargePGData.size() ==0 ){
				}
				
		}catch (Exception exception) {
			logger.error("Exception", exception);
			addActionMessage(ErrorType.UNKNOWN.getResponseMessage());
		}
		return INPUT;
	}

	// To display page without using token
	@SuppressWarnings("unchecked")
	public String displayList() {
		UserDao userDao = new UserDao();
		setListMerchant(userDao.getMerchantList());
		return INPUT;
	}

	public void validate() {
	}

	public List<Merchants> getListMerchant() {
		return listMerchant;
	}

	public void setListMerchant(List<Merchants> listMerchant) {
		this.listMerchant = listMerchant;
	}

	public Map<String, List<SurchargeDetails>> getSurchargeMerchantData() {
		return surchargeMerchantData;
	}

	public void setSurchargeMerchantData(Map<String, List<SurchargeDetails>> surchargeMerchantData) {
		this.surchargeMerchantData = surchargeMerchantData;
	}

	public Map<String, List<SurchargeMappingPopulator>> getSurchargePGData() {
		return surchargePGData;
	}

	public void setSurchargePGData(Map<String, List<SurchargeMappingPopulator>> surchargePGData) {
		this.surchargePGData = surchargePGData;
	}

	public Map<String, List<ServiceTax>> getServiceTaxData() {
		return serviceTaxData;
	}

	public void setServiceTaxData(Map<String, List<ServiceTax>> serviceTaxData) {
		this.serviceTaxData = serviceTaxData;
	}

	public Map<String, List<ChargingDetails>> getTdrData() {
		return tdrData;
	}

	public void setTdrData(Map<String, List<ChargingDetails>> tdrData) {
		this.tdrData = tdrData;
	}

	public Map<String, User> getUserProfileData() {
		return userProfileData;
	}

	public void setUserProfileData(Map<String, User> userProfileData) {
		this.userProfileData = userProfileData;
	}

	public Map<String, User> getResellerMappingData() {
		return resellerMappingData;
	}

	public void setResellerMappingData(Map<String, User> resellerMappingData) {
		this.resellerMappingData = resellerMappingData;
	}

	public Map<String, List<MapList>> getTestData() {
		return testData;
	}

	public void setTestData(Map<String, List<MapList>> testData) {
		this.testData = testData;
	}


}
