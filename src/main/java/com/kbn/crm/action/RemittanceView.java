package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Remittance;
import com.kbn.commons.user.RemittanceDao;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.commons.util.DateCreater;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class RemittanceView extends AbstractSecureAction {
	/**
	 * @Isha, Neeraj
	 */
	private static final long serialVersionUID = 7509860981455583527L;
	private static Logger logger = Logger
			.getLogger(TransactionSummaryAction.class.getName());

	private List<Remittance> aaData = new ArrayList<Remittance>();
	//private String payId;
	private String dateFrom;
	private String dateTo;
	private String status;
	private String successMessage;
	private String merchantEmailId;

	public String execute() {
		User sessionUser = new User();
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder = new DataEncoder();
		SessionUserIdentifier userIdentifier = new SessionUserIdentifier();

		try {
			String merchantPayId = userIdentifier.getMerchantPayId(sessionUser,
					getMerchantEmailId());

			setAaData(encoder.encodeRemittanceObj(new RemittanceDao()
					.findDetail(getDateFrom(), getDateTo(), merchantPayId,
							getStatus())));

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();
		if(validator.validateBlankField(getMerchantEmailId()) || getMerchantEmailId().equals(CrmFieldConstants.ALL.getValue())){
		}
        else if(!validator.validateField(CrmFieldType.MERCHANT_EMAIL_ID, getMerchantEmailId())){
        	addFieldError(CrmFieldType. MERCHANT_EMAIL_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
		
		if ((validator.validateBlankField(getDateFrom()))) {
			addFieldError(CrmFieldType.DATE_FROM.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.DATE_FROM,
				getDateFrom()))) {
			addFieldError(CrmFieldType.DATE_FROM.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if (!validator.validateBlankField(getDateTo())) {
			if (DateCreater.formatStringToDate(
					DateCreater.formatFromDate(getDateFrom())).compareTo(
					DateCreater.formatStringToDate(DateCreater
							.formatFromDate(getDateTo()))) > 0) {
				addFieldError(CrmFieldType.DATE_FROM.getName(),
						CrmFieldConstants.FROMTO_DATE_VALIDATION.getValue());
			} else if (DateCreater.diffDate(getDateFrom(), getDateTo()) > 31) {
				addFieldError(CrmFieldType.DATE_FROM.getName(),
						CrmFieldConstants.DATE_RANGE.getValue());
			}
		}

		if ((validator.validateBlankField(getDateTo()))) {
			addFieldError(CrmFieldType.DATE_TO.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.DATE_TO, getDateTo()))) {
			addFieldError(CrmFieldType.DATE_TO.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(getSuccessMessage()))) {
		} else if (!(validator.validateField(CrmFieldType.SUCCESS_MESSAGE,
				getSuccessMessage()))) {
			addFieldError(CrmFieldType.SUCCESS_MESSAGE.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(getStatus()) || getStatus().equals(
				CrmFieldConstants.ALL.getValue()))) {
		} else if (!(validator.validateField(CrmFieldType.STATUS, getStatus()))) {
			addFieldError(CrmFieldType.STATUS.getName(), validator
					.getResonseObject().getResponseMessage());
		}
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public List<Remittance> getAaData() {
		return aaData;
	}

	public void setAaData(List<Remittance> aaData) {
		this.aaData = aaData;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMerchantEmailId() {
		return merchantEmailId;
	}

	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}
}
