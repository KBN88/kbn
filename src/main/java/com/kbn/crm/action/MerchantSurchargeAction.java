package com.kbn.crm.action;

import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.user.SurchargeDetails;
import com.kbn.commons.user.SurchargeDetailsDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Shaiwal
 *
 */
public class MerchantSurchargeAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(MerchantSurchargeAction.class.getName());
	private static final long serialVersionUID = -6517340843571949786L;

	private String paymentType;
	private String payId;
	private String emailId;
	private String userType;
	private String operation;
	public Date currentDate = new Date();

	public String execute() {
		SurchargeDetailsDao surchargeDetailsDao = new SurchargeDetailsDao();

		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));
		
		if (permissions.toString().contains("Create Surcharge") || userType.equals(UserType.ADMIN.toString())){
			
			SurchargeDetails surchargeToUpdate = new SurchargeDetails();
			surchargeToUpdate = surchargeDetailsDao.findPendingDetails(payId, paymentType);
			
			if (surchargeToUpdate != null){
				
				if (operation.equals("accept")){
					SurchargeDetails activeSurcharge = new SurchargeDetails();
					activeSurcharge = surchargeDetailsDao.findDetails(payId, paymentType);
					
					if (activeSurcharge != null){
					
						updateSurchargeDetail(activeSurcharge , TDRStatus.INACTIVE);
					}
						updateSurchargeDetail(surchargeToUpdate , TDRStatus.ACTIVE);
				}
				else{
					updateSurchargeDetail(surchargeToUpdate , TDRStatus.REJECTED);
				}
				
				
			}
			
		}

		return SUCCESS;
	}

	public void updateSurchargeDetail(SurchargeDetails surchargeDetails , TDRStatus status){
		
		try {
			
			Session session = null;
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			Long id = surchargeDetails.getId();
			session.load(surchargeDetails, surchargeDetails.getId());
			SurchargeDetails surchargeDetail = (SurchargeDetails) session.get(SurchargeDetails.class, id);
			surchargeDetail.setStatus(status);
			surchargeDetail.setUpdatedDate(currentDate);
			surchargeDetail.setProcessedBy(emailId);
			session.update(surchargeDetail);
			tx.commit();
			session.close();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {

		}

		
	}
	
	
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

}
