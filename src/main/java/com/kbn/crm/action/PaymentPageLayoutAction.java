package com.kbn.crm.action;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.user.DynamicPaymentPage;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.actionBeans.DynamicPageUpdator;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.action.FetchDynamicPaymentPage;
import com.opensymphony.xwork2.ModelDriven;
/**
 * @author Isha
 */
public class PaymentPageLayoutAction extends AbstractSecureAction implements
ModelDriven<DynamicPaymentPage>  {

	private static final long serialVersionUID = -1100818649773578599L;
	private static Logger logger = Logger.getLogger(PaymentPageLayoutAction.class.getName());
	
	DynamicPaymentPage dynamicPaymentPage = new  DynamicPaymentPage();
	FetchDynamicPaymentPage fetchDynamicPage= new FetchDynamicPaymentPage();
	DynamicPageUpdator DPUpdator = new DynamicPageUpdator();
	private String payId;
	private File userLogo;
	private String userLogoFileName;

	
	public String execute(){
		try{
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			DPUpdator.updateUserDetails(dynamicPaymentPage, sessionUser.getPayId());
			
			if  (userLogo != null &&(userLogoFileName.toLowerCase().endsWith(".jpg") || userLogoFileName.toLowerCase().endsWith(".png"))){
				SaveFile(userLogoFileName, userLogo);
			}
			addActionMessage(CrmFieldConstants.DETAILS_SAVED_SUCCESSFULLY
					.getValue());
		}
		catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		
		return SUCCESS;
		
	}
	public String SetDefaultPaymentPage(){
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		setDynamicPaymentPage(fetchDynamicPage.SetDefault(sessionUser.getPayId()));
		addActionMessage(CrmFieldConstants.PLEASE_SAVE
				.getValue());
		return SUCCESS;
	}
	private String SaveFile(String filename, File controlFile) {
		String destPath,saveFilename,logoFormat;
		
		PropertiesManager propertiesManager = new PropertiesManager();
		destPath = propertiesManager.getSystemProperty("PaymentPagePath");
		logoFormat = propertiesManager.getSystemProperty("PaymentPageLogoFormat");
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		if (sessionUser.getUserType().equals(UserType.ADMIN)
				|| sessionUser.getUserType().equals(UserType.SUPERADMIN)) {
			// payId=user.getPayId();
		} else {
			payId = sessionUser.getPayId();
		}
		saveFilename = payId + logoFormat;
		File destFile = new File(destPath, saveFilename);
		try {
			FileUtils.copyFile(controlFile, destFile);
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;

	}
	@Override
	public DynamicPaymentPage getModel() {
		
		return dynamicPaymentPage;
	}

	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public DynamicPaymentPage getDynamicPaymentPage() {
		return dynamicPaymentPage;
	}
	public void setDynamicPaymentPage(DynamicPaymentPage dynamicPaymentPage) {
		this.dynamicPaymentPage = dynamicPaymentPage;
	}

	public File getUserLogo() {
		return userLogo;
	}
	public void setUserLogo(File userLogo) {
		this.userLogo = userLogo;
	}

	public String getUserLogoFileName() {
		return userLogoFileName;
	}

	public void setUserLogoFileName(String userLogoFileName) {
		this.userLogoFileName = userLogoFileName;
	}

	

}
