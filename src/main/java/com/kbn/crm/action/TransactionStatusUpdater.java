package com.kbn.crm.action;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.StatusEnquiryParameters;
import com.kbn.commons.util.CommanCsvReader;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.actionBeans.BatchResponseObject;
import com.kbn.crm.actionBeans.StatusUpdateProcessor;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class TransactionStatusUpdater extends AbstractSecureAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1733259300461998177L;
	private static Logger logger = Logger.getLogger(TransactionStatusUpdater.class.getName());
	private String fileName;

	private List<Merchants> merchantList = new LinkedList<Merchants>();
	private Map<String, String> currencyMap = new HashMap<String, String>();

	public String execute() {
		List<StatusEnquiryParameters> statusEnquiryParametersList = new LinkedList<StatusEnquiryParameters>();
		try {
			BatchResponseObject batchResponseObject = new BatchResponseObject();
			CommanCsvReader commanCsvReader = new CommanCsvReader();
			// batchFile read line by line and each line is appended of the list
			statusEnquiryParametersList = commanCsvReader.prepareStatusUpdateList(fileName);
			if (statusEnquiryParametersList.isEmpty()) {
				addActionMessage(ErrorType.INVALID_FIELD.getResponseMessage());
			} else {
				StatusUpdateProcessor statusUpdateProcessor = new StatusUpdateProcessor();
				// StatusUpdateProcessor is used to update Status
				batchResponseObject = statusUpdateProcessor.statusUpdateProcess(statusEnquiryParametersList);
				addActionMessage(batchResponseObject.getResponseMessage());
				if (StringUtils.isEmpty(batchResponseObject.getResponseMessage())) {
					batchResponseObject.setResponseMessage(CrmFieldConstants.PROCESS_INITIATED_SUCCESSFULLY.getValue());
					addActionMessage(batchResponseObject.getResponseMessage());
				}
			}
		} catch (Exception exception) {
			logger.error("Error while processing batch status update:" + exception);
			addActionMessage("Error while processing batch status update:" + exception);
		}
		return INPUT;

	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<Merchants> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<Merchants> merchantList) {
		this.merchantList = merchantList;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}

	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

}
