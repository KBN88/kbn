package com.kbn.crm.action;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.actionBeans.BinRange;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class BinRangeDisplayAction extends AbstractSecureAction {

	/**
	 * @ Neeraj
	 */
	private static final long serialVersionUID = 730021546498302127L;
	private static Logger logger = Logger.getLogger(BinRangeDisplayAction.class.getName());
	private List<BinRange> aaData = new ArrayList<BinRange>();
	private User sessionUser = new User();
	private String cardType;
	private String mopType;
	private int draw;
	private int length;
	private int start;
	private BigInteger recordsTotal;
	public BigInteger recordsFiltered;

	public String getBinRangList() {
		DataEncoder encoder = new DataEncoder();
		BinRangeDisplayServices binRangeDisplayServices = new BinRangeDisplayServices();
		BinRangeFilter binRangeFilter = new BinRangeFilter();
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		try {
			String paymentType = binRangeFilter.getPaymentType(sessionUser, cardType);
			String mopTypes = binRangeFilter.getMopType(sessionUser, mopType);
			setRecordsTotal(binRangeDisplayServices.getBinRangTotal(paymentType, mopTypes, sessionUser));
			if (getLength() == -1) {
				setLength(getRecordsTotal().intValue());
			}
			setAaData(encoder.encodeBinRange(binRangeDisplayServices.getBinRangDisplay(paymentType, mopTypes,
					sessionUser, getStart(), getLength())));
			recordsFiltered = recordsTotal;
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getMopType() {
		return mopType;
	}

	public void setMopType(String mopType) {
		this.mopType = mopType;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public List<BinRange> getAaData() {
		return aaData;
	}

	public void setAaData(List<BinRange> aaData) {
		this.aaData = aaData;
	}

	public BigInteger getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(BigInteger recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public BigInteger getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(BigInteger recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

}
