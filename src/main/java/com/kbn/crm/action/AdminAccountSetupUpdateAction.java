package com.kbn.crm.action;

import java.util.Date;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.opensymphony.xwork2.ModelDriven;

public class AdminAccountSetupUpdateAction extends AbstractSecureAction implements  ModelDriven<User> {

	/**
	 * @ Neeraj
	 */
	private static final long serialVersionUID = -2804573223359698889L;
	private static Logger logger = Logger.getLogger(AdminAccountSetupUpdateAction.class.getName());
	private User user = new User();
	public String updateAdminSetup(){
		Date date = new Date();
		try{
			AdminRecordUpdater adminRecordUpdater = new AdminRecordUpdater();
			user.setActivationDate(date);
			setUser(adminRecordUpdater.updateUserProfile(user));
			addActionMessage("Admin Status  successfully Update.");
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;     
		}
		
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}



	@Override
	public User getModel() {
		return user;
	}

}
