package com.kbn.crm.action;

import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.MerchantGridViewService;
import com.kbn.commons.user.MerchantDetails;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class ResellerGridViewAction extends AbstractSecureAction{

	/**
	 * Neeraj
	 */
	private static Logger logger = Logger.getLogger(ResellerGridViewAction.class.getName());
	private static final long serialVersionUID = 1888904677472306613L;
	private List<MerchantDetails> aaData;
	@Override
	public String execute() {
		DataEncoder encoder = new DataEncoder();
		try {	
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			aaData = encoder.encodeMerchantDetailsObj((new MerchantGridViewService()).getAllReselerMerchants(sessionUser.getResellerId()));
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}
	public List<MerchantDetails> getAaData() {
		return aaData;
	}
	public void setAaData(List<MerchantDetails> aaData) {
		this.aaData = aaData;
	}


}
