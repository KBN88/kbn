package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import com.kbn.commons.user.UserRecordsDao;

/**
 * @author Chandan
 */

public class CheckOldPassword {

	public  boolean isUsedPassword(String newPassword, String emailId) {
		List<String> oldPasswords = new ArrayList<String>();
		UserRecordsDao userRecordsDao = new UserRecordsDao();
		oldPasswords = userRecordsDao.getOldPasswords(emailId);
		for (String password : oldPasswords) {
			if (null == password) {
				continue;
			}
			if (password.equals(newPassword)) {
				return true;
			}
		}
		return false;
	}
}
