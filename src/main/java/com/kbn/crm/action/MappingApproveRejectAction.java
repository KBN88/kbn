package com.kbn.crm.action;

import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.google.gson.Gson;
import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.dao.PendingMappingRequestDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.PendingMappingRequest;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.actionBeans.UserMappingEditor;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.AcquirerType;

/**
 * @author Shaiwal
 *
 */
public class MappingApproveRejectAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(MappingApproveRejectAction.class.getName());
	private static final long serialVersionUID = -6517340843571949786L;

	private String emailId;
	private String userType;
	private String operation;
	private String merchant;

	private String merchantEmailId;
	private String mapString;
	private String acquirer;
	private String response;
	private String accountCurrencySet;

	public Date currentDate = new Date();

	public String execute() {

		try {
			
			StringBuilder permissions = new StringBuilder();
			permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));
			
			if (userType.equals(UserType.ADMIN.toString())
					|| permissions.toString().contains("Create Service Tax")) {
				setMerchantEmailId(new UserDao().getEmailIdByBusinessName(merchant));
				
				AcquirerType.getInstancefromName(acquirer).getName();
				PendingMappingRequest existingPMR = new PendingMappingRequestDao().findPendingMappingRequest(merchantEmailId, AcquirerType.getInstancefromName(acquirer).getCode());
				PendingMappingRequest existingActivePMR  = new PendingMappingRequestDao().findActiveMappingRequest(merchantEmailId, AcquirerType.getInstancefromName(acquirer).getCode());
				if (existingPMR != null){
					
					if (operation.equalsIgnoreCase("reject")){
						updateMapping(existingPMR,TDRStatus.REJECTED,"",emailId);
						setResponse(ErrorType.MAPPING_REQUEST_REJECTED.getResponseMessage());
						return SUCCESS;
					}
					updateMapping(existingPMR,TDRStatus.ACTIVE,"",emailId);
					updateMapping(existingActivePMR,TDRStatus.INACTIVE,"",emailId);
					setMapString(existingPMR.getMapString());
					setMerchantEmailId(existingPMR.getMerchantEmailId());
					setAcquirer(existingPMR.getAcquirer());
					setAccountCurrencySet(existingPMR.getAccountCurrencySet());
					
					if (mapString != null && merchantEmailId != null && acquirer != null) {
						processMapString();
						setResponse(ErrorType.MAPPING_SAVED.getResponseMessage());
						return SUCCESS;

					}
				}
				else{
					
					setResponse(ErrorType.MAPPING_REQUEST_NOT_FOUND.getResponseMessage());
					return SUCCESS;
				}
				
			}
			else{
				
				setResponse(ErrorType.MAPPING_REQUEST_APPROVAL_DENIED.getResponseMessage());
				return SUCCESS;
			}
			
		} catch (Exception exception) {
			logger.error("Exception", exception);
			setResponse(ErrorType.MAPPING_NOT_SAVED.getResponseMessage());
			return SUCCESS;
		}
		setResponse(ErrorType.MAPPING_NOT_SAVED.getResponseMessage());
		return SUCCESS;

	}

	private void processMapString() {
		UserMappingEditor userMappingEditor = new UserMappingEditor();
		Gson gson = new Gson();
		AccountCurrency[] accountCurrencies = gson.fromJson(accountCurrencySet, AccountCurrency[].class);
		userMappingEditor.decideAccountChange(getMerchantEmailId(), getMapString(), getAcquirer(), accountCurrencies);
	}
	
	public void updateMapping(PendingMappingRequest pmr, TDRStatus status , String requestedBy , String processedBy) {

		Date currentDate = new Date();
		try {

			Session session = null;
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			Long id = pmr.getId();
			session.load(pmr, pmr.getId());
			PendingMappingRequest pendingRequest = (PendingMappingRequest) session.get(PendingMappingRequest.class, id);
			pendingRequest.setStatus(status);
			pendingRequest.setUpdatedDate(currentDate);
			if(!requestedBy.equalsIgnoreCase("")){
				pendingRequest.setRequestedBy(requestedBy);
			}
			if (!processedBy.equalsIgnoreCase("")){
				pendingRequest.setProcessedBy(processedBy);
			}
			
			session.update(pendingRequest);
			tx.commit();
			session.close();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {

		}

	}

	public String getMerchantEmailId() {
		return merchantEmailId;
	}

	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}

	public String getMapString() {
		return mapString;
	}

	public void setMapString(String mapString) {
		this.mapString = mapString;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getAccountCurrencySet() {
		return accountCurrencySet;
	}

	public void setAccountCurrencySet(String accountCurrencySet) {
		this.accountCurrencySet = accountCurrencySet;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

}
