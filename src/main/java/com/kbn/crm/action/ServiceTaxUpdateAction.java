package com.kbn.crm.action;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.ChargingDetailsDao;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 *
 */
public class ServiceTaxUpdateAction extends AbstractSecureAction{

	private static final long serialVersionUID = -1465492690619542607L;

	private String response;
	private double newServiceTax;
	private static Logger logger = Logger.getLogger(ServiceTaxUpdateAction.class.getName());

	public String execute(){
		try{			
		
		Date currentDate = new Date();
		//get all active TDRs
		ChargingDetailsDao chargingDetailsDao = new ChargingDetailsDao();
		List<ChargingDetails> chargingDetailList =  chargingDetailsDao.getAllActiveChargingDetails();
		//change one by one
		//create new
		for(ChargingDetails oldChargingDetail : chargingDetailList){
			ChargingDetails newChargingDetail = SerializationUtils.clone(oldChargingDetail);
			newChargingDetail.setId(null);
			//create new TDR
			newChargingDetail.setPgServiceTax(newServiceTax);
			newChargingDetail.setBankServiceTax(newServiceTax);
			newChargingDetail.setMerchantServiceTax(newServiceTax);
			newChargingDetail.setStatus(TDRStatus.ACTIVE);
			newChargingDetail.setCreatedDate(currentDate);
			newChargingDetail.setId(null);
			//deactivate old TDR
			oldChargingDetail.setStatus(TDRStatus.INACTIVE);
			oldChargingDetail.setUpdatedDate(currentDate);

			//TODO....initiate batch statement
		//	chargingDetailsDao.update(oldChargingDetail);
		//	chargingDetailsDao.create(newChargingDetail);
		}
		setResponse("Service tax updated successfully for all active TDRs");		
		}catch(Exception exception){
			logger.error("Error updating servcice tax " + exception);
			setResponse(ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
		}
		//return response
		//send mail on completion/error
		return SUCCESS;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public double getNewServiceTax() {
		return newServiceTax;
	}

	public void setNewServiceTax(double newServiceTax) {
		this.newServiceTax = newServiceTax;
	}
}
