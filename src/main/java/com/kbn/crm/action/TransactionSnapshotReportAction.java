package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionReportService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.TransactionSnapshotReport;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.commons.util.DateCreater;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 *
 */

public class TransactionSnapshotReportAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(TransactionSnapshotReportAction.class.getName());
	private static final long serialVersionUID = -3131381841294843726L;	

	private String acquirer;
	private String paymentType;
	private String dateFrom;
	private String dateTo;
	private String merchantEmailId;
	private String currency;

	private List<TransactionSnapshotReport> aaData = new ArrayList<TransactionSnapshotReport>();

	public String execute() {
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier userIdentifier = new SessionUserIdentifier();
		User user = (User) sessionMap.get(Constants.USER.getValue());
		TransactionReportService reportGen = new TransactionReportService();
		try {
			String merchantPayId = userIdentifier.getMerchantPayId(user, merchantEmailId);

			setAaData(encoder.encodeTransactionSnapShot(reportGen.getSnapshotReport(getAcquirer(),
					getPaymentType(), getDateFrom(), getDateTo(), merchantPayId)));

			return SUCCESS;
		}
		catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public void validate(){
		CrmValidator validator = new CrmValidator();

		if(validator.validateBlankField(getAcquirer())){
		}
		else if(!validator.validateField(CrmFieldType.ACQUIRER, getAcquirer())){
			addFieldError(CrmFieldType.ACQUIRER.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

        if(validator.validateBlankField(getCurrency())){
		}
        else if(!validator.validateField(CrmFieldType.CURRENCY, getCurrency())){
        	addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

        if(validator.validateBlankField(getDateFrom())){
        }
        else if(!validator.validateField(CrmFieldType.DATE_FROM, getDateFrom())){
        	addFieldError(CrmFieldType.DATE_FROM.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

        if(validator.validateBlankField(getDateTo())){
        }
        else if(!validator.validateField(CrmFieldType.DATE_TO, getDateTo())){
        	addFieldError(CrmFieldType.DATE_TO.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
        
        if(!validator.validateBlankField(getDateTo())){
  	       if(DateCreater.formatStringToDate(DateCreater.formatFromDate(getDateFrom())).compareTo(DateCreater.formatStringToDate(DateCreater.formatFromDate(getDateTo()))) > 0) {
  	        	addFieldError(CrmFieldType.DATE_FROM.getName(), CrmFieldConstants.FROMTO_DATE_VALIDATION.getValue());
  	        }
  	        else if(DateCreater.diffDate(getDateFrom(), getDateTo()) > 31) {
  	        	addFieldError(CrmFieldType.DATE_FROM.getName(), CrmFieldConstants.DATE_RANGE.getValue());
  	        }
        }

        if(validator.validateBlankField(getMerchantEmailId()) || getMerchantEmailId().equals(CrmFieldConstants.ALL.getValue())){
		}
        else if(!validator.validateField(CrmFieldType.MERCHANT_EMAIL_ID, getMerchantEmailId())){
        	addFieldError(CrmFieldType. MERCHANT_EMAIL_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if(validator.validateBlankField(getPaymentType())){
		}
		else if(!validator.validateField(CrmFieldType.PAYMENT_TYPE, getPaymentType())){
        	addFieldError(CrmFieldType.PAYMENT_TYPE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
    }
	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public List<TransactionSnapshotReport> getAaData() {
		return aaData;
	}

	public void setAaData(List<TransactionSnapshotReport> aaData) {
		this.aaData = aaData;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getMerchantEmailId() {
		return merchantEmailId;
	}

	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}
}
