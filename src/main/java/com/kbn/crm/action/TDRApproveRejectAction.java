package com.kbn.crm.action;

import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.ChargingDetailsDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Shaiwal
 *
 */
public class TDRApproveRejectAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(TDRApproveRejectAction.class.getName());
	private static final long serialVersionUID = -6517340843571949786L;

	private Long id;
	private String emailId;
	private String userType;
	private String operation;
	public Date currentDate = new Date();

	public String execute() {
		ChargingDetailsDao chargingDetailsDao = new ChargingDetailsDao();

		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

		if (permissions.toString().contains("Create TDR") || userType.equals(UserType.ADMIN.toString())) {

			ChargingDetails chargingDetailsToUpdate = new ChargingDetails();
			ChargingDetails activeChargingDetail = new ChargingDetails();
			
			chargingDetailsToUpdate = chargingDetailsDao.find(id);
			activeChargingDetail = chargingDetailsDao.findActiveChargingDetail(chargingDetailsToUpdate.getMopType(), chargingDetailsToUpdate.getPaymentType(),
					chargingDetailsToUpdate.getTransactionType(), chargingDetailsToUpdate.getAcquirerName(), chargingDetailsToUpdate.getCurrency(), chargingDetailsToUpdate.getPayId());

			if (chargingDetailsToUpdate != null) {

				if (operation.equals("accept")) {

					if (activeChargingDetail != null) {
						updateChargingDetails(activeChargingDetail, TDRStatus.INACTIVE);
					}
					updateChargingDetails(chargingDetailsToUpdate, TDRStatus.ACTIVE);
					
				} else {
					updateChargingDetails(chargingDetailsToUpdate, TDRStatus.REJECTED);
				}

			}

		}

		return SUCCESS;
	}

	public void updateChargingDetails(ChargingDetails chargingDetails, TDRStatus status) {

		try {

			Session session = null;
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			Long id = chargingDetails.getId();
			session.load(chargingDetails, chargingDetails.getId());
			ChargingDetails cd = (ChargingDetails) session.get(ChargingDetails.class, id);
			cd.setStatus(status);
			cd.setUpdatedDate(currentDate);
			session.update(cd);
			tx.commit();
			session.close();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {

		}

	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
