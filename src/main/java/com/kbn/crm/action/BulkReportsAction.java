package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.util.BulkReportsType;
import com.kbn.crm.actionBeans.BulkReportsService;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 * Get bulk reports for longer time durations
 */
public class BulkReportsAction extends AbstractSecureAction {

	private static final long serialVersionUID = 5997072488291922491L;
	private static Logger logger = Logger
			.getLogger(BulkReportsAction.class.getName());

	private String acquirer;
	private String merchantPayId;
	private String paymentMethods;
	private String currency;
	private String dateFrom;
	private String dateTo;
	private BulkReportsType reportType;
	private String response;

	public String summaryReport(){
		BulkReportsService service = new BulkReportsService();
		try {
			service.prepareReport(acquirer, merchantPayId, paymentMethods, currency, dateFrom, dateTo, reportType);
			//TODO.... set proper response message
			setResponse("success");
		} catch (Exception exception) {
			logger.error("Exception" + exception);
		}
		return SUCCESS;
	}

	public String tdrReport(){
		BulkReportsService service = new BulkReportsService();
		try {
			service.prepareReport(acquirer, merchantPayId, paymentMethods, currency, dateFrom, dateTo, reportType);
		} catch (Exception exception) {
			logger.error("Exception" + exception);
		}
		return SUCCESS;
	}

	public String settlementReport(){
		BulkReportsService service = new BulkReportsService();
		try {
			service.prepareReport(acquirer, merchantPayId, paymentMethods, currency, dateFrom, dateTo, reportType);
		} catch (Exception exception) {
			logger.error("Exception" + exception);
		}
		return SUCCESS;
	}

	public String getAcquirer() {
		return acquirer;
	}
	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}
	public String getPaymentMethods() {
		return paymentMethods;
	}
	public void setPaymentMethods(String paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getMerchantPayId() {
		return merchantPayId;
	}
	public void setMerchantPayId(String merchantPayId) {
		this.merchantPayId = merchantPayId;
	}

	public BulkReportsType getReportType() {
		return reportType;
	}

	public void setReportType(BulkReportsType reportType) {
		this.reportType = reportType;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
