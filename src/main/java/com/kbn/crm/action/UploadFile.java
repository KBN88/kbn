package com.kbn.crm.action;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.DocumentNameConstants;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.opensymphony.xwork2.ModelDriven;

public class UploadFile extends AbstractSecureAction implements
		ModelDriven<User> {

	private static Logger logger = Logger.getLogger(UploadFile.class.getName());
	private static final long serialVersionUID = -2152501986239465437L;
	private User user = new User();
	private String payId;

	private String saveFilename;

	private File PL_AOA;
	private File PL_MOA;
	private File PL_COI;
	private File PL_COCOB;
	private File PL_POC;
	private File PL_IDOAS;
	private File PL_BR;
	private File PL_LPDOD;
	private File PL_AP;
	private File PL_BS;
	private File PF_ID;
	private File PF_CTC;
	private File PF_LOP;
	private File PF_PLS;
	private File PF_PC;
	private File PF_AP;
	private File PF_IDOAS;
	private File PF_BS;
	private File PF_LUS;
	private File PR_ID;
	private File PR_PAN;
	private File PR_POE;
	private File PR_AP;
	private File PR_BS;
	private File PR_LUS;
	private File CSA_BL;
	private File CSA_GBR;
	private File CSA_PC;
	private File LLL_RC;
	private File LLL_LAD;
	private File LLL_LOP;
	private File LLL_ID;
	private File LLL_AL;
	private File LLL_PCC;
	private File LLL_DR;
	private File LLL_AP;
	private File LLL_BS;
	private File LLL_PCL;
	private File LLL_LUS;
	private File RI_P;
	private File RI_PC;
	private File RI_DL;
	private File RI_BV;
	private File AP_PIL;
	private File AP_CUB;
	private File AP_MT;
	private File AP_EBS;
	private File AP_EBC;
	private File AP_IP;
	private File AP_AOD;
	private File T_RFB;
	private File T_SP;
	private File T_CI;
	private File T_AC;
	private File T_DTP;
	private File T_ABS;
	private File T_ST;
	private File T_VC;
	private File T_POC;

	private String ddlBusinessType;

	private String myFileContentType;
	private String PL_AOAFileName;
	private String PL_MOAFileName;
	private String PL_COIFileName;
	private String PL_COCOBFileName;
	private String PL_POCFileName;
	private String PL_IDOASFileName;
	private String PL_BRFileName;
	private String PL_LPDODFileName;
	private String PL_APFileName;
	private String PL_BSFileName;

	private String PF_IDFileName;
	private String PF_CTCFileName;
	private String PF_LOPFileName;
	private String PF_PLSFileName;
	private String PF_PCFileName;
	private String PF_APFileName;
	private String PF_IDOASFileName;
	private String PF_BSFileName;
	private String PF_LUSFileName;
	private String PR_IDFileName;
	private String PR_PANFileName;
	private String PR_POEFileName;
	private String PR_APFileName;
	private String PR_BSFileName;
	private String PR_LUSFileName;
	private String LLL_RCFileName;
	private String LLL_LADFileName;
	private String LLL_LOPFileName;
	private String LLL_IDFileName;
	private String LLL_ALFileName;
	private String LLL_PCCFileName;
	private String LLL_DRFileName;
	private String LLL_APFileName;
	private String LLL_BSFileName;
	private String LLL_PCLFileName;
	private String LLL_LUSFileName;
	private String CSA_BLFileName;
	private String CSA_GBRFileName;
	private String CSA_PCFileName;
	private String RI_PFileName;
	private String RI_PCFileName;
	private String RI_DLFileName;
	private String RI_BVFileName;
	private String AP_PILFileName;
	private String AP_CUBFileName;
	private String AP_MTFileName;
	private String AP_EBSFileName;
	private String AP_EBCFileName;
	private String AP_IPFileName;
	private String AP_AODFileName;
	private String T_RFBFileName;
	private String T_SPFileName;
	private String T_CIFileName;
	private String T_ACFileName;
	private String T_DTPFileName;
	private String T_ABSFileName;
	private String T_STFileName;
	private String T_VCFileName;
	private String T_POCFileName;

	private String destPath;

	public String execute() {

		
		
		
		if (ddlBusinessType.equals("PL")) {

			if (PL_AOA != null
					&& (PL_AOAFileName.toLowerCase().endsWith(".pdf") || PL_AOAFileName.toLowerCase().endsWith(".jpg")|| PL_AOAFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.ARTICLE_OF_ASSOCIATION.getName(), PL_AOAFileName, PL_AOA);
			}

			if (PL_MOA != null
					&& (PL_MOAFileName.toLowerCase().endsWith(".pdf") || PL_MOAFileName
							.toLowerCase().endsWith(".jpg")|| PL_MOAFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.MEMORANDUM_OF_ASSOCIATION.getName(), PL_MOAFileName, PL_MOA);

			}
			if (PL_COI != null
					&& (PL_COIFileName.toLowerCase().endsWith(".pdf") || PL_COIFileName
							.toLowerCase().endsWith(".jpg")||  PL_COIFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.CERTIFICATION_OF_INCORPORATION.getName(), PL_COIFileName,
						PL_COI);
			}
			if (PL_COCOB != null
					&& (PL_COCOBFileName.toLowerCase().endsWith(".pdf") || PL_COCOBFileName
							.toLowerCase().endsWith(".jpg")||  PL_COCOBFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.CERTIFICATION_OF_COMMENCEMENT.getName(), PL_COCOBFileName,
						PL_COCOB);

			}
			if (PL_POC != null
					&& (PL_POCFileName.toLowerCase().endsWith(".pdf") || PL_POCFileName
							.toLowerCase().endsWith(".jpg")||  PL_POCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PAN_OF_THE_COMPANY.getName(), PL_POCFileName, PL_POC);

			}
			if (PL_IDOAS != null
					&& (PL_IDOASFileName.toLowerCase().endsWith(".pdf") || PL_IDOASFileName
							.toLowerCase().endsWith(".jpg")||  PL_IDOASFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.IDENTIFICATION_DOCUMNETS.getName(), PL_IDOASFileName, PL_IDOAS);

			}
			if (PL_BR != null
					&& (PL_BRFileName.toLowerCase().endsWith(".pdf") || PL_BRFileName
							.toLowerCase().endsWith(".jpg")||  PL_BRFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.BOARD_RESOLUTION.getName(), PL_BRFileName, PL_BR);

			}
			if (PL_LPDOD != null
					&& (PL_LPDODFileName.toLowerCase().endsWith(".pdf") || PL_LPDODFileName
							.toLowerCase().endsWith(".jpg")||  PL_LPDODFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.LIST_PERSONAL_DETAILS.getName(), PL_LPDODFileName, PL_LPDOD);

			}
			if (PL_AP != null
					&& (PL_APFileName.toLowerCase().endsWith(".pdf") || PL_APFileName
							.toLowerCase().endsWith(".jpg")||  PL_APFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.ADDRESS_PROOFS.getName(), PL_APFileName, PL_AP);

			}
			if (PL_BS != null
					&& (PL_BSFileName.toLowerCase().endsWith(".pdf") || PL_BSFileName
							.toLowerCase().endsWith(".jpg")|| PL_BSFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.BANK_STATEMENT.getName(), PL_BSFileName, PL_BS);

			}
		} else if (ddlBusinessType.equals("PF")) {
			if (PF_ID != null
					&& (PF_IDFileName.toLowerCase().endsWith(".pdf") || PF_IDFileName
							.toLowerCase().endsWith(".jpg")||  PF_IDFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.IDENTIFICATION_DOCUMNETS_ALLPARTNER.getName(), PF_IDFileName, PF_ID);

			}
			if (PF_CTC != null
					&& (PF_CTCFileName.toLowerCase().endsWith(".pdf") || PF_CTCFileName
							.toLowerCase().endsWith(".jpg")||  PF_CTCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.CERTIFIED_TRUE_COPY.getName(), PF_CTCFileName, PF_CTC);

			}
			if (PF_LOP != null
					&& (PF_LOPFileName.toLowerCase().endsWith(".pdf") || PF_LOPFileName
							.toLowerCase().endsWith(".jpg")||  PF_LOPFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.LIST_OF_PARTNER.getName(), PF_LOPFileName, PF_LOP);

			}
			if (PF_PLS != null
					&& (PF_PLSFileName.toLowerCase().endsWith(".pdf") || PF_PLSFileName
							.toLowerCase().endsWith(".jpg")|| PF_PLSFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PARTNERSHIP_LETTER_SIGNED.getName(), PF_PLSFileName, PF_PLS);

			}
			if (PF_PC != null
					&& (PF_PCFileName.toLowerCase().endsWith(".pdf") || PF_PCFileName
							.toLowerCase().endsWith(".jpg")||  PF_PCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PAN_CARD.getName(), PF_PCFileName, PF_PC);

			}
			if (PF_AP != null
					&& (PF_APFileName.toLowerCase().endsWith(".pdf") || PF_APFileName
							.toLowerCase().endsWith(".jpg")||  PF_APFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.ADDRESS_PROOFS_PARTNERSHIP.getName(), PF_APFileName, PF_AP);

			}
			if (PF_IDOAS != null
					&& (PF_IDOASFileName.toLowerCase().endsWith(".pdf") || PF_IDOASFileName
							.toLowerCase().endsWith(".jpg")||  PF_IDOASFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.IDENTIFICATION_DOCUMNETS_AUTHORIZED.getName(), PF_IDOASFileName, PF_IDOAS);

			}
			if (PF_BS != null
					&& (PF_BSFileName.toLowerCase().endsWith(".pdf") || PF_BSFileName
							.toLowerCase().endsWith(".jpg")||  PF_BSFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.BANK_STATEMENT_PARTNERSHIP.getName(), PF_BSFileName, PF_BS);

			}
			if (PF_LUS != null
					&& (PF_LUSFileName.toLowerCase().endsWith(".pdf") || PF_LUSFileName
							.toLowerCase().endsWith(".jpg")|| PF_LUSFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.LICENSE_UNDER_SHOP.getName(), PF_LUSFileName, PF_LUS);

			}

		} else if (ddlBusinessType.equals("PR")) {
			if (PR_ID != null
					&& (PR_IDFileName.toLowerCase().endsWith(".pdf") || PR_IDFileName
							.toLowerCase().endsWith(".jpg")||  PR_IDFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.IDENTIFICATION_DOCUMNETS_PROPRIETOR.getName(), PR_IDFileName, PR_ID);
			}
			if (PR_PAN != null
					&& (PR_PANFileName.toLowerCase().endsWith(".pdf") || PR_PANFileName
							.toLowerCase().endsWith(".jpg")||  PR_PANFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PAN_CARD_PROPRIETOR.getName(), PR_PANFileName, PR_PAN);
			}
			if (PR_POE != null
					&& (PR_POEFileName.toLowerCase().endsWith(".pdf") || PR_POEFileName
							.toLowerCase().endsWith(".jpg")||  PR_POEFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PROOF_OF_ENTITY.getName(), PR_POEFileName, PR_POE);
			}
			if (PR_AP != null
					&& (PR_APFileName.toLowerCase().endsWith(".pdf") || PR_APFileName
							.toLowerCase().endsWith(".jpg")||  PR_APFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.ADDRESS_PROOFS_PROPRIETOR.getName(), PR_APFileName, PR_AP);
			}
			if (PR_BS != null
					&& (PR_BSFileName.toLowerCase().endsWith(".pdf") || PR_BSFileName
							.toLowerCase().endsWith(".jpg")||  PR_BSFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.BANK_STATEMENT_PROPRIETOR.getName(), PR_BSFileName, PR_BS);
			}
			if (PR_LUS != null
					&& (PR_LUSFileName.toLowerCase().endsWith(".pdf") || PR_LUSFileName
							.toLowerCase().endsWith(".jpg")||  PR_LUSFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.LICENSE_UNDER_SHOP_PROPRIETOR.getName(), PR_LUSFileName, PR_LUS);
			}
		} else if (ddlBusinessType.equals("CSA")) {
			if (CSA_BL != null
					&& (CSA_BLFileName.toLowerCase().endsWith(".pdf") || CSA_BLFileName
							.toLowerCase().endsWith(".jpg")|| CSA_BLFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.LAWS.getName(), CSA_BLFileName, CSA_BL);
			}
			if (CSA_GBR != null
					&& (CSA_GBRFileName.toLowerCase().endsWith(".pdf") || CSA_GBRFileName
							.toLowerCase().endsWith(".jpg")||  CSA_GBRFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.GENERAL_BODY_RESOLUTION.getName(), CSA_GBRFileName, CSA_GBR);
			}
			if (CSA_PC != null
					&& (CSA_PCFileName.toLowerCase().endsWith(".pdf") || CSA_PCFileName
							.toLowerCase().endsWith(".jpg")||  CSA_PCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PAN_CARD_CLUB.getName(), CSA_PCFileName, CSA_PC);
			}

		} else if (ddlBusinessType.equals("LLL")) {
			if (LLL_RC != null
					&& (LLL_RCFileName.toLowerCase().endsWith(".pdf") || LLL_RCFileName
							.toLowerCase().endsWith(".jpg")||  LLL_RCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.REGISTRATION_CERTIFICATE.getName(), LLL_RCFileName, LLL_RC);
			}
			if (LLL_LAD != null
					&& (LLL_LADFileName.toLowerCase().endsWith(".pdf") || LLL_LADFileName
							.toLowerCase().endsWith(".jpg")||  LLL_LADFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.LLPA_AGREEMENT.getName(), LLL_LADFileName, LLL_LAD);
			}
			if (LLL_LOP != null
					&& (LLL_LOPFileName.toLowerCase().endsWith(".pdf") || LLL_LOPFileName
							.toLowerCase().endsWith(".jpg")|| LLL_LOPFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.LIST_OF_PARTNERS.getName(), LLL_LOPFileName, LLL_LOP);
			}
			if (LLL_ID != null
					&& (LLL_IDFileName.toLowerCase().endsWith(".pdf") || LLL_IDFileName
							.toLowerCase().endsWith(".jpg")||  LLL_IDFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.IDENTIFICATION_DOCUMNETS_LLP.getName(), LLL_IDFileName, LLL_ID);
			}
			if (LLL_AL != null
					&& (LLL_ALFileName.toLowerCase().endsWith(".pdf") || LLL_ALFileName
							.toLowerCase().endsWith(".jpg")|| LLL_ALFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.AUTHORIZATION_LETTER.getName(), LLL_ALFileName, LLL_AL);
			}
			if (LLL_PCC != null
					&& (LLL_PCCFileName.toLowerCase().endsWith(".pdf") || LLL_PCCFileName
							.toLowerCase().endsWith(".jpg")|| LLL_PCCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PAN_CARD_COMPANY.getName(), CSA_BLFileName, LLL_PCC);
			}
			if (LLL_DR != null
					&& (LLL_DRFileName.toLowerCase().endsWith(".pdf") || LLL_DRFileName
							.toLowerCase().endsWith(".jpg")||  LLL_DRFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.DIN_REGISTRATION.getName(), LLL_DRFileName, LLL_DR);
			}
			if (LLL_AP != null
					&& (LLL_APFileName.toLowerCase().endsWith(".pdf") || LLL_APFileName
							.toLowerCase().endsWith(".jpg")|| LLL_APFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.ADDRESS_PROOFS_LLP.getName(), LLL_APFileName, LLL_AP);
			}
			if (LLL_BS != null
					&& (LLL_BSFileName.toLowerCase().endsWith(".pdf") || LLL_BSFileName
							.toLowerCase().endsWith(".jpg")||  LLL_BSFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.BANK_STATEMENT_LLP.getName(), LLL_BSFileName, CSA_BL);
			}
			if (LLL_PCL != null
					&& (LLL_PCLFileName.toLowerCase().endsWith(".pdf") || LLL_PCLFileName
							.toLowerCase().endsWith(".jpg")||  LLL_PCLFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PAN_OF_LLP.getName(), LLL_PCLFileName, LLL_PCL);
			}
			if (LLL_LUS != null
					&& (LLL_LUSFileName.toLowerCase().endsWith(".pdf") || LLL_LUSFileName
							.toLowerCase().endsWith(".jpg")||  LLL_LUSFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.LICENSE_UNDER_SHOP_LLP.getName(), LLL_LUSFileName, LLL_LUS);
			}

		} else if (ddlBusinessType.equals("RI")) {
			if (RI_P != null
					&& (RI_PFileName.toLowerCase().endsWith(".pdf") || RI_PFileName
							.toLowerCase().endsWith(".jpg")|| RI_PFileName .toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PASSPORT.getName(), RI_PFileName, RI_P);
			}
			if (RI_PC != null
					&& (RI_PCFileName.toLowerCase().endsWith(".pdf") || RI_PCFileName
							.toLowerCase().endsWith(".jpg")||  RI_PCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PAN_CARD_RESIDENT.getName(), RI_PCFileName, RI_PC);
			}
			if (RI_DL != null
					&& (RI_DLFileName.toLowerCase().endsWith(".pdf") || RI_DLFileName
							.toLowerCase().endsWith(".jpg")|| RI_DLFileName .toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.DRIVING_LICENSE.getName(), RI_DLFileName, RI_DL);
			}
			if (RI_BV != null
					&& (RI_BVFileName.toLowerCase().endsWith(".pdf") || RI_BVFileName
							.toLowerCase().endsWith(".jpg")||  RI_BVFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.BANKERS_VERIFICATION.getName(), RI_BVFileName, RI_BV);
			}
		} else if (ddlBusinessType.equals("AP")) {
			if (AP_PIL != null
					&& (AP_PILFileName.toLowerCase().endsWith(".pdf") || AP_PILFileName
							.toLowerCase().endsWith(".jpg")||  AP_PILFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.PAN_INTIMATION.getName(), AP_PILFileName, AP_PIL);
			}
			if (AP_CUB != null
					&& (AP_CUBFileName.toLowerCase().endsWith(".pdf") || AP_CUBFileName
							.toLowerCase().endsWith(".jpg")||  AP_CUBFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.CURRENT_UTILITY_BILL.getName(), AP_CUBFileName, AP_CUB);
			}
			if (AP_MT != null
					&& (AP_MTFileName.toLowerCase().endsWith(".pdf") || AP_MTFileName
							.toLowerCase().endsWith(".jpg")|| AP_MTFileName .toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.MUNICIPAL_TAX.getName(), AP_MTFileName, AP_MT);
			}
			if (AP_EBS != null
					&& (AP_EBSFileName.toLowerCase().endsWith(".pdf") || AP_EBSFileName
							.toLowerCase().endsWith(".jpg")|| AP_EBSFileName .toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.EXISTING_BANKSSTATEMENT.getName(), AP_EBSFileName, AP_EBS);
			}
			if (AP_EBC != null
					&& (AP_EBCFileName.toLowerCase().endsWith(".pdf") || AP_EBCFileName
							.toLowerCase().endsWith(".jpg")||  AP_EBCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.EXISTING_BANKS_CERTIFICATE.getName(), AP_EBCFileName, AP_EBC);
			}
			if (AP_IP != null
					&& (AP_IPFileName.toLowerCase().endsWith(".pdf") || AP_IPFileName
							.toLowerCase().endsWith(".jpg")|| AP_IPFileName .toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.INSURANCE_POLICY.getName(), AP_IPFileName, AP_IP);
			}
			if (AP_AOD != null
					&& (AP_AODFileName.toLowerCase().endsWith(".pdf") || AP_AODFileName
							.toLowerCase().endsWith(".jpg")||  AP_AODFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.ANYOTHER_DOCUMENT.getName(), AP_AODFileName, AP_AOD);
			}

		} else if (ddlBusinessType.equals("T")) {
			if (T_RFB != null
					&& (T_RFBFileName.toLowerCase().endsWith(".pdf") || T_RFBFileName
							.toLowerCase().endsWith(".jpg")|| T_RFBFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.RESOLUTION_FROM_BOARD.getName(), T_RFBFileName, T_RFB);
			}
			if (T_SP != null
					&& (T_SPFileName.toLowerCase().endsWith(".pdf") || T_SPFileName
							.toLowerCase().endsWith(".jpg")|| T_SPFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.SIGNATURE_AND_PHOTO.getName(), T_SPFileName, T_SP);
			}
			if (T_CI != null
					&& (T_CIFileName.toLowerCase().endsWith(".pdf") || T_CIFileName
							.toLowerCase().endsWith(".jpg")||  T_CIFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.CERTIFICATE_ISSUED.getName(), T_CIFileName, T_CI);
			}
			if (T_AC != null
					&& (T_ACFileName.toLowerCase().endsWith(".pdf") || T_ACFileName
							.toLowerCase().endsWith(".jpg")||  T_ACFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.ATTESTED_COPY_DEED.getName(), T_ACFileName, T_AC);
			}
			if (T_DTP != null
					&& (T_DTPFileName.toLowerCase().endsWith(".pdf") || T_DTPFileName
							.toLowerCase().endsWith(".jpg")||  T_DTPFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.DUTY_ATTESTED_PAN.getName(), T_DTPFileName, T_DTP);
			}
			if (T_ABS != null
					&& (T_ABSFileName.toLowerCase().endsWith(".pdf") || T_ABSFileName
							.toLowerCase().endsWith(".jpg")||  T_ABSFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.AUDITED_BALANCE_SHEET.getName(), T_ABSFileName, T_ABS);
			}
			if (T_ST != null
					&& (T_STFileName.toLowerCase().endsWith(".pdf") || T_STFileName
							.toLowerCase().endsWith(".jpg")||  T_STFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.SALES_TAX.getName(), T_STFileName, T_ST);
			}
			if (T_VC != null
					&& (T_VCFileName.toLowerCase().endsWith(".pdf") || T_VCFileName
							.toLowerCase().endsWith(".jpg")||  T_VCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.VOIDED_CHECK.getName(), T_VCFileName, T_VC);
			}
			if (T_POC != null
					&& (T_POCFileName.toLowerCase().endsWith(".pdf") || T_POCFileName
							.toLowerCase().endsWith(".jpg")||  T_POCFileName.toLowerCase().endsWith(".png"))) {
				SaveFile(DocumentNameConstants.CREDIT_CARD_TURNOVER.getName(), T_POCFileName, T_POC);
			}
		}

		return SUCCESS;
	}

	private String SaveFile(String saveName, String filename, File controlFile) {
		
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		if (sessionUser.getUserType().equals(UserType.ADMIN)
				|| sessionUser.getUserType().equals(UserType.SUPERADMIN)) {
			payId = user.getPayId();
		} else {
			payId = sessionUser.getPayId();
		}
		PropertiesManager propertiesManager = new PropertiesManager();
		destPath = propertiesManager.getSystemProperty("DocumentPath")+payId;
		
		saveFilename = payId + "_" + saveName + getFileExtension(filename);
		File destFile = new File(destPath, saveFilename);
		try {
			FileUtils.copyFile(controlFile, destFile);
			
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;

	}

	private String getFileExtension(String name) {
		if (name.toLowerCase().endsWith(".pdf")) {
			return ".pdf";
		} else {
			return ".jpg";
		}
	}

	public String getDdlBusinessType() {
		return ddlBusinessType;
	}

	public void setDdlBusinessType(String ddlBusinessType) {
		this.ddlBusinessType = ddlBusinessType;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getModel() {
		return user;
	}

	public File getPL_AOA() {
		return PL_AOA;
	}

	public void setPL_AOA(File pL_AOA) {
		PL_AOA = pL_AOA;
	}

	public File getPL_MOA() {
		return PL_MOA;
	}

	public void setPL_MOA(File pL_MOA) {
		PL_MOA = pL_MOA;
	}

	public File getPL_COI() {
		return PL_COI;
	}

	public void setPL_COI(File pL_COI) {
		PL_COI = pL_COI;
	}

	public File getPL_COCOB() {
		return PL_COCOB;
	}

	public void setPL_COCOB(File pL_COCOB) {
		PL_COCOB = pL_COCOB;
	}

	public File getPL_POC() {
		return PL_POC;
	}

	public void setPL_POC(File pL_POC) {
		PL_POC = pL_POC;
	}

	public File getPL_IDOAS() {
		return PL_IDOAS;
	}

	public void setPL_IDOAS(File pL_IDOAS) {
		PL_IDOAS = pL_IDOAS;
	}

	public File getPL_BR() {
		return PL_BR;
	}

	public void setPL_BR(File pL_BR) {
		PL_BR = pL_BR;
	}

	public File getPL_LPDOD() {
		return PL_LPDOD;
	}

	public void setPL_LPDOD(File pL_LPDOD) {
		PL_LPDOD = pL_LPDOD;
	}

	public File getPL_AP() {
		return PL_AP;
	}

	public void setPL_AP(File pL_AP) {
		PL_AP = pL_AP;
	}

	public File getPL_BS() {
		return PL_BS;
	}

	public void setPL_BS(File pL_BS) {
		PL_BS = pL_BS;
	}

	public File getPF_ID() {
		return PF_ID;
	}

	public void setPF_ID(File pF_ID) {
		PF_ID = pF_ID;
	}

	public File getPF_CTC() {
		return PF_CTC;
	}

	public void setPF_CTC(File pF_CTC) {
		PF_CTC = pF_CTC;
	}

	public File getPF_LOP() {
		return PF_LOP;
	}

	public void setPF_LOP(File pF_LOP) {
		PF_LOP = pF_LOP;
	}

	public File getPF_PLS() {
		return PF_PLS;
	}

	public void setPF_PLS(File pF_PLS) {
		PF_PLS = pF_PLS;
	}

	public File getPF_PC() {
		return PF_PC;
	}

	public void setPF_PC(File pF_PC) {
		PF_PC = pF_PC;
	}

	public File getPF_AP() {
		return PF_AP;
	}

	public void setPF_AP(File pF_AP) {
		PF_AP = pF_AP;
	}

	public File getPF_IDOAS() {
		return PF_IDOAS;
	}

	public void setPF_IDOAS(File pF_IDOAS) {
		PF_IDOAS = pF_IDOAS;
	}

	public File getPF_BS() {
		return PF_BS;
	}

	public void setPF_BS(File pF_BS) {
		PF_BS = pF_BS;
	}

	public File getPF_LUS() {
		return PF_LUS;
	}

	public void setPF_LUS(File pF_LUS) {
		PF_LUS = pF_LUS;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public File getPR_ID() {
		return PR_ID;
	}

	public void setPR_ID(File pR_ID) {
		PR_ID = pR_ID;
	}

	public File getPR_PAN() {
		return PR_PAN;
	}

	public void setPR_PAN(File pR_PAN) {
		PR_PAN = pR_PAN;
	}

	public File getPR_POE() {
		return PR_POE;
	}

	public void setPR_POE(File pR_POE) {
		PR_POE = pR_POE;
	}

	public File getPR_AP() {
		return PR_AP;
	}

	public void setPR_AP(File pR_AP) {
		PR_AP = pR_AP;
	}

	public File getPR_BS() {
		return PR_BS;
	}

	public void setPR_BS(File pR_BS) {
		PR_BS = pR_BS;
	}

	public File getPR_LUS() {
		return PR_LUS;
	}

	public void setPR_LUS(File pR_LUS) {
		PR_LUS = pR_LUS;
	}

	public String getPL_AOAFileName() {
		return PL_AOAFileName;
	}

	public void setPL_AOAFileName(String pL_AOAFileName) {
		PL_AOAFileName = pL_AOAFileName;
	}

	public String getSaveFilename() {
		return saveFilename;
	}

	public void setSaveFilename(String saveFilename) {
		this.saveFilename = saveFilename;
	}

	public String getPL_MOAFileName() {
		return PL_MOAFileName;
	}

	public void setPL_MOAFileName(String pL_MOAFileName) {
		PL_MOAFileName = pL_MOAFileName;
	}

	public String getPL_COIFileName() {
		return PL_COIFileName;
	}

	public void setPL_COIFileName(String pL_COIFileName) {
		PL_COIFileName = pL_COIFileName;
	}

	public String getPL_COCOBFileName() {
		return PL_COCOBFileName;
	}

	public void setPL_COCOBFileName(String pL_COCOBFileName) {
		PL_COCOBFileName = pL_COCOBFileName;
	}

	public String getPL_POCFileName() {
		return PL_POCFileName;
	}

	public void setPL_POCFileName(String pL_POCFileName) {
		PL_POCFileName = pL_POCFileName;
	}

	public String getPL_IDOASFileName() {
		return PL_IDOASFileName;
	}

	public void setPL_IDOASFileName(String pL_IDOASFileName) {
		PL_IDOASFileName = pL_IDOASFileName;
	}

	public String getPL_BRFileName() {
		return PL_BRFileName;
	}

	public void setPL_BRFileName(String pL_BRFileName) {
		PL_BRFileName = pL_BRFileName;
	}

	public String getPL_LPDODFileName() {
		return PL_LPDODFileName;
	}

	public void setPL_LPDODFileName(String pL_LPDODFileName) {
		PL_LPDODFileName = pL_LPDODFileName;
	}

	public String getPL_APFileName() {
		return PL_APFileName;
	}

	public void setPL_APFileName(String pL_APFileName) {
		PL_APFileName = pL_APFileName;
	}

	public String getPL_BSFileName() {
		return PL_BSFileName;
	}

	public void setPL_BSFileName(String pL_BSFileName) {
		PL_BSFileName = pL_BSFileName;
	}

	public String getPF_IDFileName() {
		return PF_IDFileName;
	}

	public void setPF_IDFileName(String pF_IDFileName) {
		PF_IDFileName = pF_IDFileName;
	}

	public String getPF_CTCFileName() {
		return PF_CTCFileName;
	}

	public void setPF_CTCFileName(String pF_CTCFileName) {
		PF_CTCFileName = pF_CTCFileName;
	}

	public String getPF_LOPFileName() {
		return PF_LOPFileName;
	}

	public void setPF_LOPFileName(String pF_LOPFileName) {
		PF_LOPFileName = pF_LOPFileName;
	}

	public String getPF_PLSFileName() {
		return PF_PLSFileName;
	}

	public void setPF_PLSFileName(String pF_PLSFileName) {
		PF_PLSFileName = pF_PLSFileName;
	}

	public String getPF_PCFileName() {
		return PF_PCFileName;
	}

	public void setPF_PCFileName(String pF_PCFileName) {
		PF_PCFileName = pF_PCFileName;
	}

	public String getPF_APFileName() {
		return PF_APFileName;
	}

	public void setPF_APFileName(String pF_APFileName) {
		PF_APFileName = pF_APFileName;
	}

	public String getPF_IDOASFileName() {
		return PF_IDOASFileName;
	}

	public void setPF_IDOASFileName(String pF_IDOASFileName) {
		PF_IDOASFileName = pF_IDOASFileName;
	}

	public String getPF_BSFileName() {
		return PF_BSFileName;
	}

	public void setPF_BSFileName(String pF_BSFileName) {
		PF_BSFileName = pF_BSFileName;
	}

	public String getPF_LUSFileName() {
		return PF_LUSFileName;
	}

	public void setPF_LUSFileName(String pF_LUSFileName) {
		PF_LUSFileName = pF_LUSFileName;
	}

	public String getPR_IDFileName() {
		return PR_IDFileName;
	}

	public void setPR_IDFileName(String pR_IDFileName) {
		PR_IDFileName = pR_IDFileName;
	}

	public String getPR_PANFileName() {
		return PR_PANFileName;
	}

	public void setPR_PANFileName(String pR_PANFileName) {
		PR_PANFileName = pR_PANFileName;
	}

	public String getPR_POEFileName() {
		return PR_POEFileName;
	}

	public void setPR_POEFileName(String pR_POEFileName) {
		PR_POEFileName = pR_POEFileName;
	}

	public String getPR_APFileName() {
		return PR_APFileName;
	}

	public void setPR_APFileName(String pR_APFileName) {
		PR_APFileName = pR_APFileName;
	}

	public String getPR_BSFileName() {
		return PR_BSFileName;
	}

	public void setPR_BSFileName(String pR_BSFileName) {
		PR_BSFileName = pR_BSFileName;
	}

	public String getPR_LUSFileName() {
		return PR_LUSFileName;
	}

	public void setPR_LUSFileName(String pR_LUSFileName) {
		PR_LUSFileName = pR_LUSFileName;
	}

	public String getLLL_RCFileName() {
		return LLL_RCFileName;
	}

	public void setLLL_RCFileName(String lLL_RCFileName) {
		LLL_RCFileName = lLL_RCFileName;
	}

	public String getLLL_LADFileName() {
		return LLL_LADFileName;
	}

	public void setLLL_LADFileName(String lLL_LADFileName) {
		LLL_LADFileName = lLL_LADFileName;
	}

	public String getLLL_LOPFileName() {
		return LLL_LOPFileName;
	}

	public void setLLL_LOPFileName(String lLL_LOPFileName) {
		LLL_LOPFileName = lLL_LOPFileName;
	}

	public String getLLL_IDFileName() {
		return LLL_IDFileName;
	}

	public void setLLL_IDFileName(String lLL_IDFileName) {
		LLL_IDFileName = lLL_IDFileName;
	}

	public String getLLL_ALFileName() {
		return LLL_ALFileName;
	}

	public void setLLL_ALFileName(String lLL_ALFileName) {
		LLL_ALFileName = lLL_ALFileName;
	}

	public String getLLL_PCCFileName() {
		return LLL_PCCFileName;
	}

	public void setLLL_PCCFileName(String lLL_PCCFileName) {
		LLL_PCCFileName = lLL_PCCFileName;
	}

	public String getLLL_DRFileName() {
		return LLL_DRFileName;
	}

	public void setLLL_DRFileName(String lLL_DRFileName) {
		LLL_DRFileName = lLL_DRFileName;
	}

	public String getLLL_APFileName() {
		return LLL_APFileName;
	}

	public void setLLL_APFileName(String lLL_APFileName) {
		LLL_APFileName = lLL_APFileName;
	}

	public String getLLL_BSFileName() {
		return LLL_BSFileName;
	}

	public void setLLL_BSFileName(String lLL_BSFileName) {
		LLL_BSFileName = lLL_BSFileName;
	}

	public String getLLL_PCLFileName() {
		return LLL_PCLFileName;
	}

	public void setLLL_PCLFileName(String lLL_PCLFileName) {
		LLL_PCLFileName = lLL_PCLFileName;
	}

	public String getLLL_LUSFileName() {
		return LLL_LUSFileName;
	}

	public void setLLL_LUSFileName(String lLL_LUSFileName) {
		LLL_LUSFileName = lLL_LUSFileName;
	}

	public String getCSA_BLFileName() {
		return CSA_BLFileName;
	}

	public void setCSA_BLFileName(String cSA_BLFileName) {
		CSA_BLFileName = cSA_BLFileName;
	}

	public String getCSA_GBRFileName() {
		return CSA_GBRFileName;
	}

	public void setCSA_GBRFileName(String cSA_GBRFileName) {
		CSA_GBRFileName = cSA_GBRFileName;
	}

	public String getCSA_PCFileName() {
		return CSA_PCFileName;
	}

	public void setCSA_PCFileName(String cSA_PCFileName) {
		CSA_PCFileName = cSA_PCFileName;
	}

	public String getRI_PFileName() {
		return RI_PFileName;
	}

	public void setRI_PFileName(String rI_PFileName) {
		RI_PFileName = rI_PFileName;
	}

	public String getRI_PCFileName() {
		return RI_PCFileName;
	}

	public void setRI_PCFileName(String rI_PCFileName) {
		RI_PCFileName = rI_PCFileName;
	}

	public String getRI_DLFileName() {
		return RI_DLFileName;
	}

	public void setRI_DLFileName(String rI_DLFileName) {
		RI_DLFileName = rI_DLFileName;
	}

	public String getRI_BVFileName() {
		return RI_BVFileName;
	}

	public void setRI_BVFileName(String rI_BVFileName) {
		RI_BVFileName = rI_BVFileName;
	}

	public String getAP_PILFileName() {
		return AP_PILFileName;
	}

	public void setAP_PILFileName(String aP_PILFileName) {
		AP_PILFileName = aP_PILFileName;
	}

	public String getAP_CUBFileName() {
		return AP_CUBFileName;
	}

	public void setAP_CUBFileName(String aP_CUBFileName) {
		AP_CUBFileName = aP_CUBFileName;
	}

	public String getAP_MTFileName() {
		return AP_MTFileName;
	}

	public void setAP_MTFileName(String aP_MTFileName) {
		AP_MTFileName = aP_MTFileName;
	}

	public String getAP_EBSFileName() {
		return AP_EBSFileName;
	}

	public void setAP_EBSFileName(String aP_EBSFileName) {
		AP_EBSFileName = aP_EBSFileName;
	}

	public String getAP_EBCFileName() {
		return AP_EBCFileName;
	}

	public void setAP_EBCFileName(String aP_EBCFileName) {
		AP_EBCFileName = aP_EBCFileName;
	}

	public String getAP_IPFileName() {
		return AP_IPFileName;
	}

	public void setAP_IPFileName(String aP_IPFileName) {
		AP_IPFileName = aP_IPFileName;
	}

	public String getAP_AODFileName() {
		return AP_AODFileName;
	}

	public void setAP_AODFileName(String aP_AODFileName) {
		AP_AODFileName = aP_AODFileName;
	}

	public String getT_RFBFileName() {
		return T_RFBFileName;
	}

	public void setT_RFBFileName(String t_RFBFileName) {
		T_RFBFileName = t_RFBFileName;
	}

	public String getT_SPFileName() {
		return T_SPFileName;
	}

	public void setT_SPFileName(String t_SPFileName) {
		T_SPFileName = t_SPFileName;
	}

	public String getT_CIFileName() {
		return T_CIFileName;
	}

	public void setT_CIFileName(String t_CIFileName) {
		T_CIFileName = t_CIFileName;
	}

	public String getT_ACFileName() {
		return T_ACFileName;
	}

	public void setT_ACFileName(String t_ACFileName) {
		T_ACFileName = t_ACFileName;
	}

	public String getT_DTPFileName() {
		return T_DTPFileName;
	}

	public void setT_DTPFileName(String t_DTPFileName) {
		T_DTPFileName = t_DTPFileName;
	}

	public String getT_ABSFileName() {
		return T_ABSFileName;
	}

	public void setT_ABSFileName(String t_ABSFileName) {
		T_ABSFileName = t_ABSFileName;
	}

	public String getT_STFileName() {
		return T_STFileName;
	}

	public void setT_STFileName(String t_STFileName) {
		T_STFileName = t_STFileName;
	}

	public String getT_VCFileName() {
		return T_VCFileName;
	}

	public void setT_VCFileName(String t_VCFileName) {
		T_VCFileName = t_VCFileName;
	}

	public String getT_POCFileName() {
		return T_POCFileName;
	}

	public void setT_POCFileName(String t_POCFileName) {
		T_POCFileName = t_POCFileName;
	}

	public File getCSA_BL() {
		return CSA_BL;
	}

	public void setCSA_BL(File cSA_BL) {
		CSA_BL = cSA_BL;
	}

	public File getCSA_GBR() {
		return CSA_GBR;
	}

	public void setCSA_GBR(File cSA_GBR) {
		CSA_GBR = cSA_GBR;
	}

	public File getCSA_PC() {
		return CSA_PC;
	}

	public void setCSA_PC(File cSA_PC) {
		CSA_PC = cSA_PC;
	}

	public File getLLL_RC() {
		return LLL_RC;
	}

	public void setLLL_RC(File lLL_RC) {
		LLL_RC = lLL_RC;
	}

	public File getLLL_LAD() {
		return LLL_LAD;
	}

	public void setLLL_LAD(File lLL_LAD) {
		LLL_LAD = lLL_LAD;
	}

	public File getLLL_LOP() {
		return LLL_LOP;
	}

	public void setLLL_LOP(File lLL_LOP) {
		LLL_LOP = lLL_LOP;
	}

	public File getLLL_ID() {
		return LLL_ID;
	}

	public void setLLL_ID(File lLL_ID) {
		LLL_ID = lLL_ID;
	}

	public File getLLL_AL() {
		return LLL_AL;
	}

	public void setLLL_AL(File lLL_AL) {
		LLL_AL = lLL_AL;
	}

	public File getLLL_PCC() {
		return LLL_PCC;
	}

	public void setLLL_PCC(File lLL_PCC) {
		LLL_PCC = lLL_PCC;
	}

	public File getLLL_DR() {
		return LLL_DR;
	}

	public void setLLL_DR(File lLL_DR) {
		LLL_DR = lLL_DR;
	}

	public File getLLL_AP() {
		return LLL_AP;
	}

	public void setLLL_AP(File lLL_AP) {
		LLL_AP = lLL_AP;
	}

	public File getLLL_BS() {
		return LLL_BS;
	}

	public void setLLL_BS(File lLL_BS) {
		LLL_BS = lLL_BS;
	}

	public File getLLL_PCL() {
		return LLL_PCL;
	}

	public void setLLL_PCL(File lLL_PCL) {
		LLL_PCL = lLL_PCL;
	}

	public File getLLL_LUS() {
		return LLL_LUS;
	}

	public void setLLL_LUS(File lLL_LUS) {
		LLL_LUS = lLL_LUS;
	}

	public File getRI_P() {
		return RI_P;
	}

	public void setRI_P(File rI_P) {
		RI_P = rI_P;
	}

	public File getRI_PC() {
		return RI_PC;
	}

	public void setRI_PC(File rI_PC) {
		RI_PC = rI_PC;
	}

	public File getRI_DL() {
		return RI_DL;
	}

	public void setRI_DL(File rI_DL) {
		RI_DL = rI_DL;
	}

	public File getRI_BV() {
		return RI_BV;
	}

	public void setRI_BV(File rI_BV) {
		RI_BV = rI_BV;
	}

	public File getAP_PIL() {
		return AP_PIL;
	}

	public void setAP_PIL(File aP_PIL) {
		AP_PIL = aP_PIL;
	}

	public File getAP_CUB() {
		return AP_CUB;
	}

	public void setAP_CUB(File aP_CUB) {
		AP_CUB = aP_CUB;
	}

	public File getAP_MT() {
		return AP_MT;
	}

	public void setAP_MT(File aP_MT) {
		AP_MT = aP_MT;
	}

	public File getAP_EBS() {
		return AP_EBS;
	}

	public void setAP_EBS(File aP_EBS) {
		AP_EBS = aP_EBS;
	}

	public File getAP_EBC() {
		return AP_EBC;
	}

	public void setAP_EBC(File aP_EBC) {
		AP_EBC = aP_EBC;
	}

	public File getAP_IP() {
		return AP_IP;
	}

	public void setAP_IP(File aP_IP) {
		AP_IP = aP_IP;
	}

	public File getAP_AOD() {
		return AP_AOD;
	}

	public void setAP_AOD(File aP_AOD) {
		AP_AOD = aP_AOD;
	}

	public File getT_RFB() {
		return T_RFB;
	}

	public void setT_RFB(File t_RFB) {
		T_RFB = t_RFB;
	}

	public File getT_SP() {
		return T_SP;
	}

	public void setT_SP(File t_SP) {
		T_SP = t_SP;
	}

	public File getT_CI() {
		return T_CI;
	}

	public void setT_CI(File t_CI) {
		T_CI = t_CI;
	}

	public File getT_AC() {
		return T_AC;
	}

	public void setT_AC(File t_AC) {
		T_AC = t_AC;
	}

	public File getT_DTP() {
		return T_DTP;
	}

	public void setT_DTP(File t_DTP) {
		T_DTP = t_DTP;
	}

	public File getT_ABS() {
		return T_ABS;
	}

	public void setT_ABS(File t_ABS) {
		T_ABS = t_ABS;
	}

	public File getT_ST() {
		return T_ST;
	}

	public void setT_ST(File t_ST) {
		T_ST = t_ST;
	}

	public File getT_VC() {
		return T_VC;
	}

	public void setT_VC(File t_VC) {
		T_VC = t_VC;
	}

	public File getT_POC() {
		return T_POC;
	}

	public void setT_POC(File t_POC) {
		T_POC = t_POC;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

}
