package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.Payment;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.AcquirerType;

public class SurchargeFilterAction extends AbstractSecureAction {
	private static Logger logger = Logger.getLogger(SurchargeFilterAction.class.getName());

	private static final long serialVersionUID = -7123662871783720276L;

	private String payId;
	private String acquirer;
	Set<Account> acquirerSet;
	private List<String> acquirerList = new ArrayList<String>();
	Set<Payment> paymentTypeSet;
	private List<String> paymentTypeList = new ArrayList<String>();
	public List<Merchants> listMerchant = new ArrayList<Merchants>();

	@SuppressWarnings("unchecked")
	public String execute() {
		UserDao userDao = new UserDao();
		setListMerchant(userDao.getActiveMerchantList());
		try {
			User user = new User();
			user = userDao.findPayId(getPayId());

			acquirerSet = user.getAccounts();
			for (Account account : acquirerSet) {
				acquirerList.add(account.getAcquirerName());
			}
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public String fetchPaymentType() {

		UserDao userdao = new UserDao();
		User user = new User();
		user = userdao.findPayId(payId);
		user.getAccounts();
		AcquirerType atype = AcquirerType.getInstancefromName(acquirer);
		String acquirerCode = atype.getCode();
		Account account = user.getAccountUsingAcquirerCode(acquirerCode);

		if (account == null) {
			return null;
		}

		Session session = null;
		try {
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			session.load(account, account.getId());
			paymentTypeSet = account.getPayments();

			for (Payment payment : paymentTypeSet) {

				if (payment.getPaymentType() != null && payment.getPaymentType().toString() != "") {
					paymentTypeList.add(payment.getPaymentType().getName());
				}
			}

			tx.commit();
		} finally {
			HibernateSessionProvider.closeSession(session);
		}
		return SUCCESS;
	}

	public List<Merchants> getListMerchant() {
		return listMerchant;
	}

	public void setListMerchant(List<Merchants> listMerchant) {
		this.listMerchant = listMerchant;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public List<String> getAcquirerList() {
		return acquirerList;
	}

	public void setAcquirerList(List<String> acquirerList) {
		this.acquirerList = acquirerList;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	public List<String> getPaymentTypeList() {
		return paymentTypeList;
	}

	public void setPaymentTypeList(List<String> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}
}
