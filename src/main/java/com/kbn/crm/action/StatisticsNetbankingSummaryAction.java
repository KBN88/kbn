package com.kbn.crm.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.StatisticsService;
import com.kbn.commons.user.Statistics;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class StatisticsNetbankingSummaryAction extends AbstractSecureAction{

	/**
	 * Neeraj Kumar
	 */
	private static final long serialVersionUID = 1563464276690153321L;
	private static Logger logger=Logger.getLogger(StatisticsNetbankingSummaryAction.class.getName());
	private StatisticsService  getStatistics = new StatisticsService();
	private Statistics statistics = new Statistics();
	private String emailId;
	private String currency;
	private String dateFrom;
	private String dateTo;
	
	public String creditCardTransactionSummary(){
		try{
			 Calendar cal = Calendar.getInstance();
	         DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	         Date newDate=df.parse(dateTo);
	         cal.setTime(newDate);
	         cal.add(Calendar.DAY_OF_MONTH, 1);
	         Date date2 = cal.getTime();
	         dateTo=df.format(date2);
			UserDao userDao = new UserDao();
			User user = (User) sessionMap.get(Constants.USER.getValue());
			if (user.getUserType().equals(UserType.MERCHANT)
					|| user.getUserType().equals(UserType.POSMERCHANT)) {
				setStatistics(getStatistics.getCreditCardsDashboardValues(
						user.getPayId(), getCurrency(), getDateFrom(),
						getDateTo()));

			} else {
				if (getEmailId().equals(
						CrmFieldConstants.ALL_MERCHANTS.getValue())) {
					setStatistics(getStatistics.getCreditCardsDashboardValues(
							getEmailId(), getCurrency(), getDateFrom(),
							getDateTo()));
				} else {
					setStatistics(getStatistics.getCreditCardsDashboardValues(userDao
							.findPayIdByEmail(getEmailId()).getPayId(),
							getCurrency(), getDateFrom(), getDateTo()));
				}
			}
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		
	}
	
	public String creditCardMopTypeSummary(){
		try{
			
			 Calendar cal = Calendar.getInstance();
	         DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	         Date newDate=df.parse(dateTo);
	         cal.setTime(newDate);
	         cal.add(Calendar.DAY_OF_MONTH, 1);
	         Date date2 = cal.getTime();
	         dateTo=df.format(date2);  
			UserDao userDao = new UserDao();
			User user = (User) sessionMap.get(Constants.USER.getValue());
			if (user.getUserType().equals(UserType.MERCHANT)
					|| user.getUserType().equals(UserType.POSMERCHANT)) {
				setStatistics(getStatistics.getCreditCardMopTypeValue(
						user.getPayId(), getCurrency(), getDateFrom(),
						getDateTo()));

			} else {
				if (getEmailId().equals(
						CrmFieldConstants.ALL_MERCHANTS.getValue())) {
					setStatistics(getStatistics.getCreditCardMopTypeValue(
							getEmailId(), getCurrency(), getDateFrom(),
							getDateTo()));
				} else {
					setStatistics(getStatistics.getCreditCardMopTypeValue(userDao
							.findPayIdByEmail(getEmailId()).getPayId(),
							getCurrency(), getDateFrom(), getDateTo()));
				}
			}
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		
		
	}
	public String debitCardMopTypeSummary(){
		try{
			 Calendar cal = Calendar.getInstance();
	         DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	         Date newDate=df.parse(dateTo);
	         cal.setTime(newDate);
	         cal.add(Calendar.DAY_OF_MONTH, 1);
	         Date date2 = cal.getTime();
	         dateTo=df.format(date2);
			UserDao userDao = new UserDao();
			User user = (User) sessionMap.get(Constants.USER.getValue());
			if (user.getUserType().equals(UserType.MERCHANT)
					|| user.getUserType().equals(UserType.POSMERCHANT)) {
				setStatistics(getStatistics.getDibetCardMopTypeValue(
						user.getPayId(), getCurrency(), getDateFrom(),
						getDateTo()));

			} else {
				if (getEmailId().equals(
						CrmFieldConstants.ALL_MERCHANTS.getValue())) {
					setStatistics(getStatistics.getDibetCardMopTypeValue(
							getEmailId(), getCurrency(), getDateFrom(),
							getDateTo()));
				} else {
					setStatistics(getStatistics.getDibetCardMopTypeValue(userDao
							.findPayIdByEmail(getEmailId()).getPayId(),
							getCurrency(), getDateFrom(), getDateTo()));
				}
			}
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		
	}
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public Statistics getStatistics() {
		return statistics;
	}
	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}
	
}
