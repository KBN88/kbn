package com.kbn.crm.action;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionReportService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.commons.util.DateCreater;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 *
 */

public class TransactionSummaryReportAction extends AbstractSecureAction {

	private static Logger logger = Logger
			.getLogger(TransactionSummaryReportAction.class.getName());
	private static final long serialVersionUID = -3131381841294843726L;

	private String dateFrom;
	private String dateTo;
	public String paymentType;
	public String acquirer;
	private String merchantEmailId;
	private String currency;
	private int draw;
	private int length;
	private int start;
	private BigInteger recordsTotal;
	public  BigInteger recordsFiltered;
	private List<TransactionSummaryReport> aaData = new ArrayList<TransactionSummaryReport>();

	public String execute() {
		TransactionReportService reportGen = new TransactionReportService();
	    User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier userIdentifier = new SessionUserIdentifier();

		try {
			String merchantPayId = userIdentifier.getMerchantPayId(sessionUser, getMerchantEmailId());
            if(sessionUser.getUserType().equals(UserType.SUBACQUIRER)){
            	String   payId  =  sessionUser.getParentPayId();
            	   UserDao userDao = new UserDao();
    			   User  sessionSubAcquirerUser= userDao.getUserClass(payId);
				setRecordsTotal((reportGen.getAcquirerTransactionListCountList(getDateFrom(), getDateTo(),
						merchantPayId, getPaymentType(), sessionSubAcquirerUser.getFirstName(), getCurrency(), sessionUser)));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
			setAaData(encoder.encodeTransactionSummary(reportGen.getAcquirerTransactionList(getDateFrom(), getDateTo(),
					merchantPayId, getPaymentType(), sessionSubAcquirerUser.getFirstName(), getCurrency(), sessionUser,getStart(),getLength())));
				recordsFiltered = recordsTotal;
				return SUCCESS;
			}else if (sessionUser.getUserType().equals(UserType.RESELLER)){
				setRecordsTotal((reportGen.getResellerTransactionListCountList(getDateFrom(), getDateTo(),
						merchantPayId, getPaymentType(), getAcquirer(), getCurrency(), sessionUser)));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
				setAaData(encoder.encodeTransactionSummary(reportGen.getResellerTransactionList(getDateFrom(), getDateTo(),
						merchantPayId, getPaymentType(), getAcquirer(), getCurrency(), sessionUser,getStart(),getLength())));
				recordsFiltered = recordsTotal;
				return SUCCESS;
			}else if(sessionUser.getUserType().equals(UserType.ACQUIRER)){
				setRecordsTotal((reportGen.getAcquirerTransactionListCountList(getDateFrom(), getDateTo(),
						merchantPayId, getPaymentType(), sessionUser.getFirstName(), getCurrency(), sessionUser)));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
			setAaData(encoder.encodeTransactionSummary(reportGen.getAcquirerTransactionList(getDateFrom(), getDateTo(),
					merchantPayId, getPaymentType(), sessionUser.getFirstName(), getCurrency(), sessionUser,getStart(),getLength())));
			recordsFiltered = recordsTotal;
			return SUCCESS;
			}
			else{
				setRecordsTotal((reportGen.getTransactionListCountList(getDateFrom(), getDateTo(),
						merchantPayId, getPaymentType(), getAcquirer(), getCurrency(), sessionUser)));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
			setAaData(encoder.encodeTransactionSummary(reportGen.getTransactionList(getDateFrom(), getDateTo(),
					merchantPayId, getPaymentType(), getAcquirer(), getCurrency(), sessionUser,getStart(),getLength())));
			recordsFiltered = recordsTotal;
			return SUCCESS;
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return SUCCESS;
		}
	}

	public void validate(){
		CrmValidator validator = new CrmValidator();

		if(validator.validateBlankField(getAcquirer())){
		}
		else if(!validator.validateField(CrmFieldType.ACQUIRER, getAcquirer())){
			addFieldError(CrmFieldType.ACQUIRER.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

        if(validator.validateBlankField(getCurrency())){
		}
        else if(!validator.validateField(CrmFieldType.CURRENCY, getCurrency())){
        	addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

        if(validator.validateBlankField(getDateFrom())){
        }
        else if(!validator.validateField(CrmFieldType.DATE_FROM, getDateFrom())){
        	addFieldError(CrmFieldType.DATE_FROM.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

        if(validator.validateBlankField(getDateTo())){
        }
        else if(!validator.validateField(CrmFieldType.DATE_TO, getDateTo())){
        	addFieldError(CrmFieldType.DATE_TO.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
        
        if(!validator.validateBlankField(getDateTo())){
  	       if(DateCreater.formatStringToDate(DateCreater.formatFromDate(getDateFrom())).compareTo(DateCreater.formatStringToDate(DateCreater.formatFromDate(getDateTo()))) > 0) {
  	        	addFieldError(CrmFieldType.DATE_FROM.getName(), CrmFieldConstants.FROMTO_DATE_VALIDATION.getValue());
  	        }
  	        else if(DateCreater.diffDate(getDateFrom(), getDateTo()) > 31) {
  	        	addFieldError(CrmFieldType.DATE_FROM.getName(), CrmFieldConstants.DATE_RANGE.getValue());
  	        }
        }
        
        if(validator.validateBlankField(getMerchantEmailId()) || getMerchantEmailId().equals(CrmFieldConstants.ALL.getValue())){
		}
        else if(!validator.validateField(CrmFieldType.MERCHANT_EMAIL_ID, getMerchantEmailId())){
        	addFieldError(CrmFieldType. MERCHANT_EMAIL_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if(validator.validateBlankField(getPaymentType())){
		}
		else if(!validator.validateField(CrmFieldType.PAYMENT_TYPE, getPaymentType())){
        	addFieldError(CrmFieldType.PAYMENT_TYPE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
    }

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public List<TransactionSummaryReport> getAaData() {
		return aaData;
	}

	public void setAaData(List<TransactionSummaryReport> aaData) {
		this.aaData = aaData;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getMerchantEmailId() {
		return merchantEmailId;
	}

	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public BigInteger getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(BigInteger recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public BigInteger getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(BigInteger recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

}
