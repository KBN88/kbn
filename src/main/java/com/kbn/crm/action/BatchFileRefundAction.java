package com.kbn.crm.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.User;
import com.kbn.commons.util.CommanCsvReader;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.actionBeans.BatchResponseObject;
import com.kbn.crm.actionBeans.RefundProcessor;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class BatchFileRefundAction extends AbstractSecureAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3340018960360893260L;
	private static Logger logger = Logger.getLogger(TransactionStatusUpdater.class.getName());
	private String fileName;
	private String response;
	private List<Merchants> merchantList = new LinkedList<Merchants>();
	private Map<String, String> currencyMap = new HashMap<String, String>();

	public String readDataFromCsv() throws IOException, SystemException {
		BatchResponseObject batchResponseObject = new BatchResponseObject();
		CommanCsvReader commanCsvReader = new CommanCsvReader();
		String processorResponse;
		try {
			// batchFile read line by line
			batchResponseObject = commanCsvReader.createRefundList(fileName);
			if (batchResponseObject.getBatchTransactionList().isEmpty()) {
				addActionMessage(ErrorType.INVALID_FIELD.getResponseMessage());
			} else {
				User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
				RefundProcessor refundProcessor = new RefundProcessor();
				HttpServletRequest request = ServletActionContext.getRequest();
				// RefundProcessor is used to processes, all refund process
				processorResponse = refundProcessor.processAll(batchResponseObject.getBatchTransactionList(),
						sessionUser, request.getRemoteAddr());
				response = new StringBuilder().append(batchResponseObject.getResponseMessage()).append("\tand\n")
						.append(processorResponse).toString();
				addActionMessage(response);
				if (StringUtils.isEmpty(response)) {
					setResponse((CrmFieldConstants.PROCESS_INITIATED_SUCCESSFULLY.getValue()));
					addActionMessage(response);
				}
			}
		} catch (SystemException systemException) {
			logger.error("Error while processing batch refund:" + systemException);
			setResponse(systemException.getMessage());
			addActionMessage(systemException.getMessage());
		} catch (Exception exception) {
			logger.error("Error while processing batch refund:" + exception);
			setResponse(ErrorType.REFUND_NOT_SUCCESSFULL.getResponseMessage());
			addActionMessage("Error while processing batch refund:" + exception);
		}
		return INPUT;
	}

	public String getFileName() {
		return fileName;
	}

	public List<Merchants> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<Merchants> merchantList) {
		this.merchantList = merchantList;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}

	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}