package com.kbn.crm.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.BarChartService;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class PaymentMethodChartAction extends AbstractSecureAction {

	/**
	 * Neeraj
	 */
	private static Logger logger = Logger.getLogger(ChartAction.class.getName());
	private static final long serialVersionUID = 187302492922294568L;
	private PieChart pieChart=new PieChart();
	private BarChartService getBarChartService=new BarChartService();
	private String emailId;
	private String currency;
	private String dateFrom;
	private String dateTo;
	 public String execute(){
		 try{
		 Calendar cal = Calendar.getInstance();
         DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
         Date newDate=df.parse(dateTo);
         cal.setTime(newDate);
         cal.add(Calendar.DAY_OF_MONTH, 1);
         Date date2 = cal.getTime();
         dateTo=df.format(date2);
 
			 UserDao userDao=new UserDao();
			User user=(User) sessionMap.get(Constants.USER.getValue());
			if(user.getUserType().equals(UserType.RESELLER)){
				setPieChart(getBarChartService.prepareResellerlist(user.getPayId(),user.getResellerId(),getCurrency(),getDateFrom(),getDateTo()));
				
				if(getEmailId().equals(CrmFieldConstants.ALL_MERCHANTS.getValue())) {
					setPieChart(getBarChartService.prepareResellerlist(getEmailId(),user.getResellerId(),getCurrency(),getDateFrom(),getDateTo()));
				}
				else {
					setPieChart(getBarChartService.getPaymentMethodDashboardValues(userDao.findPayIdByEmail(getEmailId()).getPayId(),getCurrency(),getDateFrom(),getDateTo()));
				}
			 return SUCCESS;
			}
			else{
			if (user.getUserType().equals(UserType.MERCHANT) || user.getUserType().equals(UserType.POSMERCHANT)) {
			 setPieChart(getBarChartService.getPaymentMethodDashboardValues(user.getPayId(), getCurrency(),getDateFrom(),getDateTo()));
				
			}
			else
			{
				if(getEmailId().equals(CrmFieldConstants.ALL_MERCHANTS.getValue())) {
					setPieChart(getBarChartService.getPaymentMethodDashboardValues(getEmailId(), getCurrency(),getDateFrom(),getDateTo()));
				}
				else {
					setPieChart(getBarChartService.getPaymentMethodDashboardValues(userDao.findPayIdByEmail(getEmailId()).getPayId(),getCurrency(),getDateFrom(),getDateTo()));
				}
			}
			 return SUCCESS;
			}
		 }catch(Exception exception){
			 logger.error("Exception", exception);
				return ERROR;
		 }

}
	public PieChart getPieChart() {
		return pieChart;
	}
	public void setPieChart(PieChart pieChart) {
		this.pieChart = pieChart;
	}
	public BarChartService getGetBarChartService() {
		return getBarChartService;
	}
	public void setGetBarChartService(BarChartService getBarChartService) {
		this.getBarChartService = getBarChartService;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

}
