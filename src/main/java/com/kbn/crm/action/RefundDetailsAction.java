package com.kbn.crm.action;

import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionHistory;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.crm.actionBeans.RefundDetailsProvider;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Chandan,Puneet
 *
 */
public class RefundDetailsAction extends AbstractSecureAction{
	
	private static Logger logger = Logger.getLogger(RefundDetailsAction.class.getName());
	private static final long serialVersionUID = -1573507922725429926L;

	private List<TransactionHistory> oldTransactions;
	private TransactionHistory transDetails = new TransactionHistory();

	private String transactionId;
	private String orderId;
	private String payId;
			
	public String execute() {
		try{
		getTransactionDetails();	
		return SUCCESS;
		}
		catch(Exception exception){
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	private void getTransactionDetails() throws SystemException{
		RefundDetailsProvider refundDetailsProvider = new RefundDetailsProvider(orderId, payId, transactionId);
		refundDetailsProvider.getAllTransactions();
		transDetails = refundDetailsProvider.getTransDetails();
		setOldTransactions(refundDetailsProvider.getOldTransactions());
		transDetails.setMopType(MopType.getmopName(transDetails.getMopType()));
		transDetails.setPaymentType(PaymentType.getpaymentName(transDetails.getPaymentType()));
	}
	
	public void validate(){
		CrmValidator validator = new CrmValidator();
		
		if(validator.validateBlankField(getPayId())){
		}
		else if(!validator.validateField(CrmFieldType.PAY_ID, getPayId())){
			addFieldError(CrmFieldType.PAY_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if(validator.validateBlankField(getOrderId())){
		}
		else if(!validator.validateField(CrmFieldType.ORDER_ID, getOrderId())){
			addFieldError(CrmFieldType.ORDER_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if(validator.validateBlankField(getTransactionId())){
		}
		else if(!validator.validateField(CrmFieldType.TRANSACTION_ID, getTransactionId())){
			addFieldError(CrmFieldType.TRANSACTION_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public TransactionHistory getTransDetails() {
		return transDetails;
	}

	public void setTransDetails(TransactionHistory transDetails) {
		this.transDetails = transDetails;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public List<TransactionHistory> getOldTransactions() {
		return oldTransactions;
	}

	public void setOldTransactions(List<TransactionHistory> oldTransactions) {
		this.oldTransactions = oldTransactions;
	}
}
