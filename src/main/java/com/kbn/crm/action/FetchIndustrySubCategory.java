package com.kbn.crm.action;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.util.BusinessType;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 *
 */
public class FetchIndustrySubCategory extends AbstractSecureAction {
	private static Logger logger = Logger.getLogger(FetchIndustrySubCategory.class.getName());
	private static final long serialVersionUID = -452094231699163577L;

	private List<String> subCategories = new LinkedList<String>();
	private String industryCategory;

	public String execute(){
		try{
			setSubCategories(BusinessType.getIndustrySubcategory(industryCategory));
		}catch(Exception exception){
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public List<String> getSubCategories() {
		return subCategories;
	}
	public void setSubCategories(List<String> subCategories) {
		this.subCategories = subCategories;
	}

	public String getIndustryCategory() {
		return industryCategory;
	}

	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}

}
