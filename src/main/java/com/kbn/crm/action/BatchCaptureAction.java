package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.BatchTransactionObj;
import com.kbn.crm.actionBeans.CaptureProcessor;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class BatchCaptureAction extends AbstractSecureAction {

	private static final long serialVersionUID = -4502244100134090359L;
	private static Logger logger = Logger.getLogger(BatchCaptureAction.class
			.getName());

	private List<BatchTransactionObj> transactionList = new ArrayList<BatchTransactionObj>();
	private String response;

	public String execute() {
		try {
			CaptureProcessor captureProcessor = new CaptureProcessor();
			response = captureProcessor.processAll(transactionList);
			if (StringUtils.isEmpty(response)) {
				setResponse(ErrorType.CAPTURE_SUCCESSFULL.getResponseMessage());
			}
		} catch (Exception exception) {
			logger.error("Error while capturing transaction:" + exception);
			setResponse(ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
		}
		transactionList.clear();
		return SUCCESS;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<BatchTransactionObj> getTransactionList() {
		return transactionList;
	}

	public void setTransactionList(List<BatchTransactionObj> transactionList) {
		this.transactionList = transactionList;
	}
}
