package com.kbn.crm.action;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.dao.NotificationDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.Messages;
import com.kbn.commons.user.Surcharge;
import com.kbn.commons.user.SurchargeDao;
import com.kbn.commons.user.SurchargeDetails;
import com.kbn.commons.user.SurchargeDetailsDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.NotificationStatusType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.NotificationDetail;

public class SurchargeDetailEditAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(SurchargeDetailEditAction.class.getName());
	private static final long serialVersionUID = -6517340843571949786L;

	private SurchargeDetails surchargeDetails = new SurchargeDetails();
	private String emailId;
	private String paymentType;
	private String response;

	private BigDecimal surchargeAmount;
	private BigDecimal surchargePercentage;
	private String paymentTypeName;
	private String loginEmailId;

	private String userType;
	private Date currentDate = new Date();

	public String execute() {

		UserDao userDao = new UserDao();
		User user = userDao.findPayIdByEmail(emailId);
		paymentTypeName = PaymentType.getpaymentName(paymentType);
		String payId = user.getPayId();

		try {
			SurchargeDao surchargeDao = new SurchargeDao();
			List<Surcharge> surchargeList = surchargeDao.findSurchargeListByPayid(payId, paymentTypeName);

			for (Surcharge surcharge : surchargeList) {
				if (surcharge.getBankSurchargeAmount().compareTo(surchargeAmount) > 0
						|| surcharge.getBankSurchargePercentage().compareTo(surchargePercentage) > 0) {
					return ERROR;
				}
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		try {

			SurchargeDetailsDao surchargeDetailsDao = new SurchargeDetailsDao();
			SurchargeDetails surchargeDetailsFromDb = surchargeDetailsDao.findDetails(payId, paymentTypeName);

			StringBuilder permissions = new StringBuilder();
			permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

			if (surchargeDetailsFromDb != null) {

				// If userType is admin , change surcharge status to inactive
				// and create new surcharge
				if (userType.equals(UserType.ADMIN.toString()) || permissions.toString().contains("Create Surcharge")) {

					disablePreviousCharge(surchargeDetailsFromDb);
					cancelPendingCharge(payId, paymentTypeName);
					createNewSurcharge(payId, TDRStatus.ACTIVE);

				} else {
					
					if (checkExistingSurchargeRequest(payId, paymentTypeName)){
						
						setResponse(ErrorType.MERCHANT_SURCHARGE_REQUEST_ALREADY_PENDING.getResponseMessage());
						return SUCCESS;
					}
					cancelPendingCharge(payId, paymentTypeName);
					createNewSurcharge(payId, TDRStatus.PENDING);
					setResponse(ErrorType.MERCHANT_SURCHARGE_REQUEST_SENT_FOR_APPROVAL.getResponseMessage());
					return SUCCESS;
				}

			} else {

				
				if (userType.equals(UserType.ADMIN.toString()) || permissions.toString().contains("Create Surcharge")) {
					cancelPendingCharge(payId, paymentTypeName);
					createNewSurcharge(payId, TDRStatus.ACTIVE);
				} else {
					
					if (checkExistingSurchargeRequest(payId, paymentTypeName)){
						
						setResponse(ErrorType.MERCHANT_SURCHARGE_REQUEST_ALREADY_PENDING.getResponseMessage());
						return SUCCESS;
					}
					createNewSurcharge(payId, TDRStatus.PENDING);
					setResponse(ErrorType.MERCHANT_SURCHARGE_REQUEST_SENT_FOR_APPROVAL.getResponseMessage());
					return SUCCESS;
				}
			}

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		setResponse(ErrorType.MERCHANT_SURCHARGE_UPDATED.getResponseMessage());
		return SUCCESS;
	}

	public void disablePreviousCharge(SurchargeDetails surchargeDetailsFromDb) {

		Session session = null;
		Long id = surchargeDetailsFromDb.getId();
		session = HibernateSessionProvider.getSession();
		Transaction tx = session.beginTransaction();
		session.load(surchargeDetailsFromDb, surchargeDetailsFromDb.getId());
		try {
			SurchargeDetails surchargeDetails = (SurchargeDetails) session.get(SurchargeDetails.class, id);
			surchargeDetails.setStatus(TDRStatus.INACTIVE);
			surchargeDetails.setUpdatedDate(currentDate);
			surchargeDetails.setProcessedBy(loginEmailId);
			session.update(surchargeDetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	public boolean checkExistingSurchargeRequest(String payId , String paymentTypeName){
		
		SurchargeDetails surchargeDetails = new SurchargeDetails();
		surchargeDetails = new SurchargeDetailsDao().findPendingDetails(payId, paymentTypeName);
		if (surchargeDetails != null){
			return true;
		}
		return false;
	}
	public void cancelPendingCharge(String payId, String paymentType) {

		Session session = null;

		SurchargeDetailsDao surchargeDetailsDao = new SurchargeDetailsDao();
		SurchargeDetails surchargeDetailsFromDb = surchargeDetailsDao.findPendingDetails(payId, paymentType);
		if (surchargeDetailsFromDb != null) {
			Long id = surchargeDetailsFromDb.getId();
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			session.load(surchargeDetailsFromDb, surchargeDetailsFromDb.getId());
			try {
				SurchargeDetails surchargeDetails = (SurchargeDetails) session.get(SurchargeDetails.class, id);
				surchargeDetails.setStatus(TDRStatus.CANCELLED);
				surchargeDetails.setUpdatedDate(currentDate);
				surchargeDetails.setProcessedBy(loginEmailId);
				session.update(surchargeDetails);
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null)
					tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
		}
	}

	public void createNewSurcharge(String payId, TDRStatus status) {
		SurchargeDetailsDao surchargeDetailsDao = new SurchargeDetailsDao();

		SurchargeDetails newSurchargeDetails = new SurchargeDetails();
		newSurchargeDetails.setPayId(payId);
		newSurchargeDetails.setPaymentType(paymentTypeName);
		newSurchargeDetails.setSurchargeAmount(surchargeAmount);
		newSurchargeDetails.setSurchargePercentage(surchargePercentage);
		newSurchargeDetails.setStatus(status);
		newSurchargeDetails.setCreatedDate(currentDate);
		newSurchargeDetails.setUpdatedDate(currentDate);
		newSurchargeDetails.setId(null);
		newSurchargeDetails.setRequestedBy(loginEmailId);
		if (status.equals(TDRStatus.ACTIVE)){
			newSurchargeDetails.setProcessedBy(loginEmailId);
		}
		surchargeDetailsDao.create(newSurchargeDetails);
		
		NotificationDetail notificationDetail = new NotificationDetail();
		notificationDetail.setCreateDate(currentDate);
		notificationDetail.setSubmittedBy(loginEmailId);
		notificationDetail.setConcernedUser(emailId);
		notificationDetail.setStatus(NotificationStatusType.UNREAD.getName());
		notificationDetail.setMessage(Messages.MERCHANT_SURCHARGE_MESSAGE.getResponseMessage());
		NotificationDao notificationDao = new NotificationDao();
		notificationDao.create(notificationDetail);
	}

	public BigDecimal getSurchargeAmount() {
		return surchargeAmount;
	}

	public void setSurchargeAmount(BigDecimal surchargeAmount) {
		this.surchargeAmount = surchargeAmount;
	}

	public BigDecimal getSurchargePercentage() {
		return surchargePercentage;
	}

	public void setSurchargePercentage(BigDecimal surchargePercentage) {
		this.surchargePercentage = surchargePercentage;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public SurchargeDetails getSurchargeDetails() {
		return surchargeDetails;
	}

	public void setSurchargeDetails(SurchargeDetails surchargeDetails) {
		this.surchargeDetails = surchargeDetails;
	}
	
	public String getLoginEmailId() {
		return loginEmailId;
	}

	public void setLoginEmailId(String loginEmailId) {
		this.loginEmailId = loginEmailId;
	}
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
