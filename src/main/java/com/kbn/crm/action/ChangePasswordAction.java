package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.actionBeans.ChangeUserPassword;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.crm.actionBeans.SessionCleaner;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author ISHA
 *
 */

public class ChangePasswordAction extends AbstractSecureAction {
	private static Logger logger = Logger.getLogger(ChangePasswordAction.class
			.getName());
	private static final long serialVersionUID = -6122140077608015851L;
	private String oldPassword;
	private String newPassword;
	private String confirmNewPassword;
	private String response;

	public String execute() {
		ChangeUserPassword changeUserPassword = new ChangeUserPassword();
		ResponseObject responseObject = new ResponseObject();
		try {
			User sessionUser = (User) (sessionMap
					.get(Constants.USER.getValue()));
			responseObject = changeUserPassword.changePassword(
					sessionUser.getEmailId(), oldPassword, newPassword);

			if (responseObject.getResponseCode().equals(
					ErrorType.PASSWORD_MISMATCH.getResponseCode())) {
				addFieldError(CrmFieldConstants.OLD_PASSWORD.getValue(),
						ErrorType.PASSWORD_MISMATCH.getResponseMessage());
				setResponse(ErrorType.PASSWORD_MISMATCH.getResponseMessage());//TODO
				return SUCCESS;
			} else if (responseObject.getResponseCode().equals(
					ErrorType.OLD_PASSWORD_MATCH.getResponseCode())) {
				 addFieldError(CrmFieldConstants.OLD_PASSWORD.getValue(),
						ErrorType.OLD_PASSWORD_MATCH.getResponseMessage()); 
				 setResponse(ErrorType.OLD_PASSWORD_MATCH.getResponseMessage());
				return SUCCESS;
			}
			setResponse(ErrorType.PASSWORD_CHANGED.getResponseMessage());
			//SessionCleaner.cleanSession(sessionMap);
			return SUCCESS;

		} catch (Exception exception) {
			logger.error("Exception", exception);
			setResponse("error");
			return ERROR;
		}
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		// Validate blank fields and invalid fields
		if ((validator.validateBlankField(getOldPassword()))) {
			setResponse(ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		} else if (!(validator.isValidPasword(getOldPassword()))) {
			setResponse(ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}

		// check fields have valid and match confirm password with new password
		if (validator.validateBlankField(getNewPassword())) {
			setResponse(ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
			if (validator.validateBlankField(getConfirmNewPassword())) {
				setResponse(ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
			}
		} else if (validator.validateBlankField(getConfirmNewPassword())) {
			setResponse(ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		} else if (!(getNewPassword().equals(getConfirmNewPassword()))) {
			setResponse(ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		} else if (!(validator.isValidPasword(getNewPassword()))) {
			setResponse(ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmNewPassword() {
		return confirmNewPassword;
	}

	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}
	
	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	

}
