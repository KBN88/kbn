package com.kbn.crm.action;

import java.util.List;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.LoginHistory;
import com.kbn.commons.user.LoginHistoryDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class LoginHistorySubUserAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(LoginHistoryAction.class.getName());
	private static final long serialVersionUID = -6336397235790682930L;
	
	private List<LoginHistory> aaData;
	private String emailId;

	public String execute() {
		LoginHistoryDao loginHistoryDao = new LoginHistoryDao();
		try {
			DataEncoder encoder = new DataEncoder();
			User user = (User) sessionMap.get(Constants.USER);
			if (user.getUserType() == UserType.MERCHANT) {
				if( (getEmailId().equals(CrmFieldConstants.ALL_USERS.getValue()))) {
					aaData = encoder.encodeLoginHistoryObj(loginHistoryDao.findAllUsers(user.getPayId()));
				}
				else {
					aaData =encoder.encodeLoginHistoryObj((List<LoginHistory>) loginHistoryDao
							.findLoginHisAllSubUser(getEmailId(), user.getPayId()));
				}
			} 
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public void validate(){
		CrmValidator validator = new CrmValidator();

		if(validator.validateBlankField(getEmailId()) || getEmailId().equals(CrmFieldConstants.ALL_MERCHANTS.getValue()) || getEmailId().equals(CrmFieldConstants.ALL_USERS.getValue())){
		}
        else if(!validator.validateField(CrmFieldType.EMAILID, getEmailId())){
        	addFieldError(CrmFieldType. EMAILID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public List<LoginHistory> getAaData() {
		return aaData;
	}

	public void setAaData(List<LoginHistory> aaData) {
		this.aaData = aaData;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}
