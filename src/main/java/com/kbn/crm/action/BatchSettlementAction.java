package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.interceptor.TokenValidationInterceptor;
import com.kbn.commons.user.Settlement;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.actionBeans.SettlementProcessor;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 *
 */
public class BatchSettlementAction extends AbstractSecureAction{

	private static final long serialVersionUID = -8238190040140690439L;
	private static Logger logger = Logger.getLogger(BatchSettlementAction.class
			.getName());

	private List<Settlement> settlementList = new ArrayList<Settlement>();
	private String token;
	private String response;
	
	public String execute(){
		try {
			SettlementProcessor settlementProcessor = new SettlementProcessor();
			settlementProcessor.processAll(settlementList);
			setResponse(CrmFieldConstants.PROCESS_INITIATED_SUCCESSFULLY.getValue());
		} catch (Exception exception) {
			logger.error("Error processing batch settlement:" + exception);
			setResponse(ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
		}
		settlementList.clear();
		return SUCCESS;
	}

	public void validate(){
		if(validateToken()){
			addFieldError(CrmFieldType.PAY_ID.getName(), ErrorType.VALIDATION_FAILED.getResponseMessage());
			return;
		}
		CrmValidator validator = new CrmValidator();
		for(Settlement settlement:settlementList){
			if ((validator.validateBlankField(settlement.getPayId()))) {
			} else if (!(validator.validateField(CrmFieldType.PAY_ID, settlement.getPayId()))) {
				addFieldError(CrmFieldType.PAY_ID.getName(), validator.getResonseObject().getResponseMessage());
			}

			if ((validator.validateBlankField(settlement.getTxnDate()))) {
				addFieldError(CrmFieldType.DATE_FROM.getName(), validator.getResonseObject().getResponseMessage());
			} else if (!(validator.validateField(CrmFieldType.DATE_FROM,settlement.getTxnDate()))) {
				addFieldError(CrmFieldType.DATE_FROM.getName(), validator.getResonseObject().getResponseMessage());
			}
			
		    if(validator.validateBlankField(settlement.getCurrencyCode())){
		    }else if(!validator.validateField(CrmFieldType.CURRENCY, settlement.getCurrencyCode())){
		      	addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		   
		   if(validator.validateBlankField(settlement.getPaymentMethod())){
			}else if(!validator.validateField(CrmFieldType.PAYMENT_TYPE, settlement.getPaymentMethod())){
				addFieldError(CrmFieldType.PAYMENT_TYPE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		   
		   if(validator.validateBlankField(settlement.getCustomerEmail()) || settlement.getCustomerEmail().equals(CrmFieldConstants.ALL.getValue())){
			}else if(!validator.validateField(CrmFieldType.MERCHANT_EMAIL_ID, settlement.getCustomerEmail())){
	       		addFieldError(CrmFieldType. MERCHANT_EMAIL_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		   
		   if(validator.validateBlankField(settlement.getOrderId())){
			}else if(!validator.validateField(CrmFieldType.ORDER_ID, settlement.getOrderId())){
				addFieldError(CrmFieldType.ORDER_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		   
		   if(validator.validateBlankField(settlement.getTxnAmount())){
			}else if(!validator.validateField(CrmFieldType.AMOUNT, settlement.getTxnAmount())){
				addFieldError(CrmFieldType.AMOUNT.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		   
		   if(validator.validateBlankField(settlement.getNetAmount())){
			}else if(!validator.validateNegativeAmount(settlement.getNetAmount())){
				addFieldError(CrmFieldType.NET_AMOUNT.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		   		   
		   if(validator.validateBlankField(settlement.getTxnId())){
			}else if(!validator.validateField(CrmFieldType.TRANSACTION_ID, settlement.getTxnId())){
				addFieldError(CrmFieldType.TRANSACTION_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		}
	}
	
	public boolean validateToken(){
		if(null==token || !(token.equals((String) sessionMap.get(TokenValidationInterceptor.SERVER_END_NAME)))){
			return true;
		}
		return false;
	}

	public List<Settlement> getSettlementList() {
		return settlementList;
	}

	public void setSettlementList(List<Settlement> settlementList) {
		this.settlementList = settlementList;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}