package com.kbn.crm.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.ServiceTaxDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.ServiceTax;
import com.kbn.commons.util.BusinessType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class ServiceTaxPlatformAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(ServiceTaxPlatformAction.class.getName());
	private static final long serialVersionUID = -6879974923614009981L;

	private Map<String, List<ServiceTax>> serviceTaxData = new HashMap<String, List<ServiceTax>>();
	private Map<String, String> industryCategory = new LinkedHashMap<String, String>();
	private String businessType;

	public String execute() {
		setIndustryCategory(BusinessType.getIndustryCategoryList());
		ServiceTaxDao serviceTaxDao = new ServiceTaxDao();
		List<ServiceTax> serviceTaxList = new ArrayList<ServiceTax>();
		Map<String, String> businessTypeList = BusinessType.getIndustryCategoryList();
		ServiceTax serviceTax = new ServiceTax();
		if (businessType != "All") {
			serviceTax = serviceTaxDao.findServiceTax(businessType);
			if (serviceTax != null) {
				serviceTaxList.add(serviceTax);
			} else {
				ServiceTax serviceTaxUndefined = new ServiceTax();
				serviceTaxUndefined.setServiceTax(BigDecimal.ZERO);
				serviceTaxUndefined.setBusinessType(businessType);
				serviceTaxUndefined.setStatus(TDRStatus.INACTIVE);
				serviceTaxList.add(serviceTaxUndefined);
			}
		}

		else {
			for (Entry<String, String> businessType : businessTypeList.entrySet()) {

				serviceTax = serviceTaxDao.findServiceTax(businessType.getValue());
				if (serviceTax != null) {
					serviceTaxList.add(serviceTax);
				} else {
					ServiceTax serviceTaxUndefined = new ServiceTax();
					serviceTaxUndefined.setServiceTax(BigDecimal.ZERO);
					serviceTaxUndefined.setBusinessType(businessType.getValue());
					serviceTaxUndefined.setStatus(TDRStatus.INACTIVE);
					serviceTaxList.add(serviceTaxUndefined);
				}

			}
		}

		serviceTaxData.put("Service Tax", serviceTaxList);
		try {
			if (serviceTaxData.size() == 0) {
				addActionMessage(ErrorType.SERVICE_TAX_DEATILS_NOT_AVAILABLE.getResponseMessage());
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			addActionMessage(ErrorType.UNKNOWN.getResponseMessage());
		}
		return INPUT;
	}

	public String display() {
		return INPUT;
	}

	public Map<String, List<ServiceTax>> getServiceTaxData() {
		return serviceTaxData;
	}

	public void setServiceTaxData(Map<String, List<ServiceTax>> serviceTaxData) {
		this.serviceTaxData = serviceTaxData;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public Map<String, String> getIndustryCategory() {
		return industryCategory;
	}

	public void setIndustryCategory(Map<String, String> industryCategory) {
		this.industryCategory = industryCategory;
	}
}
