package com.kbn.crm.action;

import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.util.TokenHelper;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.LoginHistory;
import com.kbn.commons.user.LoginHistoryDao;
import com.kbn.commons.user.PermissionType;
import com.kbn.commons.user.Permissions;
import com.kbn.commons.user.Roles;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmActions;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.actionBeans.LoginAuthenticator;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.crm.actionBeans.SessionCleaner;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author Puneet
 * 
 */

public class LoginAction extends AbstractSecureAction implements
		ServletRequestAware {

	private static Logger logger = Logger.getLogger(LoginAction.class.getName());
	private static final long serialVersionUID = -5127683348802926510L;

	private String redirectUrl;
	private String emailId;
	private String password;
	private String captcha;
	private String captchaCode;
	private HttpServletRequest request;	
	
	private String permissionString = "";
	
	@Override
	public String execute() {

		LoginAuthenticator loginAuthenticator = new LoginAuthenticator();
		ResponseObject responseObject = new ResponseObject();
		LoginHistoryDao loginHistoryDao = new LoginHistoryDao();
		LoginHistory loginHistory = new LoginHistory();
		User user;
		try {
			responseObject = loginAuthenticator.authenticate(getEmailId(),
					getPassword(), request.getHeader(CrmFieldConstants.USER_AGENT.getValue()),
					request.getRemoteAddr());
			if (!ErrorType.SUCCESS.getResponseCode().equals(
					responseObject.getResponseCode())) {
				addFieldError(CrmFieldType.EMAILID.getName(), responseObject.getResponseMessage());
				return INPUT;
			}
			
			//To add custom field to each log for all activities of this user
			MDC.put(Constants.CRM_LOG_USER_PREFIX.getValue(), getEmailId());
			logger.info("logged in");

			loginHistory = loginHistoryDao.findLastLoginByUser(getEmailId());

			SessionCleaner.cleanSession(sessionMap);
 			sessionMap = (SessionMap<String, Object>) ActionContext.getContext().getSession();
			
			user = loginAuthenticator.getUser();
			sessionMap.put(Constants.USER.getValue(),user);
			sessionMap.put(Constants.LAST_LOGIN.getValue(),	loginHistory);
			sessionMap.put(Constants.CUSTOM_TOKEN.getValue(), TokenHelper.generateGUID());
			if (user.getUserType().equals(UserType.SUBUSER) || user.getUserType().equals(UserType.SUBACQUIRER) || user.getUserType().equals(UserType.SUBADMIN)) {
				Set<Roles> roles = user.getRoles();
				Set<Permissions> permissions = roles.iterator().next().getPermissions();
				if (!permissions.isEmpty()) {
					StringBuilder perms = new StringBuilder();
					Iterator<Permissions> itr = permissions.iterator();
					while (itr.hasNext()) {
						PermissionType permissionType = itr.next().getPermissionType();
						perms.append(permissionType.getPermission());
						perms.append("-");
					}
					perms.deleteCharAt(perms.length() - 1);
					setPermissionString(perms.toString());
					sessionMap.put(Constants.USER_PERMISSION.getValue(),
							perms.toString());	
				}
			}
			else if(user.getUserType().equals(UserType.SUBADMIN)){
				redirectUrl = user.getLastActionName();
			}
			
			// redirecting to lastActionName
			redirectUrl = user.getLastActionName();
			if(redirectUrl!=null){
				
				//Quick fix for "resellerLists" action (New name is "adminResellers"
				// that was refactored   --Harpreet
				if(redirectUrl.equalsIgnoreCase("resellerLists")){
					redirectUrl = CrmActions.ADMIN_RESELLER_LISTS.getValue();
					return "redirect";
				}
				return "redirect";
			}
			return SUCCESS;
		}catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}	

	public void validate() {

		CrmValidator validator = new CrmValidator();

		if ((validator.validateBlankFields(getEmailId()))) {
			addFieldError(CrmFieldType.EMAILID.getName(), validator.getResonseObject()
					.getResponseMessage());
			return;
		} else if (validator.validateBlankFields(getPassword())) {
			addFieldError(CrmFieldType.EMAILID.getName(), validator.getResonseObject()
					.getResponseMessage());
			return;
		}

		if (!(validator.isValidEmailId(getEmailId()))) {
			addFieldError(CrmFieldType.EMAILID.getName(),
					ErrorType.INVALID_INPUT.getResponseMessage());
			return;
		}

		if (!(validator.isValidPasword(getPassword()))) {
			addFieldError(CrmFieldType.EMAILID.getName(),
					ErrorType.INVALID_INPUT.getResponseMessage());
		}
		if (validator.validateBlankField(getCaptcha())) {
			addFieldError(CrmFieldType.CAPTCHA.getName(), validator.getResonseObject().getResponseMessage());
		} 
		else if (!getCaptcha().equals(getCaptchaCode())){
			addFieldError(CrmFieldType.CAPTCHA.getName(), CrmFieldConstants.INVALID_CAPTCHA.getValue());
		}
	}

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public String getCaptchaCode() {
		return captchaCode;
	}

	public void setCaptchaCode(String captchaCode) {
		this.captchaCode = captchaCode;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getPermissionString() {
		return permissionString;
	}

	public void setPermissionString(String permissionString) {
		this.permissionString = permissionString;
	}	
	
	
}
