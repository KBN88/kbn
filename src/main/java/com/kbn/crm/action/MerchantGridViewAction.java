package com.kbn.crm.action;

import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.MerchantGridViewService;
import com.kbn.commons.user.MerchantDetails;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.commons.action.AbstractSecureAction;
public class MerchantGridViewAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(MerchantGridViewAction.class.getName());
	private static final long serialVersionUID = 3293888841176590776L;
	private List<MerchantDetails> aaData;
	private User sessionUser = new User();
	private String businessType;
	@Override
	public String execute() {
		DataEncoder encoder = new DataEncoder();
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		try {	
			if(sessionUser.getUserType().equals(UserType.ADMIN) || sessionUser.getUserType().equals(UserType.SUBADMIN)){
				if(businessType.equals("ALL")){
					aaData = encoder.encodeMerchantDetailsObj((new MerchantGridViewService()).getAllMerchants());
				}else{
					aaData = encoder.encodeMerchantDetailsObj((new MerchantGridViewService()).getAllMerchants(getBusinessType()));
				}
			}
			
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}
//Show all resellerList in Dashbord
	public String listOfReseller(){
		DataEncoder encoder = new DataEncoder();
		try {			
			aaData = encoder.encodeMerchantDetailsObj((new MerchantGridViewService()).getAllReseller());
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}
	
	 public String resellerList(){
		 DataEncoder encoder = new DataEncoder();
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		 try {			
				aaData = encoder.encodeMerchantDetailsObj((new MerchantGridViewService()).getAllReselerMerchants(sessionUser.getResellerId()));
				return SUCCESS;
			} catch (Exception exception) {
				logger.error("Exception", exception);
				return ERROR;
			}
	 }
	public List<MerchantDetails> getaaData() {
		return aaData;
	}

	public void setaaData(List<MerchantDetails> setaaData) {
		this.aaData = setaaData;
	}
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	
	
}