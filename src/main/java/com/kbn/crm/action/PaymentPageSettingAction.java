package com.kbn.crm.action;

import java.io.File;

import org.apache.log4j.Logger;

import com.kbn.commons.user.DynamicPaymentPage;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.action.FetchDynamicPaymentPage;
import com.opensymphony.xwork2.ModelDriven;
/**
 * @Isha
 */
public class PaymentPageSettingAction extends AbstractSecureAction implements
ModelDriven<DynamicPaymentPage>  {
	private String payId;
	
	private static final long serialVersionUID = -2068525281163587799L;
		private static Logger logger = Logger.getLogger(PaymentPageSettingAction.class.getName());
		DynamicPaymentPage dynamicPaymentPage = new  DynamicPaymentPage();
		FetchDynamicPaymentPage fetchDynamicPage= new FetchDynamicPaymentPage();
	public String execute(){
		try {
		
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		//setDynamicPaymentPage(dynamicPaymentPageDao.findPayId(sessionUser.getPayId()));
		setDynamicPaymentPage(fetchDynamicPage.FetchDynamicDesignPage(sessionUser.getPayId()));
		File file = new File(makeFileName());
		if(file.exists()){
			dynamicPaymentPage.setUserLogo("TRUE");
				}
		return SUCCESS;
		}catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}
	
	private  String makeFileName(){
		String name,destPath,logoFormat;
		
		PropertiesManager propertiesManager = new PropertiesManager();
		destPath = propertiesManager.getSystemProperty("PaymentPagePath");
		logoFormat = propertiesManager.getSystemProperty("PaymentPageLogoFormat");
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		if (sessionUser.getUserType().equals(UserType.ADMIN)
				|| sessionUser.getUserType().equals(UserType.SUPERADMIN)) {
			// payId=user.getPayId();
		} else {
			setPayId(sessionUser.getPayId());
		}
		name=destPath+"/"+getPayId()+logoFormat;
		
		return name;
		
	}
	@Override
	public DynamicPaymentPage getModel() {
		
		return dynamicPaymentPage;
	}

	public DynamicPaymentPage getDynamicPaymentPage() {
		return dynamicPaymentPage;
	}

	public void setDynamicPaymentPage(DynamicPaymentPage dynamicPaymentPage) {
		this.dynamicPaymentPage = dynamicPaymentPage;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}

}
