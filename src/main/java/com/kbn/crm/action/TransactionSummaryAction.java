package com.kbn.crm.action;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionSummaryService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.commons.util.DateCreater;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class TransactionSummaryAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(TransactionSummaryAction.class.getName());
	private static final long serialVersionUID = -3617221129064403081L;

	private User sessionUser = new User();
	private TransactionSummaryService getSummary = new TransactionSummaryService();
	private List<TransactionSummaryReport> aaData = new ArrayList<TransactionSummaryReport>();
	private String dateFrom;
	private String dateTo;
	private String acquirer;
	private String paymentType;
	private String status;
	private String currency;
	private String successMessage;
	private String responseMessage;
	private int draw;
	private int length;
	private int start;
	private BigInteger recordsTotal;
	private BigInteger recordsFiltered;
	private String payId;
	
	// get failed txns
	public String execute() {
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier sessionUserIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = sessionUserIdentifier.getMerchantPayId(sessionUser, getPayId());
			if(sessionUser.getUserType().equals(UserType.SUBACQUIRER)){
			    payId  =  sessionUser.getParentPayId();
			   UserDao userDao = new UserDao();
			   User  sessionSubAcquirerUser= userDao.getUserClass(payId);
			   setRecordsTotal((getSummary.getAcquirerFailedTransactionListCountList(getDateFrom(),getDateTo(), merchantPayId, getPaymentType(), sessionSubAcquirerUser.getFirstName(), getCurrency())));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
		setAaData(encoder.encodeTransactionSummary(getSummary.getAcquirerFailedTransactionListCountList(getDateFrom(),
				getDateTo(), merchantPayId,getPaymentType(), sessionSubAcquirerUser.getFirstName(), getCurrency(),getStart(),getLength())));
		
			}else if (sessionUser.getUserType().equals(UserType.RESELLER)){
				setRecordsTotal((getSummary.getResellerFailedResellerTransactionListCountList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
						.toString(), getPaymentType(), getAcquirer(), getCurrency())));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
				setAaData(encoder.encodeTransactionSummary(getSummary.getFailedResellerTransactionList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
								.toString(), getPaymentType(), getAcquirer(), getCurrency(),getStart(),getLength())));
				}else if(sessionUser.getUserType().equals(UserType.ACQUIRER)){
					
				setRecordsTotal((getSummary.getAcquirerFailedTransactionListCountList(getDateFrom(),getDateTo(), merchantPayId,getPaymentType(), sessionUser.getFirstName(), getCurrency())));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
		setAaData(encoder.encodeTransactionSummary(getSummary.getAcquirerFailedTransactionListCountList(getDateFrom(),
				getDateTo(), merchantPayId,getPaymentType(), sessionUser.getFirstName(), getCurrency(),getStart(),getLength())));
			}else{
					setRecordsTotal((getSummary.getFailedTransactionListCountList(getDateFrom(),getDateTo(), merchantPayId, sessionUser.getUserType().toString(), getPaymentType(), getAcquirer(), getCurrency())));
					if(getLength()==-1){
						setLength(getRecordsTotal().intValue());
					}
			setAaData(encoder.encodeTransactionSummary(getSummary.getFailedTransactionList(getDateFrom(),
					getDateTo(), merchantPayId, sessionUser.getUserType()
							.toString(), getPaymentType(), getAcquirer(), getCurrency(),getStart(),getLength())));
				}
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		recordsFiltered = recordsTotal;
		return SUCCESS;
	}
	
	// get incompleted transactions
	public String incomplete() {
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier sessionUserIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = sessionUserIdentifier.getMerchantPayId(sessionUser, getPayId());
			if(sessionUser.getUserType().equals(UserType.SUBACQUIRER)){
			    payId  =  sessionUser.getParentPayId();
			   UserDao userDao = new UserDao();
			   User  sessionSubAcquirerUser= userDao.getUserClass(payId);
			   setRecordsTotal((getSummary.getAcquirerIncompleteTransactionList(getDateFrom(),getDateTo(), merchantPayId, getStatus(),getPaymentType(), sessionSubAcquirerUser.getFirstName(), getCurrency())));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
		setAaData(encoder.encodeTransactionSummary(getSummary.getAcquirerIncompleteTransactionList(getDateFrom(),
				getDateTo(), merchantPayId,getStatus(),getPaymentType(), sessionSubAcquirerUser.getFirstName(), getCurrency(),getStart(),getLength())));
					
			}else if (sessionUser.getUserType().equals(UserType.RESELLER)){
				setRecordsTotal((getSummary.getResellerIncompleteTransactionListCountList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
						.toString(), getPaymentType(), getStatus(), getAcquirer(), getCurrency())));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
				setAaData(encoder.encodeTransactionSummary(getSummary.getResellerIncompleteTransactionList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
								.toString(), getPaymentType(), getStatus(), getAcquirer(), getCurrency(),getStart(),getLength())));
				}else if(sessionUser.getUserType().equals(UserType.ACQUIRER)){
					
				setRecordsTotal((getSummary.getAcquirerIncompleteTransactionList(getDateFrom(),getDateTo(), merchantPayId, getStatus(),getPaymentType(), sessionUser.getFirstName(), getCurrency())));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
		setAaData(encoder.encodeTransactionSummary(getSummary.getAcquirerIncompleteTransactionList(getDateFrom(),
				getDateTo(), merchantPayId,getStatus(),getPaymentType(), sessionUser.getFirstName(), getCurrency(),getStart(),getLength())));
			}else{
					setRecordsTotal((getSummary.getIncompleteTransactionListCountList(getDateFrom(),getDateTo(), merchantPayId, sessionUser.getUserType().toString(),getStatus(), getPaymentType(), getAcquirer(), getCurrency())));
					if(getLength()==-1){
						setLength(getRecordsTotal().intValue());
					}
			setAaData(encoder.encodeTransactionSummary(getSummary.getIncompleteTransactionList(getDateFrom(),
					getDateTo(), merchantPayId, sessionUser.getUserType()
							.toString(), getPaymentType(), getStatus(), getAcquirer(), getCurrency(),getStart(),getLength())));
				}

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		recordsFiltered = recordsTotal;
		return SUCCESS;
	}
	
	// Get only authorised transactions that need to be captured
	public String authorize() {
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();	
		SessionUserIdentifier sessionUserIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = sessionUserIdentifier.getMerchantPayId(sessionUser, getPayId());
			if(sessionUser.getUserType().equals(UserType.SUBACQUIRER)){
		    payId  =  sessionUser.getParentPayId();
		   UserDao userDao = new UserDao();
		   User  sessionSubAcquirerUser= userDao.getUserClass(payId);
		   setAaData(encoder.encodeTransactionSummary(getSummary.getAcquirerAuthorisedTransactionList(getDateFrom(),
					getDateTo(), merchantPayId, 
							 getPaymentType(), sessionSubAcquirerUser.getFirstName(), getCurrency())));
				
			}else if (sessionUser.getUserType().equals(UserType.RESELLER)){
				setAaData(encoder.encodeTransactionSummary(getSummary.getAuthorisedResellerTransactionList(getDateFrom(),
					getDateTo(), merchantPayId, sessionUser.getResellerId()
							.toString(), getPaymentType(), getAcquirer(), getCurrency())));
			}else if(sessionUser.getUserType().equals(UserType.ACQUIRER)){
				setAaData(encoder.encodeTransactionSummary(getSummary.getAcquirerAuthorisedTransactionList(getDateFrom(),
						getDateTo(), merchantPayId, 
								 getPaymentType(), sessionUser.getFirstName(), getCurrency())));
				}else{

			setAaData(encoder.encodeTransactionSummary(getSummary.getAuthorisedTransactionList(getDateFrom(),
					getDateTo(), merchantPayId, sessionUser.getUserType()
							.toString(), getPaymentType(), getAcquirer(), getCurrency())));
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	// get cancelled transactions
	public String cancelTransaction() {

		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier sessionUserIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = sessionUserIdentifier.getMerchantPayId(sessionUser, getPayId());
			if (sessionUser.getUserType().equals(UserType.RESELLER)){
				setRecordsTotal((getSummary.getResellerCancelledTransactionListCountList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
						.toString(), getCurrency())));
						if(getLength()==-1){
							setLength(getRecordsTotal().intValue());
						}
				setAaData(encoder.encodeTransactionSummary(getSummary.getResellerCancelledTransactionList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
								.toString(), getCurrency(),getStart(),getLength())));
				}else{
					setRecordsTotal((getSummary.getCancelledTransactionListCountList(getDateFrom(),
					getDateTo(), merchantPayId, sessionUser.getUserType()
							.toString(), getCurrency())));
					if(getLength()==-1){
						setLength(getRecordsTotal().intValue());
					}
			setAaData(encoder.encodeTransactionSummary(getSummary.getCancelledTransactionList(getDateFrom(),
					getDateTo(), merchantPayId, sessionUser.getUserType()
							.toString(), getCurrency(),getStart(),getLength())));
				}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return SUCCESS;
		}
		recordsFiltered = recordsTotal;
		return SUCCESS;
	}

	// get captured transactions that can be refunded
	public String capture() {
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier sessionUserIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = sessionUserIdentifier.getMerchantPayId(sessionUser, getPayId());
			if(sessionUser.getUserType().equals(UserType.SUBACQUIRER)){
			    payId  =  sessionUser.getParentPayId();
			   UserDao userDao = new UserDao();
			   User  sessionSubAcquirerUser= userDao.getUserClass(payId);
			   
			   setRecordsTotal((getSummary.getAcquirerCapturedTransactionCountList(getDateFrom(),getDateTo(), merchantPayId, getPaymentType(), sessionSubAcquirerUser.getFirstName(), getCurrency())));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
		setAaData(encoder.encodeTransactionSummary(getSummary.getAcquirerCapturedTransactionCountList(getDateFrom(),
				getDateTo(), merchantPayId, getPaymentType(), sessionSubAcquirerUser.getFirstName(), getCurrency(),getStart(),getLength())));
					
				}else if (sessionUser.getUserType().equals(UserType.RESELLER)){
				
				setRecordsTotal((getSummary.getResellerCapturedTransactionCountList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
						.toString(), getPaymentType(), getAcquirer(), getCurrency())));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
				
				setAaData(encoder.encodeTransactionSummary(getSummary.getResellerCapturedTransactionList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
								.toString(), getPaymentType(), getAcquirer(), getCurrency(),getStart(),getLength())));
				
				}else if(sessionUser.getUserType().equals(UserType.ACQUIRER)){
					
				setRecordsTotal((getSummary.getAcquirerCapturedTransactionCountList(getDateFrom(),getDateTo(), merchantPayId, getPaymentType(), sessionUser.getFirstName(), getCurrency())));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
		setAaData(encoder.encodeTransactionSummary(getSummary.getAcquirerCapturedTransactionCountList(getDateFrom(),
				getDateTo(), merchantPayId, getPaymentType(), sessionUser.getFirstName(), getCurrency(),getStart(),getLength())));
			}else{
					setRecordsTotal((getSummary.getCapturedTransactionCountList(getDateFrom(),getDateTo(), merchantPayId, sessionUser.getUserType().toString(), getPaymentType(), getAcquirer(), getCurrency())));
					if(getLength()==-1){
						setLength(getRecordsTotal().intValue());
					}
					setAaData(encoder.encodeTransactionSummary(getSummary.getCapturedTransactionList(getDateFrom(),
					getDateTo(), merchantPayId, sessionUser.getUserType()
							.toString(), getPaymentType(), getAcquirer(), getCurrency(),getStart(),getLength())));
				}
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		recordsFiltered = recordsTotal;
		return SUCCESS;
	}

	public String invalid()
	{
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier sessionUserIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = sessionUserIdentifier.getMerchantPayId(sessionUser, getPayId());
			if (sessionUser.getUserType().equals(UserType.RESELLER)){
				setAaData(encoder.encodeTransactionSummary(getSummary.getResellerInvalidTransactionList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
								.toString())));
			}else{
			setAaData(encoder.encodeTransactionSummary(getSummary.getInvalidTransactionList(getDateFrom(),
					getDateTo(), merchantPayId, sessionUser.getUserType()
							.toString())));
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}
	
	
	public String fraudTransaction()
	{
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier sessionUserIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = sessionUserIdentifier.getMerchantPayId(sessionUser, getPayId());
			if (sessionUser.getUserType().equals(UserType.RESELLER)){
				//TODO
			/*	setAaData(encoder.encodeTransactionSummary(getSummary.getResellerInvalidTransactionList(getDateFrom(),
						getDateTo(), merchantPayId, sessionUser.getResellerId()
								.toString())));
*/			}else{
			//some params are hard coded and left to configurable for future use
			setAaData(encoder.encodeTransactionSummary(getSummary.getFraudTransactionList(getDateFrom(),
					getDateTo(), merchantPayId,"ADMIN", "ALL", "ALL", "ALL")));
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}
	
	public void validate(){
		CrmValidator validator = new CrmValidator();

		if(validator.validateBlankField(getAcquirer())){
		}
		else if(!validator.validateField(CrmFieldType.ACQUIRER, getAcquirer())){
			addFieldError(CrmFieldType.ACQUIRER.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

        if(validator.validateBlankField(getCurrency())){
		}
        else if(!validator.validateField(CrmFieldType.CURRENCY, getCurrency())){
        	addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

        if(validator.validateBlankField(getDateFrom())){
        }
        else if(!validator.validateField(CrmFieldType.DATE_FROM, getDateFrom())){
        	addFieldError(CrmFieldType.DATE_FROM.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

        if(validator.validateBlankField(getDateTo())){
        }
        else if(!validator.validateField(CrmFieldType.DATE_TO, getDateTo())){
        	addFieldError(CrmFieldType.DATE_TO.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
        if(!validator.validateBlankField(getDateTo())){
	       if(DateCreater.formatStringToDate(DateCreater.formatFromDate(getDateFrom())).compareTo(DateCreater.formatStringToDate(DateCreater.formatFromDate(getDateTo()))) > 0) {
	        	addFieldError(CrmFieldType.DATE_FROM.getName(), CrmFieldConstants.FROMTO_DATE_VALIDATION.getValue());
	        }
	        else if(DateCreater.diffDate(getDateFrom(), getDateTo()) > 31) {
	        	addFieldError(CrmFieldType.DATE_FROM.getName(), CrmFieldConstants.DATE_RANGE.getValue());
	        }
        }         
        
        if(validator.validateBlankField(getPayId()) || getPayId().equals(CrmFieldConstants.ALL.getValue())){
		}
        else if(!validator.validateField(CrmFieldType.PAY_ID, getPayId())){
        	addFieldError(CrmFieldType.PAY_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}           
        
		if(validator.validateBlankField(getPaymentType())){
		}
		else if(!validator.validateField(CrmFieldType.PAYMENT_TYPE, getPaymentType())){
        	addFieldError(CrmFieldType.PAYMENT_TYPE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if(validator.validateBlankField(getSuccessMessage())){
		}
		else if(!validator.validateField(CrmFieldType.SUCCESS_MESSAGE, getSuccessMessage())){
			addFieldError(CrmFieldType.SUCCESS_MESSAGE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
    }

	public List<TransactionSummaryReport> getAaData() {
		return aaData;
	}

	public void setAaData(List<TransactionSummaryReport> aaData) {
		this.aaData = aaData;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public BigInteger getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(BigInteger recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public BigInteger getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(BigInteger recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public String getPayId() {
		return payId;
	}
 
	public void setPayId(String payId) {
		this.payId = payId;
	}
}