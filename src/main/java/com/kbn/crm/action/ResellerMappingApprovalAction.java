package com.kbn.crm.action;

import java.util.Date;

import com.kbn.commons.user.PendingResellerMappingApproval;
import com.kbn.commons.user.PendingResellerMappingDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Rahul
 *
 */
public class ResellerMappingApprovalAction extends AbstractSecureAction {

	private static final long serialVersionUID = 6467520675757548735L;

	private String merchantEmailId;
	private String resellerId;
	private String loginemailId;
	private String operation;
	private UserType userType;
	private String requestedBy;

	public String execute() {

		Date date = new Date();
		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

		if (permissions.toString().contains("Edit Merchant Details") || userType.equals(UserType.ADMIN)) {
			PendingResellerMappingDao pendingResellerMappingDao = new PendingResellerMappingDao();
			PendingResellerMappingApproval pendingMapping = pendingResellerMappingDao.find(merchantEmailId);
			if (operation.equals("accept")) {
				pendingMapping.setRequestStatus(TDRStatus.ACCEPTED.toString());
				pendingMapping.setUpdateDate(date);
				pendingMapping.setProcessedBy(loginemailId);
				pendingResellerMappingDao.update(pendingMapping);
				
				UserDao userDao = new UserDao();
				User dbUser = userDao.findPayIdByEmail(merchantEmailId);
				User userReseller = userDao.find(resellerId); 
				String resellerPayId = userReseller.getPayId(); 
				dbUser.setResellerId(resellerPayId);
				dbUser.setUpdateDate(date);
				dbUser.setUpdatedBy(loginemailId);
				userDao.update(dbUser);

			} else if (operation.equals("reject")) {
				pendingMapping.setRequestStatus(TDRStatus.REJECTED.toString());
				pendingMapping.setUpdateDate(date);
				pendingMapping.setProcessedBy(loginemailId);
				pendingResellerMappingDao.update(pendingMapping);

			}

		}
		return SUCCESS;

	}

	public String getMerchantEmailId() {
		return merchantEmailId;
	}

	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}

	public String getResellerId() {
		return resellerId;
	}

	public void setResellerId(String resellerId) {
		this.resellerId = resellerId;
	}

	public String getLoginemailId() {
		return loginemailId;
	}

	public void setLoginemailId(String loginemailId) {
		this.loginemailId = loginemailId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

}
