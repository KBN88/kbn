package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.actionBeans.ChargingDetailsFactory;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class ChargingPlatformAction extends AbstractSecureAction {

	private static Logger logger = Logger
			.getLogger(ChargingPlatformAction.class.getName());
	private static final long serialVersionUID = -6879974923614009981L;

	public List<Merchants> listMerchant = new ArrayList<Merchants>();
	private Map<String, List<ChargingDetails>> aaData = new HashMap<String, List<ChargingDetails>>();
	private String emailId;
	private String acquirer;

	@SuppressWarnings("unchecked")
	public String execute() {
		UserDao userDao = new UserDao();
		setListMerchant(userDao.getMerchantList());

		try {
			if (emailId != null && acquirer != null) {
				ChargingDetailsFactory chargingDetailProvider = new ChargingDetailsFactory();
				setAaData(chargingDetailProvider.getChargingDetailsMap(emailId,
						acquirer));
				if(aaData.size()==0){
					addActionMessage(ErrorType.CHARGINGDETAIL_NOT_FETCHED.getResponseMessage());
				}
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			addActionMessage(ErrorType.UNKNOWN.getResponseMessage());
		}
		return INPUT;
	}

	// To display page without using token
	@SuppressWarnings("unchecked")
	public String displayList() {
		UserDao userDao = new UserDao();
		setListMerchant(userDao.getMerchantList());
		return INPUT;
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();
		if ((validator.validateBlankField(getAcquirer()))) {
		} else if (!validator.validateField(CrmFieldType.ACQUIRER,
				getAcquirer())) {
			addFieldError(CrmFieldType.ACQUIRER.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if (validator.validateBlankField(getEmailId())) {
		} else if (!validator.validateField(CrmFieldType.EMAILID, getEmailId())) {
			addFieldError("emailId",
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public List<Merchants> getListMerchant() {
		return listMerchant;
	}

	public void setListMerchant(List<Merchants> listMerchant) {
		this.listMerchant = listMerchant;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	public Map<String, List<ChargingDetails>> getAaData() {
		return aaData;
	}

	public void setAaData(Map<String, List<ChargingDetails>> aaData) {
		this.aaData = aaData;
	}
}
