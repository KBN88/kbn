package com.kbn.crm.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.DocumentNameConstants;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class DownloadDocumentAction  extends AbstractSecureAction {

	/**
	 * @Isha
	 */
	private static final long serialVersionUID = -5897052159318194562L;
	private InputStream fileInputStream;
	private static Logger logger = Logger
			.getLogger(DownloadDocumentAction.class.getName());
	private String fileName;
	private String destfileName;
	private String destPath;
	private String payId;
	private String fileNameStruts;
	private String[] fileFormatList;
	private String fileFormat;
	public String execute(){
		
		try {
			PropertiesManager propertiesManager = new PropertiesManager();
			setFileFormat(propertiesManager.getSystemProperty("FileFormat"));
			
			User user = (User) sessionMap.get(Constants.USER);
			if (user.getUserType().equals(UserType.ADMIN) || user.getUserType().equals(UserType.SUPERADMIN)) {
				setPayId(getPayId());
			}
			else
			{
				setPayId(user.getPayId());
			}
			setDestPath(propertiesManager.getSystemProperty("DocumentPath")+ getPayId()+"/");
			
			fileFormatList = getFileFormat().split(",");
			for (int j=0;j<fileFormatList.length;j++){
				File f = new File(makeFileName(getFileName(),fileFormatList[j]));
				if(f.exists()){
					setDestfileName(getDestPath()+getPayId()+"_"+ getFileName()+ fileFormatList[j]);
					setFileNameStruts(getPayId()+"_"+ getFileName()+ fileFormatList[j]);
					setFileInputStream(new FileInputStream(new File(getDestfileName())));
				break;
				}
			}
			
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return SUCCESS;
		
	}
	
	private  String makeFileName(String docName,String extension){
		String name;
		name=getDestPath()+getPayId()+"_"+ docName+extension;
		
		return name;
		
	}
	
	public static Logger getLogger() {
		return logger;
	}
	public static void setLogger(Logger logger) {
		DownloadDocumentAction.logger = logger;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDestPath() {
		return destPath;
	}

	public void setDestPath(String destPath) {
		this.destPath = destPath;
	}
	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}
	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getFileNameStruts() {
		return fileNameStruts;
	}

	public void setFileNameStruts(String fileNameStruts) {
		this.fileNameStruts = fileNameStruts;
	}

	public String getDestfileName() {
		return destfileName;
	}

	public void setDestfileName(String destfileName) {
		this.destfileName = destfileName;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}
}
