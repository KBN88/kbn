package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Invoice;
import com.kbn.commons.user.InvoiceTransactionDao;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.PromotionalPaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.TransactionManager;
import com.kbn.commons.util.UrlShortener;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailBuilder;
import com.kbn.pg.core.Currency;
import com.kbn.sms.SmsSender;
import com.opensymphony.xwork2.ModelDriven;

/**
 * @author ISHA,CHANDAN
 *
 */
public class InvoiceTransaction extends AbstractSecureAction implements ModelDriven<Invoice> {
	private static Logger logger = Logger.getLogger(LoginAction.class.getName());
	private static final long serialVersionUID = 3857047834665047987L;

	private Invoice invoice = new Invoice();
	private String url;
	private String merchant;
	private boolean emailCheck;
	private String merchantPayId;
	private Map<String, String> currencyMap = new HashMap<String, String>();
	private UserDao userDao = new UserDao();
	private List<Merchants> merchantList = new ArrayList<Merchants>();

	@SuppressWarnings("unchecked")
	public String execute() {
		PropertiesManager propertyManager = new PropertiesManager();
		InvoiceTransactionDao invoiceTransactionDao = new InvoiceTransactionDao();
		UrlShortener urlShortener = new UrlShortener();

		try {
			User user = (User) sessionMap.get(Constants.USER.getValue());
			merchantList = new UserDao().getActiveMerchantList();
			invoice.setInvoiceId(TransactionManager.getNewTransactionId());
			invoice.setCreateDate(new Date());
			if (user.getUserType().equals(UserType.ADMIN) || user.getUserType().equals(UserType.SUBADMIN)
					|| user.getUserType().equals(UserType.RESELLER) || user.getUserType().equals(UserType.SUPERADMIN)) {
				currencyMap = Currency.getAllCurrency();
				SessionUserIdentifier userIdentifier = new SessionUserIdentifier();
				setMerchantPayId(userIdentifier.getMerchantPayId(user, getMerchant()));
				invoice.setPayId(getMerchantPayId());
			} else {
				currencyMap = Currency.getSupportedCurreny(user);
				if (user.getUserType().equals(UserType.SUBUSER)) {
					String parentPayId = user.getParentPayId();
					User parentUser = userDao.findPayId(parentPayId);
					currencyMap = Currency.getSupportedCurreny(parentUser);
					invoice.setPayId(user.getParentPayId());
				} else if (user.getUserType().equals(UserType.MERCHANT)) {
					invoice.setPayId(user.getPayId());
				}
			}
			invoice.setInvoiceType(PromotionalPaymentType.INVOICE_PAYMENT.getName());
			invoice.setSaltKey(AccountPasswordScrambler.encrpytPwd(invoice.getInvoiceId()));
			invoice.setReturnUrl(propertyManager.getSystemProperty(CrmFieldConstants.INVOICE_RETURN_URL.getValue()));
			url = propertyManager.getSystemProperty(CrmFieldConstants.INVOICE_URL.getValue()) + invoice.getInvoiceId();
			invoice.setShortUrl(urlShortener.shortenUrl(url));
			invoiceTransactionDao.create(invoice);
			if (isEmailCheck()) {
				EmailBuilder emailBuilder = new EmailBuilder();
				emailBuilder.invoiceLinkEmail(url, invoice.getEmail(), invoice.getName());
			}
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("error generating invoice url", exception);
			return ERROR;
		}
	}

	@SkipValidation
	public String invoiceEmail() {
		CrmValidator validator = new CrmValidator();
		InvoiceTransactionDao invoiceTransactionDao = new InvoiceTransactionDao();

		try {
			// custom validation
			if ((validator.validateBlankField(invoice.getInvoiceId()))) {
				addActionError(validator.getResonseObject().getResponseMessage());
				return ERROR;
			} else if (!(validator.validateField(CrmFieldType.INVOICE_ID, invoice.getInvoiceId()))) {
				return ERROR;
			}
			EmailBuilder emailBuilder = new EmailBuilder();
			Invoice invoiceDB = invoiceTransactionDao.findByInvoiceId(invoice.getInvoiceId());
			PropertiesManager propertyManager = new PropertiesManager();
			setUrl(propertyManager.getSystemProperty(CrmFieldConstants.INVOICE_URL.getValue())
					+ invoiceDB.getInvoiceId());
			emailBuilder.invoiceLinkEmail(getUrl(), invoiceDB.getEmail(), invoiceDB.getName());
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	@SkipValidation
	public String invoiceSMS() {
		CrmValidator validator = new CrmValidator();
		InvoiceTransactionDao invoiceTransactionDao = new InvoiceTransactionDao();

		try {
			// custom validation
			if ((validator.validateBlankField(invoice.getInvoiceId()))) {
				addActionError(validator.getResonseObject().getResponseMessage());
				return ERROR;
			} else if (!(validator.validateField(CrmFieldType.INVOICE_ID, invoice.getInvoiceId()))) {
				return ERROR;
			}
			Invoice invoiceDB = invoiceTransactionDao.findByInvoiceId(invoice.getInvoiceId());
			PropertiesManager propertyManager = new PropertiesManager();
			setUrl(propertyManager.getSystemProperty(CrmFieldConstants.INVOICE_URL.getValue())
					+ invoiceDB.getInvoiceId());
			invoiceTransactionDao.update(invoiceDB);
			SmsSender smsSender = new SmsSender();
			smsSender.sendPromoSMS(invoiceDB, invoiceDB.getShortUrl());
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public void validate() {
		CrmValidator validator = new CrmValidator();
		User user = (User) sessionMap.get(Constants.USER);
		merchantList = new UserDao().getActiveMerchantList();
		if (user.getUserType().equals(UserType.ADMIN) || user.getUserType().equals(UserType.SUBADMIN)
				|| user.getUserType().equals(UserType.SUPERADMIN)) {
			currencyMap = Currency.getAllCurrency();
			;
			if (getMerchant().equals(CrmFieldConstants.SELECT_MERCHANT.getValue())) {
				addFieldError(CrmFieldConstants.MERCHANT.getValue(), CrmFieldConstants.SELECT_MERCHANT.getValue());
			}
		} else {
			currencyMap = Currency.getSupportedCurreny(user);
			if (user.getUserType().equals(UserType.SUBUSER)) {
				String parentPayId = user.getParentPayId();
				User parentUser = userDao.findPayId(parentPayId);
				currencyMap = Currency.getSupportedCurreny(parentUser);
			}
		}
		if ((validator.validateBlankField(invoice.getInvoiceNo()))) {
			addFieldError(CrmFieldType.INVOICE_NUMBER.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.INVOICE_NUMBER, invoice.getInvoiceNo()))) {
			addFieldError(CrmFieldType.INVOICE_NUMBER.getName(), validator.getResonseObject().getResponseMessage());
		}
	
		if ((validator.validateBlankField(invoice.getPhone()))) {
			addFieldError(CrmFieldType.INVOICE_PHONE.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.INVOICE_PHONE, invoice.getPhone()))) {
			addFieldError(CrmFieldType.INVOICE_PHONE.getName(), validator.getResonseObject().getResponseMessage());
		}

		if (validator.validateBlankField(invoice.getEmail())) {
			addFieldError(CrmFieldType.INVOICE_EMAIL.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.isValidEmailId(invoice.getEmail()))) {
			addFieldError(CrmFieldType.INVOICE_EMAIL.getName(), validator.getResonseObject().getResponseMessage());
		}
		if ((validator.validateBlankField(invoice.getAmount()))) {
			addFieldError(CrmFieldType.INVOICE_AMOUNT.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.INVOICE_AMOUNT, invoice.getAmount()))) {
			addFieldError(CrmFieldType.INVOICE_AMOUNT.getName(), validator.getResonseObject().getResponseMessage());
		}
		if ((validator.validateBlankField(invoice.getServiceCharge()))) {
			invoice.setServiceCharge("0.00");
		} else if (!(validator.validateField(CrmFieldType.INVOICE_AMOUNT, invoice.getServiceCharge()))) {
			addFieldError("serviceCharge", validator.getResonseObject().getResponseMessage());
		}
		if ((validator.validateBlankField(invoice.getTotalAmount()))) {
			addFieldError("totalAmount", validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.INVOICE_AMOUNT, invoice.getTotalAmount()))) {
			addFieldError("totalAmount", validator.getResonseObject().getResponseMessage());
		}

		
		if ((validator.validateBlankField(invoice.getBusinessName()))) {
		} else if (!(validator.validateField(CrmFieldType.BUSINESS_NAME, invoice.getBusinessName()))) {
			addFieldError(CrmFieldType.BUSINESS_NAME.getName(), validator.getResonseObject().getResponseMessage());
		}
		
		if ((validator.validateBlankField(invoice.getQuantity()))) {
		} else if (!(validator.validateField(CrmFieldType.QUANTITY, invoice.getQuantity()))) {
			addFieldError(CrmFieldType.QUANTITY.getName(), validator.getResonseObject().getResponseMessage());
		}
		
		if ((validator.validateBlankField(invoice.getProductDesc()))) {
		} else if (!(validator.validateField(CrmFieldType.INVOICE_DESCRIPTION, invoice.getProductDesc()))) {
			addFieldError(CrmFieldType.INVOICE_DESCRIPTION.getName(),
					validator.getResonseObject().getResponseMessage());
		}
		if (invoice.getCurrencyCode().equals(CrmFieldConstants.SELECT_CURRENCY.getValue())
				|| validator.validateBlankField(invoice.getCurrencyCode())) {
			addFieldError(CrmFieldType.INVOICE_CURRENCY_CODE.getName(), CrmFieldConstants.SELECT_CURRENCY.getValue());
		} else if (!(validator.validateField(CrmFieldType.INVOICE_CURRENCY_CODE, invoice.getCurrencyCode()))) {
			addFieldError(CrmFieldType.INVOICE_CURRENCY_CODE.getName(), CrmFieldConstants.SELECT_CURRENCY.getValue());
		}
		if (invoice.getExpiresDay().isEmpty()) {
			if (invoice.getExpiresHour().isEmpty()) {
				addFieldError(CrmFieldType.INVOICE_EXPIRES_DAY.getName(),
						validator.getResonseObject().getResponseMessage());
				addFieldError(CrmFieldType.INVOICE_EXPIRES_HOUR.getName(),
						ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
			} else {
				invoice.setExpiresDay("0");
			}
		} else if (invoice.getExpiresHour().isEmpty()) {
			invoice.setExpiresHour("0");
		} else if ((Integer.parseInt(invoice.getExpiresDay().toString()) == 0
				&& Integer.parseInt(invoice.getExpiresHour().toString()) == 0)
				|| (Integer.parseInt(invoice.getExpiresDay().toString()) < 0
						|| Integer.parseInt(invoice.getExpiresHour().toString()) < 0)) {
			addFieldError(CrmFieldType.INVOICE_EXPIRES_DAY.getName(),
					validator.getResonseObject().getResponseMessage());
			addFieldError(CrmFieldType.INVOICE_EXPIRES_HOUR.getName(),
					validator.getResonseObject().getResponseMessage());
		}
		if ((validator.validateBlankField(invoice.getInvoiceId()))) {
		} else if (!(validator.validateField(CrmFieldType.INVOICE_ID, invoice.getInvoiceId()))) {
			addFieldError(CrmFieldType.INVOICE_ID.getName(), validator.getResonseObject().getResponseMessage());
		}
	}

	@Override
	public Invoice getModel() {
		return invoice;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isEmailCheck() {
		return emailCheck;
	}

	public void setEmailCheck(boolean emailCheck) {
		this.emailCheck = emailCheck;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}

	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getMerchantPayId() {
		return merchantPayId;
	}

	public void setMerchantPayId(String merchantPayId) {
		this.merchantPayId = merchantPayId;
	}

	public List<Merchants> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<Merchants> merchantList) {
		this.merchantList = merchantList;
	}
}
