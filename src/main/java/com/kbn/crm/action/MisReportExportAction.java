package com.kbn.crm.action;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CSVUtils;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.TransactionManager;
import com.kbn.crm.charging.TdrCalculator;
import com.kbn.emailer.EmailBuilder;
import com.kbn.pg.core.NodalAccountMapper;
public class MisReportExportAction {
	private static Logger logger = Logger.getLogger(MisReportExportAction.class.getName());

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	static int serialNumber;
	public void exportCSV(String acquirer, String payId, String fromDate, String toDate, UserType admin)
			throws SystemException, IOException {
		PropertiesManager propManager = new PropertiesManager();
		String fileName = propManager.getMisReports("misFileLocation");
		EmailBuilder emailBuilder = new EmailBuilder();
		List<TransactionSummaryReport> transactionSummaryList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat misReportDateFormate = new SimpleDateFormat(
				CrmFieldConstants.INPUT_DATE_FOR_MIS_REPORT.getValue());
		TdrCalculator tdrCalculator = new TdrCalculator();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection.prepareStatement("{call misReportsExport(?,?,?,?,?)}")) {
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, acquirer);
				prepStmt.setString(5, admin.toString());

				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						String strDate = misReportDateFormate.format(new Date());

						transReport.setBusinessName(rs.getString(CrmFieldType.BUSINESS_NAME.getName()));
						transReport.setAccountNumber(rs.getString(CrmFieldType.ACCOUNT_NO.getName()));
						transReport.setIfscCode(rs.getString(CrmFieldType.IFSC_CODE.getName()));
						transReport.setBankName(rs.getString(CrmFieldType.BANK_NAME.getName()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID.getName()));
						transReport.setTransactionId(rs.getString(FieldType.TXN_ID.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS.toString()));
						transReport.setTxnDate(rs.getString(CrmFieldConstants.CREATE_DATE.toString()));
						transReport.setApprovedAmount(rs.getString(FieldType.AMOUNT.toString()));
						transReport.setAcquirer(rs.getString(FieldType.ACQUIRER_TYPE.getName()));
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE.getName()));
						transReport.setCurrentDate(strDate);
						String acquirerType = transReport.getAcquirer();
						if (acquirerType.equals(NodalAccountMapper.FSS.getCode())) {
							transReport.setBankRecieveFund(CrmFieldConstants.FSS_BANK.getValue());
						} else if (acquirerType.equals(NodalAccountMapper.AMEX.getCode())) {
							transReport.setBankRecieveFund(CrmFieldConstants.AMEX_BANK.getValue());
						} else if (acquirerType.equals(NodalAccountMapper.DIREC_PAY.getCode())) {
							transReport.setBankRecieveFund(CrmFieldConstants.DIREC_PAY_BANK.getValue());
						} else if (acquirerType.equals(NodalAccountMapper.MOBIKWIK.getCode())) {
							transReport.setBankRecieveFund(CrmFieldConstants.MOBIKWIK_BANK.getValue());
						}

						if (null != rs.getString(FieldType.CURRENCY_CODE.toString())) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs.getString(FieldType.CURRENCY_CODE.toString())));
						} else {
							transReport.setCurrencyName("");
						}
						if (null != rs.getString(FieldType.PAYMENT_TYPE.toString())) {
							transReport.setPaymentMethod(
									PaymentType.getpaymentName(rs.getString(FieldType.PAYMENT_TYPE.toString())));
						} else {
							transReport.setPaymentMethod("");
						}

						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport.setMopType("");
						}

						transReport.setCurrencyCode(rs.getString(FieldType.CURRENCY_CODE.getName()));
						if (!acquirerType.equals(NodalAccountMapper.CITRUS_PAY.getCode())) {
							transReport = tdrCalculator.setMisTransactionReportCalculate(transReport);

						}
						// Set date again as per display format
						transReport.setTxnDate(
								misReportDateFormate.format(rs.getTimestamp(CrmFieldConstants.CREATE_DATE.toString())));
						transactionSummaryList.add(transReport);

					}
                
					FileWriter writer = new FileWriter(fileName);
					// Fetch Header Value in misSettlement.properties file
					CSVUtils.writeLine(writer,
							Arrays.asList(propManager.getMisAllAcquireExportCSV("allAcquirerHeaderKey")));
					for (TransactionSummaryReport transactionSummaryReport : transactionSummaryList) {
						List<String> list = new ArrayList<>();
						if (transactionSummaryReport.getSerialNumber() == null) {
							serialNumber++;
							transactionSummaryReport.setSerialNumber((Integer.toString(serialNumber)));
							list.add(transactionSummaryReport.getSerialNumber());
						}
						list.add(transactionSummaryReport.getBusinessName());
						list.add("'"+transactionSummaryReport.getPayId());
						list.add("'"+transactionSummaryReport.getTransactionId());
						list.add("'"+transactionSummaryReport.getAccountNumber());
						if (transactionSummaryReport.getNodalAccount() == null) {
							transactionSummaryReport.setNodalAccount(
									propManager.getMisAllAcquireExportCSV("allAcquirerNodalAccountNumber"));
							list.add(transactionSummaryReport.getNodalAccount());
						}
						if (transactionSummaryReport.getAggregatorName() == null) {
							transactionSummaryReport
									.setAggregatorName(propManager.getMisAllAcquireExportCSV("aggregatorName"));
							list.add(transactionSummaryReport.getAggregatorName());
						}
						list.add(transactionSummaryReport.getTxnType());
						list.add(transactionSummaryReport.getTxnDate());
						list.add(transactionSummaryReport.getBankName());
						list.add(transactionSummaryReport.getIfscCode());
						list.add(transactionSummaryReport.getBankRecieveFund());
						list.add(transactionSummaryReport.getApprovedAmount());
						list.add(transactionSummaryReport.getTotalAggregatorcommissionAmount());
						list.add(transactionSummaryReport.getTotalAmountPayToMerchant());
						list.add(transactionSummaryReport.getTotalPayoutToNodal());
						list.add(transactionSummaryReport.getTxnDate());
						CSVUtils.writeLine(writer, list);
					}
					writer.close();
					//DO save Settlement report 
					
					
					
					// Fetch email Value in misSettlement.properties file
					emailBuilder.sendMisReport(
							propManager.getMisAllAcquireExportCSV("AllAcquirerSendSettlementReportEmailId"),
							propManager.getMisAllAcquireExportCSV("subject"), fileName);
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
	}

	// CitrusSettlementReport
	public List<TransactionSummaryReport> exportCitrusCSV(String acquirer, String payId, String fromDate, String toDate,
			UserType admin) throws SystemException, IOException {
		PropertiesManager propManager = new PropertiesManager();
		String fileName = propManager.getMisReports("citrusMisFileLocation");
		EmailBuilder emailBuilder = new EmailBuilder();
		List<TransactionSummaryReport> transactionSummaryList = new ArrayList<TransactionSummaryReport>();
		Map<String, TransactionSummaryReport> mapTransactionSummaryList = new HashMap<String, TransactionSummaryReport>();
		SimpleDateFormat misReportDateFormate = new SimpleDateFormat(
				CrmFieldConstants.INPUT_DATE_FOR_MIS_REPORT.getValue());
		TdrCalculator tdrCalculator = new TdrCalculator();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection.prepareStatement("{call misReportsExport(?,?,?,?,?)}")) {
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, acquirer);
				prepStmt.setString(5, admin.toString());
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						String strDate = misReportDateFormate.format(new Date());
						transReport.setBusinessName(rs.getString(CrmFieldType.BUSINESS_NAME.getName()));
						transReport.setAccountNumber(rs.getString(CrmFieldType.ACCOUNT_NO.getName()));
						transReport.setIfscCode(rs.getString(CrmFieldType.IFSC_CODE.getName()));
						transReport.setBankName(rs.getString(CrmFieldType.BANK_NAME.getName()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID.getName()));
						transReport.setTransactionId(rs.getString(FieldType.TXN_ID.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS.toString()));
						transReport.setTxnDate(rs.getString(CrmFieldConstants.CREATE_DATE.toString()));
						transReport.setApprovedAmount(rs.getString(FieldType.AMOUNT.toString()));
						transReport.setAcquirer(rs.getString(FieldType.ACQUIRER_TYPE.getName()));
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE.getName()));
						transReport.setCurrentDate(strDate);
						transReport.setTxnRefNumber(getTxnRefNumber());
						if (null != rs.getString(FieldType.CURRENCY_CODE.toString())) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs.getString(FieldType.CURRENCY_CODE.toString())));
						} else {
							transReport.setCurrencyName("");
						}
						if (null != rs.getString(FieldType.PAYMENT_TYPE.toString())) {
							transReport.setPaymentMethod(
									PaymentType.getpaymentName(rs.getString(FieldType.PAYMENT_TYPE.toString())));
						} else {
							transReport.setPaymentMethod("");
						}

						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport.setMopType("");
						}
						transReport.setCurrencyCode(rs.getString(FieldType.CURRENCY_CODE.getName()));
						transReport = tdrCalculator.setMisTransactionReportCalculate(transReport);
						transReport.setTotalAmountPayToMerchant(transReport.getTotalPayoutToNodal());
						String ifcsCode = rs.getString(CrmFieldType.IFSC_CODE.getName()).substring(0, 4);
						if (ifcsCode.equals(CrmFieldConstants.IFSC_CODE.getValue())) {
							transReport.setMsgType("R41");
						} else {
							transReport.setMsgType("NO6");
						}
						// Parse date as per display format
						transReport.setTxnDate(
								misReportDateFormate.format(rs.getTimestamp(CrmFieldConstants.CREATE_DATE.toString())));
						transactionSummaryList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}

		for (TransactionSummaryReport transactionSummaryReport : transactionSummaryList) {
			String payid = transactionSummaryReport.getPayId();

			// Check condition when TotalAmountPayToMerchant is Null
			String amount = transactionSummaryReport.getTotalAmountPayToMerchant();
			if (amount == null) {
				transactionSummaryReport.setTotalAmountPayToMerchant("0");
			}
			if (!mapTransactionSummaryList.containsKey(payid)) {
				mapTransactionSummaryList.put(payid, transactionSummaryReport);
			} else {
				BigDecimal totalMerchantNodalAmount = new BigDecimal(
						transactionSummaryReport.getTotalAmountPayToMerchant());
				TransactionSummaryReport transactionSummaryReportFromMap = mapTransactionSummaryList.get(payid);
				transactionSummaryReportFromMap.setTotalAmountPayToMerchant(totalMerchantNodalAmount
						.add(new BigDecimal(transactionSummaryReportFromMap.getTotalAmountPayToMerchant())).toString());
				mapTransactionSummaryList.put(payid, transactionSummaryReportFromMap);
			}
		}
		transactionSummaryList.clear();

		for (Entry<String, TransactionSummaryReport> entry : mapTransactionSummaryList.entrySet()) {
			transactionSummaryList.add(entry.getValue());
		}
		Date date = new Date();
		SimpleDateFormat AppDateFormat = new SimpleDateFormat(CrmFieldConstants.INPUT_DATE_FOR_MIS_REPORT.getValue());
		FileWriter writer = new FileWriter(fileName);
		CSVUtils.writeLine(writer, Arrays.asList(propManager.getMisAllAcquireExportCSV("citrusHeader"),
				AppDateFormat.format(date), propManager.getMisAllAcquireExportCSV("topHeaderPgName")));
		// Fetch Header Value in misSettlement.properties file
		CSVUtils.writeLine(writer, Arrays.asList(propManager.getMisAllAcquireExportCSV("citrusSettlementHeaderKey")));
		for (TransactionSummaryReport transactionSummaryReport : transactionSummaryList) {
			serialNumber++;
			List<String> list = new ArrayList<>();
			if (transactionSummaryReport.getDetail() == null) {
				transactionSummaryReport.setDetail(propManager.getMisAllAcquireExportCSV("detail"));
				list.add(transactionSummaryReport.getDetail());
			}
			list.add(transactionSummaryReport.getMsgType());
			if (transactionSummaryReport.getNodalAccount() == null) {
				transactionSummaryReport
						.setNodalAccount(propManager.getMisAllAcquireExportCSV("citrusNodalAccountNumber"));
				list.add(transactionSummaryReport.getNodalAccount());
			}
			if (transactionSummaryReport.getAggregatorName() == null) {
				transactionSummaryReport.setAggregatorName(propManager.getMisAllAcquireExportCSV("aggregatorName"));
				list.add(transactionSummaryReport.getAggregatorName());
			}
			if (transactionSummaryReport.getAddressLine1() == null) {
				transactionSummaryReport.setAddressLine1("");
				list.add(transactionSummaryReport.getAddressLine1());
			}
			if (transactionSummaryReport.getAddressLine2() == null) {
				transactionSummaryReport.setAddressLine2("");
				list.add(transactionSummaryReport.getAddressLine2());
			}
			if (transactionSummaryReport.getAddressLine3() == null) {
				transactionSummaryReport.setAddressLine3("");
				list.add(transactionSummaryReport.getAddressLine3());
			}
			list.add(transactionSummaryReport.getIfscCode());
			list.add("'"+transactionSummaryReport.getAccountNumber());
			list.add(transactionSummaryReport.getBusinessName());
			if (transactionSummaryReport.getBenAddress1() == null) {
				transactionSummaryReport.setBenAddress1("");
				list.add(transactionSummaryReport.getBenAddress1());
			}
			if (transactionSummaryReport.getBenAddress2() == null) {
				transactionSummaryReport.setBenAddress2("");
				list.add(transactionSummaryReport.getBenAddress2());
			}
			if (transactionSummaryReport.getBenAddress3() == null) {
				transactionSummaryReport.setBenAddress3(propManager.getMisAllAcquireExportCSV("benAddress3"));
				list.add(transactionSummaryReport.getBenAddress3());
			}
			if (transactionSummaryReport.getBenAddress4() == null) {
				transactionSummaryReport.setBenAddress4("");
				list.add(transactionSummaryReport.getBenAddress4());
			}
			list.add(transactionSummaryReport.getTxnDate());
			list.add(transactionSummaryReport.getTotalAmountPayToMerchant());
			if (transactionSummaryReport.getSendertoRcvrInfo() == null) {
				transactionSummaryReport.setSendertoRcvrInfo(propManager.getMisAllAcquireExportCSV("sendertoRcvrInfo"));
				list.add(transactionSummaryReport.getSendertoRcvrInfo());
			}
			list.add("'"+transactionSummaryReport.getTxnRefNumber());
			CSVUtils.writeLine(writer, list);
			CSVUtils.writeLine(writer, Arrays.asList(propManager.getMisAllAcquireExportCSV("citrusFooter"),
					Integer.toString(serialNumber)));
		}
		writer.close();
		// Fetch email Value in misSettlement.properties file
		emailBuilder.sendMisReport(propManager.getMisAllAcquireExportCSV("citrusAcquirerEmailId"),
				propManager.getMisAllAcquireExportCSV("subject"), fileName);
		return transactionSummaryList;

	}

	private String getTxnRefNumber() {
		return TransactionManager.getNewTransactionId();
	}

}
