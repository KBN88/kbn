package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionDetailsService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CustTransactionAuthentication;
import com.kbn.commons.util.UserStatusType;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Sunil
 *
 */

public class EmailValidationAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(EmailValidationAction.class.getName());
	private static final long serialVersionUID = 5995449017764989418L;

	private String id;
	private UserDao userDao = new UserDao();
	private User user = new User();
	public String execute(){
		try{
			user = userDao.findByAccountValidationKey(getId());
			
			if(getId() == null){
				return ERROR;
			}else if(user == null) {
				addActionMessage(ErrorType.ALREADY_VALIDATE_EMAIL.getResponseMessage());
				return "validate";
			}
			else if(user.isEmailValidationFlag()) {
				addActionMessage(ErrorType.ALREADY_VALIDATE_EMAIL.getResponseMessage());
				return "validate";
			}
			if(user.getUserType().equals(UserType.ADMIN)){
			userDao.updateEmailValidation(getId(), UserStatusType.ACTIVE , true);
			}else{
				userDao.updateEmailValidation(getId(), UserStatusType.SUSPENDED , true);
			}
			return SUCCESS;
		}
		catch(Exception exception){
			logger.error("Exception", exception);
			return ERROR;
		}
	}
	
	public String transactionAuthentication(){
		try{			
			if(getId() == null){
				return ERROR;
			}
			TransactionDetailsService transactionDetailsService = new TransactionDetailsService();
			String txnAuthentication= transactionDetailsService.getTransactionAuthentication(getId());
			
			if(txnAuthentication.equals(CustTransactionAuthentication.SUCCESS.getAuthenticationName())){
				return "alreadyAuthenticate";
			}
			transactionDetailsService.updateTransactionAuthentication(CustTransactionAuthentication.SUCCESS.getAuthenticationName(), getId());
			return SUCCESS;
		}
		catch(Exception exception){
			logger.error("Exception", exception);
			return ERROR;
		}
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
