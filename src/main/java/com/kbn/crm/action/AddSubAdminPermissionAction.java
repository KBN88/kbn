package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.struts2.dispatcher.SessionMap;

import com.kbn.commons.user.PermissionType;
import com.kbn.commons.util.Constants;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Rahul
 *
 */
public class AddSubAdminPermissionAction extends AbstractSecureAction {

	private static final long serialVersionUID = -6847937476962274485L;

	@SuppressWarnings("null")
	public List<PermissionType> getSubAdminPermissionType(SessionMap<String, Object> sessionMap) {

		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

		String permision = permissions.toString();

		List<String> list = new ArrayList<String>(Arrays.asList(permision.split("-")));
		List<PermissionType> permissionTypeList = new ArrayList<PermissionType>();
		for (String permissionName : list) {
			PermissionType pType = PermissionType.getInstanceFromName(permissionName);
			permissionTypeList.add(pType);
		}

		return permissionTypeList;

	}

}
