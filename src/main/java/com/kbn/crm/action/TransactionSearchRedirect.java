package com.kbn.crm.action;

import com.kbn.commons.user.TransactionSearch;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.opensymphony.xwork2.ModelDriven;

public class TransactionSearchRedirect extends AbstractSecureAction implements
		ModelDriven<TransactionSearch> {

	private static final long serialVersionUID = 5763627063996812999L;
	private TransactionSearch transactionSearch = new TransactionSearch();

	@Override
	public String execute() {
		return SUCCESS;
	}

	public TransactionSearch getModel() {
		return transactionSearch;
	}
}
