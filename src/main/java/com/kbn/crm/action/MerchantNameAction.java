package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.StatusType;
import com.kbn.crm.actionBeans.CurrencyMapProvider;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.Currency;

/**
 * @author Chandan
 *
 */
public class MerchantNameAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(MerchantNameAction.class
			.getName());
	private static final long serialVersionUID = -5990800125330748024L;

	private List<Merchants> merchantList = new ArrayList<Merchants>();
	private Map<String, String> currencyMap = new HashMap<String, String>();
	private User sessionUser = null;
	private List<StatusType> lst;

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			if(sessionUser.getUserType().equals(UserType.ADMIN)
					|| sessionUser.getUserType().equals(UserType.SUPERADMIN)) {
				setMerchantList(new UserDao().getMerchantActiveList());
				currencyMap = Currency.getAllCurrency();
			}else if(sessionUser.getUserType().equals(UserType.RESELLER)) {
				setMerchantList(new UserDao().getResellerMerchantList(sessionUser.getResellerId()));
				currencyMap = Currency.getAllCurrency();
			}else if(sessionUser.getUserType().equals(UserType.MERCHANT) || sessionUser.getUserType().equals(UserType.SUBUSER)  || sessionUser.getUserType().equals(UserType.SUBACQUIRER)) {
				Merchants merchant = new Merchants();
				merchant.setEmailId(sessionUser.getEmailId());
				merchant.setBusinessName(sessionUser.getBusinessName());
				merchant.setPayId(sessionUser.getPayId());
				merchantList.add(merchant);
				if(sessionUser.getUserType().equals(UserType.SUBUSER) || sessionUser.getUserType().equals(UserType.SUBACQUIRER)){
					String parentMerchantPayId = sessionUser.getParentPayId();
					UserDao userDao = new UserDao();
					User parentMerchant = userDao
							.findPayId(parentMerchantPayId);
					merchant.setMerchant(parentMerchant);
					merchantList.add(merchant);
					Object[] obj = merchantList.toArray();
					for(Object sortList : obj){
						if(merchantList.indexOf(sortList) != merchantList.lastIndexOf(sortList)){
							merchantList.remove(merchantList.lastIndexOf(sortList));
						}
					}
				}
				CurrencyMapProvider currencyMapProvider = new CurrencyMapProvider();
 				currencyMap = currencyMapProvider.currencyMap(sessionUser);
			}

			setLst(StatusType.getStatusType());
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}

		return INPUT;
	}

	public String subUserList() {
		try {
			sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			if (sessionUser.getUserType().equals(UserType.MERCHANT)) {
				setMerchantList(new UserDao().getSubUserList(sessionUser
						.getPayId()));
				currencyMap = Currency.getSupportedCurreny(sessionUser);
			} else if (sessionUser.getUserType().equals(UserType.SUBUSER) || sessionUser.getUserType().equals(UserType.SUBACQUIRER)) {
				Merchants merchant = new Merchants();
				User user = new User();
				UserDao userDao = new UserDao();
				user = userDao.findPayId(sessionUser.getParentPayId());
				merchant.setEmailId(user.getEmailId());
				merchant.setBusinessName(user.getBusinessName());
				merchant.setPayId(user.getPayId());
				merchantList.add(merchant);
				currencyMap = Currency.getSupportedCurreny(sessionUser);
			}

			setLst(StatusType.getStatusType());
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}

		return INPUT;
	}

	public List<Merchants> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<Merchants> merchantList) {
		this.merchantList = merchantList;
	}

	public List<StatusType> getLst() {
		return lst;
	}

	public void setLst(List<StatusType> lst) {
		this.lst = lst;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}

	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

}
