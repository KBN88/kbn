package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.UserDao;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailBuilder;

public class SendBulkEmailAction extends AbstractSecureAction {

	/**
	 * @neeraj
	 */
	private static final long serialVersionUID = 2421842267236138348L;
	private static Logger logger = Logger.getLogger(SendBulkEmailAction.class.getName());
	private String subject;
	private String messageBody;

	public String sendBulkEmail() {
		EmailBuilder emailBuilder = new EmailBuilder();
		List<Merchants> merchantsList = new ArrayList<Merchants>();
		try {
			merchantsList = new UserDao().featchAllmerchant();
			boolean first = true;
			String emailID = "";
			for (Merchants merchants : merchantsList) {
				String merchantemailId = merchants.getEmailId();
				if (first) {
					emailID += merchantemailId;
					first = false;
				} else {
					emailID += "," + merchantemailId;
				}
			}
			emailBuilder.sendBulkEmailServiceTaxUpdate(emailID,subject,messageBody);
			addActionMessage("Email successfully sends.");
		} catch (SystemException exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;

	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
