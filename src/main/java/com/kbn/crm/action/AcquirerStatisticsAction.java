package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.AcquirerStatisticsService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Statistics;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class AcquirerStatisticsAction extends AbstractSecureAction{

	/**
	 * @Neeraj 
	 */
	private static final long serialVersionUID = -6520984184729590169L;
	private static Logger logger = Logger.getLogger(AcquirerStatisticsAction.class.getName());
	private AcquirerStatisticsService getAcquirerStatistics = new AcquirerStatisticsService();
	private Statistics statistics = new Statistics();
	private String currency;
	public String execute() {
		User user = (User) sessionMap.get(Constants.USER.getValue());
		try {
			
			setStatistics(getAcquirerStatistics.getDashboardValues(user.getFirstName(),getCurrency()));
			
			return SUCCESS;	
		
	} catch (Exception exception) {
		logger.error("Exception", exception);
		return ERROR;
	}
}
	public void validate(){
		CrmValidator validator = new CrmValidator();
		
		if(validator.validateBlankField(getCurrency())){
		}
        else if(!validator.validateField(CrmFieldType.CURRENCY, getCurrency())){
        	addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}
	public Statistics getStatistics() {
		return statistics;
	}

	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}



}
