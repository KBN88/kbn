package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.PieChartService;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class ChartAction extends AbstractSecureAction {
	/**
	 * 
	 */
	private static Logger logger = Logger
			.getLogger(ChartAction.class.getName());
	private static final long serialVersionUID = -6570650892300476380L;
	private PieChart pieChart = new PieChart();
	private PieChartService getPieChartService = new PieChartService();
	private String emailId;
	private String currency;
	private String dateFrom;
	private String dateTo;

	public String execute() {
		try {

			UserDao userDao = new UserDao();
			User user = (User) sessionMap.get(Constants.USER.getValue());
			if (user.getUserType().equals(UserType.MERCHANT)
					|| user.getUserType().equals(UserType.POSMERCHANT)) {
				setPieChart(getPieChartService.getDashboardValues(
						user.getPayId(), getCurrency(), getDateFrom(),
						getDateTo()));

			} else if (user.getUserType().equals(UserType.ACQUIRER)) {
				setPieChart(getPieChartService.getPieChartDashboardValues(
						user.getFirstName(), getCurrency(), getDateFrom(),
						getDateTo()));

				return SUCCESS;
			} else {
				if (getEmailId().equals(
						CrmFieldConstants.ALL_MERCHANTS.getValue())) {
					setPieChart(getPieChartService.getDashboardValues(
							getEmailId(), getCurrency(), getDateFrom(),
							getDateTo()));
				} else {
					setPieChart(getPieChartService.getDashboardValues(userDao
							.findPayIdByEmail(getEmailId()).getPayId(),
							getCurrency(), getDateFrom(), getDateTo()));
				}
			}
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public PieChart getPieChart() {
		return pieChart;
	}

	public void setPieChart(PieChart pieChart) {
		this.pieChart = pieChart;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

}