package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.HourlyAnalyticsTrasactionsService;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class HourlyAnalyticsTrasactionsAction extends AbstractSecureAction {

	/**
	 * @ Neeraj
	 */
	private static final long serialVersionUID = -1459984742006150832L;
	private static Logger logger = Logger.getLogger(HourlyAnalyticsTrasactionsAction.class.getName());
	private List<PieChart> pieChart = new ArrayList<PieChart>();
	private HourlyAnalyticsTrasactionsService getHourlyAnalyticsTrasactions = new HourlyAnalyticsTrasactionsService();
	private String payId;
	private String currency;
	private String dateFrom;
	private String dateTo;
	private String industryTypes;

	public String execute() {
		try {
			if (getPayId().equals(CrmFieldConstants.ALL.getValue())) {

				setPieChart(getHourlyAnalyticsTrasactions.preparelist(getPayId(), getIndustryTypes(), getCurrency(),
						getDateFrom(), getDateTo()));
			} else {
				setPieChart(getHourlyAnalyticsTrasactions.preparelist(getPayId(), getIndustryTypes(), getCurrency(),
						getDateFrom(), getDateTo()));
			}
			return SUCCESS;

		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public List<PieChart> getPieChart() {
		return pieChart;
	}

	public void setPieChart(List<PieChart> pieChart) {
		this.pieChart = pieChart;
	}

	public String getIndustryTypes() {

		return industryTypes;
	}

	public void setIndustryTypes(String industryTypes) {
		this.industryTypes = industryTypes;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

}
