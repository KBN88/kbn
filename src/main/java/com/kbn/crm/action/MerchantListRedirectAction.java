package com.kbn.crm.action;

import java.util.Map;
import java.util.TreeMap;

import com.kbn.commons.util.BusinessType;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 *
 */
public class MerchantListRedirectAction extends AbstractSecureAction {

	private static final long serialVersionUID = -2541609533197706532L;
	private Map<String, String> industryTypes = new TreeMap<String, String>();

	public String execute(){
		setIndustryTypes(BusinessType.getIndustryCategoryList());
		return INPUT;
	}

	public Map<String, String> getIndustryTypes() {
		return industryTypes;
	}

	public void setIndustryTypes(Map<String, String> industryTypes) {
		this.industryTypes = industryTypes;
	}


}
