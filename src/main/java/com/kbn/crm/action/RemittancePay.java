package com.kbn.crm.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.Remittance;
import com.kbn.commons.user.RemittanceDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.Currency;
import com.opensymphony.xwork2.ModelDriven;

public class RemittancePay extends AbstractSecureAction implements
		ModelDriven<Remittance> {

	private static Logger logger = Logger.getLogger(RemittancePay.class
			.getName());
	/**
	 * @Isha
	 */
	private static final long serialVersionUID = -7849896163387202051L;

	private Remittance remittance = new Remittance();
	private List<Merchants> merchantList = new ArrayList<Merchants>();
	private Map<String, String> currencyMap = new HashMap<String, String>();
	private boolean checkUtr;
	private User user = new User();

	@SuppressWarnings("unchecked")
	public String execute() {
		RemittanceDao remittanceDao = new RemittanceDao();

		if (remittance.getRemittedAmount().equals("0.0")) {
			fetchRemitt();
			addActionMessage(CrmFieldConstants.AMOUNT_CAN_NOT_BE_BLANK
					.getValue());
			setMerchantList(new UserDao().getActiveMerchantList());
			currencyMap = Currency.getSupportedCurreny(user);

		} else if (remittance.getMerchant().equals("ALL")) {

			addActionMessage(CrmFieldConstants.PLEASE_SELECT_MERCHANT
					.getValue());
			setMerchantList(new UserDao().getActiveMerchantList());
			currencyMap = Currency.getSupportedCurreny(user);

		} else {

			setCheckUtr(remittanceDao.findUtr(remittance.getUtr()));
			if (isCheckUtr() != true) {
				fetchRemitt();
				addActionMessage(CrmFieldConstants.UTR_ALREADY_EXCITED
						.getValue());
				ChangeDateFormat();
				setMerchantList(new UserDao().getActiveMerchantList());

				currencyMap = Currency.getSupportedCurreny(user);
			} else {
				fetchRemitt();

				remittanceDao.create(remittance);
				addActionMessage(CrmFieldConstants.DETAILS_SAVED_SUCCESSFULLY
						.getValue());
				ChangeDateFormat();
				setMerchantList(new UserDao().getActiveMerchantList());

				currencyMap = Currency.getSupportedCurreny(user);
			}
		}
		return SUCCESS;
	}

	private void fetchRemitt() {
		UserDao userDao = new UserDao();

		setUser(userDao.findPayId(remittance.getMerchant()));
		remittance.setPayId(remittance.getMerchant());
		remittance.setMerchant(user.getBusinessName());

		remittance.setStatus(CrmFieldConstants.INITIATED.getValue());

		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = "";
		String remittanceDate = "";
		try {
			startDate = formatDate.format(df.parse(remittance.getDateFrom()));
			remittanceDate = formatDate.format(df.parse(remittance
					.getRemittanceDate()));
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		remittance.setDateFrom(startDate);
		remittance.setRemittanceDate(remittanceDate);

	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if ((validator.validateBlankField(remittance.getMerchant()))) {
		} else if (!(validator.validateField(CrmFieldType.PAY_ID,
				remittance.getMerchant()))) {
			addFieldError("merchant", validator.getResonseObject()
					.getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getUtr()))) {
			addFieldError(CrmFieldType.UTR.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.UTR,
				remittance.getUtr()))) {
			addFieldError(CrmFieldType.UTR.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getRemittedAmount()))) {
			addFieldError("netAmount", validator.getResonseObject()
					.getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.AMOUNT,
				remittance.getRemittedAmount()))) {
			addFieldError("netAmount", validator.getResonseObject()
					.getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getRemittedAmount()))) {
			addFieldError("remittedAmount", validator.getResonseObject()
					.getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.AMOUNT,
				remittance.getRemittedAmount()))) {
			addFieldError("remittedAmount", validator.getResonseObject()
					.getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getStatus()))) {
		} else if (!(validator.validateField(CrmFieldType.DATE_FROM,
				remittance.getStatus()))) {
			addFieldError(CrmFieldType.STATUS.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getRemittanceDate()))) {
			addFieldError("remittanceDate", validator.getResonseObject()
					.getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.DATE_FROM,
				remittance.getRemittanceDate()))) {
			addFieldError("remittanceDate", validator.getResonseObject()
					.getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getDateFrom()))) {
			addFieldError(CrmFieldType.DATE_FROM.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.DATE_FROM,
				remittance.getDateFrom()))) {
			addFieldError(CrmFieldType.DATE_FROM.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getCurrency()))) {
			addFieldError(CrmFieldType.CURRENCY.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.CURRENCY,
				remittance.getCurrency()))) {
			addFieldError(CrmFieldType.CURRENCY.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getIfscCode()))) {
			addFieldError(CrmFieldType.IFSC_CODE.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.IFSC_CODE,
				remittance.getIfscCode()))) {
			addFieldError(CrmFieldType.IFSC_CODE.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getAccountNo()))) {
			addFieldError(CrmFieldType.ACCOUNT_NO.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.ACCOUNT_NO,
				remittance.getAccountNo()))) {
			addFieldError(CrmFieldType.ACCOUNT_NO.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getAccHolderName()))) {
			addFieldError(CrmFieldType.ACC_HOLDER_NAME.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.ACC_HOLDER_NAME,
				remittance.getAccHolderName()))) {
			addFieldError(CrmFieldType.ACC_HOLDER_NAME.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getBankName()))) {
			addFieldError(CrmFieldType.BANK_NAME.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.BANK_NAME,
				remittance.getBankName()))) {
			addFieldError(CrmFieldType.BANK_NAME.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(remittance.getPayId()))) {
		} else if (!(validator.validateField(CrmFieldType.PAY_ID,
				remittance.getPayId()))) {
			addFieldError(CrmFieldType.PAY_ID.getName(), validator
					.getResonseObject().getResponseMessage());
		}
	}
	
	private String ChangeDateFormat(){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
		String startDate = "";
		String remittanceDate = "";
		try {
			startDate = formatDate.format(df.parse(remittance.getDateFrom()));
			remittanceDate = formatDate.format(df.parse(remittance
					.getRemittanceDate()));
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		remittance.setDateFrom(startDate);
		remittance.setRemittanceDate(remittanceDate);
		return SUCCESS;
	}

	@Override
	public Remittance getModel() {
		return remittance;
	}

	public List<Merchants> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<Merchants> merchantList) {
		this.merchantList = merchantList;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}

	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isCheckUtr() {
		return checkUtr;
	}

	public void setCheckUtr(boolean checkUtr) {
		this.checkUtr = checkUtr;
	}

}
