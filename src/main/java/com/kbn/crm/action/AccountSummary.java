package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.kbn.commons.user.Account;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class AccountSummary extends AbstractSecureAction {
	/**
	 * 
	 */
	private static Logger logger = Logger.getLogger(AccountSummary.class
			.getName());
	private static final long serialVersionUID = 959533707653092826L;
	private User user = new User();
     List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
	public String execute() {

		try {
			Set<Account> accountList=new HashSet<Account>();
			user = (User) sessionMap.get(Constants.USER.getValue());
			accountList = user.getAccounts();
			 for (Account account : accountList) {
			if (null != accountList) {
				Set<ChargingDetails> data = account.getChargingDetails();
				for (PaymentType paymentType : PaymentType.values()) {
					String paymentName = paymentType.getName();
				for (ChargingDetails chargingDetails : data) {
					if (chargingDetails.getStatus().equals(TDRStatus.ACTIVE) && chargingDetails.getPaymentType().getName().equals(paymentName)) {
						chargingDetailsList.add(chargingDetails);
					}
				}
				}
			}
			 }
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return null;

	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<ChargingDetails> getChargingDetailsList() {
		return chargingDetailsList;
	}

	public void setChargingDetailsList(List<ChargingDetails> chargingDetailsList) {
		this.chargingDetailsList = chargingDetailsList;
	}

}
