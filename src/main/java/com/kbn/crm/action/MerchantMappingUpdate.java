package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.google.gson.Gson;
import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.dao.NotificationDao;
import com.kbn.commons.dao.PendingMappingRequestDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.Messages;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.PendingMappingRequest;
import com.kbn.commons.user.ServiceTax;
import com.kbn.commons.user.Surcharge;
import com.kbn.commons.user.SurchargeDao;
import com.kbn.commons.user.SurchargeDetails;
import com.kbn.commons.user.SurchargeDetailsDao;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.NotificationStatusType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.actionBeans.UserMappingEditor;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.NotificationDetail;

/**
 * @author Puneet
 *
 */
public class MerchantMappingUpdate extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(MerchantMappingUpdate.class.getName());
	private static final long serialVersionUID = -9103516274778187455L;

	private String merchantEmailId;
	private String mapString;
	private String acquirer;
	private String response;
	private String accountCurrencySet;
	private String userType;
	private String emailId;

	public String execute() {

		try {
			
			StringBuilder permissions = new StringBuilder();
			permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));
			
			if (userType.equals(UserType.ADMIN.toString())
					|| permissions.toString().contains("Create Merchant Mapping")) {
				
				PendingMappingRequest existingPMR = new PendingMappingRequestDao().findPendingMappingRequest(merchantEmailId, acquirer);
				PendingMappingRequest existingActivePMR = new PendingMappingRequestDao().findActiveMappingRequest(merchantEmailId, acquirer);
				
				if (existingPMR != null){
					updateMapping(existingPMR,TDRStatus.CANCELLED,"",emailId);
				}
				if (existingActivePMR != null){
					updateMapping(existingActivePMR,TDRStatus.INACTIVE,"",emailId);
				}
				
				createNewMappingEntry(TDRStatus.ACTIVE, emailId, emailId);
				
				if (mapString != null && merchantEmailId != null && acquirer != null) {
					processMapString();
					updateSurchargeMapping();
					setResponse(ErrorType.MAPPING_SAVED.getResponseMessage());
					return SUCCESS;
				}
			}
			else{
				
				PendingMappingRequest existingPMR = new PendingMappingRequestDao().findPendingMappingRequest(merchantEmailId, acquirer);
				if (existingPMR != null){
					
					setResponse(ErrorType.MAPPING_REQUEST_ALREADY_PENDING.getResponseMessage());
					return SUCCESS;
				}
				
				createNewMappingEntry(TDRStatus.PENDING, emailId, "");
				setResponse(ErrorType.MAPPING_SAVED_FOR_APPROVAL.getResponseMessage());
				return SUCCESS;
				
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			setResponse(ErrorType.MAPPING_NOT_SAVED.getResponseMessage());
			return SUCCESS;
		}
		setResponse(ErrorType.MAPPING_NOT_SAVED.getResponseMessage());
		return SUCCESS;
	}
	
	
	public void updateMapping(PendingMappingRequest pmr, TDRStatus status , String requestedBy , String processedBy) {

		Date currentDate = new Date();
		try {

			Session session = null;
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			Long id = pmr.getId();
			session.load(pmr, pmr.getId());
			PendingMappingRequest pendingRequest = (PendingMappingRequest) session.get(PendingMappingRequest.class, id);
			pendingRequest.setStatus(status);
			pendingRequest.setUpdatedDate(currentDate);
			if(!requestedBy.equalsIgnoreCase("")){
				pendingRequest.setRequestedBy(requestedBy);
			}
			if (!processedBy.equalsIgnoreCase("")){
				pendingRequest.setProcessedBy(processedBy);
			}
			
			session.update(pendingRequest);
			tx.commit();
			session.close();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {

		}

	}
	
	public void updateSurchargeMapping(){
		
		String payId = new UserDao().getPayIdByEmailId(merchantEmailId);
		String acquirerName = AcquirerType.getInstancefromCode(acquirer).getName();
		
		List<Surcharge> activeSurchargeList = new ArrayList<Surcharge>();
		List<SurchargeDetails> activeSurchargeDetailsList = new ArrayList<SurchargeDetails>();
		Set<String> paymentTypeSet = new HashSet<String>();
		Map<PaymentType,MopType> paymentTypeMopTypeMap = new HashMap<PaymentType,MopType>();
		
		activeSurchargeList = new SurchargeDao().findActiveSurchargeListByPayIdAcquirer(payId , acquirerName);
		activeSurchargeDetailsList = new SurchargeDetailsDao().getActiveSurchargeDetailsByPayId(payId);
		
		List<String> mapStringlist = new ArrayList<String>(Arrays.asList(mapString.split(",")));
		for (String mapStrings : mapStringlist){
			String[] tokens = mapStrings.split("-");
			if (tokens[0].equalsIgnoreCase("Credit Card") || tokens[0].equalsIgnoreCase("Debit Card")){
				PaymentType key = PaymentType.getInstanceIgnoreCase(tokens[0]);
				MopType value = MopType.getmop(tokens[1]);
				paymentTypeMopTypeMap.put(key, value);
			}
			else if (tokens[0].equalsIgnoreCase("Net Banking")){
				PaymentType key = PaymentType.getInstanceIgnoreCase(tokens[0]);
				MopType value = MopType.getmop(tokens[1]);
				paymentTypeMopTypeMap.put(key, value);
			}
			
		}
		
		for(Surcharge surcharge: activeSurchargeList){
			
			
			boolean isMatch =false;
			
			for (Map.Entry<PaymentType,MopType> entry: paymentTypeMopTypeMap.entrySet()){
				if (surcharge.getPaymentType().equals(entry.getKey()) && surcharge.getMopType().equals(entry.getValue())){
					isMatch = true;
				}
			}
			
			if (isMatch){
				continue;
			}
			else{
				deactivateSurcharge(surcharge);
			}
			
		}
		
		for (String  mapStrings : mapStringlist){
			String[] tokens = mapStrings.split("-");
			paymentTypeSet.add(tokens[0]);
		}
		
		for (SurchargeDetails sd : activeSurchargeDetailsList){
			
			boolean isMatch =false;
			
			for (String paymentType : paymentTypeSet){
				if (sd.getPaymentType().equalsIgnoreCase(paymentType)){
					isMatch = true;
				}
			}
			
			if (isMatch){
				continue;
			}
			else{
				deactivateSurchargeDetails(sd);
			}
		}
		
		
		
		System.out.println(paymentTypeSet);
	}
	
	public void deactivateSurchargeDetails(SurchargeDetails sd){
		
		Date currentDate = new Date();
		try {

			Session session = null;
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			Long id = sd.getId();
			session.load(sd, sd.getId());
			SurchargeDetails surchargeDetails = (SurchargeDetails) session.get(SurchargeDetails.class, id);
			surchargeDetails.setStatus(TDRStatus.INACTIVE);
			surchargeDetails.setUpdatedDate(currentDate);
			surchargeDetails.setProcessedBy(emailId);
			
			session.update(surchargeDetails);
			tx.commit();
			session.close();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {

		}
		
	}


public void deactivateSurcharge(Surcharge sch){
		
		Date currentDate = new Date();
		try {

			Session session = null;
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			Long id = sch.getId();
			session.load(sch, sch.getId());
			Surcharge surcharge= (Surcharge) session.get(Surcharge.class, id);
			surcharge.setStatus(TDRStatus.INACTIVE);
			surcharge.setUpdatedDate(currentDate);
			surcharge.setProcessedBy(emailId);
			
			session.update(surcharge);
			tx.commit();
			session.close();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {

		}
		
	}

	public void createNewMappingEntry(TDRStatus status , String requestedBy , String processedBy){
		
		PendingMappingRequest pmr = new PendingMappingRequest();
		
		Date date = new Date();
		pmr.setMerchantEmailId(merchantEmailId);
		pmr.setMapString(mapString);
		pmr.setAcquirer(acquirer);
		pmr.setAccountCurrencySet(accountCurrencySet);
		pmr.setCreatedDate(date);
		pmr.setUpdatedDate(date);
		pmr.setStatus(status);
		
		if (!requestedBy.equalsIgnoreCase("")){
			pmr.setRequestedBy(requestedBy);
		}
		
		if(!processedBy.equalsIgnoreCase("")){
			pmr.setProcessedBy(processedBy);
		}
		
		new PendingMappingRequestDao().create(pmr);
		
		NotificationDetail notificationDetail = new NotificationDetail();
		notificationDetail.setCreateDate(date);
		notificationDetail.setSubmittedBy(requestedBy);
		notificationDetail.setConcernedUser(merchantEmailId);
		notificationDetail.setStatus(NotificationStatusType.UNREAD.getName());
		notificationDetail.setMessage(Messages.MERCHANT_MAPPING_MESSAGE.getResponseMessage());
		NotificationDao notificationDao = new NotificationDao();
		notificationDao.create(notificationDetail);
	}
	private void processMapString() {
		UserMappingEditor userMappingEditor = new UserMappingEditor();
		Gson gson = new Gson();
		AccountCurrency[] accountCurrencies = gson.fromJson(accountCurrencySet, AccountCurrency[].class);
		userMappingEditor.decideAccountChange(getMerchantEmailId(), getMapString(), getAcquirer(), accountCurrencies);
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();
		Gson gson = new Gson();
		AccountCurrency[] accountCurrencies = gson.fromJson(accountCurrencySet, AccountCurrency[].class);
		if ((validator.validateBlankField(getAcquirer()))) {
		} else if (!validator.validateField(CrmFieldType.ACQUIRER, getAcquirer())) {
			addFieldError(CrmFieldType.ACQUIRER.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if ((validator.validateBlankField(getMapString()))) {
		} else if (!validator.validateField(CrmFieldType.MAP_STRING, getMapString())) {
			addFieldError(CrmFieldType.MAP_STRING.getName(), ErrorType.INVALID_FIELD.getResponseMessage());

		}
		if ((validator.validateBlankField(getMerchantEmailId()))) {
		} else if (!validator.validateField(CrmFieldType.MERCHANT_EMAILID, getMerchantEmailId())) {
			addFieldError(CrmFieldType.MERCHANT_EMAILID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());

		}
		if ((validator.validateBlankField(getResponse()))) {
		} else if (!validator.validateField(CrmFieldType.RESPONSE, getResponse())) {
			addFieldError(CrmFieldType.RESPONSE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());

		}
		// AccountCurrency Class validation
		for (AccountCurrency accountCurrencyFE : accountCurrencies) {
			if ((validator.validateBlankField(accountCurrencyFE.getAcqPayId()))) {
			} else if (!(validator.validateField(CrmFieldType.ACQ_PAYID, accountCurrencyFE.getAcqPayId()))) {
				addFieldError(CrmFieldType.ACQ_PAYID.getName(), validator.getResonseObject().getResponseMessage());
			}
			if ((validator.validateBlankField(accountCurrencyFE.getMerchantId()))) {
			} else if (!(validator.validateField(CrmFieldType.MERCHANTID, accountCurrencyFE.getMerchantId()))) {
				addFieldError(CrmFieldType.MERCHANTID.getName(), validator.getResonseObject().getResponseMessage());
			}
			if ((validator.validateBlankField(accountCurrencyFE.getCurrencyCode()))) {
			} else if (!(validator.validateField(CrmFieldType.CURRENCY, accountCurrencyFE.getCurrencyCode()))) {
				addFieldError(CrmFieldType.CURRENCY.getName(), validator.getResonseObject().getResponseMessage());
			}
			if ((validator.validateBlankField(accountCurrencyFE.getPassword()))) {
			} else if (!(validator.validateField(CrmFieldType.ACCOUNT_PASSWORD, accountCurrencyFE.getPassword()))) {
				addFieldError(CrmFieldType.ACCOUNT_PASSWORD.getName(),
						validator.getResonseObject().getResponseMessage());
			}
			if ((validator.validateBlankField(accountCurrencyFE.getTxnKey()))) {
			} else if (!(validator.validateField(CrmFieldType.TXN_KEY, accountCurrencyFE.getTxnKey()))) {
				addFieldError(CrmFieldType.TXN_KEY.getName(), validator.getResonseObject().getResponseMessage());
			}
		}
	}

	public String getMapString() {
		return mapString;
	}

	public void setMapString(String mapString) {
		this.mapString = mapString;
	}

	public String display() {
		return NONE;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	public String getMerchantEmailId() {
		return merchantEmailId;
	}

	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getAccountCurrencySet() {
		return accountCurrencySet;
	}

	public void setAccountCurrencySet(String accountCurrencySet) {
		this.accountCurrencySet = accountCurrencySet;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
}
