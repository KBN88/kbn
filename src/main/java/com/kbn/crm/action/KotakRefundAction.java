package com.kbn.crm.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.netbanking.kotak.BatchFileCreator;
import com.kbn.netbanking.kotak.RefundFieldsCreator;

public class KotakRefundAction extends AbstractSecureAction {

	private static final long serialVersionUID = -6916806304042310081L;
	private InputStream fileInputStream;
	private String fileName;
	private String dateFrom;
	private String dateTo;
	

	public String execute() {
		RefundFieldsCreator rfc = new RefundFieldsCreator();
		try {

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYYY" + "000001");
			fileName = sdf.format(date) + ".txt";

			List<TransactionSummaryReport> refundList = rfc
					.getTransactionRefundReport(dateFrom, dateTo);
			if(refundList.isEmpty()){
				addActionMessage("No Transaction for Refund");
				return INPUT;
			}
			
			BatchFileCreator bfc = new BatchFileCreator();
			String content = bfc.createFile(refundList);

			File file = new File("file.txt");

			FileUtils.writeStringToFile(file, content);

			fileInputStream = new FileInputStream(file);
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
}
