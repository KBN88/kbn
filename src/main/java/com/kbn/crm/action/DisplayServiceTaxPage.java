package com.kbn.crm.action;

import java.util.LinkedHashMap;
import java.util.Map;

import com.kbn.commons.util.BusinessType;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Rahul
 *
 */
public class DisplayServiceTaxPage extends AbstractSecureAction{

	private static final long serialVersionUID = -3054395368649186158L;
	private Map<String,String> industryCategory = new LinkedHashMap<String,String>();
	public String execute(){
		setIndustryCategory(BusinessType.getIndustryCategoryList());
		return INPUT;
		
	}
	public Map<String,String> getIndustryCategory() {
		return industryCategory;
	}
	public void setIndustryCategory(Map<String,String> industryCategory) {
		this.industryCategory = industryCategory;
	}

}
