package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.TransactionType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.Currency;

/**
 * @author Puneet
 *
 */
public class AcquirerRouterRedirect extends AbstractSecureAction{

	private static final long serialVersionUID = -3118366599992623506L;
	public List<TransactionType> transList = new ArrayList<TransactionType>();
	private Map<String, String> currencyMap = Currency.getAllCurrency();
	public List<Merchants> listMerchant = new ArrayList<Merchants>();

	@SuppressWarnings("unchecked")
	public String execute(){
		UserDao userDao = new UserDao();
		setListMerchant(userDao.getMerchantList());	
		transList.add(TransactionType.AUTHORISE);
		transList.add(TransactionType.SALE);		
		return SUCCESS;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}
	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

	public List<Merchants> getListMerchant() {
		return listMerchant;
	}

	public void setListMerchant(List<Merchants> listMerchant) {
		this.listMerchant = listMerchant;
	}
	
	
}
