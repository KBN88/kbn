package com.kbn.crm.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionReportService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.Currency;

/**
 * @author Isha
 */
public class RemittanceAmountCall extends AbstractSecureAction {
	private String dateFrom;
	private String payId;
	private String netAmount;
	private String currency;

	private static Logger logger = Logger
			.getLogger(TransactionSummaryReportAction.class.getName());
	private static final long serialVersionUID = -4041744354667387858L;
	private List<TransactionSummaryReport> aaData = new ArrayList<TransactionSummaryReport>();
	private User sessionUser = new User();

	public String execute() {
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		TransactionReportService reportGen = new TransactionReportService();
		DataEncoder encoder = new DataEncoder();
		try {
			setAaData(encoder
					.encodeTransactionSummary(reportGen.getRemittanceTransactionList(
							getDateFrom(), getDateFrom(), getPayId(), "ALL",
							"ALL", Currency.getNumericCode(getCurrency()),
							sessionUser)));
			setNetAmount(ListUtil().toString());

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if (validator.validateBlankField(getDateFrom())) {
		} else if (!validator.validateField(CrmFieldType.DATE_FROM,
				getDateFrom())) {
			addFieldError(CrmFieldType.DATE_FROM.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getPayId())) {
		} else if (!validator.validateField(CrmFieldType.PAY_ID, getPayId())) {
			addFieldError(CrmFieldType.PAY_ID.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getCurrency())) {
		} else if (!validator.validateField(CrmFieldType.CURRENCY,
				getCurrency())) {
			addFieldError(CrmFieldType.CURRENCY.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		// TODO... remove hard coding
		if (validator.validateBlankField(getNetAmount())) {
		} else if (!validator
				.validateField(CrmFieldType.AMOUNT, getNetAmount())) {
			addFieldError("netAmount",
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	private String ListUtil() {
		BigDecimal sum = BigDecimal.valueOf(0.0);

		for (TransactionSummaryReport summary : aaData) {
			sum = sum.add(BigDecimal.valueOf(Double.parseDouble(summary
					.getNetAmount())));
		}
		return sum.toString();
	}

	public List<TransactionSummaryReport> getAaData() {
		return aaData;
	}

	public void setAaData(List<TransactionSummaryReport> aaData) {
		this.aaData = aaData;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
