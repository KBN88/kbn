package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.ChargingDetailsDao;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class UpdateServiceTaxAction extends AbstractSecureAction{

	/**
	 * Neeraj
	 */
	private static final long serialVersionUID = -228182371499688572L;
	private static Logger logger=Logger.getLogger(UpdateServiceTaxAction.class.getName());
	private ChargingDetails chargingDetails = new ChargingDetails();
	private String merchantServiceTax;
	
    private String response;
	public String execute() {
		try{
			ChargingDetailsDao chargingDetailsDao=new ChargingDetailsDao();
			//chargingDetailsDao.updateServiceTax(merchantServiceTax);
		}catch(Exception exception){
			setResponse(ErrorType.CHARGINGDETAIL_NOT_SAVED.getResponseMessage());
			logger.error("Exception", exception);
		}
		
		return SUCCESS;
		
	}
	public ChargingDetails getChargingDetails() {
		return chargingDetails;
	}
	public void setChargingDetails(ChargingDetails chargingDetails) {
		this.chargingDetails = chargingDetails;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getMerchantServiceTax() {
		return merchantServiceTax;
	}
	public void setMerchantServiceTax(String merchantServiceTax) {
		this.merchantServiceTax = merchantServiceTax;
	}
	
}