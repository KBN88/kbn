package com.kbn.crm.action;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserRecordsDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.TransactionManager;
import com.kbn.commons.util.UserStatusType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailBuilder;


/**
 * @author Neeraj
 */
public class ForgetPasswordAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(ForgetPasswordAction.class.getName());
	private static final long serialVersionUID = 4184065113906121002L;

	private String emailId;
	private String id;
	private String payId;
	private String newPassword;
	private String confirmNewPassword;
	private String response;
	private String errorMessage;
	private String errorCode;
	
	private User user = new User();
	
	@SkipValidation
	public String execute() {
		try {
			if(!validateEmail()) {
				UserDao userDao = new UserDao();
				String accountValidationKey = TransactionManager.getNewTransactionId();
				user = userDao.findPayIdByEmail(getEmailId());
				if (user != null){
					if(!user.getUserStatus().equals(UserStatusType.ACTIVE.toString())) {
						String payId =  user.getPayId();
						if(!StringUtils.isEmpty(payId)){
							// Sending Email for Email Validation
							EmailBuilder emailBuilder = new EmailBuilder();
							emailBuilder.passwordResetEmail(accountValidationKey,getEmailId());
							userDao.updateAccountValidationKey(accountValidationKey, payId);
							setResponse(ErrorType.RESET_LINK_SENT.getResponseMessage());
						}
						else
						{
							setResponse(ErrorType.INVALID_EMAIL.getResponseMessage());
							setErrorMessage(ErrorType.INVALID_EMAIL.getResponseMessage());
						}
					}
					else
					{
						setResponse(ErrorType.INVALID_EMAIL.getResponseMessage());
						setErrorMessage(ErrorType.INVALID_EMAIL.getResponseMessage());
					}
				}
				else
				{
					setResponse(ErrorType.INVALID_EMAIL.getResponseMessage());
					setErrorMessage(ErrorType.INVALID_EMAIL.getResponseMessage());
				}
			}
			else {
				setResponse(getErrorMessage());
			}				
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return SUCCESS;
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String resetPassword()
	{
		try{
			UserDao userDao = new UserDao();
			user = userDao.findByAccountValidationKey(getId());
			
			if(user ==  null){
				return ERROR;
			}
			if(user.isEmailValidationFlag()) {
				addActionMessage(ErrorType.ALREADY_PASSWORD_RESET.getResponseMessage());
				return "reset";
			}
			setPayId(user.getPayId());
			return SUCCESS;
		}
		catch(Exception exception){
			logger.error("Exception", exception);
			return ERROR;
		}
	}
	
	public String resetUserPassword()
	{
		UserDao userDao = new UserDao();
		UserRecordsDao userRecordsDao = new UserRecordsDao();
		user = userDao.findPayId(payId);
		try{
			if(!validateFields()) {			
				if(user ==  null){
					return ERROR;
				}
				userRecordsDao.createDetails(user.getEmailId(), user.getPassword(), user.getPayId());
				String salt = (new PropertiesManager()).getSalt(user.getPayId());
				if (null == salt) {
					throw new SystemException(ErrorType.AUTHENTICATION_UNAVAILABLE,
							ErrorType.AUTHENTICATION_UNAVAILABLE.getResponseCode());
				}
				String hashedPassword = Hasher.getHash(getNewPassword().concat(salt));
				user.setPassword(hashedPassword);
				user.setEmailValidationFlag(true);
				if(user.getUserType().equals(UserType.SUBUSER) ||user.getUserType().equals(UserType.SUBACQUIRER)) {
					user.setUserStatus(UserStatusType.ACTIVE);
				}
				userDao.update(user);
				setResponse(ErrorType.PASSWORD_RESET.getResponseMessage());
				setErrorCode(ErrorType.PASSWORD_RESET.getCode());
			}
			else {
				setResponse(getErrorMessage());
			}
				
			return SUCCESS;
		}
		catch(Exception exception){
			logger.error("Exception", exception);
			return SUCCESS;
		}
	}
	
	public boolean validateEmail() {
		CrmValidator validator = new CrmValidator();

		//Validate blank and invalid fields
		if (validator.validateBlankField(getEmailId())) {
			setErrorMessage(validator.getResonseObject().getResponseMessage());
			return true;
		} else if (!(validator.isValidEmailId(getEmailId()))) {
			setErrorMessage(ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
			return true;
		}
		return false;
	}
	
	
	public boolean validateFields() throws SystemException{
		CrmValidator validator = new CrmValidator();
		
		//check fields have valid and match confirm password with new password
		if(validator.validateBlankField(getNewPassword())){
			setErrorMessage(ErrorType.NEW_PASSWORD.getResponseMessage());
			return true;	 									   
		}else if(validator.validateBlankField(getConfirmNewPassword())){
			setErrorMessage(ErrorType.CONFIRM_NEW_PASSWORD.getResponseMessage());
			return true;
	 	}else if (!(getNewPassword().equals (getConfirmNewPassword()))){
	 		setErrorMessage(ErrorType.PASSWORD_MISMATCH.getResponseMessage());
	 		return true;			
		}else if(!(validator.isValidPasword(getNewPassword()))){
			setErrorMessage(ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
			return true;
		}	
		if(newPassword.equals(user.getPassword())){
			setErrorMessage(ErrorType.OLD_PASSWORD_MATCH.getResponseMessage());
			return true;
		}
		if(new CheckOldPassword().isUsedPassword(newPassword, user.getEmailId())) {
			setErrorMessage(ErrorType.OLD_PASSWORD_MATCH.getResponseMessage());
			return true;
		}
			
		return false;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmNewPassword() {
		return confirmNewPassword;
	}

	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}	
}
