package com.kbn.crm.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.dao.NotificationDao;
import com.kbn.commons.dao.ServiceTaxDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.Messages;
import com.kbn.commons.user.ServiceTax;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.NotificationGenerator;
import com.kbn.commons.util.NotificationStatusType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.NotificationDetail;

/**
 * @author Shaiwal
 *
 */
public class ServiceTaxEditAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(ServiceTaxEditAction.class.getName());
	private static final long serialVersionUID = -6517340843571949786L;

	private Long id;
	private String businessType;
	private String status;
	private BigDecimal serviceTax;
	private String response;
	private String userType;
	private String loginEmailId;
	public Date currentDate = new Date();

	public String execute() {
		Session session = null;

		ServiceTaxDao serviceTaxDao = new ServiceTaxDao();

		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

		ServiceTax findPendingRequest = new ServiceTax();
		findPendingRequest = serviceTaxDao.findPendingRequest(businessType);

		if (userType.equals(UserType.ADMIN.toString()) || permissions.toString().contains("Create Service Tax")) {

			// Cancel Pending Request
			if (findPendingRequest != null) {
				try {

					session = HibernateSessionProvider.getSession();
					Transaction tx = session.beginTransaction();
					id = findPendingRequest.getId();
					session.load(findPendingRequest, findPendingRequest.getId());
					ServiceTax serviceTaxDetails = (ServiceTax) session.get(ServiceTax.class, id);

					serviceTaxDetails.setStatus(TDRStatus.CANCELLED);
					serviceTaxDetails.setUpdatedDate(currentDate);
					serviceTaxDetails.setProcessedBy(loginEmailId);
					session.update(serviceTaxDetails);
					tx.commit();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				}

			}

			ServiceTax activeServiceTax = new ServiceTax();
			activeServiceTax = serviceTaxDao.findServiceTax(businessType);

			// Deactivate active service tax
			if (activeServiceTax != null) {
				try {
					
					session = HibernateSessionProvider.getSession();
					Transaction txn = session.beginTransaction();
					id = activeServiceTax.getId();
					session.load(activeServiceTax, activeServiceTax.getId());
					ServiceTax serviceTaxDetail = (ServiceTax) session.get(ServiceTax.class, id);
					if (userType.equals(UserType.ADMIN.toString())
							|| permissions.toString().contains("Create Service Tax")) {
						serviceTaxDetail.setStatus(TDRStatus.INACTIVE);
						serviceTaxDetail.setUpdatedDate(currentDate);
						serviceTaxDetail.setProcessedBy(loginEmailId);
					}
					session.update(serviceTaxDetail);
					txn.commit();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				}
			}

			try {

				createNewServiceTax(businessType, serviceTax, userType);
			} catch (HibernateException e) {
				e.printStackTrace();
			}

			setResponse(ErrorType.MAPPING_SAVED.getResponseMessage());
			return SUCCESS;
		}

		else {
			if (findPendingRequest != null) {

				setResponse(ErrorType.SERVICE_TAX_REQUEST_ALREADY_PENDING.getResponseMessage());
				return SUCCESS;
			}

			else {
				createNewServiceTax(businessType, serviceTax, userType);
				setResponse(ErrorType.SERVICE_TAX_REQUEST_SAVED_FOR_APPROVAL.getResponseMessage());
				return SUCCESS;
			}
		}

	}

	private void createNewServiceTax(String businessType, BigDecimal serviceTax, String userType) {

		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));
		ServiceTaxDao serviceTaxDao = new ServiceTaxDao();
		ServiceTax newServiceTax = new ServiceTax();
		newServiceTax.setCreatedDate(currentDate);
		newServiceTax.setBusinessType(businessType);
		if (userType.equals(UserType.ADMIN.toString()) || permissions.toString().contains("Create Service Tax")) {
			newServiceTax.setStatus(TDRStatus.ACTIVE);
			newServiceTax.setProcessedBy(loginEmailId);
			newServiceTax.setRequestedBy(loginEmailId);
		} else {
			newServiceTax.setStatus(TDRStatus.PENDING);
			newServiceTax.setRequestedBy(loginEmailId);
			
			NotificationGenerator notificationGenerator = new NotificationGenerator();
			notificationGenerator.UpdateRequestNotification("CREATE_SERVICE_TAX",loginEmailId, Messages.PENDING_SERVICE_TAX.getResponseMessage());
		}
		newServiceTax.setUpdatedDate(currentDate);
		newServiceTax.setServiceTax(serviceTax);

		serviceTaxDao.create(newServiceTax);

	}

	public BigDecimal getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(BigDecimal serviceTax) {
		this.serviceTax = serviceTax;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getLoginEmailId() {
		return loginEmailId;
	}

	public void setLoginEmailId(String loginEmailId) {
		this.loginEmailId = loginEmailId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
