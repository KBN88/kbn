package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import com.kbn.commons.user.Settlement;
import com.kbn.commons.user.SettlementDao;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.commons.util.DateCreater;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class SettlementReportAction extends AbstractSecureAction {

	private static final long serialVersionUID = 6450352382366075281L;

	private static Logger logger = Logger.getLogger(SettlementReportAction.class.getName());
	private List<Settlement> aaData = new ArrayList<Settlement>();
	private String payId;
	private String dateFrom;
	private String dateTo;
	private String currency;
	private String paymentType;

	public String execute() {
		User sessionUser = new User();
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder = new DataEncoder();
		SettlementDao settlementDao = new SettlementDao();
		SessionUserIdentifier sessionUserIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = sessionUserIdentifier.getMerchantPayId(sessionUser, payId);
			setAaData(encoder.encodeSettlementSearchObj(settlementDao
					.findDetail(getDateFrom(), getDateTo(), merchantPayId,
							getCurrency(), getPaymentType())));
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if ((validator.validateBlankField(getPayId()))
				|| getPayId().equals(CrmFieldConstants.ALL.getValue())) {
		} else if (!(validator.validateField(CrmFieldType.PAY_ID, getPayId()))) {
			addFieldError(CrmFieldType.PAY_ID.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(getDateFrom()))) {
			addFieldError(CrmFieldType.DATE_FROM.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.DATE_FROM,
				getDateFrom()))) {
			addFieldError(CrmFieldType.DATE_FROM.getName(), validator
					.getResonseObject().getResponseMessage());
		}

		if (!validator.validateBlankField(getDateTo())) {
			if (DateCreater.formatStringToDate(
					DateCreater.formatFromDate(getDateFrom())).compareTo(
					DateCreater.formatStringToDate(DateCreater
							.formatFromDate(getDateTo()))) > 0) {
				addFieldError(CrmFieldType.DATE_FROM.getName(),
						CrmFieldConstants.FROMTO_DATE_VALIDATION.getValue());
			} else if (DateCreater.diffDate(getDateFrom(), getDateTo()) > 31) {
				addFieldError(CrmFieldType.DATE_FROM.getName(),
						CrmFieldConstants.DATE_RANGE.getValue());
			}
		}

		if ((validator.validateBlankField(getDateTo()))) {
			addFieldError(CrmFieldType.DATE_TO.getName(), validator
					.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.DATE_TO, getDateTo()))) {
			addFieldError(CrmFieldType.DATE_TO.getName(), validator
					.getResonseObject().getResponseMessage());
		}
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public List<Settlement> getAaData() {
		return aaData;
	}

	public void setAaData(List<Settlement> aaData) {
		this.aaData = aaData;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
}
