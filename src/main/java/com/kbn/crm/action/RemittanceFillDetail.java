package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.Currency;

/**
 * @author Isha
 *
 */
public class RemittanceFillDetail extends AbstractSecureAction {
	private static Logger logger = Logger
			.getLogger(RemittanceFillDetail.class.getName());
	private static final long serialVersionUID = 7987400846181942380L;
	
	public List<Merchants> listMerchant = new ArrayList<Merchants>();
	private String bankName;
	private String accHolderName;
	private String accountNo;
	private String ifscCode;
	private String payId;
	private List<String> curr = new ArrayList<String>();

	@SuppressWarnings("unchecked")
	public String execute() {
		UserDao userDao = new UserDao();
		Map<String, String> currencyMap = new HashMap<String, String>();
		setListMerchant(userDao.getActiveMerchantList());
		try {
			User user = new User();
			user = userDao.findPayId(getPayId());
			setBankName(user.getBankName());
			setAccHolderName(user.getAccHolderName());
			setAccountNo(user.getAccountNo());
			setIfscCode(user.getIfscCode());
			currencyMap = Currency.getSupportedCurreny(user);

			for (String key : currencyMap.keySet()) {
				curr.add(currencyMap.get(key));
			}

			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if (validator.validateBlankField(getPayId())) {
		} else if (!validator.validateField(CrmFieldType.PAY_ID, getPayId())) {
			addFieldError(CrmFieldType.PAY_ID.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if (validator.validateBlankField(getBankName())) {
		} else if (!validator.validateField(CrmFieldType.BANK_NAME, getBankName())) {
			addFieldError(CrmFieldType.BANK_NAME.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if (validator.validateBlankField(getAccHolderName())) {
		} else if (!validator.validateField(CrmFieldType.ACC_HOLDER_NAME, getAccHolderName())) {
			addFieldError(CrmFieldType.ACC_HOLDER_NAME.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if (validator.validateBlankField(getAccountNo())) {
		} else if (!validator.validateField(CrmFieldType.ACCOUNT_NO, getAccountNo())) {
			addFieldError(CrmFieldType.ACCOUNT_NO.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if (validator.validateBlankField(getIfscCode())) {
		} else if (!validator.validateField(CrmFieldType.IFSC_CODE, getIfscCode())) {
			addFieldError(CrmFieldType.IFSC_CODE.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
    }

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public List<Merchants> getListMerchant() {
		return listMerchant;
	}

	public void setListMerchant(List<Merchants> listMerchant) {
		this.listMerchant = listMerchant;
	}

	public String getAccHolderName() {
		return accHolderName;
	}

	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public List<String> getCurr() {
		return curr;
	}

	public void setCurr(List<String> curr) {
		this.curr = curr;
	}
}
