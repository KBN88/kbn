package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.StatisticsService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Statistics;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.commons.action.AbstractSecureAction;
/**
 * @author Chandan
 */
public class StatisticsAction extends AbstractSecureAction{
	
	private static Logger logger = Logger.getLogger(StatisticsAction.class.getName());
	private static final long serialVersionUID = 841437520227833172L;
	private StatisticsService  getStatistics = new StatisticsService();
	private Statistics statistics = new Statistics();
	private String emailId;
	private String currency;
	private String dateFrom;
	private String dateTo;

	public String execute() {
		try {
			UserDao userDao =  new UserDao();
			User user = (User) sessionMap.get(Constants.USER.getValue());
			if (user.getUserType().equals(UserType.RESELLER)) {
				setStatistics(getStatistics.getDashboardResellerValues(user.getPayId(),user.getResellerId(), getCurrency()));
				if(getEmailId().equals(CrmFieldConstants.ALL_MERCHANTS.getValue())) {
					setStatistics(getStatistics.getDashboardResellerValues(getEmailId(), user.getResellerId(),getCurrency()));
				}
				else {
					setStatistics(getStatistics.getDashboardResellerValues(userDao.findPayIdByEmail(getEmailId()).getPayId(),user.getResellerId(),getCurrency()));
				}
			return SUCCESS;	
				
			}else{
			
			if (user.getUserType().equals(UserType.MERCHANT) || user.getUserType().equals(UserType.POSMERCHANT)) {
			setStatistics(getStatistics.getDashboardValues(user.getPayId(), getCurrency(),getDateFrom(),getDateTo()));
			}
			else
			{
				if(getEmailId().equals(CrmFieldConstants.ALL_MERCHANTS.getValue())) {
					setStatistics(getStatistics.getDashboardValues(getEmailId(), getCurrency(),getDateFrom(),getDateTo()));
				}
				else {
				setStatistics(getStatistics.getDashboardValues(userDao.findPayIdByEmail(getEmailId()).getPayId(),getCurrency() ,getDateFrom(),getDateTo()));
				}
			}
			return SUCCESS;	
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public void validate(){
		CrmValidator validator = new CrmValidator();

		if(validator.validateBlankField(getEmailId())){
		}else if(getEmailId().equals(CrmFieldConstants.ALL_MERCHANTS.getValue())){			
		}
        else if(!validator.validateField(CrmFieldType.EMAILID, getEmailId())){
        	addFieldError(CrmFieldType. EMAILID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if(validator.validateBlankField(getCurrency())){
		}
        else if(!validator.validateField(CrmFieldType.CURRENCY, getCurrency())){
        	addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}
	
	public Statistics getStatistics() {
		return statistics;
	}
	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
}
