package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Settlement;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.PaymentType;
import com.kbn.crm.actionBeans.SettlementProcessor;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Rahul
 *
 */
public class SettlementProcessAction extends AbstractSecureAction {

	private static final long serialVersionUID = -7185217746078300385L;
	private static Logger logger = Logger.getLogger(SettlementProcessAction.class
			.getName());

	private String response;
	private String origTxnId;
	private String amount;
	private String payId;
	private String orderId;
	private String currencyCode;
	private String mopType;
	private String txnDate;
	private String paymentMethod;
	private String tdr;
	private String serviceTax;
	private String netAmount;
	private String cust_email;
	private String merchantFixCharge;
	private String merchant;
	private String txnType;

	public String execute() {
		try {
			Settlement settlement = prepareObject();
			SettlementProcessor settlementProcessor = new SettlementProcessor();
			settlementProcessor.process(settlement);
			setResponse(CrmFieldConstants.PROCESS_INITIATED_SUCCESSFULLY.getValue());
		} catch (Exception exception) {
			logger.error("Error processing batch settlement:" + exception);
			setResponse(ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
		}
		return SUCCESS;
	}

	public Settlement prepareObject(){
		Settlement settlement = new Settlement();
		settlement.setTxnId(origTxnId);
		settlement.setTxnAmount(amount);
		settlement.setPayId(payId);
		settlement.setOrderId(orderId);
		settlement.setCurrencyCode(currencyCode);
		settlement.setMop(mopType);			
		settlement.setPaymentMethod(PaymentType.getInstance(paymentMethod).getName());
		settlement.setTdr(tdr);
		settlement.setMerchantFixCharge(merchantFixCharge);
		settlement.setServiceTax(serviceTax);
		settlement.setNetAmount(netAmount);
		settlement.setCustomerEmail(cust_email);
		settlement.setTxnDate(txnDate);
		settlement.setMerchant(merchant);
		settlement.setTxnType(txnType);
		return settlement;
	}
	
	public void validate() {
		CrmValidator validator = new CrmValidator();

		if ((validator.validateBlankField(getPayId())) || getPayId().equals(CrmFieldConstants.ALL.getValue())) {
		}else if (!(validator.validateField(CrmFieldType.PAY_ID, getPayId()))) {
			addFieldError(CrmFieldType.PAY_ID.getName(), validator.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(txnDate))) {
			addFieldError(CrmFieldType.DATE_FROM.getName(), validator.getResonseObject().getResponseMessage());
		}else if (!(validator.validateField(CrmFieldType.DATE_FROM,	txnDate))) {
			addFieldError(CrmFieldType.DATE_FROM.getName(), validator.getResonseObject().getResponseMessage());
		}

	   if(validator.validateBlankField(getCurrencyCode())){
	   }else if(!validator.validateField(CrmFieldType.CURRENCY, getCurrencyCode())){
	      	addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
	   }

	   if(validator.validateBlankField(getPaymentMethod())){
	   }else if(!validator.validateField(CrmFieldType.PAYMENT_TYPE, getPaymentMethod())){
       	addFieldError(CrmFieldType.PAYMENT_TYPE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
	   }

	   if(validator.validateBlankField(getCust_email()) || getCust_email().equals(CrmFieldConstants.ALL.getValue()) || getCust_email().equals(CrmFieldConstants.NOT_AVAILABLE.getValue())){
	   }else if(!validator.validateField(CrmFieldType.MERCHANT_EMAIL_ID, getCust_email())){
       	addFieldError(CrmFieldType. MERCHANT_EMAIL_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
	   }

	   if(validator.validateBlankField(getOrderId())){
	   }else if(!validator.validateField(CrmFieldType.ORDER_ID, getOrderId())){
			addFieldError(CrmFieldType.ORDER_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
	   }

	   if(validator.validateBlankField(getAmount())){
	   }else if(!validator.validateField(CrmFieldType.AMOUNT, getAmount())){
			addFieldError(CrmFieldType.AMOUNT.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
	   }

	   if(validator.validateBlankField(getNetAmount())){
	   }else if(!validator.validateNegativeAmount(getNetAmount())){
			addFieldError(CrmFieldType.NET_AMOUNT.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

	   if(validator.validateBlankField(getOrigTxnId())){
	   }else if(!validator.validateField(CrmFieldType.TRANSACTION_ID, getOrigTxnId())){
			addFieldError(CrmFieldType.TRANSACTION_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public String getOrigTxnId() {
		return origTxnId;
	}

	public void setOrigTxnId(String origTxnId) {
		this.origTxnId = origTxnId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getMopType() {
		return mopType;
	}

	public void setMopType(String mopType) {
		this.mopType = mopType;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}

	public String getTdr() {
		return tdr;
	}

	public void setTdr(String tdr) {
		this.tdr = tdr;
	}

	public String getCust_email() {
		return cust_email;
	}

	public void setCust_email(String cust_email) {
		this.cust_email = cust_email;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getMerchantFixCharge() {
		return merchantFixCharge;
	}

	public void setMerchantFixCharge(String merchantFixCharge) {
		this.merchantFixCharge = merchantFixCharge;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

}
