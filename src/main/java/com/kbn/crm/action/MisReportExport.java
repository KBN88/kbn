package com.kbn.crm.action;

import java.io.IOException;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.UserType;

public class MisReportExport {
	public static void main(String[] args) throws SystemException, IOException {
		String acquirerName = "ALL";
		String merchantName = "ALL";
		String dateTimeFrom = "2017-04-19 09:00:00";
		String dateTimeTo =   "2017-04-19 11:56:00";
		MisReportExportAction misReportExport = new MisReportExportAction();
		misReportExport.exportCSV(acquirerName, merchantName, dateTimeFrom, dateTimeTo, UserType.ADMIN);
		//misReportExport.exportCitrusCSV(acquirerName, merchantName, dateTimeFrom, dateTimeTo, UserType.ADMIN);
	}

}
