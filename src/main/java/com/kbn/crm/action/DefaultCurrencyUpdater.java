package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.commons.action.AbstractSecureAction;


/**
 * @author SHASHI
 *
 */
public class DefaultCurrencyUpdater extends AbstractSecureAction {
	private static Logger logger = Logger.getLogger(DefaultCurrencyUpdater.class
			.getName());

	private static final long serialVersionUID = 7631759858298731581L;
	private String defaultCurrency;
	private String response;
	public String execute() {
		User userFromDB;
		UserDao userDao = new UserDao();
		User sessionUser;
		try {
			sessionUser = (User) sessionMap.get(Constants.USER.getValue());
				userFromDB = userDao.findPayId(sessionUser.getPayId());
				userFromDB.setDefaultCurrency(defaultCurrency);
				userDao.update(userFromDB);
				sessionMap.replace(Constants.USER.getValue(),userFromDB);
				response = CrmFieldConstants.DEFAUL_CURRENCY_UPDATE.getValue();
			return SUCCESS;
		}
		catch (Exception exception) {
			logger.error("DefaultCurrencyUpdater", exception);
			ErrorType.INVALID_DEFAULT_CURRENCY.getResponseMessage();
			return ERROR;
		}

	}
	public void validate(){
		CrmValidator validator = new CrmValidator();
		 if(!validator.validateField(CrmFieldType.DEFAULT_CURRENCY, getDefaultCurrency())){
			 addFieldError(CrmFieldType.DEFAULT_CURRENCY.getName(),ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
		}
	} 
	
	public String getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
