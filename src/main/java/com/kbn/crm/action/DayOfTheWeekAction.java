package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.WeekChartService;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.commons.action.ForwardAction;

public class DayOfTheWeekAction extends ForwardAction {

	private static final long serialVersionUID = -7869874251716549348L;

	private static Logger logger = Logger.getLogger(DayOfTheWeekAction.class
			.getName());
	
    private List<PieChart> pieChart=new ArrayList<PieChart>();
	private WeekChartService getWeekChartService=new WeekChartService();
	private String emailId;
	private String currency;
	private String dateFrom;
	private String dateTo;
	 public String execute(){	 
			try{
				UserDao userDao=new UserDao();
				User user=(User) sessionMap.get(Constants.USER.getValue());
				if(user.getUserType().equals(UserType.MERCHANT)|| user.getUserType().equals(UserType.POSMERCHANT)){
					setPieChart(getWeekChartService.preparelist(user.getPayId(),getCurrency(),getDateFrom(),getDateTo()));
				}
				else
				{
					if(getEmailId().equals(CrmFieldConstants.ALL_MERCHANTS.getValue())) {
						setPieChart(getWeekChartService.preparelist(getEmailId(), getCurrency(),getDateFrom(),getDateTo()));
					}
					else {
						setPieChart(getWeekChartService.preparelist(userDao.findPayIdByEmail(getEmailId()).getPayId(),getCurrency(),getDateFrom(),getDateTo()));
					}
				}
			 return SUCCESS;
			
		 }catch(Exception exception){
			 logger.error("Exception", exception);
				return ERROR;
		 }

}
	
	public List<PieChart> getPieChart() {
		return pieChart;
	}

	public void setPieChart(List<PieChart> pieChart) {
		this.pieChart = pieChart;
	}

	public WeekChartService getGetWeekChartService() {
		return getWeekChartService;
	}
	public void setGetWeekChartService(WeekChartService getWeekChartService) {
		this.getWeekChartService = getWeekChartService;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

}