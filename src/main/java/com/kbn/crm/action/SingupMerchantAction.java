package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.actionBeans.CreateNewUser;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailBuilder;

public class SingupMerchantAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(SingupMerchantAction.class.getName());
	private static final long serialVersionUID = 5995449017764989418L;

	private String emailId;
	private String password;
	private String businessName;
	private String mobile;
	private String confirmPassword;
	private String userRoleType;
	private String industryCategory;
	private String industrySubCategory;
	public String execute() {
		CreateNewUser createUser = new CreateNewUser();
		ResponseObject responseObject = new ResponseObject();
		
		try {
			if(userRoleType.equals(CrmFieldConstants.USER_RESELLER_TYPE.getValue())){
				responseObject = createUser.createUser(getUserInstance(),UserType.RESELLER , "");
				}else{
				responseObject = createUser.createUser(getUserInstance(),UserType.MERCHANT , "");
			     }
			if (!ErrorType.SUCCESS.getResponseCode().equals(
					responseObject.getResponseCode())) {
				addActionMessage(responseObject.getResponseMessage());
				
				}
			if(ErrorType.SUCCESS.getResponseCode().equals(responseObject.getResponseCode())){
			addActionMessage("User successfully registered.");
			}
			// Sending Email for Email Validation
			EmailBuilder emailBuilder = new EmailBuilder();
			emailBuilder.emailValidator(responseObject);
			
           return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	private User getUserInstance() {
		User user = new User();
		user.setEmailId(getEmailId());
		user.setPassword(getPassword());
		user.setMobile(getMobile());
		user.setBusinessName(getBusinessName());
		if(userRoleType.equals(CrmFieldConstants.USER_RESELLER_TYPE.getValue())){	
		}else{
			user.setIndustryCategory(industryCategory);
			user.setIndustrySubCategory(industrySubCategory);
		}
		return user;
	}

	public void validate() {

		CrmValidator validator = new CrmValidator();
		String userType = userRoleType;

		//Validate blank and invalid fields
		if (validator.validateBlankField(getPassword())) {
			addFieldError(CrmFieldType.PASSWORD.getName(), validator.getResonseObject().getResponseMessage());
			if (validator.validateBlankField(getConfirmPassword())) {
				addFieldError(CrmFieldConstants.CONFIRM_PASSWORD.getValue(), validator.getResonseObject().getResponseMessage());
			}
		} else if (validator.validateBlankField(getConfirmPassword())) {
			addFieldError(CrmFieldConstants.CONFIRM_PASSWORD.getValue(), validator.getResonseObject().getResponseMessage());
		} else if (!(getPassword().equals(getConfirmPassword()))) {
			addFieldError(CrmFieldType.PASSWORD.getName(), ErrorType.PASSWORD_MISMATCH.getResponseMessage());
		} else if (!(validator.isValidPasword(getPassword()))) {
			addFieldError(CrmFieldType.PASSWORD.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}

		if ((validator.validateBlankField(getBusinessName()))) {
			addFieldError(CrmFieldType.BUSINESS_NAME.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.BUSINESS_NAME,getBusinessName()))) {
			addFieldError(CrmFieldType.BUSINESS_NAME.getName(), validator.getResonseObject().getResponseMessage());
		}

		if (validator.validateBlankField(getMobile())) {
			addFieldError(CrmFieldType.MOBILE.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.MOBILE, getMobile()))) {
			addFieldError(CrmFieldType.MOBILE.getName(), validator.getResonseObject().getResponseMessage());
		}

		if (validator.validateBlankField(getEmailId())) {
			addFieldError(CrmFieldType.EMAILID.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.isValidEmailId(getEmailId()))) {
			addFieldError(CrmFieldType.EMAILID.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		if(!(userType.equalsIgnoreCase(UserType.RESELLER.toString()))){
		if (validator.validateBlankField(getIndustryCategory())) {
			addFieldError(CrmFieldType.INDUSTRY_CATEGORY.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.INDUSTRY_CATEGORY, getIndustryCategory()))) {
			addFieldError(CrmFieldType.INDUSTRY_CATEGORY.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		if (validator.validateBlankField(getIndustrySubCategory())) {
			addFieldError(CrmFieldType.INDUSTRY_SUB_CATEGORY.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.INDUSTRY_SUB_CATEGORY, getIndustrySubCategory()))) {
			addFieldError(CrmFieldType.INDUSTRY_SUB_CATEGORY.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		}
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getIndustryCategory() {
		return industryCategory;
	}

	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}

	public String getIndustrySubCategory() {
		return industrySubCategory;
	}

	public void setIndustrySubCategory(String industrySubCategory) {
		this.industrySubCategory = industrySubCategory;
	}

	public String getUserRoleType() {
		return userRoleType;
	}

	public void setUserRoleType(String userRoleType) {
		this.userRoleType = userRoleType;
	}


}
