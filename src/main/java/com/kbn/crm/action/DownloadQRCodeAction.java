package com.kbn.crm.action;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;

import com.kbn.commons.user.Invoice;
import com.kbn.commons.user.InvoiceTransactionDao;
import com.kbn.commons.util.QRCodeCreator;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class DownloadQRCodeAction extends AbstractSecureAction {

	private static final long serialVersionUID = -5708726455052826940L;
	private InputStream fileInputStream;
	private static Logger logger = Logger
			.getLogger(DownloadQRCodeAction.class.getName());
	private String invoiceId;

	public String downloadQRCode() {
		try {
			InvoiceTransactionDao invoiceTransactionDao = new InvoiceTransactionDao();

			Invoice invoiceDB = invoiceTransactionDao
					.findByInvoiceId(getInvoiceId());

			BufferedImage image = new QRCodeCreator().generateQRCode(invoiceDB);
			File file = new File("qrcode.jpg");
			ImageIO.write(image, "jpg", file);
			fileInputStream = new FileInputStream(file);

		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return SUCCESS;

	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

}
