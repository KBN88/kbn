package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.NetBankingType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TransactionType;
import com.kbn.commons.util.UPIType;
import com.kbn.commons.util.WalletType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Currency;

public class MerchantMappingAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(MerchantMappingAction.class.getName());
	private static final long serialVersionUID = 905765909007885886L;

	private int countList;
	public List<PaymentType> paymentList = new ArrayList<PaymentType>();
	public Map<String,Object> mopList = new LinkedHashMap<String,Object>();
	public List<MopType> mopListCC = new ArrayList<MopType>();
	public List<MopType> mopListDC = new ArrayList<MopType>();
	public List<NetBankingType> mopListNB = new ArrayList<NetBankingType>();
	//public List<MopType> mopListWL = new ArrayList<MopType>();
	public List<UPIType> mopListUPI = new ArrayList<UPIType>();
	public List<WalletType> mopListWL = new ArrayList<WalletType>();
	public List<TransactionType> transList = new ArrayList<TransactionType>();
	public List<Merchants> listMerchant = new ArrayList<Merchants>();
	private String merchantEmailId;
	private String acquirer;
	private Map<String,String> currencies;

	@SuppressWarnings("unchecked")
	public String execute() {
		UserDao userDao = new UserDao();
		try{	
			setListMerchant(userDao.getMerchantList());	
			if (!(acquirer == null || acquirer.equals(""))) {				
				createList();
				setCountList(PaymentType.values().length);							
			}
		}catch(Exception exception){
			logger.error("Exception", exception);
			return ERROR;
		}
		return INPUT;
	}

	public void validate(){
		CrmValidator validator = new CrmValidator();
		if ((validator.validateBlankField(getAcquirer()))) {
		} else if (!(validator.validateField(CrmFieldType.ACQUIRER, getAcquirer()))) {
			addFieldError(CrmFieldType.ACQUIRER.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
	}

	private void createList(){

		AcquirerType acqType = AcquirerType.getInstancefromCode(acquirer);
		List<PaymentType> supportedPaymentTypes = PaymentType.getGetPaymentsFromSystemProp(acquirer);		
		setCurrencies(Currency.getAllCurrency());

		switch(acqType){
		case CITRUS_PAY:
			for(PaymentType pay:supportedPaymentTypes){
				switch(pay){
				case CREDIT_CARD:
					mopListCC=(MopType.getCITRUSCCMops());
					break;
				case DEBIT_CARD:
					mopListDC = (MopType.getCITRUSDCMops());
					break;
				case NET_BANKING:					
					mopListNB = (NetBankingType.getCITRUSNBMops());
					break;
				case WALLET:
					break;
				default:
					break;
				}
			}
			break;
		case FSS:
			for(PaymentType pay:supportedPaymentTypes){
				switch(pay){
				case CREDIT_CARD:
					mopListCC=(MopType.getFSSCCMops());
					break;
				case DEBIT_CARD:
					mopListDC = (MopType.getFSSDCMops());
					break;
				case NET_BANKING:
					break;
				case WALLET:
					break;
				default:
					break;				
				}
			}
			break;
		case SBI:
			for (PaymentType pay : supportedPaymentTypes) {
				switch (pay) {
				case CREDIT_CARD:
					mopListCC = (MopType.getSBICCMops());
					break;
				case DEBIT_CARD:
					mopListDC = (MopType.getSBIDCMops());
					break;
				case NET_BANKING:
					mopListNB = (NetBankingType.getSBINBMops());
					break;
				default:
					break;
				}
			}
			break;
		case PAYTM:
			for(PaymentType pay:supportedPaymentTypes){
				switch(pay){
				case CREDIT_CARD:
					break;
				case DEBIT_CARD:
					break;
				case NET_BANKING:
					mopListNB = (NetBankingType.getCITRUSNBMops());
					break;
				case WALLET:
					//mopListWL = MopType.getPAYTMWLMops();
					break;
				default:
					break;				
				}
			}
			break;
		case AMEX:
			for(PaymentType pay:supportedPaymentTypes){
				switch(pay){
				case CREDIT_CARD:
					mopListCC=(MopType.getAMEXCCMops());
					break;
				default:
					break;				
				}
			}

			break;
		case DIREC_PAY:
			for(PaymentType pay:supportedPaymentTypes){
				switch(pay){
				case NET_BANKING:
					mopListNB = (NetBankingType.getDIRECPAYNBMops());
					break;
				default:
					break;    
				}
			}			
			break;
		case KOTAK:
			for(PaymentType pay:supportedPaymentTypes){
				switch(pay){
				case NET_BANKING:
					mopListNB =(NetBankingType.getKOTAKNBMops());
					break;
				default:
					break;
				}
			}
			break;
		case YESBANK:
			for(PaymentType pay:supportedPaymentTypes){
				switch(pay){
				case NET_BANKING:
					mopListNB = (NetBankingType.getYESBANKNBMops());
					break;
				default:
					break;    
				}
			}
			break;
		case MOBIKWIK:
			for(PaymentType pay:supportedPaymentTypes){
				switch(pay){
				case WALLET:
					//mopListWL = MopType.getMOBIKWIKWLMops();
					break;
				default:
					break;
				}
			}
			break;
		case ATOM:
			for (PaymentType pay : supportedPaymentTypes) {
				switch (pay) {
				case CREDIT_CARD:
					//mopListCC = (MopType.getPAYPLUTUSCCMops());
					break;
				case DEBIT_CARD:
					//mopListDC = (MopType.getPAYPLUTUSDCMops());
					break;
				case NET_BANKING:
					mopListNB = (NetBankingType.getAtomNBMops());
					break;

				default:
					break;
				}
			}
			break;
		case BILLDESK:
			for (PaymentType pay : supportedPaymentTypes) {
				switch (pay) {
				case CREDIT_CARD:
					mopListCC = (MopType.getBILLDESKCCMops());
					break;
				case DEBIT_CARD:
					mopListDC = (MopType.getBILLDESKDCMops());
					break;
				case NET_BANKING:
					mopListNB = (NetBankingType.getBILLDESKNBMops());
					break;
				case WALLET:
					mopListWL = (WalletType.getBILLDESKWLMops());
					break;
				default:
					break;
				}
			}
			break;
		case SAFEX_PAY:
			for (PaymentType pay : supportedPaymentTypes) {
				switch (pay) {
				case CREDIT_CARD:
					mopListCC = (MopType.getSAFEXPAYCCMops());
					break;
				case DEBIT_CARD:
					mopListDC = (MopType.getSAFEXPAYDCMops());
					break;
				case NET_BANKING:
					mopListNB = (NetBankingType.getSAFEXPAYNBMops());
					break;
				case WALLET:
					mopListWL = (WalletType.getSAFEXPAYWLMops());
					break;
				case UPI:
					mopListUPI = (UPIType.getSAFEXPAYUPIMops());
					break;
				default:
					break;
				}
			}
			break;	
		case CYBER_SOURCE:
			for (PaymentType pay : supportedPaymentTypes) {
				switch (pay) {
				case CREDIT_CARD:
					mopListCC = (MopType.getCYBERSOURCECCMops());
					break;
				case DEBIT_CARD:
					mopListDC = (MopType.getCYBERSOURCEDCMops());
					break;
				case NET_BANKING:
					break;
				case WALLET:
					break;
				default:
					break;
				}
			}
			break;
		case AXIS_BANK_UPI:
			for (PaymentType pay : supportedPaymentTypes) {
				switch (pay) {
				case UPI:
					mopListUPI = (UPIType.getAXISUPIMops());
					break;
				default:
					break;
				}
			}
			break;
			
		case GPAY:
			for (PaymentType pay : supportedPaymentTypes) {
				switch (pay) {
				case UPI:
					mopListUPI = (UPIType.getGPAYUPIMops());
					break;
				default:
					break;
				}
			}
			break;
		case EPAYLATER:
			for (PaymentType pay : supportedPaymentTypes) {
				switch (pay) {
				case WALLET:
					mopListWL = WalletType.getEPAYLATERWLMops();
					break;
				default:
					break;
				}
			}
			break;
		case ISGPAY:
			for (PaymentType pay : supportedPaymentTypes) {
				switch (pay) {
				case CREDIT_CARD:
					mopListCC = (MopType.getISGPAYCCMops());
					break;
				case DEBIT_CARD:
					mopListDC = (MopType.getISGPAYDCMops());
					break;
				case NET_BANKING:
					mopListNB = (NetBankingType.getISGPAYNBMops());
					break;
				default:
					break;
				}
			}
			break;
		case EZEECLICK:
			for(PaymentType pay:supportedPaymentTypes){
				switch(pay){
				case CREDIT_CARD:
					mopListCC=(MopType.getEZEECLICKCCMops());
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
		if(mopListCC.size()!=0){
			mopList.put(PaymentType.CREDIT_CARD.getName(), mopListCC);
		}
		if(mopListDC.size()!=0){
			mopList.put(PaymentType.DEBIT_CARD.getName(), mopListDC);
		}if (mopListUPI.size() != 0) {
			mopList.put(PaymentType.UPI.getName(), mopListUPI);
		}
	    if(!(mopListNB.isEmpty())){
	    	Collections.sort(mopListNB);
		 mopList.put(PaymentType.NET_BANKING.getName(), mopListNB);	
	   }
	    if(!(mopListWL.isEmpty())){
	    	Collections.sort(mopListWL);
		 mopList.put(PaymentType.WALLET.getName(), mopListWL);	
	   }
	 /*  if(mopListWL.size()!=0){
			mopList.put(PaymentType.WALLET.getName(), mopListWL);
	   }*/
		transList = TransactionType.chargableMopTxn();
	}

	public String display() {
		return NONE;
	}

	public int getCountList() {
		return countList;
	}

	public void setCountList(int countList) {
		this.countList = countList;
	}

	public List<Merchants> getListMerchant() {
		return listMerchant;
	}

	public void setListMerchant(List<Merchants> listMerchant) {
		this.listMerchant = listMerchant;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	public Map<String,String> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(Map<String,String> currencies) {
		this.currencies = currencies;
	}

	public String getMerchantEmailId() {
		return merchantEmailId;
	}

	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}
}
