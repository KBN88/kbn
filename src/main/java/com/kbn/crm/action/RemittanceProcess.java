package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.RemittanceDao;
import com.kbn.commons.user.RemittanceStatusType;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailBuilder;

/**
 * @Isha
 */
public class RemittanceProcess extends AbstractSecureAction {

	private static final long serialVersionUID = 8192075713660364823L;
	private static Logger logger = Logger.getLogger(RemittanceProcess.class
			.getName());
	private String utr;
	private String response;
	private String payId;
	private String merchant;
	private String datefrom;
	private String netAmount;
	private String remittedDate;
	private String remittedAmount;
	private String status;

	public String execute() {
		RemittanceDao remittanceDao = new RemittanceDao();
		UserDao userDao = new UserDao();

		try {
			remittanceDao.updateStatus(getUtr(),
					RemittanceStatusType.PROCESSED.toString());

			setResponse(CrmFieldConstants.PROCESS_INITIATED_SUCCESSFULLY
					.getValue());

			EmailBuilder emailBuilder = new EmailBuilder();
			emailBuilder.remittanceProcessEmail(userDao, utr, payId, merchant,
					datefrom, netAmount, remittedDate, remittedAmount, status);

			return SUCCESS;
		} catch (Exception exception) {
			logger.error(exception);
			return ERROR;
		}
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if (validator.validateBlankField(getUtr())) {
		} else if (!validator.validateField(CrmFieldType.UTR, getUtr())) {
			addFieldError(CrmFieldType.UTR.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getResponse())) {
		} else if (!validator.validateField(CrmFieldType.RESPONSE,
				getResponse())) {
			addFieldError(CrmFieldType.RESPONSE.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public String getUtr() {
		return utr;
	}

	public void setUtr(String utr) {
		this.utr = utr;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getDatefrom() {
		return datefrom;
	}

	public void setDatefrom(String datefrom) {
		this.datefrom = datefrom;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getRemittedDate() {
		return remittedDate;
	}

	public void setRemittedDate(String remittedDate) {
		this.remittedDate = remittedDate;
	}

	public String getRemittedAmount() {
		return remittedAmount;
	}

	public void setRemittedAmount(String remittedAmount) {
		this.remittedAmount = remittedAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
