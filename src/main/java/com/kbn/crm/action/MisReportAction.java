package com.kbn.crm.action;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionReportService;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class MisReportAction extends AbstractSecureAction {

	/**
	 * @Neeraj
	 */
	private static final long serialVersionUID = -4396029354686788213L;
	private static Logger logger = Logger.getLogger(MisReportAction.class.getName());
	private String dateFrom;
	private String dateTo;
	public String acquirer;
	private String merchantEmailId;
	private int draw;
	private int length;
	private int start;
	private BigInteger recordsTotal;
	public  BigInteger recordsFiltered;
	private List<TransactionSummaryReport> aaData = new ArrayList<TransactionSummaryReport>();
	
	public String execute(){
		TransactionReportService reportGen = new TransactionReportService();
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier userIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = userIdentifier.getMerchantPayId(sessionUser, getMerchantEmailId());
			if (sessionUser.getUserType().equals(UserType.RESELLER)){
				setAaData(encoder.encodeTransactionSummary(reportGen.getResellerMISReportList(getDateFrom(), getDateTo(),
						merchantPayId, getAcquirer(), sessionUser)));
				return SUCCESS;
			}else if(sessionUser.getUserType().equals(UserType.ACQUIRER)){
				setRecordsTotal((reportGen.getAcquirerMISReportListCountList(getDateFrom(), getDateTo(),
						merchantPayId, sessionUser.getFirstName(), sessionUser)));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
			setAaData(encoder.encodeTransactionSummary(reportGen.getAcquirerMISReportList(getDateFrom(), getDateTo(),
					merchantPayId, sessionUser.getFirstName(), sessionUser ,getStart(),getLength())));
			recordsFiltered = recordsTotal;
			return SUCCESS;
			}else{
				setRecordsTotal((reportGen.getMISReportListCountList(getDateFrom(), getDateTo(),
						merchantPayId, getAcquirer(), sessionUser)));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
			setAaData(encoder.encodeTransactionSummary(reportGen.getMISReportList(getDateFrom(), getDateTo(),
					merchantPayId, getAcquirer(), sessionUser ,getStart(),getLength())));
			recordsFiltered = recordsTotal;
			return SUCCESS;
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return SUCCESS;
		}
	}
	
	public String citrusMISReport(){
		TransactionReportService reportGen = new TransactionReportService();
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder  = new DataEncoder();
		SessionUserIdentifier userIdentifier = new SessionUserIdentifier();
		try {
			String merchantPayId = userIdentifier.getMerchantPayId(sessionUser, getMerchantEmailId());
			if (sessionUser.getUserType().equals(UserType.RESELLER)){
				setAaData(encoder.encodeTransactionSummary(reportGen.getResellerMISCitrusReportList(getDateFrom(), getDateTo(),
						merchantPayId, getAcquirer(), sessionUser)));
				return SUCCESS;
			}else{
				setRecordsTotal((reportGen.getMISReportListCountList(getDateFrom(), getDateTo(),
						merchantPayId, getAcquirer(), sessionUser)));
				if(getLength()==-1){
					setLength(getRecordsTotal().intValue());
				}
			setAaData(encoder.encodeTransactionSummary(reportGen.getMISCitrusReportList(getDateFrom(), getDateTo(),
					merchantPayId, getAcquirer(), sessionUser,getStart(),getLength())));

			return SUCCESS;
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return SUCCESS;
		}
		
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getAcquirer() {
		return acquirer;
	}
	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}
	public String getMerchantEmailId() {
		return merchantEmailId;
	}
	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}
	public List<TransactionSummaryReport> getAaData() {
		return aaData;
	}
	public void setAaData(List<TransactionSummaryReport> aaData) {
		this.aaData = aaData;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public BigInteger getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(BigInteger recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public BigInteger getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(BigInteger recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	
}
