package com.kbn.crm.action;

import org.apache.log4j.Logger;
import com.google.gson.Gson;
import com.kbn.commons.user.NotificationEmailer;
import com.kbn.crm.actionBeans.MerchantRecordUpdater;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class SendNotificationEmailAction extends AbstractSecureAction {

	/**
	 * @Neeraj
	 */
	private static final long serialVersionUID = 5574018510722062093L;
    private static Logger logger = Logger.getLogger(SendNotificationEmailAction.class.getName());
	private NotificationEmailer notificationEmailer = new NotificationEmailer();
	private String payId;
	private String transactionEmail;

	public String saveNotificationEamil() {
		MerchantRecordUpdater merchantRecordUpdater = new MerchantRecordUpdater();
		try{
			Gson gson = new Gson();
			NotificationEmailer notificationEmailer = gson.fromJson(transactionEmail, NotificationEmailer.class);
			setNotificationEmailer(merchantRecordUpdater.updateNotificationEmail(notificationEmailer, payId));
		}catch (Exception exception) {
			logger.error(exception);
		}
		addActionMessage("Update Successfuly Notification Flag");
		return SUCCESS;
	}

	public NotificationEmailer getNotificationEmailer() {
		return notificationEmailer;
	}

	public void setNotificationEmailer(NotificationEmailer notificationEmailer) {
		this.notificationEmailer = notificationEmailer;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getTransactionEmail() {
		return transactionEmail;
	}

	public void setTransactionEmail(String transactionEmail) {
		this.transactionEmail = transactionEmail;
	}

	
}
