package com.kbn.crm.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.actionBeans.ChargingDetailsMaintainer;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.opensymphony.xwork2.ModelDriven;

/**
 * @author Puneet
 *
 */
public class ChargingDetailEditAction extends AbstractSecureAction implements
		ModelDriven<ChargingDetails> {

	private static Logger logger = Logger.getLogger(ChargingDetailEditAction.class.getName());
	private static final long serialVersionUID = -6517340843571949786L;

	private ChargingDetails chargingDetails = new ChargingDetails();
	private String emailId;
	private String acquirer;
	private String response;
	private String userType;
	private String loginUserEmailId;
	private boolean isPermissionGranted =false;
	public String execute() {
		try {
			
			StringBuilder permissions = new StringBuilder();
			permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));
			
			if(permissions.toString().contains("Create TDR")){
				isPermissionGranted = true;
			}
			
			if(chargingDetails.getMerchantTDR() == 0.0) {
				chargingDetails.setResponse("proper values not entered");
				return SUCCESS;
			}
			ChargingDetailsMaintainer editChargingDetails = new ChargingDetailsMaintainer();
			editChargingDetails.editChargingDetail(emailId, acquirer,
					chargingDetails,isPermissionGranted,userType,loginUserEmailId);

		} catch (Exception exception) {
			setResponse(ErrorType.CHARGINGDETAIL_NOT_SAVED.getResponseMessage());
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public String updateAll() {
		
		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));
		
		if(permissions.toString().contains("Create TDR")){
			isPermissionGranted = true;
		}
		
		try {			
			ChargingDetailsMaintainer editChargingDetails = new ChargingDetailsMaintainer();
			editChargingDetails.editAllChargingDetails(emailId, acquirer, chargingDetails,isPermissionGranted,userType);
			setResponse("All details saved successfully ");
		} catch (Exception exception) {
			setResponse(ErrorType.CHARGINGDETAIL_NOT_SAVED.getResponseMessage());
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}
	
	
	public String updateAllWallet() {
		try {			
			ChargingDetailsMaintainer editChargingDetails = new ChargingDetailsMaintainer();
			editChargingDetails.editAllWalletChargingDetails(emailId, acquirer, chargingDetails);
			setResponse("All details saved successfully ");
		} catch (Exception exception) {
			setResponse(ErrorType.CHARGINGDETAIL_NOT_SAVED.getResponseMessage());
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}
	

	public void validate() {
		CrmValidator validator = new CrmValidator();
		if((validator.validateBlankField(getAcquirer()))){
		}else if(!validator.validateField(CrmFieldType.ACQUIRER, getAcquirer())){
				addFieldError(CrmFieldType.ACQUIRER.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if(validator.validateBlankField(getEmailId())){
		}
	    else if(!validator.validateField(CrmFieldType.EMAILID, getEmailId())){
	    	addFieldError(CrmFieldType.EMAILID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}	
		if((validator.validateBlankField(chargingDetails.getAcquirerName()))){
		}else if(!validator.validateField(CrmFieldType.ACQUIRER, chargingDetails.getAcquirerName())){
				addFieldError(CrmFieldType.ACQUIRER.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if((validator.validateBlankField(chargingDetails.getCurrency()))){
		}else if(!validator.validateField(CrmFieldType.CURRENCY, chargingDetails.getCurrency())){
				addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if((validator.validateBlankField(chargingDetails.getPayId()))){
		}else if(!validator.validateField(CrmFieldType.PAY_ID, chargingDetails.getCurrency())){
				addFieldError(CrmFieldType.PAY_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if((validator.validateBlankField(getResponse()))){
		}else if(!validator.validateField(CrmFieldType.RESPONSE, getResponse())){
				addFieldError(CrmFieldType.RESPONSE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if(!validator.validateChargingDetailsValues(chargingDetails)){
			addFieldError(CrmFieldType.RESPONSE.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	@Override
	public ChargingDetails getModel() {
		return chargingDetails;
	}

	public ChargingDetails getChargingDetails() {
		return chargingDetails;
	}

	public void setChargingDetails(ChargingDetails chargingDetails) {
		this.chargingDetails = chargingDetails;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public boolean isPermissionGranted() {
		return isPermissionGranted;
	}

	public void setPermissionGranted(boolean isPermissionGranted) {
		this.isPermissionGranted = isPermissionGranted;
	}

	public String getLoginUserEmailId() {
		return loginUserEmailId;
	}

	public void setLoginUserEmailId(String loginUserEmailId) {
		this.loginUserEmailId = loginUserEmailId;
	}

}
