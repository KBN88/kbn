package com.kbn.crm.action;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.user.PermissionType;
import com.kbn.commons.user.Permissions;
import com.kbn.commons.user.Roles;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class EditUserCallAction extends AbstractSecureAction{

	private static final long serialVersionUID = 6921991472327981800L;

	private static Logger logger = Logger.getLogger(MerchantNameAction.class
			.getName());
	
	private List<PermissionType> listPermissionType;
	
	private String firstName;
	private String lastName;
	private String mobile;
	private String emailId;
	private Boolean isActive;
	private String permissionString="";
	
	public String execute() {
		try {
			User sessionUser = null;			
			sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			if(sessionUser.getUserType().equals(UserType.MERCHANT)) {
				setListPermissionType(PermissionType.getSubUserPermissionType());
				getPermissions();
			}else if(sessionUser.getUserType().equals(UserType.ACQUIRER)){
				setListPermissionType(PermissionType.getSubAcquirerPermissionType());
				getPermissions();
			}

		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return INPUT;
	}
	
	private void getPermissions() {
		Session session = null;
		try {

			User subUser = new UserDao().find(getEmailId());
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			session.load(subUser, subUser.getEmailId());

			Set<Roles> roles = subUser.getRoles();
			Set<Permissions> permissions = roles.iterator().next().getPermissions();
			if (!permissions.isEmpty()) {
				StringBuilder perms = new StringBuilder();
				Iterator<Permissions> itr = permissions.iterator();
				while (itr.hasNext()) {
					PermissionType permissionType = itr.next().getPermissionType();
					perms.append(permissionType.getPermission());
					perms.append("-");
				}
				perms.deleteCharAt(perms.length() - 1);
				setPermissionString(perms.toString());
			}
			tx.commit();
		} finally {
			HibernateSessionProvider.closeSession(session);
		}
	}
	
	public List<PermissionType> getListPermissionType() {
		return listPermissionType;
	}
	public void setListPermissionType(List<PermissionType> listPermissionType) {
		this.listPermissionType = listPermissionType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPermissionString() {
		return permissionString;
	}

	public void setPermissionString(String permissionString) {
		this.permissionString = permissionString;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}
