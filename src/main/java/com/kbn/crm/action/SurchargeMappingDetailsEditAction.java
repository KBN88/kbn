package com.kbn.crm.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.dao.NotificationDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.Messages;
import com.kbn.commons.user.Surcharge;
import com.kbn.commons.user.SurchargeDao;
import com.kbn.commons.user.SurchargeDetails;
import com.kbn.commons.user.SurchargeDetailsDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.NotificationStatusType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.NotificationDetail;

/**
 * @author Shaiwal
 *
 */
public class SurchargeMappingDetailsEditAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(SurchargeMappingDetailsEditAction.class.getName());
	private static final long serialVersionUID = -6517340843571949786L;

	private SurchargeDetails surchargeDetails = new SurchargeDetails();
	private String emailId;
	private String paymentType;

	private Long id;
	private String payId;
	private String acquirer;
	private String merchantIndustryType;
	private String mopType;
	private BigDecimal bankSurchargePercentageOn;
	private BigDecimal bankSurchargePercentageOff;
	private BigDecimal bankSurchargeAmountOn;
	private BigDecimal bankSurchargeAmountOff;
	private boolean allowOnOff;
	private String status;
	private String response;
	private String userType;
	private String loginUserEmailId;
	public Date currentDate = new Date();

	public String execute() {
		
		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

		UserDao userDao = new UserDao();
		User user = userDao.findPayIdByEmail(emailId);
		payId = user.getPayId();
		String payId = user.getPayId();
		Session session = null;

		SurchargeDao surchargeDao = new SurchargeDao();

		SurchargeDetailsDao surchargeDetailsDao = new SurchargeDetailsDao();

		String paymentTypeName = PaymentType.getInstanceUsingCode(paymentType).getName();
		SurchargeDetails surchargeDetail = surchargeDetailsDao.findDetails(payId, paymentTypeName);

		if (surchargeDetail.getSurchargePercentage().compareTo(bankSurchargePercentageOn) < 0
				|| surchargeDetail.getSurchargeAmount().compareTo(bankSurchargeAmountOn) < 0) {
			return ERROR;
		}
		if (allowOnOff) {
			if (surchargeDetail.getSurchargePercentage().compareTo(bankSurchargePercentageOff) < 0
					|| surchargeDetail.getSurchargeAmount().compareTo(bankSurchargeAmountOff) < 0) {
				return ERROR;
			}
		}
		
		if (status.equals(TDRStatus.ACTIVE.getName())) {

			if (userType.equals(UserType.ADMIN.toString()) || permissions.toString().contains("Create Surcharge")){
			try {
				
				// Cancel any pending requests for surcharge update
				cancelPendingCharge(payId, paymentType, acquirer,mopType);

				List<Surcharge> existingSurchargeList = new ArrayList<Surcharge>();
				existingSurchargeList = surchargeDao.findSurchargeListByPayIdAcquirerName(payId, paymentType, acquirer,
						mopType);

				for (Surcharge surcharge : existingSurchargeList) {

					try {

						session = HibernateSessionProvider.getSession();
						Transaction tx = session.beginTransaction();
						id = surcharge.getId();
						session.load(surcharge, surcharge.getId());
						Surcharge surchargeDetails = (Surcharge) session.get(Surcharge.class, id);
						surchargeDetails.setStatus(TDRStatus.INACTIVE);
						surchargeDetails.setUpdatedDate(currentDate);
						surchargeDetails.setProcessedBy(loginUserEmailId);
						session.update(surchargeDetails);
						tx.commit();
						session.close();

					} catch (HibernateException e) {
						e.printStackTrace();
					} finally {

					}
				}
			}

			catch (Exception e) {
				e.printStackTrace();
			}
			}
			
		}
			
			if (allowOnOff) {
				
				if (userType.equals(UserType.ADMIN.toString()) || permissions.toString().contains("Create Surcharge")){
					createNewSurcharge("1", bankSurchargeAmountOn, bankSurchargePercentageOn , TDRStatus.ACTIVE);
					createNewSurcharge("2", bankSurchargeAmountOff, bankSurchargePercentageOff,TDRStatus.ACTIVE);
					setResponse(ErrorType.BANK_SURCHARGE_UPDATED.getResponseMessage());
					return SUCCESS;
				}
				else{
					
					List<Surcharge> pendingSurchargeList = new ArrayList<Surcharge>();
					pendingSurchargeList = surchargeDao.findPendingSurchargeListByPayIdAcquirerName(payId, paymentType, acquirer,
							mopType);
					if (pendingSurchargeList.size()>0){
						
						setResponse(ErrorType.BANK_SURCHARGE_REQUEST_ALREADY_PENDING.getResponseMessage());
						return SUCCESS;
					}
					else{
						createNewSurcharge("1", bankSurchargeAmountOn, bankSurchargePercentageOn,TDRStatus.PENDING);
						createNewSurcharge("2", bankSurchargeAmountOff, bankSurchargePercentageOff,TDRStatus.PENDING);
						setResponse(ErrorType.BANK_SURCHARGE_REQUEST_SENT_FOR_APPROVAL.getResponseMessage());
						return SUCCESS;
					}
				
				}
			} else {
				if (userType.equals(UserType.ADMIN.toString()) || permissions.toString().contains("Create Surcharge")){
					createNewSurcharge("0", bankSurchargeAmountOn, bankSurchargePercentageOn , TDRStatus.ACTIVE);
					setResponse(ErrorType.BANK_SURCHARGE_UPDATED.getResponseMessage());
					return SUCCESS;
				}
				else{
					
					List<Surcharge> pendingSurchargeList = new ArrayList<Surcharge>();
					pendingSurchargeList = surchargeDao.findPendingSurchargeListByPayIdAcquirerName(payId, paymentType, acquirer,
							mopType);
					if (pendingSurchargeList.size()>0){
						
						setResponse(ErrorType.BANK_SURCHARGE_REQUEST_ALREADY_PENDING.getResponseMessage());
						return SUCCESS;
					}
					else{
						createNewSurcharge("0", bankSurchargeAmountOn, bankSurchargePercentageOn,TDRStatus.PENDING);
						setResponse(ErrorType.BANK_SURCHARGE_REQUEST_SENT_FOR_APPROVAL.getResponseMessage());
						return SUCCESS;
					}
					
				}
				
			}

	//	return SUCCESS;
	}

	private void createNewSurcharge(String onOff, BigDecimal bankSurchargeAmount, BigDecimal bankSurchargePercentage , TDRStatus status) {

		PaymentType payType = PaymentType.getInstanceUsingCode(paymentType);
		MopType mpType = MopType.getInstance(mopType);
		SurchargeDao surchargeDao = new SurchargeDao();
		Surcharge newSurcharge = new Surcharge();
		newSurcharge.setAcquirerName(acquirer);
		newSurcharge.setAllowOnOff(allowOnOff);
		newSurcharge.setBankSurchargeAmount(bankSurchargeAmount);
		newSurcharge.setBankSurchargePercentage(bankSurchargePercentage);
		newSurcharge.setCreatedDate(currentDate);
		newSurcharge.setMerchantIndustryType(merchantIndustryType);
		newSurcharge.setMopType(mpType);
		newSurcharge.setOnOff(onOff);
		newSurcharge.setPayId(payId);
		newSurcharge.setPaymentType(payType);
		newSurcharge.setStatus(status);
		newSurcharge.setUpdatedDate(currentDate);
		if (status.equals(TDRStatus.ACTIVE)){
			newSurcharge.setProcessedBy(loginUserEmailId);
		}
		newSurcharge.setRequestedBy(loginUserEmailId);
				
		NotificationDetail notificationDetail = new NotificationDetail();
		NotificationDao notificationDao = new NotificationDao();
		notificationDetail.setCreateDate(currentDate);
		notificationDetail.setSubmittedBy(loginUserEmailId);
		notificationDetail.setConcernedUser(emailId);
		notificationDetail.setStatus(NotificationStatusType.UNREAD.getName());
		notificationDetail.setMessage(Messages.BANK_SURCHARGE_MESSAGE.getResponseMessage());
		
		notificationDao.create(notificationDetail);
		surchargeDao.create(newSurcharge);

	}
	
	public void cancelPendingCharge(String payId, String paymentTypeName , String acquirerName,String mopTypeName) {

		Session session = null;
			SurchargeDao surchargeDao = new SurchargeDao();
			List<Surcharge> pendingSurchargeList = new ArrayList<Surcharge>();
			pendingSurchargeList = surchargeDao.findPendingSurchargeListByPayIdAcquirerName(payId, paymentType, acquirer,
					mopType);

			if (pendingSurchargeList.size()>0){
			for (Surcharge surcharge : pendingSurchargeList) {

				try {

					session = HibernateSessionProvider.getSession();
					Transaction tx = session.beginTransaction();
					id = surcharge.getId();
					session.load(surcharge, surcharge.getId());
					Surcharge surchargeDetails = (Surcharge) session.get(Surcharge.class, id);
					surchargeDetails.setStatus(TDRStatus.CANCELLED);
					surchargeDetails.setUpdatedDate(currentDate);
					surchargeDetails.setProcessedBy(loginUserEmailId);
					session.update(surchargeDetails);
					tx.commit();
					session.close();

				} catch (HibernateException e) {
					e.printStackTrace();
				} finally {

				}
	}
	}
	}

	public String getMerchantIndustryType() {
		return merchantIndustryType;
	}

	public void setMerchantIndustryType(String merchantIndustryType) {
		this.merchantIndustryType = merchantIndustryType;
	}

	public String getMopType() {
		return mopType;
	}

	public void setMopType(String mopType) {
		this.mopType = mopType;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public SurchargeDetails getSurchargeDetails() {
		return surchargeDetails;
	}

	public void setSurchargeDetails(SurchargeDetails surchargeDetails) {
		this.surchargeDetails = surchargeDetails;
	}

	public BigDecimal getBankSurchargePercentageOn() {
		return bankSurchargePercentageOn;
	}

	public void setBankSurchargePercentageOn(BigDecimal bankSurchargePercentageOn) {
		this.bankSurchargePercentageOn = bankSurchargePercentageOn;
	}

	public BigDecimal getBankSurchargePercentageOff() {
		return bankSurchargePercentageOff;
	}

	public void setBankSurchargePercentageOff(BigDecimal bankSurchargePercentageOff) {
		this.bankSurchargePercentageOff = bankSurchargePercentageOff;
	}

	public BigDecimal getBankSurchargeAmountOn() {
		return bankSurchargeAmountOn;
	}

	public void setBankSurchargeAmountOn(BigDecimal bankSurchargeAmountOn) {
		this.bankSurchargeAmountOn = bankSurchargeAmountOn;
	}

	public BigDecimal getBankSurchargeAmountOff() {
		return bankSurchargeAmountOff;
	}

	public void setBankSurchargeAmountOff(BigDecimal bankSurchargeAmountOff) {
		this.bankSurchargeAmountOff = bankSurchargeAmountOff;
	}

	public boolean isAllowOnOff() {
		return allowOnOff;
	}

	public void setAllowOnOff(boolean allowOnOff) {
		this.allowOnOff = allowOnOff;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getLoginUserEmailId() {
		return loginUserEmailId;
	}

	public void setLoginUserEmailId(String loginUserEmailId) {
		this.loginUserEmailId = loginUserEmailId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}


}
