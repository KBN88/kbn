package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.SearchUserService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class SearchUserAction extends AbstractSecureAction { 

	private static Logger logger = Logger.getLogger(SearchUserAction.class.getName());
	private static final long serialVersionUID = 6916374933409478597L;

	private List<Merchants> aaData = new ArrayList<Merchants>();
	private String emailId;
	private String phoneNo;

	public String execute() {
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
        SearchUserService searchUserService = new SearchUserService();
		DataEncoder encoder = new DataEncoder();
		try {
			if(sessionUser.getUserType().equals(UserType.ACQUIRER)){
				setAaData(encoder.encodeMerchantsObj(searchUserService.getAcquirerSubUsers(sessionUser.getPayId())));
			}else{
				setAaData(encoder.encodeMerchantsObj(searchUserService.getSubUsers(sessionUser.getPayId())));
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public void validate(){
		CrmValidator validator = new CrmValidator();

		if(validator.validateBlankField(getEmailId())){
		}
        else if(!validator.validateField(CrmFieldType.EMAILID, getEmailId())){
        	addFieldError(CrmFieldType. EMAILID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if(validator.validateBlankField(getPhoneNo())){
		}
        else if(!validator.validateField(CrmFieldType.MOBILE, getPhoneNo())){
        	addFieldError("phoneNo", ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public List<Merchants> getAaData() {
		return aaData;
	}

	public void setAaData(List<Merchants> aaData) {
		this.aaData = aaData;
	}
}
