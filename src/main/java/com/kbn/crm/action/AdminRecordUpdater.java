package com.kbn.crm.action;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;

public class AdminRecordUpdater {
	private User userFromDB = null;
	private UserDao userDao = new UserDao();
	public User updateUserProfile(User userFE) {
		userFromDB = userDao.findPayId(userFE.getPayId());
		userFromDB.setBusinessName(userFE.getBusinessName());
		userFromDB.setEmailId(userFE.getEmailId());
		userFromDB.setUserStatus(userFE.getUserStatus());
		userDao.update(userFromDB);
		return userFromDB;
	}

}
