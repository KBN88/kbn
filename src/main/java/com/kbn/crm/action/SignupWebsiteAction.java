package com.kbn.crm.action;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.TransactionType;
import com.kbn.crm.actionBeans.CreateNewUser;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailBuilder;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.RequestCreator;

/**
 * @author Isha
 *
 */

public class SignupWebsiteAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(SignupWebsiteAction.class.getName());
	private static final long serialVersionUID = 5995449017764989418L;

	private String emailId;
	private String password;
	private String businessName;
	private String mobile;
	private String confirmPassword;
	private String captcha;
	private String orderId;
	private String amount;
	
	public String execute() {
		CreateNewUser createUser = new CreateNewUser();
		ResponseObject responseObject = new ResponseObject();

		try {
			responseObject = createUser.createUser(getUserInstance(), UserType.MERCHANT, "");
			if (!ErrorType.SUCCESS.getResponseCode().equals(
					responseObject.getResponseCode())) {
				addActionMessage(responseObject.getResponseMessage());
				return INPUT;
				
				
			}
			// Sending Email for Email Validation
			EmailBuilder emailBuilder = new EmailBuilder();
			emailBuilder.emailValidator(responseObject);
			
			RequestCreator requestCreator= new RequestCreator();
			requestCreator.WebsitePackageRequest(generateRequest());
			
			return NONE;
			
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}
	public Fields generateRequest() throws SystemException {
		Fields fields = new Fields();
		PropertiesManager propertyManager = new PropertiesManager();
		Date date = new Date();
		SimpleDateFormat dateformat = new SimpleDateFormat("ddMMyyHHmmss");
		dateformat.format(new Date());

		fields.put(FieldType.PAY_ID.getName(), propertyManager.getSystemProperty(CrmFieldConstants.WEBSITE_SIGNUP_PAYID.getValue()));
		fields.put(FieldType.ORDER_ID.getName(),  getOrderId() + dateformat.format(date));
		fields.put(FieldType.AMOUNT.getName(), Amount.formatAmount(getAmount(), propertyManager.getSystemProperty(CrmFieldConstants.WEBSITE_PACKAGE_CURRENCY.getValue())));
		fields.put(FieldType.TXNTYPE.getName(), TransactionType.SALE.getName());
		fields.put(FieldType.CUST_NAME.getName(), getBusinessName());
		fields.put(FieldType.CUST_EMAIL.getName(), getEmailId());
		fields.put(FieldType.PRODUCT_DESC.getName(), "Website Package");
		fields.put(FieldType.CURRENCY_CODE.getName(), propertyManager.getSystemProperty(CrmFieldConstants.WEBSITE_PACKAGE_CURRENCY.getValue()));
		fields.put(FieldType.RETURN_URL.getName(),propertyManager.getSystemProperty(CrmFieldConstants.WEBSITE_SIGNUP_RETURNURL.getValue()));
		fields.put(FieldType.HASH.getName(), Hasher.getHash(fields) );
		return fields;
	}
	private User getUserInstance() {
		User user = new User();
		user.setEmailId(getEmailId());
		user.setPassword(getPassword());
		user.setMobile(getMobile());
		user.setBusinessName(getBusinessName());

		return user;
	}

	public void validate() {

		CrmValidator validator = new CrmValidator();

		//Validate blank and invalid fields
		if (validator.validateBlankField(getPassword())) {
			addFieldError(CrmFieldType.PASSWORD.getName(), validator.getResonseObject().getResponseMessage());
			if (validator.validateBlankField(getConfirmPassword())) {
				addFieldError(CrmFieldConstants.CONFIRM_PASSWORD.getValue(), validator.getResonseObject().getResponseMessage());
			}
		} else if (validator.validateBlankField(getConfirmPassword())) {
			addFieldError(CrmFieldConstants.CONFIRM_PASSWORD.getValue(), validator.getResonseObject().getResponseMessage());
		} else if (!(getPassword().equals(getConfirmPassword()))) {
			addFieldError(CrmFieldType.PASSWORD.getName(), ErrorType.PASSWORD_MISMATCH.getResponseMessage());
		} else if (!(validator.isValidPasword(getPassword()))) {
			addFieldError(CrmFieldType.PASSWORD.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}

		if ((validator.validateBlankField(getBusinessName()))) {
			addFieldError(CrmFieldType.BUSINESS_NAME.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.BUSINESS_NAME,getBusinessName()))) {
			addFieldError(CrmFieldType.BUSINESS_NAME.getName(), validator.getResonseObject().getResponseMessage());
		}

		if (validator.validateBlankField(getMobile())) {
			addFieldError(CrmFieldType.MOBILE.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.MOBILE, getMobile()))) {
			addFieldError(CrmFieldType.MOBILE.getName(), validator.getResonseObject().getResponseMessage());
		}

		if (validator.validateBlankField(getEmailId())) {
			addFieldError(CrmFieldType.EMAILID.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.isValidEmailId(getEmailId()))) {
			addFieldError(CrmFieldType.EMAILID.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		
		if (validator.validateBlankField(getCaptcha())) {
			addFieldError(CrmFieldType.CAPTCHA.getName(), validator.getResonseObject().getResponseMessage());
		} 
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}

	
	

	

	
}
