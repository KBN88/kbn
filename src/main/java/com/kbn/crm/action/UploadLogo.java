package com.kbn.crm.action;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.opensymphony.xwork2.ModelDriven;

public class UploadLogo extends AbstractSecureAction implements
		ModelDriven<User> {

	private static Logger logger = Logger.getLogger(UploadLogo.class.getName());
	private File UserLogo;
	private String UserLogoFileName;
	private String payId;
	private static final long serialVersionUID = 505168992585123166L;
	private User user = new User();

	public String execute() {
		if (UserLogo != null && (UserLogoFileName.toLowerCase().endsWith(".jpg") || UserLogoFileName.toLowerCase().endsWith(".png"))) {
			SaveFile(UserLogoFileName, UserLogo);
		}
		return SUCCESS;
	}

	private String SaveFile(String filename, File controlFile) {
		String destPath;
		String saveFilename;
		PropertiesManager propertiesManager = new PropertiesManager();
		destPath = propertiesManager.getSystemProperty("DocumentPath");
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		if (sessionUser.getUserType().equals(UserType.ADMIN)
				|| sessionUser.getUserType().equals(UserType.SUPERADMIN)) {
			// payId=user.getPayId();
		} else {
			payId = sessionUser.getPayId();
		}
		saveFilename = payId + ".jpg";
		File destFile = new File(destPath, saveFilename);
		try {
			FileUtils.copyFile(controlFile, destFile);
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;

	}

	public File getUserLogo() {
		return UserLogo;
	}

	public void setUserLogo(File userLogo) {
		UserLogo = userLogo;
	}

	public String getUserLogoFileName() {
		return UserLogoFileName;
	}

	public void setUserLogoFileName(String userLogoFileName) {
		UserLogoFileName = userLogoFileName;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		return null;
	}

}
