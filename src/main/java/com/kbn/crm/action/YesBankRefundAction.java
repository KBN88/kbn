package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.netbanking.yesbank.RefundFieldsCreator;

public class YesBankRefundAction extends AbstractSecureAction{

	private static final long serialVersionUID = -663755198176956580L;
	private String dateFrom;
	private String dateTo;
	private List<TransactionSummaryReport> aaData = new ArrayList<TransactionSummaryReport>();
	
	public String execute(){
		RefundFieldsCreator rfc = new RefundFieldsCreator();
		
		try {
			aaData = rfc.getTransactionRefundReport(dateFrom, dateTo);
			// aaData.add(transactionSummaryReport);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return SUCCESS;
		
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public List<TransactionSummaryReport> getAaData() {
		return aaData;
	}

	public void setAaData(List<TransactionSummaryReport> aaData) {
		this.aaData = aaData;
	}

}
