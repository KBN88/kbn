package com.kbn.crm.action;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionSearchService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.CustomerAddress;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class AuthoriseDetailsAction extends AbstractSecureAction{

	private static final long serialVersionUID = 5442462672736496234L;
	private static Logger logger = Logger.getLogger(AuthoriseDetailsAction.class.getName());
	
	private String orderId;
	private TransactionSummaryReport transactionSummary = new TransactionSummaryReport();
	private CustomerAddress aaData;
	private String transactionId;
	private String custName;
	private String internalCardIssusserBank;
	private String internalCardIssusserCountry;
	private boolean voidEnable = false;
	
	public String execute() {
		DataEncoder encoder = new DataEncoder();
		TransactionSearchService transactionSearchService = new TransactionSearchService();
		try {			
			setAaData(encoder.encodeCustomerAddressObj(transactionSearchService.getCustAddress(
					getTransactionId())));
			transactionSummary = transactionSearchService.getTransaction(getTransactionId());
			if(StringUtils.isEmpty(transactionSummary.getCaptureTxnId())){
				voidEnable = DateUtils.isSameDay(new Date(),transactionSummary.getDate());
			}
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}
	
	public void validate(){
		CrmValidator validator = new CrmValidator();

		if(validator.validateBlankField(getOrderId())){
		}
		else if(!validator.validateField(CrmFieldType.ORDER_ID, getOrderId())){
			addFieldError(CrmFieldType.ORDER_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if(validator.validateBlankField(getCustName())){
		}
		else if(!validator.validateField(CrmFieldType.FIRSTNAME, getCustName())){
        	addFieldError("custName", ErrorType.INVALID_FIELD.getResponseMessage());
		}
		if(validator.validateBlankField(getTransactionId())){
		}
		else if(!validator.validateField(CrmFieldType.TRANSACTION_ID, getTransactionId())){
			addFieldError(CrmFieldType.TRANSACTION_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}	
	
	public CustomerAddress getAaData() {
		return aaData;
	}
	
	public void setAaData(CustomerAddress aaData) {
		this.aaData = aaData;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public TransactionSummaryReport getTransactionSummary() {
		return transactionSummary;
	}

	public void setTransactionSummary(TransactionSummaryReport transactionSummary) {
		this.transactionSummary = transactionSummary;
	}

	public boolean isVoidEnable() {
		return voidEnable;
	}

	public void setVoidEnable(boolean voidEnable) {
		this.voidEnable = voidEnable;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getInternalCardIssusserBank() {
		return internalCardIssusserBank;
	}

	public void setInternalCardIssusserBank(String internalCardIssusserBank) {
		this.internalCardIssusserBank = internalCardIssusserBank;
	}

	public String getInternalCardIssusserCountry() {
		return internalCardIssusserCountry;
	}

	public void setInternalCardIssusserCountry(String internalCardIssusserCountry) {
		this.internalCardIssusserCountry = internalCardIssusserCountry;
	}

}
