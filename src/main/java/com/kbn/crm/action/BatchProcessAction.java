package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.interceptor.TokenValidationInterceptor;
import com.kbn.commons.user.Remittance;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.actionBeans.RemittanceProcessor;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class BatchProcessAction extends AbstractSecureAction {

	private static final long serialVersionUID = 2896827956294696632L;
	private static Logger logger = Logger.getLogger(BatchProcessAction.class
			.getName());
	
	private List<Remittance> remittanceList = new ArrayList<Remittance>();
	private String token;
	private String response;
	public String execute(){
		try {
			
		RemittanceProcessor  remittanceProcessor = new RemittanceProcessor();
		remittanceProcessor.processAll(remittanceList);
		setResponse(CrmFieldConstants.PROCESS_INITIATED_SUCCESSFULLY.getValue());
	} catch (Exception exception) {
		logger.error("Error batch processing:" + exception);
		setResponse(ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
	}
		remittanceList.clear();
	return SUCCESS;
		
	}
	
	public void validate(){
		if(validateToken()){
			addFieldError(CrmFieldType.PAY_ID.getName(), ErrorType.VALIDATION_FAILED.getResponseMessage());
			return;
		}
		CrmValidator validator = new CrmValidator();
		for(Remittance remittance:remittanceList){
			if ((validator.validateBlankField(remittance.getPayId()))) {
			} else if (!(validator.validateField(CrmFieldType.PAY_ID, remittance.getPayId()))) {
				addFieldError(CrmFieldType.PAY_ID.getName(), validator.getResonseObject().getResponseMessage());
			}

			if ((validator.validateBlankField(remittance.getDateFrom()))) {
			} else if (!(validator.validateField(CrmFieldType.DATE_FROM,remittance.getDateFrom()))) {
				addFieldError(CrmFieldType.DATE_FROM.getName(), validator.getResonseObject().getResponseMessage());
			}
			
			if(validator.validateBlankField(remittance.getCurrency())){
		    }else if(!validator.validateField(CrmFieldType.CURRENCY, remittance.getCurrency())){
		      	addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
			
		   if(validator.validateBlankField(remittance.getRemittedAmount())){
			}
			else if(!validator.validateField(CrmFieldType.AMOUNT, remittance.getRemittedAmount())){
				addFieldError(CrmFieldType.AMOUNT.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		   
		   if(validator.validateBlankField(remittance.getNetAmount())){
			}
			else if(!validator.validateField(CrmFieldType.NET_AMOUNT, remittance.getNetAmount())){
				addFieldError(CrmFieldType.NET_AMOUNT.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		   
		   if(validator.validateBlankField(remittance.getMerchant())){
			}
			else if(!validator.validateField(CrmFieldType.MERCHANT, remittance.getMerchant())){
				addFieldError(CrmFieldType.MERCHANT.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
			}
		   
		   if (validator.validateBlankField(remittance.getUtr())) {
			} 
		   else if (!validator.validateField(CrmFieldType.UTR, remittance.getUtr())) {
				addFieldError(CrmFieldType.UTR.getName(),ErrorType.INVALID_FIELD.getResponseMessage());
			}

			if (validator.validateBlankField(remittance.getStatus())) {
			}
			else if (!validator.validateField(CrmFieldType.STATUS,remittance.getStatus())) {
				addFieldError(CrmFieldType.STATUS.getName(),ErrorType.INVALID_FIELD.getResponseMessage());
			}
			
			if ((validator.validateBlankField(remittance.getRemittanceDate()))) {
			} else if (!(validator.validateField(CrmFieldType.REMITTED_DATE,remittance.getRemittanceDate()))) {
				addFieldError(CrmFieldType.REMITTED_DATE.getName(), validator.getResonseObject().getResponseMessage());
			}
		}
	}
	public boolean validateToken(){
		if(null==token || !(token.equals((String) sessionMap.get(TokenValidationInterceptor.SERVER_END_NAME)))){
			return true;
		}
		return false;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	public List<Remittance> getRemittanceList() {
		return remittanceList;
	}

	public void setRemittanceList(List<Remittance> remittanceList) {
		this.remittanceList = remittanceList;
	}
}
