package com.kbn.crm.action;

import java.util.List;
import org.apache.log4j.Logger;

import com.kbn.commons.user.PermissionType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.crm.commons.action.AbstractSecureAction;
/**
 * @author Rahul
 *
 */
public class AddUserCallAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(MerchantNameAction.class.getName());
	private static final long serialVersionUID = 9033493264155370845L;
	private List<PermissionType> listPermissionType;

	public String execute() {
		try {
			User sessionUser = null;
			sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			
			if(sessionUser.getUserType().equals(UserType.MERCHANT)) {
				setListPermissionType(PermissionType.getSubUserPermissionType());
				return "merchant";
			} else if(sessionUser.getUserType().equals(UserType.ACQUIRER)){
				setListPermissionType(PermissionType.getSubAcquirerPermissionType());
				return "reseller";
			} else if(sessionUser.getUserType().equals(UserType.ADMIN)){
				setListPermissionType(PermissionType.getPermissionType());
				return "subAdmin";
			} else if(sessionUser.getUserType().equals(UserType.SUBADMIN)){
				setListPermissionType(new AddSubAdminPermissionAction().getSubAdminPermissionType(sessionMap));
				return "subAdmin";
			}
		}catch(Exception exception)
	{
		logger.error("Exception", exception);
		return ERROR;
	};
	return null;
	}

	public List<PermissionType> getListPermissionType() {
		return listPermissionType;
	}

	public void setListPermissionType(List<PermissionType> listPermissionType) {
		this.listPermissionType = listPermissionType;
	}
}
