package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.SearchInvoiceService;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Invoice;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DataEncoder;
import com.kbn.commons.util.DateCreater;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class InvoiceSearchAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(InvoiceSearchAction.class
			.getName());
	private static final long serialVersionUID = 8559806979618843084L;

	private List<Invoice> aaData = new ArrayList<Invoice>();
	private String invoiceNo;
	private String customerEmail;
	private String merchantEmailId;
	private String currency;
	private String dateFrom;
	private String dateTo;
	private String invoiceType;

	public String execute() {
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		DataEncoder encoder = new DataEncoder();
		SearchInvoiceService getInvoice = new SearchInvoiceService();

		SessionUserIdentifier userIdentifier = new SessionUserIdentifier();

		try {
			String merchantPayId = userIdentifier.getMerchantPayId(sessionUser,
					getMerchantEmailId());
			setAaData(encoder.encodeInvoiceSearchObj(getInvoice.getInvoiceList(
					getDateFrom(), getDateTo(), merchantPayId, sessionUser
							.getUserType().toString(), getInvoiceNo(),
					getCustomerEmail(), getCurrency(),getInvoiceType())));

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if (validator.validateBlankField(getInvoiceNo())) {
		} else if (!validator.validateField(CrmFieldType.INVOICE_NUMBER,
				getInvoiceNo())) {
			addFieldError("invoiceNo",
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getCurrency())) {
		} else if (!validator.validateField(CrmFieldType.CURRENCY,
				getCurrency())) {
			addFieldError(CrmFieldType.CURRENCY.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getDateFrom())) {
		} else if (!validator.validateField(CrmFieldType.DATE_FROM,
				getDateFrom())) {
			addFieldError(CrmFieldType.DATE_FROM.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getDateTo())) {
		} else if (!validator.validateField(CrmFieldType.DATE_TO, getDateTo())) {
			addFieldError(CrmFieldType.DATE_TO.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
		
		if(!validator.validateBlankField(getDateTo())){
	 	       if(DateCreater.formatStringToDate(DateCreater.formatFromDate(getDateFrom())).compareTo(DateCreater.formatStringToDate(DateCreater.formatFromDate(getDateTo()))) > 0) {
	 	        	addFieldError(CrmFieldType.DATE_FROM.getName(), CrmFieldConstants.FROMTO_DATE_VALIDATION.getValue());
	 	        }
	 	        else if(DateCreater.diffDate(getDateFrom(), getDateTo()) > 31) {
	 	        	addFieldError(CrmFieldType.DATE_FROM.getName(), CrmFieldConstants.DATE_RANGE.getValue());
	 	        }
	         }

		if (validator.validateBlankField(getMerchantEmailId())
				|| getMerchantEmailId()
						.equals(CrmFieldConstants.ALL.getValue())) {
		} else if (!validator.validateField(CrmFieldType.MERCHANT_EMAIL_ID,
				getMerchantEmailId())) {
			addFieldError(CrmFieldType.MERCHANT_EMAIL_ID.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getCustomerEmail())) {
		} else if (!validator.validateField(CrmFieldType.CUSTOMER_EMAIL_ID,
				getCustomerEmail())) {
			addFieldError(CrmFieldType.CUSTOMER_EMAIL_ID.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public List<Invoice> getAaData() {
		return aaData;
	}

	public void setAaData(List<Invoice> aaData) {
		this.aaData = aaData;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getMerchantEmailId() {
		return merchantEmailId;
	}

	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	
}
