package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.BatchTransactionObj;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.actionBeans.RefundProcessor;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class BatchRefundAction extends AbstractSecureAction {

	private static final long serialVersionUID = -4502244100134090359L;
	private static Logger logger = Logger.getLogger(BatchCaptureAction.class
			.getName());
	private List<BatchTransactionObj> refundList = new ArrayList<BatchTransactionObj>();
	private String response;

	public String execute() {
		try {
			HttpServletRequest request = ServletActionContext.getRequest();
			User sessionUser=(User) sessionMap.get(Constants.USER.getValue());
			RefundProcessor refundProcessor = new RefundProcessor();
			response = refundProcessor.processAll(refundList, sessionUser, request.getRemoteAddr());
			if(StringUtils.isEmpty(response)){
				setResponse(CrmFieldConstants.PROCESS_INITIATED_SUCCESSFULLY.getValue());
			}
		}
		catch (SystemException systemException) {
			logger.error("Error while processing batch refund:" + systemException);
			setResponse(systemException.getMessage());
		}catch (Exception exception) {
			logger.error("Error while processing batch refund:" + exception);
			setResponse(ErrorType.REFUND_NOT_SUCCESSFULL.getResponseMessage());
		}
		refundList.clear();
		return SUCCESS;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<BatchTransactionObj> getRefundList() {
		return refundList;
	}

	public void setRefundList(List<BatchTransactionObj> refundList) {
		this.refundList = refundList;
	}
}

