package com.kbn.crm.action;

import java.util.Date;

import org.apache.log4j.Logger;

import com.kbn.commons.user.PendingUserApproval;
import com.kbn.commons.user.PendingUserApprovalDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.ModeType;
import com.kbn.commons.util.TDRStatus;
import com.kbn.commons.util.UserStatusType;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Rahul
 *
 */
public class UpdateUserProfileAction extends AbstractSecureAction {

	private static final long serialVersionUID = 3663172661005555870L;
	private static Logger logger = Logger.getLogger(UpdateUserProfileAction.class.getName());

	private String emailId;
	private String payId;
	private String businessName;
	private String firstName;
	private String lastName;
	private String companyName;
	private String contactPerson;

	private String merchantType;
	private String resellerId;
	private String productDetail;
	private Date registrationDate;
	private Date updateDate;
	private String requestedBy;
	private String mobile;
	private boolean transactionSmsFlag;
	private String telephoneNo;
	private String fax;
	private String address;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private ModeType modeType;
	private String comments;
	private String whiteListIpAddress;
	private String bankName;
	private String ifscCode;
	private String accHolderName;
	private String currency;
	private String branchName;
	private String panCard;
	private String accountNo;
	private boolean emailValidationFlag;
	private String organisationType;
	private String website;
	private String multiCurrency;
	private String businessModel;
	private String operationAddress;
	private String operationState;
	private String operationCity;
	private String operationPostalCode;
	private String dateOfEstablishment;
	private String cin;
	private String pan;
	private String panName;
	private String noOfTransactions;
	private String amountOfTransactions;
	private String attemptTrasacation;
	private String industryCategory;
	private String industrySubCategory;
	private String transactionEmailId;
	private boolean transactionEmailerFlag;
	private boolean expressPayFlag;
	private boolean merchantHostedFlag;
	private boolean iframePaymentFlag;
	private boolean transactionAuthenticationEmailFlag;
	private boolean transactionCustomerEmailFlag;
	private boolean refundTransactionCustomerEmailFlag;
	private boolean refundTransactionMerchantEmailFlag;
	private boolean retryTransactionCustomeFlag;
	private boolean surchargeFlag;
	private float extraRefundLimit;
	private String defaultCurrency;
	private String amexSellerId;
	private String mCC;
	private String defaultLanguage;
	private UserStatusType userStatus;
	private String loginEmailId;
	private UserType loginUserType;
	private String operation;

	public String execute() {
		Date date = new Date();
		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

		if (permissions.toString().contains("Edit Merchant Details") || loginUserType.equals(UserType.ADMIN)) {
			if (operation.equals("reject")) {
				PendingUserApprovalDao pendingUserApprovalDao = new PendingUserApprovalDao();
				PendingUserApproval pendingUser = pendingUserApprovalDao.find(payId);
				pendingUser.setRequestStatus(TDRStatus.REJECTED.toString());
				pendingUser.setUpdateDate(date);
				pendingUser.setProcessedBy(loginEmailId);
				pendingUserApprovalDao.update(pendingUser);
				return SUCCESS;
			} else if(operation.equals("accept")){
			UserDao userDao = new UserDao();
			User dbUser = userDao.findPayId(payId);
			dbUser.setAccHolderName(accHolderName);
			dbUser.setAccountNo(accountNo);
			dbUser.setAddress(address);
			dbUser.setAmexSellerId(amexSellerId);
			dbUser.setAmountOfTransactions(amountOfTransactions);
			dbUser.setAttemptTrasacation(attemptTrasacation);
			dbUser.setBankName(bankName);
			dbUser.setBranchName(branchName);
			dbUser.setBusinessModel(businessModel);
			dbUser.setBusinessName(businessName);
			dbUser.setCin(cin);
			dbUser.setCity(city);
			dbUser.setCompanyName(companyName);
			dbUser.setComments(comments);
			dbUser.setContactPerson(contactPerson);
			dbUser.setCountry(country);
			dbUser.setCurrency(currency);
			dbUser.setDateOfEstablishment(dateOfEstablishment);
			dbUser.setDefaultCurrency(defaultCurrency);
			dbUser.setDefaultLanguage(defaultLanguage);
			dbUser.setEmailValidationFlag(emailValidationFlag);
			dbUser.setExpressPayFlag(expressPayFlag);
			dbUser.setExtraRefundLimit(extraRefundLimit);
			dbUser.setFax(fax);
			dbUser.setFirstName(firstName);
			dbUser.setIframePaymentFlag(iframePaymentFlag);
			dbUser.setIfscCode(ifscCode);
			dbUser.setIndustryCategory(industryCategory);
			dbUser.setIndustrySubCategory(industrySubCategory);
			dbUser.setLastName(lastName);
			dbUser.setMCC(mCC);
			dbUser.setMerchantHostedFlag(merchantHostedFlag);
			dbUser.setMerchantType(merchantType);
			dbUser.setMobile(mobile);
			dbUser.setModeType(modeType);
			dbUser.setMultiCurrency(multiCurrency);
			dbUser.setNoOfTransactions(noOfTransactions);
			dbUser.setOperationAddress(operationAddress);
			dbUser.setOperationCity(operationCity);
			dbUser.setOperationPostalCode(operationPostalCode);
			dbUser.setOperationState(operationState);
			dbUser.setOrganisationType(organisationType);
			dbUser.setPan(pan);
			dbUser.setPanCard(panCard);
			dbUser.setPanName(panName);
			dbUser.setPayId(payId);
			dbUser.setPostalCode(postalCode);
			dbUser.setProductDetail(productDetail);
			dbUser.setRefundTransactionCustomerEmailFlag(refundTransactionCustomerEmailFlag);
			dbUser.setRefundTransactionMerchantEmailFlag(refundTransactionMerchantEmailFlag);
			dbUser.setRegistrationDate(registrationDate);
			dbUser.setResellerId(resellerId);
			dbUser.setRetryTransactionCustomeFlag(retryTransactionCustomeFlag);
			dbUser.setState(state);
			dbUser.setSurchargeFlag(surchargeFlag);
			dbUser.setTelephoneNo(telephoneNo);
			dbUser.setTransactionAuthenticationEmailFlag(transactionAuthenticationEmailFlag);
			dbUser.setTransactionCustomerEmailFlag(refundTransactionCustomerEmailFlag);
			dbUser.setTransactionEmailId(transactionEmailId);
			dbUser.setTransactionEmailerFlag(transactionEmailerFlag);
			dbUser.setTransactionSmsFlag(transactionSmsFlag);
			dbUser.setUserStatus(userStatus);
			dbUser.setWebsite(website);
			dbUser.setWhiteListIpAddress(whiteListIpAddress);
			dbUser.setUpdateDate(date);
			dbUser.setUpdatedBy(loginEmailId);

			userDao.update(dbUser);

			PendingUserApprovalDao pendingUserApprovalDao = new PendingUserApprovalDao();
			PendingUserApproval pendingUser = pendingUserApprovalDao.find(payId);
			pendingUser.setProcessedBy(loginEmailId);
			pendingUser.setRequestStatus(TDRStatus.ACCEPTED.toString());
			pendingUser.setUpdateDate(date);
			pendingUserApprovalDao.update(pendingUser);
			}
		}
		return SUCCESS;

	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

	public String getResellerId() {
		return resellerId;
	}

	public void setResellerId(String resellerId) {
		this.resellerId = resellerId;
	}

	public String getProductDetail() {
		return productDetail;
	}

	public void setProductDetail(String productDetail) {
		this.productDetail = productDetail;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public boolean isTransactionSmsFlag() {
		return transactionSmsFlag;
	}

	public void setTransactionSmsFlag(boolean transactionSmsFlag) {
		this.transactionSmsFlag = transactionSmsFlag;
	}

	public String getTelephoneNo() {
		return telephoneNo;
	}

	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public ModeType getModeType() {
		return modeType;
	}

	public void setModeType(ModeType modeType) {
		this.modeType = modeType;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getWhiteListIpAddress() {
		return whiteListIpAddress;
	}

	public void setWhiteListIpAddress(String whiteListIpAddress) {
		this.whiteListIpAddress = whiteListIpAddress;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAccHolderName() {
		return accHolderName;
	}

	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getPanCard() {
		return panCard;
	}

	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public boolean isEmailValidationFlag() {
		return emailValidationFlag;
	}

	public void setEmailValidationFlag(boolean emailValidationFlag) {
		this.emailValidationFlag = emailValidationFlag;
	}

	public String getOrganisationType() {
		return organisationType;
	}

	public void setOrganisationType(String organisationType) {
		this.organisationType = organisationType;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getMultiCurrency() {
		return multiCurrency;
	}

	public void setMultiCurrency(String multiCurrency) {
		this.multiCurrency = multiCurrency;
	}

	public String getBusinessModel() {
		return businessModel;
	}

	public void setBusinessModel(String businessModel) {
		this.businessModel = businessModel;
	}

	public String getOperationAddress() {
		return operationAddress;
	}

	public void setOperationAddress(String operationAddress) {
		this.operationAddress = operationAddress;
	}

	public String getOperationState() {
		return operationState;
	}

	public void setOperationState(String operationState) {
		this.operationState = operationState;
	}

	public String getOperationCity() {
		return operationCity;
	}

	public void setOperationCity(String operationCity) {
		this.operationCity = operationCity;
	}

	public String getOperationPostalCode() {
		return operationPostalCode;
	}

	public void setOperationPostalCode(String operationPostalCode) {
		this.operationPostalCode = operationPostalCode;
	}

	public String getDateOfEstablishment() {
		return dateOfEstablishment;
	}

	public void setDateOfEstablishment(String dateOfEstablishment) {
		this.dateOfEstablishment = dateOfEstablishment;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getPanName() {
		return panName;
	}

	public void setPanName(String panName) {
		this.panName = panName;
	}

	public String getNoOfTransactions() {
		return noOfTransactions;
	}

	public void setNoOfTransactions(String noOfTransactions) {
		this.noOfTransactions = noOfTransactions;
	}

	public String getAmountOfTransactions() {
		return amountOfTransactions;
	}

	public void setAmountOfTransactions(String amountOfTransactions) {
		this.amountOfTransactions = amountOfTransactions;
	}

	public String getAttemptTrasacation() {
		return attemptTrasacation;
	}

	public void setAttemptTrasacation(String attemptTrasacation) {
		this.attemptTrasacation = attemptTrasacation;
	}

	public String getIndustryCategory() {
		return industryCategory;
	}

	public void setIndustryCategory(String industryCategory) {
		this.industryCategory = industryCategory;
	}

	public String getIndustrySubCategory() {
		return industrySubCategory;
	}

	public void setIndustrySubCategory(String industrySubCategory) {
		this.industrySubCategory = industrySubCategory;
	}

	public String getTransactionEmailId() {
		return transactionEmailId;
	}

	public void setTransactionEmailId(String transactionEmailId) {
		this.transactionEmailId = transactionEmailId;
	}

	public boolean isTransactionEmailerFlag() {
		return transactionEmailerFlag;
	}

	public void setTransactionEmailerFlag(boolean transactionEmailerFlag) {
		this.transactionEmailerFlag = transactionEmailerFlag;
	}

	public boolean isExpressPayFlag() {
		return expressPayFlag;
	}

	public void setExpressPayFlag(boolean expressPayFlag) {
		this.expressPayFlag = expressPayFlag;
	}

	public boolean isMerchantHostedFlag() {
		return merchantHostedFlag;
	}

	public void setMerchantHostedFlag(boolean merchantHostedFlag) {
		this.merchantHostedFlag = merchantHostedFlag;
	}

	public boolean isIframePaymentFlag() {
		return iframePaymentFlag;
	}

	public void setIframePaymentFlag(boolean iframePaymentFlag) {
		this.iframePaymentFlag = iframePaymentFlag;
	}

	public boolean isTransactionAuthenticationEmailFlag() {
		return transactionAuthenticationEmailFlag;
	}

	public void setTransactionAuthenticationEmailFlag(boolean transactionAuthenticationEmailFlag) {
		this.transactionAuthenticationEmailFlag = transactionAuthenticationEmailFlag;
	}

	public boolean isTransactionCustomerEmailFlag() {
		return transactionCustomerEmailFlag;
	}

	public void setTransactionCustomerEmailFlag(boolean transactionCustomerEmailFlag) {
		this.transactionCustomerEmailFlag = transactionCustomerEmailFlag;
	}

	public boolean isRefundTransactionCustomerEmailFlag() {
		return refundTransactionCustomerEmailFlag;
	}

	public void setRefundTransactionCustomerEmailFlag(boolean refundTransactionCustomerEmailFlag) {
		this.refundTransactionCustomerEmailFlag = refundTransactionCustomerEmailFlag;
	}

	public boolean isRefundTransactionMerchantEmailFlag() {
		return refundTransactionMerchantEmailFlag;
	}

	public void setRefundTransactionMerchantEmailFlag(boolean refundTransactionMerchantEmailFlag) {
		this.refundTransactionMerchantEmailFlag = refundTransactionMerchantEmailFlag;
	}

	public boolean isRetryTransactionCustomeFlag() {
		return retryTransactionCustomeFlag;
	}

	public void setRetryTransactionCustomeFlag(boolean retryTransactionCustomeFlag) {
		this.retryTransactionCustomeFlag = retryTransactionCustomeFlag;
	}

	public boolean isSurchargeFlag() {
		return surchargeFlag;
	}

	public void setSurchargeFlag(boolean surchargeFlag) {
		this.surchargeFlag = surchargeFlag;
	}

	public float getExtraRefundLimit() {
		return extraRefundLimit;
	}

	public void setExtraRefundLimit(float extraRefundLimit) {
		this.extraRefundLimit = extraRefundLimit;
	}

	public String getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	public String getAmexSellerId() {
		return amexSellerId;
	}

	public void setAmexSellerId(String amexSellerId) {
		this.amexSellerId = amexSellerId;
	}

	public String getmCC() {
		return mCC;
	}

	public void setmCC(String mCC) {
		this.mCC = mCC;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public UserStatusType getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatusType userStatus) {
		this.userStatus = userStatus;
	}

	public String getLoginEmailId() {
		return loginEmailId;
	}

	public void setLoginEmailId(String loginEmailId) {
		this.loginEmailId = loginEmailId;
	}

	public UserType getLoginUserType() {
		return loginUserType;
	}

	public void setLoginUserType(UserType loginUserType) {
		this.loginUserType = loginUserType;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

}
