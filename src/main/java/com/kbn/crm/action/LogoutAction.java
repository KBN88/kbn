package com.kbn.crm.action;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmActions;
import com.kbn.crm.actionBeans.SessionCleaner;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class LogoutAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(LogoutAction.class.getName());
	private static final long serialVersionUID = 9203388873112676406L;

	public String logout() {
		try {
			User user = (User) sessionMap.get(Constants.USER.getValue());
			UserDao userDao = new UserDao();
			if(user!=null){
				//updating lastActionName as home 
				user = userDao.findPayId(user.getPayId());
				String lastActionName = CrmActions.HOME.getValue();
				user.setLastActionName(lastActionName);
				userDao.update(user);
				
				MDC.put(Constants.CRM_LOG_USER_PREFIX.getValue(), user.getEmailId());
				logger.info("logged out");
			}
			SessionCleaner.cleanSession(sessionMap);	
			
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}
}
