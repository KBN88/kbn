package com.kbn.crm.action;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.dao.BinRangeDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.util.CommanCsvReader;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.actionBeans.BatchResponseObject;
import com.kbn.crm.actionBeans.BinRange;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class BinRangeManeger extends AbstractSecureAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4486785339968262228L;
	private static Logger logger = Logger.getLogger(BinRangeManeger.class.getName());
	private String fileName;
	private String response;
	private List<Merchants> merchantList = new LinkedList<Merchants>();
	private Map<String, String> currencyMap = new HashMap<String, String>();

	public String execute() {
		BatchResponseObject batchResponseObject = new BatchResponseObject();
		CommanCsvReader commanCsvReader = new CommanCsvReader();
		try {
			// batchFile read line by line
			batchResponseObject = commanCsvReader.csvReaderForBinRange(fileName);
			if (batchResponseObject.getBinRangeResponseList().isEmpty()) {
				addActionMessage(ErrorType.INVALID_FIELD.getResponseMessage());
			} else {
				List<BinRange> binListObj = batchResponseObject.getBinRangeResponseList();
				BinRangeDao binRangeDao = new BinRangeDao();
				response = binRangeDao.insertAll(binListObj);
			}
			addActionMessage(response);
			if (StringUtils.isEmpty(response)) {
				setResponse((CrmFieldConstants.PROCESS_INITIATED_SUCCESSFULLY.getValue()));
				addActionMessage(response);
			}

		} catch (Exception exception) {
			logger.error("Error while processing binRange:" + exception);
			addActionMessage("Error while processing Binranges:" + exception);
		}
		return INPUT;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<Merchants> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<Merchants> merchantList) {
		this.merchantList = merchantList;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}

	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

}
