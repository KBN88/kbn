package com.kbn.crm.action;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 *
 */
public class MerchantMappingDisplay extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(MerchantMappingDisplay.class.getName());
	private static final long serialVersionUID = 8733557567586189516L;
	private String merchantEmailId;	
	private String acquirer;
	private String response;
	private String mappedString;
	private Object currencyString;

	public String execute() {
		try {
			if (acquirer != null && merchantEmailId != null) {
				getMapping();
				return SUCCESS;
			}
		}
		catch(Exception exception){
			logger.error("Exception", exception);	
			setResponse(ErrorType.MAPPING_NOT_FETCHED.getResponseMessage());
			return SUCCESS;
		   }	
		 return SUCCESS;
		}

	private void getMapping() throws SystemException {
		User user = new UserDao().find(merchantEmailId);
		user.getAccounts();
		Account account = user.getAccountUsingAcquirerCode(acquirer);
		if (account == null) {
			setMappedString("");
			return;
		}
		Session session = null;
		try {
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			session.load(account, account.getId());
			setMappedString(account.getMappedString());
			Set<AccountCurrency> accountCurrencySet = account
					.getAccountCurrencySet();
			setCurrencyString(accountCurrencySet);
			tx.commit();
			for (AccountCurrency accountCurrency : accountCurrencySet) {
				if(!StringUtils.isAnyEmpty(accountCurrency.getPassword())){
					String decryptedPassword = AccountPasswordScrambler.decryptPassword(accountCurrency.getPassword());
					accountCurrency.setPassword(decryptedPassword);
				}
			}
			
		} finally {
			HibernateSessionProvider.closeSession(session);
		}
	}

	
	public void validate(){
    	CrmValidator validator = new CrmValidator();

		if ((validator.validateBlankField(getAcquirer()))) {
			addFieldError(CrmFieldType.ACQUIRER.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.ACQUIRER, getAcquirer()))) {
			addFieldError(CrmFieldType.ACQUIRER.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		if ((validator.validateBlankField(getMerchantEmailId()))) {
			addFieldError(CrmFieldType.MERCHANT_EMAIL_ID.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.MERCHANT_EMAIL_ID, getMerchantEmailId()))) {
			addFieldError(CrmFieldType.MERCHANT_EMAIL_ID.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
		if ((validator.validateBlankField(getResponse()))) {		
		} else if (!(validator.validateField(CrmFieldType.RESPONSE, getResponse()))) {
			addFieldError(CrmFieldType.RESPONSE.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
    }

	public String display() {
		return NONE;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	public String getMerchantEmailId() {
		return merchantEmailId;
	}

	public void setMerchantEmailId(String merchantEmailId) {
		this.merchantEmailId = merchantEmailId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getMappedString() {
		return mappedString;
	}

	public void setMappedString(String mappedString) {
		this.mappedString = mappedString;
	}

	public Object getCurrencyString() {
		return currencyString;
	}

	public void setCurrencyString(Object currencyString) {
		this.currencyString = currencyString;
	}
}
