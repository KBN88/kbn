package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.dao.ServiceTaxDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.ServiceTax;
import com.kbn.commons.user.Surcharge;
import com.kbn.commons.user.SurchargeDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Shaiwal
 *
 */
public class BankSurchargeApproveRejectAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(BankSurchargeApproveRejectAction.class.getName());
	private static final long serialVersionUID = -6517340843571949786L;

	private String paymentType;
	private String payId;
	private String mopType;
	private String acquirer;
	private String emailId;
	private String userType;
	private String operation;
	private String response;
	public Date currentDate = new Date();

	public String execute() {
		SurchargeDao surchargeDao = new SurchargeDao();

		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

		if (permissions.toString().contains("Create Surcharge") || userType.equals(UserType.ADMIN.toString())) {

			List<Surcharge> surchargeToUpdate = new ArrayList <Surcharge>();
			
			surchargeToUpdate = surchargeDao.findPendingSurchargeListByPayIdAcquirerName(payId,paymentType,acquirer,mopType);

			if (surchargeToUpdate != null) {

				if (operation.equals("accept")) {
					List<Surcharge> activeSurchargeList = new ArrayList <Surcharge>();
					activeSurchargeList = surchargeDao.findSurchargeListByPayIdAcquirerName(payId,paymentType,acquirer,mopType);

					if (activeSurchargeList != null) {

						for (Surcharge surcharge : activeSurchargeList){
							updateSurcharge(surcharge, TDRStatus.INACTIVE);
						}
					}
					
					for (Surcharge surcharge:surchargeToUpdate){
						updateSurcharge(surcharge, TDRStatus.ACTIVE);
					}
					setResponse(ErrorType.BANK_SURCHARGE_UPDATED.getResponseMessage());
					return SUCCESS;
					
				} else {
					for (Surcharge surcharge:surchargeToUpdate){
						updateSurcharge(surcharge, TDRStatus.REJECTED);
					}
					setResponse(ErrorType.BANK_SURCHARGE_REJECTED.getResponseMessage());
					return SUCCESS;
				}

			}

		}

		return SUCCESS;
	}

	public void updateSurcharge(Surcharge surcharge, TDRStatus status) {

		try {

			Session session = null;
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			Long id = surcharge.getId();
			session.load(surcharge, surcharge.getId());
			Surcharge sch = (Surcharge) session.get(Surcharge.class, id);
			sch.setStatus(status);
			sch.setUpdatedDate(currentDate);
			sch.setProcessedBy(emailId);
			session.update(sch);
			tx.commit();
			session.close();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {

		}

	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getMopType() {
		return mopType;
	}

	public void setMopType(String mopType) {
		this.mopType = mopType;
	}

	public String getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
