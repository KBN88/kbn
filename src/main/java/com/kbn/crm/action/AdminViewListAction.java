package com.kbn.crm.action;

import java.util.List;
import org.apache.log4j.Logger;

import com.kbn.commons.user.MerchantDetails;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class AdminViewListAction extends AbstractSecureAction {

	/**
	 * @author Neeraj
	 */
	private static final long serialVersionUID = 5558565922924803420L;
	private static Logger logger = Logger.getLogger(AdminViewListAction.class.getName());
	private List<MerchantDetails> aaData;

	public String execute() {
		DataEncoder encoder = new DataEncoder();
		try {
			aaData = encoder.encodeMerchantDetailsObj((new UserDao()).getAllAdminList());
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public List<MerchantDetails> getAaData() {
		return aaData;
	}

	public void setAaData(List<MerchantDetails> aaData) {
		this.aaData = aaData;
	}
}
