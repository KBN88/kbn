package com.kbn.crm.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.LineChartService;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class LineChartAction extends AbstractSecureAction{

	/**
	 * 
	 */
	private static Logger logger = Logger.getLogger(LineChartAction.class.getName());
	private static final long serialVersionUID = 7154448717124485623L;
    private List<PieChart> pieChart=new ArrayList<PieChart>();
    private LineChartService getlineChartService=new LineChartService();
    private String emailId;
	private String currency;
	private String dateFrom;
	private String dateTo;
	public String execute() {
		try{
			Calendar date = Calendar.getInstance();
	           date.set(Calendar.DAY_OF_MONTH, 1);
	           DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	              dateFrom=df.format(date.getTime());
	              Calendar cal = Calendar.getInstance();
	              cal.add(Calendar.DAY_OF_MONTH, 1);
	              Date date2 = cal.getTime();
	              dateTo=df.format(date2);      
		UserDao userDao=new UserDao();
		User user=(User) sessionMap.get(Constants.USER.getValue());
		if(user.getUserType().equals(UserType.RESELLER)){
			setPieChart(getlineChartService.prepareResellerlist(user.getPayId(),user.getResellerId(),getCurrency(),getDateFrom(),getDateTo()));
			
			if(getEmailId().equals(CrmFieldConstants.ALL_MERCHANTS.getValue())) {
				setPieChart(getlineChartService.prepareResellerlist(getEmailId(),user.getResellerId(),getCurrency(),getDateFrom(),getDateTo()));
			}
			else {
				setPieChart(getlineChartService.preparelist(userDao.findPayIdByEmail(getEmailId()).getPayId(),getCurrency(),getDateFrom(),getDateTo()));
			}
		 return SUCCESS;
		}
		else if(user.getUserType().equals(UserType.ACQUIRER)){
			setPieChart(getlineChartService.linePreparelist(user.getFirstName(), getCurrency(),getDateFrom(),getDateTo()));
		
		 return SUCCESS;
		}else{
			if(user.getUserType().equals(UserType.MERCHANT)|| user.getUserType().equals(UserType.POSMERCHANT)){
				setPieChart(getlineChartService.preparelist(user.getPayId(),getCurrency(),getDateFrom(),getDateTo()));
			}
			else
			{
				if(getEmailId().equals(CrmFieldConstants.ALL_MERCHANTS.getValue())) {
					setPieChart(getlineChartService.preparelist(getEmailId(), getCurrency(),getDateFrom(),getDateTo()));
				}
				else {
					setPieChart(getlineChartService.preparelist(userDao.findPayIdByEmail(getEmailId()).getPayId(),getCurrency(),getDateFrom(),getDateTo()));
				}
			}
			 return SUCCESS;
		}
	    }catch(Exception exception){
		 logger.error("Exception", exception);
			return ERROR;
	 }

}
	
	public List<PieChart> getPieChart() {
		return pieChart;
	}
	public void setPieChart(List<PieChart> pieChart) {
		this.pieChart = pieChart;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}


	public LineChartService getGetlineChartService() {
		return getlineChartService;
	}


	public void setGetlineChartService(LineChartService getlineChartService) {
		this.getlineChartService = getlineChartService;
	}
	
}
