package com.kbn.crm.action;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.BinCountryMapperType;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.SaltFactory;
import com.kbn.crm.actionBeans.CurrencyMapProvider;
import com.kbn.crm.actionBeans.IndustryTypeCategoryProvider;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.opensymphony.xwork2.ModelDriven;

/**
 * @author Neeraj, Puneet
 *
 */
public class MerchantAccountSetupAction extends AbstractSecureAction implements ModelDriven<User>{

	private static Logger logger = Logger.getLogger(MerchantAccountSetupAction.class.getName());
	private static final long serialVersionUID = -642857372066409390L;

	private User user = new User();
	private String salt;
	private Boolean isFlag;
	private Map<String, String> currencyMap = new LinkedHashMap<String, String>();
	private Map<String,String> industryTypes = new LinkedHashMap<String,String>();

	public String execute() {
		UserDao userDao = new UserDao();
		AccountPasswordScrambler accPwdScrambler = new AccountPasswordScrambler();
		try {
			setUser(userDao.findPayId(user.getPayId()));				
			setUser(accPwdScrambler.retrieveAndDecryptPass(user)); //Decrypt password to display at front end
			setSalt(SaltFactory.getSaltProperty(user));			
			// set currencies
			CurrencyMapProvider currencyMapProvider = new CurrencyMapProvider();
			currencyMap = currencyMapProvider.currencyMap(user);
			//set IndustryTypes 
			IndustryTypeCategoryProvider IndustryTypeCategoryProvider = new IndustryTypeCategoryProvider();
			setIndustryTypes(IndustryTypeCategoryProvider.industryTypes(user));
			if(currencyMap.isEmpty()){
				addFieldError(CrmFieldType.DEFAULT_CURRENCY.getName(),ErrorType.UNMAPPED_CURRENCY_ERROR.getResponseMessage());
				addActionMessage("No currency mapped!!");
				return SUCCESS;
			}
			
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;     
		}
	}

	//to provide default country
	public String getDefaultCountry(){
		if(StringUtils.isBlank(user.getCountry())){
			return BinCountryMapperType.INDIA.getName();
		}else{
			return user.getCountry();
		}
	}

	public void  validate(){
		CrmValidator validator = new CrmValidator();
		if(!validator.validateField(CrmFieldType.PAY_ID, user.getPayId())){
			addFieldError(CrmFieldType.PAY_ID.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
	}
	public String  validatePrimary(){
		Set<Account> accounts = null;
		accounts = user.getAccounts();
		for (Account account : accounts) {
			if(account.isPrimaryStatus()) {
				setIsFlag(true);
				return SUCCESS;
			}
		}
		setIsFlag(false);
		return SUCCESS;
	}
	
	public User getModel() {
		return user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Boolean getIsFlag() {
		return isFlag;
	}

	public void setIsFlag(Boolean isFlag) {
		this.isFlag = isFlag;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}

	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

	public Map<String,String> getIndustryTypes() {
		return industryTypes;
	}

	public void setIndustryTypes(Map<String,String> industryTypes) {
		this.industryTypes = industryTypes;
	}
}
