package com.kbn.crm.action;

import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.dao.ServiceTaxDao;
import com.kbn.commons.user.ServiceTax;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.NotificationGenerator;
import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Shaiwal
 *
 */
public class ServiceTaxApproveRejectAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(ServiceTaxApproveRejectAction.class.getName());
	private static final long serialVersionUID = -6517340843571949786L;

	private String businessType;
	private String emailId;
	private String userType;
	private String operation;
	public Date currentDate = new Date();

	public String execute() {
		ServiceTaxDao serviceTaxDao = new ServiceTaxDao();

		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));

		try{
		if (permissions.toString().contains("Create Service Tax") || userType.equals(UserType.ADMIN.toString())) {

			ServiceTax serviceTaxToUpdate = new ServiceTax();
			serviceTaxToUpdate = serviceTaxDao.findPendingRequest(businessType);
			NotificationGenerator notificationGenerator = new NotificationGenerator();
			User user = (User) sessionMap.get(Constants.USER.getValue());
			
			if (serviceTaxToUpdate != null) {

				if (operation.equals("accept")) {
					ServiceTax activeServiceTax = new ServiceTax();
					activeServiceTax = serviceTaxDao.findServiceTax(businessType);

					if (activeServiceTax != null) {

						updateServiceTax(activeServiceTax, TDRStatus.INACTIVE);
					}
					updateServiceTax(serviceTaxToUpdate, TDRStatus.ACTIVE);
					
					
					String Message = "Service tax update request approved for Industry Type " + businessType +" by "+ user.getEmailId();
					notificationGenerator.UpdateApproveRejectNotificationForServiceTax(user, Message,businessType,serviceTaxToUpdate.getRequestedBy(),"accept");
					
				} else {
					String Message = "Service tax update request rejected for Industry type " + businessType +" by " + user.getEmailId();
					updateServiceTax(serviceTaxToUpdate, TDRStatus.REJECTED);
					notificationGenerator.UpdateApproveRejectNotificationForServiceTax(null, Message,businessType,serviceTaxToUpdate.getRequestedBy(),"reject");
				}

			}

		}
		
		else{
			addActionMessage("Denied operation , contact Admin");
		}
		}
		catch(Exception e){
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public void updateServiceTax(ServiceTax serviceTax, TDRStatus status) {

		try {

			Session session = null;
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			Long id = serviceTax.getId();
			session.load(serviceTax, serviceTax.getId());
			ServiceTax sTax = (ServiceTax) session.get(ServiceTax.class, id);
			sTax.setStatus(status);
			sTax.setUpdatedDate(currentDate);
			sTax.setProcessedBy(emailId);
			session.update(sTax);
			tx.commit();
			session.close();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {

		}

	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

}
