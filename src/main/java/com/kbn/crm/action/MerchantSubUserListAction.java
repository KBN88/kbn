package com.kbn.crm.action;

import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.MerchantGridViewService;
import com.kbn.commons.user.MerchantDetails;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.DataEncoder;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class MerchantSubUserListAction extends AbstractSecureAction {
	/**
	 * Neeraj
	 */
	private static final long serialVersionUID = -3220279696930666685L;
	private static Logger logger = Logger.getLogger(MerchantSubUserListAction.class.getName());
	private List<MerchantDetails> aaData;
	private User sessionUser = new User();
	private String payId;
	@Override
	public String execute() {
		DataEncoder encoder = new DataEncoder();
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		try {	
			if(sessionUser.getUserType().equals(UserType.ADMIN) || sessionUser.getUserType().equals(UserType.SUBADMIN)){
				if(payId.equals("ALL")){
					aaData = encoder.encodeMerchantDetailsObj((new MerchantGridViewService()).getAllMerchantSubUser());
				}else{
					aaData = encoder.encodeMerchantDetailsObj((new MerchantGridViewService()).getAllMerchantSubUserList(getPayId()));
				}
			}
			
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public List<MerchantDetails> getaaData() {
		return aaData;
	}

	public void setaaData(List<MerchantDetails> setaaData) {
		this.aaData = setaaData;
	}
}
