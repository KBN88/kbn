package com.kbn.crm.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.PendingResellerMappingDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.crm.actionBeans.MerchantMappingFactory;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * Neeraj, Rahul
 */
public class ResellerMappingAction extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(ResellerMappingAction.class.getName());
	private static final long serialVersionUID = -6604356643967237232L;
	private List<Merchants> listMerchant = new ArrayList<Merchants>();
	private List<Merchants> listReseller = new ArrayList<Merchants>();
	private List<Merchants> resellerMerchantList = new ArrayList<Merchants>();
	private Map<String, List<Merchants>> resellerList = new HashMap<String, List<Merchants>>();
	private String emailId;
	private String reseller;
	private String userType;
	private String response;
	private List <Merchants> merchantList = new ArrayList<Merchants>();
	UserDao userDao = new UserDao();

	@SuppressWarnings("unchecked")
	public String execute() {
		UserDao userDao = new UserDao();
		setListMerchant(userDao.getMerchantList());
		setListReseller(userDao.getResellerList());
		return INPUT;
	}

	@SuppressWarnings("unchecked")
	public String resellerMappingSave() {
		StringBuilder permissions = new StringBuilder();
		permissions.append(sessionMap.get(Constants.USER_PERMISSION.getValue()));
		try {
			if (emailId != null && !emailId.equals("") && reseller != null && !reseller.equals("") && userType != null
					&& !userType.equals("")) {
				
				boolean existingMappingRequest = new PendingResellerMappingDao().checkExistingMappingRequest(emailId, reseller);

				// If user is neither Admin nor has reseller mapping authorization , check if pending request is already pending
				if((!userType.equals(UserType.ADMIN.toString()) && !permissions.toString().contains("Create Reseller Mapping")))
				{
					if(existingMappingRequest ){
						setResponse("Reseller Mapping request already pending for this reseller and merchant");
						return SUCCESS;
					}
				}
				
				User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
				MerchantMappingFactory merchantMappingFactory = new MerchantMappingFactory();
				setResellerList(merchantMappingFactory.getMerchantMappingFactory(emailId, reseller, userType,
						permissions, sessionUser));
				setListMerchant(userDao.getMerchantList());
				setListReseller(userDao.getResellerList());
				if (userType.equals(UserType.ADMIN.toString()) || permissions.toString().contains("Create Reseller Mapping")) {
					setResponse("Reseller details updated");
				}
				else{
					setResponse("Reseller mapping update request sent");
					
				}
			} else {
				setResponse("Something went wrong , reseller details not updated");
			}
		} catch (Exception exception) {
			setResponse("Something went wrong, reseller details not updated");
			logger.error(exception);

		}
		return SUCCESS;

	}

	public String mappedMerchantList() {
		UserDao userDao = new UserDao();
		User userReseller = userDao.find(reseller);
		String resellerPayId = userReseller.getPayId();

		setMerchantList(userDao.getActiveResellerMerchants(resellerPayId));
		setResellerMerchantList(merchantList);
		setResponse("Success");
		return SUCCESS;

	}

	public List<Merchants> getListMerchant() {
		return listMerchant;
	}

	public void setListMerchant(List<Merchants> listMerchant) {
		this.listMerchant = listMerchant;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public List<Merchants> getListReseller() {
		return listReseller;
	}

	public void setListReseller(List<Merchants> listReseller) {
		this.listReseller = listReseller;
	}

	public Map<String, List<Merchants>> getResellerList() {
		return resellerList;
	}

	public void setResellerList(Map<String, List<Merchants>> resellerList) {
		this.resellerList = resellerList;
	}

	public String getReseller() {
		return reseller;
	}

	public void setReseller(String reseller) {
		this.reseller = reseller;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<Merchants> getResellerMerchantList() {
		return resellerMerchantList;
	}

	public void setResellerMerchantList(List<Merchants> resellerMerchantList) {
		this.resellerMerchantList = resellerMerchantList;
	}

	public List<Merchants> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<Merchants> merchantList) {
		this.merchantList = merchantList;
	}

}
