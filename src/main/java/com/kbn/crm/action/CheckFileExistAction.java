package com.kbn.crm.action;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.DocumentNameConstants;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class CheckFileExistAction extends AbstractSecureAction{

	/**
	 * @Isha
	 */
	private static final long serialVersionUID = 2210846014694727792L;
	
	private static Logger logger = Logger
			.getLogger(CheckFileExistAction.class.getName());
	private String destPath;
	private String fileName;
	private String payId;
	private String fileNameStruts;
	private String fileFormat;
	
	public List<String> docName;
	private String[] fileFormatList;
	public String execute(){
	try
	{
		
		PropertiesManager propertiesManager = new PropertiesManager();
		
		setFileFormat(propertiesManager.getSystemProperty("FileFormat"));
		
		User user = (User) sessionMap.get(Constants.USER);
		if (user.getUserType().equals(UserType.ADMIN) || user.getUserType().equals(UserType.SUPERADMIN)) {
			setPayId(getPayId());
		}
		else
		{
			setPayId(user.getPayId());
		}
		 setDestPath(propertiesManager.getSystemProperty("DocumentPath")+ getPayId()+"/");
		 docName = DocumentNameConstants.getDocType();  ///check type of doc
		 StringBuilder stringBuilder = new StringBuilder();
		
		 fileFormatList = getFileFormat().split(",");	 //ext of doc
		 for (int i=0;i<docName.size();i++) {
			for (int j=0;j<fileFormatList.length;j++)
			{
				
				File file = new File(makeFileName(docName.get(i),fileFormatList[j]));
				if(file.exists()){
					stringBuilder.append(docName.get(i).toString());
					stringBuilder.append(",");
						}
			}
			
		}
		 if (stringBuilder.length()>0){
			 setFileName(stringBuilder.toString().substring(0,stringBuilder.length()-1));
		 	}
		
		
		  return SUCCESS;
	} catch (Exception exception) {
		logger.error("Exception", exception);
		return ERROR;
	}
	}
	
	private  String makeFileName(String docName,String extension){
		String name;
		name=getDestPath()+getPayId()+"_"+ docName+extension;
		
		return name;
		
	}
	public String getDestPath() {
		return destPath;
	}
	public void setDestPath(String destPath) {
		this.destPath = destPath;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public String getFileNameStruts() {
		return fileNameStruts;
	}
	public void setFileNameStruts(String fileNameStruts) {
		this.fileNameStruts = fileNameStruts;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}


}
