package com.kbn.crm.action;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.dao.HibernateSessionProvider;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.PermissionType;
import com.kbn.commons.user.Permissions;
import com.kbn.commons.user.Roles;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.UserStatusType;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class SubUserEditAction extends AbstractSecureAction {

	/**
	 * @ Neeraj
	 */
	private static final long serialVersionUID = -2429379754814283308L;
	private static Logger logger = Logger.getLogger(SubUserEditAction.class.getName());
	private String firstName;
	private String lastName;
	private String mobile;
	private String emailId;
	private Boolean isActive;

	private List<String> lstPermissionType;
	private List<PermissionType> listPermissionType;
	private String permissionString = "";

	public String editSubUser() {
		try {

			User user = new User();
			UserDao userDao = new UserDao();
			user = userDao.find(getEmailId());

			editPermission(user);
			user.setFirstName(getFirstName());
			user.setLastName(getLastName());
			user.setMobile(getMobile());

			if (getIsActive()) {
				user.setUserStatus(UserStatusType.ACTIVE);
			} else {
				user.setUserStatus(UserStatusType.PENDING);
			}
			userDao.update(user);
			addActionMessage(CrmFieldConstants.USER_DETAILS_UPDATED.getValue());
			return INPUT;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	private void editPermission(User user) {
		Session session = null;
		User sessionUser = null;
		sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		try {
			session = HibernateSessionProvider.getSession();
			Transaction tx = session.beginTransaction();
			session.load(user, user.getEmailId());

			Set<Roles> roles = user.getRoles();
			Iterator<Roles> itr = roles.iterator();
			Roles role = new Roles();
			if (!roles.isEmpty()) {
				role = itr.next();
				Iterator<Permissions> permissionIterator = role.getPermissions().iterator();
				while (permissionIterator.hasNext()) {
					// not used but compulsory for iterator working
					@SuppressWarnings("unused")
					Permissions permission = permissionIterator.next();
					permissionIterator.remove();
				}
			}
			if (sessionUser.getUserType().equals(UserType.MERCHANT)) {
				setListPermissionType(PermissionType.getSubUserPermissionType());
				if (lstPermissionType == null) {

				} else {
					for (String permissionType : lstPermissionType) {
						Permissions permission = new Permissions();
						permission.setPermissionType(PermissionType.getInstanceFromName(permissionType));
						role.addPermission(permission);
					}
				}
			} else if (sessionUser.getUserType().equals(UserType.ACQUIRER)) {
				setListPermissionType(PermissionType.getSubAcquirerPermissionType());
				if (lstPermissionType == null) {

				} else {
					for (String permissionType : lstPermissionType) {
						Permissions permission = new Permissions();
						permission.setPermissionType(PermissionType.getInstanceFromName(permissionType));
						role.addPermission(permission);
					}
				}
			}

			// set permission string
			getPermissions(user);
			tx.commit();
		} finally {
			HibernateSessionProvider.closeSession(session);
		}
	}

	private void getPermissions(User agent) {
		Set<Roles> roles = agent.getRoles();
		Set<Permissions> permissions = roles.iterator().next().getPermissions();
		if (!permissions.isEmpty()) {
			StringBuilder perms = new StringBuilder();
			Iterator<Permissions> itr = permissions.iterator();
			while (itr.hasNext()) {
				PermissionType permissionType = itr.next().getPermissionType();
				perms.append(permissionType.getPermission());
				perms.append("-");
			}
			perms.deleteCharAt(perms.length() - 1);
			setPermissionString(perms.toString());
		}
	}

	public void validate() {
		
		User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
		if (sessionUser.getUserType().equals(UserType.MERCHANT)) {
			setListPermissionType(PermissionType.getSubUserPermissionType());
		} else if (sessionUser.getUserType().equals(UserType.ACQUIRER)) {
			setListPermissionType(PermissionType.getSubAcquirerPermissionType());
		}
		CrmValidator validator = new CrmValidator();

		if ((validator.validateBlankField(getFirstName()))) {
			addFieldError(CrmFieldType.FIRSTNAME.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.FIRSTNAME, getFirstName()))) {
			addFieldError(CrmFieldType.FIRSTNAME.getName(), validator.getResonseObject().getResponseMessage());
		}

		if ((validator.validateBlankField(getLastName()))) {
			addFieldError(CrmFieldType.LASTNAME.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.LASTNAME, getLastName()))) {
			addFieldError(CrmFieldType.LASTNAME.getName(), validator.getResonseObject().getResponseMessage());
		}

		if (validator.validateBlankField(getMobile())) {
			addFieldError(CrmFieldType.MOBILE.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.validateField(CrmFieldType.MOBILE, getMobile()))) {
			addFieldError(CrmFieldType.MOBILE.getName(), validator.getResonseObject().getResponseMessage());
		}

		if (validator.validateBlankField(getEmailId())) {
			addFieldError(CrmFieldType.EMAILID.getName(), validator.getResonseObject().getResponseMessage());
		} else if (!(validator.isValidEmailId(getEmailId()))) {
			addFieldError(CrmFieldType.EMAILID.getName(), ErrorType.INVALID_FIELD_VALUE.getResponseMessage());
		}
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public List<String> getLstPermissionType() {
		return lstPermissionType;
	}

	public void setLstPermissionType(List<String> lstPermissionType) {
		this.lstPermissionType = lstPermissionType;
	}

	public List<PermissionType> getListPermissionType() {
		return listPermissionType;
	}

	public void setListPermissionType(List<PermissionType> listPermissionType) {
		this.listPermissionType = listPermissionType;
	}

	public String getPermissionString() {
		return permissionString;
	}

	public void setPermissionString(String permissionString) {
		this.permissionString = permissionString;
	}
}
