package com.kbn.crm.action;

import java.util.LinkedHashMap;
import java.util.Map;

import com.kbn.commons.util.BusinessType;
import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 *
 */
public class DisplaySignupPage extends AbstractSecureAction {

	private static final long serialVersionUID = 3472447203584744945L;
	private Map<String,String> industryCategory = new LinkedHashMap<String,String>();

	public String execute(){
		industryCategory = BusinessType.getIndustryCategoryList();
		return INPUT;
	}

	public Map<String,String> getIndustryCategory() {
		return industryCategory;
	}

	public void setIndustryCategory(Map<String,String> industryCategory) {
		this.industryCategory = industryCategory;
	}
}
