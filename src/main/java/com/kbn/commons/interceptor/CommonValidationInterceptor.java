package com.kbn.commons.interceptor;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.util.CrmValidator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * @author Puneet
 * 
 */

public class CommonValidationInterceptor extends AbstractInterceptor {

	private static Logger logger = Logger.getLogger(CommonValidationInterceptor.class.getName());
	private static final long serialVersionUID = -8412279724011707370L;
	
	@SuppressWarnings("unchecked")
	@Override
	public String intercept(ActionInvocation actionInvocation) {
		
		CrmValidator validator = new CrmValidator();
		HttpServletRequest	request = ServletActionContext.getRequest();
		Map<String,String[]> requestMap = request.getParameterMap();
		try {
			for (Entry<String, String[]> httpRequestentry : requestMap.entrySet()) {				
				for(String parameter:httpRequestentry.getValue()){
					parameter = validator.validateRequestParameter(parameter);
				}			
			}
			return actionInvocation.invoke();			
		}catch (ClassCastException classCastException) {
			logger.error("Class cast exception", classCastException);
			return Action.ERROR;
		}catch (Exception exception) {
			logger.error("Exception", exception);
			return Action.ERROR;
		}	
	}	
}
