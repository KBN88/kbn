package com.kbn.commons.interceptor;

import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.PropertiesManager;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class SubAdminPermissionInterceptor extends AbstractInterceptor {

	/**
	 * @ Neeraj
	 * 
	 */
	private static final long serialVersionUID = 4146811630295786099L;
	private static Logger logger = Logger.getLogger(SubAdminPermissionInterceptor.class.getName());

	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
		try {
			PropertiesManager prop = new PropertiesManager();
			Map<String, Object> sessionMap = actionInvocation.getInvocationContext().getSession();
			Object userObject = sessionMap.get(Constants.USER.getValue());
			if (null == userObject) {
				return Action.LOGIN;
			}

			User user = (User) userObject;
			if (user.getUserType().equals(UserType.SUBADMIN)) {
				String permissionString = sessionMap.get("USER_PERMISSION").toString();
				String actionName = actionInvocation.getProxy().getActionName();

				if (prop.getSuAdminPermissionProperty(CrmFieldConstants.VIEW_TRANSACTIONS.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.VIEW_TRANSACTIONS.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.VIEW_REPORTS.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.VIEW_REPORTS.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.CREATE_INVOICE.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.CREATE_INVOICE.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.VIEW_INVOICE.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.VIEW_INVOICE.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.VIEW_REMITTANCE.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.VIEW_REMITTANCE.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.VIEW_MERCHANT_SETUP.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.VIEW_MERCHANT_SETUP.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.CREATE_MAPPING.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.CREATE_MAPPING.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.VIEW_ANALYTICS.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.VIEW_ANALYTICS.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.VIEW_SEARCH_PAYMENT.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.VIEW_SEARCH_PAYMENT.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.VIEW_RECONCILIATION.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.VIEW_RECONCILIATION.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.CREATE_BATCH_OPERATION.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.CREATE_BATCH_OPERATION.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.FRAUD_PREVENTION.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.FRAUD_PREVENTION.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.CREATE_BULK_EMAIL.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.CREATE_BULK_EMAIL.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.VIEW_CASHBACK.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.VIEW_CASHBACK.getValue())) {
						return Action.LOGIN;
					}
				} else if (prop.getSuAdminPermissionProperty(CrmFieldConstants.CREATE_HELPTIKECT.name())
						.contains(actionName)) {
					if (!permissionString.contains(CrmFieldConstants.CREATE_HELPTIKECT.getValue())) {
						return Action.LOGIN;
					}
				}
			}
			return actionInvocation.invoke();
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return Action.ERROR;
		}
	}

}
