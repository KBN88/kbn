package com.kbn.commons.interceptor;

import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.UserStatusType;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * @author Puneet
 * 
 */

public class SessionValidationIntercepter extends AbstractInterceptor {

	private static Logger logger = Logger.getLogger(SessionValidationIntercepter.class.getName());
	private static final long serialVersionUID = 6475203929530326688L;

	@Override
	public String intercept(ActionInvocation actionInvocation) {
		try {
			Map<String, Object> sessionMap = actionInvocation
					.getInvocationContext().getSession();
			Object userObject = sessionMap.get(Constants.USER.getValue());

			if (null == userObject) {
				return Action.LOGIN;
			}

			User user = (User) userObject;
			if(!(user.getUserStatus().equals(UserStatusType.ACTIVE)
					|| user.getUserStatus().equals(UserStatusType.TRANSACTION_BLOCKED))){
				return Action.LOGIN;
			}

			return actionInvocation.invoke();
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return Action.ERROR;
		}
	}
}
