package com.kbn.commons.interceptor;

import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
/**
 * @author Puneet
 * 
 */

public class AdminAuthorizationInterceptor extends AbstractInterceptor {
	private static Logger logger = Logger.getLogger(AdminAuthorizationInterceptor.class.getName());
	private static final long serialVersionUID = 852707981164914179L;

	@Override
	public String intercept(ActionInvocation actionInvocation) {
		try {
			Map<String, Object> sessionMap = actionInvocation
					.getInvocationContext().getSession();
			Object userObject = sessionMap.get(Constants.USER.getValue());

			if (null == userObject) {
				return Action.LOGIN;
			}

			User user = (User) userObject;
		if(user.getUserType().equals(UserType.ADMIN)){
			return actionInvocation.invoke();
		}else if(user.getUserType().equals(UserType.SUBADMIN)){
			//check permsiion code comes here
			return actionInvocation.invoke();
		}
		return Action.LOGIN;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return Action.ERROR;
		}
	}
}
