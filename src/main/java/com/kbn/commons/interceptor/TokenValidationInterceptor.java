package com.kbn.commons.interceptor;

import java.util.Map;
import org.apache.log4j.Logger;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * @author Puneet
 *
 */
public class TokenValidationInterceptor extends AbstractInterceptor {

	private static Logger logger = Logger
			.getLogger(TokenValidationInterceptor.class.getName());
	private static final long serialVersionUID = 3443707353188932224L;
	private static final String FRONT_END_NAME = "token";
	public static final String SERVER_END_NAME = "customToken";
	public static final String INVALID_TOKEN_CODE = "invalid.token";

	@SuppressWarnings("rawtypes")
	@Override
	public String intercept(ActionInvocation invocation) {
		try {

			Map<String, Object> params = ActionContext.getContext()
					.getParameters();

			Object tokenObjectFE = params.get(FRONT_END_NAME);
			String tokenFE = ((String[]) tokenObjectFE)[0];
			if (null == tokenFE) {
				return INVALID_TOKEN_CODE;
			}
			Map session = ActionContext.getContext().getSession();
			String sessionToken = (String) session.get(SERVER_END_NAME);

			if (!tokenFE.equals(sessionToken)) {
				return INVALID_TOKEN_CODE;
			}
			return invocation.invoke();
		} catch (Exception exception) {
			logger.error(exception);
			return Action.ERROR;
		}
	}
}
