/**
 * 
 */
package com.kbn.commons.interceptor;

import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmActions;
import com.kbn.commons.util.CrmFieldConstants;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * @author Harpreet
 *
 */
public class StateSaveInterceptor extends AbstractInterceptor {
	 
	private static final long serialVersionUID = -7169667296028325138L;
	private static Logger logger = Logger.getLogger(StateSaveInterceptor.class.getName());

 
	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
		 
		try {
			Map<String, Object> sessionMap = actionInvocation
					.getInvocationContext().getSession();
			String lastActionName = ActionContext.getContext().getName();
			
			//checking action name from white List (CrmActions)
			CrmActions[] whiteActions = CrmActions.values();
			for(CrmActions action:whiteActions){
				if(action.getValue().equalsIgnoreCase(lastActionName)){
					// adding session attribute to save last activity state
					sessionMap.put(CrmFieldConstants.LAST_ACTION_NAME.getValue(), lastActionName);
					break;
				}
			}

			return actionInvocation.invoke();
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return Action.ERROR;
		}
	}

	
	
}
