package com.kbn.commons.crypto;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;

public interface CryptoManager {
	public void secure(Fields fields) throws SystemException;

	public void removeSecureFields(Fields fields);

	public void hashCardDetails(Fields fields) throws SystemException;
	
	public void encryptCardDetails(Fields fields);
	
	public String maskCardNumber(String cardNumber);
}
