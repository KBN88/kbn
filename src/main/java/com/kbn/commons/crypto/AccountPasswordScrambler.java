package com.kbn.commons.crypto;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.SystemConstants;

public class AccountPasswordScrambler {

	private static Logger logger = Logger.getLogger(AccountPasswordScrambler.class.getName());

	public User retrieveAndDecryptPass(User user){
		Set<Account> accounts = user.getAccounts();

		for(Account account:accounts){
			Set<AccountCurrency>accountCurrencySet = account.getAccountCurrencySet();
			for(AccountCurrency accountCurrency:accountCurrencySet){
				if(!StringUtils.isAnyEmpty(accountCurrency.getPassword())){
					String decryptedPassword = decryptPassword(accountCurrency.getPassword());
					accountCurrency.setPassword(decryptedPassword);
				}
			}			
		}		
		return user;
	}


	public static String encrpytPwd(String password){
		try {
			KeyProvider keyProvider = KeyProviderFactory.getKeyProvider();
			Scrambler scrambler = new Scrambler(keyProvider.getKey(SystemConstants.DEFAULT_KEY_ID));
			return scrambler.encrypt(password);
		} catch (Exception exception) {
			MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(), Constants.CRM_LOG_PREFIX.getValue());
			logger.error("Unable to encrypt password", exception);
		}
		return password;

	}

	public static String decryptPassword(String encryptedString){
		KeyProvider keyProvider = KeyProviderFactory.getKeyProvider();
		Scrambler scrambler = new Scrambler(keyProvider.getKey(SystemConstants.DEFAULT_KEY_ID));
		return scrambler.decrypt(encryptedString);
	}

}
