package com.kbn.commons.crypto;

import java.security.PublicKey;

public class AxisEncrypt {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String checksum = "l9pW5FhoBYBiOpKHjmeTNO0kD/oLSZco8Bl4lQaSP96LwPO6MbzKvq+bBy1Q+vZyiCLJGoDCd+wekZxjZuJW6fM0zjjrDBOCN/ImDv4a89vFsg6GOGvptXH0NuIa3bQ/JOBsazGH+ENiLRYO9yKTX5zpTUPXsalPktonCgPb/IVkqypZXKcP4sFWG74pzIPnjm/+bK8Nb+wzgLuqj1Tp7uoZcMESQhTGtGFAGmRi3CiaJKwAhPvEbHoCFTtB+jMTqsrHfCwYgAOPlMXy+2Zdgi/5HNgYTdImnGS5DZJyTYx0i7cfrWZMd+SMnK+KaPfb9mVc7EkgTxnRdPVKixIhHv8GTCutf/MMwu7ttCPWlZQR2+lRXTzjoHTkzx+x85w77cYpN7mEK0gI7cKiINhDm2LYNmfpUPTyAExDgXARg4dsieDxC5FXqhyK+9R8aIFg5zrZIUINA7RVAYS1nCcRXnO/Rbb5wa0m/cAW5XpfKld6FzOA7Ld5sAZTRPleARgvHvEvK2GymXFCZu7qCaGOq4gaW8xt7suFRVWKbc9dHbGGK86+hgbbpjFIhQy+rVFoEz1Xkrqywx4ETxHwZhqFBj6RAa4VnZMsgNw28KMcLYLRmBknY+Ztkwwbh8f+1zwBWDLbbO+RsicaY4bdrHwlrebYtRkBUfZiuenG8n/5mE9dfWh1hHioL7sg7JwSzLpWprot7cr5uR1KDpysMwxZQSsp2Wnh/CvFm0T1qYSK04G+paIJGdntVrkxXiyHIyiJXduSDdrKVUkVVRgr+N0KNNDtBKOft4VWxQO3Wgy7PtP+rLGswTwf/9UNoUapJSiqwJ0SR0/iU+JibKMensslry3ONLGott5LGbIuqz4cnc/aQEwctCrVG2iWwF+CWgc2am3ktJucRGWVwONCPsKr5iU13OuEtS2DRVgsHKzyekS+A0vppkxt3gLes/xmkl3Uxwmsx3iaPuPG2qQqhZ46gdlQKoRk/VfBSB14K6aTnfockskmh0ovVE3pG7Qkk9D5WsQHfGoL7TteKH3ds1ZI07KpN9EUGEdg4nnjM8WNyEPuaaJ0FFkyiV5Ob+Zbj1sm9zx74Yvc4Smf4fByErLq6JovXEdFit73Grxyndq3WbEd+FR/OqrHX3gLnaI55htHQOQsDQZN/04GbhDGvRxkwboolTijJ5NPbJGmZhyRxDEvdsyykHlsC9AOKVOYcGXx";
	/*	String merchid = "CollectTest";
		String merchchanid = "CollectApp";
		String unqtxnid = "ajshakjhsjkhaksjjgjahgsjgakjgsjajs";
		String unqcustid = "919773187515";
		String orderid = "Gold";
		String amount = "12.00";
		String txndtl = "pay now";
		String currency = "INR";
		String customervpa = "bhartipay@axis";
		String expiry = "30";
		String sId = "123";

		checksum = merchid + merchchanid + unqtxnid + unqcustid + orderid + amount + txndtl + currency + customervpa
				+ expiry + sId;*/
       //System.out.println("CheckSum: "+ encryptKEY2(checksum,""));

	}

	public static String encryptKEY2(String encrypteddata, String publicKey1) {
		RsaEncrDecryption rsa = new RsaEncrDecryption();
		String encrypStrReadbleFormat = encrypteddata;
		try {
			PublicKey publicKey = rsa.readPublicKey(publicKey1);

			byte[] encryptValue = RsaEncrDecryption.encrypt(encrypStrReadbleFormat, publicKey);
			encrypStrReadbleFormat = javax.xml.bind.DatatypeConverter.printHexBinary(encryptValue);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("EncrypStr In hexaDecimal from generated checksum : " + encrypStrReadbleFormat);
		}
		return encrypStrReadbleFormat;
	}

}
