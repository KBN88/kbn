package com.kbn.commons.crypto;

import java.security.MessageDigest;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.codec.binary.Hex;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;

public class Hasher {

	public Hasher() {
	}

	public static String getHash(String input) throws SystemException {
		String response = null;

		MessageDigest messageDigest = MessageDigestProvider.provide();
		messageDigest.update(input.getBytes());
		MessageDigestProvider.consume(messageDigest);

		response = new String(Hex.encodeHex(messageDigest.digest()));

		return response.toUpperCase();
	}// getSHA256Hex()

	public static String getHash(Fields fields) throws SystemException {

		// Append salt of merchant
		String salt = (new PropertiesManager()).getSalt(fields
				.get(FieldType.PAY_ID.getName()));
		if (null == salt) {
			  fields.put(FieldType.RESPONSE_CODE.getName(),
						ErrorType.INVALID_PAYID_ATTEMPT.getCode());
			throw new SystemException(ErrorType.INVALID_PAYID_ATTEMPT,
					"Invalid " + FieldType.PAY_ID.getName());
		}	 
		// Sort the request map
		Fields internalFields = fields.removeInternalFields();
		Map<String, String> treeMap = new TreeMap<String, String>(
				fields.getFields());
		fields.put(internalFields);
		
		// Calculate the hash string
		StringBuilder allFields = new StringBuilder();
		for (String key : treeMap.keySet()) {
			allFields.append(ConfigurationConstants.FIELD_SEPARATOR.getValue());
			allFields.append(key);
			allFields.append(ConfigurationConstants.FIELD_EQUATOR.getValue());
			allFields.append(fields.get(key));
		}

		allFields.deleteCharAt(0); // Remove first FIELD_SEPARATOR
		allFields.append(salt);

		// Calculate hash at server side
		return getHash(allFields.toString());
	}
}
