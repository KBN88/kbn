package com.kbn.commons.crypto;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.kbn.commons.util.SystemConstants;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * @author Surender
 *
 */
public class Scrambler {

	public static final String ALGO = "AES";
	private String key = null;
	private Key keyObj = null;
	
	public Scrambler(String key){
		this.key = key;
		this.keyObj = new SecretKeySpec(key.getBytes(), ALGO);
	}
	
	public String encrypt(String data){
		try {
			IvParameterSpec iv = new IvParameterSpec(key.getBytes(SystemConstants.DEFAULT_ENCODING_UTF_8));
			Cipher cipher = Cipher.getInstance(ALGO + "/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, keyObj, iv);
			
			byte[] encValue = cipher.doFinal(data.getBytes(SystemConstants.DEFAULT_ENCODING_UTF_8));
			
			return new BASE64Encoder().encode(encValue);
		} catch (NoSuchAlgorithmException noSuchAlgorithmException) {

			noSuchAlgorithmException.printStackTrace();
		} catch (NoSuchPaddingException noSuchPaddingException) {

			noSuchPaddingException.printStackTrace();
		} catch (InvalidKeyException invalidKeyException) {

			invalidKeyException.printStackTrace();
		} catch (IllegalBlockSizeException illegalBlockSizeException) {

			illegalBlockSizeException.printStackTrace();
		} catch (BadPaddingException badPaddingException) {

			badPaddingException.printStackTrace();
		} catch (InvalidAlgorithmParameterException invalidAlgorithmParameterException) {

			invalidAlgorithmParameterException.printStackTrace();
		} catch (UnsupportedEncodingException unsupportedEncodingException) {

			unsupportedEncodingException.printStackTrace();
		}
		
		//Ideally this should be a non reachable code
		return null;
	}
	
	public String decrypt(String encryptedData){
		try {
			IvParameterSpec iv = new IvParameterSpec(key.getBytes(SystemConstants.DEFAULT_ENCODING_UTF_8));
			Cipher cipher = Cipher.getInstance(ALGO + "/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, keyObj, iv);
			
			byte[] decodedData = new BASE64Decoder().decodeBuffer(encryptedData);
			byte[] decValue = cipher.doFinal(decodedData);
			
			return new String(decValue);
			
		} catch (NoSuchAlgorithmException noSuchAlgorithmException) {

			noSuchAlgorithmException.printStackTrace();
		} catch (NoSuchPaddingException noSuchPaddingException) {

			noSuchPaddingException.printStackTrace();
		} catch (InvalidKeyException invalidKeyException) {

			invalidKeyException.printStackTrace();
		} catch (IOException ioException) {

			ioException.printStackTrace();
		} catch (IllegalBlockSizeException illegalBlockSizeException) {

			illegalBlockSizeException.printStackTrace();
		} catch (BadPaddingException badPaddingException) {

			badPaddingException.printStackTrace();
		} catch (InvalidAlgorithmParameterException invalidAlgorithmParameterException) {

			invalidAlgorithmParameterException.printStackTrace();
		}
		
		//Ideally this will be a non reachable code
		return null;
	}
}
