package com.kbn.commons.crypto;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;


public class DefaultCryptoManager implements CryptoManager {
	public static final String MASK_START_CHARS = "-XXXX-XXXX-";
	private static final KeyProvider keyProvider = KeyProviderFactory.getKeyProvider();

	public DefaultCryptoManager() {
	}

	public void secure(Fields fields) throws SystemException {
		try {
			hashCardDetails(fields);
			
		} finally {

			removeSecureFields(fields);
		}
	}

	public void hashCardDetails(Fields fields) throws SystemException {
		String cardNumber = fields.get(FieldType.CARD_NUMBER.getName());
		if (null != cardNumber) {
			fields.put(FieldType.H_CARD_NUMBER.getName(),
					Hasher.getHash(cardNumber));
		}
	}

	public void removeSecureFields(Fields fields) {
		// Remove CVV, if present - Do not ever store CVV
		fields.remove(FieldType.CVV.getName());

		fields.remove(FieldType.CARD_EXP_DT.getName());
		
		CryptoUtil.truncateCardNumber(fields);
	}

	public void encryptCardDetails(Fields fields) {
		String key = keyProvider.getKey(fields);
		Scrambler scrambler = new Scrambler(key);

		// Encrypt Card number
		String cardNumber = fields.get(FieldType.CARD_NUMBER.getName());
		if (null != cardNumber) {
			cardNumber = scrambler.encrypt(cardNumber);
			fields.put(FieldType.S_CARD_NUMBER.getName(), cardNumber);
		}

		// Encrypt Expiry date
		String expiryDate = fields.get(FieldType.CARD_EXP_DT.getName());
		if (null != expiryDate) {
			expiryDate = scrambler.encrypt(expiryDate);
			fields.put(FieldType.S_CARD_EXP_DT.getName(), expiryDate);
		}
	}
	
	public String maskCardNumber(String cardNumber){
		StringBuilder mask = new StringBuilder();
		mask.append(cardNumber.subSequence(0, 4));
		mask.append(MASK_START_CHARS);
		mask.append(cardNumber.substring(cardNumber.length() - 4));
		
		return mask.toString();
	}
}
