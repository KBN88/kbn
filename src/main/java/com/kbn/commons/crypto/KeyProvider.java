package com.kbn.commons.crypto;

import com.kbn.commons.util.Fields;

public interface KeyProvider {
	public String getKey(Fields fields);
	
	public String getKey(String keyId);
}
