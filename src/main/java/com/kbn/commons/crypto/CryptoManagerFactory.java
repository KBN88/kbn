package com.kbn.commons.crypto;

public class CryptoManagerFactory {

	public CryptoManagerFactory() {
	}
	
	public static CryptoManager getCryptoManager(){
		return new DefaultCryptoManager();
	}

}
