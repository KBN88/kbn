package com.kbn.commons.crypto;

import com.kbn.commons.dao.CacheProvider;
import com.kbn.commons.dao.KeysCacheProviderFactory;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

public class DefaultKeyProvider implements KeyProvider {

	private static final CacheProvider cache = KeysCacheProviderFactory.getKeysCache();
	
	public DefaultKeyProvider() {
	}

	public String getKey(Fields fields) {
		
		return cache.get(fields.get(FieldType.KEY_ID.getName()));
	}

	public String getKey(String keyId) {
		return cache.get(keyId);
	}
}
