package com.kbn.commons.crypto;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;
import sun.misc.BASE64Decoder;

public class RsaEncrDecryption {
	public static PublicKey readPublicKey(String publicKey1) throws Exception {
		String pubKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwVkwNfGuYkLXAM2wxM+Z\r\n" + 
				"GpEITpSN79H8cerGhQ3TVhqdW3d1LJq6e1DzYdrUqzW7IMdrgkwR46OpybfrB/gF\r\n" + 
				"RKy6hENKLGOWvfu+g+/X/uAexiHQvOrjLj2KEMoihffF68lkR1JL+EGBvTqox2Ug\r\n" + 
				"oRuCXuIYQYR0zTSgTdK0Rdm5WzWsbWB7qDzmJLIRuQLjBViSI+ZeYVmpc1hkd7Ji\r\n" + 
				"mgV86ysmMCQ2Vw/ywCifuk/LoTzBVhYeBD4yOe4RDTKvzjnaBO3PAxJsl43/ujeU\r\n" + 
				"ig4FqxxWvsOVF0PmKHQ3FpZuLEPQG4GQAFLk6ArlMgS+zbR7yklTiNcCGXrTghlN\r\n" + 
				"RQIDAQAB";
		publicKey1 = pubKey;
		byte[] keyBytes = new byte[pubKey.length()];
		BASE64Decoder decoder = new BASE64Decoder();
		keyBytes = decoder.decodeBuffer(pubKey);
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey = keyFactory.generatePublic(spec);
		return publicKey;
	}

	public static byte[] encrypt(String text, PublicKey key) {
		byte[] cipherText = null;
		try {
			// get an RSA cipher object and print the provider

			final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

			// encrypt the plain text using the public key
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(text.getBytes());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cipherText;
	}

}
