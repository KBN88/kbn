package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.DateCreater;

public class LineChartService extends HibernateAbstractDao {
	private static Logger logger = Logger.getLogger(LineChartService.class
			.getName());


	public LineChartService() {
	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}
	public List<PieChart> getDashboardValues(String payId, String currency, String dateFrom, String dateTo)
			throws SystemException, ParseException {
		List<PieChart> pieChartsList = new ArrayList<PieChart>();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection.prepareStatement("{call totalTransactionRecord(?,?,?,?)}")) {
				dateTo = DateCreater.formatToDate(dateTo);
				dateFrom = DateCreater.formatFromDate(dateFrom);
				prepStmt.setString(1, payId);
				prepStmt.setString(2, currency);
				prepStmt.setString(3, dateFrom);
				prepStmt.setString(4, dateTo);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						PieChart chart = new PieChart();
						chart.setTotalSuccess(rs.getString(CrmFieldConstants.TOTAL_SUCCESS.getValue()));
						chart.setTotalFailed(rs.getString(CrmFieldConstants.TOTAL_FAILED.getValue()));
						chart.setTotalRefunded(rs.getString(CrmFieldConstants.TOTAL_REFUNDED.getValue()));
						// chart.setTotalPending(rs.getString(CrmFieldConstants.TOTAL_PENDING.getValue()));
						chart.setTxndate(rs.getString(CrmFieldConstants.TXN_DATE.getValue()));
						pieChartsList.add(chart);
					}

				}
			}
		} catch (SQLException exception) {
			logger.error("Database error");
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		} finally {
			autoClose();
		}
		return pieChartsList;
	}

	public List<PieChart> preparelist(String payId, String currency,
			String dateFrom, String dateTo) throws SystemException,
			ParseException {

		List<PieChart> pieChartsList = getDashboardValues(payId, currency,
				dateFrom, dateTo);
		List<String> dummyList = prepareDateList(dateFrom, dateTo);
		boolean flag = false;
		List<PieChart> finalPiechartList = new ArrayList<PieChart>();

		for (String date : dummyList) {

			for (PieChart pieChart : pieChartsList) {
				flag = false;
				if (pieChart.getTxndate().equals(date)) {
					finalPiechartList.add(pieChart);
					flag = true;
					break;
				}
			}
			if (!flag) {
				PieChart chart = new PieChart();
				chart.setTotalSuccess("0");
				chart.setTotalFailed("0");
				chart.setTotalPending("0");
				chart.setTotalRefunded("0");
				chart.setTxndate((date));
				finalPiechartList.add(chart);
			}
		}

		return finalPiechartList;
	}

	public List<String> prepareDateList(String dateFrom, String dateTo)
			throws ParseException {

		List<String> dates = new ArrayList<String>();

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");

		Date start = sdf.parse(dateFrom);
		Date end = sdf.parse(dateTo);
		calendar.setTime(start);
		while (calendar.getTime().before(end)) {
			Date result = calendar.getTime();
			String dateString = sdf1.format(result);
			dates.add(dateString);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		return dates;
	}

	public List<PieChart> prepareResellerlist(String payId, String resellerId,
			String currency, String dateFrom, String dateTo)
			throws SystemException, ParseException {

		List<PieChart> pieChartsList = getDashboardResellerValues(payId,
				resellerId, currency, dateFrom, dateTo);
		List<String> dummyList = prepareDateList(dateFrom, dateTo);
		boolean flag = false;
		List<PieChart> finalPiechartList = new ArrayList<PieChart>();

		for (String date : dummyList) {

			for (PieChart pieChart : pieChartsList) {
				flag = false;
				if (pieChart.getTxndate().equals(date)) {
					finalPiechartList.add(pieChart);
					flag = true;
					break;
				}
			}
			if (!flag) {
				PieChart chart = new PieChart();
				chart.setTotalSuccess("0");
				chart.setTotalFailed("0");
				chart.setTotalPending("0");
				chart.setTotalRefunded("0");
				chart.setTxndate((date));
				finalPiechartList.add(chart);
			}
		}

		return finalPiechartList;
	}

	private List<PieChart> getDashboardResellerValues(String payId,
			String resellerId, String currency, String dateFrom, String dateTo)
			throws SystemException, ParseException {
		List<PieChart> pieChartsList = new ArrayList<PieChart>();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call resellerLineChartTransactionRecord(?,?,?,?,?)}")) {
				dateTo = DateCreater.formatToDate(dateTo);
				dateFrom = DateCreater.formatFromDate(dateFrom);
				prepStmt.setString(1, payId);
				prepStmt.setString(2, resellerId);
				prepStmt.setString(3, currency);
				prepStmt.setString(4, dateFrom);
				prepStmt.setString(5, dateTo);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						PieChart chart = new PieChart();
						chart.setTotalSuccess(rs
								.getString(CrmFieldConstants.TOTAL_SUCCESS
										.getValue()));
						chart.setTotalFailed(rs
								.getString(CrmFieldConstants.TOTAL_FAILED
										.getValue()));
						chart.setTotalRefunded(rs
								.getString(CrmFieldConstants.TOTAL_REFUNDED
										.getValue()));
						// chart.setTotalPending(rs.getString(CrmFieldConstants.TOTAL_PENDING.getValue()));
						chart.setTxndate(rs
								.getString(CrmFieldConstants.TXN_DATE
										.getValue()));
						pieChartsList.add(chart);
					}

				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChartsList;
	}

	public List<PieChart> linePreparelist(String acquirer, String currency,
			String dateFrom, String dateTo)throws SystemException,
			ParseException {

		List<PieChart> pieChartsList = getAcquirerDashboardValues(acquirer, currency,
				dateFrom, dateTo);
		List<String> dummyList = prepareDateList(dateFrom, dateTo);
		boolean flag = false;
		List<PieChart> finalPiechartList = new ArrayList<PieChart>();

		for (String date : dummyList) {

			for (PieChart pieChart : pieChartsList) {
				flag = false;
				if (pieChart.getTxndate().equals(date)) {
					finalPiechartList.add(pieChart);
					flag = true;
					break;
				}
			}
			if (!flag) {
				PieChart chart = new PieChart();
				chart.setTotalSuccess("0");
				chart.setTotalFailed("0");
				chart.setTotalPending("0");
				chart.setTotalRefunded("0");
				chart.setTxndate((date));
				finalPiechartList.add(chart);
			}
		}

		return finalPiechartList;
	}
	
	public List<PieChart> getAcquirerDashboardValues(String acquirer, String currency,
			String dateFrom, String dateTo) throws SystemException,
			ParseException {
		List<PieChart> pieChartsList = new ArrayList<PieChart>();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirerLineChart(?,?,?,?)}")) {
				dateTo = DateCreater.formatToDate(dateTo);
				dateFrom = DateCreater.formatFromDate(dateFrom);
				prepStmt.setString(1, acquirer);
				prepStmt.setString(2, currency);
				prepStmt.setString(3, dateFrom);
				prepStmt.setString(4, dateTo);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						PieChart chart = new PieChart();
						chart.setTotalSuccess(rs
								.getString(CrmFieldConstants.TOTAL_SUCCESS
										.getValue()));
						chart.setTotalFailed(rs
								.getString(CrmFieldConstants.TOTAL_FAILED
										.getValue()));
						chart.setTotalRefunded(rs
								.getString(CrmFieldConstants.TOTAL_REFUNDED
										.getValue()));
						chart.setTxndate(rs
								.getString(CrmFieldConstants.TXN_DATE
										.getValue()));
						pieChartsList.add(chart);
					}

				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChartsList;
	}

}