package com.kbn.commons.dao;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.DateCreater;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;

public class TransactionSummaryService {

	private static Logger logger = Logger
			.getLogger(TransactionSummaryService.class.getName());
	public List<TransactionSummaryReport> transactionList = new ArrayList<TransactionSummaryReport>();
	private SimpleDateFormat formatDate = new SimpleDateFormat(
			CrmFieldConstants.DATE_TIME_FORMAT.getValue());

	public TransactionSummaryService() {
	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public List<TransactionSummaryReport> getFailedTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String userType, String paymentType, String acquirer,
			String currency, int start, int length) throws SQLException, ParseException,
			SystemException {

		List<TransactionSummaryReport> transactionListSale = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListEnroll = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListNewOrder = new ArrayList<TransactionSummaryReport>();

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call failedSummary(?,?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				prepStmt.setInt(8, start);
				prepStmt.setInt(9, length);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						if (!rs.getString(FieldType.CURRENCY_CODE.toString())
								.isEmpty()) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transReport
								.setPgTxnMessage(rs
										.getString(FieldType.PG_TXN_MESSAGE
												.toString()));

						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						if (!rs.getString(FieldType.PAYMENT_TYPE.toString())
								.isEmpty()) {
							if (PaymentType.getpaymentName(rs
									.getString(FieldType.PAYMENT_TYPE
											.toString())) != null) {
								transReport
										.setPaymentMethod(PaymentType.getpaymentName(rs
												.getString(FieldType.PAYMENT_TYPE
														.toString())));
							}
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							if ((rs.getString(FieldType.PAYMENT_TYPE.getName()))
									.equals(PaymentType.NET_BANKING.getCode())) {
								transReport
										.setCardNo("Net-Banking Transaction");
							} else if ((rs.getString(FieldType.PAYMENT_TYPE
									.getName())).equals(PaymentType.WALLET
									.getCode())) {
								transReport.setCardNo("Wallet Transaction");
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

						}
						if (!(rs.getString(FieldType.PAYMENT_TYPE.getName()))
								.equals(PaymentType.NET_BANKING.getCode())
								|| !(rs.getString(FieldType.PAYMENT_TYPE
										.getName())).equals(PaymentType.WALLET
										.getCode())) {
							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
									.toString()) != null) {
								transReport
										.setInternalCardIssusserBank(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserBank(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
									.toString()) != null) {
								transReport
										.setInternalCardIssusserCountry(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserCountry(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));
					/*	if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/
						if (transReport.getTxnType().equals(
								TransactionType.SALE.getName())
								|| transReport.getTxnType().equals(
										TransactionType.AUTHORISE.getName())) {
							transactionListSale.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.ENROLL.getName())) {
							transactionListEnroll.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.NEWORDER.getName())) {
							transactionListNewOrder.add(transReport);
						}
					}
				}

				if (!transactionListEnroll.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListEnroll
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						if (transReport.getOrigTransactionId().equals(0)) {
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport
												.getOrigTransactionId())) {
									flag = false;
									break;
								}
							}
						} else {
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport.getTransactionId())) {
									flag = false;
									break;
								}
							}
						}

						if (flag)
							transactionListSale.add(transReport);
					}
				}

				if (!transactionListNewOrder.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListNewOrder
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						while (itrSale.hasNext()) {
							TransactionSummaryReport transReportSale = itrSale
									.next();
							if (transReportSale.getOrigTransactionId().equals(
									transReport.getTransactionId())) {
								flag = false;
								break;
							}
						}
						if (flag)
							transactionListSale.add(transReport);

					}
				}
				transactionList.addAll(transactionListSale);
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getIncompleteTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String userType, String paymentType, String status,
			String acquirer, String currency, int start, int length) throws SQLException,
			ParseException, SystemException {

		List<TransactionSummaryReport> transactionListSale = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListEnroll = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListNewOrder = new ArrayList<TransactionSummaryReport>();

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call incompleteSummary(?,?,?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, status);
				prepStmt.setString(7, acquirer);
				prepStmt.setString(8, currency);
				prepStmt.setInt(9, start);
				prepStmt.setInt(10, length);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						if (!rs.getString(FieldType.CURRENCY_CODE.toString())
								.isEmpty()) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						if (!rs.getString(FieldType.PAYMENT_TYPE.toString())
								.isEmpty()) {
							if (PaymentType.getpaymentName(rs
									.getString(FieldType.PAYMENT_TYPE
											.toString())) != null) {
								transReport
										.setPaymentMethod(PaymentType.getpaymentName(rs
												.getString(FieldType.PAYMENT_TYPE
														.toString())));
							}
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							if ((rs.getString(FieldType.PAYMENT_TYPE.getName()))
									.equals(PaymentType.NET_BANKING.getCode())) {
								transReport
										.setCardNo("Net-Banking Transaction");
							} else if ((rs.getString(FieldType.PAYMENT_TYPE
									.getName())).equals(PaymentType.WALLET
									.getCode())) {
								transReport.setCardNo("Wallet Transaction");
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

						}

						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));
				/*		if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/

						if (transReport.getTxnType().equals(
								TransactionType.SALE.getName())
								|| transReport.getTxnType().equals(
										TransactionType.AUTHORISE.getName())) {
							transactionListSale.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.ENROLL.getName())) {
							transactionListEnroll.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.NEWORDER.getName())) {
							transactionListNewOrder.add(transReport);
						}
						transReport.setCustPhone(rs
								.getString(FieldType.CUST_PHONE.toString()));
					}
				}

				if (!transactionListEnroll.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListEnroll
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						while (itrSale.hasNext()) {
							TransactionSummaryReport transReportSale = itrSale
									.next();
							if (transReportSale.getOrigTransactionId().equals(
									transReport.getOrigTransactionId())) {
								flag = false;
								break;
							}
						}
						if (flag)
							transactionListSale.add(transReport);
					}
				}

				if (!transactionListNewOrder.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListNewOrder
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						while (itrSale.hasNext()) {
							TransactionSummaryReport transReportSale = itrSale
									.next();
							if (transReportSale.getOrigTransactionId().equals(
									transReport.getTransactionId())) {
								flag = false;
								break;
							}
						}
						if (flag)
							transactionListSale.add(transReport);

					}
				}
				transactionList.addAll(transactionListSale);
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getInvalidTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String userType) throws SQLException, ParseException,
			SystemException {
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call invalidSummary(?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						if (!rs.getString(FieldType.PAYMENT_TYPE.toString())
								.isEmpty()) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.STATUS.toString())
								.isEmpty()) {
							transReport.setStatus(rs.getString(FieldType.STATUS
									.toString()));
						} else {
							transReport
									.setStatus(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.CUST_EMAIL.toString())
								.isEmpty()) {
							transReport
									.setCustomerEmail(rs
											.getString(FieldType.CUST_EMAIL
													.toString()));
						} else {
							transReport
									.setCustomerEmail(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.AMOUNT.toString())
								.isEmpty()) {
							transReport.setApprovedAmount(rs
									.getString(FieldType.AMOUNT.toString()));
						} else {
							transReport.setApprovedAmount("0.00");
						}
						if (!rs.getString(FieldType.ORDER_ID.toString())
								.isEmpty()) {
							transReport.setOrderId(rs
									.getString(FieldType.ORDER_ID.toString()));
						} else {
							transReport.setOrderId("0");
						}

					/*	if (!rs.getString(FieldType.RESPONSE_MESSAGE.toString())
								.isEmpty()) {
							transReport.setResponseMsg(rs
									.getString(FieldType.RESPONSE_MESSAGE
											.toString()));
						} else {
							transReport
									.setResponseMsg(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/
						if (!rs.getString(FieldType.CUST_NAME.toString())
								.isEmpty()) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							if ((rs.getString(FieldType.PAYMENT_TYPE.getName()))
									.equals(PaymentType.NET_BANKING.getCode())) {
								transReport
										.setCardNo("Net-Banking Transaction");
							} else if ((rs.getString(FieldType.PAYMENT_TYPE
									.getName())).equals(PaymentType.WALLET
									.getCode())) {
								transReport.setCardNo("Wallet Transaction");
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

						}
						if (!rs.getString(FieldType.PRODUCT_DESC.toString())
								.isEmpty()) {
							transReport.setProductDesc(rs
									.getString(FieldType.PRODUCT_DESC
											.toString()));
						} else {
							transReport
									.setProductDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
				/*		if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/
						transactionList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getAuthorisedTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String userType, String paymentType, String acquirer,
			String currency) throws SQLException, ParseException,
			SystemException {

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call authoriseSummary(?,?,?,?,?,?,?)}")) {
				if (fromDate.isEmpty()) {
					prepStmt.setString(1, "");
				} else {
					prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				}
				if (toDate.isEmpty()) {
					prepStmt.setString(2, "");
				} else {
					prepStmt.setString(2, DateCreater.formatToDate(toDate));
				}
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);

				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setCurrencyName(propManager
								.getAlphabaticCurrencyCode(rs
										.getString(FieldType.CURRENCY_CODE
												.toString())));
						transReport.setCardNo(rs.getString(FieldType.CARD_MASK
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.toString()));

						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						String captureTxnId = rs
								.getString(CrmFieldConstants.CAPTURE_TXN_ID
										.toString());
						transReport.setCaptureTxnId(captureTxnId);
						if (null != captureTxnId) {
							transReport
									.setStatus(StatusType.CAPTURED.getName());
						} else {
							transReport.setStatus(rs.getString(FieldType.STATUS
									.toString()));
						}
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							transReport
									.setCardNo(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
								.toString()) != null) {
							transReport
									.setInternalCardIssusserBank(rs
											.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
													.toString()));
						} else {
							transReport
									.setInternalCardIssusserBank(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
								.toString()) != null) {
							transReport
									.setInternalCardIssusserCountry(rs
											.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
													.toString()));
						} else {
							transReport
									.setInternalCardIssusserCountry(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transactionList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getCancelledTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String userType, String currency,int start ,int length ) throws SQLException,
			ParseException, SystemException {

		List<TransactionSummaryReport> transactionListSale = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListEnroll = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListNewOrder = new ArrayList<TransactionSummaryReport>();

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call cancelledSummary(?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, currency);
				prepStmt.setInt(6, start);
				prepStmt.setInt(7, length);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						transReport.setPaymentMethod(PaymentType
								.getpaymentName(rs
										.getString(FieldType.PAYMENT_TYPE
												.toString())));
						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						if (!rs.getString(FieldType.CURRENCY_CODE.toString())
								.isEmpty()) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));

					/*	if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/

						// Add data to lists
						if (transReport.getTxnType().equals(
								TransactionType.SALE.getName())
								|| transReport.getTxnType().equals(
										TransactionType.AUTHORISE.getName())) {
							transactionListSale.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.ENROLL.getName())) {
							transactionListEnroll.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.NEWORDER.getName())) {
							transactionListNewOrder.add(transReport);
						}
					}
				}

				if (!transactionListEnroll.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListEnroll
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						if (transactionListSale.size() != 0) {
							Iterator<TransactionSummaryReport> itrSale = transactionListSale
									.iterator();
							boolean flag = true;
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport
												.getOrigTransactionId())) {
									flag = false;
									break;
								}
							}
							if (flag)
								transactionListSale.add(transReport);
						} else {
							transactionListSale.add(transReport);
						}
					}
				}

				if (!transactionListNewOrder.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListNewOrder
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						if (transactionListSale.size() != 0) {
							Iterator<TransactionSummaryReport> itrSale = transactionListSale
									.iterator();
							boolean flag = true;
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport.getTransactionId())) {
									flag = false;
									break;
								}
							}
							if (flag)
								transactionListSale.add(transReport);
						} else {
							transactionListSale.add(transReport);
						}
					}
				}
				transactionList.addAll(transactionListSale);
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getCapturedTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String userType, String paymentType, String acquirer,
			String currency, int start, int length) throws SQLException,
			ParseException, SystemException {

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call capturedSummary(?,?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				prepStmt.setInt(8, start);
				prepStmt.setInt(9, length);

				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));
						if (rs.getString(FieldType.CURRENCY_CODE.toString()) != null) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (!(rs.getString(FieldType.PAYMENT_TYPE.getName()))
								.equals(PaymentType.NET_BANKING.getCode())
								|| !(rs.getString(FieldType.PAYMENT_TYPE
										.getName())).equals(PaymentType.WALLET
										.getCode())) {
							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
									.toString()) != null) {
								transReport
										.setInternalCardIssusserBank(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserBank(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
									.toString()) != null) {
								transReport
										.setInternalCardIssusserCountry(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserCountry(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));

						if (rs.getString(FieldType.CUST_EMAIL.toString()) != null) {
							transReport
									.setCustomerEmail(rs
											.getString(FieldType.CUST_EMAIL
													.toString()));
						} else {
							transReport
									.setCustomerEmail(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (rs.getString(FieldType.PAYMENT_TYPE.toString()) != null) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString("MOP_TYPE") != null) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							if ((rs.getString(FieldType.PAYMENT_TYPE.getName()))
									.equals(PaymentType.NET_BANKING.getCode())) {
								transReport
										.setCardNo("Net-Banking Transaction");
							} else if ((rs.getString(FieldType.PAYMENT_TYPE
									.getName())).equals(PaymentType.WALLET
									.getCode())) {
								transReport.setCardNo("Wallet Transaction");
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setRefundableAmount(rs
								.getString(CrmFieldConstants.REFUNDABLE_AMOUNT
										.getValue().toString()));
						transReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID
										.getName()));

						transactionList.add(transReport);
					}
				}

			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	//to fetch fraud txns
	public List<TransactionSummaryReport> getFraudTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String userType, String paymentType, String acquirer,
			String currency) throws SQLException, ParseException,
			SystemException {

		List<TransactionSummaryReport> transactionListSale = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListEnroll = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListNewOrder = new ArrayList<TransactionSummaryReport>();

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call fraudSummary(?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						if (!rs.getString(FieldType.CURRENCY_CODE.toString())
								.isEmpty()) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transReport
								.setPgTxnMessage(rs
										.getString(FieldType.PG_TXN_MESSAGE
												.toString()));

						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						if (!rs.getString(FieldType.PAYMENT_TYPE.toString())
								.isEmpty()) {
							if (PaymentType.getpaymentName(rs
									.getString(FieldType.PAYMENT_TYPE
											.toString())) != null) {
								transReport
										.setPaymentMethod(PaymentType.getpaymentName(rs
												.getString(FieldType.PAYMENT_TYPE
														.toString())));
							}
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							if ((rs.getString(FieldType.PAYMENT_TYPE.getName()))
									.equals(PaymentType.NET_BANKING.getCode())) {
								transReport
										.setCardNo("Net-Banking Transaction");
							} else if ((rs.getString(FieldType.PAYMENT_TYPE
									.getName())).equals(PaymentType.WALLET
									.getCode())) {
								transReport.setCardNo("Wallet Transaction");
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

						}
						if (!(rs.getString(FieldType.PAYMENT_TYPE.getName()))
								.equals(PaymentType.NET_BANKING.getCode())
								|| !(rs.getString(FieldType.PAYMENT_TYPE
										.getName())).equals(PaymentType.WALLET
										.getCode())) {
							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
									.toString()) != null) {
								transReport
										.setInternalCardIssusserBank(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserBank(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
									.toString()) != null) {
								transReport
										.setInternalCardIssusserCountry(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserCountry(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));
						/*if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/
						if (transReport.getTxnType().equals(
								TransactionType.SALE.getName())
								|| transReport.getTxnType().equals(
										TransactionType.AUTHORISE.getName())) {
							transactionListSale.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.ENROLL.getName())) {
							transactionListEnroll.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.NEWORDER.getName())) {
							transactionListNewOrder.add(transReport);
						}
					}
				}

				if (!transactionListEnroll.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListEnroll
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						if (transReport.getOrigTransactionId().equals(0)) {
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport
												.getOrigTransactionId())) {
									flag = false;
									break;
								}
							}
						} else {
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport.getTransactionId())) {
									flag = false;
									break;
								}
							}
						}

						if (flag)
							transactionListSale.add(transReport);
					}
				}

				if (!transactionListNewOrder.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListNewOrder
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						while (itrSale.hasNext()) {
							TransactionSummaryReport transReportSale = itrSale
									.next();
							if (transReportSale.getOrigTransactionId().equals(
									transReport.getTransactionId())) {
								flag = false;
								break;
							}
						}
						if (flag)
							transactionListSale.add(transReport);

					}
				}
				transactionList.addAll(transactionListSale);
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	
	public List<TransactionSummaryReport> getAuthorisedResellerTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId, String paymentType, String acquirer,
			String currency) throws SQLException, ParseException,
			SystemException {

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call resellerAuthoriseSummary(?,?,?,?,?,?,?)}")) {
				if (fromDate.isEmpty()) {
					prepStmt.setString(1, "");
				} else {
					prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				}
				if (toDate.isEmpty()) {
					prepStmt.setString(2, "");
				} else {
					prepStmt.setString(2, DateCreater.formatToDate(toDate));
				}
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, resellerId);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);

				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setCurrencyName(propManager
								.getAlphabaticCurrencyCode(rs
										.getString(FieldType.CURRENCY_CODE
												.toString())));
						transReport.setCardNo(rs.getString(FieldType.CARD_MASK
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.toString()));

						transReport.setMopType(rs.getString(FieldType.MOP_TYPE
								.toString()));

						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							transReport
									.setCardNo(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
								.toString()) != null) {
							transReport
									.setInternalCardIssusserBank(rs
											.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
													.toString()));
						} else {
							transReport
									.setInternalCardIssusserBank(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
								.toString()) != null) {
							transReport
									.setInternalCardIssusserCountry(rs
											.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
													.toString()));
						} else {
							transReport
									.setInternalCardIssusserCountry(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transactionList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getResellerCapturedTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId, String paymentType, String acquirer,
			String currency,int start , int length) throws SQLException, ParseException,
			SystemException {

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call resellerCapturedSummary(?,?,?,?,?,?,?,?,?)}")) {
				if (fromDate.isEmpty()) {
					prepStmt.setString(1, "");
				} else {
					prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				}
				if (toDate.isEmpty()) {
					prepStmt.setString(2, "");
				} else {
					prepStmt.setString(2, DateCreater.formatToDate(toDate));
				}
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, resellerId);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				prepStmt.setInt(8, start);
				prepStmt.setInt(9, length);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						if (rs.getString(FieldType.CURRENCY_CODE.toString()) != null) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (!(rs.getString(FieldType.PAYMENT_TYPE.getName()))
								.equals(PaymentType.NET_BANKING.getCode())
								|| !(rs.getString(FieldType.PAYMENT_TYPE
										.getName())).equals(PaymentType.WALLET
										.getCode())) {
							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
									.toString()) != null) {
								transReport
										.setInternalCardIssusserBank(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserBank(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
									.toString()) != null) {
								transReport
										.setInternalCardIssusserCountry(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserCountry(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));

						if (rs.getString(FieldType.CUST_EMAIL.toString()) != null) {
							transReport
									.setCustomerEmail(rs
											.getString(FieldType.CUST_EMAIL
													.toString()));
						} else {
							transReport
									.setCustomerEmail(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (rs.getString(FieldType.PAYMENT_TYPE.toString()) != null) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString("MOP_TYPE") != null) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							if ((rs.getString(FieldType.PAYMENT_TYPE.getName()))
									.equals(PaymentType.NET_BANKING.getCode())) {
								transReport
										.setCardNo("Net-Banking Transaction");
							} else if ((rs.getString(FieldType.PAYMENT_TYPE
									.getName())).equals(PaymentType.WALLET
									.getCode())) {
								transReport.setCardNo("Wallet Transaction");
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setRefundableAmount(rs
								.getString(CrmFieldConstants.REFUNDABLE_AMOUNT
										.getValue().toString()));

						transactionList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getResellerIncompleteTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId, String paymentType, String status,
			String acquirer, String currency, int start,int length ) throws SQLException,
			ParseException, SystemException {
		List<TransactionSummaryReport> transactionListSale = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListEnroll = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListNewOrder = new ArrayList<TransactionSummaryReport>();

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call resellerIncompleteSummary(?,?,?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, resellerId);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, status);
				prepStmt.setString(7, acquirer);
				prepStmt.setString(8, currency);
				prepStmt.setInt(9, start);
				prepStmt.setInt(10, length);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						if (!rs.getString(FieldType.CURRENCY_CODE.toString())
								.isEmpty()) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						transReport.setPaymentMethod(PaymentType
								.getpaymentName(rs
										.getString(FieldType.PAYMENT_TYPE
												.toString())));
						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							transReport
									.setCardNo(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));
				/*		if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/

						if (transReport.getTxnType().equals(
								TransactionType.SALE.getName())
								|| transReport.getTxnType().equals(
										TransactionType.AUTHORISE.getName())) {
							transactionListSale.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.ENROLL.getName())) {
							transactionListEnroll.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.NEWORDER.getName())) {
							transactionListNewOrder.add(transReport);
						}
					}
				}

				if (!transactionListEnroll.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListEnroll
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						while (itrSale.hasNext()) {
							TransactionSummaryReport transReportSale = itrSale
									.next();
							if (transReportSale.getOrigTransactionId().equals(
									transReport.getOrigTransactionId())) {
								flag = false;
								break;
							}
						}
						if (flag)
							transactionListSale.add(transReport);
					}
				}

				if (!transactionListNewOrder.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListNewOrder
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						while (itrSale.hasNext()) {
							TransactionSummaryReport transReportSale = itrSale
									.next();
							if (transReportSale.getOrigTransactionId().equals(
									transReport.getTransactionId())) {
								flag = false;
								break;
							}
						}
						if (flag)
							transactionListSale.add(transReport);

					}
				}
				transactionList.addAll(transactionListSale);
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getFailedResellerTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId, String paymentType, String acquirer,
			String currency, int start ,int length) throws SQLException, ParseException,
			SystemException {
		{
			List<TransactionSummaryReport> transactionListSale = new ArrayList<TransactionSummaryReport>();
			List<TransactionSummaryReport> transactionListEnroll = new ArrayList<TransactionSummaryReport>();
			List<TransactionSummaryReport> transactionListNewOrder = new ArrayList<TransactionSummaryReport>();

			PropertiesManager propManager = new PropertiesManager();

			try (Connection connection = getConnection()) {
				try (PreparedStatement prepStmt = connection
						.prepareStatement("{call resellerFailedSummary(?,?,?,?,?,?,?,?,?)}")) {
					prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
					prepStmt.setString(2, DateCreater.formatToDate(toDate));
					prepStmt.setString(3, merchantPayId);
					prepStmt.setString(4, resellerId);
					prepStmt.setString(5, paymentType);
					prepStmt.setString(6, acquirer);
					prepStmt.setString(7, currency);
					prepStmt.setInt(8, start);
					prepStmt.setInt(9, length);
					try (ResultSet rs = prepStmt.executeQuery()) {
						while (rs.next()) {
							TransactionSummaryReport transReport = new TransactionSummaryReport();
							transReport.setOid(rs.getString(FieldType.OID
									.toString()));
							transReport
									.setOrigTransactionId(rs
											.getString(FieldType.ORIG_TXN_ID
													.toString()));
							transReport.setTxnDate(formatDate.format(rs
									.getTimestamp(CrmFieldConstants.CREATE_DATE
											.toString())));
							transReport.setTransactionId(rs
									.getString(FieldType.TXN_ID.toString()));
							transReport.setApprovedAmount(rs
									.getString(FieldType.AMOUNT.toString()));
							if (!rs.getString(
									FieldType.CURRENCY_CODE.toString())
									.isEmpty()) {
								transReport
										.setCurrencyName(propManager.getAlphabaticCurrencyCode(rs
												.getString(FieldType.CURRENCY_CODE
														.toString())));
							} else {
								transReport
										.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

							transReport
									.setCustomerEmail(rs
											.getString(FieldType.CUST_EMAIL
													.toString()));
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
							if (!rs.getString(FieldType.MOP_TYPE.toString())
									.isEmpty()) {
								if (MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())) != null) {
									transReport
											.setMopType(MopType.getmopName(rs
													.getString(FieldType.MOP_TYPE
															.toString())));
								}
							} else {
								transReport
										.setMopType(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
							transReport.setTxnType(rs
									.getString(FieldType.TXNTYPE.toString()));
							transReport.setPayId(rs.getString(FieldType.PAY_ID
									.toString()));
							transReport.setBusinessName(rs
									.getString(CrmFieldType.BUSINESS_NAME
											.getName().toString()));
							transReport.setStatus(rs.getString(FieldType.STATUS
									.toString()));
							transReport.setOrderId(rs
									.getString(FieldType.ORDER_ID.toString()));
							transReport.setResponseMsg(rs
									.getString(FieldType.RESPONSE_MESSAGE
											.toString()));
							if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
								transReport.setCustomerName(rs
										.getString(FieldType.CUST_NAME
												.toString()));
							} else {
								transReport
										.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
							if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
								transReport.setCardNo(rs
										.getString(FieldType.CARD_MASK
												.toString()));
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
							transReport.setProductDesc(rs
									.getString(FieldType.PRODUCT_DESC
											.toString()));
							transReport.setAcquirer(rs
									.getString(FieldType.ACQUIRER_TYPE
											.toString()));
				/*			if (!rs.getBlob(
									CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString()).equals(null)) {
								Blob blob = (Blob) rs
										.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
												.toString());
								byte[] data = Base64.decodeBase64(blob
										.getBytes(1, (int) blob.length()));
								String s = new String(data);
								transReport.setInternalRequestDesc(s);
							} else {
								transReport
										.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}*/
							if (transReport.getTxnType().equals(
									TransactionType.SALE.getName())
									|| transReport.getTxnType()
											.equals(TransactionType.AUTHORISE
													.getName())) {
								transactionListSale.add(transReport);
							} else if (transReport.getTxnType().equals(
									TransactionType.ENROLL.getName())) {
								transactionListEnroll.add(transReport);
							} else if (transReport.getTxnType().equals(
									TransactionType.NEWORDER.getName())) {
								transactionListNewOrder.add(transReport);
							}
						}
					}

					if (!transactionListEnroll.isEmpty()) {
						Iterator<TransactionSummaryReport> itr = transactionListEnroll
								.iterator();
						while (itr.hasNext()) {
							TransactionSummaryReport transReport = itr.next();
							Iterator<TransactionSummaryReport> itrSale = transactionListSale
									.iterator();
							boolean flag = true;
							if (transReport.getOrigTransactionId().equals(0)) {
								while (itrSale.hasNext()) {
									TransactionSummaryReport transReportSale = itrSale
											.next();
									if (transReportSale.getOrigTransactionId()
											.equals(transReport
													.getOrigTransactionId())) {
										flag = false;
										break;
									}
								}
							} else {
								while (itrSale.hasNext()) {
									TransactionSummaryReport transReportSale = itrSale
											.next();
									if (transReportSale.getOrigTransactionId()
											.equals(transReport
													.getTransactionId())) {
										flag = false;
										break;
									}
								}
							}

							if (flag)
								transactionListSale.add(transReport);
						}
					}

					if (!transactionListNewOrder.isEmpty()) {
						Iterator<TransactionSummaryReport> itr = transactionListNewOrder
								.iterator();
						while (itr.hasNext()) {
							TransactionSummaryReport transReport = itr.next();
							Iterator<TransactionSummaryReport> itrSale = transactionListSale
									.iterator();
							boolean flag = true;
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport.getTransactionId())) {
									flag = false;
									break;
								}
							}
							if (flag)
								transactionListSale.add(transReport);

						}
					}
					transactionList.addAll(transactionListSale);
				}
			} catch (SQLException exception) {
				logger.error("Database error", exception);
				throw new SystemException(ErrorType.DATABASE_ERROR,
						ErrorType.DATABASE_ERROR.getResponseMessage());
			}
			return transactionList;
		}
	}

	public List<TransactionSummaryReport> getResellerCancelledTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId, String currency, int start, int length) throws SQLException,
			ParseException, SystemException {

		List<TransactionSummaryReport> transactionListSale = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListEnroll = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListNewOrder = new ArrayList<TransactionSummaryReport>();

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call resellerCancelledSummary(?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, resellerId);
				prepStmt.setString(5, currency);
				prepStmt.setInt(6, start);
				prepStmt.setInt(7, length);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						transReport.setPaymentMethod(PaymentType
								.getpaymentName(rs
										.getString(FieldType.PAYMENT_TYPE
												.toString())));
						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						if (!rs.getString(FieldType.CURRENCY_CODE.toString())
								.isEmpty()) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));

					/*	if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/

						// Add data to lists
						if (transReport.getTxnType().equals(
								TransactionType.SALE.getName())
								|| transReport.getTxnType().equals(
										TransactionType.AUTHORISE.getName())) {
							transactionListSale.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.ENROLL.getName())) {
							transactionListEnroll.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.NEWORDER.getName())) {
							transactionListNewOrder.add(transReport);
						}
					}
				}

				if (!transactionListEnroll.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListEnroll
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						if (transactionListSale.size() != 0) {
							Iterator<TransactionSummaryReport> itrSale = transactionListSale
									.iterator();
							boolean flag = true;
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport
												.getOrigTransactionId())) {
									flag = false;
									break;
								}
							}
							if (flag)
								transactionListSale.add(transReport);
						} else {
							transactionListSale.add(transReport);
						}
					}
				}

				if (!transactionListNewOrder.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListNewOrder
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						if (transactionListSale.size() != 0) {
							Iterator<TransactionSummaryReport> itrSale = transactionListSale
									.iterator();
							boolean flag = true;
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport.getTransactionId())) {
									flag = false;
									break;
								}
							}
							if (flag)
								transactionListSale.add(transReport);
						} else {
							transactionListSale.add(transReport);
						}
					}
				}
				transactionList.addAll(transactionListSale);
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getResellerInvalidTransactionList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId) throws SQLException, ParseException,
			SystemException {
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call resellerInvalidSummary(?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, resellerId);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						if (!rs.getString(FieldType.PAYMENT_TYPE.toString())
								.isEmpty()) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.STATUS.toString())
								.isEmpty()) {
							transReport.setStatus(rs.getString(FieldType.STATUS
									.toString()));
						} else {
							transReport
									.setStatus(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.CUST_EMAIL.toString())
								.isEmpty()) {
							transReport
									.setCustomerEmail(rs
											.getString(FieldType.CUST_EMAIL
													.toString()));
						} else {
							transReport
									.setCustomerEmail(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.AMOUNT.toString())
								.isEmpty()) {
							transReport.setApprovedAmount(rs
									.getString(FieldType.AMOUNT.toString()));
						} else {
							transReport.setApprovedAmount("0.00");
						}
						if (!rs.getString(FieldType.ORDER_ID.toString())
								.isEmpty()) {
							transReport.setOrderId(rs
									.getString(FieldType.ORDER_ID.toString()));
						} else {
							transReport.setOrderId("0");
						}

						if (!rs.getString(FieldType.RESPONSE_MESSAGE.toString())
								.isEmpty()) {
							transReport.setResponseMsg(rs
									.getString(FieldType.RESPONSE_MESSAGE
											.toString()));
						} else {
							transReport
									.setResponseMsg(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.CUST_NAME.toString())
								.isEmpty()) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (!rs.getString(FieldType.CARD_MASK.toString())
								.isEmpty()) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							transReport
									.setCardNo(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.PRODUCT_DESC.toString())
								.isEmpty()) {
							transReport.setProductDesc(rs
									.getString(FieldType.PRODUCT_DESC
											.toString()));
						} else {
							transReport
									.setProductDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
					/*	if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/
						transactionList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public BigInteger getCapturedTransactionCountList(String fromDate,
			String toDate, String merchantPayId, String userType,
			String paymentType, String acquirer, String currency)
			throws SQLException, ParseException, SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call countCaptured(?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}

	public BigInteger getFailedTransactionListCountList(String fromDate,
			String toDate, String merchantPayId, String userType,
			String paymentType, String acquirer, String currency)
			throws SQLException, ParseException, SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call countFailedSummary(?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}

	public BigInteger getIncompleteTransactionListCountList(String fromDate, String toDate, String merchantPayId,
			String userType, String paymentType, String status,
			String acquirer, String currency) throws SQLException,
			ParseException, SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call countIncompleteSummary(?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, status);
				prepStmt.setString(7, acquirer);
				prepStmt.setString(8, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}

	public BigInteger getCancelledTransactionListCountList(String fromDate, String toDate, String merchantPayId,
			String userType, String currency) throws SQLException,
			ParseException, SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call countCancelledSummary(?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}

	public BigInteger getAcquirerCapturedTransactionCountList(String fromDate,
			String toDate, String merchantPayId,
			String paymentType, String acquirer, String currency)
			throws SQLException, ParseException, SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call count_AcquirerCaptured(?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}

	public List<TransactionSummaryReport> getAcquirerCapturedTransactionCountList(String fromDate, String toDate, String merchantPayId,
			String paymentType, String acquirer,
			String currency, int start, int length) throws SQLException,
			ParseException, SystemException {

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirer_CapturedSummary(?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setInt(7, start);
				prepStmt.setInt(8, length);

				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));
						if (rs.getString(FieldType.CURRENCY_CODE.toString()) != null) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (!(rs.getString(FieldType.PAYMENT_TYPE.getName()))
								.equals(PaymentType.NET_BANKING.getCode())
								|| !(rs.getString(FieldType.PAYMENT_TYPE
										.getName())).equals(PaymentType.WALLET
										.getCode())) {
							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
									.toString()) != null) {
								transReport
										.setInternalCardIssusserBank(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserBank(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
									.toString()) != null) {
								transReport
										.setInternalCardIssusserCountry(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserCountry(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));

						if (rs.getString(FieldType.CUST_EMAIL.toString()) != null) {
							transReport
									.setCustomerEmail(rs
											.getString(FieldType.CUST_EMAIL
													.toString()));
						} else {
							transReport
									.setCustomerEmail(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (rs.getString(FieldType.PAYMENT_TYPE.toString()) != null) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString("MOP_TYPE") != null) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							if ((rs.getString(FieldType.PAYMENT_TYPE.getName()))
									.equals(PaymentType.NET_BANKING.getCode())) {
								transReport
										.setCardNo("Net-Banking Transaction");
							} else if ((rs.getString(FieldType.PAYMENT_TYPE
									.getName())).equals(PaymentType.WALLET
									.getCode())) {
								transReport.setCardNo("Wallet Transaction");
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setRefundableAmount(rs
								.getString(CrmFieldConstants.REFUNDABLE_AMOUNT
										.getValue().toString()));

						transactionList.add(transReport);
					}
				}

			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public BigInteger getAcquirerIncompleteTransactionList(String fromDate, String toDate, String merchantPayId,
			 String paymentType, String status,
			String acquirer, String currency) throws SQLException,
			ParseException, SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call count_AcquirerIncompleteSummary(?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, status);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}


	public List<TransactionSummaryReport> getAcquirerIncompleteTransactionList(
			String fromDate, String toDate, String merchantPayId,
		    String paymentType, String status,
			String acquirer, String currency, int start, int length) throws SQLException,
			ParseException, SystemException {

		List<TransactionSummaryReport> transactionListSale = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListEnroll = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListNewOrder = new ArrayList<TransactionSummaryReport>();

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirer_IncompleteSummary(?,?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, status);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				prepStmt.setInt(8, start);
				prepStmt.setInt(9, length);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						if (!rs.getString(FieldType.CURRENCY_CODE.toString())
								.isEmpty()) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						if (!rs.getString(FieldType.PAYMENT_TYPE.toString())
								.isEmpty()) {
							if (PaymentType.getpaymentName(rs
									.getString(FieldType.PAYMENT_TYPE
											.toString())) != null) {
								transReport
										.setPaymentMethod(PaymentType.getpaymentName(rs
												.getString(FieldType.PAYMENT_TYPE
														.toString())));
							}
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							if ((rs.getString(FieldType.PAYMENT_TYPE.getName()))
									.equals(PaymentType.NET_BANKING.getCode())) {
								transReport
										.setCardNo("Net-Banking Transaction");
							} else if ((rs.getString(FieldType.PAYMENT_TYPE
									.getName())).equals(PaymentType.WALLET
									.getCode())) {
								transReport.setCardNo("Wallet Transaction");
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

						}

						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));
				/*		if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/

						if (transReport.getTxnType().equals(
								TransactionType.SALE.getName())
								|| transReport.getTxnType().equals(
										TransactionType.AUTHORISE.getName())) {
							transactionListSale.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.ENROLL.getName())) {
							transactionListEnroll.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.NEWORDER.getName())) {
							transactionListNewOrder.add(transReport);
						}
						transReport.setCustPhone(rs
								.getString(FieldType.CUST_PHONE.toString()));
					}
				}

				if (!transactionListEnroll.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListEnroll
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						while (itrSale.hasNext()) {
							TransactionSummaryReport transReportSale = itrSale
									.next();
							if (transReportSale.getOrigTransactionId().equals(
									transReport.getOrigTransactionId())) {
								flag = false;
								break;
							}
						}
						if (flag)
							transactionListSale.add(transReport);
					}
				}

				if (!transactionListNewOrder.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListNewOrder
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						while (itrSale.hasNext()) {
							TransactionSummaryReport transReportSale = itrSale
									.next();
							if (transReportSale.getOrigTransactionId().equals(
									transReport.getTransactionId())) {
								flag = false;
								break;
							}
						}
						if (flag)
							transactionListSale.add(transReport);

					}
				}
				transactionList.addAll(transactionListSale);
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public BigInteger getAcquirerFailedTransactionListCountList(String fromDate,
			String toDate, String merchantPayId,
			String paymentType, String acquirer, String currency)
			throws SQLException, ParseException, SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call countAcquirer_FailedSummary(?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}


	public List<TransactionSummaryReport> getAcquirerFailedTransactionListCountList(
			String fromDate, String toDate, String merchantPayId,
		    String paymentType, String acquirer,
			String currency, int start, int length) throws SQLException, ParseException,
			SystemException {

		List<TransactionSummaryReport> transactionListSale = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListEnroll = new ArrayList<TransactionSummaryReport>();
		List<TransactionSummaryReport> transactionListNewOrder = new ArrayList<TransactionSummaryReport>();

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirer_failedSummary(?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setInt(7, start);
				prepStmt.setInt(8, length);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						if (!rs.getString(FieldType.CURRENCY_CODE.toString())
								.isEmpty()) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport
									.setCurrencyName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transReport
								.setPgTxnMessage(rs
										.getString(FieldType.PG_TXN_MESSAGE
												.toString()));

						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						if (!rs.getString(FieldType.PAYMENT_TYPE.toString())
								.isEmpty()) {
							if (PaymentType.getpaymentName(rs
									.getString(FieldType.PAYMENT_TYPE
											.toString())) != null) {
								transReport
										.setPaymentMethod(PaymentType.getpaymentName(rs
												.getString(FieldType.PAYMENT_TYPE
														.toString())));
							}
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							if ((rs.getString(FieldType.PAYMENT_TYPE.getName()))
									.equals(PaymentType.NET_BANKING.getCode())) {
								transReport
										.setCardNo("Net-Banking Transaction");
							} else if ((rs.getString(FieldType.PAYMENT_TYPE
									.getName())).equals(PaymentType.WALLET
									.getCode())) {
								transReport.setCardNo("Wallet Transaction");
							} else {
								transReport
										.setCardNo(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

						}
						if (!(rs.getString(FieldType.PAYMENT_TYPE.getName()))
								.equals(PaymentType.NET_BANKING.getCode())
								|| !(rs.getString(FieldType.PAYMENT_TYPE
										.getName())).equals(PaymentType.WALLET
										.getCode())) {
							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
									.toString()) != null) {
								transReport
										.setInternalCardIssusserBank(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserBank(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}

							if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
									.toString()) != null) {
								transReport
										.setInternalCardIssusserCountry(rs
												.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
														.toString()));
							} else {
								transReport
										.setInternalCardIssusserCountry(CrmFieldConstants.NOT_AVAILABLE
												.getValue());
							}
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.toString()));
						/*if (!rs.getBlob(
								CrmFieldConstants.INTERNAL_REQUEST_FIELDS
										.toString()).equals(null)) {
							Blob blob = (Blob) rs
									.getBlob(CrmFieldConstants.INTERNAL_REQUEST_FIELDS
											.toString());
							byte[] data = Base64.decodeBase64(blob.getBytes(1,
									(int) blob.length()));
							String s = new String(data);
							transReport.setInternalRequestDesc(s);
						} else {
							transReport
									.setInternalRequestDesc(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}*/
						if (transReport.getTxnType().equals(
								TransactionType.SALE.getName())
								|| transReport.getTxnType().equals(
										TransactionType.AUTHORISE.getName())) {
							transactionListSale.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.ENROLL.getName())) {
							transactionListEnroll.add(transReport);
						} else if (transReport.getTxnType().equals(
								TransactionType.NEWORDER.getName())) {
							transactionListNewOrder.add(transReport);
						}
					}
				}

				if (!transactionListEnroll.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListEnroll
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						if (transReport.getOrigTransactionId().equals(0)) {
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport
												.getOrigTransactionId())) {
									flag = false;
									break;
								}
							}
						} else {
							while (itrSale.hasNext()) {
								TransactionSummaryReport transReportSale = itrSale
										.next();
								if (transReportSale.getOrigTransactionId()
										.equals(transReport.getTransactionId())) {
									flag = false;
									break;
								}
							}
						}

						if (flag)
							transactionListSale.add(transReport);
					}
				}

				if (!transactionListNewOrder.isEmpty()) {
					Iterator<TransactionSummaryReport> itr = transactionListNewOrder
							.iterator();
					while (itr.hasNext()) {
						TransactionSummaryReport transReport = itr.next();
						Iterator<TransactionSummaryReport> itrSale = transactionListSale
								.iterator();
						boolean flag = true;
						while (itrSale.hasNext()) {
							TransactionSummaryReport transReportSale = itrSale
									.next();
							if (transReportSale.getOrigTransactionId().equals(
									transReport.getTransactionId())) {
								flag = false;
								break;
							}
						}
						if (flag)
							transactionListSale.add(transReport);

					}
				}
				transactionList.addAll(transactionListSale);
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public List<TransactionSummaryReport> getAcquirerAuthorisedTransactionList(String fromDate, String toDate, String merchantPayId,
			 String paymentType, String acquirer,
			String currency) throws SQLException, ParseException,
			SystemException {

		PropertiesManager propManager = new PropertiesManager();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirer_authoriseSummary(?,?,?,?,?,?)}")) {
				if (fromDate.isEmpty()) {
					prepStmt.setString(1, "");
				} else {
					prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				}
				if (toDate.isEmpty()) {
					prepStmt.setString(2, "");
				} else {
					prepStmt.setString(2, DateCreater.formatToDate(toDate));
				}
				prepStmt.setString(3, merchantPayId);
				//prepStmt.setString(4, userType);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);

				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport
									.setPaymentMethod(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setCurrencyName(propManager
								.getAlphabaticCurrencyCode(rs
										.getString(FieldType.CURRENCY_CODE
												.toString())));
						transReport.setCardNo(rs.getString(FieldType.CARD_MASK
								.toString()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						transReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.toString()));

						if (!rs.getString(FieldType.MOP_TYPE.toString())
								.isEmpty()) {
							if (MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())) != null) {
								transReport.setMopType(MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
							}
						} else {
							transReport
									.setMopType(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						String captureTxnId = rs
								.getString(CrmFieldConstants.CAPTURE_TXN_ID
										.toString());
						transReport.setCaptureTxnId(captureTxnId);
						if (null != captureTxnId) {
							transReport
									.setStatus(StatusType.CAPTURED.getName());
						} else {
							transReport.setStatus(rs.getString(FieldType.STATUS
									.toString()));
						}
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setResponseMsg(rs
								.getString(FieldType.RESPONSE_MESSAGE
										.toString()));
						if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
							transReport.setCustomerName(rs
									.getString(FieldType.CUST_NAME.toString()));
						} else {
							transReport
									.setCustomerName(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						if (rs.getString(FieldType.CARD_MASK.toString()) != null) {
							transReport.setCardNo(rs
									.getString(FieldType.CARD_MASK.toString()));
						} else {
							transReport
									.setCardNo(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}
						transReport.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.toString()));
						if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
								.toString()) != null) {
							transReport
									.setInternalCardIssusserBank(rs
											.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
													.toString()));
						} else {
							transReport
									.setInternalCardIssusserBank(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						if (rs.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
								.toString()) != null) {
							transReport
									.setInternalCardIssusserCountry(rs
											.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
													.toString()));
						} else {
							transReport
									.setInternalCardIssusserCountry(CrmFieldConstants.NOT_AVAILABLE
											.getValue());
						}

						transactionList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}


	public BigInteger getResellerCapturedTransactionCountList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId, String paymentType, String acquirer,
			String currency) throws SQLException, ParseException,
			SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call count_resellerCapturedSummary(?,?,?,?,?,?,?)}")) {
				if (fromDate.isEmpty()) {
					prepStmt.setString(1, "");
				} else {
					prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				}
				if (toDate.isEmpty()) {
					prepStmt.setString(2, "");
				} else {
					prepStmt.setString(2, DateCreater.formatToDate(toDate));
				}
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, resellerId);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
		
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}

	public BigInteger getResellerIncompleteTransactionListCountList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId, String paymentType, String status,
			String acquirer, String currency) throws SQLException,
			ParseException, SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call count_resellerIncompleteSummary(?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, resellerId);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, status);
				prepStmt.setString(7, acquirer);
				prepStmt.setString(8, currency);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}

	public BigInteger getResellerFailedResellerTransactionListCountList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId, String paymentType, String acquirer,
			String currency) throws SQLException, ParseException,
			SystemException {
		  BigInteger total = null;
			try (Connection connection = getConnection()) {
				try (PreparedStatement prepStmt = connection
						.prepareStatement("{call count_resellerFailedSummary(?,?,?,?,?,?,?)}")) {
					prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
					prepStmt.setString(2, DateCreater.formatToDate(toDate));
					prepStmt.setString(3, merchantPayId);
					prepStmt.setString(4, resellerId);
					prepStmt.setString(5, paymentType);
					prepStmt.setString(6, acquirer);
					prepStmt.setString(7, currency);

					try (ResultSet rs = prepStmt.executeQuery()) {
						while (rs.next()) {
							
							total = rs.getBigDecimal(FieldType.COUNT.getName())
									.toBigInteger();
						}
					}
				}

			} catch (SQLException exception) {
				logger.error("Database error", exception);
				throw new SystemException(ErrorType.DATABASE_ERROR,
						ErrorType.DATABASE_ERROR.getResponseMessage());
			}
			return total;
		}

	public BigInteger getResellerCancelledTransactionListCountList(
			String fromDate, String toDate, String merchantPayId,
			String resellerId, String currency) throws SQLException,
			ParseException, SystemException {
		 BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call count_resellerCancelledSummary(?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, resellerId);
				prepStmt.setString(5, currency);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}
}