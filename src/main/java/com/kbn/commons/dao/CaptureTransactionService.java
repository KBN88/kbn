package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.StatusType;

public class CaptureTransactionService {

	private static Logger logger = Logger.getLogger(CaptureTransactionService.class.getName());

	public static final String query = "Select TXN_ID, TXNTYPE, STATUS from TRANSACTION where ORIG_TXN_ID =? and PAY_ID =? and (TXNTYPE ='CAPTURE' or TXNTYPE = 'AUTHORISE')";

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public String getCapturedTransaction(String origTxnId, String payId)
			throws SystemException {
		String txnId = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStament = connection.prepareStatement(query)) {
				prepStament.setString(1, origTxnId);
				prepStament.setString(2, payId);
				try (ResultSet rs = prepStament.executeQuery()) {
					while (rs.next()) {
						if(rs.getString(FieldType.STATUS.getName()).equalsIgnoreCase(StatusType.CAPTURED.getName())){
							txnId = rs.getString(FieldType.TXN_ID.getName());
						}
					}
				}
			}
		} catch (SQLException sQLException) {
			MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(),
					Constants.CRM_LOG_PREFIX.getValue());
			logger.error("Unable to close connection", sQLException);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getInternalMessage());
		}
		return txnId;
	}
}