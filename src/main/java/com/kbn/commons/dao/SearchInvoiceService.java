package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Invoice;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.DateCreater;
import com.kbn.commons.util.PropertiesManager;

public class SearchInvoiceService {

	private static Logger logger = Logger.getLogger(SearchInvoiceService.class
			.getName());
	public List<Invoice> invoiceList = new ArrayList<Invoice>();
	private SimpleDateFormat formatDate = new SimpleDateFormat(
			CrmFieldConstants.DATE_TIME_FORMAT.getValue());

	public SearchInvoiceService() {
	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public List<Invoice> getInvoiceList(String fromDate, String toDate,
			String merchantPayId, String userType, String invoiceNo,
			String customerEmail, String currency, String invoiceType)
			throws SQLException, ParseException, SystemException {

		PropertiesManager propManager = new PropertiesManager();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call invoiceList(?,?,?,?,?,?,?,?)}")) {
				prepStmt.setString(1, DateCreater.formatFromDate(fromDate));
				prepStmt.setString(2, DateCreater.formatToDate(toDate));
				prepStmt.setString(3, merchantPayId);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, invoiceNo);
				prepStmt.setString(6, customerEmail);
				prepStmt.setString(7, currency);
				prepStmt.setString(8, invoiceType);

				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						Invoice invoice = new Invoice();
						invoice.setInvoiceId(rs.getString("invoiceId"));
						invoice.setCreateDate(formatDate.parse(formatDate
								.format(rs.getTimestamp("createDate"))));
						invoice.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME.getName()
										.toString()));
						invoice.setName(rs.getString("name"));
						invoice.setEmail(rs.getString("email"));
						invoice.setInvoiceNo(rs.getString("invoiceNo"));
						invoice.setTotalAmount(rs.getString("totalAmount"));
						invoice.setCurrencyCode(propManager
								.getAlphabaticCurrencyCode(rs
										.getString("currencyCode")));
						invoice.setInvoiceType(rs.getString("invoiceType"));

						invoiceList.add(invoice);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return invoiceList;
	}
}
