package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Statistics;
import com.kbn.commons.util.CrmFieldConstants;

public class AcquirerStatisticsService {
	private static Logger logger = Logger.getLogger(AcquirerStatisticsService.class.getName());
	
	public AcquirerStatisticsService(){
		
	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}
	public Statistics getDashboardValues(String acquirer, String currency) throws SystemException {
		Statistics statistics = new Statistics();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call aquirer_statisticsSummary(?,?)}")) {
				prepStmt.setString(1, acquirer);
				prepStmt.setString(2, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					rs.next();
					statistics.setTotalSuccess(rs
							.getString(CrmFieldConstants.TOTAL_SUCCESS
									.getValue()));
					statistics.setTotalFailed(rs
							.getString(CrmFieldConstants.TOTAL_FAILED
									.getValue()));
					statistics.setTotalRefunded(rs
							.getString(CrmFieldConstants.TOTAL_REFUNDED
									.getValue()));
					statistics.setRefundedAmount(rs
							.getString(CrmFieldConstants.REFUNDED_AMOUNT
									.getValue()));
					statistics.setApprovedAmount(rs
							.getString(CrmFieldConstants.APPROVED_AMOUNT
									.getValue()));
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return statistics;
	}
	
}
