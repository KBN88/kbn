package com.kbn.commons.dao;

import java.io.File;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.kbn.chargeback.action.beans.Chargeback;
import com.kbn.chargeback.action.beans.ChargebackComment;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.CitrusPaySubscription;
import com.kbn.commons.user.DynamicPaymentPage;
import com.kbn.commons.user.Invoice;
import com.kbn.commons.user.LoginHistory;
import com.kbn.commons.user.MerchantComments;
import com.kbn.commons.user.Mop;
import com.kbn.commons.user.MopTransaction;
import com.kbn.commons.user.NotificationEmailer;
import com.kbn.commons.user.Payment;
import com.kbn.commons.user.PendingMappingRequest;
import com.kbn.commons.user.PendingResellerMappingApproval;
import com.kbn.commons.user.PendingUserApproval;
import com.kbn.commons.user.Permissions;
import com.kbn.commons.user.Remittance;
import com.kbn.commons.user.Roles;
import com.kbn.commons.user.ServiceTax;
import com.kbn.commons.user.Settlement;
import com.kbn.commons.user.Surcharge;
import com.kbn.commons.user.SurchargeDetails;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserRecords;
import com.kbn.commons.user.WhitelableBranding;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.actionBeans.BinRange;
import com.kbn.oneclick.Token;
import com.kbn.pg.core.NotificationDetail;
import com.kbn.pg.fraudPrevention.model.FraudPrevention;
import com.kbn.pg.router.RouterRule;
import com.kbn.ticketing.core.HelpTicket;
import com.kbn.ticketing.core.TicketComment;
/**
 * @author neeraj
 *
 */
public class HibernateSessionProvider {
	private static Logger logger = Logger.getLogger(HibernateSessionProvider.class
			.getName());
	private SessionFactory factory;

	private static final String hbmddlAutoSettingName = "hibernate.hbm2ddl.auto";
	private static final String hbmddlAutoSetting = "update";

	private static class SessionHelper {
		private static final HibernateSessionProvider provider = new HibernateSessionProvider();
	}
	private HibernateSessionProvider() {

	    // configures settings from hibernate.cfg.xml
        File xmlConfigFile = new File(new PropertiesManager().getSystemProperty(
                Constants.HIBERNATE_CONFIG_FILE.getValue()));
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure(xmlConfigFile).applySetting(hbmddlAutoSettingName, hbmddlAutoSetting)
                .build();
	try {
			factory = new MetadataSources(registry).addAnnotatedClass(User.class)
												   .addAnnotatedClass(UserRecords.class)
												   .addAnnotatedClass(Roles.class)
												   .addAnnotatedClass(Permissions.class)
												   .addAnnotatedClass(LoginHistory.class)
												   .addAnnotatedClass(Account.class)
												   .addAnnotatedClass(Payment.class)
												   .addAnnotatedClass(Mop.class)
												   .addAnnotatedClass(MopTransaction.class)
												   .addAnnotatedClass(ChargingDetails.class)
												   .addAnnotatedClass(MerchantComments.class)
												   .addAnnotatedClass(Invoice.class)
												   .addAnnotatedClass(Remittance.class)
												   .addAnnotatedClass(DynamicPaymentPage.class)
												   .addAnnotatedClass(Token.class)
												   .addAnnotatedClass(AccountCurrency.class)
												   .addAnnotatedClass(Settlement.class)
												   .addAnnotatedClass(CitrusPaySubscription.class)
												   .addAnnotatedClass(FraudPrevention.class)
												   .addAnnotatedClass(Chargeback.class)
												   .addAnnotatedClass(HelpTicket.class)
												   .addAnnotatedClass(TicketComment.class)
												   .addAnnotatedClass(ChargebackComment.class)
												   .addAnnotatedClass(SurchargeDetails.class)
												   .addAnnotatedClass(Surcharge.class)
												   .addAnnotatedClass(ServiceTax.class)
												   .addAnnotatedClass(PendingUserApproval.class)
											//	   .addAnnotatedClass(NotificationDetail.class)
												   .addAnnotatedClass(BinRange.class)
												   .addAnnotatedClass(PendingResellerMappingApproval.class)
												   .addAnnotatedClass(NotificationEmailer.class)
												   .addAnnotatedClass(NotificationDetail.class )
												   .addAnnotatedClass(PendingMappingRequest.class)
												   .addAnnotatedClass(RouterRule.class)
												   .addAnnotatedClass(WhitelableBranding.class)
												   .buildMetadata().buildSessionFactory();

		} catch (Exception exception) {
			logger.error("Error creating hibernate session" + exception);
			StandardServiceRegistryBuilder.destroy(registry);
			throw exception;
		}
	}

	private SessionFactory getFactory() {
		return factory;
	}

	public static SessionFactory getSessionFactory() {
		return SessionHelper.provider.getFactory();
	}

	public static Session getSession() {
		return getSessionFactory().openSession();
	}

	public static void closeSession(Session session) {
		if(null != session && session.isOpen()) {
			session.close();
			session = null;
		}
	}
}
