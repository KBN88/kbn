package com.kbn.commons.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.exception.SystemException;
import com.kbn.pg.core.NotificationDetail;

public class NotificationDao extends HibernateAbstractDao {
	private static Logger logger = Logger.getLogger(NotificationDao.class.getName());

	public NotificationDao() {
		super();
	}

	private static final String getCompleteNotificationWithPayIdQuery = "from NotificationDetail notifi where notifi.payId = :payId";
	private static final String getUnreadStatusCount = "SELECT COUNT(notifi.status) FROM NotificationDetail notifi WHERE (notifi.payId = :payId) and (notifi.status = 'unread')";
	

	public void create(NotificationDetail notificationDetail) throws DataAccessLayerException {
		super.save(notificationDetail);
	}

	public void delete(NotificationDetail notificationDetail) throws DataAccessLayerException {
		super.delete(notificationDetail);
	}

	public NotificationDetail find(Long id) throws DataAccessLayerException {
		return (NotificationDetail) super.find(NotificationDetail.class, id);
	}

	@SuppressWarnings("rawtypes")
	public List findAll() throws DataAccessLayerException {
		return super.findAll(NotificationDetail.class);
	}

	public void update(NotificationDetail notificationDetail) throws DataAccessLayerException {
		super.saveOrUpdate(notificationDetail);
	}

	@SuppressWarnings("unchecked")
	public List<NotificationDetail> getNotificationsByPayId(String payId) {
		List<NotificationDetail> notificationList = new ArrayList<NotificationDetail>();
		try {
			startOperation();
			List<Object[]> selectNotifications = getSession().createQuery(
					"Select id, createDate,message, status,submittedBy,concernedUser from NotificationDetail notify where notify.payId='" + payId + "'")
					.getResultList();

			for (Object[] objects : selectNotifications) {
				NotificationDetail notificationDetail = new NotificationDetail();
				notificationDetail.setId((Long) objects[0]);
				notificationDetail.setCreateDate((Timestamp)objects[1]);
				notificationDetail.setMessage((String) objects[2]);
				notificationDetail.setStatus((String) objects[3]);
				//notificationDetail.setSubmittedBy((String) objects[4]);
				notificationDetail.setConcernedUser((String) objects[5]);
				notificationList.add(notificationDetail);
			}

			getTx().commit();
			return notificationList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return notificationList;
	}

	@SuppressWarnings("unchecked")
	public List<NotificationDetail> getUnreadNotifications(String payId) {
		List<NotificationDetail> notificationList = new ArrayList<NotificationDetail>();
		try {
			startOperation();
			List<Object[]> selectNotifications = getSession().createQuery(
					"Select id, createDate,message, status,submittedBy,concernedUser from NotificationDetail notify where notify.status='unread' and notify.payId='" + payId + "' ")
					.getResultList();

			for (Object[] objects : selectNotifications) {
				NotificationDetail notificationDetail = new NotificationDetail();
				notificationDetail.setId((Long) objects[0]);
				notificationDetail.setCreateDate((Timestamp)objects[1]);
				notificationDetail.setMessage((String) objects[2]);
				notificationDetail.setStatus((String) objects[3]);
				//notificationDetail.setSubmittedBy((String) objects[4]);
				notificationDetail.setConcernedUser((String) objects[5]);
				notificationList.add(notificationDetail);
			}

			getTx().commit();
			return notificationList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return notificationList;
	}
	public NotificationDetail findPayId(String payId) {
		NotificationDetail notificationDetail = findByPayId(payId);
		return notificationDetail;

	}

	protected NotificationDetail findByPayId(String payId) {

		NotificationDetail notificationDetail = null;
		try {
			startOperation();
			notificationDetail = (NotificationDetail) getSession().createQuery(getCompleteNotificationWithPayIdQuery)
					.setParameter("payId", payId).setCacheable(true).getSingleResult();
			getTx().commit();
			return notificationDetail;
		} catch (NoResultException noResultException) {
			return null;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			logger.error(hibernateException);
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return notificationDetail;
	}
	public long notificationCount(String payId) throws SystemException{
		long count =0;
		try {
		startOperation();
	
		count = (long) getSession().createQuery(getUnreadStatusCount)
									  .setParameter("payId", payId).setCacheable(true)
		                                  .getSingleResult();
		getTx().commit();
		return count;
		
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			logger.error(hibernateException);
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return count;
	}
	
	public void updateStatus(int id) {
		try {
			startOperation();
			getSession()
					.createNativeQuery("update Notificationdetail noti set noti.status = 'read'  where noti.id = :id")
									.setParameter("id", id).executeUpdate();
			getTx().commit();
			
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
	}

}