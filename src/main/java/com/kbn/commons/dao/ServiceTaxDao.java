package com.kbn.commons.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.user.ServiceTax;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.BusinessType;
import com.kbn.commons.util.TDRStatus;

public class ServiceTaxDao extends HibernateAbstractDao {

	public void create(ServiceTax serviceTax) throws DataAccessLayerException {
		super.save(serviceTax);
	}

	public BigDecimal getServiceTax(BusinessType businessType) {
		ServiceTax serviceTax = null;
		try {
			startOperation();
			serviceTax = (ServiceTax) getSession()
					.createQuery(
							"from ServiceTax ST where ST.businessType = :businessType and ST.status = 'ACTIVE' ")
					.setParameter("businessType", businessType).setCacheable(true).uniqueResult();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;;
		}
		return serviceTax.getServiceTax();

	}

	public ServiceTax findServiceTax(String businessType) {
		ServiceTax serviceTax = null;
		try {
			startOperation();
			serviceTax = (ServiceTax) getSession()
					.createQuery(
							"from ServiceTax ST where ST.businessType = :businessType and ST.status = 'ACTIVE' ")
					.setParameter("businessType", businessType).setCacheable(true).uniqueResult();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;
		}
		return serviceTax;

	}
	
	public ServiceTax findPendingRequest(String businessType) {
		ServiceTax serviceTax = null;
		try {
			startOperation();
			serviceTax = (ServiceTax) getSession()
					.createQuery(
							"from ServiceTax ST where ST.businessType = :businessType and ST.status = 'PENDING' ")
					.setParameter("businessType", businessType).setCacheable(true).uniqueResult();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;
		}
		return serviceTax;

	}
	
	public BigDecimal findServiceTaxByPayId(String payId) {
		ServiceTax serviceTax = null;
		
		User user = new User();
		UserDao userDao = new UserDao();
		user = userDao.findPayId(payId);
		String businessType = user.getIndustryCategory();
		try {
			startOperation();
			serviceTax = (ServiceTax) getSession()
					.createQuery(
							"from ServiceTax ST where ST.businessType = :businessType and ST.status = 'ACTIVE' ")
					.setParameter("businessType", businessType).setCacheable(true).uniqueResult();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;
		}
		return serviceTax.getServiceTax();

	}
	
	@SuppressWarnings("unchecked")
	public void addOrUpdateServiceTax(String businessType, BigDecimal tax) {

		ServiceTax serviceTaxFromDb = new ServiceTax();
		serviceTaxFromDb = findServiceTax(businessType);
		Session session = null;
		Date currentDate = new Date();

		if (serviceTaxFromDb != null) {

			try {
				session = HibernateSessionProvider.getSession();
				Transaction tx = session.beginTransaction();
				Long id = serviceTaxFromDb.getId();
				session.load(serviceTaxFromDb, serviceTaxFromDb.getId());
				ServiceTax serviceTaxDetails = (ServiceTax) session.get(ServiceTax.class, id);
				serviceTaxDetails.setStatus(TDRStatus.INACTIVE);
				serviceTaxDetails.setUpdatedDate(currentDate);
				session.update(serviceTaxDetails);
				tx.commit();
				session.close();

			} catch (HibernateException e) {
				e.printStackTrace();
			} finally {

			}
		}

		ServiceTax newServiceTax = new ServiceTax();
		newServiceTax.setServiceTax(tax);
		newServiceTax.setBusinessType(businessType);
		newServiceTax.setStatus(TDRStatus.ACTIVE);
		newServiceTax.setCreatedDate(currentDate);
		newServiceTax.setUpdatedDate(currentDate);

		create(newServiceTax);

	}
	
	
	@SuppressWarnings("unchecked")
	public List<ServiceTax> getServiceTaxList() {
		List<ServiceTax> serviceTaxList = new ArrayList<ServiceTax>();
		serviceTaxList = null;	
		try {
			
			startOperation();

			serviceTaxList = getSession().createQuery("from ServiceTax ST where ST.status='ACTIVE'")
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return serviceTaxList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return serviceTaxList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ServiceTax> getPendingServiceTaxList() {
		List<ServiceTax> serviceTaxList = new ArrayList<ServiceTax>();
		serviceTaxList = null;	
		try {
			
			startOperation();

			serviceTaxList = getSession().createQuery("from ServiceTax ST where ST.status='PENDING'")
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return serviceTaxList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return serviceTaxList;
	}

}
