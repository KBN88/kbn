package com.kbn.commons.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionSnapshotReport;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.User;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.DateCreater;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.TransactionManager;
import com.kbn.crm.charging.TdrCalculator;
import com.kbn.pg.core.NodalAccountMapper;

/**
 * @author Puneet
 *
 */

public class TransactionReportService {

	private static Logger logger = Logger
			.getLogger(TransactionReportService.class.getName());

	private static final String getTotalRefundTxnQuery = "Select SUM(Amount) as REFUND_AMOUNT from TRANSACTION where TXNTYPE='REFUND' and STATUS='Captured' and ORIG_TXN_ID=?  ";

	public TransactionReportService() {
	}
	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}
	public List<TransactionSummaryReport> getTransactionRefundReport(
			String fromDate, String toDate, String payid, String userType,
			String paymentType, String acquirer, String currency, int start, int length)
			throws SystemException {
		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionRefundList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call refundReport(?,?,?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payid);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				prepStmt.setInt(8, start);
				prepStmt.setInt(9, length);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport refundReport = new TransactionSummaryReport();
						refundReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						refundReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						refundReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME
										.toString()));
						refundReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.TXN_DATE
										.toString())));
						refundReport.setApprovedAmount(rs
								.getString(CrmFieldConstants.TXN_AMOUNT
										.toString()));
						refundReport.setRefundDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.REFUND_DATE
										.toString())));
						refundReport.setRefundedAmount(rs
								.getString(CrmFieldConstants.REFUND_AMOUNT
										.toString()));
						refundReport.setRefundableAmount(rs
								.getString(CrmFieldConstants.REFUNDABLE_AMOUNT
										.toString()));
						refundReport.setPaymentMethod(PaymentType
								.getpaymentName(rs
										.getString(FieldType.PAYMENT_TYPE
												.toString()))
								+ " - "
								+ MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							refundReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							refundReport.setCurrencyName("");
						}
						refundReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						refundReport.setInternalUserEmail(rs
								.getString(CrmFieldType.INTERNAL_USER_EMAIL
										.toString()));
						refundReport.setInternalCustIp(rs
								.getString(CrmFieldType.INTERNAL_CUST_IP
										.toString()));
						refundReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						refundReport
								.setSettlementStatus(rs
										.getString(CrmFieldConstants.STATUS
												.toString()));
						refundReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.toString()));

						transactionRefundList.add(refundReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionRefundList;
	}

	public List<TransactionSummaryReport> getTransactionList(String fromDate,
			String toDate, String payId, String paymentType, String acquirer,
			String currency, User user, int start, int length) throws SystemException {

		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionSummaryList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		TdrCalculator tdrCalculator = new TdrCalculator();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call transactionsAllMerch(?,?,?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setString(7, user.getUserType().toString());
				prepStmt.setInt(8, start);
				prepStmt.setInt(9, length);
				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setTxnDate(rs
								.getString(CrmFieldConstants.CREATE_DATE
										.toString()));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport.setPaymentMethod("");
						}

						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport.setMopType("");
						}

						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport.setCurrencyName("");
						}

						transReport.setApprovedAmount(rs
								.getString(CrmFieldConstants.CAPTURED_AMOUNT
										.toString()));

						transReport = getRefundAmount(transReport);
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.getName()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.getName()));
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.getName()));
						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.getName()));

						transReport.setChargebackAmount(rs
								.getString(CrmFieldConstants.CHARGEBACK_AMOUNT
										.toString()));

						transReport = tdrCalculator.setTdrValues(transReport,
								user);

						// Set date again as per display format
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));

						transReport.setAuthCode(rs
								.getString(FieldType.AUTH_CODE.toString()));
						transReport
								.setBusinessName(rs
										.getString(CrmFieldType.BUSINESS_NAME
												.getName()));

						// Set Status for processed button in summary report
						transReport
								.setSettlementStatus(rs
										.getString(CrmFieldConstants.STATUS
												.toString()));

						transactionSummaryList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionSummaryList;
	}

	private TransactionSummaryReport getRefundAmount(
			TransactionSummaryReport transactionSummaryReport)
			throws SystemException {
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement(getTotalRefundTxnQuery)) {
				prepStmt.setString(1,
						transactionSummaryReport.getTransactionId());
				try (ResultSet rs = prepStmt.executeQuery()) {
					rs.next();
					if (null != rs.getString(CrmFieldConstants.REFUND_AMOUNT
							.toString()))
						transactionSummaryReport.setRefundedAmount(rs
								.getString(CrmFieldConstants.REFUND_AMOUNT
										.toString()));
					else
						transactionSummaryReport.setRefundedAmount("0.00");
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionSummaryReport;
	}

	public List<TransactionSnapshotReport> getSnapshotReport(String acquirer,
			String paymentType, String fromDate, String toDate, String payId)
			throws SystemException {

		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSnapshotReport> transactionList = new ArrayList<TransactionSnapshotReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.INPUT_DATE_FORMAT.getValue());
		try (Connection connection = getConnection()) {
			toDate = DateCreater.formatToDate(toDate);
			fromDate = DateCreater.formatFromDate(fromDate);
			CallableStatement cs;
			if (acquirer.equals(CrmFieldConstants.ALL.toString())
					&& paymentType.equals(CrmFieldConstants.ALL.toString())) {
				cs = connection
						.prepareCall("{call merchtransactionstatsAll(?,?,?)}");
				cs.setString(1, fromDate);
				cs.setString(2, toDate);
				cs.setString(3, payId);
			} else if ((acquirer.equals(CrmFieldConstants.ALL.toString()))) {
				cs = connection
						.prepareCall("{call merchtransactionstatsPayment(?,?,?,?)}");
				cs.setString(1, paymentType);
				cs.setString(2, fromDate);
				cs.setString(3, toDate);
				cs.setString(4, payId);
			} else if ((paymentType.equals(CrmFieldConstants.ALL.toString()))) {
				cs = connection
						.prepareCall("{call merchtransactionstatsAccount(?,?,?,?)}");
				cs.setString(1, fromDate);
				cs.setString(2, toDate);
				cs.setString(3, payId);
				cs.setString(4, acquirer);
			} else {
				cs = connection
						.prepareCall("{call merchtransactionstats(?,?,?,?,?)}");
				cs.setString(1, fromDate);
				cs.setString(2, toDate);
				cs.setString(3, payId);
				cs.setString(4, acquirer);
				cs.setString(5, paymentType);
			}

			try (ResultSet rs = cs.executeQuery()) {

				while (rs.next()) {
					TransactionSnapshotReport transactionSum = new TransactionSnapshotReport();

					int total = rs.getInt(CrmFieldConstants.TOTAL_TRANSACTIONS
							.getValue());
					if (total == 0) {
						continue;
					}
					int pending = rs.getInt(CrmFieldConstants.TOTAL_PENDING
							.getValue());
					int success = rs.getInt(CrmFieldConstants.TOTAL_SUCCESS
							.getValue());

					int dropped = rs.getInt(CrmFieldConstants.TOTAL_DROPPED
							.getValue());
					int baunced = rs.getInt(CrmFieldConstants.TOTAL_BAUNCED
							.getValue());

					transactionSum.setPaymentMethod(PaymentType
							.getpaymentName(rs
									.getString(CrmFieldConstants.PAYMENT_METHOD
											.getValue())));
					transactionSum.setCurrency(propManager
							.getAlphabaticCurrencyCode(rs
									.getString(FieldType.CURRENCY_CODE
											.toString())));
					transactionSum.setTotalTransactions(total);
					transactionSum.setTotalDropped(dropped);
					transactionSum.setTotalBaunced(baunced);
					transactionSum.setSuccessTransactions(success);
					transactionSum.setFailedTransactions(pending);
					transactionSum
							.setDate(formatDate.format(rs
									.getTimestamp(CrmFieldConstants.TXN_DATE
											.getValue())));
					transactionSum.setApprovedAmount(rs
							.getString(CrmFieldConstants.APPROVED_AMOUNT
									.getValue()));

					transactionList.add(transactionSum);
				}
			}
			cs.close();
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}

		return transactionList;

	}

	public List<TransactionSummaryReport> getResellerTransactionList(
			String fromDate, String toDate, String payId, String paymentType,
			String acquirer, String currency, User user, int start,int length) throws SystemException {

		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionSummaryList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		TdrCalculator tdrCalculator = new TdrCalculator();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call resellerTransactionsAllMerch(?,?,?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setString(7, user.getResellerId().toString());
				prepStmt.setInt(8, start);
				prepStmt.setInt(9, length);
				

				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setTxnDate(rs
								.getString(CrmFieldConstants.CREATE_DATE
										.toString()));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport.setPaymentMethod("");
						}

						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport.setMopType("");
						}

						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport.setCurrencyName("");
						}

						transReport.setApprovedAmount(rs
								.getString(CrmFieldConstants.CAPTURED_AMOUNT
										.toString()));

						transReport = getRefundAmount(transReport);
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.getName()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.getName()));
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.getName()));
						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.getName()));

						transReport.setChargebackAmount(rs
								.getString(CrmFieldConstants.CHARGEBACK_AMOUNT
										.toString()));

						transReport = tdrCalculator.setTdrValues(transReport,
								user);

						// Set date again as per display format
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));

						transReport.setAuthCode(rs
								.getString(FieldType.AUTH_CODE.toString()));
						transReport
								.setBusinessName(rs
										.getString(CrmFieldType.BUSINESS_NAME
												.getName()));

						transactionSummaryList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionSummaryList;
	}

	public List<TransactionSummaryReport> getResellerTransactionRefundReport(
			String fromDate, String toDate, String payid, String resellerId,
			String paymentType, String acquirer, String currency)
			throws SystemException {
		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionRefundList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call resellerRefundReport(?,?,?,?,?,?,?)}")) {

				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payid);
				prepStmt.setString(4, resellerId);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);

				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport refundReport = new TransactionSummaryReport();
						refundReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						refundReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						refundReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME
										.toString()));
						refundReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.TXN_DATE
										.toString())));
						refundReport.setApprovedAmount(rs
								.getString(CrmFieldConstants.TXN_AMOUNT
										.toString()));
						refundReport.setRefundDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.REFUND_DATE
										.toString())));
						refundReport.setRefundedAmount(rs
								.getString(CrmFieldConstants.REFUND_AMOUNT
										.toString()));
						refundReport.setRefundableAmount(rs
								.getString(CrmFieldConstants.REFUNDABLE_AMOUNT
										.toString()));
						refundReport.setPaymentMethod(PaymentType
								.getpaymentName(rs
										.getString(FieldType.PAYMENT_TYPE
												.toString()))
								+ " - "
								+ MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							refundReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							refundReport.setCurrencyName("");
						}
						refundReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						refundReport.setInternalUserEmail(rs
								.getString(CrmFieldType.INTERNAL_USER_EMAIL
										.toString()));
						refundReport.setInternalCustIp(rs
								.getString(CrmFieldType.INTERNAL_CUST_IP
										.toString()));
						refundReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.toString()));
						transactionRefundList.add(refundReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionRefundList;
	}

	public List<TransactionSummaryReport> getResellerMISReportList(
			String dateFrom, String dateTo, String merchantPayId,
			String acquirer, User sessionUser) {

		return null;
	}

	public List<TransactionSummaryReport> getMISReportList(String fromDate,
			String toDate, String payId, String acquirer, User user, int start ,int length )
			throws SystemException {

		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionSummaryList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		SimpleDateFormat misReportDateFormate = new SimpleDateFormat(
				CrmFieldConstants.INPUT_DATE_FOR_MIS_REPORT.getValue());
		TdrCalculator tdrCalculator = new TdrCalculator();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call misReports(?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, acquirer);
				prepStmt.setString(5, user.getUserType().toString());
				prepStmt.setInt(6, start);
				prepStmt.setInt(7, length);
				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						String strDate = misReportDateFormate
								.format(new Date());

						transReport
								.setBusinessName(rs
										.getString(CrmFieldType.BUSINESS_NAME
												.getName()));
						transReport.setAccountNumber(rs
								.getString(CrmFieldType.ACCOUNT_NO.getName()));
						transReport.setIfscCode(rs
								.getString(CrmFieldType.IFSC_CODE.getName()));
						transReport.setBankName(rs
								.getString(CrmFieldType.BANK_NAME.getName()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.getName()));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setTxnDate(rs
								.getString(CrmFieldConstants.CREATE_DATE
										.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.getName()));
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.getName()));
						transReport.setCurrentDate(strDate);
						String acquirerType = transReport.getAcquirer();
						if (acquirerType.equals(NodalAccountMapper.FSS
								.getCode())) {
							transReport
									.setBankRecieveFund(CrmFieldConstants.FSS_BANK
											.getValue());
						} else if (acquirerType.equals(NodalAccountMapper.AMEX
								.getCode())) {
							transReport
									.setBankRecieveFund(CrmFieldConstants.AMEX_BANK
											.getValue());
						} else if (acquirerType
								.equals(NodalAccountMapper.DIREC_PAY.getCode())) {
							transReport
									.setBankRecieveFund(CrmFieldConstants.DIREC_PAY_BANK
											.getValue());
						} else if (acquirerType
								.equals(NodalAccountMapper.MOBIKWIK.getCode())) {
							transReport
									.setBankRecieveFund(CrmFieldConstants.MOBIKWIK_BANK
											.getValue());
						}

						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport.setCurrencyName("");
						}
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport.setPaymentMethod("");
						}

						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport.setMopType("");
						}

						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.getName()));
						if (!acquirerType.equals(NodalAccountMapper.CITRUS_PAY
								.getCode())) {
							transReport = tdrCalculator
									.setMisTransactionReportCalculate(
											transReport);

						}

						// Set date again as per display format
						transReport.setTxnDate(misReportDateFormate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transactionSummaryList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionSummaryList;
	}

	public List<TransactionSummaryReport> getResellerMISCitrusReportList(
			String dateFrom, String dateTo, String merchantPayId,
			String acquirer, User sessionUser) {

		return null;
	}

	public List<TransactionSummaryReport> getMISCitrusReportList(
			String fromDate, String toDate, String payId, String acquirer,
			User user, int start ,int length ) throws SystemException {
		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionSummaryList = new ArrayList<TransactionSummaryReport>();
		Map<String, TransactionSummaryReport> mapTransactionSummaryList = new HashMap<String, TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		SimpleDateFormat misReportDateFormate = new SimpleDateFormat(
				CrmFieldConstants.INPUT_DATE_FOR_MIS_REPORT.getValue());
		TdrCalculator tdrCalculator = new TdrCalculator();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call misReports(?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, acquirer);
				prepStmt.setString(5, user.getUserType().toString());
				prepStmt.setInt(6, start);
				prepStmt.setInt(7, length);
				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						String strDate = misReportDateFormate
								.format(new Date());
						transReport
								.setBusinessName(rs
										.getString(CrmFieldType.BUSINESS_NAME
												.getName()));
						transReport.setAccountNumber(rs
								.getString(CrmFieldType.ACCOUNT_NO.getName()));
						transReport.setIfscCode(rs
								.getString(CrmFieldType.IFSC_CODE.getName()));
						transReport.setBankName(rs
								.getString(CrmFieldType.BANK_NAME.getName()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.getName()));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setTxnDate(rs
								.getString(CrmFieldConstants.CREATE_DATE
										.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.getName()));
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.getName()));
						transReport.setCurrentDate(strDate);
						transReport.setTxnRefNumber(getTxnRefNumber());
						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport.setCurrencyName("");
						}
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport.setPaymentMethod("");
						}

						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport.setMopType("");
						}

						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.getName()));

						transReport = tdrCalculator
								.setMisTransactionReportCalculate(transReport);
						transReport.setTotalAmountPayToMerchant(transReport
								.getTotalPayoutToNodal());
						String ifcsCode = rs.getString(
								CrmFieldType.IFSC_CODE.getName()).substring(0,
								4);
						if (ifcsCode.equals(CrmFieldConstants.IFSC_CODE
								.getValue())) {
							transReport.setMsgType("R41");
						} else {
							transReport.setMsgType("NO6");
						}

						// Parse date as per display format
						transReport.setTxnDate(misReportDateFormate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transactionSummaryList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}

		for (TransactionSummaryReport transactionSummaryReport : transactionSummaryList) {
			String payid = transactionSummaryReport.getPayId();

			// Check condition when TotalAmountPayToMerchant is Null
			String amount = transactionSummaryReport
					.getTotalAmountPayToMerchant();
			if (amount == null) {
				transactionSummaryReport.setTotalAmountPayToMerchant("0");
			}
			if (!mapTransactionSummaryList.containsKey(payid)) {
				mapTransactionSummaryList.put(payid, transactionSummaryReport);
			} else {

				BigDecimal totalMerchantNodalAmount = new BigDecimal(
						transactionSummaryReport.getTotalAmountPayToMerchant());
				TransactionSummaryReport transactionSummaryReportFromMap = mapTransactionSummaryList
						.get(payid);
				transactionSummaryReportFromMap
						.setTotalAmountPayToMerchant(totalMerchantNodalAmount
								.add(new BigDecimal(
										transactionSummaryReportFromMap
												.getTotalAmountPayToMerchant()))
								.toString());
				mapTransactionSummaryList.put(payid,
						transactionSummaryReportFromMap);
			}
		}
		transactionSummaryList.clear();

		for (Entry<String, TransactionSummaryReport> entry : mapTransactionSummaryList
				.entrySet()) {
			transactionSummaryList.add(entry.getValue());
		}
		return transactionSummaryList;
	}

	private String getTxnRefNumber() {
		return TransactionManager.getNewTransactionId();
	}
	public BigInteger getTransactionListCountList(String fromDate,
			String toDate, String payId, String paymentType, String acquirer,
			String currency, User user) throws SystemException {
	    	BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call countTransactionsAllMerch(?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setString(7, user.getUserType().toString());
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}
	public List<TransactionSummaryReport> getRemittanceTransactionList(String fromDate,
			String toDate, String payId, String paymentType, String acquirer,
			String currency, User user) throws SystemException {

		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionSummaryList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		TdrCalculator tdrCalculator = new TdrCalculator();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call remittanceTransactionAmount(?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setString(7, user.getUserType().toString());
				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setTxnDate(rs
								.getString(CrmFieldConstants.CREATE_DATE
										.toString()));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport.setPaymentMethod("");
						}

						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport.setMopType("");
						}

						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport.setCurrencyName("");
						}

						transReport.setApprovedAmount(rs
								.getString(CrmFieldConstants.CAPTURED_AMOUNT
										.toString()));

						transReport = getRefundAmount(transReport);
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.getName()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.getName()));
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.getName()));
						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.getName()));

						transReport.setChargebackAmount(rs
								.getString(CrmFieldConstants.CHARGEBACK_AMOUNT
										.toString()));

						transReport = tdrCalculator.setTdrValues(transReport,
								user);

						// Set date again as per display format
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));

						transReport.setAuthCode(rs
								.getString(FieldType.AUTH_CODE.toString()));
						transReport
								.setBusinessName(rs
										.getString(CrmFieldType.BUSINESS_NAME
												.getName()));

						// Set Status for processed button in summary report
						transReport
								.setSettlementStatus(rs
										.getString(CrmFieldConstants.STATUS
												.toString()));

						transactionSummaryList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionSummaryList;
	}
	public BigInteger getTransactionRefundReportCountList(String fromDate, String toDate, String payid, String userType,
			String paymentType, String acquirer, String currency)
			throws SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call countRefundReport(?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payid);
				prepStmt.setString(4, userType);
				prepStmt.setString(5, paymentType);
				prepStmt.setString(6, acquirer);
				prepStmt.setString(7, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}
	public BigInteger getMISReportListCountList(String fromDate,
			String toDate, String payId, String acquirer, User user)
			throws SystemException {
		  BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call countMisReports(?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, acquirer);
				prepStmt.setString(5, user.getUserType().toString());
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}
	public BigInteger getAcquirerTransactionRefundReportCountList(String fromDate, String toDate, String payid,
			String paymentType, String acquirer, String currency)
			throws SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirer_countRefundReport(?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payid);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}
	public List<TransactionSummaryReport> getAcquirerTransactionRefundReport(
			String fromDate, String toDate, String payid, 
			String paymentType, String acquirer, String currency, int start, int length)
			throws SystemException {
		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionRefundList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirer_refundReport(?,?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payid);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setInt(7, start);
				prepStmt.setInt(8, length);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport refundReport = new TransactionSummaryReport();
						refundReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						refundReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						refundReport.setBusinessName(rs
								.getString(CrmFieldType.BUSINESS_NAME
										.toString()));
						refundReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.TXN_DATE
										.toString())));
						refundReport.setApprovedAmount(rs
								.getString(CrmFieldConstants.TXN_AMOUNT
										.toString()));
						refundReport.setRefundDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.REFUND_DATE
										.toString())));
						refundReport.setRefundedAmount(rs
								.getString(CrmFieldConstants.REFUND_AMOUNT
										.toString()));
						refundReport.setRefundableAmount(rs
								.getString(CrmFieldConstants.REFUNDABLE_AMOUNT
										.toString()));
						refundReport.setPaymentMethod(PaymentType
								.getpaymentName(rs
										.getString(FieldType.PAYMENT_TYPE
												.toString()))
								+ " - "
								+ MopType.getmopName(rs
										.getString(FieldType.MOP_TYPE
												.toString())));
						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							refundReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							refundReport.setCurrencyName("");
						}
						refundReport.setPayId(rs.getString(FieldType.PAY_ID
								.toString()));
						refundReport.setInternalUserEmail(rs
								.getString(CrmFieldType.INTERNAL_USER_EMAIL
										.toString()));
						refundReport.setInternalCustIp(rs
								.getString(CrmFieldType.INTERNAL_CUST_IP
										.toString()));
						refundReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						refundReport
								.setSettlementStatus(rs
										.getString(CrmFieldConstants.STATUS
												.toString()));
						refundReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.toString()));

						transactionRefundList.add(refundReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionRefundList;
	}
	public BigInteger getAcquirerTransactionListCountList(String fromDate,
			String toDate, String payId, String paymentType, String acquirer,
			String currency, User user) throws SystemException {
	    	BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call countAcquirer_TransactionsAllMerch(?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setString(7, user.getUserType().toString());
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}
	public List<TransactionSummaryReport> getAcquirerTransactionList(String fromDate,
			String toDate, String payId, String paymentType, String acquirer,
			String currency, User user, int start, int length) throws SystemException {

		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionSummaryList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		TdrCalculator tdrCalculator = new TdrCalculator();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirer_transactionsAllMerch(?,?,?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setString(7, user.getUserType().toString());
				prepStmt.setInt(8, start);
				prepStmt.setInt(9, length);
				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();

						transReport.setOid(rs.getString(FieldType.OID
								.toString()));
						transReport.setTxnDate(rs
								.getString(CrmFieldConstants.CREATE_DATE
										.toString()));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setOrderId(rs.getString(FieldType.ORDER_ID
								.toString()));
						transReport.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.toString()));
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport.setPaymentMethod("");
						}

						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport.setMopType("");
						}

						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport.setCurrencyName("");
						}

						transReport.setApprovedAmount(rs
								.getString(CrmFieldConstants.CAPTURED_AMOUNT
										.toString()));

						transReport = getRefundAmount(transReport);
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.getName()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.getName()));
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.getName()));
						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.getName()));

						transReport.setChargebackAmount(rs
								.getString(CrmFieldConstants.CHARGEBACK_AMOUNT
										.toString()));

						transReport = tdrCalculator.setTdrValues(transReport,
								user);

						// Set date again as per display format
						transReport.setTxnDate(formatDate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));

						transReport.setAuthCode(rs
								.getString(FieldType.AUTH_CODE.toString()));
						transReport
								.setBusinessName(rs
										.getString(CrmFieldType.BUSINESS_NAME
												.getName()));

						// Set Status for processed button in summary report
						transReport
								.setSettlementStatus(rs
										.getString(CrmFieldConstants.STATUS
												.toString()));

						transactionSummaryList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionSummaryList;
	}
	public BigInteger getAcquirerMISReportListCountList(String fromDate,
			String toDate, String payId, String acquirer, User user)
			throws SystemException {
		  BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirer_countMisReports(?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, acquirer);
				prepStmt.setString(5, user.getUserType().toString());
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}
	public List<TransactionSummaryReport> getAcquirerMISReportList(String fromDate,
			String toDate, String payId, String acquirer, User user, int start ,int length )
			throws SystemException {

		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSummaryReport> transactionSummaryList = new ArrayList<TransactionSummaryReport>();
		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		SimpleDateFormat misReportDateFormate = new SimpleDateFormat(
				CrmFieldConstants.INPUT_DATE_FOR_MIS_REPORT.getValue());
		TdrCalculator tdrCalculator = new TdrCalculator();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call acquirer_misReports(?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, acquirer);
				prepStmt.setString(5, user.getUserType().toString());
				prepStmt.setInt(6, start);
				prepStmt.setInt(7, length);
				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						TransactionSummaryReport transReport = new TransactionSummaryReport();
						String strDate = misReportDateFormate
								.format(new Date());

						transReport
								.setBusinessName(rs
										.getString(CrmFieldType.BUSINESS_NAME
												.getName()));
						transReport.setAccountNumber(rs
								.getString(CrmFieldType.ACCOUNT_NO.getName()));
						transReport.setIfscCode(rs
								.getString(CrmFieldType.IFSC_CODE.getName()));
						transReport.setBankName(rs
								.getString(CrmFieldType.BANK_NAME.getName()));
						transReport.setPayId(rs.getString(FieldType.PAY_ID
								.getName()));
						transReport.setTransactionId(rs
								.getString(FieldType.TXN_ID.toString()));
						transReport.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transReport.setTxnDate(rs
								.getString(CrmFieldConstants.CREATE_DATE
										.toString()));
						transReport.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.toString()));
						transReport.setAcquirer(rs
								.getString(FieldType.ACQUIRER_TYPE.getName()));
						transReport.setTxnType(rs.getString(FieldType.TXNTYPE
								.getName()));
						transReport.setCurrentDate(strDate);
						String acquirerType = transReport.getAcquirer();
						if (acquirerType.equals(NodalAccountMapper.FSS
								.getCode())) {
							transReport
									.setBankRecieveFund(CrmFieldConstants.FSS_BANK
											.getValue());
						} else if (acquirerType.equals(NodalAccountMapper.AMEX
								.getCode())) {
							transReport
									.setBankRecieveFund(CrmFieldConstants.AMEX_BANK
											.getValue());
						} else if (acquirerType
								.equals(NodalAccountMapper.DIREC_PAY.getCode())) {
							transReport
									.setBankRecieveFund(CrmFieldConstants.DIREC_PAY_BANK
											.getValue());
						} else if (acquirerType
								.equals(NodalAccountMapper.MOBIKWIK.getCode())) {
							transReport
									.setBankRecieveFund(CrmFieldConstants.MOBIKWIK_BANK
											.getValue());
						}

						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							transReport.setCurrencyName(propManager
									.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
						} else {
							transReport.setCurrencyName("");
						}
						if (null != rs.getString(FieldType.PAYMENT_TYPE
								.toString())) {
							transReport.setPaymentMethod(PaymentType
									.getpaymentName(rs
											.getString(FieldType.PAYMENT_TYPE
													.toString())));
						} else {
							transReport.setPaymentMethod("");
						}

						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
							transReport.setMopType(MopType.getmopName(rs
									.getString(FieldType.MOP_TYPE.toString())));
						} else {
							transReport.setMopType("");
						}

						transReport.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.getName()));
						if (!acquirerType.equals(NodalAccountMapper.CITRUS_PAY
								.getCode())) {
							transReport = tdrCalculator
									.setMisTransactionReportCalculate(
											transReport);

						}

						// Set date again as per display format
						transReport.setTxnDate(misReportDateFormate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transactionSummaryList.add(transReport);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionSummaryList;
	}
	public BigInteger getResellerTransactionListCountList(String fromDate,
			String toDate, String payId, String paymentType, String acquirer,
			String currency, User user) throws SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call count_resellerTransactionsAllMerch(?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, paymentType);
				prepStmt.setString(5, acquirer);
				prepStmt.setString(6, currency);
				prepStmt.setString(7, user.getResellerId().toString());
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}

}