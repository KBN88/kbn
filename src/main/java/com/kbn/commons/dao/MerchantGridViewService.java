package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.MerchantDetails;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.UserStatusType;

public class MerchantGridViewService {
	private static Logger logger = Logger.getLogger(TransactionReportService.class.getName());
	private final static String query = "select payId, businessName, emailId, userStatus,Mobile,registrationDate,userType from User where (userType='MERCHANT') order by payId ";
	private final static String querySubUser = "select p.payId,p.emailId, p.userStatus,p.Mobile,p.registrationDate,p.userType,u.businessName from User p inner join User u ON (p.parentPayId = u.payId) where (p.userType='SUBUSER') order by p.payId ";
	private final static String querySubUserList = "select payId, emailId, userStatus,Mobile,registrationDate,userType, (select businessName from User  where (userType='MERCHANT' and payId =?)) As 'businessName' from User where (userType='SUBUSER' and parentPayId =?) GROUP BY parentPayId ";
	private final static String businesTypeQuery = "select payId, businessName, emailId, userStatus,Mobile,registrationDate,userType from User where (userType='MERCHANT' and industryCategory =?) order by payId ";
	private final static String rsellerListQuery = "select payId, businessName, emailId, userStatus,Mobile,registrationDate,userType from User where (userType='Reseller') order by payId ";
	private final static String reselerQuery = "select payId,resellerId, businessName, emailId, userStatus,Mobile,registrationDate,userType from User where userType='MERCHANT' and resellerId=? ";

	public MerchantGridViewService() {
	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public List<MerchantDetails> getAllMerchants() throws SystemException {
		List<MerchantDetails> merchants = new ArrayList<MerchantDetails>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection.prepareStatement(query)) {
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {

						MerchantDetails merchant = new MerchantDetails();
						merchant.setPayId(rs.getString("payId"));
						merchant.setBusinessName(rs.getString("businessName"));
						merchant.setEmailId(rs.getString("emailId"));
						merchant.setMobile(rs.getString("Mobile"));
						merchant.setRegistrationDate(rs.getString("registrationDate"));
						merchant.setUserType(rs.getString("userType"));
						String status = rs.getString("userStatus");

						if (status != null) {
							UserStatusType userStatus = UserStatusType.valueOf(status);
							merchant.setStatus(userStatus);
						}
						merchants.add(merchant);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return merchants;
	}

	public List<MerchantDetails> getAllReselerMerchants(String resellerId) throws SystemException {
		List<MerchantDetails> merchants = new ArrayList<MerchantDetails>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection.prepareStatement(reselerQuery)) {
				prepStmt.setString(1, resellerId);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						MerchantDetails merchant = new MerchantDetails();
						merchant.setPayId(rs.getString("payId"));
						merchant.setResellerId(rs.getString("resellerId"));
						merchant.setBusinessName(rs.getString("businessName"));
						merchant.setEmailId(rs.getString("emailId"));
						merchant.setMobile(rs.getString("Mobile"));
						merchant.setRegistrationDate(rs.getString("registrationDate"));
						merchant.setUserType(rs.getString("userType"));
						String status = rs.getString("userStatus");

						if (status != null) {
							UserStatusType userStatus = UserStatusType.valueOf(status);
							merchant.setStatus(userStatus);
						}
						merchants.add(merchant);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return merchants;
	}

	public List<MerchantDetails> getAllReseller() throws SystemException {
		List<MerchantDetails> merchants = new ArrayList<MerchantDetails>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection.prepareStatement(rsellerListQuery)) {
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {

						MerchantDetails merchant = new MerchantDetails();
						merchant.setPayId(rs.getString("payId"));
						merchant.setBusinessName(rs.getString("businessName"));
						merchant.setEmailId(rs.getString("emailId"));
						merchant.setMobile(rs.getString("Mobile"));
						merchant.setRegistrationDate(rs.getString("registrationDate"));
						merchant.setUserType(rs.getString("userType"));
						String status = rs.getString("userStatus");

						if (status != null) {
							UserStatusType userStatus = UserStatusType.valueOf(status);
							merchant.setStatus(userStatus);
						}
						merchants.add(merchant);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return merchants;
	}

	public List<MerchantDetails> getAllMerchants(String businessType) throws SystemException {
		List<MerchantDetails> merchants = new ArrayList<MerchantDetails>();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection.prepareStatement(businesTypeQuery)) {
				prepStmt.setString(1, businessType);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {

						MerchantDetails merchant = new MerchantDetails();
						merchant.setPayId(rs.getString("payId"));
						merchant.setBusinessName(rs.getString("businessName"));
						merchant.setEmailId(rs.getString("emailId"));
						merchant.setMobile(rs.getString("Mobile"));
						merchant.setRegistrationDate(rs.getString("registrationDate"));
						merchant.setUserType(rs.getString("userType"));
						String status = rs.getString("userStatus");

						if (status != null) {
							UserStatusType userStatus = UserStatusType.valueOf(status);
							merchant.setStatus(userStatus);
						}
						merchants.add(merchant);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}

		return merchants;
	}

	public List<MerchantDetails> getAllMerchantSubUser()throws SystemException {
		List<MerchantDetails> merchants = new ArrayList<MerchantDetails>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection.prepareStatement(querySubUser)) {
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {

						MerchantDetails merchant = new MerchantDetails();
						merchant.setPayId(rs.getString("payId"));
						merchant.setEmailId(rs.getString("emailId"));
						merchant.setBusinessName(rs.getString("businessName"));
						merchant.setMobile(rs.getString("Mobile"));
						merchant.setRegistrationDate(rs.getString("registrationDate"));
						merchant.setUserType(rs.getString("userType"));
						String status = rs.getString("userStatus");

						if (status != null) {
							UserStatusType userStatus = UserStatusType.valueOf(status);
							merchant.setStatus(userStatus);
						}
						merchants.add(merchant);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return merchants;
	}

	public List<MerchantDetails> getAllMerchantSubUserList(String payId)throws SystemException {
	List<MerchantDetails> merchants = new ArrayList<MerchantDetails>();
		UserDao userDao = new UserDao();
	   User user = userDao.findPayIdByEmail(payId);
	   String parentPayId = user.getPayId();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection.prepareStatement(querySubUserList)) {
				prepStmt.setString(1, parentPayId);
				prepStmt.setString(2, parentPayId);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {

						MerchantDetails merchant = new MerchantDetails();
						merchant.setPayId(rs.getString("payId"));
						merchant.setEmailId(rs.getString("emailId"));
						merchant.setBusinessName(rs.getString("businessName"));
						merchant.setMobile(rs.getString("Mobile"));
						merchant.setRegistrationDate(rs.getString("registrationDate"));
						merchant.setUserType(rs.getString("userType"));
						String status = rs.getString("userStatus");

						if (status != null) {
							UserStatusType userStatus = UserStatusType.valueOf(status);
							merchant.setStatus(userStatus);
						}
						merchants.add(merchant);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return merchants;
}
}