package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionHistory;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;

public class TransactionDetailsService {

	private static Logger logger = Logger
			.getLogger(TransactionDetailsService.class.getName());

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public final static String query = "Select ORIG_TXN_ID ,ORDER_ID, TXN_ID, CREATE_DATE, PAY_ID,ACQUIRER_TYPE, "
			+ "CARD_MASK, MOP_TYPE, PAYMENT_TYPE, STATUS, TXNTYPE, CUST_EMAIL, "
			+ "INTERNAL_CUST_IP, INTERNAL_CUST_COUNTRY_NAME,INTERNAL_CARD_ISSUER_BANK,INTERNAL_CARD_ISSUER_COUNTRY, ACQ_ID, CURRENCY_CODE, AMOUNT,OID from "
			+ "TRANSACTION where OID in (select OID from TRANSACTION where TXN_ID=?)";

	public final static String orderIdSearchQuery = "Select ORIG_TXN_ID ,ORDER_ID, TXN_ID, CREATE_DATE, PAY_ID,ACQUIRER_TYPE, CARD_MASK, MOP_TYPE, PAYMENT_TYPE,"
			+ " STATUS, TXNTYPE, CUST_EMAIL, INTERNAL_CUST_IP,  ACQ_ID, CURRENCY_CODE, AMOUNT from "
			+ " TRANSACTION T where ORDER_ID in (select ORDER_ID from TRANSACTION where TXN_ID = ?) and "
			+ " PAY_ID in (select PAY_ID from TRANSACTION where TXN_ID = ?)";

	public final static String getOIDQuery = "Select MIN(OID) As 'OID' from TRANSACTION where TXN_ID = ?";
	public final static String txnAuthenticationQuery = "Select INTERNAL_TXN_AUTHENTICATION from TRANSACTION where TXN_ID = ?";
	public final static String updateAuthenticationQuery = "Update TRANSACTION Set INTERNAL_TXN_AUTHENTICATION = ? where TXN_ID = ?";

	public TransactionDetailsService() {

	}

	public List<TransactionHistory> getTransaction(String txnId)
			throws SystemException {
		List<TransactionHistory> transactions = new ArrayList<TransactionHistory>();

		try (Connection connection = getConnection()) {

			try (PreparedStatement prepStament = connection
					.prepareStatement(query)) {
				prepStament.setString(1, txnId);
				try (ResultSet rs = prepStament.executeQuery()) {

					transactions = setTransactionList(rs);

					if (transactions.size() == 0) {
						rs.close();
						prepStament.close();
						PreparedStatement prepStamentForOrderId = connection
								.prepareStatement(orderIdSearchQuery);
						prepStamentForOrderId.setString(1, txnId);
						prepStamentForOrderId.setString(2, txnId);
						ResultSet resultSetOrderId = prepStamentForOrderId
								.executeQuery();
						transactions = setTransactionList(resultSetOrderId);
						resultSetOrderId.close();
						prepStamentForOrderId.close();
					}
				}
			}
		} catch (SQLException sQLException) {
			MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(),
					Constants.CRM_LOG_PREFIX.getValue());
			logger.error("Unable to close connection", sQLException);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getInternalMessage());
		}
		return transactions;
	}

	private List<TransactionHistory> setTransactionList(ResultSet rs)
			throws SQLException {
		List<TransactionHistory> transactions = new ArrayList<TransactionHistory>();
		while (rs.next()) {
			TransactionHistory transaction = new TransactionHistory();

			transaction.setOrigTxnId(rs.getString(FieldType.ORIG_TXN_ID
					.getName()));
			transaction.setOrderId(rs.getString(FieldType.ORDER_ID.getName()));
			transaction.setTxnId(rs.getString(FieldType.TXN_ID.getName()));
			transaction.setCreateDate(rs
					.getString(CrmFieldConstants.CREATE_DATE.getValue()));
			transaction.setPayId(rs.getString(FieldType.PAY_ID.getName()));
			transaction.setCardNumber(rs.getString(FieldType.CARD_MASK
					.getName()));
			transaction.setMopType(rs.getString(FieldType.MOP_TYPE.getName()));
			transaction.setStatus(rs.getString(FieldType.STATUS.getName()));
			transaction.setTxnType(rs.getString(FieldType.TXNTYPE.getName()));
			transaction.setCustEmail(rs.getString(FieldType.CUST_EMAIL
					.getName()));
			transaction.setInternalCustIP(rs
					.getString(FieldType.INTERNAL_CUST_IP.getName()));
			transaction.setInternalCustCountryName(rs
					.getString(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName()));
			transaction.setInternalCardIssusserBank(rs
					.getString(FieldType.INTERNAL_CARD_ISSUER_BANK.getName()));
			transaction
					.setInternalCardIssusserCountry(rs
							.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
									.getName()));

			transaction.setAcqId(rs.getString(FieldType.ACQ_ID.getName()));
			transaction.setCurrencyCode(rs.getString(FieldType.CURRENCY_CODE
					.getName()));
			transaction.setAmount(rs.getBigDecimal(FieldType.AMOUNT.getName()));
			transaction.setAcquirerCode(rs.getString(FieldType.ACQUIRER_TYPE
					.getName()));
			transaction.setPaymentType(rs.getString(FieldType.PAYMENT_TYPE
					.getName()));

			transactions.add(transaction);
		}
		return transactions;
	}

	public String getOID(String txnId) throws SystemException {
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStament = connection
					.prepareStatement(getOIDQuery)) {
				prepStament.setString(1, txnId);
				try (ResultSet rs = prepStament.executeQuery()) {
					rs.next();
					return rs.getString(FieldType.OID.getName());
				}
			}

		} catch (SQLException sQLException) {
			MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(),
					Constants.CRM_LOG_PREFIX.getValue());
			logger.error("Unable to close connection", sQLException);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getInternalMessage());
		}
	}

	public String getTransactionAuthentication(String txnId)
			throws SystemException {
		String txnAuthentication = "";
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStament = connection
					.prepareStatement(txnAuthenticationQuery)) {
				prepStament.setString(1, getOID(txnId));
				try (ResultSet rs = prepStament.executeQuery()) {
					rs.next();
					txnAuthentication = rs
							.getString(FieldType.INTERNAL_TXN_AUTHENTICATION
									.getName());
				}
			}
		} catch (SQLException sQLException) {
			MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(),
					Constants.CRM_LOG_PREFIX.getValue());
			logger.error("Unable to close connection", sQLException);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getInternalMessage());
		}

		return txnAuthentication;
	}

	public String updateTransactionAuthentication(
			String internalTxnAuthentication, String txnId)
			throws SystemException {
		String txnAuthentication = "";
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStament = connection
					.prepareStatement(updateAuthenticationQuery)) {
				prepStament.setString(1, internalTxnAuthentication);
				prepStament.setString(2, getOID(txnId));
				prepStament.executeUpdate();
			}
		} catch (SQLException sQLException) {
			MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(),
					Constants.CRM_LOG_PREFIX.getValue());
			logger.error("Unable to close connection", sQLException);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getInternalMessage());
		}

		return txnAuthentication;
	}
}
