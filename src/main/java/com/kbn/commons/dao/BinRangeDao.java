package com.kbn.commons.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.exception.ErrorType;
import com.kbn.crm.actionBeans.BinRange;

public class BinRangeDao extends HibernateAbstractDao {

	private static final String getCompleteBinRangeDetailQuery = "from BinRange b where b.binCode = :binCode";

	private static Logger logger = Logger.getLogger(BinRangeDao.class.getName());

	public void create(BinRange binRange) throws DataAccessLayerException {
		super.save(binRange);
	}

	public void delete(BinRange binRange) throws DataAccessLayerException {
		super.delete(binRange);
	}

	public BinRange find(Long id) throws DataAccessLayerException {
		return (BinRange) super.find(BinRange.class, id);
	}

	public BinRange find(String name) throws DataAccessLayerException {
		return (BinRange) super.find(BinRange.class, name);
	}

	public BinRange findBinCode(String binCode) {
		return (BinRange) findByBinCode(binCode);

	}

	private BinRange findByBinCode(String binCode) {
		BinRange responseUser = null;
		try {
			startOperation();
			responseUser = (BinRange) getSession().createQuery(getCompleteBinRangeDetailQuery)
					.setParameter("binCode", binCode).setCacheable(true).getSingleResult();
			getTx().commit();
			return responseUser;
		} catch (NoResultException noResultException) {
			return null;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			logger.error(hibernateException);
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return responseUser;
	}

	public String insertAll(List<BinRange> binListObj) {

		Session session = HibernateSessionProvider.getSession();
		Transaction tx = session.beginTransaction();
		StringBuilder message = new StringBuilder();
		long rowCount = 0;
		try {
			for (BinRange binRangeObj : binListObj) {
				if (!tx.isActive()) {
					tx = session.beginTransaction();
				}
				rowCount++;
				try {
					BinRange binRangeDB = findBinCode(binRangeObj.getBinCode());
					if (null != binRangeDB) {
						binRangeDB.setBinCode(binRangeObj.getBinCode());
						binRangeDB.setCardType(binRangeObj.getCardType());
						binRangeDB.setGroupCode(binRangeObj.getGroupCode());
						binRangeDB.setIssuerBankName(binRangeObj.getIssuerBankName());
						binRangeDB.setIssuerCountry(binRangeObj.getIssuerCountry());
						binRangeDB.setProductName(binRangeObj.getProductName());
						binRangeDB.setRfu1(binRangeObj.getRfu1());
						binRangeDB.setRfu2(binRangeObj.getRfu2());
						session.saveOrUpdate(binRangeDB);
					} else {
						session.saveOrUpdate(binRangeObj);
					}
					tx.commit();
				} catch (Exception exception) {
					message.append(
							"sr. no." + rowCount + " " + ErrorType.CSV_NOT_SUCCESSFULLY_UPLOAD.getResponseMessage()
									+ " Bin code: " + binRangeObj.getBinCode() + "\n");
					logger.error("Error while processing binRange:" + exception + rowCount);
					tx.rollback();
					session.clear();
					tx = session.beginTransaction();
				}
			}
		} catch (Exception exception) {
			message.append(exception.toString());
			logger.error("Error while processing binRange:" + exception);
		} finally {
			session.close();
		}
		return message.toString();

	}

	@SuppressWarnings("rawtypes")
	public List findAll() throws DataAccessLayerException {
		return super.findAll(BinRange.class);
	}

	public void update(BinRange binRange) throws DataAccessLayerException {
		super.saveOrUpdate(binRange);
	}

}
