package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.DateCreater;

public class HourlyAnalyticsTrasactionsService {
	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public List<PieChart> getDashboardValues(String payId, String industryCategory, String currency, String dateFrom,
			String dateTo) throws SystemException, ParseException {
		List<PieChart> pieChartsList = new ArrayList<PieChart>();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call industry_base_totalTransaction(?,?,?,?,?)}")) {
				dateTo = DateCreater.formatToDate(dateTo);
				dateFrom = DateCreater.formatFromDate(dateFrom);
				prepStmt.setString(1, payId);
				prepStmt.setString(2, industryCategory);
				prepStmt.setString(3, currency);
				prepStmt.setString(4, dateFrom);
				prepStmt.setString(5, dateTo);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						PieChart chart = new PieChart();
						chart.setTotalSuccess(rs.getString(CrmFieldConstants.TOTAL_SUCCESS.getValue()));
						chart.setTotalFailed(rs.getString(CrmFieldConstants.TOTAL_FAILED.getValue()));
						chart.setTotalRefunded(rs.getString(CrmFieldConstants.TOTAL_REFUNDED.getValue()));
						chart.setTxndate(rs.getString(CrmFieldConstants.TXN_DATE.getValue()));
						pieChartsList.add(chart);
					}

				}
			}
		} catch (SQLException exception) {
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChartsList;
	}

	public List<PieChart> preparelist(String payId, String industryCategory, String currency, String dateFrom,
			String dateTo) throws SystemException, ParseException {

		List<PieChart> pieChartsList = getDashboardValues(payId, industryCategory, currency, dateFrom, dateTo);
		List<String> dummyList = prepareDateList(dateFrom, dateTo);
		boolean flag = false;
		List<PieChart> finalPiechartList = new ArrayList<PieChart>();

		for (String date : dummyList) {

			for (PieChart pieChart : pieChartsList) {
				flag = false;
				if (pieChart.getTxndate().equals(date)) {
					finalPiechartList.add(pieChart);
					flag = true;
					break;
				}
			}
			if (!flag) {
				PieChart chart = new PieChart();
				chart.setTotalSuccess("0");
				chart.setTotalFailed("0");
				chart.setTotalPending("0");
				chart.setTotalRefunded("0");
				chart.setTxndate((date));
				finalPiechartList.add(chart);
			}
		}
		return finalPiechartList;
	}

	private List<String> prepareDateList(String dateFrom,String dateTo) throws ParseException {
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		List<String> dates = new ArrayList<String>();
		DateFormat dateFormat = new SimpleDateFormat("HH");
		Date date = new Date();
		String currentDate =format.format(date); 
       if(currentDate.equals(dateFrom)){
    	   String datestring = dateFormat.format(date);
   		int hoursList = Integer.parseInt(datestring);
   		for (int i = 0; i <= hoursList; i++) {
   			dates.add(i + "");
   		}
       }else{
   		int hours = 24;
   		for (int i = 0; i <= hours; i++) {
   			dates.add(i + "");
   		}
       }
		return dates;

	}
}
