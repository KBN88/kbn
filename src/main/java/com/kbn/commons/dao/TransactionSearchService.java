package com.kbn.commons.dao;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.CustomerAddress;
import com.kbn.commons.user.TransactionSearch;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.User;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.DateCreater;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.TransactionType;

public class TransactionSearchService {

	private static Logger logger = Logger
			.getLogger(TransactionSearchService.class.getName());
	private static final String selectTransactionQuery = "select CREATE_DATE,TXN_ID, AMOUNT,MOP_TYPE, STATUS,PAYMENT_TYPE,ORDER_ID,CURRENCY_CODE,CUST_EMAIL,"
			+ "PRODUCT_DESC,CARD_MASK,PAY_ID,(select MIN(INTERNAL_TXN_AUTHENTICATION) FROM TRANSACTION WHERE TXNTYPE='NEWORDER' AND TXN_ID = T.OID) as INTERNAL_TXN_AUTHENTICATION,(select TXN_ID FROM TRANSACTION WHERE TXNTYPE='CAPTURE' AND STATUS ='CAPTURED' AND OID =T.OID) as CAPTURE_TXN_ID  "
			+ "FROM TRANSACTION T Where TXN_ID = ?";
	private static final String acquirerTypequery = "Select ACQUIRER_TYPE from TRANSACTION WHERE TXN_ID=? ";

	private static final String getSaleTxnQuery = "Select ORIG_TXN_ID, OID, TXN_ID, PAY_ID, CURRENCY_CODE from TRANSACTION WHERE TXNTYPE='SALE' AND OID=?  ";

	private static final String getTransactionListQuery = "Select ORIG_TXN_ID, OID, TXN_ID, PAY_ID, CURRENCY_CODE from TRANSACTION WHERE (CREATE_DATE >= ? AND CREATE_DATE< ?) AND TXNTYPE IN('SALE','ENROLL') AND ACQUIRER_TYPE=? ";

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}
	public TransactionSummaryReport getTransaction(String txnId)
			throws SystemException {
		TransactionSummaryReport transactionSummary = new TransactionSummaryReport();
		PropertiesManager propManager = new PropertiesManager();
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement(selectTransactionQuery)) {
				prepStmt.setString(1, txnId);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						transactionSummary.setDate(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.getValue()));
						transactionSummary.setTransactionId(rs
								.getString(FieldType.TXN_ID.getName()));
						transactionSummary.setApprovedAmount(rs
								.getString(FieldType.AMOUNT.getName()));
						transactionSummary.setMopType(MopType.getmopName(rs
								.getString(FieldType.MOP_TYPE.toString())));
						transactionSummary.setPaymentMethod(PaymentType
								.getpaymentName(rs
										.getString(FieldType.PAYMENT_TYPE
												.toString())));
						transactionSummary.setOrderId(rs
								.getString(FieldType.ORDER_ID.getName()));
						transactionSummary.setCurrencyName(propManager
								.getAlphabaticCurrencyCode(rs
										.getString(FieldType.CURRENCY_CODE
												.toString())));
						transactionSummary.setCustomerEmail(rs
								.getString(FieldType.CUST_EMAIL.getName()));
						transactionSummary.setStatus(rs
								.getString(FieldType.STATUS.getName()));
						transactionSummary.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC.getName()));
						transactionSummary.setCardNo(rs
								.getString(FieldType.CARD_MASK.getName()));
						transactionSummary.setPayId(rs
								.getString(FieldType.PAY_ID.getName()));
						transactionSummary.setCaptureTxnId(rs
								.getString(CrmFieldConstants.CAPTURE_TXN_ID
										.getValue()));
						transactionSummary
						.setInternalTxnAuthentication(rs
								.getString(FieldType.INTERNAL_TXN_AUTHENTICATION
										.getName()));
						break;
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionSummary;
	}

	public CustomerAddress getCustAddress(String txnId) throws SystemException {
		CustomerAddress custAddress = new CustomerAddress();
		String query = "select CREATE_DATE, CUST_NAME,CUST_PHONE,CUST_STREET_ADDRESS1,CUST_STREET_ADDRESS2,CUST_CITY,CUST_STATE,CUST_COUNTRY,CUST_ZIP,CUST_SHIP_NAME,"
				+ " CUST_SHIP_STREET_ADDRESS1,CUST_SHIP_STREET_ADDRESS2,CUST_SHIP_CITY,CUST_SHIP_STATE,CUST_SHIP_COUNTRY,CUST_SHIP_ZIP, "
				+ " (select MIN(INTERNAL_TXN_AUTHENTICATION) FROM TRANSACTION WHERE TXNTYPE='NEWORDER' AND TXN_ID = BD.TXN_ID) as INTERNAL_TXN_AUTHENTICATION "
				+ " from BILLING_DETAILS BD Where 1 = 1 ";
		if (!txnId.trim().isEmpty()) {
			query = query
					+ " and TXN_ID in (Select OID from TRANSACTION where TXN_ID=?) ";
		}

		try {
			try (Connection connection = getConnection()) {
				try (PreparedStatement prepStmt = connection
						.prepareStatement(query)) {
					if (!txnId.trim().isEmpty()) {
						prepStmt.setString(1, txnId);
					}

					try (ResultSet rs = prepStmt.executeQuery()) {
						if (rs.next()) {
							if (rs.getString(FieldType.INTERNAL_TXN_AUTHENTICATION
									.toString()) != null) {
								custAddress
								.setInternalTxnAuthentication(rs
										.getString(FieldType.INTERNAL_TXN_AUTHENTICATION
												.toString()));
							} else {
								custAddress.setInternalTxnAuthentication(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}

							if (rs.getString(FieldType.CUST_NAME.toString()) != null) {
								custAddress.setCustName(rs
										.getString(FieldType.CUST_NAME
												.toString()));
							} else {
								custAddress.setCustName(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_PHONE.toString()) != null) {
								custAddress.setCustPhone(rs
										.getString(FieldType.CUST_PHONE
												.toString()));
							} else {
								custAddress.setCustPhone(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_STREET_ADDRESS1
									.toString()) != null) {
								custAddress
								.setCustStreetAddress1((rs
										.getString(FieldType.CUST_STREET_ADDRESS1
												.toString())));
							} else {
								custAddress.setCustStreetAddress1(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_STREET_ADDRESS2
									.toString()) != null) {
								custAddress
								.setCustStreetAddress2(rs
										.getString(FieldType.CUST_STREET_ADDRESS2
												.toString()));
							} else {
								custAddress.setCustStreetAddress2(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_CITY.toString()) != null) {
								custAddress.setCustCity(rs
										.getString(FieldType.CUST_CITY
												.toString()));
							} else {
								custAddress.setCustCity(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_STATE.toString()) != null) {
								custAddress.setCustState(rs
										.getString(FieldType.CUST_STATE
												.toString()));
							} else {
								custAddress.setCustState(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_COUNTRY.toString()) != null) {
								custAddress.setCustCountry(rs
										.getString(FieldType.CUST_COUNTRY
												.toString()));
							} else {
								custAddress.setCustCountry(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_ZIP.toString()) != null) {
								custAddress.setCustZip(rs
										.getString(FieldType.CUST_ZIP
												.toString()));
							} else {
								custAddress.setCustZip(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_SHIP_NAME
									.toString()) != null) {
								custAddress.setCustShipName((rs
										.getString(FieldType.CUST_SHIP_NAME
												.toString())));
							} else {
								custAddress.setCustShipName(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_SHIP_STREET_ADDRESS1
									.toString()) != null) {
								custAddress
								.setCustShipStreetAddress1((rs
										.getString(FieldType.CUST_SHIP_STREET_ADDRESS1
												.toString())));
							} else {
								custAddress.setCustShipStreetAddress1(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_SHIP_STREET_ADDRESS2
									.toString()) != null) {
								custAddress
								.setCustShipStreetAddress2(rs
										.getString(FieldType.CUST_SHIP_STREET_ADDRESS2
												.toString()));
							} else {
								custAddress.setCustShipStreetAddress2(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_SHIP_CITY
									.toString()) != null) {
								custAddress.setCustShipCity(rs
										.getString(FieldType.CUST_SHIP_CITY
												.toString()));
							} else {
								custAddress.setCustShipCity(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_SHIP_STATE
									.toString()) != null) {
								custAddress.setCustShipState(rs
										.getString(FieldType.CUST_SHIP_STATE
												.toString()));
							} else {
								custAddress.setCustShipState(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_SHIP_COUNTRY
									.toString()) != null) {
								custAddress.setCustShipCountry(rs
										.getString(FieldType.CUST_SHIP_COUNTRY
												.toString()));
							} else {
								custAddress.setCustShipCountry(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
							if (rs.getString(FieldType.CUST_SHIP_ZIP.toString()) != null) {
								custAddress.setCustShipZip(rs
										.getString(FieldType.CUST_SHIP_ZIP
												.toString()));
							} else {
								custAddress.setCustShipZip(CrmFieldConstants.NOT_AVAILABLE.getValue());
							}
						} else {
							custAddress.setCustName(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustPhone(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustStreetAddress1(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustStreetAddress2(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustCity(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustState(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustCountry(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustZip(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustShipStreetAddress1(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustShipStreetAddress2(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustShipCity(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustShipState(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustShipCountry(CrmFieldConstants.NOT_AVAILABLE.getValue());
							custAddress.setCustShipZip(CrmFieldConstants.NOT_AVAILABLE.getValue());
						}
					}
				}
			}

		} catch (SQLException exception) {
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return custAddress;
	}

	public List<TransactionSummaryReport> getTransactionsForStatusUpdate(
			String acquirer) throws SystemException {
		List<TransactionSummaryReport> transactionList = new ArrayList<TransactionSummaryReport>();
		Date date = new Date();

		Date endDate = DateUtils.addMilliseconds(date, -Integer
				.parseInt(ConfigurationConstants.CITRUSPAY_STATUTS_UPDATE_TIME
						.getValue()));
		Date startDate = DateUtils
				.addMilliseconds(
						endDate,
						-Integer.parseInt(ConfigurationConstants.CITRUSPAY_STATUTS_UPDATE_INTERVAL_TIME
								.getValue()));

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement(getTransactionListQuery)) {
				prepStmt.setString(1, DateCreater.formatDateForDb(startDate));
				prepStmt.setString(2, DateCreater.formatDateForDb(endDate));
				prepStmt.setString(3, acquirer);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSummaryReport transaction = new TransactionSummaryReport();

						transaction.setTransactionId(rs
								.getString(FieldType.TXN_ID.getName()));
						transaction.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.getName()));
						transaction.setPayId(rs.getString(FieldType.PAY_ID
								.getName()));
						transaction
						.setOid(rs.getString(FieldType.OID.getName()));
						transaction.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.getName()));
						transactionList.add(transaction);
					}
				}
			}
		} catch (SQLException exception) {
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transactionList;
	}

	public static String getAcquirer(String txnId) throws SystemException {
		String acquirer = "";

		try (Connection connection = DataAccessObject.getBasicConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement(acquirerTypequery)) {
				prepStmt.setString(1, txnId);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						acquirer = (rs.getString(FieldType.ACQUIRER_TYPE
								.getName()));
					}
				}
			}

		} catch (SQLException exception) {
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return acquirer;
	}

	// "Select ORIG_TXN_ID, OID, TXN_ID, PAY_ID, CURRENCY_CODE from TRANSACTION WHERE TXNTYPE='SALE' AND OID=?  ";
	public TransactionSummaryReport getSaleTransaction(String oid)
			throws SystemException {

		TransactionSummaryReport transaction = new TransactionSummaryReport();

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement(getSaleTxnQuery)) {

				prepStmt.setString(1, oid);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						transaction.setTransactionId(rs
								.getString(FieldType.TXN_ID.getName()));
						transaction.setCurrencyCode(rs
								.getString(FieldType.CURRENCY_CODE.getName()));
						transaction.setPayId(rs.getString(FieldType.PAY_ID
								.getName()));
						transaction
						.setOid(rs.getString(FieldType.OID.getName()));
						transaction.setOrigTransactionId(rs
								.getString(FieldType.ORIG_TXN_ID.getName()));
					}
				}
			}

		} catch (SQLException exception) {
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transaction;
	}

	public List<TransactionSearch> getAllResellerTransactions(
			String transactionId, String orderId, String customerEmail,
			String payId, String paymentType, String Userstatus,
			String currency, String fromDate, String toDate, String resellerId, User user, int start,int length)
					throws SystemException {
		PropertiesManager propManager = new PropertiesManager();
		List<TransactionSearch> transactions = new ArrayList<TransactionSearch>();
		SimpleDateFormat txnDateFormate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call reseller_searchPayment(?,?,?,?,?,?,?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, transactionId);
				prepStmt.setString(4, orderId);
				prepStmt.setString(5, customerEmail);
				prepStmt.setString(6, payId);
				prepStmt.setString(7, paymentType);
				prepStmt.setString(8, Userstatus);
				prepStmt.setString(9, currency);
				prepStmt.setString(10, resellerId);
				prepStmt.setString(11, user.getUserType().toString());
				prepStmt.setInt(12, start);
				prepStmt.setInt(13, length);
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						TransactionSearch transaction = new TransactionSearch();

					    transaction.setTransactionId((rs.getBigDecimal(FieldType.TXN_ID.toString())).toBigInteger());
						transaction.setPayId(rs.getString(FieldType.PAY_ID.toString()));
						transaction.setCustomerName(rs.getString(FieldType.CUST_NAME.toString()));
						
						if (null != rs.getString(FieldType.CUST_EMAIL.toString())) {
						transaction.setCustomerEmail(rs.getString(FieldType.CUST_EMAIL.toString()));
						}else {
							transaction.setCustomerEmail(CrmFieldConstants.NOT_AVAILABLE.getValue());
						}
						
						transaction.setMerchants(rs.getString(CrmFieldType.BUSINESS_NAME.getName()));
						//transaction.setBusinessName(rs.getString(CrmFieldType.BUSINESS_NAME.getName()));
						
						transaction.setTxnType(rs.getString(FieldType.TXNTYPE.toString()));
						
						if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
						transaction.setMopType(MopType.getmopName(rs.getString(FieldType.MOP_TYPE.toString())));
						}else{
							transaction.setMopType(CrmFieldConstants.NOT_AVAILABLE.getValue());
						}
						if (null != rs.getString(FieldType.PAYMENT_TYPE.toString())) {
							transaction.setPaymentMethods(PaymentType.getpaymentName(rs.getString(FieldType.PAYMENT_TYPE.toString())));
						} else {
							transaction.setPaymentMethods(CrmFieldConstants.NOT_AVAILABLE.getValue());
						}

						if(null != rs.getString(FieldType.PAYMENT_TYPE.toString())){
							if (null != rs.getString(FieldType.CARD_MASK.toString())) {
								transaction.setCardNumber(rs.getString(FieldType.CARD_MASK.toString()));
							}else if((rs.getString(FieldType.PAYMENT_TYPE.getName())).equals(PaymentType.NET_BANKING.getCode())){
								transaction.setCardNumber(CrmFieldConstants.NET_BANKING.getValue());	
							}else if((rs.getString(FieldType.PAYMENT_TYPE.getName())).equals(PaymentType.WALLET.getCode())){
								transaction.setCardNumber(CrmFieldConstants.WALLET.getValue());
							}
						}else {
							transaction.setCardNumber(CrmFieldConstants.NOT_AVAILABLE.getValue());
						}
						transaction.setStatus(rs.getString(FieldType.STATUS
								.toString()));
						transaction.setDateFrom(txnDateFormate.format(rs
								.getTimestamp(CrmFieldConstants.CREATE_DATE
										.toString())));
						transaction.setAmount(rs
								.getString(CrmFieldConstants.CAPTURED_AMOUNT
										.toString()));
						transaction.setOrderId(rs
								.getString(FieldType.ORDER_ID.toString()));
						transaction.setProductDesc(rs
								.getString(FieldType.PRODUCT_DESC
										.toString()));
						transaction
						.setInternalCardIssusserBank(rs
								.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
										.toString()));
						transaction
						.setInternalCardIssusserCountry(rs
								.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
										.toString()));
						if (null != rs.getString(FieldType.CURRENCY_CODE
								.toString())) {
							transaction
							.setCurrency(propManager.getAlphabaticCurrencyCode(rs
									.getString(FieldType.CURRENCY_CODE
											.toString())));
						} else {
							transaction.setCurrency(CrmFieldConstants.NOT_AVAILABLE.getValue());
						}
						transactions.add(transaction);
					}

				}
			}

	} catch (SQLException exception) {
		throw new SystemException(ErrorType.DATABASE_ERROR,
				ErrorType.DATABASE_ERROR.getResponseMessage());
	}

	return transactions;
}
	

	public TransactionSummaryReport getSubscriptionTransaction(String pgRefNum, String oid) throws SystemException{
		TransactionSummaryReport transaction = null;
		try {
			try (Connection connection = getConnection()) {
				try (PreparedStatement prepStmt = connection
						.prepareStatement("select OID from TRANSACTION where PG_REF_NUM= ? and OID= ?")) {
					prepStmt.setString(1, pgRefNum);
					prepStmt.setString(2, oid);

					try (ResultSet rs = prepStmt.executeQuery()) {
						while (rs.next()) {
							transaction = new TransactionSummaryReport();
							transaction.setOid(rs.getString(FieldType.OID
									.toString()));
						}
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Error extracting transactions for citrus webhook"
					+ sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return transaction;
	}
		
	public List<TransactionSearch> getAllTransactions(String transactionId,
			String orderId, String customerEmail, String payId,
			String paymentType, String Userstatus, String currency,
			String fromDate, String toDate, User user, int start, int length)
					throws SystemException {
				PropertiesManager propManager = new PropertiesManager();
				List<TransactionSearch> transactions = new ArrayList<TransactionSearch>();
				SimpleDateFormat txnDateFormate = new SimpleDateFormat(
						CrmFieldConstants.DATE_TIME_FORMAT.getValue());
				try (Connection connection = getConnection()) {
					try (PreparedStatement prepStmt = connection
							.prepareStatement("{call searchPayment(?,?,?,?,?,?,?,?,?,?,?,?)}")) {
						toDate = DateCreater.formatToDate(toDate);
						fromDate = DateCreater.formatFromDate(fromDate);
						prepStmt.setString(1, fromDate);
						prepStmt.setString(2, toDate);
						prepStmt.setString(3, transactionId);
						prepStmt.setString(4, orderId);
						prepStmt.setString(5, customerEmail);
						prepStmt.setString(6, payId);
						prepStmt.setString(7, paymentType);
						prepStmt.setString(8, Userstatus);
						prepStmt.setString(9, currency);
						prepStmt.setString(10, user.getUserType().toString());
						prepStmt.setInt(11, start);
						prepStmt.setInt(12, length);
						try (ResultSet rs = prepStmt.executeQuery()) {
							while (rs.next()) {
								TransactionSearch transaction = new TransactionSearch();

							    transaction.setTransactionId((rs.getBigDecimal(FieldType.TXN_ID.toString())).toBigInteger());
								transaction.setPayId(rs.getString(FieldType.PAY_ID.toString()));
								transaction.setCustomerName(rs.getString(FieldType.CUST_NAME.toString()));
								
								if (null != rs.getString(FieldType.CUST_EMAIL.toString())) {
								transaction.setCustomerEmail(rs.getString(FieldType.CUST_EMAIL.toString()));
								}else {
									transaction.setCustomerEmail(CrmFieldConstants.NOT_AVAILABLE.getValue());
								}
								
								transaction.setMerchants(rs.getString(CrmFieldType.BUSINESS_NAME.getName()));
								//transaction.setBusinessName(rs.getString(CrmFieldType.BUSINESS_NAME.getName()));
								
								transaction.setTxnType(rs.getString(FieldType.TXNTYPE.toString()));
								
								if (null != rs.getString(FieldType.MOP_TYPE.toString())) {
								transaction.setMopType(MopType.getmopName(rs.getString(FieldType.MOP_TYPE.toString())));
								}else{
									transaction.setMopType(CrmFieldConstants.NOT_AVAILABLE.getValue());
								}
								if (null != rs.getString(FieldType.PAYMENT_TYPE.toString())) {
									transaction.setPaymentMethods(PaymentType.getpaymentName(rs.getString(FieldType.PAYMENT_TYPE.toString())));
								} else {
									transaction.setPaymentMethods(CrmFieldConstants.NOT_AVAILABLE.getValue());
								}

								if(null != rs.getString(FieldType.PAYMENT_TYPE.toString())){
									if (null != rs.getString(FieldType.CARD_MASK.toString())) {
										transaction.setCardNumber(rs.getString(FieldType.CARD_MASK.toString()));
									}else if((rs.getString(FieldType.PAYMENT_TYPE.getName())).equals(PaymentType.NET_BANKING.getCode())){
										transaction.setCardNumber(CrmFieldConstants.NET_BANKING.getValue());	
									}else if((rs.getString(FieldType.PAYMENT_TYPE.getName())).equals(PaymentType.WALLET.getCode())){
										transaction.setCardNumber(CrmFieldConstants.WALLET.getValue());
									}
								}else {
									transaction.setCardNumber(CrmFieldConstants.NOT_AVAILABLE.getValue());
								}
								transaction.setStatus(rs.getString(FieldType.STATUS
										.toString()));
								transaction.setDateFrom(txnDateFormate.format(rs
										.getTimestamp(CrmFieldConstants.CREATE_DATE
												.toString())));
								transaction.setAmount(rs
										.getString(CrmFieldConstants.CAPTURED_AMOUNT
												.toString()));
								transaction.setOrderId(rs
										.getString(FieldType.ORDER_ID.toString()));
								transaction.setProductDesc(rs
										.getString(FieldType.PRODUCT_DESC
												.toString()));
								transaction
								.setInternalCardIssusserBank(rs
										.getString(FieldType.INTERNAL_CARD_ISSUER_BANK
												.toString()));
								transaction
								.setInternalCardIssusserCountry(rs
										.getString(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
												.toString()));
								if (null != rs.getString(FieldType.CURRENCY_CODE
										.toString())) {
									transaction
									.setCurrency(propManager.getAlphabaticCurrencyCode(rs
											.getString(FieldType.CURRENCY_CODE
													.toString())));
								} else {
									transaction.setCurrency(CrmFieldConstants.NOT_AVAILABLE.getValue());
								}
								transactions.add(transaction);
							}

						}
					}

			} catch (SQLException exception) {
				throw new SystemException(ErrorType.DATABASE_ERROR,
						ErrorType.DATABASE_ERROR.getResponseMessage());
			}

			return transactions;
}
	public BigInteger getAllTransactionsCountList(String transactionId,
			String orderId, String customerEmail, String payId,
			String paymentType, String Userstatus, String currency,
			String fromDate, String toDate, User user)
					throws SystemException {
				BigInteger total = null;
				try (Connection connection = getConnection()) {
					try (PreparedStatement prepStmt = connection
							.prepareStatement("{call countSearchPayment(?,?,?,?,?,?,?,?,?,?)}")) {
						toDate = DateCreater.formatToDate(toDate);
						fromDate = DateCreater.formatFromDate(fromDate);
						prepStmt.setString(1, fromDate);
						prepStmt.setString(2, toDate);
						prepStmt.setString(3, transactionId);
						prepStmt.setString(4, orderId);
						prepStmt.setString(5, customerEmail);
						prepStmt.setString(6, payId);
						prepStmt.setString(7, paymentType);
						prepStmt.setString(8, Userstatus);
						prepStmt.setString(9, currency);
						prepStmt.setString(10, user.getUserType().toString());
						try (ResultSet rs = prepStmt.executeQuery()) {
							while (rs.next()) {
								total = rs.getBigDecimal(FieldType.COUNT.getName())
										.toBigInteger();
							}
						}
					}

				} catch (SQLException exception) {
					logger.error("Database error", exception);
					throw new SystemException(ErrorType.DATABASE_ERROR,
							ErrorType.DATABASE_ERROR.getResponseMessage());
				}
				return total;
			}
	public BigInteger getResellerAllTransactionsCountList(
			String transactionId, String orderId, String customerEmail,
			String payId, String paymentType, String Userstatus,
			String currency, String fromDate, String toDate, String resellerId, User user)
					throws SystemException {
		BigInteger total = null;
		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call count_reseller_searchPayment(?,?,?,?,?,?,?,?,?,?,?)}")) {
				toDate = DateCreater.formatToDate(toDate);
				fromDate = DateCreater.formatFromDate(fromDate);
				prepStmt.setString(1, fromDate);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, transactionId);
				prepStmt.setString(4, orderId);
				prepStmt.setString(5, customerEmail);
				prepStmt.setString(6, payId);
				prepStmt.setString(7, paymentType);
				prepStmt.setString(8, Userstatus);
				prepStmt.setString(9, currency);
				prepStmt.setString(10, resellerId);
				prepStmt.setString(11, user.getUserType().toString());
				try (ResultSet rs = prepStmt.executeQuery()) {
					while (rs.next()) {
						total = rs.getBigDecimal(FieldType.COUNT.getName())
								.toBigInteger();
					}
				}
			}

		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return total;
	}
}