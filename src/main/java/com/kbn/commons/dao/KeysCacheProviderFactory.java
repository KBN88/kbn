package com.kbn.commons.dao;

import com.kbn.commons.crypto.Scrambler;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.PropertiesManager;

public class KeysCacheProviderFactory {

	public KeysCacheProviderFactory() {
	}

	public static CacheProvider getKeysCache() {

		return (new KeysCacheProviderFactory()).getKeysCacheProvider();
	}

	public CacheProvider getKeysCacheProvider() {
		CacheProvider cache = new DefaultCacheProvider();

		for (int count = 1; count < Integer
				.parseInt(Constants.MAX_NUMBER_OF_KEYS.getValue()); count++) {
			String generatedKey = createKey(count);
			if (null == generatedKey || generatedKey.isEmpty()) {
				break;
			}

			cache.put(String.valueOf(count), generatedKey);
		}

		return cache;
	}

	public String createKey(int keyId) {

		// Get key from file
		String encryptedFileKey = getKeyFromFile(keyId);
		if (null == encryptedFileKey || encryptedFileKey.isEmpty()) {
			return null;
		}

		// decrypt key from file
		Scrambler scrambler = new Scrambler(getKek());
		String plainTextFileKey = scrambler.decrypt(encryptedFileKey);

		// Get key from data store
		String keyInDataStore = getKeyFromDataStore(keyId);
		if (null == keyInDataStore || keyInDataStore.isEmpty()) {
			return null;
		}

		// Decrypt key from data store
		String plainTextDataStoreKey = scrambler.decrypt(keyInDataStore);

		// Combine the decrypted keys
		return plainTextFileKey + plainTextDataStoreKey;
	}

	public String getKek() {
		return (new PropertiesManager()).getSystemProperty("KEK");
	}

	public String getKeyFromDataStore(int keyId) {
		return (new KeysDao()).fetchKey(keyId);
	}

	public String getKeyFromFile(int keyId) {
		return (new PropertiesManager()).getKey(String.valueOf(keyId));
	}
}
