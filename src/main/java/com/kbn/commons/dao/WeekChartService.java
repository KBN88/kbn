package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.PaymentType;

public class WeekChartService {

	private static Logger logger = Logger.getLogger(WeekChartService.class
			.getName());

	public WeekChartService() {

	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public List<PieChart> getDashboardValues(String payId, String currency,
			String dateFrom, String dateTo) throws SystemException,
			ParseException {

		List<PieChart> pieChartsList = new ArrayList<PieChart>();

		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection
					.prepareStatement("{call weeklyTransaction(?,?,?,?)}")) {
				DateFormat dateFormat = new SimpleDateFormat(
						"EEE MMM dd yyyy HH:mm:ss");
				SimpleDateFormat dbformat = new SimpleDateFormat("yyyy-MM-dd");
				String startDate = dbformat.format(dateFormat.parse(dateFrom));
				String endDate = dbformat.format(dateFormat.parse(dateTo));

				String as = endDate + " 23:59:59";

				preparedStatement.setString(1, payId);
				preparedStatement.setString(2, currency);
				preparedStatement.setString(3, startDate);
				preparedStatement.setString(4, as);
				try (ResultSet rs = preparedStatement.executeQuery()) {

					while (rs.next()) {
						PieChart chart = new PieChart();
						chart.setCc(rs.getString(PaymentType.CREDIT_CARD
								.getCode()));	
						chart.setDc(rs.getString(PaymentType.DEBIT_CARD
								.getCode()));
						chart.setNb(rs.getString(PaymentType.NET_BANKING
								.getCode()));
						chart.setWl(rs.getString(PaymentType.WALLET.getCode()));
						chart.setTxndate(rs
								.getString(CrmFieldConstants.TXN_DATE
										.getValue()));
						pieChartsList.add(chart);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChartsList;
	}

	public List<PieChart> preparelist(String payId, String currency,
			String dateFrom, String dateTo) throws SystemException,
			ParseException {

		List<PieChart> pieChartsList = getDashboardValues(payId, currency,
				dateFrom, dateTo);
		List<String> dummyList = prepareDateList(dateFrom, dateTo);
		boolean flag = false;
		List<PieChart> finalPiechartList = new ArrayList<PieChart>();

		for (String date : dummyList) {

			for (PieChart pieChart : pieChartsList) {
				flag = false;
				if (pieChart.getTxndate().equals(date)) {
					finalPiechartList.add(pieChart);
					flag = true;
					break;
				}
			}
			if (!flag) {
				PieChart chart = new PieChart();
				chart.setCc("0");
				chart.setDc("0");
				chart.setNb("0");
				chart.setWl("0");
				chart.setTxndate((date));
				finalPiechartList.add(chart);
			}
		}

		return finalPiechartList;
	}

	public List<String> prepareDateList(String dateFrom, String dateTo)
			throws ParseException {

		List<String> dates = new ArrayList<String>();

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"EEE MMM dd yyyy HH:mm:ss");
		SimpleDateFormat dbformat = new SimpleDateFormat("yyyy-MM-dd");

		Date start = dateFormat.parse(dateFrom);
		Date end = dateFormat.parse(dateTo);

		Calendar cal = Calendar.getInstance();
		cal.setTime(end);
		cal.add(Calendar.DATE, 1); // add 1 days

		Date date = cal.getTime();

		calendar.setTime(start);
		while (calendar.getTime().before(date)) {
			Date result = calendar.getTime();
			String dateString = dbformat.format(result);
			dates.add(dateString);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		return dates;
	}
}
