package com.kbn.commons.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.SearchUser;
import com.kbn.commons.user.UserDao;
import com.kbn.ticketing.commons.util.SubAdmin;

public class SearchUserService {

	private static Logger logger = Logger.getLogger(SearchUserService.class.getName());
	public List<SearchUser> transactionList = new ArrayList<SearchUser>();

	public SearchUserService() {
	}

	public List<Merchants> getSubUsers(String parentPayId) throws SQLException, ParseException, SystemException {
		List<Merchants> subUser = new ArrayList<Merchants>();
		UserDao userDao = new UserDao();
		try {
			subUser = userDao.getSubUsers(parentPayId);
		} catch (Exception exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}

		return subUser;

	}

	public List<Merchants> getAcquirerSubUsers(String parentPayId)
			throws SQLException, ParseException, SystemException {
		List<Merchants> subUser = new ArrayList<Merchants>();
		UserDao userDao = new UserDao();
		try {
			subUser = userDao.getAcquirerSubUsers(parentPayId);
		} catch (Exception exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}

		return subUser;
	}	

	public List<SubAdmin> getAgentsList(String sessionPayId) throws SQLException, ParseException, SystemException {
		List<SubAdmin> agentList = new ArrayList<SubAdmin>();
		UserDao userDao = new UserDao();
		try {
			agentList = userDao.getAgents(sessionPayId);
		} catch (Exception exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return agentList;

	}
}
