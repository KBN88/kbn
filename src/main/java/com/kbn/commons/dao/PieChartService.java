package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.util.CrmFieldConstants;

public class PieChartService {
	private static Logger logger = Logger.getLogger(PieChartService.class
			.getName());

	public PieChartService() {

	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public PieChart getDashboardValues(String payId, String currency,
			String dateFrom, String dateTo) throws SystemException,
			ParseException {
		PieChart pieChart = new PieChart();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection
					.prepareStatement("{call chartTotalSummary(?,?,?,?)}")) {
				DateFormat df = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				String startDate = sdf1.format(df.parse(dateFrom));
				String endDate = sdf1.format(df.parse(dateTo));
				preparedStatement.setString(1, payId);
				preparedStatement.setString(2, currency);
				preparedStatement.setString(3, startDate);
				preparedStatement.setString(4, endDate);
				try (ResultSet rs = preparedStatement.executeQuery()) {
					rs.next();
					pieChart.setAmex(rs.getString(CrmFieldConstants.TOTAL_AMEX
							.getValue()));
					pieChart.setMastercard(rs
							.getString(CrmFieldConstants.TOTAL_MASTER
									.getValue()));
					pieChart.setNet(rs
							.getString(CrmFieldConstants.TOTAL_NETBANKING
									.getValue()));
					pieChart.setVisa(rs.getString(CrmFieldConstants.TOTAL_VISA
							.getValue()));
					pieChart.setMaestro(rs
							.getString(CrmFieldConstants.TOTAL_MESTRO_CARDS
									.getValue()));
					pieChart.setEzeeClick(rs
							.getString(CrmFieldConstants.TOTAL_EZEECLICK
									.getValue()));
					pieChart.setOther(rs
							.getString(CrmFieldConstants.TOTAL_OTHER
									.getValue()));

				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChart;
	}

	public PieChart getPieChartDashboardValues(String acquirer,
			String currency, String dateFrom, String dateTo)throws SystemException,
			ParseException {
		PieChart pieChart = new PieChart();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection
					.prepareStatement("{call acquirerPieChart(?,?,?,?)}")) {
				DateFormat df = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				String startDate = sdf1.format(df.parse(dateFrom));
				String endDate = sdf1.format(df.parse(dateTo));
				preparedStatement.setString(1, acquirer);
				preparedStatement.setString(2, currency);
				preparedStatement.setString(3, startDate);
				preparedStatement.setString(4, endDate);
				try (ResultSet rs = preparedStatement.executeQuery()) {
					rs.next();
					pieChart.setAmex(rs.getString(CrmFieldConstants.TOTAL_AMEX
							.getValue()));
					pieChart.setMastercard(rs
							.getString(CrmFieldConstants.TOTAL_MASTER
									.getValue()));
					pieChart.setNet(rs
							.getString(CrmFieldConstants.TOTAL_NETBANKING
									.getValue()));
					pieChart.setVisa(rs.getString(CrmFieldConstants.TOTAL_VISA
							.getValue()));
					pieChart.setMaestro(rs
							.getString(CrmFieldConstants.TOTAL_MESTRO_CARDS
									.getValue()));
					pieChart.setEzeeClick(rs
							.getString(CrmFieldConstants.TOTAL_EZEECLICK
									.getValue()));
					pieChart.setOther(rs
							.getString(CrmFieldConstants.TOTAL_OTHER
									.getValue()));

				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChart;
	}

}
