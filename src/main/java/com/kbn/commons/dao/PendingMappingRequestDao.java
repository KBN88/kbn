package com.kbn.commons.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.user.PendingMappingRequest;
import com.kbn.commons.user.ServiceTax;

public class PendingMappingRequestDao extends HibernateAbstractDao {

	public void create(PendingMappingRequest pendingMappingRequest) throws DataAccessLayerException {
		super.save(pendingMappingRequest);
	}


	@SuppressWarnings("unchecked")
	public List<PendingMappingRequest> getPendingMappingRequest() {
		List<PendingMappingRequest> pendingMappingRequestList = new ArrayList<PendingMappingRequest>();
		pendingMappingRequestList = null;
		try {
			
			startOperation();

			pendingMappingRequestList = getSession().createQuery("from PendingMappingRequest PMR where PMR.status='PENDING'")
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return pendingMappingRequestList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return pendingMappingRequestList;
	}
	
	
	public PendingMappingRequest findPendingMappingRequest(String merchantEmailId , String acquirer) {
		PendingMappingRequest pendingMappingRequest = null;
		try {
			startOperation();
			pendingMappingRequest = (PendingMappingRequest) getSession()
					.createQuery(
							"from PendingMappingRequest PMR where PMR.merchantEmailId = :merchantEmailId and PMR.acquirer = :acquirer and PMR.status = 'PENDING' ")
					.setParameter("merchantEmailId", merchantEmailId)
					.setParameter("acquirer", acquirer)
					.setCacheable(true).uniqueResult();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;
		}
		return pendingMappingRequest;

	}
	
	public PendingMappingRequest findActiveMappingRequest(String merchantEmailId , String acquirer) {
		PendingMappingRequest pendingMappingRequest = null;
		try {
			startOperation();
			pendingMappingRequest = (PendingMappingRequest) getSession()
					.createQuery(
							"from PendingMappingRequest PMR where PMR.merchantEmailId = :merchantEmailId and PMR.acquirer = :acquirer and PMR.status = 'ACTIVE' ")
					.setParameter("merchantEmailId", merchantEmailId)
					.setParameter("acquirer", acquirer)
					.setCacheable(true).uniqueResult();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;
		}
		return pendingMappingRequest;

	}
	

}
