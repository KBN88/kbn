package com.kbn.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.PieChart;
import com.kbn.commons.util.CrmFieldConstants;

public class BarChartService {
	private static Logger logger = Logger.getLogger(BarChartService.class
			.getName());

	public BarChartService() {

	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public PieChart getDashboardValues(String payId, String currency,
			String dateFrom, String dateTo) throws SystemException,
			ParseException {
		PieChart pieChart = new PieChart();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection
					.prepareStatement("{call barChartTotalSummary(?,?,?,?)}")) {
				DateFormat df = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				String startDate = sdf1.format(df.parse(dateFrom));
				String endDate = sdf1.format(df.parse(dateTo));
				preparedStatement.setString(1, payId);
				preparedStatement.setString(2, currency);
				preparedStatement.setString(3, startDate);
				preparedStatement.setString(4, endDate);
				try (ResultSet rs = preparedStatement.executeQuery()) {
					rs.next();
					pieChart.setTotalCredit(rs
							.getString(CrmFieldConstants.TOTAL_CREDIT
									.getValue()));
					pieChart.setTotalDebit(rs
							.getString(CrmFieldConstants.TOTAL_DEBIT.getValue()));
					pieChart.setNet(rs
							.getString(CrmFieldConstants.TOTAL_NETBANKING
									.getValue()));
					pieChart.setOther(rs
							.getString(CrmFieldConstants.TOTAL_OTHER.getValue()));

				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChart;
	}

	public PieChart prepareResellerlist(String payId, String resellerId,
			String currency, String dateFrom, String dateTo)
			throws SystemException, ParseException {
		PieChart pieChart = new PieChart();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection
					.prepareStatement("{call resellerBarChartTransactionRecord(?,?,?,?,?)}")) {
				DateFormat df = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				String startDate = sdf1.format(df.parse(dateFrom));
				String endDate = sdf1.format(df.parse(dateTo));
				preparedStatement.setString(1, payId);
				preparedStatement.setString(2, resellerId);
				preparedStatement.setString(3, currency);
				preparedStatement.setString(4, startDate);
				preparedStatement.setString(5, endDate);
				try (ResultSet rs = preparedStatement.executeQuery()) {
					rs.next();
					pieChart.setTotalCredit(rs
							.getString(CrmFieldConstants.TOTAL_CREDIT
									.getValue()));
					pieChart.setTotalDebit(rs
							.getString(CrmFieldConstants.TOTAL_DEBIT.getValue()));
					pieChart.setNet(rs
							.getString(CrmFieldConstants.TOTAL_NETBANKING
									.getValue()));
					pieChart.setOther(rs
							.getString(CrmFieldConstants.TOTAL_OTHER.getValue()));

				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChart;
	}

	public PieChart getPaymentMethodDashboardValues(String payId,
			String currency, String dateFrom, String dateTo)
			throws SystemException, ParseException {
		PieChart pieChart = new PieChart();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection
					.prepareStatement("{call paymentMethodsSummary(?,?,?,?)}")) {
				DateFormat df = new SimpleDateFormat("dd-MM-yy");
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				String startDate = sdf1.format(df.parse(dateFrom));
				String endDate = sdf1.format(df.parse(dateTo));
				preparedStatement.setString(1, payId);
				preparedStatement.setString(2, currency);
				preparedStatement.setString(3, startDate);
				preparedStatement.setString(4, endDate);
				try (ResultSet rs = preparedStatement.executeQuery()) {
					rs.next();
					pieChart.setTotalCreditCardsTransaction(rs
							.getString(CrmFieldConstants.TOTAL_CREDITCARDS_TRANSACTION
									.getValue()));
					pieChart.setTotalNetBankingTransaction(rs
							.getString(CrmFieldConstants.TOTAL_NETBANKING_TRANSACTION
									.getValue()));
					pieChart.setTotalDebitCardsTransaction(rs
							.getString(CrmFieldConstants.TOTAL_DEBIT_TRANSACTION
									.getValue()));
					pieChart.setTotalWalletTransaction(rs
							.getString(CrmFieldConstants.TOTAL_WALLET_TRANSACTION
									.getValue()));

				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChart;
	}

	public PieChart getAcquirerBarChartDashboardValues(String acquirer,
			String currency, String dateFrom, String dateTo) throws SystemException,
			ParseException {
		PieChart pieChart = new PieChart();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection
					.prepareStatement("{call acquirerBarChart(?,?,?,?)}")) {
				DateFormat df = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				String startDate = sdf1.format(df.parse(dateFrom));
				String endDate = sdf1.format(df.parse(dateTo));
				preparedStatement.setString(1, acquirer);
				preparedStatement.setString(2, currency);
				preparedStatement.setString(3, startDate);
				preparedStatement.setString(4, endDate);
				try (ResultSet rs = preparedStatement.executeQuery()) {
					rs.next();
					pieChart.setTotalCredit(rs
							.getString(CrmFieldConstants.TOTAL_CREDIT
									.getValue()));
					pieChart.setTotalDebit(rs
							.getString(CrmFieldConstants.TOTAL_DEBIT.getValue()));
					pieChart.setNet(rs
							.getString(CrmFieldConstants.TOTAL_NETBANKING
									.getValue()));
					pieChart.setOther(rs
							.getString(CrmFieldConstants.TOTAL_OTHER.getValue()));

				}
			}
		} catch (SQLException exception) {
			logger.error("Database error", exception);
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return pieChart;
	}
}