package com.kbn.commons.rule;

/**
 * @author Surender
 *
 */
public enum OperandType {
	STRING,
	NUMBER
}
