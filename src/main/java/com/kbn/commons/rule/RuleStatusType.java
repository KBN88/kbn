package com.kbn.commons.rule;

/**
 * @author Surender
 *
 */
public enum RuleStatusType {
	TRUE,
	FALSE,
	INVALID,
	PENDING
}
