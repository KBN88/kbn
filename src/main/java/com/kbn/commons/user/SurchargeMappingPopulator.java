package com.kbn.commons.user;

import java.math.BigDecimal;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;

public class SurchargeMappingPopulator {
	
	private String paymentType;
	private String payId;
	private String acquirerName;
	private String onOff;
	
	private String status;
	
	private String mopType;
	
	private String merchantIndustryType;
	private boolean allowOnOff;
	private BigDecimal bankSurchargeAmountOn;
	private BigDecimal bankSurchargePercentageOn;
	private BigDecimal bankSurchargeAmountOff;
	private BigDecimal bankSurchargePercentageOff;
	private String merchantName;
	private String requestedBy;
	
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public String getOnOff() {
		return onOff;
	}
	public void setOnOff(String onOff) {
		this.onOff = onOff;
	}
	public String getMerchantIndustryType() {
		return merchantIndustryType;
	}
	public void setMerchantIndustryType(String merchantIndustryType) {
		this.merchantIndustryType = merchantIndustryType;
	}
	public String getMopType() {
		return mopType;
	}
	public void setMopType(String mopType) {
		this.mopType = mopType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getBankSurchargeAmountOn() {
		return bankSurchargeAmountOn;
	}
	public void setBankSurchargeAmountOn(BigDecimal bankSurchargeAmountOn) {
		this.bankSurchargeAmountOn = bankSurchargeAmountOn;
	}
	public BigDecimal getBankSurchargePercentageOn() {
		return bankSurchargePercentageOn;
	}
	public void setBankSurchargePercentageOn(BigDecimal bankSurchargePercentageOn) {
		this.bankSurchargePercentageOn = bankSurchargePercentageOn;
	}
	public BigDecimal getBankSurchargeAmountOff() {
		return bankSurchargeAmountOff;
	}
	public void setBankSurchargeAmountOff(BigDecimal bankSurchargeAmountOff) {
		this.bankSurchargeAmountOff = bankSurchargeAmountOff;
	}
	public BigDecimal getBankSurchargePercentageOff() {
		return bankSurchargePercentageOff;
	}
	public void setBankSurchargePercentageOff(BigDecimal bankSurchargePercentageOff) {
		this.bankSurchargePercentageOff = bankSurchargePercentageOff;
	}
	public boolean isAllowOnOff() {
		return allowOnOff;
	}
	public void setAllowOnOff(boolean allowOnOff) {
		this.allowOnOff = allowOnOff;
	}
	public String getAcquirerName() {
		return acquirerName;
	}
	public void setAcquirerName(String acquirerName) {
		this.acquirerName = acquirerName;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	
}
