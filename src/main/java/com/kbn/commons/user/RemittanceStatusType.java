package com.kbn.commons.user;

public enum RemittanceStatusType {
	INITIATED ("Initiated","Initiated"),
	PROCESSED ("Processed","Processed");
	
	
	private final String name;
	private final String code;
	
	private RemittanceStatusType(String name, String code){
		this.name = name;
		this.code = code;
	}
	public String getName() {
		return name;
	}
	
	public String getCode(){
		return code;
	}
}
