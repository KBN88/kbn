package com.kbn.commons.user;

import com.kbn.commons.exception.ErrorType;

public interface UserAuthenticator {
	public ErrorType authenticate(String id, String password);
}//UserAuthenticator
