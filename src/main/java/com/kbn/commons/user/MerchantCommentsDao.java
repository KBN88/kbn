package com.kbn.commons.user;

import java.util.List;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

public class MerchantCommentsDao extends HibernateAbstractDao{
	
	public MerchantCommentsDao() {
        super();
    }
	
	public void create(MerchantComments merchantComments) throws DataAccessLayerException {
        super.save(merchantComments);
    }
	
	public void delete(MerchantComments merchantComments) throws DataAccessLayerException {
        super.delete(merchantComments);
    }
	
	public void update(MerchantComments merchantComments) throws DataAccessLayerException {
        super.saveOrUpdate(merchantComments);
    }
	
	@SuppressWarnings("rawtypes")
	public  List findAll() throws DataAccessLayerException{
	    return super.findAll(MerchantComments.class);
	}
	 
	public MerchantComments find(Long id) throws DataAccessLayerException {
	    return (MerchantComments) super.find(MerchantComments.class, id);
	}
	 
	public MerchantComments find(String name) throws DataAccessLayerException {
	    return (MerchantComments) super.find(MerchantComments.class, name);
	}


}
