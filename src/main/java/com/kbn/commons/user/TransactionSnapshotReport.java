package com.kbn.commons.user;

import java.io.Serializable;

public class TransactionSnapshotReport implements Serializable {

	private static final long serialVersionUID = 7868515293781701587L;	
	
	private String date;
	private String paymentMethod;
	private String currency;
	private int totalTransactions;
	private int successTransactions;
	private int failedTransactions;
	private int pendingTransactions;
	private int totalDropped;
	private int totalBaunced;
	private String approvedAmount;
			
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public int getTotalTransactions() {
		return totalTransactions;
	}
	public void setTotalTransactions(int totalTransactions) {
		this.totalTransactions = totalTransactions;
	}
	public int getSuccessTransactions() {
		return successTransactions;
	}
	public void setSuccessTransactions(int successTransactions) {
		this.successTransactions = successTransactions;
	}
	public int getFailedTransactions() {
		return failedTransactions;
	}
	public void setFailedTransactions(int failedTransactions) {
		this.failedTransactions = failedTransactions;
	}
	public int getPendingTransactions() {
		return pendingTransactions;
	}
	public void setPendingTransactions(int pendingTransactions) {
		this.pendingTransactions = pendingTransactions;
	}
	public String getApprovedAmount() {
		return approvedAmount;
	}
	public void setApprovedAmount(String approvedAmount) {
		this.approvedAmount = approvedAmount;
	}
	public int getTotalDropped() {
		return totalDropped;
	}
	public void setTotalDropped(int totalDropped) {
		this.totalDropped = totalDropped;
	}
	public int getTotalBaunced() {
		return totalBaunced;
	}
	public void setTotalBaunced(int totalBaunced) {
		this.totalBaunced = totalBaunced;
	}
	
	
}
