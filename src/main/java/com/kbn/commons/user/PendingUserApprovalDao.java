package com.kbn.commons.user;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

/**
 * @author Rahul
 *
 */
public class PendingUserApprovalDao extends HibernateAbstractDao {

	public PendingUserApprovalDao() {
	}

	@SuppressWarnings("unused")
	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public void create(PendingUserApproval pendingUserApproval) throws DataAccessLayerException {
		super.save(pendingUserApproval);
	}

	public void update(PendingUserApproval pendingUserApproval) throws DataAccessLayerException {
		super.saveOrUpdate(pendingUserApproval);
	}

	public PendingUserApproval find(String payId) {
		PendingUserApproval responseUser = null;
		try {
			startOperation();
			responseUser = (PendingUserApproval) getSession()
					.createQuery("from PendingUserApproval P where P.payId = :payId and P.requestStatus = 'PENDING'")
					.setParameter("payId", payId).setCacheable(true).uniqueResult();	
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;
		}
		return responseUser;

	}

	@SuppressWarnings("unchecked")
	public List<PendingUserApproval> getPendingUserProfileList() {
		List<PendingUserApproval> userProfileList = new ArrayList<PendingUserApproval>();
		userProfileList = null;
		try {

			startOperation();

			userProfileList = getSession().createQuery("from PendingUserApproval where requestStatus ='PENDING' ").setCacheable(true).getResultList();
			getTx().commit();
			return userProfileList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return userProfileList;
	}

}
