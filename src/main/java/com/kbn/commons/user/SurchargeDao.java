package com.kbn.commons.user;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.chargeback.action.beans.Chargeback;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;

public class SurchargeDao extends HibernateAbstractDao {

	public void create(Surcharge surcharge) throws DataAccessLayerException {
		super.save(surcharge);
	}

	public Surcharge findDetails(String payId, String paymentType) {
		Surcharge responseUser = null;
		try {
			startOperation();
			responseUser = (Surcharge) getSession()
					.createQuery("from Surcharge S where S.payId = :payId and S.paymentType = :paymentType ")
					.setParameter("payId", payId).setParameter("paymentType", paymentType).setCacheable(true)
					.uniqueResult();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;
		}
		return responseUser;

	}

	@SuppressWarnings("unchecked")
	public List<Surcharge> findSurchargeListByPayid(String payId, String paymentTypeName) {
		PaymentType paymentType = PaymentType.getInstance(paymentTypeName);
		List<Surcharge> userSurchargeList = new ArrayList<Surcharge>();

		try {
			
			startOperation();

			userSurchargeList = getSession().createQuery("from Surcharge C where C.payId= :payId and C.paymentType=:paymentType and C.status='ACTIVE'")
					.setParameter("payId", payId)
					.setParameter("paymentType", paymentType)
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return userSurchargeList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return userSurchargeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Surcharge> findSurchargeListByPayIdAcquirerName(String payId, String paymentTypeName , String acquirerName,String mopTypeName) {
		PaymentType paymentType = PaymentType.getInstanceUsingCode(paymentTypeName);
		if (paymentType == null){
			paymentType = PaymentType.getInstance(paymentTypeName);
		}
		MopType mopType = MopType.getInstance(mopTypeName);
		List<Surcharge> userSurchargeList = new ArrayList<Surcharge>();

		try {
			
			startOperation();

			userSurchargeList = getSession().createQuery("from Surcharge C where C.payId= :payId and C.paymentType=:paymentType and C.acquirerName =:acquirerName and mopType =:mopType and C.status='ACTIVE'")
					.setParameter("payId", payId)
					.setParameter("paymentType", paymentType)
					.setParameter("acquirerName", acquirerName)
					.setParameter("mopType", mopType)
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return userSurchargeList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return userSurchargeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Surcharge> findPendingSurchargeListByPayIdAcquirerName(String payId, String paymentTypeName , String acquirerName,String mopTypeName) {
		PaymentType paymentType = PaymentType.getInstanceUsingCode(paymentTypeName);
		if (paymentType == null){
			paymentType = PaymentType.getInstance(paymentTypeName);
		}
		MopType mopType = MopType.getInstance(mopTypeName);
		List<Surcharge> userSurchargeList = new ArrayList<Surcharge>();

		try {
			
			startOperation();

			userSurchargeList = getSession().createQuery("from Surcharge C where C.payId= :payId and C.paymentType=:paymentType and C.acquirerName =:acquirerName and mopType =:mopType and C.status='PENDING'")
					.setParameter("payId", payId)
					.setParameter("paymentType", paymentType)
					.setParameter("acquirerName", acquirerName)
					.setParameter("mopType", mopType)
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return userSurchargeList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return userSurchargeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Surcharge> findPendingSurchargeList() {
		List<Surcharge> pendingSurchargeList = new ArrayList<Surcharge>();

		try {
			
			startOperation();

			pendingSurchargeList = getSession().createQuery("from Surcharge S where S.status='PENDING'")
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return pendingSurchargeList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return pendingSurchargeList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Surcharge> findActiveSurchargeListByPayIdAcquirer(String payId, String acquirerName) {
		List<Surcharge> activeSurchargeList = new ArrayList<Surcharge>();

		try {
			
			startOperation();

			activeSurchargeList = getSession().createQuery("from Surcharge S where S.payId = :payId and S.acquirerName = :acquirerName and S.status='ACTIVE'")
					.setParameter("payId", payId)
					.setParameter("acquirerName", acquirerName)
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return activeSurchargeList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return activeSurchargeList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Surcharge> findDetailsOnUsOffUs(String payId , String acquirerName , MopType mopType , PaymentType paymentType) {
		
		List<Surcharge> userSurchargeList = new ArrayList<Surcharge>();

		try {
			
			startOperation();

			userSurchargeList = getSession().createQuery("from Surcharge C where C.payId= :payId and C.paymentType=:paymentType and C.status='ACTIVE' and C.mopType=:mopType and C.acquirerName=:acquirerName")
					.setParameter("payId", payId)
					.setParameter("paymentType", paymentType)
					.setParameter("mopType", mopType)
					.setParameter("acquirerName", acquirerName).setCacheable(true)
					.getResultList();
			getTx().commit();
			return userSurchargeList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return userSurchargeList;
	}
	
	
	@SuppressWarnings("unchecked")
	public boolean isChargingDetailsMappedWithSurcharge(String payId , String acquirerName , MopType mopType , PaymentType paymentType) {
		List<Surcharge> surchargeDetailsList = new ArrayList<Surcharge>();
		try {
			startOperation();			
			surchargeDetailsList = getSession().createQuery("from Surcharge C where C.payId= :payId and C.paymentType=:paymentType and C.status='ACTIVE' and C.mopType=:mopType and C.acquirerName=:acquirerName")
									.setParameter("payId", payId)
									.setParameter("paymentType", paymentType)
									.setParameter("mopType", mopType)
									.setParameter("acquirerName", acquirerName).setCacheable(true)
									.getResultList();
			getTx().commit();
			if (surchargeDetailsList.size() > 0) {
				return true;
			}
			else{
				return false;
			}
			
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		
		return false;
	}

}
