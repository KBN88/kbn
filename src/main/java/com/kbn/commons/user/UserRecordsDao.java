package com.kbn.commons.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

/**
 * @author Puneet
 * 
 */

public class UserRecordsDao extends HibernateAbstractDao {

	private Date date = new Date();
	private UserRecords userRecords = new UserRecords();
	
	public UserRecordsDao() {
		super();
	}

	public void createDetails(String emailId, String password, String payId){
		userRecords.setEmailId(emailId);
		userRecords.setPassword(password);
		userRecords.setPayId(payId);
		userRecords.setCreateDate(date);	
		create(userRecords);
	}
	
	public void create(UserRecords userRecords) throws DataAccessLayerException {
		super.save(userRecords);
	}

	public void delete(UserRecords userRecords) throws DataAccessLayerException {
		super.delete(userRecords);
	}

	public UserRecords find(Long id) throws DataAccessLayerException {
		return (UserRecords) super.find(UserRecords.class, id);
	}

	public UserRecords find(String name) throws DataAccessLayerException {
		return (UserRecords) super.find(UserRecords.class, name);
	}

	@SuppressWarnings("rawtypes")
	public List findAll() throws DataAccessLayerException {
		return super.findAll(UserRecords.class);
	}

	public void update(UserRecords userRecords) throws DataAccessLayerException {
		super.saveOrUpdate(userRecords);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getOldPasswords(String emailId) {
		List<String> userOldPasswords = new ArrayList<String>();
		try {
			startOperation();
			userOldPasswords = getSession().createQuery("Select password from UserRecords UR where UR.emailId = :emailId  order by UR.id desc")
														.setParameter("emailId", emailId)
														.setMaxResults(4).getResultList();
			getTx().commit();
	      }
		catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		}
		catch (HibernateException hibernateException) {
			handleException(hibernateException);
		}
		finally {
			autoClose();
		}
		return userOldPasswords;
	}
}
