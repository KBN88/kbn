package com.kbn.commons.user;

import java.util.List;

import com.kbn.commons.dao.HibernateAbstractDao;

public class ContactInfoDao extends HibernateAbstractDao {

	public ContactInfoDao() {
		super();
	}

	public void create(ContactInfo contactInfo) {
		super.save(contactInfo);
	}

	public void delete(ContactInfo contactInfo) {
		super.delete(contactInfo);
	}

	public ContactInfo find(String userId) {
		return (ContactInfo) super.find(ContactInfo.class, userId);
	}

	public void update(ContactInfo user) {
		super.saveOrUpdate(user);
	}

	@SuppressWarnings("unchecked")
	public List<ContactInfo> findAll() {
		return (List<ContactInfo>) super.findAll(ContactInfo.class);
	}

}
