package com.kbn.commons.user;

import java.util.List;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;



public class RolesDao extends HibernateAbstractDao {

	public RolesDao() {
        super();
    }

	public void create(Roles roles) throws DataAccessLayerException {
        super.saveOrUpdate(roles);
    }
	
	 public void delete(Roles roles) throws DataAccessLayerException {
	        super.delete(roles);
	    }
	
	 public Roles find(Long id) throws DataAccessLayerException {
	        return (Roles) super.find(Roles.class, id);
	    }

	 public Roles find(String name) throws DataAccessLayerException {
	        return (Roles) super.find(Roles.class, name);
	    }
	 @SuppressWarnings("rawtypes")
	 public  List findAll() throws DataAccessLayerException{
	        return super.findAll(Roles.class);
	    }
	 
	 public void update(Roles roles) throws DataAccessLayerException {
	        super.saveOrUpdate(roles);
	    }
	
}
