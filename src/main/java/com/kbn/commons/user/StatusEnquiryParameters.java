package com.kbn.commons.user;

import com.kbn.crm.commons.action.AbstractSecureAction;

public class StatusEnquiryParameters extends AbstractSecureAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6981816834351382941L;

	private String txnId;
	private String payId;
	private String origTxnId;
	private String amount;
	private String authCode;
	private String rrn;
	private String acqId;
	private String status;
	private String responseCode;
	private String responseMessage;
	private String pgTxnMessage;
	private String pgRespCode;
	private String pgRefNum;
	private String rfu1;
	private String rfu2;
	private String acquirerType;
	private String orderId;
	private String txnType;
	private String pgDateTime;
	private String currencyCode;
	

	public StatusEnquiryParameters(String txnId, String payId, String origTxnId, String amount, String authCode,
			String rrn, String acqId, String status, String responseCode, String responseMessage, String pgTxnMessage,
			String pgRespCode, String pgRefNum, String rfu1, String rfu2, String acquirerType, String orderId,
			String txnType, String pgDateTime,String currencyCode) {

		this.txnId = txnId;
		this.payId = payId;
		this.origTxnId = origTxnId;
		this.amount = amount;
		this.authCode = authCode;
		this.rrn = rrn;
		this.acqId = acqId;
		this.status = status;
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.pgTxnMessage = pgTxnMessage;
		this.pgRespCode = pgRespCode;
		this.pgRefNum = pgRefNum;
		this.rfu1 = rfu1;
		this.rfu2 = rfu2;
		this.acquirerType = acquirerType;
		this.orderId = orderId;
		this.txnType = txnType;
		this.pgDateTime = pgDateTime;
		this.currencyCode=currencyCode;

	}

	public String getAcquirerType() {
		return acquirerType;
	}

	public void setAcquirerType(String acquirerType) {
		this.acquirerType = acquirerType;
	}

	

	
	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getOrigTxnId() {
		return origTxnId;
	}

	public String getPgDateTime() {
		return pgDateTime;
	}

	public void setPgDateTime(String pgDateTime) {
		this.pgDateTime = pgDateTime;
	}

	public void setOrigTxnId(String origTxnId) {
		this.origTxnId = origTxnId;
	}

	public String getAmount() {
		return amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getAcqId() {
		return acqId;
	}

	public void setAcqId(String acqId) {
		this.acqId = acqId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getPgTxnMessage() {
		return pgTxnMessage;
	}

	public void setPgTxnMessage(String pgTxnMessage) {
		this.pgTxnMessage = pgTxnMessage;
	}

	public String getPgRespCode() {
		return pgRespCode;
	}

	public void setPgRespCode(String pgRespCode) {
		this.pgRespCode = pgRespCode;
	}

	public String getPgRefNum() {
		return pgRefNum;
	}

	public void setPgRefNum(String pgRefNum) {
		this.pgRefNum = pgRefNum;
	}

	public String getRfu1() {
		return rfu1;
	}

	public void setRfu1(String rfu1) {
		this.rfu1 = rfu1;
	}

	public String getRfu2() {
		return rfu2;
	}

	public void setRfu2(String rfu2) {
		this.rfu2 = rfu2;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

}
