package com.kbn.commons.user;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TransactionType;

public class ChargingDetailsDao  extends HibernateAbstractDao{
	
	public ChargingDetailsDao() {
        super();
    }
	
	public void create(ChargingDetails chargingDetails) throws DataAccessLayerException {
        super.save(chargingDetails);
    }
	
	public void delete(ChargingDetails chargingDetails) throws DataAccessLayerException {
        super.delete(chargingDetails);
    }
	
	public void update(ChargingDetails chargingDetails) throws DataAccessLayerException {
        super.saveOrUpdate(chargingDetails);
    }
	
	@SuppressWarnings("rawtypes")
	public  List findAll() throws DataAccessLayerException{
	    return super.findAll(ChargingDetails.class);
	}
	 
	public ChargingDetails find(Long id) throws DataAccessLayerException {
	    return (ChargingDetails) super.find(ChargingDetails.class, id);
	}
	 
	public ChargingDetails find(String name) throws DataAccessLayerException {
	    return (ChargingDetails) super.find(ChargingDetails.class, name);
	}

	public List<ChargingDetails> findDetail(String date, String payId, String acquirerName, String paymentType, String mopType, String currency){
		List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();

		try {
			startOperation();
			String sqlQuery = "select * from ChargingDetails where (case when updatedDate is null then :date between createdDate and current_timestamp() else"
				+" :date between createdDate and updatedDate end) and payId=:payId and acquirerName=:acquirerName and paymentType=:paymentType and mopType=:mopType "
				+" and currency=:currency";
			chargingDetailsList = getSession().createNativeQuery(sqlQuery, ChargingDetails.class)
					   						  .setParameter("payId", payId).setParameter("date", date)
					   						  .setParameter("acquirerName", acquirerName).setParameter("mopType", mopType)
					   						  .setParameter("paymentType", paymentType).setParameter("currency", currency)
					   						  .getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return chargingDetailsList;
	}

	public boolean isChargingDetailsSet(String payId) {
		BigInteger count= new BigInteger("0");
		try {
			startOperation();
			String sqlQuery = "select count(*) from ChargingDetails where payId=:payId and status='ACTIVE' and merchantTDR > 0";
			count = (BigInteger) getSession().createNativeQuery(sqlQuery)
											 .setParameter("payId", payId)
											 .getSingleResult();
			getTx().commit();
			if(count.intValue() > 0){
				return true;
			}else{
				return false;
			}	
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return false;	
	}

	@SuppressWarnings("unchecked")
	public List<ChargingDetails> getAllActiveChargingDetails(String payId) {
		List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
		try {
			startOperation();			
			chargingDetailsList = getSession().createQuery("from ChargingDetails C where C.payId= :payId and C.status='ACTIVE' and C.merchantTDR > 0")
									.setParameter("payId", payId).setCacheable(true)
									.getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return chargingDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ChargingDetails> getAllActiveSaleChargingDetails(String payId , PaymentType paymentType) {
		List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
		try {
			startOperation();
			if(paymentType.getName().equalsIgnoreCase(PaymentType.NET_BANKING.getName())){
				chargingDetailsList = getSession().createQuery("from ChargingDetails C where C.payId= :payId and C.paymentType=:paymentType and C.status='ACTIVE'")
						.setParameter("payId", payId).setParameter("paymentType", paymentType).setCacheable(true)
						.getResultList();
			}
			else
			{
			chargingDetailsList = getSession().createQuery("from ChargingDetails C where C.payId= :payId and C.paymentType=:paymentType and C.status='ACTIVE' and C.transactionType='SALE'")
									.setParameter("payId", payId).setParameter("paymentType", paymentType).setCacheable(true)
									.getResultList();
			}
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return chargingDetailsList;
	}

	@SuppressWarnings("unchecked")
	public List<ChargingDetails> getAllActiveChargingDetails() {
		List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
		try {
			startOperation();			
			chargingDetailsList = getSession().createQuery("from ChargingDetails C where C.status='ACTIVE' and C.merchantTDR > 0")
									.getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return chargingDetailsList;
	}

	public List<ChargingDetails> findBankList(String payId){
		List<ChargingDetails> bankList = new ArrayList<ChargingDetails>();

		try {
			startOperation();
			String sqlQuery = "select * from ChargingDetails where payId=:payId and paymentType='NET_BANKING' and status='ACTIVE'";
			bankList = getSession().createNativeQuery(sqlQuery, ChargingDetails.class)
								   .setParameter("payId", payId)
								   .getResultList();
			
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return bankList;
	}
	
	public List<ChargingDetails> getPendingChargingDetailList(){
		List<ChargingDetails> pendingChargingDetailsList = new ArrayList<ChargingDetails>();

		try {
			startOperation();
			String sqlQuery = "select * from ChargingDetails where status='PENDING'";
			pendingChargingDetailsList = getSession().createNativeQuery(sqlQuery, ChargingDetails.class)
								   .getResultList();
			
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return pendingChargingDetailsList;
	}

	public List<ChargingDetails> findCardList(String payId){
		List<ChargingDetails> bankList = new ArrayList<ChargingDetails>();
		
		try {
			startOperation();
			String sqlQuery = "select * from ChargingDetails where payId=:payId and (paymentType='CREDIT_CARD' or paymentType='DEBIT_CARD') and status='ACTIVE'";

			bankList = getSession().createNativeQuery(sqlQuery, ChargingDetails.class).setParameter("payId", payId).getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return bankList;
	}
	
	public  ChargingDetails findPendingChargingDetail (MopType mopType , PaymentType paymentType , TransactionType transactionType , String acquirerName , String currency,
			String payId){
		
		ChargingDetails chargingDetails = null;	
		try {
			startOperation();
			chargingDetails = (ChargingDetails) getSession()
					.createQuery("from ChargingDetails CD where CD.mopType = :mopType and CD.transactionType =:transactionType and CD.acquirerName = :acquirerName and "
							+ "CD.payId = :payId and CD.paymentType = :paymentType and CD.currency = :currency and CD.status = 'PENDING'")
					.setParameter("payId", payId).setParameter("paymentType", paymentType)
					.setParameter("mopType", mopType)
					.setParameter("transactionType", transactionType)
					.setParameter("acquirerName", acquirerName)
					.setParameter("currency", currency)
					.setParameter("paymentType", paymentType).setCacheable(true)
					.uniqueResult();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;
		}
		return chargingDetails;
		
	}
	
	
	public  ChargingDetails findActiveChargingDetail (MopType mopType , PaymentType paymentType , TransactionType transactionType , String acquirerName , String currency,
			String payId){
		
		ChargingDetails chargingDetails = null;
		try {
			startOperation();
			chargingDetails = (ChargingDetails) getSession()
					.createQuery("from ChargingDetails CD where CD.mopType = :mopType and CD.transactionType =:transactionType and CD.acquirerName = :acquirerName and "
							+ "CD.payId = :payId and CD.paymentType = :paymentType and CD.currency = :currency and CD.status = 'ACTIVE'")
					.setParameter("payId", payId).setParameter("paymentType", paymentType)
					.setParameter("mopType", mopType)
					.setParameter("transactionType", transactionType)
					.setParameter("acquirerName", acquirerName)
					.setParameter("currency", currency)
					.setParameter("paymentType", paymentType).setCacheable(true)
					.uniqueResult();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			// return responseUser;
		}
		return chargingDetails;
	}
	
	
	
	
	public Double getBankTDR(String acquirerName,String mopType,String paymentType,String transactionType) throws HibernateException, Exception{
		//BigInteger count= new BigInteger("0");
		Double result=null;
			startOperation();
			String sqlQuery = "select bankTDR from ChargingDetails where acquirerName=:acquirerName and mopType=:mopType and paymentType=:paymentType and transactionType=:transactionType";
			result=(Double)getSession().createNativeQuery(sqlQuery)
					.setParameter("acquirerName", acquirerName)
											.setParameter("mopType", mopType)
											.setParameter("paymentType", paymentType)
											.setParameter("transactionType", transactionType)
											 .getSingleResult();
		return result;	
	}
	
	
	
}
