package com.kbn.commons.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import org.hibernate.annotations.Proxy;

import com.kbn.commons.util.ReccuringCycleType;
import com.kbn.commons.util.SubscriptionStatus;
/**
 * @author Puneet
 *
 */
@Entity@Proxy(lazy= false)
public class CitrusPaySubscription implements Serializable{

	private static final long serialVersionUID = 2793989169080160573L;

	@Id@Column(nullable=false,unique=true)
	private String recurringTransactionId;
	@Column(nullable=false,unique=true)
	private String subscriptionId;
	private String payId;
	private String customerEmail;
	private String merchantId;
	private String nextBillingDate;
	private String lastPaymentDate;
	private String transactionOid;
	private String amount;
	private String cardHolderName;
	private String currency;
	private String responseMessage;
	private String responseCode;
	private String orderId;
	private String callBackUrl;
	private Long recurringTransactionsProcessed;
	private Long recurringTransactionCount;
	//for charging a payment more than once in a cycle (RFU)
	private Long billingPeriodUnit;

	@Enumerated(EnumType.STRING)
	private SubscriptionStatus status;
	@Enumerated(EnumType.STRING)
	private ReccuringCycleType recurringTransactionInterval;

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getNextBillingDate() {
		return nextBillingDate;
	}

	public void setNextBillingDate(String nextBillingDate) {
		this.nextBillingDate = nextBillingDate;
	}

	public String getLastPaymentDate() {
		return lastPaymentDate;
	}

	public void setLastPaymentDate(String lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	public String getTransactionOid() {
		return transactionOid;
	}

	public void setTransactionOid(String transactionOid) {
		this.transactionOid = transactionOid;
	}

	public Long getRecurringTransactionsProcessed() {
		return recurringTransactionsProcessed;
	}

	public void setRecurringTransactionsProcessed(
			Long recurringTransactionsProcessed) {
		this.recurringTransactionsProcessed = recurringTransactionsProcessed;
	}

	public Long getRecurringTransactionCount() {
		return recurringTransactionCount;
	}

	public void setRecurringTransactionCount(Long recurringTransactionCount) {
		this.recurringTransactionCount = recurringTransactionCount;
	}

	public ReccuringCycleType getRecurringTransactionInterval() {
		return recurringTransactionInterval;
	}

	public void setRecurringTransactionInterval(
			ReccuringCycleType recurringTransactionInterval) {
		this.recurringTransactionInterval = recurringTransactionInterval;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Long getBillingPeriodUnit() {
		return billingPeriodUnit;
	}

	public void setBillingPeriodUnit(Long billingPeriodUnit) {
		this.billingPeriodUnit = billingPeriodUnit;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getRecurringTransactionId() {
		return recurringTransactionId;
	}

	public void setRecurringTransactionId(String recurringTransactionId) {
		this.recurringTransactionId = recurringTransactionId;
	}

	public SubscriptionStatus getStatus() {
		return status;
	}

	public void setStatus(SubscriptionStatus status) {
		this.status = status;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}
}
