package com.kbn.commons.user;

import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

public class DynamicPaymentPageDao extends HibernateAbstractDao {
	
	private static Logger logger = Logger.getLogger(DynamicPaymentPageDao.class
			.getName());
	private static final String getCompleteDetail = "from DynamicPaymentPage D where D.payId = :payId";
	public DynamicPaymentPageDao(){
		 super();
	}
	public void create(DynamicPaymentPage dynamicPaymentPage) throws DataAccessLayerException {
        super.save(dynamicPaymentPage);
    }

	public void delete(DynamicPaymentPage dynamicPaymentPage) throws DataAccessLayerException {
        super.delete(dynamicPaymentPage);
    }

	public void update(DynamicPaymentPage dynamicPaymentPage) throws DataAccessLayerException {
        super.saveOrUpdate(dynamicPaymentPage);
    }

	public DynamicPaymentPage findPayId(String payId1) {
		return (DynamicPaymentPage) findByPayId(payId1);
	}

	protected DynamicPaymentPage findByPayId(String payId1) {

		DynamicPaymentPage responseUser = null;
		try {
			startOperation();
			responseUser = (DynamicPaymentPage) getSession().createQuery(getCompleteDetail)
			.setParameter("payId", payId1)
			.setCacheable(true).getSingleResult();
			getTx().commit();

			return responseUser;
		}catch (NoResultException noResultException){
					return null;
		}catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			logger.error(hibernateException);
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return responseUser;
	}
}
