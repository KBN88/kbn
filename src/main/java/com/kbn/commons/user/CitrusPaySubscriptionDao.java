package com.kbn.commons.user;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

/**
 * @author Puneet
 *
 */
public class CitrusPaySubscriptionDao extends HibernateAbstractDao {

	private final static String fectchCompletedSubsQuery = "from CitrusPaySubscription C where C.recurringTransactionsProcessed = C.recurringTransactionCount and status='ACTIVE'";
	private final static String getSubscriptionByIdQuery = "from CitrusPaySubscription C where C.subscriptionId = :subscriptionId";
	
	public void create(CitrusPaySubscription citrusPaySubscription) throws DataAccessLayerException {
		super.save(citrusPaySubscription);
	}

	public void delete(CitrusPaySubscription citrusPaySubscription) throws DataAccessLayerException {
		super.delete(citrusPaySubscription);
	}

	public CitrusPaySubscription find(Long id) throws DataAccessLayerException {
		return (CitrusPaySubscription) super.find(CitrusPaySubscription.class, id);
	}

	public CitrusPaySubscription find(String name) throws DataAccessLayerException {
		return (CitrusPaySubscription) super.find(CitrusPaySubscription.class, name);
	}

	@SuppressWarnings("rawtypes")
	public List findAll() throws DataAccessLayerException {
		return super.findAll(CitrusPaySubscription.class);
	}

	public void update(CitrusPaySubscription citrusPaySubscription) throws DataAccessLayerException {
		super.saveOrUpdate(citrusPaySubscription);
	}

	@SuppressWarnings("unchecked")
	public List<CitrusPaySubscription> findCompletedSubs(){
		List<CitrusPaySubscription> list = new ArrayList<CitrusPaySubscription>();
		try{
			startOperation();
			list = getSession().createQuery(fectchCompletedSubsQuery).getResultList();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return list;
	}

	public CitrusPaySubscription findBySubscriptionId(String subscriptionId){
		CitrusPaySubscription citrusPaySubscription = new CitrusPaySubscription();
		try{
			startOperation();
			citrusPaySubscription = (CitrusPaySubscription) getSession().createQuery(getSubscriptionByIdQuery)
		    .setParameter("subscriptionId", subscriptionId).getSingleResult();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return citrusPaySubscription;
	}
}
