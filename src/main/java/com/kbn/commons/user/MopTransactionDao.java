package com.kbn.commons.user;

import java.util.List;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

public class MopTransactionDao extends HibernateAbstractDao{
	
	public MopTransactionDao() {
        super();
    }

	public void create(MopTransaction mopTransaction) throws DataAccessLayerException {
        super.save(mopTransaction);
    }
	
	public void delete(MopTransaction mopTransaction) throws DataAccessLayerException {
        super.save(mopTransaction);
    }
	
	public void update(MopTransaction mopTransaction) throws DataAccessLayerException {
        super.saveOrUpdate(mopTransaction);
    }
	
	@SuppressWarnings("rawtypes")
	public  List findAll() throws DataAccessLayerException{
	    return super.findAll(MopTransaction.class);
	}
	 
	public MopTransaction find(Long id) throws DataAccessLayerException {
	    return (MopTransaction) super.find(MopTransaction.class, id);
	}
	 
	public MopTransaction find(String name) throws DataAccessLayerException {
	    return (MopTransaction) super.find(MopTransaction.class, name);
	}

}
