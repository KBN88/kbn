package com.kbn.commons.user;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

/**
 * @author Rahul
 *
 */
public class PendingResellerMappingDao extends HibernateAbstractDao{
	
	public PendingResellerMappingDao() {
	}

	@SuppressWarnings("unused")
	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}
	
	public void create(PendingResellerMappingApproval pendingResellerMappingApproval) throws DataAccessLayerException {
		super.save(pendingResellerMappingApproval);
	}

	public void update(PendingResellerMappingApproval pendingResellerMappingApproval) throws DataAccessLayerException {
		super.saveOrUpdate(pendingResellerMappingApproval);
	}
	
	public PendingResellerMappingApproval find(String emailId) {
		PendingResellerMappingApproval responseUser = null;
		try {
			startOperation();
			responseUser = (PendingResellerMappingApproval) getSession()
					.createQuery("from PendingResellerMappingApproval PR where PR.merchantEmailId = :emailId and PR.requestStatus = 'PENDING'")
					.setParameter("emailId", emailId).setCacheable(true).uniqueResult();	
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return responseUser;

	}
	
	public boolean checkExistingMappingRequest(String emailId, String reseller) {
		PendingResellerMappingApproval pendingRequest = null;
		boolean exists = false ;
		try {
			
			startOperation();
			pendingRequest = (PendingResellerMappingApproval) getSession()
					.createQuery("from PendingResellerMappingApproval PR where PR.merchantEmailId = :emailId and PR.resellerId = :reseller and PR.requestStatus = 'PENDING'")
					.setParameter("emailId", emailId)
					.setParameter("reseller", reseller)
					.setCacheable(true).uniqueResult();	
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		if (pendingRequest != null){
			exists = true;
		}
		return exists;

	}
	
	
	public PendingResellerMappingApproval findExistingMappingRequest(String emailId, String reseller) {
		PendingResellerMappingApproval pendingRequest = null;
		try {
			
			startOperation();
			pendingRequest = (PendingResellerMappingApproval) getSession()
					.createQuery("from PendingResellerMappingApproval PR where PR.merchantEmailId = :emailId and PR.resellerId = :reseller and PR.requestStatus = 'PENDING'")
					.setParameter("emailId", emailId)
					.setParameter("reseller", reseller)
					.setCacheable(true).uniqueResult();	
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return pendingRequest;

	}
	
	
	@SuppressWarnings("unchecked")
	public List<PendingResellerMappingApproval> getPendingResellerMappingList() {
		List<PendingResellerMappingApproval> userProfileList = new ArrayList<PendingResellerMappingApproval>();
		userProfileList = null;
		try {

			startOperation();

			userProfileList = getSession().createQuery("from PendingResellerMappingApproval PRMA where PRMA.requestStatus = 'PENDING'").setCacheable(true).getResultList();
			getTx().commit();
			return userProfileList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return userProfileList;
	}

}
