package com.kbn.commons.user;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.SystemException;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.exception.ErrorType;

public class RemittanceDao extends HibernateAbstractDao {
	public RemittanceDao() {
		super();
	}

	public void create(Remittance remittanceDao)
			throws DataAccessLayerException {
		super.save(remittanceDao);
	}

	public void delete(Remittance remittanceDao)
			throws DataAccessLayerException {
		super.delete(remittanceDao);
	}

	public void update(Remittance remittanceDao)
			throws DataAccessLayerException {
		super.saveOrUpdate(remittanceDao);
	}

	public List<Remittance> findDetail(String dateFrom, String dateTo,
			String payId, String status) throws ParseException {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = formatDate.format(df.parse(dateFrom));
		String endDate = formatDate.format(df.parse(dateTo));
		List<Remittance> remittancedetailsList = new ArrayList<Remittance>();

		try {
			startOperation();
			String sqlQuery = "select * from Remittance where "
					+ " (remittanceDate >= :dateFrom and remittanceDate <= :dateTo)  and payId like :payId  "
					+ " and status like :status order by id desc ";
			if (payId.trim().equals("ALL")) {
				payId = "%";
			}
			if (status.trim().equals("ALL")) {
				status = "%";
			}
			remittancedetailsList = getSession().createNativeQuery(sqlQuery,Remittance.class)
										 .setParameter("dateFrom", startDate)
										 .setParameter("dateTo", endDate)
										 .setParameter("payId", payId)
										 .setParameter("status", status).getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return remittancedetailsList;
	}

	public void updateStatus(String utr, String status) throws SystemException {
		try {
			startOperation();
			int updateStatus = getSession().createQuery("update Remittance R set R.status = :status where R.utr = :utr")
						.setParameter("status", status)
						.setParameter("utr", utr).executeUpdate();			
			getTx().commit();
			if(1!=updateStatus){
				throw new SystemException("Error updating remittance with UTR: " + utr +ErrorType.DATABASE_ERROR);
			}
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
	}

	public Boolean findUtr(String utr) {

		String utrNumber;
		try {
			startOperation();
			String sqlQuery = "select IFNULL(MIN(utr),'') AS utr from Remittance where utr = :utr ";
			utrNumber = (String) getSession().createNativeQuery(sqlQuery)
										 .setParameter("utr", utr).getSingleResult();
			getTx().commit();
			if (utrNumber.isEmpty()) {
				return true;
			} else {
				return false;
			}
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();

		}
		return false;
	}

}
