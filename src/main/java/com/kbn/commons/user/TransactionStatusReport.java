package com.kbn.commons.user;

import java.io.Serializable;

public class TransactionStatusReport implements Serializable{

	/**
	 * @neeraj
	 */
	private static final long serialVersionUID = -2876227768207414182L;
	private String status;
	private String responseCode;
	private String approvedAmount;
	private String transactionId;
	private String message;
	
	private String moptype;
	private String cardmask;
	private String acqId;
	private String txnType;
	private String currency;
	
	private String paymentType;
	private String payId;
	private String orderId;
	private String orginTxnId;
	private String custEmail;
	private String custPhone;
	private String dateTime;
	
	
	
	public String getMoptype() {
		return moptype;
	}
	public void setMoptype(String moptype) {
		this.moptype = moptype;
	}
	public String getCardmask() {
		return cardmask;
	}
	public void setCardmask(String cardmask) {
		this.cardmask = cardmask;
	}
	public String getAcqId() {
		return acqId;
	}
	public void setAcqId(String acqId) {
		this.acqId = acqId;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrginTxnId() {
		return orginTxnId;
	}
	public void setOrginTxnId(String orginTxnId) {
		this.orginTxnId = orginTxnId;
	}
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getCustPhone() {
		return custPhone;
	}
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	public String getApprovedAmount() {
		return approvedAmount;
	}
	public void setApprovedAmount(String approvedAmount) {
		this.approvedAmount = approvedAmount;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	
}