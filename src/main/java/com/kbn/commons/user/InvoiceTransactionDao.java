package com.kbn.commons.user;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

public class InvoiceTransactionDao extends HibernateAbstractDao{
	
	public InvoiceTransactionDao() {
        super();
    }
	public void create(Invoice invoiceTransaction) throws DataAccessLayerException {
        super.save(invoiceTransaction);
    }

	public void delete(Invoice invoiceTransaction) throws DataAccessLayerException {
        super.delete(invoiceTransaction);
    }

	public void update(Invoice invoiceTransaction) throws DataAccessLayerException {
        super.saveOrUpdate(invoiceTransaction);
    }

	public Invoice findByInvoiceId(String invoiceId) {
		Invoice invoice = null;
		try {
			startOperation();
			invoice = (Invoice) getSession().createQuery("from Invoice I where I.invoiceId = :invoiceId")
												.setParameter("invoiceId", invoiceId).getSingleResult();
			getTx().commit();
			return invoice;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return invoice;
	}
}
