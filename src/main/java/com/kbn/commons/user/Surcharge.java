package com.kbn.commons.user;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TDRStatus;

@Entity
@Proxy(lazy= false)
@Table
public class Surcharge implements Serializable{
	
	private static final long serialVersionUID = 6451344616066902378L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private PaymentType paymentType;
	
	private String payId;
	private String acquirerName;
	private String onOff;
	
	@Enumerated(EnumType.STRING)
	private TDRStatus status;
	
	@Enumerated(EnumType.STRING)
	private MopType mopType;
	
	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private boolean allowOnOff;
	
	private String merchantIndustryType;
	private BigDecimal bankSurchargeAmount;
	private BigDecimal bankSurchargePercentage;
	@Transient
	private BigDecimal merchantSurchargeAmount;
	@Transient
	private BigDecimal merchantSurchargePercentage;
	@Transient
	private BigDecimal pgSurchargeAmount;
	@Transient
	private BigDecimal pgSurchargePercentage;
	@Transient
	private BigDecimal serviceTax;
	private Date createdDate;
	private Date updatedDate;
	private transient String merchantName;
	
	private String requestedBy;
	private String processedBy;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public PaymentType getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public String getOnOff() {
		return onOff;
	}
	public void setOnOff(String onOff) {
		this.onOff = onOff;
	}
	public String getMerchantIndustryType() {
		return merchantIndustryType;
	}
	public void setMerchantIndustryType(String merchantIndustryType) {
		this.merchantIndustryType = merchantIndustryType;
	}
	public BigDecimal getBankSurchargeAmount() {
		return bankSurchargeAmount;
	}
	public void setBankSurchargeAmount(BigDecimal bankSurchargeAmount) {
		this.bankSurchargeAmount = bankSurchargeAmount;
	}
	public BigDecimal getBankSurchargePercentage() {
		return bankSurchargePercentage;
	}
	public void setBankSurchargePercentage(BigDecimal bankSurchargePercentage) {
		this.bankSurchargePercentage = bankSurchargePercentage;
	}
	public BigDecimal getPgSurchargeAmount() {
		return pgSurchargeAmount;
	}
	public void setPgSurchargeAmount(BigDecimal pgSurchargeAmount) {
		this.pgSurchargeAmount = pgSurchargeAmount;
	}
	public BigDecimal getPgSurchargePercentage() {
		return pgSurchargePercentage;
	}
	public void setPgSurchargePercentage(BigDecimal pgSurchargePercentage) {
		this.pgSurchargePercentage = pgSurchargePercentage;
	}
	public BigDecimal getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(BigDecimal serviceTax) {
		this.serviceTax = serviceTax;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public BigDecimal getMerchantSurchargeAmount() {
		return merchantSurchargeAmount;
	}
	public void setMerchantSurchargeAmount(BigDecimal merchantSurchargeAmount) {
		this.merchantSurchargeAmount = merchantSurchargeAmount;
	}
	public BigDecimal getMerchantSurchargePercentage() {
		return merchantSurchargePercentage;
	}
	public void setMerchantSurchargePercentage(BigDecimal merchantSurchargePercentage) {
		this.merchantSurchargePercentage = merchantSurchargePercentage;
	}
	public MopType getMopType() {
		return mopType;
	}
	public void setMopType(MopType mopType) {
		this.mopType = mopType;
	}
	public TDRStatus getStatus() {
		return status;
	}
	public void setStatus(TDRStatus status) {
		this.status = status;
	}
	public String getAcquirerName() {
		return acquirerName;
	}
	public void setAcquirerName(String acquirerName) {
		this.acquirerName = acquirerName;
	}
	public boolean isAllowOnOff() {
		return allowOnOff;
	}
	public void setAllowOnOff(boolean allowOnOff) {
		this.allowOnOff = allowOnOff;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	public String getProcessedBy() {
		return processedBy;
	}
	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	
}
