package com.kbn.commons.user;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.util.PaymentType;

public class SurchargeDetailsDao extends HibernateAbstractDao{
	
	public void create(SurchargeDetails surchargeDetails) throws DataAccessLayerException {
		super.save(surchargeDetails);
	}
	
	public SurchargeDetails findDetails(String payId, String paymentType){
		SurchargeDetails responseUser = null; 
		try {
			startOperation();
			responseUser = (SurchargeDetails) getSession().createQuery("from SurchargeDetails SD where SD.payId = :payId and SD.paymentType = :paymentType and SD.status = 'ACTIVE'" )
					.setParameter("payId", payId)
					.setParameter("paymentType", paymentType)
					.setCacheable(true).uniqueResult();
			getTx().commit();
		}catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			//return responseUser;
		}
		return responseUser;
		
	}
	
	public SurchargeDetails findPendingDetails(String payId, String paymentType){
		SurchargeDetails responseUser = null; 
		try {
			startOperation();
			responseUser = (SurchargeDetails) getSession().createQuery("from SurchargeDetails SD where SD.payId = :payId and SD.paymentType = :paymentType and SD.status = 'PENDING'" )
					.setParameter("payId", payId)
					.setParameter("paymentType", paymentType)
					.setCacheable(true).uniqueResult();
			getTx().commit();
		}catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
			//return responseUser;
		}
		return responseUser;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<SurchargeDetails> findPendingDetails() {
		List<SurchargeDetails> pendingSurchargeDetailsList = new ArrayList<SurchargeDetails>();
		try {
			startOperation();
			pendingSurchargeDetailsList = getSession().createQuery("from SurchargeDetails SD where SD.status='PENDING'")
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return pendingSurchargeDetailsList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return pendingSurchargeDetailsList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SurchargeDetails> getActiveSurchargeDetailsByPayId(String payId) {
		List<SurchargeDetails> activeSurchargeDetailsList = new ArrayList<SurchargeDetails>();
		try {
			startOperation();
			activeSurchargeDetailsList = getSession().createQuery("from SurchargeDetails SD where SD.payId = :payId and SD.status='ACTIVE'")
					.setParameter("payId", payId)
					.setCacheable(true)
					.getResultList();
			getTx().commit();
			return activeSurchargeDetailsList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return activeSurchargeDetailsList;
	}
}
