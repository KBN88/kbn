package com.kbn.commons.user;

import java.util.List;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

public class AccountDao extends HibernateAbstractDao {

	public AccountDao(){
		super();
	}

	public void create(Account account) throws DataAccessLayerException {
        super.save(account);
    }
	
	public void delete(Account account) throws DataAccessLayerException {
        super.delete(account);
    }
	
	public void update(Account account) throws DataAccessLayerException {
        super.saveOrUpdate(account);
    }
	
	@SuppressWarnings("rawtypes")
	public  List findAll() throws DataAccessLayerException{
	    return super.findAll(Account.class);
	}
	 
	public Account find(Long id) throws DataAccessLayerException {
	    return (Account) super.find(Account.class, id);
	}
	 
	public Account find(String name) throws DataAccessLayerException {
	    return (Account) super.find(Account.class, name);
	}
}
