package com.kbn.commons.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.pg.core.Currency;

public class SettlementDao extends HibernateAbstractDao {

	private static Logger logger = Logger.getLogger(SettlementDao.class
			.getName());

	public SettlementDao() {
	}

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public void create(Settlement settlement) throws DataAccessLayerException {
		super.save(settlement);
	}

	public List<Settlement> findDetail(String dateFrom, String dateTo,
			String payId, String currency, String paymentType)
			throws SystemException, ParseException, SQLException {

		List<Settlement> settlementRecordList = new ArrayList<Settlement>();

		String toDate = dateTo;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(toDate));
		c.add(Calendar.DATE, 1); // number of days to add
		toDate = sdf.format(c.getTime());

		java.util.Date to = sdf.parse(toDate);
		java.util.Date from = sdf.parse(dateFrom);
		sdf.applyPattern("yyyy-MM-dd");
		toDate = sdf.format(to);
		dateFrom = sdf.format(from);

		SimpleDateFormat formatDate = new SimpleDateFormat(
				CrmFieldConstants.DATE_TIME_FORMAT.getValue());

		try (Connection connection = getConnection()) {
			try (PreparedStatement prepStmt = connection
					.prepareStatement("{call settlementRecord(?,?,?,?,?)}")) {
				prepStmt.setString(1, dateFrom);
				prepStmt.setString(2, toDate);
				prepStmt.setString(3, payId);
				prepStmt.setString(4, currency);
				prepStmt.setString(5, paymentType);
				try (ResultSet rs = prepStmt.executeQuery()) {

					while (rs.next()) {
						Settlement settlement = new Settlement();
						settlement.setTxnDate(formatDate.format(rs
								.getTimestamp("txnDate")));
						settlement.setCreateDate(formatDate(rs
								.getTimestamp("creationTimeStamp")));
						settlement.setMerchant(rs.getString("merchant"));
						settlement.setTxnId(rs.getString("txnId"));
						settlement.setPaymentMethod(rs
								.getString("paymentMethod"));
						settlement.setTxnType(rs.getString("txnType"));
						settlement.setMop(rs.getString("mop"));
						settlement
								.setCurrencyCode(Currency.getAlphabaticCode(rs
										.getString("currencyCode")));
						if ("REFUND".equals((rs.getString("txnType")))) {
							settlement.setTxnAmount('-' + rs
									.getString("txnAmount"));
						} else {
							settlement.setTxnAmount(rs.getString("txnAmount"));
						}
						settlement.setMerchantFixCharge(rs
								.getString("merchantFixCharge"));
						settlement.setTdr(rs.getString("tdr"));
						settlement.setServiceTax(rs.getString("serviceTax"));
						if ("REFUND".equals((rs.getString("txnType")))) {
							settlement.setNetAmount('-' + rs
									.getString("netAmount"));
						} else {
							settlement.setNetAmount(rs.getString("netAmount"));
						}

						settlementRecordList.add(settlement);
					}
				}
			}
		} catch (SQLException exception) {
			logger.error("Database error");
			throw new SystemException(ErrorType.DATABASE_ERROR,
					ErrorType.DATABASE_ERROR.getResponseMessage());
		}
		return settlementRecordList;
	}

	// TODO... move to date creater and merge with settlementprocessorFunction
	public static String formatDate(Date date) throws ParseException {
		try {
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy 'T' HH:mm:ss");
			return df.format(date);
		} catch (Exception exception) {
			logger.error("Error parsing date for settlement report");
		}
		return null;
	}
}
