package com.kbn.commons.security;

public class AuthorityFactory {

	private AuthorityFactory() {
	}

	public static Authority getAuthority(){
		return new AuthorityImpl();
	}
}
