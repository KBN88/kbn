package com.kbn.commons.security;

import com.kbn.commons.exception.ErrorType;
import com.kbn.crm.commons.action.AbstractAction;

public class AuthorityImpl implements Authority {

	public AuthorityImpl() {
	}

	public String checkLogin(String userId, String password) {
		String result = AbstractAction.INPUT;
		
		Authenticator authenticator = AuthenticatorFactory.getAuthenticator();
		ErrorType errorType = authenticator.checkLogin(userId, password);
		
		if(errorType.equals(ErrorType.SUCCESS)){
			result = AbstractAction.LOGIN;
		}
		
		return result;
	}
}
