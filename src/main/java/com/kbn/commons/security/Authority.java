package com.kbn.commons.security;

public interface Authority {
	public String checkLogin(String userId, String password);
}
