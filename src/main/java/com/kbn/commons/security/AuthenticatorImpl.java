package com.kbn.commons.security;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

import com.kbn.amex.Constants;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.commons.util.UserStatusType;
import com.kbn.pg.action.beans.PaymentTypeProvider;

public class AuthenticatorImpl implements Authenticator {
	private User user;
	private List<ChargingDetails> supportedChargingDetailsList = new ArrayList<ChargingDetails>();
	public AuthenticatorImpl() {
	}

	public ErrorType checkLogin(String userId, String password) {
		ErrorType errorType = ErrorType.UNKNOWN;
		return errorType;
	}

	public User getUserFromPayId(Fields fields) throws SystemException{
		UserDao userDao = new UserDao();
		User user = userDao.findPayId(fields.get(FieldType.PAY_ID.getName()));
		return user;
	}

	public void authenticate(Fields fields) throws SystemException {

		String payId = fields.get(FieldType.PAY_ID.getName());
		User user_ = getUser();

		//Check if user is found
		if(null == user_){
			throw new SystemException(ErrorType.USER_NOT_FOUND, "No such user, PayId=" + payId);
		}

		//Check transaction status of user
		UserStatusType userStatus = user_.getUserStatus();
		if(!userStatus.equals(UserStatusType.ACTIVE) && !userStatus.equals(UserStatusType.PENDING)){
			if(!(fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.STATUS.getName()))){
				fields.put(FieldType.STATUS.getName(),StatusType.AUTHENTICATION_FAILED.getName());
				throw new SystemException(ErrorType.PERMISSION_DENIED, "User not allowed to transact, PayId=" + payId);
			}
		}

		String merchantHostedFlag = fields.get(FieldType.IS_MERCHANT_HOSTED.getName());
		if(!StringUtils.isBlank(merchantHostedFlag) && merchantHostedFlag.equals(Constants.Y)){
			if(!user.isMerchantHostedFlag()){
				fields.put(FieldType.STATUS.getName(),StatusType.INVALID.getName());
				throw new SystemException(ErrorType.PAYMENT_OPTION_NOT_SUPPORTED, "Merchant not allowed to perform direct transactoin payId= " + payId);
			}
		}
    }

	public void validatePaymentOptions(Fields fields) throws SystemException{
		PaymentTypeProvider paymentTypeProvider = new PaymentTypeProvider(user.getPayId()); 
		List<ChargingDetails> chargingDetailsList = paymentTypeProvider.getChargingDetailsList();				
		List<ChargingDetails> supportedChargingDetailsList = new ArrayList<ChargingDetails>();
		String paymentTypeCode = fields.get(FieldType.PAYMENT_TYPE.getName());
		String mopTypeCode = fields.get(FieldType.MOP_TYPE.getName());
		for(ChargingDetails chargingDetails:chargingDetailsList){
			if(chargingDetails.getPaymentType().getCode().equals(paymentTypeCode) && 
					chargingDetails.getMopType().getCode().equals(mopTypeCode)){
				supportedChargingDetailsList.add(chargingDetails);
			}
		}
		if(supportedChargingDetailsList.isEmpty()){
			fields.put(FieldType.STATUS.getName(),StatusType.INVALID.getName());
			throw new SystemException(ErrorType.PAYMENT_OPTION_NOT_SUPPORTED, "Merchant not supported for this transactoin type payId= " + fields.get(FieldType.PAY_ID.getName()));
		}
		setSupportedChargingDetailsList(supportedChargingDetailsList);
	}

	public void isUserExists(Fields fields) throws SystemException {
		String payId = fields.get(FieldType.PAY_ID.getName());
		User user_ = getUserFromPayId(fields);

		//if user is found
		if(null == user_){
			throw new SystemException(ErrorType.USER_NOT_FOUND, "No such user, PayId=" + payId);
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<ChargingDetails> getSupportedChargingDetailsList() {
		return supportedChargingDetailsList;
	}

	public void setSupportedChargingDetailsList(
			List<ChargingDetails> supportedChargingDetailsList) {
		this.supportedChargingDetailsList = supportedChargingDetailsList;
	}
}
