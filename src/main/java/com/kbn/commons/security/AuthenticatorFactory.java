package com.kbn.commons.security;

public class AuthenticatorFactory {

	private AuthenticatorFactory() {
	}
	
	public static Authenticator getAuthenticator(){
		return new AuthenticatorImpl();
	}
}
