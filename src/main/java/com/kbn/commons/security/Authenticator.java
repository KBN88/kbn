package com.kbn.commons.security;

import java.util.List;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Fields;

public interface Authenticator {

	public ErrorType checkLogin(String userId, String password);
	public void authenticate(Fields fields) throws SystemException;
	public User getUserFromPayId(Fields fields) throws SystemException;
	public User getUser();
	public void setUser(User user);
	public void validatePaymentOptions(Fields fields) throws SystemException;
	public List<ChargingDetails> getSupportedChargingDetailsList();
	public void isUserExists(Fields fields) throws SystemException;
}
