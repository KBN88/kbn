package com.kbn.commons.util;

import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class DefaultBinRangeAPI {

	public void getdefualBinrage(Fields fields, String cardBin) throws IOException, ParseException {
		String citrusApiFlag = (ConfigurationConstants.BIN_RANGE_CITRUS_API.getValue());
		String requestUrl;
		if (citrusApiFlag.equals("1")) {
			requestUrl = (ConfigurationConstants.CITRUS_BIN_RANGE_IDENTIFIER_URL.getValue());
			BinRangeCommunicator binRangeCommunicator = new BinRangeCommunicator();
			StringBuilder response = binRangeCommunicator.getCommunicator(requestUrl, cardBin);
			BinRangeParser binRangeParser = new BinRangeParser();
			JSONObject jObject = binRangeParser.getBinParser(response);
			fetchYeasBankAPI(fields, jObject);
		} else {
			requestUrl = (ConfigurationConstants.BIN_RANGE_IDENTIFIER_URL.getValue());
			BinRangeCommunicator binRangeCommunicator = new BinRangeCommunicator();
			StringBuilder response = binRangeCommunicator.getCommunicator(requestUrl, cardBin);
			BinRangeParser binRangeParser = new BinRangeParser();
			JSONObject jObject = binRangeParser.getBinParser(response);
			fetchBinDBAPI(fields, jObject);
		}
	}

	// Using BINDB API
	private void fetchBinDBAPI(Fields fields, JSONObject jObject) {

		Object mopType = jObject.get("brand");
		mopType = BinMapperType.getCodeUsingName(mopType.toString());
		if (null != mopType) {
			fields.put(FieldType.MOP_TYPE.getName(), (String) mopType);
		}

		Object issuer = jObject.get("issuer");
		if (null != issuer) {
			fields.put(FieldType.INTERNAL_CARD_ISSUER_BANK.getName(), (String) issuer);
		}

		Object paymentType = jObject.get("type");
		paymentType = BinMapperType.getCodeUsingName(paymentType.toString());
		if (null != paymentType) {
			fields.put(FieldType.PAYMENT_TYPE.getName(), (String) paymentType);
		}

		Object cardCountryCode = jObject.get("country_code");
		cardCountryCode = BinCountryMapperType.getNameUsingCode(cardCountryCode.toString());
		if (null != cardCountryCode) {
			fields.put(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName(), (String) cardCountryCode);
		}

	}
	
	// Using YesBank BinRange API
	private void fetchYeasBankAPI(Fields fields, JSONObject jObject) {
		Object cardScheme = jObject.get("card-brand");
		cardScheme = BinMapperType.getCodeUsingName(cardScheme.toString().toUpperCase());
		if(null != cardScheme){
			fields.put(FieldType.MOP_TYPE.getName(),(String)cardScheme);
		}

		Object issuingBank = jObject.get("issuer");
		if(null != issuingBank){
			fields.put(FieldType.INTERNAL_CARD_ISSUER_BANK.getName(), (String)issuingBank);
		}

		Object cardType = jObject.get("card-type");
		cardType = BinMapperType.getCodeUsingName(cardType.toString());
		if(null != cardType){
			fields.put(FieldType.PAYMENT_TYPE.getName(), (String)cardType);
		}

		Object cardCountry = jObject.get("country");
		if(null != cardCountry){
			fields.put(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName(), (String)cardCountry);
		}

	}
}
