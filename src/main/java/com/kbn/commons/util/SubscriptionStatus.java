package com.kbn.commons.util;

public enum SubscriptionStatus {

	ACTIVE		("Active"),
	INACTIVE	("Inactive"),
	ERROR		("Error");

	private final String name;

	private SubscriptionStatus(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
