package com.kbn.commons.util;

public enum ReccuringCycleType {

	DAILY       ("daily", "d", true),
	WEEKLY      ("weekly", "w", true),
	MONTHLY     ("monthly", "m", true),
	QUARTERLY   ("quarterly", "q", false),
	HALF_YEARLY ("halfYearly", "h", false),
	YEARLY      ("yearly", "y", false);

	private final String name;
	private final String code;
	private final boolean isCitrusSupported;

	private ReccuringCycleType(String name, String code, boolean isCitrusSupported) {
		this.name = name;
		this.code = code;
		this.isCitrusSupported = isCitrusSupported;
	}
	
	public static ReccuringCycleType getInstanceFromName(String name){
		for(ReccuringCycleType reccuringCycleType :ReccuringCycleType.values()){
			if(reccuringCycleType.getName().equals(name)){
				return reccuringCycleType;
			}
		}
		return null; 
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public boolean isCitrusSupported() {
		return isCitrusSupported;
	}
}
