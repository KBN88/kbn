package com.kbn.commons.util;

import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;

public class GoogleURLShortener {

	public Response getResponse (String longUrl){
		PropertiesManager propertyManager = new PropertiesManager();
		OAuthRequest oAuthRequest = new OAuthRequest(
				Verb.POST,propertyManager
				.getSystemProperty(CrmFieldConstants.GOOGLE_URL_SHORTENER.getValue()));

		oAuthRequest.addHeader("Content-Type", "application/json");
		String json = "{\"longUrl\": \"" + longUrl + "\"}";
		oAuthRequest.addPayload(json);
		return oAuthRequest.send();
		
		
	}
}
