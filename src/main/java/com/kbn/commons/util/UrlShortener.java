package com.kbn.commons.util;

import java.util.Map;
import com.google.common.reflect.TypeToken;
import com.google.gson.GsonBuilder;
import java.lang.reflect.Type;
import org.scribe.model.Response;

public class UrlShortener {

	public String shortenUrl(String longUrl) {

		Response response = new GoogleURLShortener().getResponse(longUrl);
		
		Type typeOfMap = new TypeToken<Map<String, String>>() {
			private static final long serialVersionUID = -5804690425921813742L;
		}.getType();
		Map<String, String> responseMap = new GsonBuilder().create().fromJson(
				response.getBody(), typeOfMap);
		return responseMap.get("id");

	}
}
