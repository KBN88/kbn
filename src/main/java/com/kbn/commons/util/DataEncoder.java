package com.kbn.commons.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.owasp.esapi.ESAPI;

import com.kbn.commons.user.CustomerAddress;
import com.kbn.commons.user.Invoice;
import com.kbn.commons.user.LoginHistory;
import com.kbn.commons.user.MerchantDetails;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.Remittance;
import com.kbn.commons.user.SearchUser;
import com.kbn.commons.user.Settlement;
import com.kbn.commons.user.TransactionSearch;
import com.kbn.commons.user.TransactionSnapshotReport;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.crm.actionBeans.BinRange;
import com.kbn.ticketing.commons.util.SubAdmin;

/**
 * @author Puneet, Neeraj
 *
 */
public class DataEncoder {

	public static String encodeString(String data) {
		return ESAPI.encoder().encodeForHTML(data);
	}

	public List<MerchantDetails> encodeMerchantDetailsObj(List<MerchantDetails> merchants) {
//TODO.. code commented for live deployment
		for (MerchantDetails merchant : merchants) {
			merchant.setBusinessName(encodeString(merchant.getBusinessName()));
			merchant.setPayId(encodeString(merchant.getPayId()));
		//	merchant.setResellerId(encodeString(merchant.getResellerId()));
			merchant.setEmailId(encodeString(merchant.getEmailId()));
			merchant.setMobile(encodeString(merchant.getMobile()));
		//	merchant.setUserType(encodeString(merchant.getUserType()));
		}
		return merchants;
	}

	public List<TransactionSummaryReport> encodeTransactionSummary(
			List<TransactionSummaryReport> transactions) {

		for (TransactionSummaryReport transaction : transactions) {
			transaction.setAcquirer(encodeString(transaction.getAcquirer()));
			transaction.setApprovedAmount(encodeString(transaction
					.getApprovedAmount()));
			transaction.setBusinessName(encodeString(transaction
					.getBusinessName()));
			transaction.setCaptureTxnId(encodeString(transaction
					.getCaptureTxnId()));
			transaction.setCardNo(encodeString(transaction.getCardNo()));
			transaction.setChargebackAmount(encodeString(transaction
					.getChargebackAmount()));
			transaction.setCurrencyCode(encodeString(transaction
					.getCurrencyCode()));
			transaction.setCustomerEmail(encodeString(transaction
					.getCustomerEmail()));
			transaction.setCustomerName(encodeString(transaction
					.getCustomerName()));
			transaction.setInternalRequestDesc(encodeString(transaction
					.getInternalRequestDesc()));
			transaction.setMopType(encodeString(transaction.getMopType()));
			transaction.setNetAmount(encodeString(transaction.getNetAmount()));
			transaction.setOid(encodeString(transaction.getOid()));
			transaction.setOrderId(encodeString(transaction.getOrderId()));
			transaction.setOrigTransactionId(encodeString(transaction
					.getOrigTransactionId()));
			transaction.setOrigTxnDate(encodeString(transaction
					.getOrigTxnDate()));
			transaction.setPayId(encodeString(transaction.getPayId()));
			transaction.setPaymentMethod(encodeString(transaction
					.getPaymentMethod()));
			transaction.setProductDesc(encodeString(transaction
					.getProductDesc()));
			transaction
					.setRefundDate(encodeString(transaction.getRefundDate()));
			transaction.setRefundedAmount(encodeString(transaction
					.getRefundedAmount()));
			transaction.setResponseMsg(encodeString(transaction
					.getResponseMsg()));
			transaction
					.setServiceTax(encodeString(transaction.getServiceTax()));
			transaction
					.setSettleDate(encodeString(transaction.getSettleDate()));
			transaction.setStatus(encodeString(transaction.getStatus()));
			transaction.setTdr(encodeString(transaction.getTdr()));
			transaction.setTransactionId(encodeString(transaction
					.getTransactionId()));
			transaction.setTxnDate(encodeString(transaction.getTxnDate()));
			transaction.setTxnType(encodeString(transaction.getTxnType()));
			transaction.setInternalCardIssusserBank(encodeString(transaction.getInternalCardIssusserBank()));
			transaction.setInternalCardIssusserCountry(encodeString(transaction.getInternalCardIssusserCountry()));
			transaction.setPgTxnMessage(encodeString(transaction.getPgTxnMessage()));
			transaction.setPayId(encodeString(transaction.getPayId()));
			transaction.setAggregatorName(encodeString(transaction.getAggregatorName()));
		}
		return transactions;
	}

	public List<TransactionSnapshotReport> encodeTransactionSnapShot(
			List<TransactionSnapshotReport> transactions) {

		for (TransactionSnapshotReport transaction : transactions) {
			transaction.setApprovedAmount(encodeString(transaction
					.getApprovedAmount()));
			transaction.setCurrency(encodeString(transaction.getCurrency()));
			transaction.setPaymentMethod(encodeString(transaction
					.getPaymentMethod()));
			transaction.setDate(encodeString(transaction.getDate()));
		}
		return transactions;
	}

	public List<TransactionSearch> encodeTransactionSearchObj(
			List<TransactionSearch> transactions) {

		for (TransactionSearch transaction : transactions) {
			transaction.setAmount(encodeString(transaction.getAmount()));
			transaction
					.setCardNumber(encodeString(transaction.getCardNumber()));
			transaction.setCurrency(encodeString(transaction.getCurrency()));
			transaction.setCustomerEmail(encodeString(transaction
					.getCustomerEmail()));
			transaction.setCustomerName(encodeString(transaction
					.getCustomerName()));
			transaction.setMerchants(encodeString(transaction.getMerchants()));
			transaction.setOrderId(encodeString(transaction.getOrderId()));
			transaction.setPayId(encodeString(transaction.getPayId()));
			transaction.setPaymentMethods(encodeString(transaction
					.getPaymentMethods()));
			transaction.setProductDesc(encodeString(transaction
					.getProductDesc()));
			transaction.setStatus(encodeString(transaction.getStatus()));
			transaction.setTxnType(encodeString(transaction.getTxnType()));
			transaction.setMopType(encodeString(transaction.getMopType()));
			transaction.setInternalCardIssusserBank(transaction.getInternalCardIssusserBank());
			transaction.setInternalCardIssusserCountry(transaction.getInternalCardIssusserCountry());

		}
		return transactions;
	}

	public List<Invoice> encodeInvoiceSearchObj(List<Invoice> invoices) {
		for (Invoice invoice : invoices) {
			invoice.setAddress(encodeString(invoice.getAddress()));
			invoice.setAmount(encodeString(invoice.getAmount()));
			invoice.setBusinessName(encodeString(invoice.getBusinessName()));
			invoice.setCountry(encodeString(invoice.getCountry()));
			invoice.setCurrencyCode(encodeString(invoice.getCurrencyCode()));
			invoice.setEmail(encodeString(invoice.getEmail()));
			invoice.setExpiresDay(encodeString(invoice.getExpiresDay()));
			invoice.setExpiresHour(encodeString(invoice.getExpiresDay()));
			invoice.setInvoiceId(encodeString(invoice.getInvoiceId()));
			invoice.setInvoiceNo(encodeString(invoice.getInvoiceNo()));
			invoice.setName(encodeString(invoice.getName()));
			invoice.setPayId(encodeString(invoice.getPayId()));
			invoice.setPhone(encodeString(invoice.getPhone()));
			invoice.setProductDesc(encodeString(invoice.getProductDesc()));
			invoice.setProductName(encodeString(invoice.getProductName()));
			invoice.setQuantity(encodeString(invoice.getQuantity()));
			invoice.setReturnUrl(encodeString(invoice.getReturnUrl()));
			invoice.setSaltKey(encodeString(invoice.getSaltKey()));
			invoice.setServiceCharge(encodeString(invoice.getServiceCharge()));
			invoice.setState(encodeString(invoice.getState()));
			invoice.setTotalAmount(encodeString(invoice.getTotalAmount()));
			invoice.setZip(encodeString(invoice.getZip()));
			invoice.setQuantity(encodeString(invoice.getQuantity()));
			invoice.setCity(encodeString(invoice.getCity()));
		}
		return invoices;
	}
	
	public List<Settlement> encodeSettlementSearchObj(List<Settlement> settlements) {
		for (Settlement settlement : settlements) {
			settlement.setTxnAmount(encodeString(settlement.getTxnAmount()));
			settlement.setCurrencyCode(encodeString(settlement.getCurrencyCode()));
			settlement.setCustomerEmail(encodeString(settlement.getCustomerEmail()));
			settlement.setMerchant(encodeString(settlement.getMerchant()));
			settlement.setMop(encodeString(settlement.getMop()));
			settlement.setNetAmount(encodeString(settlement.getNetAmount()));
			settlement.setOrderId(encodeString(settlement.getOrderId()));
			settlement.setPayId(encodeString(settlement.getPayId()));
			settlement.setPaymentMethod(encodeString(settlement.getPaymentMethod()));
			settlement.setServiceTax(encodeString(settlement.getServiceTax()));
			settlement.setStatus(encodeString(settlement.getStatus()));
			settlement.setTdr(encodeString(settlement.getTdr()));
			settlement.setTxnId(encodeString(settlement.getTxnId()));
		}
		return settlements;
	}

	public List<Remittance> encodeRemittanceObj(List<Remittance> remittances) {
		for (Remittance remittance : remittances) {
			remittance.setAccHolderName(encodeString(remittance
					.getAccHolderName()));
			remittance.setAccountNo(encodeString(remittance.getAccountNo()));
			remittance.setBankName(encodeString(remittance.getBankName()));
			remittance.setCurrency(encodeString(remittance.getCurrency()));
			remittance.setDateFrom(encodeString(remittance.getDateFrom()));
			remittance.setIfscCode(encodeString(remittance.getIfscCode()));
			remittance.setMerchant(encodeString(remittance.getMerchant()));
			remittance.setNetAmount(encodeString(remittance.getNetAmount()));
			remittance.setPayId(encodeString(remittance.getPayId()));
			remittance.setRemittanceDate(encodeString(remittance
					.getRemittanceDate()));
			remittance.setRemittedAmount(encodeString(remittance
					.getRemittedAmount()));
			remittance.setUtr(encodeString(remittance.getUtr()));
			remittance.setStatus(encodeString(remittance.getStatus()));
		}
		return remittances;
	}

	public List<LoginHistory> encodeLoginHistoryObj(List<LoginHistory> histories) {
		for (LoginHistory loginHistory : histories) {
			String loginDate = loginHistory.getTimeStamp();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			Date changedate = null;
			try {
				changedate = simpleDateFormat.parse(loginDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String formattedDate = sdf.format(changedate);
			
			loginHistory.setBrowser(encodeString(loginHistory.getBrowser()));
			loginHistory.setBusinessName(encodeString(loginHistory
					.getBusinessName()));
			loginHistory.setEmailId(encodeString(loginHistory.getEmailId()));
			loginHistory.setFailureReason(encodeString(loginHistory
					.getFailureReason()));
			loginHistory.setIp(encodeString(loginHistory.getIp()));
			loginHistory.setOs(encodeString(loginHistory.getOs()));
			loginHistory.setTimeStamp(formattedDate);
			loginHistory.setId(loginHistory.getId());
		}
		return histories;
	}

	public List<Merchants> encodeMerchantsObj(List<Merchants> merchants) {
		for (Merchants merchant : merchants) {
			merchant.setBusinessName(encodeString(merchant.getBusinessName()));
			merchant.setEmailId(encodeString(merchant.getEmailId()));
			merchant.setFirstName(encodeString(merchant.getFirstName()));
			merchant.setLastName(encodeString(merchant.getLastName()));
			merchant.setMobile(encodeString(merchant.getMobile()));
			merchant.setPayId(encodeString(merchant.getPayId()));
		}
		return merchants;
	}
	public List<SubAdmin> encodeAgentsObj(List<SubAdmin> agents) {
		for (SubAdmin subAdmin : agents) {
			
			subAdmin.setAgentEmailId(encodeString(subAdmin.getAgentEmailId()));
			subAdmin.setAgentFirstName(encodeString(subAdmin.getAgentFirstName()));
			subAdmin.setAgentLastName(encodeString(subAdmin.getAgentLastName()));
			subAdmin.setAgentMobile(encodeString(subAdmin.getAgentMobile()));
			subAdmin.setPayId(encodeString(subAdmin.getPayId()));
		}
		return agents;
	}
	public List<SearchUser> encodeSearchUserObj(List<SearchUser> users) {
		for (SearchUser searchUser : users) {
			searchUser.setEmailId(encodeString(searchUser.getEmailId()));
			searchUser.setFirstName(encodeString(searchUser.getFirstName()));
			searchUser.setLastName(encodeString(searchUser.getLastName()));
			searchUser.setPhone(encodeString(searchUser.getPhone()));
		}
		return users;
	}
public CustomerAddress encodeCustomerAddressObj(CustomerAddress customerAddress){
	
		customerAddress.setCustCity(encodeString(customerAddress.getCustCity()));
		customerAddress.setCustCountry(encodeString(customerAddress.getCustCountry()));
		customerAddress.setCustName(encodeString(customerAddress.getCustName()));
		customerAddress.setCustPhone(encodeString(customerAddress.getCustPhone()));
		customerAddress.setCustShipCity(encodeString(customerAddress.getCustShipCity()));
		customerAddress.setCustShipName(encodeString(customerAddress.getCustShipName()));
		customerAddress.setCustShipCountry(encodeString(customerAddress.getCustShipCountry()));
		customerAddress.setCustShipState(encodeString(customerAddress.getCustShipState()));
		customerAddress.setCustShipStreetAddress1(encodeString(customerAddress.getCustShipStreetAddress1()));
		customerAddress.setCustShipStreetAddress2(encodeString(customerAddress.getCustShipStreetAddress2()));
		customerAddress.setCustShipZip(encodeString(customerAddress.getCustShipZip()));
		customerAddress.setCustState(encodeString(customerAddress.getCustState()));
		customerAddress.setCustStreetAddress1(encodeString(customerAddress.getCustStreetAddress1()));
		customerAddress.setCustStreetAddress2(encodeString(customerAddress.getCustStreetAddress2()));
		customerAddress.setCustZip(encodeString(customerAddress.getCustZip()));
		
	
	return customerAddress;
	
}

public List<BinRange> encodeBinRange(List<BinRange> binRangDisplay) {
	for (BinRange binRange : binRangDisplay) {
		
		binRange.setBinCode(encodeString(binRange.getBinCode()));
		//binRange.setCardType(encodeString(binRange.getCardType().toString()));
		binRange.setGroupCode(encodeString(binRange.getGroupCode()));
		binRange.setIssuerBankName(encodeString(binRange.getIssuerBankName()));
		binRange.setIssuerCountry(encodeString(binRange.getIssuerCountry()));
		//binRange.setMopType(encodeString(binRange.getMopType()));
		binRange.setProductName(encodeString(binRange.getProductName()));
		//binRange.setRfu1(encodeString(binRange.getRfu1()));
	}
	return binRangDisplay;
}

}


