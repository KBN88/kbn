package com.kbn.commons.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class LocateCountryName {
	private static Logger logger = Logger.getLogger(LocateCountryName.class.getName());

	public String findCountryCode(String remoteAddr) throws org.json.simple.parser.ParseException {
		String countryNameCode = null;
		logger.info("Ip address:" + remoteAddr);
		try {
			HttpPost httpPost = new HttpPost(ConfigurationConstants.LOCATE_COUNTRY_NAME.getValue());
			List<NameValuePair> postData = new ArrayList<>();
			postData.add(new BasicNameValuePair("user-id", ConfigurationConstants.USER_ID.getValue()));
			postData.add(new BasicNameValuePair("api-key", ConfigurationConstants.APP_KEY.getValue()));
			postData.add(new BasicNameValuePair("ip", remoteAddr));
			httpPost.setEntity(new UrlEncodedFormEntity(postData));

			HttpResponse response = HttpClients.createDefault().execute(httpPost);
			String jsonStr = EntityUtils.toString(response.getEntity());
			JSONParser parser = new JSONParser();
			Object obj;

			obj = parser.parse(jsonStr);

			JSONObject json = (JSONObject) obj;
			logger.info("json Obeject:" + json);

			countryNameCode = json.get("country").toString();
			logger.info("Ip address:" + json.get("country").toString());
			logger.info("Country Name:" + countryNameCode);

		} catch (IOException | ParseException ex) {
			ex.printStackTrace();
		}

		return countryNameCode;
	}
}
