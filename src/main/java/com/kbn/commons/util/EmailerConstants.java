package com.kbn.commons.util;

public enum EmailerConstants {

	CONTACT_US_EMAIL ("noreplay@urspay.com"),
	GATEWAY ("Urspay"),
	COMPANY ("Urspay Payment Gateway"),
	WEBSITE ("www.urspay.com"),
	PHONE_NO ("+91 11 4141 6504");

	private final String value;

	public String getValue() {
		return value;
	}

	private EmailerConstants(String value){
		this.value = value;
	}
}
