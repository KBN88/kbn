package com.kbn.commons.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.NotificationDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.MerchantDetails;
import com.kbn.commons.user.Permissions;
import com.kbn.commons.user.PermissionsDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.crm.action.ServiceTaxEditAction;
import com.kbn.pg.core.NotificationDetail;

/**
 * @author Shaiwal
 *
 */

public class NotificationGenerator {

	private static Logger logger = Logger.getLogger(ServiceTaxEditAction.class.getName());
	
	public void UpdateRequestNotification(String subject ,String loginEmailId , String Message){
		
		try{
			
			UserDao userDao = new UserDao();
			String payId = new UserDao().getPayIdByEmailId(loginEmailId);
			User loginUser = new User();
			String notifierEmailId = null;
			if (payId != null){
				loginUser = new UserDao().findPayId(payId);
			}
			else{
				throw new SystemException(ErrorType.USER_NOT_FOUND, "User Not found");
			}
			
			// Get logged in users' parent User
			String parentPayId = loginUser.getParentPayId();
			User parentUser = userDao.findPayId(parentPayId);
			
			
			//Get permissions granted to parent user
			List<String> permissionsList = new ArrayList<String>();
			//permissionsList = new PermissionsDao().getAllPermissionsByEmailId(parentUser.getEmailId());
			
			permissionsList = new PermissionsDao().getAllPermissionsByEmailId(parentUser.getEmailId());
			
			// If parent user is the admin , raise notification for Admin
			if( parentUser.getUserType().toString().equalsIgnoreCase("ADMIN")){
				notifierEmailId = parentUser.getEmailId();
			}
			
			//If parent user has the permissions , send notification to that user
			else if (checkPermissionToApprove(permissionsList,subject)){
				notifierEmailId = parentUser.getEmailId();
			}
			
			//Else send notification to parent of parent user , i.e Admin
			else{
				User parentParentUser = userDao.findPayId(parentUser.getParentPayId());
				notifierEmailId = parentParentUser.getEmailId();
			}
			
				Date currentDate = new Date();
				NotificationDetail notificationDetail = new NotificationDetail();
				notificationDetail.setCreateDate(currentDate);
				notificationDetail.setSubmittedBy(loginEmailId);
				notificationDetail.setNotifierEmailId(notifierEmailId);
				notificationDetail.setStatus(NotificationStatusType.UNREAD.getName());
				notificationDetail.setMessage(Message);
				
				NotificationDao notificationDao = new NotificationDao();
				notificationDao.create(notificationDetail);
			
		}
		catch(Exception e){
			
		}
	}
	
public void UpdateApproveRejectNotificationForServiceTax(User user , String Message, String businessType , String requestedBy ,String result){
		
	
	if (result.equalsIgnoreCase("accept")){
		
		List<String> usersToBeNotified = new ArrayList<String>();
		usersToBeNotified =  new UserDao().getMerchantEmailIdListByBusinessType(businessType);
			
			if (user.getUserType().equals(UserType.ADMIN)){
				usersToBeNotified.add(requestedBy);
			}
		
			else if (user.getUserType().equals(UserType.SUBADMIN)){
				String parentAdminPayId = user.getParentPayId();
				String parentAdminEmailId = new UserDao().getEmailIdByPayId(parentAdminPayId);
				usersToBeNotified.add(parentAdminEmailId);
				usersToBeNotified.add(requestedBy);
			}
			else if (user.getUserType().equals(UserType.ASSOCIATE)){
				String parentSubAdminPayId = user.getParentPayId();
				User parentSubAdmin = new UserDao().findPayId(parentSubAdminPayId);
				String parentSubAdminEmailId = parentSubAdmin.getEmailId();
				
				User parentAdmin = new UserDao().findPayId(parentSubAdmin.getParentPayId());
				String parentAdminEmailId = parentAdmin.getEmailId();
				
				usersToBeNotified.add(parentSubAdminEmailId);
				usersToBeNotified.add(parentAdminEmailId);
				
			}
			
			for (String emailId : usersToBeNotified){
				
				Date currentDate = new Date();
				NotificationDetail notificationDetail = new NotificationDetail();
				notificationDetail.setCreateDate(currentDate);
				notificationDetail.setSubmittedBy(user.getEmailId());
				notificationDetail.setNotifierEmailId(requestedBy);
				notificationDetail.setStatus(NotificationStatusType.UNREAD.getName());
				notificationDetail.setMessage(Message);
				
				NotificationDao notificationDao = new NotificationDao();
				notificationDao.create(notificationDetail);
			}
	}
	
	else{
		
		Date currentDate = new Date();
		NotificationDetail notificationDetail = new NotificationDetail();
		notificationDetail.setCreateDate(currentDate);
		notificationDetail.setSubmittedBy(null);
		notificationDetail.setNotifierEmailId(user.getEmailId());
		notificationDetail.setStatus(NotificationStatusType.UNREAD.getName());
		notificationDetail.setMessage(Message);
		
		NotificationDao notificationDao = new NotificationDao();
		notificationDao.create(notificationDetail);
	}
	}

public boolean checkPermissionToApprove(List<String> permissionsList , String subject) {
	
	boolean isAllowed =false ;
	
	for (String permission : permissionsList){
		if (permission.equals(subject)){
			isAllowed = true;
		}
	}
	
	return isAllowed;
}
}
