package com.kbn.commons.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.Remittance;
import com.kbn.commons.user.RemittanceDao;
import com.kbn.commons.user.UserDao;
import com.kbn.crm.commons.action.AbstractSecureAction;

import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

public class RemittanceUploadFile extends AbstractSecureAction {
	
/**
	 * @ Neeraj
	 */
	private static final long serialVersionUID = -7229160821072604017L;
	private String fileName;
    private List<Merchants> merchantList = new ArrayList<Merchants>();
    private Map<String, String> currencyMap = new HashMap<String, String>();   
   @SuppressWarnings("unchecked")
   public String uploadFile() throws IOException, SystemException{
	  RemittanceDao remittanceDao = new RemittanceDao();
			  BufferedReader bufferedReader = null;
			  String line = "";
			  String cvsSplitBy = ",";
			  try {
			  setMerchantList(new UserDao().getActiveMerchantList());
			  bufferedReader = new BufferedReader(new FileReader(fileName));
			   while ((line = bufferedReader.readLine()) != null) {
				   ColumnPositionMappingStrategy<Remittance> mappingStrategy 
                   = new ColumnPositionMappingStrategy<Remittance>();
                     mappingStrategy.setType(Remittance.class);
                     String[] columns = new String[] {"payId","merchant","bankName","accHolderName","accountNo","ifscCode","currency","dateFrom","netAmount","remittedAmount","utr","remittanceDate","status"};
                     mappingStrategy.setColumnMapping(columns);
                     CsvToBean<Remittance> csv = new CsvToBean<Remittance>();
                     List<Remittance> remittanceList = csv.parse(mappingStrategy, bufferedReader);
                     
                     for (int i = 0; i < remittanceList.size(); i++) 
                     {try
                     {
                    	  Remittance remittance = remittanceList.get(i);
                  	      validateRemittance(remittance);
                  	      remittanceDao.create(remittance);
                     }catch(SystemException e){
                    	 addActionMessage("Invaild row"+i);
                    	 e.printStackTrace();
                     }
                    	
                     }
                     addActionMessage(CrmFieldConstants.DETAILS_SAVED_SUCCESSFULLY.getValue());
			   }
			  } catch (FileNotFoundException e) {
			   e.printStackTrace();
			  } catch (IOException e) {
			   e.printStackTrace();
			  }
	return INPUT;
 }

private void validateRemittance(Remittance remittance)throws SystemException{
	CrmValidator validator = new CrmValidator();

		if ((validator.validateBlankField(remittance.getAccHolderName()))){
			
		} else if (!(validator.validateField(CrmFieldType.ACC_HOLDER_NAME,remittance.getAccHolderName()))) {
			throw new SystemException(ErrorType.ACC_HOLDER_NAME,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getAccountNo()))){
		} else if (!(validator.validateField(CrmFieldType.ACCOUNT_NO,remittance.getAccountNo()))) {
			throw new SystemException(ErrorType.ACCOUNT_NO,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getBankName()))){
		} else if (!(validator.validateField(CrmFieldType.BANK_NAME,remittance.getBankName()))) {
			throw new SystemException(ErrorType.BANK_NAME,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getCurrency()))){
		} else if (!(validator.validateField(CrmFieldType.CURRENCY,remittance.getCurrency()))) {
			throw new SystemException(ErrorType.CURRENCY,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getDateFrom()))){
		} else if (!(validator.validateField(CrmFieldType.DATE_FROM,remittance.getDateFrom()))) {
			throw new SystemException(ErrorType.DATE_FROM,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getIfscCode()))){
		} else if (!(validator.validateField(CrmFieldType.IFSC_CODE,remittance.getIfscCode()))) {
			throw new SystemException(ErrorType.IFSC_CODE,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getMerchant()))){
		} else if (!(validator.validateField(CrmFieldType.MERCHANT,remittance.getMerchant()))) {
			throw new SystemException(ErrorType.MERCHANT,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getNetAmount()))){
		} else if (!(validator.validateField(CrmFieldType.NET_AMOUNT,remittance.getNetAmount()))) {
			throw new SystemException(ErrorType.NET_AMOUNT,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getPayId()))){
		} else if (!(validator.validateField(CrmFieldType.PAY_ID,remittance.getPayId()))) {
			throw new SystemException(ErrorType.PAY_ID,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getRemittanceDate()))){
		} else if (!(validator.validateField(CrmFieldType.DATE_FROM,remittance.getRemittanceDate()))) {
			throw new SystemException(ErrorType.DATE_FROM,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getStatus()))){
		} else if (!(validator.validateField(CrmFieldType.STATUS,remittance.getStatus()))) {
			throw new SystemException(ErrorType.STATUS,"" + " is invalid field");
		}
		if ((validator.validateBlankField(remittance.getUtr()))){
		} else if (!(validator.validateField(CrmFieldType.UTR,remittance.getUtr()))) {
			throw new SystemException(ErrorType.UTR,"" + " is invalid field");
		}
	  
	}


	

	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public List<Merchants> getMerchantList() {
		return merchantList;
	}
	public void setMerchantList(List<Merchants> merchantList) {
		this.merchantList = merchantList;
	}
	public Map<String, String> getCurrencyMap() {
		return currencyMap;
	}
	public void setCurrencyMap(Map<String, String> currencyMap) {
		this.currencyMap = currencyMap;
	}

	
}

