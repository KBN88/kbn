package com.kbn.commons.util;

import java.net.URI;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.http.client.fluent.Async;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.concurrent.FutureCallback;
import org.apache.log4j.Logger;

import com.kbn.commons.util.threadpool.ThreadPoolProvider;
import com.kbn.pg.recurringPayments.MerchantWebhookInvoker;

/**
 * @author Puneet
 *
 */
public class AsyncPostCreater {
	private static Logger logger = Logger.getLogger(MerchantWebhookInvoker.class.getName());

	public void sendPost(Fields fields, String requestUrl) {
		sendPost(fields.getFields(), requestUrl);
	}

	public void sendPost(Map<String, String> requestMap, String requestUrl) {
		try {
			Async async = Async.newInstance().use(ThreadPoolProvider.getExecutorService());
			URIBuilder builder;

			builder = new URIBuilder(requestUrl);
			for (Entry<String, String> entry : requestMap.entrySet()) {
				builder.addParameter(entry.getKey(), entry.getValue());
			}

			URI requestURL = null;
			requestURL = builder.build();
			final Request request = Request.Post(requestURL);
			request.socketTimeout(3000);
			async.execute(request, new FutureCallback<Content>() {
				public void failed(final Exception exception) {
					logger.error("Error sending async post: " + exception);
				}

				public void completed(final Content content) {
					logger.info("Async request completed: " + request);
				}

				public void cancelled() {
				}
			});
		} catch (Exception exception) {
			logger.error("Error while preparing async post: " + exception);
		}
	}
}
