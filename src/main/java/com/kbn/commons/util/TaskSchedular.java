package com.kbn.commons.util;

import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.kbn.citruspay.CitrusPayStatusEnquirer;
import com.kbn.netbanking.kotak.KotakStatusEnquirer;
import com.kbn.netbanking.yesbank.YesBankStatusEnquirer;

public class TaskSchedular extends TimerTask {
	private static Logger logger = Logger.getLogger(TaskSchedular.class
			.getName());

	CitrusPayStatusEnquirer citrusStatusEnquirer = new CitrusPayStatusEnquirer();
	KotakStatusEnquirer kotakStatusEnquirer = new KotakStatusEnquirer();
	YesBankStatusEnquirer yesBankStatusEnquirer = new YesBankStatusEnquirer();

	@Override
	public void run() {
		logger.info("Status Enquiry Started.");
		citrusStatusEnquirer.citrusStatusEnquirer();
	//	kotakStatusEnquirer.statusEnquirer();
	//	yesBankStatusEnquirer.statusEnquirer();
	}
}
