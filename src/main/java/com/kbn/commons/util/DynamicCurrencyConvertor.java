package com.kbn.commons.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.kbn.pg.core.Amount;

public class DynamicCurrencyConvertor {

	private static Logger logger = Logger.getLogger(DynamicCurrencyConvertor.class.getName());

	public String findDynamicCurrencyExchange(String currencyCode, Fields fields) {
		String currencyChangeRate = null;
		String currencyChangeRate1 = null;
		try {

			HttpPost httpPost = new HttpPost(ConfigurationConstants.DYNAMICCURRENCYCONVERTOR.getValue());
			List<NameValuePair> postData = new ArrayList<>();
			postData.add(new BasicNameValuePair("user-id", ConfigurationConstants.USER_ID.getValue()));
			postData.add(new BasicNameValuePair("api-key", ConfigurationConstants.APP_KEY.getValue()));
			postData.add(new BasicNameValuePair("from-value", Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
					fields.get(FieldType.CURRENCY_CODE.getName()))));
			postData.add(new BasicNameValuePair("from-type", currencyCode));
			postData.add(new BasicNameValuePair("to-type", "INR"));
			httpPost.setEntity(new UrlEncodedFormEntity(postData));

			HttpResponse response = HttpClients.createDefault().execute(httpPost);
			String jsonStr = EntityUtils.toString(response.getEntity());
			JSONParser parser = new JSONParser();
			Object obj;

			obj = parser.parse(jsonStr);

			JSONObject json = (JSONObject) obj;
			logger.info("json Obeject:" + json);

			currencyChangeRate = json.get("result-float").toString();
			currencyChangeRate1 = Amount.formatAmount(currencyChangeRate,
					fields.get(FieldType.CURRENCY_CODE.getName()));

			logger.info("Current Currency Value Amount:" + json.get("result-float").toString());
			logger.info("Current Currency Value Amount:" + currencyChangeRate);

		} catch (Exception exception) {

		}
		return currencyChangeRate1;

	}

}
