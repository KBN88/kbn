package com.kbn.commons.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.kbn.crm.actionBeans.BinRange;

public class DataBaseBinRangeApi {
	// Using your database BinRange 
	public void getBinDb(Fields fields, BinRange binRangeAPI) throws ParseException {
		Gson gson = new Gson();
		String jsonInString = gson.toJson(binRangeAPI);
		JSONParser jsonParser = new JSONParser();
		Object jsonObject;
		jsonObject = jsonParser.parse(jsonInString);
			JSONObject jObject = (JSONObject) jsonObject;
			Object cardScheme = jObject.get("mopType");
			cardScheme = BinMapperType.getCodeUsingName(cardScheme.toString().toUpperCase());
			if (null != cardScheme) {
				fields.put(FieldType.MOP_TYPE.getName(), (String) cardScheme);
			}
			Object paymentType = jObject.get("cardType");
			paymentType = PaymentType.getInstanceUsingStringValue(paymentType.toString()).getCode();
			if (null != paymentType) {
				fields.put(FieldType.PAYMENT_TYPE.getName(), (String) paymentType);
			}
			Object issuerBankName = jObject.get("issuerBankName");
			if (null != issuerBankName) {
				fields.put(FieldType.INTERNAL_CARD_ISSUER_BANK.getName(), (String) issuerBankName);
			}
			Object cardCountry = jObject.get("issuerCountry");
			if (null != cardCountry) {
				fields.put(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName(), (String) cardCountry);
			}
	}

	
	}

