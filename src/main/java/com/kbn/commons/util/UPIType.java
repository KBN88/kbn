package com.kbn.commons.util;


import java.util.ArrayList;
import java.util.List;

public enum UPIType {
	
	SAFEXPAY_UPI ("ICICI UPI", "500","ICICI UPI","60","","","",""),
	AXIS_BANK_UPI("AXIS BANK UPI","502","","","AXIS BANK UPI","UP","",""),
	GPAY("GPAY","503","","","","","GPAY","UP");
	
	private final String name;
	private final String code;
	private final String SafexPayname;
	private final String SafexPaycode;
	private final String AxisBankname;
	private final String Axiscode;
	private final String gpayName;
	private final String gpayCode;

	private UPIType(String name, String code, String SafexPayname, String SafexPaycode, String AxisBankname,
			String Axiscode, String gpayName, String gpayCode) {
		this.name = name;
		this.code = code;
		this.SafexPayname = SafexPayname;
		this.SafexPaycode = SafexPaycode;
		this.AxisBankname = AxisBankname;
		this.Axiscode = Axiscode;
		this.gpayName = gpayName;
		this.gpayCode = gpayCode;

	}

	public static List<UPIType> getSAFEXPAYUPIMops() {
		return getGetMopsFromSystemProp("SAFEXPAYUPI");
	}

	public static List<UPIType> getAXISUPIMops() {
		return getGetMopsFromSystemProp("AXISUPI");
	}

	public static List<UPIType> getGPAYUPIMops() {
		return getGetMopsFromSystemProp("GPAYUPI");
	}

	public static List<UPIType> getGetMopsFromSystemProp(String mopsList) {
		PropertiesManager propertiesManager = new PropertiesManager();

		List<String> mopStringList = (List<String>) Helper.parseFields(propertiesManager.getAcquirerMopType(mopsList));

		List<UPIType> mops = new ArrayList<UPIType>();

		for (String mopCode : mopStringList) {
			UPIType mop = getmop(mopCode);
			mops.add(mop);
		}
		return mops;
	}

	public static UPIType getmop(String mopCode) {
		UPIType mopObj = null;
		if (null != mopCode) {
			for (UPIType mop : UPIType.values()) {
				if (mopCode.equals(mop.getCode().toString())) {
					mopObj = mop;
					break;
				}
			}
		}
		return mopObj;
	}

	public static String getmopName(String mopCode) {
		UPIType upiType = UPIType.getmop(mopCode);
		if (upiType == null) {
			return "";
		}
		return upiType.getName();
	}

	public static String getSafexPaycode(String mopCode) {
		UPIType upiType = UPIType.getmop(mopCode);
		if (upiType == null) {
			return "";
		}
		return upiType.getSafexPaycode();
	}

	public static String getAxiscode(String mopCode) {
		UPIType upiType = UPIType.getmop(mopCode);
		if (upiType == null) {
			return "";
		}
		return upiType.getAxiscode();
	}

	// GPAY
	public static String getGpayCode(String mopCode) {
		UPIType upiType = UPIType.getmop(mopCode);
		if (upiType == null) {
			return "";
		}
		return upiType.getGpayCode();
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public String getSafexPayname() {
		return SafexPayname;
	}

	public String getSafexPaycode() {
		return SafexPaycode;
	}

	public String getAxisBankname() {
		return AxisBankname;
	}

	public String getAxiscode() {
		return Axiscode;
	}

	public String getGpayName() {
		return gpayName;
	}

	public String getGpayCode() {
		return gpayCode;
	}

}
