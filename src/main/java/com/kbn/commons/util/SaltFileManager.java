package com.kbn.commons.util;


public class SaltFileManager extends PropertiesManager{

	public boolean insertSalt(String payId,String salt){
		PropertiesManager propertiesManager = new PropertiesManager();
		
		String saltPropertiesFile = propertiesManager.getSystemProperty(Constants.SALT_FILE_PATH_NAME.getValue()); 
		
		return setProperty(payId,salt,saltPropertiesFile);		
	}	
}
