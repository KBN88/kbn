package com.kbn.commons.util;

import org.apache.log4j.Logger;

import com.kbn.citruspay.ResponseFactory;
import com.kbn.commons.dao.BinRangeDao;
import com.kbn.crm.actionBeans.BinRange;

/**
 * @author Neeraj
 *
 */

public class BinRangeProvider {
	private static Logger logger = Logger.getLogger(ResponseFactory.class.getName());

	public void findBinRange(Fields fields) {
		try {
			String cardNumber = fields.get(FieldType.CARD_NUMBER.getName());
			String cardBin = cardNumber.replace(" ", "").replace(",", "").substring(0, 6);
			BinRangeDao binRangeDao = new BinRangeDao();
			BinRange binRange = binRangeDao.findBinCode(cardBin);
			//use dataBase binRange 
			if (binRange != null) {
				DataBaseBinRangeApi binRangeAPI = new DataBaseBinRangeApi();
				binRangeAPI.getBinDb(fields, binRange);
			}//Use default BinRange API 
			else {
				DefaultBinRangeAPI api = new DefaultBinRangeAPI();
				api.getdefualBinrage(fields, cardBin);
			}
		} catch (Exception exception) {
			logger.error("Unable to Process Bin API Request", exception);
		}
	}

}
