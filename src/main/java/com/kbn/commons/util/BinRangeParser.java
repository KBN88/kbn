package com.kbn.commons.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class BinRangeParser {

	public JSONObject getBinParser(StringBuilder response) throws ParseException {
		Object obj;
		JSONParser parser = new JSONParser();
		obj = parser.parse(response.toString());
		JSONObject jObject = (JSONObject) obj;
		return jObject;
	}

}
