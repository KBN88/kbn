package com.kbn.commons.util;

public enum FieldFormatType {
	NUMBER,
	ALPHA,
	ALPHASPACE,
	ALPHANUM,
	SPECIAL,
	ALPHASPACENUM,
	EMAIL,
	DATE,
	URL,
	AMOUNT,
	NONE,
	REGEX
}
