package com.kbn.commons.util;


import java.util.ArrayList;
import java.util.List;

public enum WalletType {
	
	//Bill Desk
	PAYTM_WALLET ("PAYTM WALLET", "101","PAYTM WALLET","PT2", "", "", "", ""),
	MOBIKWIK_WALLET ("MOBIKWIK WALLET", "102","MOBIKWIK WALLET","MBK", "", "", "Mobikwik", "3"),
	AIRTEL_MONEY_WALLET ("AIRTEL MONEY WALLET", "103","AIRTEL MONEY WALLET","ATL", "", "", "", ""),
	VODAFONE_MPESA_WALLET ("VODAFONE MPESA WALLET", "104","VODAFONE MPESA WALLET","VM3", "", "", "M-Pesa", "67"),
	MONEY_ON_MOBILE_WALLET ("MONEY ON MOBILE WALLET", "105","MONEY ON MOBILE WALLET","MOM", "", "", "", ""),
	RELIANCE_JIO_WALLET ("RELIANCE JIO WALLET", "106","RELIANCE JIO WALLET","RJM", "", "", "", ""),
	OLA_MONEY_WALLET ("OLA MONEY WALLET", "107","OLA MONEY WALLET","OLA", "", "", "Ola Money", "46"),
	THE_MOBILE_WALLET ("THE MOBILE WALLET", "108","THE MOBILE WALLET","TMW", "", "", "The Mobile Wallet", "62"),
	PNB_WALLET ("PUNJAB NATIONAL BANK WALLET", "109","PUNJAB NATIONAL BANK WALLET","PNW", "", "", "", ""),
	YES_BANK_WALLET ("YES BANK WALLET", "110","YES BANK WALLET","YBW", "", "", "", ""),
	DCB_CIPPY_WALLET ("DCB CIPPY WALLET", "111","DCB CIPPY WALLET","DCW", "", "", "", ""),
	ADITYA_BIRLA_WALLET ("ADITYA BIRLA WALLET", "112","ADITYA BIRLA WALLET","ABW", "", "", "", ""),
	FREECHARGE_WALLET ("FREECHARGE WALLET", "113","FREECHARGE WALLET","FRW", "", "", "FreeCharge", "59"),
	EBIX_CASH_WALLET ("EBIX CASH WALLET", "114","EBIX CASH WALLET","ITZ", "", "", "", ""),
	PHONEPE_WALLET ("PHONEPE WALLET", "115","PHONEPE WALLET","PHW", "", "", "", ""),
	OXIGEN_WALLET ("OXIGEN WALLET", "116","OXIGEN WALLET","OXY", "", "", "Oxigen Wallet", "7"),
	PAYCASH_WALLET ("PAYCASH WALLET", "117","PAYCASH WALLET","PCH", "", "", "", ""),
	ICC_CASHCARD_WALLET ("ICC CASHCARD WALLET", "118","ICC CASHCARD WALLET","ICC", "", "", "", ""),
	// SafexPay Wallet
	EZE_CLICK_WALLET ("EZECLICK", "120","ICC CASHCARD WALLET","ICC", "", "", "ezeClick", "57"),
	JIO_MONEY_WALLET ("JIO MONEY", "121","ICC CASHCARD WALLET","ICC", "", "", "Jio Money", "61"),
	PAYZAPP_WALLET ("PayZapp", "122","ICC CASHCARD WALLET","ICC", "", "", "PayZapp", "64"),
	SBIBUDDY_WALLET ("SBI Buddy", "123","ICC CASHCARD WALLET","ICC", "", "", "SBI Buddy", "68"),
	
	//Epay Later
	EPAYLATER_WALLET ("EPAYLATER WALLET", "119","","", "EPAYLATER WALLET", "EPWL", "", ""),
	
	// Demo Wallet Code
	DEMO_WALLET ("DEMO Wallet", "100","", "", "", "", "", "");
	
	private final String name;
	private final String code;
	private final String billDeskName;
	private final String billDeskCode;
	private final String epayLaterName;
	private final String epayLaterCode;
	private final String safexPayName;
	private final String safexPayCode;

	private WalletType(String name, String code, String billDeskName, String billDeskCode, String epayLaterName,
			String epayLaterCode, String safexPayName, String safexPayCode) {
		this.name = name;
		this.code = code;

		this.billDeskName = billDeskName;
		this.billDeskCode = billDeskCode;
		this.epayLaterName = epayLaterName;
		this.epayLaterCode = epayLaterCode;
		this.safexPayName = safexPayName;
		this.safexPayCode = safexPayCode;

	}

	public static List<WalletType> getBILLDESKWLMops() {
		return getGetMopsFromSystemProp("BILLDESKWL");
	}

	public static List<WalletType> getSAFEXPAYWLMops() {
		return getGetMopsFromSystemProp("SAFEXPAYWL");
	}

	public static List<WalletType> getEPAYLATERWLMops() {
		return getGetMopsFromSystemProp("EPAYLATERWL");
	}

	public static List<WalletType> getAIRTELWLMops() {
		return getGetMopsFromSystemProp("AIRTELWL");
	}

	public static List<WalletType> getGetMopsFromSystemProp(String mopsList) {
		PropertiesManager propertiesManager = new PropertiesManager();

		List<String> mopStringList = (List<String>) Helper.parseFields(propertiesManager.getAcquirerMopType(mopsList));

		List<WalletType> mops = new ArrayList<WalletType>();

		for (String mopCode : mopStringList) {
			WalletType mop = getmop(mopCode);
			mops.add(mop);
		}
		return mops;
	}

	public static WalletType getmop(String mopCode) {
		WalletType mopObj = null;
		if (null != mopCode) {
			for (WalletType mop : WalletType.values()) {
				if (mopCode.equals(mop.getCode().toString())) {
					mopObj = mop;
					break;
				}
			}
		}
		return mopObj;
	}

	public static String getmopName(String mopCode) {
		WalletType walletType = WalletType.getmop(mopCode);
		if (walletType == null) {
			return "";
		}
		return walletType.getName();
	}

	public static String getBillDeskCode(String mopCode) {
		WalletType walletType = WalletType.getmop(mopCode);
		if (walletType == null) {
			return "";
		}
		return walletType.getBillDeskCode();
	}

	// SafexPay Wallet Code
	public static String getSafexPayCode(String mopCode) {
		WalletType walletType = WalletType.getmop(mopCode);
		if (walletType == null) {
			return "";
		}
		return walletType.getSafexPayCode();
	}

	public static String getEpayLaterCode(String mopCode) {
		WalletType walletType = WalletType.getmop(mopCode);
		if (walletType == null) {
			return "";
		}
		return walletType.getEpayLaterCode();
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public String getBillDeskName() {
		return billDeskName;
	}

	public String getBillDeskCode() {
		return billDeskCode;
	}

	public String getEpayLaterName() {
		return epayLaterName;
	}

	public String getEpayLaterCode() {
		return epayLaterCode;
	}

	public String getSafexPayName() {
		return safexPayName;
	}

	public String getSafexPayCode() {
		return safexPayCode;
	}

}
