package com.kbn.commons.util;

public enum BulkReportsType {

	SUMMARY_REPORT 			("Summary Report"), 
	SETTLEMENT_REPORT 		("Settlement Report"),
	TDR_REPORT 				("TDR Report");

	private final String name;

	private BulkReportsType(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
