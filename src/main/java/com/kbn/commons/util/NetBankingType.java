package com.kbn.commons.util;

import java.util.ArrayList;
import java.util.List;

public enum NetBankingType {
	
	       //DIRECPAY & CITRUSPAY
			ALLAHABAD_BANK ("Allahabad Bank", "1000","","","Allahabad Bank","ALB","Allahabad Bank","1056","","","Allahabad Bank","129","","","Allahabad Bank - Retail","1056"),
			AXIS_BANK ("Axis Bank", "1005","AXIS Bank","CID002","AXIS BANK","UTI","Axis Bank","1003","","","Axis Bank","49","","","Axis Bank","1003"),
			BANK_OF_BAHRAIN_AND_KUWAIT ("Bank Of Bahrain And Kuwait", "1043","Bank of Bahrain & Kuwait"," CID017","Bank Of Bahrain And Kuwait","BBK","","","","","Bank of Bahrain and Kuwait","27","","","",""),
			BANK_OF_INDIA ("Bank Of India", "1009","Bank of India","CID019","","","","","","","Bank Of India","134","","","Bank of India","1012"),
			BANK_OF_MAHARASHTRA ("Bank Of Maharashtra", "1064","Bank of Maharashtra","CID021","Bank Of Maharashtra","BOM","Bank of Maharashtra","1033","","","Bank of Maharashtra","41","","","Bank of Maharashtra","1033"),
			CANARA_BANK ("Canara Bank", "1055","Canara Bank","CID051","Canara Bank","CNB","Canara Bank","1030","","","Canara Bank","36","","","Canara Bank NetBanking","1030"), 
			CENTRAL_BANK_OF_INDIA ("Central Bank Of India", "1063","Central Bank of India","CID023","Central Bank of India","CBI","Central Bank of India","1028","","","Central Bank of India","40","","","Central Bank of India","1028"),
			CITIBANK ("CitiBank", "1010","Citibank","CID003","","","","","","","Citibank","14","","","",""),
			CITY_UNION_BANK ("City Union Bank", "1060","City Union Bank","CID024","","","","","","","City Union Bank","37","","","",""),
			CORPORATION_BANK ("Coporation Bank", "1034","Corporation Bank","CID025","Corporation Bank","CRP","Corporation Bank","1004","","","Corporation Bank","21","","","Corporation Bank","1004"),
			DEUTSCHE_BANK ("Deutsche Bank", "1026","Deutsche Bank","CID006","Deutsche Bank","DBK","Deutsche Bank","1024","","","Deutsche Bank","17","","","Deutsche Bank","1024"),
			DEVELOPMENT_CREDIT_BANK ("Development Credit Bank", "1040","Development Credit Bank","CID026","","","","","","","Development Credit Bank","25","","","",""),
			FEDERAL_BANK ("FEDERAL BANK", "1027","Federal Bank","CID009","Federal Bank","FBK","Federal Bank","1019","","","Federal Bank","18","","","Fedral Bank","1019"), 
			HDFC_BANK ("Hdfc Bank", "1004","HDFC Bank","CID010","HDFC BANK","HDF","HDFC Bank","1006","","","HDFC Bank","6","","","HDFC Bank","1006"),
			ICICI_BANK ("Icici Bank", "1013","ICICI Bank","CID001","ICICI BANK","ICI","ICICI Bank","1002","","","ICICI Bank","5","","","",""),
			INDIAN_BANK ("INDIAN BANK", "1069","Indian Bank","CID008","INDIAN BANK","INB","Indian Bank","1026","","","Indian Bank","44","","","Indian Ban","1026"),
			INDIAN_OVERSEAS_BANK ("Indian Overseas Bank", "1049","Indian Overseas Bank","CID027","Indian Overseas Bank","IOB","Indian Overseas Bank","1029","","","Indian Overseas Bank","32","","","Indian Overseas Bank","1029"),
			INDUSIND_BANK ("IndusInd Bank", "1054","IndusInd Bank","CID028","IndusInd Bank","IDS","IndusInd Bank","1015","","","IndusInd Bank","35","","","Indusind Bank","1015"),
			INDUSTRIAL_DEVELOPMENT_BANK_OF_INDIA ("Industrial Development Bank Of India", "1003","IDBI Bank","CID011","Industrial Development Bank Of India","IDB","IDBI Bank","1007","","","Industrial Development Bank of India","10","","","IDBI Bank","1007"),
			ING_VYSYA_BANK ("IngVysya Bank", "1062","ING Vysya Bank","CID029","","","","","","","","","","","",""),
			JAMMU_AND_KASHMIR_BANK ("Jammu And Kashmir Bank", "1041","Jammu & Kashmir Bank","CID030","Jammu & Kashmir Bank","JKB","Jammu and Kashmir Bank","1001","","","Jammu and Kashmir Bank","48","","","Jammu and Kashmir Bank","1001"),
			KARNATAKA_BANK_LTD ("Karnatka Bank Ltd", "1032","Karnataka Bank","CID031","Karnataka Bank Ltd","KBL","Karnataka Bank","1008","","","Karnataka Bank Ltd","20","","","Karnataka Bank","1008"),
			KARUR_VYSYA_BANK ("KarurVysya Bank", "1048","KarurVysya Bank","CID032","KarurVysya Bank","KVB","Karur Vysya Bank","1018","","","Karur Vysya Bank","31","","","Karur Vysya Bank","1018"),
			KOTAK_BANK ("Kotak Bank", "1012","Kotak Mahindra Bank","CID033","KOTAK BANK","162","KOTAK Bank","1013","","","Kotak Bank","45","","","Kotak Mahindra Bank","1013"),
			ORIENTAL_BANK_OF_COMMERCE ("Oriental Bank Of Commerce", "1042","Oriental Bank of Commerce","CID035","Oriental Bank of Commerce","OBC","Oriental Bank of Commerce","1035","","","Oriental Bank of Commerce","58","","","ORIENTAL BANK OF COMMERCE","1035"),
			RATNAKAR_BANK ("Ratnakar Bank", "1053","Ratnakar Bank","CID064","Ratnakar Bank","RBL","Ratnakar Bank","1066","","","RBL Bank","34","","","RBL Bank","1066"),
			SOUTH_INDIAN_BANK ("South Indian Bank", "1045","South Indian Bank","CID037","South Indian Bank","SIB","","","","","South Indian Bank","29","","","",""),
			STATE_BANK_OF_BIKANER_AND_JAIPUR ("State Bank Of Bikaner And Jaipur", "1050","State Bank of Bikaner and Jaipur","CID013","","","","","","","State Bank of Bikaner and Jaipur","33","","","",""),
			STATE_BANK_OF_HYDERABAD("StateBank Of Hyderabad", "1039","State Bank of Hyderabad","CID012","","","","","","","State Bank of Hyderabad","24","","","",""), 
			STATE_BANK_OF_INDIA ("State Bank Of India", "1030","SBI Bank","CID005","STATE BANK OF INDIA","SBI","STATE BANK OF INDIA","1014","","","State Bank of India","19","","","State Bank of India","1014"), 
			STATE_BANK_OF_MYSORE ("State Bank Of Mysore", "1037","State Bank of Mysore","CID014","","","","","","","State Bank of Mysore","22","","","",""),
			STATE_BANK_OF_PATIALA ("State Bank Of Patiala", "1068","State Bank Of Patiala","CID043","","","","","","","State Bank of Patiala","43","","","",""), 
			STATE_BANK_OF_TRAVANCORE ("State Bank Of Travancore", "1061","State Bank of Travancore","CID015","","","","","","","State Bank of Travancore","38","","","",""),
			TAMILNAD_MERCANTILE_BANK ("Tamilnad Mercantile Bank", "1065","Tamilnad Mercantile Bank","CID040","Tamilnad Mercantile Bank","TMB","Tamilnad Mercantile Bank","1044","","","Tamilnad Mercantile Bank","42","","","",""),
			UNION_BANK_OF_INDIA ("Union Bank Of India", "1038","Union Bank","CID007","Union Bank of India","UBI","Union Bank of India","1016","","","","","","","Union Bank","1016"),
			UNITED_BANK_OF_INDIA ("United Bank Of India", "1046","United Bank of India","CID041","United Bank of India","UNI","United Bank of India","1041","","","United Bank of India","30","","","United Bank of India","1041"),
			VIJAYA_BANK ("Vijay Bank", "1044","Vijaya Bank","CID042","Vijaya Bank","VJB","Vijaya Bank","1039","","","Vijaya bank","28","","","Vijaya Bank","1039"),
			YES_BANK ("Yes Bank", "1001","YES Bank","CID004","Yes Bank Ltd","YBK","Yes Bank","1005","","","Yes Bank","47","","","Yes Bank","1005"),
			ANDHRA_CORPORATE_BANK ("Andhra Corporate Bank", "1125","","","Andhra Corporate Bank","ADC","","","","","","","","","",""),
			DHANALAXMI_CORPORATE_BANK ("DHANALAXMI BANK CORPORATE", "1126","","","DHANALAXMI BANK CORPORATE","DL2","","","","","","","","","",""),
			ESAF_SMALL_FINANCE_BANK ("ESAF SMALL FINANCE BANK", "1127","","","ESAF SMALL FINANCE BANK","ESF","","","","","","","","","",""),
			LAXMI_VILAS_BANK_CORPORATE ("Laxmi Vilas Bank - Corporate Net Banking", "1128","","","Laxmi Vilas Bank - Corporate Net Banking","LVC","","","","","","","","","",""),
			PUNJAB_NATIONAL_BANK ("Punjab National Bank", "1129","","","Punjab National Bank","PNB","Punjab National Bank","1049","","","Punjab National Bank","54","","","Punjab & Sind Bank","1055"),
			RATNAKAR_BANK_CORPORATE ("RATNAKAR BANK CORPORATE", "1130","","","RATNAKAR BANK CORPORATE","RTC","","","","","","","","","",""),
			SURYODAY_SMALL_FINANCE_BANK ("SURYODAY SMALL FINANCE BANK", "1131","","","SURYODAY SMALL FINANCE BANK","SRB","","","","","","","","","",""),
			SHAMRAO_VITHAL_RETAIL__BANK ("SHAMRAO VITHAL RETAIL BANK", "1132","","","SHAMRAO VITHAL RETAIL BANK","SVC","","","","","","","","","",""),
			THANE_BHARAT_SAHAKARI__BANK ("Thane Bharat Sahakari Bank", "1133","","","Thane Bharat Sahakari Bank","TBB","","","","","","","","","",""),
			TJSB__BANK ("TJSB Bank", "1134","TJSB Bank","CID0530","TJSB Bank","TJB","","","","","","","","","",""),

		
			// Direcpay Only
			DEMO_BANK ("DEMO Bank", "1025","DEMO Bank","CID050","","","","","","","Test Bank","63","","","",""),
			
			//CITRUSPAY ONLY
			ANDHRA_BANK ("Andhra Bank", "1091","Andhra Bank","CID016","Andhra Bank","ADB","Andhra Bank","1058","","","Andhra Bank","50","","","Andhra Bank","1058"),
			BANK_OF_BRAODA_CORPORATE_ACCOUNTS("Bank of Baroda Corporate Accounts", "1092","Bank of Baroda Corporate Accounts","CID018","Bank of Baroda Corporate Accounts","BBC","","","","","Bank Of Baroda","133","","","Bank of Baroda Net Banking Corporate","1075"),
			BANK_OF_BARODA_RETAIL_ACCOUNTS("Bank of Baroda Retail Accounts", "1093","Bank of Baroda Retail Accounts","CID020","Bank of Baroda Retail Accounts","BBR","","","","","","","","","Bank of Baroda Net Banking Retail","1076"),
			CATHOLIC_SYRIAN_BANK("Catholic Syrian Bank", "1094","Catholic Syrian Bank","CID022","Catholic Syrian Bank","CSB","Catholic Syrian Bank","1031","","","Catholic Syrian Bank","51","","","CSB Bank","1031"),
			LAKSHMI_VILAS_BANK_NETBANKING("Lakshmi Vilas Bank NetBanking", "1095","Lakshmi Vilas Bank NetBanking","CID034","LAXMI VILAS RETAIL BANK","LVR","Lakshmi Vilas Bank NetBanking","1009","","","Lakshmi Vilas Bank","130","","","Lakshmi Vilas Bank NetBanking","1009"),	
			PUNJAB_NATIONAL_BANK_CORPORATE_ACCOUNTS("Punjab National Bank Corporate Accounts", "1096","Punjab National Bank Corporate Accounts","CID036","Punjab National Bank Corporate Accounts","CPN","","","","","","","","","Punjab National Bank - Corporate","1077"),
			STANDARD_CHARTERED_BANK("Standard Chartered Bank", "1097","Standard Chartered Bank","CID038","Standard Chartered Bank","SCB","","","","","","","","","",""),
			SYNDICATE_BANK ("SYNDICATE BANK", "1098","Syndicate Bank","CID039","SYNDICATE BANK","SYD","","","","","","","","","",""),
			AXIS_CORPORATE_BANK ("Axis Corporate Bank", "1099","AXIS Corporate Bank","CID080","","","","","","","","","","","",""),
			ICICI_CORPORATE_BANK ("ICICI Corporate Bank", "1100","ICICI Corporate Bank","CID050","","","","","","","","","","","",""),
			PNB_CORPORATE_BANK ("PNB Corporate Bank", "1101","PNB Corporate Bank","CID044","","","","","","","","","","","",""),
			HSBC_BANK ("HSBC Bank", "1102","HSBC Bank","CID090","","","","","","","","","","","",""),
			UCO_BANK ("UCO Bank", "1103","UCO Bank","CID070","UCO Bank","UCO","UCO Bank","1057","","","UCO Bank","56","","","UCO Bank","1057"),
			COSMOS_BANK ("COSMOS Bank", "1104","COSMOS Bank","CID053","Cosmos Bank","COB","","","","","","","","","",""),
			//
			DCB_BANK ("DCB BANK", "1105","DCB BANK","CID0530","DCB Bank","DCB","DCB Bank","1027","","","DCB Bank (Corporate)","52","","","DCB BANK","1027"),
			DENA_BANK ("DENA BANK", "1106","DENA BANK","CID009","DENA BANK","DEN","","","","","","","","","",""),
			DHANALAXMI_BANK ("DHANALAXMI BANK", "1107","DHANALAXMI BANK","CID055","DHANALAXMI BANK","DLB","Dhanalaxmi Bank","1038","","","Dhanlaxmi Bank","53","","","Dhanlaxmi Bank","1038"),
			JANTA_SAHAKARI_BANK ("JANTA SAHAKARI BANK", "1108","","","JANTA SAHAKARI BANK","JSB","","","","","","","","","",""),
			NKGSB_COOPERATIVE_BANK ("NKGSB CO-OPERATIVE BANK", "1109","","","NKGSB CO-OPERATIVE BANK","NKB","","","","","","","","","",""),
			PUNJAB_AND_MAHARASHTRA_COOPERATIVE_BANK ("PUNJAB AND MAHARASHTRA COOPERATIVE BANK", "1110","","","Punjab & Maharastra Coop Bank","PMC","PUNJAB AND MAHARASHTRA COOPERATIVE BANK","1065","","","","","","","",""),
			PUNJAB_AND_SIND_BANK ("PUNJAB AND SIND BANK", "1111","","","PUNJAB AND SIND BANK","PSB","PUNJAB AND SIND BANK","1055","","","Punjab and Sind Bank","55","","","Punjab & Sind Bank","1055"),
			SARASWAT_COOPERATIVE_BANK ("SARASWAT COOPERATIVE BANK", "1112","","","SARASWAT COOPERATIVE BANK","SWB","SARASWAT COOPERATIVE BANK","1053","","","","","","","SaraSwat Bank","1053"),
			SHAMRAO_VITHAL_COOPERATIVE_BANK ("SHAMRAO VITHAL COOPERATIVE BANK", "1113","","","Shamrao Vitthal Co-operative Bank - Corporate Net Banking","SV2","","","","","","","","","",""),
			TAMILNADU_COOPERATIVE_BANK ("Tamilnadu State Coop Bank", "1114","Tamilnadu State Coop Bank","CID054","Tamilnadu State Coop Bank","TNC","","","","","","","","","Tamilnad Mercantile Bank","1044"),	
			THANE_JANATA_SAHAKARI_BANKA_LTD ("THANE JANATA SAHAKARI BANKÂ LTD", "1115","","","","","","","","","","","","","Janata Sahakari Bank LTD Pune","1072"),	
			KALYAN_JANATA_SAHAKARI_BANK ("KALYAN JANATA SAHAKARI BANK", "1116","Tamilnadu State Coop Bank","C","Kalyan Janata Sahakari Bank","KJB","","","","","","","","","",""),	
			THE_MEHSANA_URBAN_COOP_BANK_LTD ("THE MEHSANA URBAN CO OP BANK LTD", "1117","","","The Mehsana Urban Co Op Bank Ltd","MSB","","","","","","","","","",""),	
			BANDHAN_BANK ("BANDHAN BANK", "1118","","","Bandhan bank","BDN","","","","","","","","","",""),
			DIGIBANK_BY_DBS ("DIGIBANK BY DBS", "1119","","","digibank by DBS","DBS","","","","","","","","","",""),
			IDFC_BANK ("IDFC BANK", "1120","","","IDFC Bank","IDN","","","","","","","","","IDFC FIRST Bank Limited","1073"),
			BASSIEN_CATHOLIC_COOPERATIVE_BANK ("BASSIEN CATHOLIC CO-OPERATIVE BANK", "1121","","","Bassien Catholic Co-Operative Bank","BCB","","","","","","","","","",""),
			PNB_YUVA_BANK ("PNB YUVA BANK", "1122","","","PNB YUVA BANK","PNY","","","","","","","","","",""),
			THE_KALUPUR_COMMERCIAL_COOPERATIVE_BANK_LIMITED ("THE KALUPUR COMMERCIAL COOPERATIVE BANK LIMITED", "1123","","","The Kalupur Commercial Cooperative Bank Limited","KLB","","","","","","","","","",""),
			EQUITAS_SMALL_FINANCE_BANK ("EQUITAS SMALL FINANCE BANK", "1124","","","Equitas Small Finance Bank","EQB","Equitas Small Finance Bank","1063","","","","","","","Equitas Bank","1063"),
			AU_SMALL_FINANCE_BANK("AU Small Finance Bank", "1135","","","AU Small Finance Bank","AU","","","","","","","","","",""),
			ZOROASTRIAN_BANK("Zoroastrian Bank", "1136","","","Zoroastrian Bank","ZOB","","","","","","","","","",""),
		    AIRTEL_BANK("AIRTEL BANK", "1137","","","","","","","","","","","AIRTEL BANK","NB","","");
		
		private final String name;
		private final String code;
		private final String citrusname;
		private final String citruscode;

		private final String billDeskName;
		private final String billDeskCode;
		private final String isgPayName;
		private final String isgPayCode;
		private final String sbiName;
		private final String sbiCode;
		private final String safexPayName;
		private final String safexPayCode;
		private final String airtelBankName;
		private final String airtelBankCode;
		private final String atomBankName;
		private final String atomBankCode;
		private NetBankingType (String name, String code, String citrusname, String citruscode
				, String billDeskName, String billDeskCode , String isgPayName, String isgPayCode, 
				String sbiName, String sbiCode,String safexPayName, String safexPayCode,String airtelBankName, String airtelBankCode
				,String atomBankName, String atomBankCode){
			this.name = name;
			this.code = code;
			this.citrusname = citrusname;
			this.citruscode = citruscode;
			this.billDeskName = billDeskName;
			this.billDeskCode = billDeskCode;
			this.isgPayName   = isgPayName;
			this.isgPayCode   = isgPayCode;
			this.sbiName   = sbiName;
			this.sbiCode   = sbiCode;
			this.safexPayName   = safexPayName;
			this.safexPayCode   = safexPayCode;
			this.airtelBankName   = airtelBankName;
			this.airtelBankCode   = airtelBankCode;
			this.atomBankName   = atomBankName;
			this.atomBankCode   = atomBankCode;
		}
		public String getIsgPayCode() {
			return isgPayCode;
		}
		public String getAirtelBankName() {
			return airtelBankName;
		}
		public String getAirtelBankCode() {
			return airtelBankCode;
		}
		public static List<NetBankingType> getCITRUSNBMops(){
			return getGetMopsFromSystemProp("CITRUSNB");		
		}
		//DO..
		public static List<NetBankingType> getDIRECPAYNBMops(){
			return getGetMopsFromSystemProp("DIRECPAYNB");		
		}
		//DO..
		public static List<NetBankingType> getKOTAKNBMops(){
			return getGetMopsFromSystemProp("KOTAKNB");		
		}
		//DO..
		public static List<NetBankingType> getYESBANKNBMops(){
			return getGetMopsFromSystemProp("YESBANKNB");		
		}
		
		//DO..
		public static List<NetBankingType> getBILLDESKNBMops() {
			return getGetMopsFromSystemProp("BILLDESKNB");
		}
		//DO..
		public static List<NetBankingType> getISGPAYNBMops() {
			return getGetMopsFromSystemProp("ISGPAYNB");
		}
		//DO..
		public static List<NetBankingType> getSBINBMops() {
			return getGetMopsFromSystemProp("SBINB");
		}
		//DO..
		public static List<NetBankingType> getSAFEXPAYNBMops() {
			return getGetMopsFromSystemProp("SAFEXPAYNB");
		}
		
		public static List<NetBankingType> getAIRTELNBMops() {
			return getGetMopsFromSystemProp("AIRTELBANKNB");
		}
		// Atom
		public static List<NetBankingType> getAtomNBMops() {
			return getGetMopsFromSystemProp("ATOMBANKNB");
		}
		
		
		public static List<NetBankingType> getGetMopsFromSystemProp(String mopsList){
			PropertiesManager propertiesManager= new PropertiesManager();
			
			List<String> mopStringList= (List<String>) Helper.parseFields(propertiesManager.getAcquirerMopType(mopsList));
			
			List<NetBankingType> mops = new ArrayList<NetBankingType>();
			
			for(String mopCode:mopStringList){
				NetBankingType mop = getmop(mopCode);
				mops.add(mop);
			}
			return mops;
		}
		public static NetBankingType getmop(String mopCode){
			NetBankingType mopObj = null;
			if(null!=mopCode){
			for(NetBankingType mop:NetBankingType.values()){
				if(mopCode.equals(mop.getCode().toString())){
					mopObj=mop;
					break;
				}
			  }
			}
			return mopObj;
		}
		public static String getmopName(String mopCode){
			NetBankingType NetbankingType = NetBankingType.getmop(mopCode);		
			if(NetbankingType == null) {
				return "";
			}
			return NetbankingType.getName();
		}
		
		public static String getCitruscode(String mopCode){
			NetBankingType NetbankingType = NetBankingType.getmop(mopCode);		
			if(NetbankingType == null) {
				return "";
			}
			return NetbankingType.getCitruscode();
		}
		

		
		// SafexPay bank Code
		
		public static String getSafexPaycode(String mopCode){
			NetBankingType NetbankingType = NetBankingType.getmop(mopCode);		
			if(NetbankingType == null) {
				return "";
			}
			return NetbankingType.getSafexPayCode();
		}
		
		
		public static String getBillDeskCode(String mopCode) {
			NetBankingType NetbankingType = NetBankingType.getmop(mopCode);		
			if(NetbankingType == null) {
				return "";
			}
			return NetbankingType.getBillDeskCode();
		}
		public static String getISGPayCode(String mopCode) {
			NetBankingType NetbankingType = NetBankingType.getmop(mopCode);		
			if(NetbankingType == null) {
				return "";
			}
			return NetbankingType.getISGPayCode();
		}
		
		public static String getAirtelBankCode(String mopCode) {
			NetBankingType NetbankingType = NetBankingType.getmop(mopCode);		
			if(NetbankingType == null) {
				return "";
			}
			return NetbankingType.getAirtelBankCode();
		}
		// Atom Bank
		public static String getAtomBankCode(String mopCode) {
			NetBankingType NetbankingType = NetBankingType.getmop(mopCode);		
			if(NetbankingType == null) {
				return "";
			}
			return NetbankingType.getAtomBankCode();
		}
		
		public String getName() {
			return name;
		}

		public String getCode(){
			return code;
		}

		public String getCitrusname() {
			return citrusname;
		}

		public String getCitruscode() {
			return citruscode;
		}
		public String getBillDeskName() {
			return billDeskName;
		}
		public String getBillDeskCode() {
			return billDeskCode;
		}
		public String getIsgPayName() {
			return isgPayName;
		}
		public String getISGPayCode() {
			return isgPayCode;
		}
		public String getSbiName() {
			return sbiName;
		}
		public String getSbiCode() {
			return sbiCode;
		}
		public String getSafexPayName() {
			return safexPayName;
		}
		public String getSafexPayCode() {
			return safexPayCode;
		}
		public String getAtomBankName() {
			return atomBankName;
		}
		public String getAtomBankCode() {
			return atomBankCode;
		}

}
