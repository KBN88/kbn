package com.kbn.safexpay;

import com.kbn.pg.core.Processor;

public class SafexPayProcessorFactory {

	public static Processor getInstance() {
		return new SafexPayProcessor();
	}

}
