package com.kbn.safexpay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;

public class SafexPayTransactionFetchService extends HibernateAbstractDao {

	private static Logger logger = Logger.getLogger(SafexPayTransactionFetchService.class.getName());
	private User user = new User();

	private static final String origTxnQuery = "select ORIG_TXN_ID, PAY_ID ,PG_REF_NUM, CURRENCY_CODE,PG_GATEWAY,ACQ_ID from TRANSACTION where TXN_ID=?";

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public Map<String, String> fetchFafexPayOrigTxn(String txnId) throws SystemException {
		Map<String, String> requestMap = new HashMap<String, String>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(origTxnQuery)) {
				preparedStatement.setString(1, txnId);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						requestMap.put("origTxnId", resultSet.getString("ORIG_TXN_ID"));
						requestMap.put("payId", resultSet.getString("PAY_ID"));
						requestMap.put("PG_REF_NUM", resultSet.getString("PG_REF_NUM"));
						requestMap.put("CURRENCY_CODE", resultSet.getString("CURRENCY_CODE"));
						requestMap.put("PG_GATEWAY", resultSet.getString("PG_GATEWAY"));
						requestMap.put("ACQ_ID", resultSet.getString("ACQ_ID"));
					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching Original Transaction Id for ISGPay Manual status enquery "
					+ sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}finally {
			autoClose();
		}
		return requestMap;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
