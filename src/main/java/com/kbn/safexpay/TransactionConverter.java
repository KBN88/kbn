package com.kbn.safexpay;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.WhitelableBranding;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.NetBankingType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.UPIType;
import com.kbn.commons.util.WalletType;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Currency;

public class TransactionConverter {
	private static Logger logger = Logger.getLogger(TransactionConverter.class.getName());
	public static final char QUOTE = '"';
	public static final char COMMA = ',';
	public static final char COLON = ':';

	public String createSaleTransaction(Fields fields) {
		UserDao userDao = new UserDao();
		// White-Label Response url
		HttpServletRequest request = ServletActionContext.getRequest();
		String brandingURL = request.getRequestURL().toString();
		URL urlTemp = null;
		try {
			urlTemp = new URL(brandingURL);
		} catch (MalformedURLException exception) {
			logger.error("WhitLabe Brand HostUrl error :" + exception);
		}
		String brandingHost = urlTemp.getHost();
		logger.info("WhitLabe Brand HostUrl :" + brandingHost);
		WhitelableBranding userReseller = userDao.findByWhitelabelBrandURL(brandingHost);
		String merchant_key = fields.get(FieldType.TXN_KEY.getName());
		StringBuilder txn_details = new StringBuilder();
		appendTxnDetailsInfo(fields, txn_details,userReseller);
		String Encrypted_txn_details = PayGateCryptoUtils.encrypt(txn_details.toString(), merchant_key);
		StringBuilder pg_details = new StringBuilder();
		StringBuilder Upi_details = new StringBuilder();	
		if (PaymentType.NET_BANKING.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
			appendPGDetailsNetBankingInfo(fields, pg_details);
		} else if (PaymentType.WALLET.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
			appendPGDetailsWalletInfo(fields, pg_details);
		} else if (PaymentType.UPI.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
			appendPGDetailsUPIInfo(fields, pg_details);
			appendPGDetailsVartualIDnfo(fields, Upi_details);
			
		} else {
			appendPGDetailsInfo(fields, pg_details);

		}
		String Encrypted_pg_details = PayGateCryptoUtils.encrypt(pg_details.toString(), merchant_key);
		String Encrypted_vpa_address = PayGateCryptoUtils.encrypt(Upi_details.toString(), merchant_key);

		StringBuilder card_details = new StringBuilder();
		if (PaymentType.NET_BANKING.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
			appendCardDetailsNetBankingInfo(fields, card_details);
		} else if ((PaymentType.WALLET.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName())))) {
			appendCardDetailsNetBankingInfo(fields, card_details);
		}  else if ((PaymentType.UPI.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName())))) {
			   appendCardDetailsNetBankingInfo(fields, card_details);
		}else {
			appendCardDetailsInfo(fields, card_details);
		}
		String Encrypted_card_details = PayGateCryptoUtils.encrypt(card_details.toString(), merchant_key);

		StringBuilder cust_details = new StringBuilder();
		appendCustomerDetailsInfo(fields, cust_details);
		String Encrypted_cust_details = PayGateCryptoUtils.encrypt(cust_details.toString(), merchant_key);

		StringBuilder bill_details = new StringBuilder();
		appendBillDetailsInfo(fields, bill_details);
		String Encrypted_bill_details = PayGateCryptoUtils.encrypt(bill_details.toString(), merchant_key);

		StringBuilder ship_details = new StringBuilder();
		appendShipDetailsInfo(fields, ship_details);
		String Encrypted_ship_details = PayGateCryptoUtils.encrypt(ship_details.toString(), merchant_key);

		StringBuilder item_details = new StringBuilder();
		appendItemDetailsInfo(fields, item_details);
		String Encrypted_item_details = PayGateCryptoUtils.encrypt(item_details.toString(), merchant_key);

		StringBuilder other_details = new StringBuilder();
		appendOtherDetailsInfo(fields, other_details);
		String Encrypted_other_details = PayGateCryptoUtils.encrypt(other_details.toString(), merchant_key);

		StringBuilder httpRequest = new StringBuilder();
		httpRequest.append("<HTML>");
		httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
		httpRequest.append("<form name=\"form1\" action=\"");
		httpRequest.append(ConfigurationConstants.SAFEXPAY_TRANSACTION_URL.getValue());
		httpRequest.append("\" method=\"post\">");
		httpRequest.append("<input type=\"hidden\" name=\"me_id\" value=\"");
		httpRequest.append(fields.get(FieldType.MERCHANT_ID.getName()));
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"txn_details\" value=\"");
		httpRequest.append(Encrypted_txn_details);
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"pg_details\" value=\"");
		httpRequest.append(Encrypted_pg_details);
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"card_details\" value=\"");
		httpRequest.append(Encrypted_card_details);
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"cust_details\" value=\"");
		httpRequest.append(Encrypted_cust_details);
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"bill_details\" value=\"");
		httpRequest.append(Encrypted_bill_details);
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"ship_details\" value=\"");
		httpRequest.append(Encrypted_ship_details);
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"item_details\" value=\"");
		httpRequest.append(Encrypted_item_details);
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"other_details\" value=\"");
		httpRequest.append(Encrypted_other_details);
		httpRequest.append("\">");
		httpRequest.append("<input type=\"hidden\" name=\"Upi_details\" value=\"");
		httpRequest.append(Encrypted_vpa_address);
		httpRequest.append("\">");
		httpRequest.append("</form>");
		httpRequest.append("<script language=\"JavaScript\">");
		httpRequest.append("function OnLoadEvent()");
		httpRequest.append("{document.form1.submit();}");
		httpRequest.append("</script>");
		httpRequest.append("</BODY>");
		httpRequest.append("</HTML>");

		return httpRequest.toString();

	}
	private void appendPGDetailsWalletInfo(Fields fields, StringBuilder pg_details) {
		pg_details.append(WalletType.getSafexPayCode(fields.get(FieldType.MOP_TYPE.getName())));
		pg_details.append(Constants.SEPARATOR);
		String walletType = fields.get(FieldType.PAYMENT_TYPE.getName());
		if (walletType.equals("WL")) {
			walletType = "WA";
		}
		pg_details.append(walletType);
		pg_details.append(Constants.SEPARATOR);
		String mopType = fields.get(FieldType.MOP_TYPE.getName());
		if (mopType.equals("VI")) {
			mopType = "1";
		} else if (mopType.equals("MC")) {
			mopType = "2";
		} else if (mopType.equals("MS")) {
			mopType = "5";
		} else if (mopType.equals("RU")) {
			mopType = "6";
		} else if (mopType.equals("AX")) {
			mopType = "3";
		} else {
			mopType = "7";
		}
		pg_details.append(mopType);
		pg_details.append(Constants.SEPARATOR);
		pg_details.append("6");
	}

	private void appendCardDetailsNetBankingInfo(Fields fields, StringBuilder card_details) {
		card_details.append("");
		card_details.append(Constants.SEPARATOR);
		card_details.append("");
		card_details.append(Constants.SEPARATOR);
		card_details.append("");
		card_details.append(Constants.SEPARATOR);
		card_details.append("");
		card_details.append(Constants.SEPARATOR);
		card_details.append("");

	}
    private void appendPGDetailsVartualIDnfo(Fields fields, StringBuilder upi_details) {
	upi_details.append(fields.get(FieldType.UPI.getName()));

	}
	
	
	// 63|Test Bank|NB|
	private void appendPGDetailsUPIInfo(Fields fields, StringBuilder pg_details) {
		pg_details.append(UPIType.getSafexPaycode(fields.get(FieldType.MOP_TYPE.getName())));
		pg_details.append(Constants.SEPARATOR);
		pg_details.append(fields.get(FieldType.PAYMENT_TYPE.getName()));
		pg_details.append(Constants.SEPARATOR);

		String mopType = fields.get(FieldType.MOP_TYPE.getName());
		if (mopType.equals("VI")) {
			mopType = "1";
		} else if (mopType.equals("MC")) {
			mopType = "2";
		} else if (mopType.equals("MS")) {
			mopType = "5";
		} else if (mopType.equals("RU")) {
			mopType = "6";
		} else if (mopType.equals("AX")) {
			mopType = "3";
		} else {
			mopType = "7";
		}
		pg_details.append(mopType);
		pg_details.append(Constants.SEPARATOR);
		pg_details.append("6");
		
	}

	

	// 63|Test Bank|NB|
	private void appendPGDetailsNetBankingInfo(Fields fields, StringBuilder pg_details) {
		pg_details.append(NetBankingType.getSafexPaycode(fields.get(FieldType.MOP_TYPE.getName())));
		pg_details.append(Constants.SEPARATOR);
		pg_details.append(fields.get(FieldType.PAYMENT_TYPE.getName()));
		pg_details.append(Constants.SEPARATOR);

		String mopType = fields.get(FieldType.MOP_TYPE.getName());
		if (mopType.equals("VI")) {
			mopType = "1";
		} else if (mopType.equals("MC")) {
			mopType = "2";
		} else if (mopType.equals("MS")) {
			mopType = "5";
		} else if (mopType.equals("RU")) {
			mopType = "6";
		} else if (mopType.equals("AX")) {
			mopType = "3";
		} else {
			mopType = "7";
		}
		pg_details.append(mopType);
		pg_details.append(Constants.SEPARATOR);
		pg_details.append("6");
	}

	private void appendOtherDetailsInfo(Fields fields, StringBuilder other_details) {
		other_details.append(Constants.udf_1);
		other_details.append(Constants.SEPARATOR);
		other_details.append(Constants.udf_2);
		other_details.append(Constants.SEPARATOR);
		other_details.append(Constants.udf_3);
		other_details.append(Constants.SEPARATOR);
		other_details.append(Constants.udf_4);
		other_details.append(Constants.SEPARATOR);
		other_details.append(Constants.udf_5);
	}

	private void appendItemDetailsInfo(Fields fields, StringBuilder iteam_details) {
		iteam_details.append(Constants.item_count);
		iteam_details.append(Constants.SEPARATOR);
		iteam_details.append(Constants.item_value);
		iteam_details.append(Constants.SEPARATOR);
		iteam_details.append(Constants.item_category);

	}

	private void appendShipDetailsInfo(Fields fields, StringBuilder ship_details) {
		String address = fields.get(FieldType.CUST_SHIP_STREET_ADDRESS1.getName());
		String address2 = fields.get(FieldType.CUST_SHIP_STREET_ADDRESS2.getName());

		if (address == null) {
			address = "";
		}
		if (address2 == null) {
			address2 = "";
		}
		String city = fields.get(FieldType.CUST_SHIP_CITY.getName());
		if (city == null) {
			city = "";
		}
		String state = fields.get(FieldType.CUST_SHIP_STATE.getName());
		if (state == null) {
			state = "";
		}
		String country = fields.get(FieldType.CUST_SHIP_COUNTRY.getName());
		if (country == null) {
			country = "";
		}
		String zipCode = fields.get(FieldType.CUST_SHIP_ZIP.getName());
		if (zipCode == null) {
			zipCode = "";
		}

		ship_details.append(address + address2);
		ship_details.append(Constants.SEPARATOR);
		ship_details.append(city);
		ship_details.append(Constants.SEPARATOR);
		ship_details.append(state);
		ship_details.append(Constants.SEPARATOR);
		ship_details.append(country);
		ship_details.append(Constants.SEPARATOR);
		ship_details.append(zipCode);
		ship_details.append(Constants.SEPARATOR);
		ship_details.append(Constants.ship_days);
		ship_details.append(Constants.SEPARATOR);
		ship_details.append(Constants.address_count);
	}

	private void appendBillDetailsInfo(Fields fields, StringBuilder bill_details) {
		String address = fields.get(FieldType.CUST_STREET_ADDRESS1.getName());
		String address2 = fields.get(FieldType.CUST_STREET_ADDRESS1.getName());

		if (address == null) {
			address = "";
		}
		if (address2 == null) {
			address2 = "";
		}
		String city = fields.get(FieldType.CUST_CITY.getName());
		if (city == null) {
			city = "";
		}
		String state = fields.get(FieldType.CUST_STATE.getName());
		if (state == null) {
			state = "";
		}
		String country = fields.get(FieldType.CUST_COUNTRY.getName());
		if (country == null) {
			country = "";
		}
		String zipCode = fields.get(FieldType.CUST_ZIP.getName());
		if (zipCode == null) {
			zipCode = "";
		}
		bill_details.append(address + address2);
		bill_details.append(Constants.SEPARATOR);
		bill_details.append(city);
		bill_details.append(Constants.SEPARATOR);
		bill_details.append(state);
		bill_details.append(Constants.SEPARATOR);
		bill_details.append(country);
		bill_details.append(Constants.SEPARATOR);
		bill_details.append(zipCode);

	}

	private void appendCustomerDetailsInfo(Fields fields, StringBuilder cust_details) {
		String custphone  = fields.get(FieldType.CUST_PHONE.getName());
		cust_details.append(fields.get(FieldType.CUST_NAME.getName()));
		cust_details.append(Constants.SEPARATOR);
		cust_details.append(fields.get(FieldType.CUST_EMAIL.getName()));
		cust_details.append(Constants.SEPARATOR);
		if(custphone == null) {
			custphone ="9999999999";
		}
		cust_details.append(custphone);
		cust_details.append(Constants.SEPARATOR);
		cust_details.append(fields.get(FieldType.TXN_ID.getName()));
		cust_details.append(Constants.SEPARATOR);
		cust_details.append(Constants.is_logged_in);

	}

	private void appendCardDetailsInfo(Fields fields, StringBuilder card_details) {
		String expDate = fields.get(FieldType.CARD_EXP_DT.getName());
		String expMonths = expDate.substring(0, 2);
		String expyear = expDate.substring(2, 6);
		card_details.append(fields.get(FieldType.CARD_NUMBER.getName()));
		card_details.append(Constants.SEPARATOR);
		card_details.append(expMonths);
		card_details.append(Constants.SEPARATOR);
		card_details.append(expyear);
		card_details.append(Constants.SEPARATOR);
		card_details.append(fields.get(FieldType.CVV.getName()));
		card_details.append(Constants.SEPARATOR);
		card_details.append(fields.get(FieldType.CUST_NAME.getName()));

	}

	private void appendPGDetailsInfo(Fields fields, StringBuilder pg_details) {
		String mopType = fields.get(FieldType.MOP_TYPE.getName());
		if (mopType.equals("AX")) {
			pg_details.append("1");
		} else {
			// Password is used only  safexpay Pg_ID 
			pg_details.append(fields.get(FieldType.PASSWORD.getName()));
		}
		pg_details.append(Constants.SEPARATOR);
		pg_details.append(fields.get(FieldType.PAYMENT_TYPE.getName()));
		pg_details.append(Constants.SEPARATOR);

		if (mopType.equals("VI")) {
			mopType = "1";
		} else if (mopType.equals("MC")) {
			mopType = "2";
		} else if (mopType.equals("MS")) {
			mopType = "5";
		} else if (mopType.equals("RU")) {
			mopType = "6";
		} else if (mopType.equals("AX")) {
			mopType = "3";
		} else {
			mopType = "7";
		}
		pg_details.append(mopType);
		pg_details.append(Constants.SEPARATOR);
		pg_details.append("6");
	}

	private void appendTxnDetailsInfo(Fields fields, StringBuilder txn_details,WhitelableBranding userReseller) {
		String currencyCode = Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName()));
		txn_details.append(Constants.AG_ID);
		txn_details.append(Constants.SEPARATOR);
		txn_details.append(fields.get(FieldType.MERCHANT_ID.getName()));
		txn_details.append(Constants.SEPARATOR);
		txn_details.append(fields.get(FieldType.TXN_ID.getName()));
		txn_details.append(Constants.SEPARATOR);
		String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));
		txn_details.append(amount);
		txn_details.append(Constants.SEPARATOR);
		if(currencyCode.equals("GBP")) {
			txn_details.append("GBR");
			txn_details.append(Constants.SEPARATOR);
		}else if(currencyCode.equals("EUR")) {
			txn_details.append("ITA");
			txn_details.append(Constants.SEPARATOR);
		}else if(currencyCode.equals("USD")) {
			txn_details.append("USA");
			txn_details.append(Constants.SEPARATOR);
		}else {
			txn_details.append(Constants.Country);
			txn_details.append(Constants.SEPARATOR);
		}
		txn_details.append(currencyCode);
		txn_details.append(Constants.SEPARATOR);
		String txnType = fields.get(FieldType.TXNTYPE.getName());
		if (txnType.equals("ENROLL")) {
			txnType = "SALE";
		}
		txn_details.append(txnType);
		txn_details.append(Constants.SEPARATOR);
		if (userReseller != null) {
		txn_details.append("https://"+userReseller.getBrandURL()+"/crm/jsp/safexPayResponse");
			}else {
				txn_details.append(ConfigurationConstants.SAFEXPAY_RETURN_URL.getValue());
			}
		txn_details.append(Constants.SEPARATOR);
		if (userReseller != null) {
		 txn_details.append("https://"+userReseller.getBrandURL()+"/crm/jsp/safexPayResponse");
			}else {
		 txn_details.append(ConfigurationConstants.SAFEXPAY_RETURN_URL.getValue());
			}
		
		txn_details.append(Constants.SEPARATOR);
		txn_details.append(Constants.Channel);

	}

	public String createRefundTransaction(Fields fields) {

		StringBuilder request = new StringBuilder();
		appendRefundDeatils(fields, request);
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);
		return jsonRequest.toString();

	}

	private void appendRefundDeatils(Fields fields, StringBuilder request) {
		String merchant_key = fields.get(FieldType.TXN_KEY.getName());
		String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));
		String ag_ref1 = fields.get(FieldType.PG_REF_NUM.getName());
		String ag_ref = PayGateCryptoUtils.encrypt(ag_ref1, merchant_key);
		String refund_amount = PayGateCryptoUtils.encrypt(amount, merchant_key);
		String refund_reason = PayGateCryptoUtils.encrypt("Refund Reason", merchant_key);
		appendJsonField(Constants.AGID, "Paygate", request);
		appendJsonField(Constants.MERCHANT_ID, fields.get(FieldType.MERCHANT_ID.getName()), request);
		appendJsonField(Constants.PG_REFENCE, ag_ref, request);
		appendJsonField(Constants.REFUND_AMOUNT, refund_amount, request);
		appendJsonField(Constants.REFUND_REASON, refund_reason, request);
	}

	private void appendJsonField(String name, String value, StringBuilder request) {
		if (null == value || value.isEmpty()) {
			return;
		}

		if (request.length() > 0) {
			// Append comma
			request.append(COMMA);
		}

		// Append name
		request.append(QUOTE);
		request.append(name);
		request.append(QUOTE);

		// Append colon
		request.append(COLON);

		// Append value
		request.append(QUOTE);
		request.append(value);
		request.append(QUOTE);

	}

// status API 
	public String createSatatusTransaction(Fields fields) {
		StringBuilder request = new StringBuilder();
		appendStatusDeatils(fields, request);
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);
		return jsonRequest.toString();
	}

	private void appendStatusDeatils(Fields fields, StringBuilder request) {
		String merchant_key = fields.get(FieldType.TXN_KEY.getName());
		String ag_ref = PayGateCryptoUtils.encrypt(fields.get(FieldType.PG_REF_NUM.getName()), merchant_key);
		String orderId = PayGateCryptoUtils.encrypt(fields.get(FieldType.TXN_ID.getName()), merchant_key);
		appendJsonField(Constants.AGID, "Paygate", request);
		appendJsonField(Constants.MERCHANT_ID, fields.get(FieldType.MERCHANT_ID.getName()), request);
		appendJsonField(Constants.ORDER_NO, orderId, request);
		appendJsonField(Constants.PG_REFENCE, ag_ref, request);
	}

}
