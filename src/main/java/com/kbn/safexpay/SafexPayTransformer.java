package com.kbn.safexpay;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class SafexPayTransformer {
	private String[] txn_response_List;

	public SafexPayTransformer(String decryptedTxn_response) {
		this.txn_response_List = decryptedTxn_response.split("\\|");

	}

	public ErrorType getSaleResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("Successful")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("Failed") || respCode.equals("Aborted") || respCode.equals("")) {
			errorType = ErrorType.TXN_FAILED;
		} else if (respCode.equals("RDF")) {
			errorType = ErrorType.DUPLICATE;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getSaleStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Successful")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("Failed") || respCode.equals("Aborted") || respCode.equals("")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("RDF")) {
			status = StatusType.DUPLICATE;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}
	//Paygate|201905040001|1905190854449058|2000.00|IND|INR|2019-05-19|08:54:29.0|1029950927632572416||Failed|00046|Received fail response from the bank.
//	[Paygate|201710270001|1902111548081001|1.00|IND|INR|2019-02-11|15:50:43.0|1094904050068520960|190421552291|Successful|0|Successful]
	public void updateSafexPayResponse(Fields fields) {
		StatusType status = getSaleStatus(txn_response_List[10].toString());
		ErrorType errorType = getSaleResponse(txn_response_List[10].toString());
		String  pg_rrn  =  (txn_response_List[9].toString());
		fields.put(FieldType.PG_GATEWAY.getName(), txn_response_List[1].toString());
		fields.put(FieldType.TXN_ID.getName(), txn_response_List[2].toString());
		fields.put(FieldType.AMOUNT.getName(), txn_response_List[3].toString());
		fields.put(FieldType.CUST_COUNTRY.getName(), txn_response_List[4].toString());
		fields.put(FieldType.PG_DATE_TIME.getName(), txn_response_List[6].toString()+txn_response_List[7].toString());
		fields.put(FieldType.PG_REF_NUM.getName(), txn_response_List[8].toString());
        if(pg_rrn.equals("")) {
        	pg_rrn ="0";
        	fields.put(FieldType.RRN.getName(),pg_rrn);
		}
        else {
        	fields.put(FieldType.RRN.getName(), txn_response_List[9].toString());
        }
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.PG_RESP_CODE.getName(), txn_response_List[11].toString());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), txn_response_List[12].toString());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
		fields.put(FieldType.ACQ_ID.getName(), fields.get(FieldType.TXN_KEY.getName()));
	}

}
