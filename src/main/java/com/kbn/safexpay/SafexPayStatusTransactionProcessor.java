package com.kbn.safexpay;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class SafexPayStatusTransactionProcessor implements TransactionProcessor{
	private static Logger logger = Logger.getLogger(SafexPayStatusTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		JSONParser parser = new JSONParser();
		Object obj = null;
		String merchant_key = fields.get(FieldType.TXN_KEY.getName());
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createSatatusTransaction(fields);
		logger.info("Request to SafexPay: " + request);
		String statusResponse = communicator.Refundtransact(request, ConfigurationConstants.SAFEXPAY_ENQUIRY_URL.getValue());
		String Encrypted_txn_details = PayGateCryptoUtils.decrypt(statusResponse, merchant_key);
		try {
			obj = parser.parse(Encrypted_txn_details);
		} catch (ParseException exception) {
			exception.printStackTrace();
		}              
		JSONObject jObject = (JSONObject) obj;
		SafexPayRefundTransformer transformer = new SafexPayRefundTransformer(jObject.get("txn_response").toString());
		transformer.updateSafexPayStatusResponse(fields);
		fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));

	}

}
