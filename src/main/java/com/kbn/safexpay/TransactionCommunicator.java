package com.kbn.safexpay;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

public class TransactionCommunicator {
	private static Logger logger = Logger.getLogger(TransactionCommunicator.class.getName());

	public String sendAuthorization(String request, Fields fields) throws SystemException {
		PrintWriter out;
		String response = "";
		logger.info("Request sent to SAFEXPAY: " + request);
		try {
			out = ServletActionContext.getResponse().getWriter();
			out.write(request);
			// Return response
			response = ErrorType.SUCCESS.getCode();
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
		} catch (IOException iOException) {
			logger.error(iOException);
			response = ErrorType.UNKNOWN.getCode();
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, iOException, "Network Exception with SAFEXPAY");
		}
		return response;

	}

	// Refund API
	public String Refundtransact(String request, String hostUrl) throws SystemException {
		String response = "";

		try {

			URL url = new URL(hostUrl);
			URLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			DataOutputStream dataoutputstream = new DataOutputStream(connection.getOutputStream());

			dataoutputstream.writeBytes(request);
			dataoutputstream.flush();
			dataoutputstream.close();
			BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String decodedString;
			while ((decodedString = bufferedreader.readLine()) != null) {
				response = response + decodedString;
			}

		} catch (IOException ioException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with SafexPay API " + hostUrl.toString());
		}
		return response;
	}
}
