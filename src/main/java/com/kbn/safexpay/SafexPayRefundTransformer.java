package com.kbn.safexpay;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class SafexPayRefundTransformer {
	private String responseMap;

	public SafexPayRefundTransformer(String response) {
		this.responseMap = response;
	}

	public ErrorType getRefundResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("00000")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("00054")) {
			errorType = ErrorType.TXN_FAILED;
		} else if (respCode.equals("RDF")) {
			errorType = ErrorType.DUPLICATE;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	public StatusType getRefundStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("00000")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("00054")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("RDF")) {
			status = StatusType.DUPLICATE;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

	public void updateSafexPayRefundResponse(Fields fields) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(responseMap);
		} catch (ParseException exception) {
			exception.printStackTrace();
		}
		JSONObject jObject = (JSONObject) obj;
		StatusType status = getRefundStatus(jObject.get("res_code").toString());
		ErrorType errorType = getRefundResponse(jObject.get("res_code").toString());
		fields.put(FieldType.RRN.getName(), jObject.get("pg_ref").toString());
		fields.put(FieldType.PG_REF_NUM.getName(), jObject.get("ag_ref").toString());
		fields.put(FieldType.PG_DATE_TIME.getName(),
				jObject.get("txn_date").toString() + jObject.get("txn_time").toString());
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.PG_RESP_CODE.getName(), jObject.get("status").toString());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), jObject.get("status").toString());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());

	}
	//{"txn_response":{"ag_id":"Paygate","me_id":"201710270001","order_no":"1902131614121001","amount":"1.00","country":"IND","currency":"INR","txn_date":"13-02-2019","txn_time":"16:16:42","ag_ref":"1095635367815053312","pg_ref":"190441652791","status":"Successful","res_code":"00000","res_message":"Successful"},"pg_details":{"pg_id":"107","paymode":"DC","emi_months":"0"},"fraud_details":{},"other_details":{"udf_1":"","udf_2":"","udf_3":"","udf_4":"","udf_5":""}}
	//{"order_no":"1905241134221002","country":"IND","res_message":"Successful","amount":"1.00","ag_ref":"1031801193524862976","me_id":"201903010003","res_code":"00000","bank_response":"3DS authentication failed","txn_date":"24-05-2019","ag_id":"Paygate","txn_time":"11:26:47","currency":"INR","pg_ref":"","status":"Failed"}
	public void updateSafexPayStatusResponse(Fields fields) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(responseMap);
		} catch (ParseException exception) {
			exception.printStackTrace();
		}
		JSONObject jObject = (JSONObject) obj;
		StatusType status = getStatus(jObject.get("status").toString());
		ErrorType errorType = getResponse(jObject.get("status").toString());
		fields.put(FieldType.RRN.getName(), jObject.get("pg_ref").toString());
		fields.put(FieldType.PG_REF_NUM.getName(), jObject.get("ag_ref").toString());
		fields.put(FieldType.PG_DATE_TIME.getName(),
				jObject.get("txn_date").toString() + jObject.get("txn_time").toString());
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.PG_RESP_CODE.getName(), jObject.get("res_code").toString());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), jObject.get("bank_response").toString());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
	}

	private ErrorType getResponse(String respCode) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("Successful")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("Failed")) {
			errorType = ErrorType.TXN_FAILED;
		} else if (respCode.equals("RDF")) {
			errorType = ErrorType.DUPLICATE;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	private StatusType getStatus(String respCode) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("Successful")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("Failed")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("RDF")) {
			status = StatusType.DUPLICATE;
		} else {
			status = StatusType.ERROR;
		}

		return status;
	}

}
