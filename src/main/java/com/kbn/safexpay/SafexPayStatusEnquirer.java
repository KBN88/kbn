package com.kbn.safexpay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.TransactionSearchService;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;

public class SafexPayStatusEnquirer {
	private static Logger logger = Logger.getLogger(SafexPayStatusEnquirer.class.getName());

	public void statusEnquirer() {

		TransactionSearchService transactionSearchService = new TransactionSearchService();
		try {
			List<TransactionSummaryReport> transactionList = transactionSearchService
					.getTransactionsForStatusUpdate(AcquirerType.SAFEX_PAY.getCode());
			for (TransactionSummaryReport transaction : transactionList) {

				Map<String, String> reqMap = prepareFields(transaction);
				Fields fields = new Fields(reqMap);
				SafexPayStatusTransactionProcessor processor = new SafexPayStatusTransactionProcessor();
				processor.transact(fields);
				String response = fields.get(FieldType.STATUS.getName());
				if (!response.equals("Error")) {
					// update new order transaction
					fields.updateStatus();
					// Update sale txn
					fields.updateTransactionDetails();
				}

			}
		} catch (SystemException systemException) {
			logger.error("Error updating status for SAFEXPAY", systemException);
		}
	}

	private Map<String, String> prepareFields(TransactionSummaryReport transaction) {
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put(FieldType.PAY_ID.getName(), transaction.getPayId());
		requestMap.put(FieldType.TXN_ID.getName(), transaction.getTransactionId());
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), transaction.getOrigTransactionId());
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.SAFEX_PAY.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), transaction.getCurrencyCode());
		requestMap.put(FieldType.MERCHANT_ID.getName(), transaction.getPgGateway());
		requestMap.put(FieldType.TXN_KEY.getName(), transaction.getTxnKey());
		requestMap.put(FieldType.PG_REF_NUM.getName(), transaction.getPgRefNum());
		return requestMap;
	}

	public String manualStatusEnquirer(String orderId) {
		String response = "";
		try {

			Map<String, String> reqMap = prepareFields(orderId);
			Fields fields = new Fields(reqMap);

			SafexPayStatusTransactionProcessor processor = new SafexPayStatusTransactionProcessor();
			processor.transact(fields);
			response = fields.get(FieldType.STATUS.getName());
			if (!response.equals("Error")) {
				// update new order transaction
				fields.updateStatus();
				// Update sale txn
				fields.updateTransactionDetails();
			}

		} catch (SystemException systemException) {
			logger.error("Error updating manual status for Payplutus", systemException);
		}

		return response;
	}

	private Map<String, String> prepareFields(String txnID) throws SystemException {
		// Fetching PayId and Original transaction id
		SafexPayTransactionFetchService safexPayTransactionFetchService = new SafexPayTransactionFetchService();
		Map<String, String> responseMap = safexPayTransactionFetchService.fetchFafexPayOrigTxn(txnID);
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put(FieldType.PAY_ID.getName(), responseMap.get("payId"));
		requestMap.put(FieldType.TXN_ID.getName(), txnID);
		requestMap.put(FieldType.ORIG_TXN_ID.getName(), responseMap.get("origTxnId"));
		requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.STATUS.getName());
		requestMap.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.SAFEX_PAY.getCode());
		requestMap.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
		requestMap.put(FieldType.CURRENCY_CODE.getName(), responseMap.get("CURRENCY_CODE"));
		   String merchantKey = responseMap.get("ACQ_ID");
		    if(merchantKey == null) {
			UserDao userDao = new UserDao();
			User user = userDao.findBySafexpayUser(responseMap.get("payId"));
			Account account = user.getAccountUsingAcquirerCode(AcquirerType.SAFEX_PAY.getCode());
			AccountCurrency accountCurrency = account.getAccountCurrency(responseMap.get("CURRENCY_CODE"));	
			//Get key
			merchantKey = accountCurrency.getTxnKey();
			requestMap.put(FieldType.MERCHANT_ID.getName(), accountCurrency.getMerchantId());
			requestMap.put(FieldType.TXN_KEY.getName(), merchantKey);
		   logger.info("Null Txn_Key :" + merchantKey);
		}else {
			requestMap.put(FieldType.MERCHANT_ID.getName(), responseMap.get("PG_GATEWAY"));
			requestMap.put(FieldType.TXN_KEY.getName(), responseMap.get("ACQ_ID"));
		}
		requestMap.put(FieldType.PG_REF_NUM.getName(), responseMap.get("PG_REF_NUM"));
		return requestMap;
	}

}
