package com.kbn.safexpay;

public class Constants {
	public static final String AG_ID = "Paygate";
	public static final String AGID = "ag_id";
	public static final String MERCHANT_ID = "me_id";
	public static final String PG_REFENCE = "ag_ref";
	public static final String ORDER_NO = "order_no";
	public static final String REFUND_AMOUNT = "refund_amount";
	public static final String REFUND_REASON = "refund_reason";

	public static final String Country = "IND";
	public static final String Channel  = "WEB";
	//public static final String pg_id ="107";//test
	//public static final String pg_id ="602";
	public static final String pg_id_NetBanking ="63";
	public static final String is_logged_in ="Y";
	public static final String EQUATOR = "=";
	public static final String COLON = ":";
	public static final String SEPARATOR = "|";
	public static final String OPENING_BRACE = "{";
	public static final String CLOSING_BRACE = "}";
	public static final String OPENING_SQUARE_BRACE = "[";
	public static final String CLOSING_SQUARE_BRACE = "]";
	
	public static final String ship_days ="";
	public static final String address_count ="";
	public static final String item_count = "";	
	public static final String item_value = "";
	public static final String item_category = "";
	
	public static final String udf_1 = "";	
	public static final String udf_2 = "";
	public static final String udf_3 = "";
	public static final String udf_4 = "";
	public static final String udf_5 = "";

	

}
