package com.kbn.safexpay;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class SafexPayRefundTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(SafexPayRefundTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createRefundTransaction(fields);
		logger.info("Request to SafexPay: " + request);
		String response = communicator.Refundtransact(request, ConfigurationConstants.SAFEXPAY_REFUND_URL.getValue());
		SafexPayRefundTransformer transformer = new SafexPayRefundTransformer(response);
		transformer.updateSafexPayRefundResponse(fields);
	}

}
