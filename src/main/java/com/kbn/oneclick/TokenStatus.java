package com.kbn.oneclick;

/**
 * @author Surender
 *
 */
public enum TokenStatus {
	ACTIVE,
	EXPIRED,
	INACTIVE
}
