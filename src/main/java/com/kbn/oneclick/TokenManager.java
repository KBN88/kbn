package com.kbn.oneclick;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

/**
 * @author Sunil
 *
 */
public class TokenManager {
	TokenDao tokenDao = new TokenDao();
	private static Logger logger = Logger.getLogger(TokenManager.class
			.getName());
	//Add new token
	public Token addToken(Fields fields) throws SystemException{
		logger.info("Adding new token");
		Token token = TokenFactory.instance(fields);

		String encryptedCard = token.getCardNumber();
		String payID = token.getPayId();
		String emailID = token.getEmail();
		Token tokenFromDB = tokenDao.getCardNumber(encryptedCard, payID, emailID);
		if(tokenFromDB == null ){
			tokenDao.create(token);
			logger.info("New token added successfully");
		}
		else{
			logger.info("Token exist for the same card");
		}
		return token;
	}

	// Fetching card for the Express payment page which already stored
	public void fetchCard(Fields fields) throws SystemException{
		// Fetching Save card
		Token token = TokenFactory.instance(fields);

		String payID = token.getPayId();
		String emailID = token.getEmail();
		tokenDao.getAll(payID, emailID);
	}

	// Remove Save card for given tokenID
	public void removeSavedCard(Fields fields){
		try {
			Token token = TokenFactory.instanceDelete(fields);
			logger.info("Removing token with Id = " + token.getId() + "and EmailID = " + token.getEmail());
			tokenDao.delete(token);
		} catch (SystemException exception) {
			logger.error("Exception", exception);
		}
	}
	
	//Remove all tokens matching the cardHash
	public int removeTokensForCard(Fields fields){
		logger.info("Removing all tokens for card");

		return 0;
	}

	public Map<String, Object> getAll(Fields fields){
		logger.info("Get all tokens with email = " + fields.get(FieldType.CUST_EMAIL.getName()) + "and PayId =" + fields.get(FieldType.PAY_ID.getName()));
		Map<String, Object> token = tokenDao.getAll(fields.get(FieldType.PAY_ID.getName()), fields.get(FieldType.CUST_EMAIL.getName()));

		return token;
	}

	public Map<String, String> getToken(Fields fields){
		logger.info("Get Token with ID = " + fields.get(FieldType.TOKEN_ID.getName()));
		
		Map<String, String> requestMap = new HashMap<String, String>();
		try {
			requestMap = tokenDao.getToken(fields.get(FieldType.TOKEN_ID.getName()));
			requestMap = TokenFactory.dcrypt(fields, requestMap);
		} catch (SystemException exception) {
			logger.error("Exception", exception);
		}
		return requestMap;

	}
	public TokenDao getTokenDao() {
		return tokenDao;
	}

	public void setTokenDao(TokenDao tokenDao) {
		this.tokenDao = tokenDao;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		TokenManager.logger = logger;
	}
}
