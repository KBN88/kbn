package com.kbn.oneclick;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class Token implements Serializable {

	private static final long serialVersionUID = -7871360544517257253L;
	
	@Id
	private String id;
	private String payId;
	private String mopType;
	private String paymentType;
	@Enumerated(EnumType.STRING)
	private TokenStatus status;
	private String customerName;
	private String email;
	private String cardMask;
	private String cardNumber;
	private String expiryDate;
	private String keyId;
	private String cardIssuerCountry;
	private String cardIssuerBank;


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getMopType() {
		return mopType;
	}
	public void setMopType(String mopType) {
		this.mopType = mopType;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCardMask() {
		return cardMask;
	}
	public void setCardMask(String cardMask) {
		this.cardMask = cardMask;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TokenStatus getStatus() {
		return status;
	}
	public void setStatus(TokenStatus status) {
		this.status = status;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getKeyId() {
		return keyId;
	}
	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}
	public String getCardIssuerCountry() {
		return cardIssuerCountry;
	}
	public void setCardIssuerCountry(String cardIssuerCountry) {
		this.cardIssuerCountry = cardIssuerCountry;
	}
	public String getCardIssuerBank() {
		return cardIssuerBank;
	}
	public void setCardIssuerBank(String cardIssuerBank) {
		this.cardIssuerBank = cardIssuerBank;
	}
}// Token
