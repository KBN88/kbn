package com.kbn.oneclick;

import java.util.Map;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.crypto.KeyProvider;
import com.kbn.commons.crypto.KeyProviderFactory;
import com.kbn.commons.crypto.Scrambler;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionManager;

/**
 * @author Sunil
 *
 */
public class TokenFactory {
	private static CryptoManager cryptoManager = CryptoManagerFactory.getCryptoManager();
	private static final KeyProvider keyProvider = KeyProviderFactory.getKeyProvider();
	
	public static Token instance(Fields fields) throws SystemException{
		Token token = new Token();
		token.setId(TransactionManager.getNewTransactionId());
		token.setMopType(fields.get(FieldType.MOP_TYPE.getName()));
		token.setPaymentType(fields.get(FieldType.PAYMENT_TYPE.getName()));
		token.setCustomerName(fields.get(FieldType.CUST_NAME.getName()));
		token.setEmail(fields.get(FieldType.CUST_EMAIL.getName()));
		token.setStatus(TokenStatus.ACTIVE);
		
		cryptoManager.encryptCardDetails(fields);
		token.setCardNumber(fields.get(FieldType.S_CARD_NUMBER.getName()));
		token.setExpiryDate(fields.get(FieldType.S_CARD_EXP_DT.getName()));
		token.setCardMask(cryptoManager.maskCardNumber(fields.get(FieldType.CARD_NUMBER.getName())));
		cryptoManager.hashCardDetails(fields);
		token.setKeyId(fields.get(FieldType.KEY_ID.getName()));
		token.setPayId(fields.get(FieldType.PAY_ID.getName()));
		token.setCardIssuerBank(fields.get(FieldType.INTERNAL_CARD_ISSUER_BANK.getName()));
		token.setCardIssuerCountry(fields.get(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()));
		
		return token;
	}

	public static Token instanceDelete(Fields fields) throws SystemException{
		Token token = new Token();
		
		token.setEmail(fields.get(FieldType.CUST_EMAIL.getName()));
		token.setPayId(fields.get(FieldType.PAY_ID.getName()));
		token.setId(fields.get(FieldType.TOKEN_ID.getName()));
		
		return token;
	}
	
	public static Map<String, String> dcrypt(Fields fields, Map<String, String> requestMap) throws SystemException{
		
		String key = keyProvider.getKey(fields);
		Scrambler scrambler = new Scrambler(key);
		String pan = scrambler.decrypt(requestMap.get(FieldType.CARD_NUMBER.getName()));
		String expDate = scrambler.decrypt(requestMap.get(FieldType.CARD_EXP_DT.getName()));
		requestMap.put(FieldType.CARD_NUMBER.getName(),pan);
		requestMap.put(FieldType.CARD_EXP_DT.getName(),expDate);
		requestMap.put(FieldType.MOP_TYPE.getName(),requestMap.get(FieldType.MOP_TYPE.getName()));
		requestMap.put(FieldType.PAYMENT_TYPE.getName(),requestMap.get(FieldType.PAYMENT_TYPE.getName()));
		return requestMap;
	}
	public static CryptoManager getCryptoManager() {
		return cryptoManager;
	}

	public static void setCryptoManager(CryptoManager cryptoManager_) {
		cryptoManager = cryptoManager_;
	}
}
