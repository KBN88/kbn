package com.kbn.mobikwik;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Amount;

public class MobikwikUtil {
	private static Logger logger = Logger.getLogger(MobikwikUtil.class
			.getName());

	public static String calculateChecksum(String secretKey, String allParamValue) throws SystemException{
		byte[] dataToEncryptByte = allParamValue.getBytes();
		byte[] keyBytes = secretKey.getBytes();
		SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "HmacSHA256");
		Mac mac;
		try {
			mac = Mac.getInstance("HmacSHA256");
			mac.init(secretKeySpec);
		} catch (NoSuchAlgorithmException | InvalidKeyException exception) {
			logger.error("Error calculating mobilwik hash " + exception);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
		}
		byte[] checksumByte = mac.doFinal(dataToEncryptByte);
		String checksum = toHex(checksumByte);
		return checksum;
	}

	public static boolean verifyChecksum(String secretKey, String allParamVauleExceptChecksum, String checksumReceived)
			throws SystemException {

		String checksumCalculated = calculateChecksum(secretKey, allParamVauleExceptChecksum);

		if (checksumReceived.equals(checksumCalculated)) {
			return true;
		}
		return false;
	}

	public static String toHex(byte[] bytes) {
		StringBuilder buffer = new StringBuilder(bytes.length * 2);

		byte[] arrayOfByte = bytes;
		int j = bytes.length;
		for (int i = 0; i < j; i++) {
			Byte b = Byte.valueOf(arrayOfByte[i]);
			String str = Integer.toHexString(b.byteValue());
			int len = str.length();
			if (len == 8) {
				buffer.append(str.substring(6));
			} else if (str.length() == 2) {
				buffer.append(str);
			} else {
				buffer.append("0" + str);
			}
		}
		return buffer.toString();
	}
	
	public static String prepareCheckSumStringRequest(Fields fields){

		StringBuilder checkSumString = new StringBuilder();
		checkSumString.append(Constants.SINGLE_QUOTE);
		checkSumString.append(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),fields.get(FieldType.CURRENCY_CODE.getName())));
		checkSumString.append(Constants.SINGLE_QUOTE);
		checkSumString.append(Constants.SINGLE_QUOTE);
		checkSumString.append(fields.get(FieldType.TXN_ID.getName()));
		checkSumString.append(Constants.SINGLE_QUOTE);
		checkSumString.append(Constants.SINGLE_QUOTE);
		checkSumString.append(ConfigurationConstants.MOBIKWIK_RETURN_URL.getValue());
		checkSumString.append(Constants.SINGLE_QUOTE);
		checkSumString.append(Constants.SINGLE_QUOTE);
		checkSumString.append(fields.get(FieldType.MERCHANT_ID.getName()));
		checkSumString.append(Constants.SINGLE_QUOTE);

		return checkSumString.toString();
	}

	public static String prepareCheckSumStringResponse(Map<String,String> responseMap){
		StringBuilder checksumString = new StringBuilder();
		checksumString.append(Constants.SINGLE_QUOTE);
		checksumString.append(responseMap.get(Constants.RESPCODE));
		checksumString.append(Constants.SINGLE_QUOTE + Constants.SINGLE_QUOTE);
		checksumString.append(responseMap.get(Constants.ORDERID));
		checksumString.append(Constants.SINGLE_QUOTE + Constants.SINGLE_QUOTE);
		checksumString.append(responseMap.get(Constants.TXNAMOUNT));
		checksumString.append(Constants.SINGLE_QUOTE + Constants.SINGLE_QUOTE);
		checksumString.append(responseMap.get(Constants.RESPMSG));
		checksumString.append(Constants.SINGLE_QUOTE + Constants.SINGLE_QUOTE);
		checksumString.append(responseMap.get(Constants.MID));
		checksumString.append(Constants.SINGLE_QUOTE);
		if (!StringUtils.isEmpty(responseMap.get(Constants.BANKTXNID))) {
			checksumString.append(Constants.SINGLE_QUOTE);
			checksumString.append(responseMap.get(Constants.BANKTXNID));
			checksumString.append(Constants.SINGLE_QUOTE);
		}
		return checksumString.toString();
	}
}
