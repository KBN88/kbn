package com.kbn.mobikwik;

/**
 * @author Sunil
 *
 */
public enum MobikwikResultType {
	SUCCESS                   ("0"), 
	DECLINED                  ("1"),
	CANCELLED_AT_LOGIN_PAGE   ("40"),
	CANCELLED_AT_TOPUP_PAGE   ("41"),
	CANCELLED_AT_DEBIT_PAGE   ("42");

	private MobikwikResultType(String code) {
		this.code = code;
	}

	private final String code;

	public String getCode() {
		return code;
	}

	public static MobikwikResultType getInstance(String code) {

		MobikwikResultType[] resultTypes = MobikwikResultType.values();
		for (MobikwikResultType resultType : resultTypes) {
			if (resultType.getCode().equals(code)) {
				return resultType;
			}
		}
		// Return null if unexpected value is returned in parameter "respCode"
		return DECLINED;
	}
}
