package com.kbn.mobikwik;
import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

/**
 * @author Sunil
 *
 */
public class MobikwikTransformer {
	private Response response;

	public MobikwikTransformer(Response response){
		this.response = response;
	}

	public void updateResponse(Fields fields){
		ErrorType errorType = getResponseCode();
		StatusType status = getStatus();

		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.RRN.getName(),response.getPgTxnId());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getResponseCode());
		if(StringUtils.isEmpty(response.getStatusmessage())){
			fields.put(FieldType.PG_TXN_MESSAGE.getName(), response.getStatus());
		}else{
			fields.put(FieldType.PG_TXN_MESSAGE.getName(), response.getStatusmessage());
		}
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), response.getStatusmessage());
		fields.put(FieldType.PG_RESP_CODE.getName(), response.getStatusCode());
	}

	public ErrorType getResponseCode(){
		String respCode = response.getStatusCode();
		ErrorType errorType = null;

		if(null == respCode){
			errorType = ErrorType.ACQUIRER_ERROR;
			return errorType;
		}

		MobikwikResultType resultType = MobikwikResultType.getInstance(respCode);
		switch(resultType){
		case CANCELLED_AT_DEBIT_PAGE:
		case CANCELLED_AT_LOGIN_PAGE:
		case CANCELLED_AT_TOPUP_PAGE:
			errorType = ErrorType.CANCELLED;
			break;
		case SUCCESS:
			errorType = ErrorType.SUCCESS;
			break;
		case DECLINED:
			errorType = ErrorType.DECLINED;
			break;
		default:
			errorType = ErrorType.DECLINED;
			break;
		}
		return errorType;
	}

	public StatusType getStatus() {
		String respCode = response.getStatusCode();
		StatusType statusType = null;
		if (null == respCode) {
			statusType = StatusType.ERROR;
			return statusType;
		}

		MobikwikResultType resultType = MobikwikResultType.getInstance(respCode);

		switch(resultType){
		case CANCELLED_AT_DEBIT_PAGE:
		case CANCELLED_AT_LOGIN_PAGE:
		case CANCELLED_AT_TOPUP_PAGE:
			statusType = StatusType.CANCELLED;
			break;
		case SUCCESS:
			statusType = StatusType.CAPTURED;
			break;
		case DECLINED:
			statusType = StatusType.DECLINED;
			break;
		default:
			statusType = StatusType.DECLINED;
			break;
		}
		return statusType;
	}
}
