package com.kbn.mobikwik;

import java.util.Map;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;

/**
 * @author Sunil
 *
 */
public class ResponseProcessor implements Processor {

	public void compareHash(Fields fields, Map<String,String> responseMap) throws SystemException{

		String mobikwikKey = fields.get(FieldType.PASSWORD.getName());		
		String checksumString = MobikwikUtil.prepareCheckSumStringResponse(responseMap);

		String checksumReceived = responseMap.get(Constants.CHECKSUMHASH);

		if (!MobikwikUtil.verifyChecksum(mobikwikKey, checksumString, checksumReceived)){
			fields.put(FieldType.STATUS.getName(), StatusType.ERROR.getName());
			fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(),	TransactionType.SALE.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SIGNATURE_MISMATCH.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SIGNATURE_MISMATCH.getResponseMessage());			
			fields.updateNewOrderDetails();
			fields.updateTransactionDetails();
			new ResponseCreator().create(fields);
			throw new SystemException(ErrorType.SIGNATURE_MISMATCH,ErrorType.SIGNATURE_MISMATCH.getResponseMessage());
		}
	}

	public void preProcess(Fields fields) throws SystemException {

	}

	public void process(Fields fields) throws SystemException {
		fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(),fields.get(FieldType.TXNTYPE.getName()));

		if(null != fields.get(FieldType.TXN_ID.getName())){
			fields.updateNewOrderDetails();
			fields.updateTransactionDetails();
		}
	}

	public void postProcess(Fields fields) throws SystemException {
		fields.remove(FieldType.HASH.getName());
		new ResponseCreator().create(fields);
	}
}
