package com.kbn.mobikwik;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Amount;


/**
 * @author Puneet
 *
 */
public class TransactionConverter {

	public static final String WALLET_OPEN_TAG =       "<wallet>";
	public static final String WALLET_CLOSE_TAG =      "</wallet>";
	public static final String STATUS_OPEN_TAG =       "<status>";
	public static final String STATUS_CLOSE_TAG =      "</status>";
	public static final String STATUS_CODE_OPEN_TAG =  "<statuscode>";
	public static final String STATUS_CODE_CLOSE_TAG = "</statuscode>";
	public static final String TXN_ID_OPEN_TAG =       "<txid>";
	public static final String TXN_ID_CLOSE_TAG =      "</txid>";
	public static final String BANK_TXN_ID_OPEN_TAG =  "<refId>";
	public static final String BANK_TXN_ID_CLOSE_TAG = "</refId>";	
	public static final String STATUS_MSG_OPEN_TAG =   "<statusmessage>";
	public static final String STATUS_MSG_CLOSE_TAG =  "</statusmessage>";
	public static final String AMOUNT_START_TAG =      "<amount>";
	public static final String AMOUNT_CLOSE_TAG =      "</amount>";
	public static final String ORDER_TYPE_START_TAG =  "<ordertype>";
	public static final String ORDER_TYPE_CLOSE_TAG =  "</ordertype>";
	public static final String CHECKSUM_OPEN_TAG =     "<checksum>";
	public static final String CHECKSUM_CLOSE_TAG =    "</checksum>";

	public Response toTransaction(String xml){
		xml = getTextBetweenTags(xml,WALLET_OPEN_TAG,WALLET_CLOSE_TAG);
		Response response = new Response();
		response.setStatusCode(getTextBetweenTags(xml,STATUS_CODE_OPEN_TAG,STATUS_CODE_CLOSE_TAG));
		response.setTxnId(getTextBetweenTags(xml,TXN_ID_OPEN_TAG,TXN_ID_CLOSE_TAG));
		response.setPgTxnId(getTextBetweenTags(xml,BANK_TXN_ID_OPEN_TAG,BANK_TXN_ID_CLOSE_TAG));
		response.setStatus(getTextBetweenTags(xml,STATUS_OPEN_TAG,STATUS_CLOSE_TAG));		
		response.setStatusmessage(getTextBetweenTags(xml,STATUS_MSG_OPEN_TAG,STATUS_MSG_CLOSE_TAG));
		response.setAmount(getTextBetweenTags(xml,AMOUNT_START_TAG,AMOUNT_CLOSE_TAG));
		response.setChecksum(getTextBetweenTags(xml,CHECKSUM_OPEN_TAG,CHECKSUM_CLOSE_TAG));
		return response;
	}

	public String createSaleTransaction(Fields fields) throws SystemException {

		// TODO... eliminate DB call
		User user = new UserDao().findPayId(fields.get(FieldType.PAY_ID
				.getName()));

		String requestUrl = ConfigurationConstants.MOBIKWIK_TRANSACTION_URL
				.getValue();
		String amount = Amount.toDecimal(
				fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));
		String checkSumString = MobikwikUtil
				.prepareCheckSumStringRequest(fields);

		// Request
		StringBuilder request = new StringBuilder();
		request.append("<HTML>");
		request.append("<BODY OnLoad=\"OnLoadEvent();\" >");
		request.append("<form name=\"form1\" action=\"");
		request.append(requestUrl);
		request.append("\" method=\"post\">");
		request.append("<input type=\"hidden\" name=\"amount\" value=\"");
		request.append(amount);
		request.append("\">");
		request.append("<input type=\"hidden\" name=\"orderid\" value=\"");
		request.append(fields.get(FieldType.TXN_ID.getName()));
		request.append("\">");
		request.append("<input type=\"hidden\" name=\"mid\" value=\"");
		request.append(fields.get(FieldType.MERCHANT_ID.getName()));
		request.append("\">");
		request.append("<input type=\"hidden\" name=\"merchantname\" value=\"");
		request.append(user.getBusinessName());
		request.append("\">");
		request.append("<input type=\"hidden\" name=\"redirecturl\" value=\"");
		request.append(ConfigurationConstants.MOBIKWIK_RETURN_URL.getValue());
		request.append("\">");
		request.append("<input type=\"hidden\" name=\"showmobile\" value=\"");
		request.append(ConfigurationConstants.MOBIKWIK_SHOWMOBILE.getValue());
		request.append("\">");
		request.append("<input type=\"hidden\" name=\"version\" value=\"");
		request.append(ConfigurationConstants.MOBIKWIK_VERSION.getValue());
		request.append("\">");
		request.append("<input type=\"hidden\" name=\"checksum\" value=\"");

		request.append(MobikwikUtil.calculateChecksum(
				fields.get(FieldType.PASSWORD.getName()), checkSumString));

		request.append("\">");
		request.append("</form>");
		request.append("<script language=\"JavaScript\">");
		request.append("function OnLoadEvent()");
		request.append("{document.form1.submit();}");
		request.append("</script>");
		request.append("</BODY>");
		request.append("</HTML>");

		return request.toString();
	}

	public void prepareFieldsForRefund(Fields fields) throws SystemException{

	    StringBuilder hashString = new StringBuilder();
	    hashString.append(Constants.SINGLE_QUOTE);
	    hashString.append(fields.get(FieldType.MERCHANT_ID.getName()));
	    hashString.append(Constants.SINGLE_QUOTE);
	    hashString.append(Constants.SINGLE_QUOTE);
	    hashString.append(fields.get(FieldType.ORIG_TXN_ID.getName()));
	    hashString.append(Constants.SINGLE_QUOTE);
	    hashString.append(Constants.SINGLE_QUOTE);
	    hashString.append(Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),fields.get(FieldType.CURRENCY_CODE.getName())));	 
	    hashString.append(Constants.SINGLE_QUOTE);

	   String hash = MobikwikUtil.calculateChecksum(fields.get(FieldType.PASSWORD.getName()), hashString.toString());
	   fields.put(FieldType.HASH.getName(),hash);		
	}

	public void prepareFieldsForStatus(Fields fields) throws SystemException{

	    StringBuilder hashString = new StringBuilder();
	    hashString.append(Constants.SINGLE_QUOTE);
	    hashString.append(fields.get(FieldType.MERCHANT_ID.getName()));
	    hashString.append(Constants.SINGLE_QUOTE);
	    hashString.append(Constants.SINGLE_QUOTE);
	    hashString.append(fields.get(FieldType.ORIG_TXN_ID.getName()));
	    hashString.append(Constants.SINGLE_QUOTE);

	   String hash = MobikwikUtil.calculateChecksum(fields.get(FieldType.PASSWORD.getName()), hashString.toString());
	   fields.put(FieldType.HASH.getName(),hash);		
	}

	public String getTextBetweenTags(String text, String tag1, String tag2) {

		int leftIndex = text.indexOf(tag1);
		if(leftIndex == -1){
			return null;
		}
		int rightIndex = text.indexOf(tag2);
		if (rightIndex != -1) {
			leftIndex = leftIndex + tag1.length();
			return text.substring(leftIndex, rightIndex);
		}
		return null;
	}//getTextBetweenTags()
}