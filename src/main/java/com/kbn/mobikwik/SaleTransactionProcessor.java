package com.kbn.mobikwik;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class SaleTransactionProcessor implements TransactionProcessor {

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();		
		TransactionCommunicator communicator = new TransactionCommunicator();

		String request = converter.createSaleTransaction(fields);
		communicator.sendAuthorization(request, fields);
	}
}
