package com.kbn.mobikwik;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.Amount;

public class TransactionCommunicator {

	private static Logger logger = Logger.getLogger(TransactionCommunicator.class
			.getName());

	public void sendAuthorization(String request, Fields fields) throws SystemException{
		PrintWriter out;
		logger.info("Request sent to mobikwik: " + request); 
		try {
			out = ServletActionContext.getResponse().getWriter();

			out.write(request);
			//Set response
			fields.put(FieldType.RESPONSE_CODE.getName(),ErrorType.SUCCESS.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.SUCCESS.getResponseMessage());
			fields.put(FieldType.STATUS.getName(), StatusType.SENT_TO_BANK.getName());

		} catch (IOException iOException) {
			logger.error(iOException);
			fields.put(FieldType.RESPONSE_CODE.getName(),ErrorType.INTERNAL_SYSTEM_ERROR.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
			fields.put(FieldType.STATUS.getName(),StatusType.ERROR.getName());
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					iOException, "Network Exception with Mobikwik");
		}
	}

	public String transactRefund(Fields fields, String hostUrl)
			throws SystemException {
		StringBuilder requestParams = new StringBuilder();

		PostMethod postMethod = new PostMethod(hostUrl);
		postMethod.addParameter(Constants.MID,
				fields.get(FieldType.MERCHANT_ID.getName()));
		postMethod.addParameter(Constants.TXNID,
				fields.get(FieldType.ORIG_TXN_ID.getName()));
		postMethod.addParameter(Constants.AMOUNT,
				Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
						fields.get(FieldType.CURRENCY_CODE.getName())));
		postMethod.addParameter(Constants.CHECKSUMHASH,
				fields.get(FieldType.HASH.getName()));

		NameValuePair[] requestFields = postMethod.getParameters();
		for (NameValuePair nameValuePair : requestFields) {
			requestParams.append(nameValuePair.toString());
			requestParams.append(Constants.AND_SEPRATOR);
		}

		logger.info("Request parameters to mobikwik for refund"
				+ requestParams.toString());
		return transact(postMethod, hostUrl);
	}

	public String transactStatus(Fields fields, String hostUrl)
			throws SystemException {

		StringBuilder getUrl = new StringBuilder(hostUrl);
		getUrl.append(Constants.QUESTION_MARK);
		getUrl.append(Constants.MID);
		getUrl.append(Constants.EQUATOR);
		getUrl.append(fields.get(FieldType.MERCHANT_ID.getName()));
		getUrl.append(Constants.AND_SEPRATOR);
		getUrl.append(Constants.ORDERID);
		getUrl.append(Constants.EQUATOR);
		getUrl.append(fields.get(FieldType.ORIG_TXN_ID.getName()));
		getUrl.append(Constants.AND_SEPRATOR);
		getUrl.append(Constants.CHECKSUMHASH);
		getUrl.append(Constants.EQUATOR);
		getUrl.append(fields.get(FieldType.HASH.getName()));

		GetMethod getMethod = new GetMethod(getUrl.toString());
		logger.info("Get request to mobikwik url: " + getUrl.toString());

		return transact(getMethod,hostUrl);
	}

//HttpMethodBase
	public String transact(HttpMethod httpMethod, String hostUrl)
			throws SystemException {
		String response = "";

		try {
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(httpMethod);

			if (httpMethod.getStatusCode() == HttpStatus.SC_OK) {
				response = httpMethod.getResponseBodyAsString();
				logger.info("Response from mobikwik: " + response);
			} else {
				throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
						"Network Exception with Mobikwik " + hostUrl.toString()
								+ "recieved response code"
								+ httpMethod.getStatusCode());
			}
		} catch (IOException ioException) {
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					ioException, "Network Exception with Mobikwik "
							+ hostUrl.toString());
		}
		logger.info("Response message from mobikwik: " + response);
		return response;
	}
}
