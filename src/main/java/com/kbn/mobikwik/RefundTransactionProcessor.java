package com.kbn.mobikwik;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

/**
 * @author Puneet
 *
 */
public class RefundTransactionProcessor implements TransactionProcessor{

	@Override
	public void transact(Fields fields) throws SystemException {

		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();

		converter.prepareFieldsForRefund(fields);
		String response = communicator.transactRefund(fields, ConfigurationConstants.MOBIKWIK_REFUND_URL.getValue());
		Response responseObject = converter.toTransaction(response);

		MobikwikTransformer transformer = new MobikwikTransformer(responseObject);
		transformer.updateResponse(fields);
	}

}
