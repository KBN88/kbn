package com.kbn.mobikwik;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class StatusTransactionProcessor implements TransactionProcessor{

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();

		converter.prepareFieldsForStatus(fields);
		String response = communicator.transactStatus(fields, ConfigurationConstants.MOBIKWIK_STATUS_ENQUIRY_URL.getValue());
		Response responseObject = converter.toTransaction(response);

		MobikwikTransformer transformer = new MobikwikTransformer(responseObject);
		transformer.updateResponse(fields);
	}
}
