package com.kbn.mobikwik;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.Amount;

/**
 * @author Sunil
 *
 */
public class ResponseFactory {
	private static Logger logger = Logger.getLogger(ResponseFactory.class
			.getName());

	public static Response createResponse(String responseString)
			throws SystemException {

		Response response = new Response();
		
		response.setStatusCode("000");

		return response;
	}

	public static Map<String, String> parseAuthResponse(Map<String, String> request)  {
		logger.info("Response from mobikwik: " + request); 
		Map<String, String> mapValues = new HashMap<String, String>();
		String pgResponseCode = request.get(Constants.RESPCODE);
		if(pgResponseCode.equals("0")){
			mapValues.put(FieldType.STATUS.getName(), StatusType.CAPTURED.getName());
			mapValues.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getResponseCode());
			mapValues.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
		} else if(pgResponseCode.equals("40") || pgResponseCode.equals("41") || pgResponseCode.equals("42")){
			mapValues.put(FieldType.STATUS.getName(), StatusType.CANCELLED.getName());
			mapValues.put(FieldType.RESPONSE_CODE.getName(), ErrorType.CANCELLED.getResponseCode());
			mapValues.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.CANCELLED.getResponseMessage());
		}else{
			mapValues.put(FieldType.STATUS.getName(), StatusType.DECLINED.getName());
			mapValues.put(FieldType.RESPONSE_CODE.getName(), ErrorType.DECLINED.getResponseCode());
			mapValues.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.DECLINED.getResponseMessage());
		}
		mapValues.put(FieldType.PG_RESP_CODE.getName(), pgResponseCode);
		mapValues.put(FieldType.PG_TXN_MESSAGE.getName(), request.get(Constants.RESPMSG));
		mapValues.put(FieldType.AMOUNT.getName(), Amount.formatAmount(request.get(Constants.TXNAMOUNT),request.get(FieldType.CURRENCY_CODE.getName())));

		if (!StringUtils.isEmpty(request.get(Constants.BANKTXNID))) {
			mapValues.put(FieldType.RRN.getName(),request.get(Constants.BANKTXNID));
		}
		return mapValues;
	}

}
