package com.kbn.mobikwik;

import com.kbn.pg.core.Processor;

/**
 * @author Sunil
 *
 */
public class MobikwikProcessorFactory {
	public static Processor getInstance(){
		return new MobikwikProcessor();
	}
}