package com.kbn.mobikwik;

/**
 * @author Sunil
 *
 */
public class Constants {
	public static final String MID = "mid";	
	public static final String TXNID = "txid";
	public static final String ORDERID = "orderid";
	public static final String RESPCODE = "statuscode";
	public static final String RESPMSG = "statusmessage";
	public static final String BANKTXNID = "refid";
	public static final String TXNAMOUNT = "amount";
	public static final String CHECKSUMHASH = "checksum";
	public static final String STATUS = "status";
	public static final String AMOUNT = "amount";
	public static final String REFUNDAMOUNT = "REFUNDAMOUNT";
	public static final String TXNTYPE = "TXNTYPE";
	public static final String COMMENTS = "COMMENTS";
	public static final String TXNDATE = "TXNDATE";
	public static final String GATEWAYNAME = "GATEWAYNAME";
	public static final String BANKNAME = "BANKNAME";
	public static final String PAYMENTMODE = "PAYMENTMODE";
	public static final String SINGLE_QUOTE = "'";
	public static final String EQUATOR = "=";
	public static final String AND_SEPRATOR = "&";
	public static final String QUESTION_MARK = "?";
}
