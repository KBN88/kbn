package com.kbn.pg.history;

import com.kbn.pg.core.Processor;

public class HistoryProcessorFactory {

	public HistoryProcessorFactory() {
	}

	public static Processor getInstance(){
		return new HistoryProcessor();
	}
}
