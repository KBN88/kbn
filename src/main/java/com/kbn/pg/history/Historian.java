package com.kbn.pg.history;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.Amount;

public class Historian {

	public Historian() {
	}

	public void findPrevious(Fields fields) throws SystemException {
		//Previous check already done, return
		Fields previous = fields.getPrevious();
		if (null != previous && previous.size() > 0) {
			return;
		}
		
		// Check if a previous related transaction exists in system
		String origTxnId = fields.get(FieldType.ORIG_TXN_ID.getName());
		if (null != origTxnId) {
			String txnType = fields.get(FieldType.TXNTYPE.getName());
			if (txnType.equals(TransactionType.REFUND.getName())
					|| txnType.equals(TransactionType.CAPTURE.getName()) || txnType.equals(TransactionType.STATUS.getName())) {
				fields.refreshPrevious();
			}//if
			else if(txnType.equals(TransactionType.SALE.getName())){
				//Add previous for webservice based transactions(without session)
				if(StringUtils.isEmpty(fields.get(FieldType.ACQUIRER_TYPE.getName()))){
					fields.refreshPrevious();
				}
			}//else if
		}// if
	}

	public void populateFieldsFromPrevious(Fields fields) {
		Fields previous = fields.getPrevious();
		if (null != previous && previous.size() > 0) {

			// ORDER_ID in this request is required from previous request, this
			// will allow to link support
			// transactions to link to original transactions
			fields.put(FieldType.ORDER_ID.getName(),
					previous.get(FieldType.ORDER_ID.getName()));

			// Currency Code is required to process amount formating in support
			// transactions
			String currencyCode = previous.get(FieldType.CURRENCY_CODE
					.getName());
			if (null != currencyCode) {
				fields.put(FieldType.CURRENCY_CODE.getName(), currencyCode);
			}

			// Acquirer of original transaction
			String acquirerType = previous.get(FieldType.ACQUIRER_TYPE
					.getName());
			if (null != acquirerType) {
				fields.put(FieldType.ACQUIRER_TYPE.getName(), acquirerType);
			}
			
			//get PG_REF_NO for capture
			String pgRefNo = previous.get(FieldType.PG_REF_NUM.getName());
			if (null != pgRefNo) {
				fields.put(FieldType.PG_REF_NUM.getName(), pgRefNo);
			}
			
			// OID of original transaction
			String oid = previous.get(FieldType.OID.getName());
			if (null != oid) {
				fields.put(FieldType.OID.getName(), oid);
			}
			
			// Acquirer of original transaction
			String acq_id = previous.get(FieldType.ACQ_ID.getName());
			if (null != acq_id) {
				fields.put(FieldType.ACQ_ID.getName(), acq_id);
			}
			
			// Mop type of original transaction
			String mopType = previous.get(FieldType.MOP_TYPE.getName());
			if (null != mopType) {
				fields.put(FieldType.MOP_TYPE.getName(), mopType);
			}
			
			// Payment type of original transaction
			String paymentType = previous.get(FieldType.PAYMENT_TYPE.getName());
			if (null != paymentType) {
				fields.put(FieldType.PAYMENT_TYPE.getName(), paymentType);
			}
			
			// Card mask of original transaction for inserting into sale transaction in case of webservice
			String cardMask = previous.get(FieldType.CARD_MASK.getName());
			if (null != cardMask){
				fields.put(FieldType.CARD_MASK.getName(), cardMask);
			}
			// Date & Time of original transaction
			String pgDateTime = previous.get(FieldType.PG_DATE_TIME.getName());
			if (null != pgDateTime) {
			fields.put(FieldType.PG_DATE_TIME.getName(), pgDateTime);
			}
			//TODO.. use only for yes bank status enquiry
			// Amount of original transaction
			String txnType = fields.get(FieldType.TXNTYPE.getName());
			if (txnType.equals(TransactionType.STATUS.getName())) {
				String amount = previous.get(FieldType.AMOUNT.getName());
				if (null != amount) {
					fields.put(FieldType.AMOUNT.getName(),
							Amount.formatAmount(amount, currencyCode));
				}
			} else {

			}
		}

		}
	

	public void validateSupportTransaction(Fields fields)
			throws SystemException {
		String origTxnId = fields.get(FieldType.ORIG_TXN_ID.getName());
		if (null != origTxnId) {

			String txnType = fields.get(FieldType.TXNTYPE.getName());
			if (txnType.equals(TransactionType.REFUND.getName())
					|| txnType.equals(TransactionType.CAPTURE.getName())) {
				Fields previous = fields.getPrevious();
				if (null == previous || previous.size() < 1) {
					fields.put(FieldType.STATUS.getName(),
							StatusType.REJECTED.getName());
					throw new SystemException(ErrorType.NO_SUCH_TRANSACTION,
							"Previous transaction not found");
				}
			}// if
		}// if
	}

	public void duplicateProcessor(Fields fields) throws SystemException {
		fields.checkDuplicate();
		String duplicate = fields.get(FieldType.DUPLICATE_YN.getName());
		if (null != duplicate && duplicate.equals("Y")) {
			fields.setValid(false);
			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.DUPLICATE.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.DUPLICATE.getResponseMessage());
			fields.put(FieldType.STATUS.getName(),
					StatusType.DUPLICATE.getName());
		}
	}

	public boolean isOrderIdBasedDuplicateAllowed(Fields fields) {
		boolean isAllowed = false;
		String flag = ConfigurationConstants.DUPLICATE_ON_ORDER_ID.getValue();
		if (StringUtils.isEmpty(flag)) {
			isAllowed = true;
		}		

		if (flag.equals("0")) {
			isAllowed = false;
		} else {
			isAllowed = true;
		}
		
		//If configuration based check is done
		if(!isAllowed){
			String duplicateYn = fields.get(FieldType.DUPLICATE_YN.getName());
			if(null != duplicateYn && duplicateYn.equals("Y")){
				isAllowed = true;
			}
		}
		
		return isAllowed;
	}

	public void detectDuplicate(Fields fields) throws SystemException {
		if (!isOrderIdBasedDuplicateAllowed(fields)) {
			return;
		}

		TransactionType transactionType = TransactionType.getInstance(fields
				.get(FieldType.TXNTYPE.getName()));
		switch (transactionType) {
		case AUTHORISE:
			duplicateProcessor(fields);
			break;
		case SALE:
			duplicateProcessor(fields);
			break;
		case NEWORDER:
			duplicateProcessor(fields);
			break;
		default:
			break;
		}
	}
}
