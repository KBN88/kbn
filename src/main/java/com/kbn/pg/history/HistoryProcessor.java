package com.kbn.pg.history;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.Processor;

public class HistoryProcessor implements Processor {

	private Historian historian = new Historian();
	
	public HistoryProcessor() {
	}
	
	public void preProcess(Fields fields) throws SystemException {
		historian.findPrevious(fields);
	}//preProcess()

	public void process(Fields fields) throws SystemException {
		historian.populateFieldsFromPrevious(fields);
		
		historian.validateSupportTransaction(fields);
		
		//Check duplicate authorization transaction
		historian.detectDuplicate(fields);
	}

	public void postProcess(Fields fields) {
		if(fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.NEWORDER.getName())
				&& fields.get(FieldType.STATUS.getName()).equals(StatusType.PENDING.getName())){
			String responseCode = fields.get(FieldType.RESPONSE_CODE.getName());
			if(null == responseCode){
				fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getCode());
				fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
			}
		}
	}
}
