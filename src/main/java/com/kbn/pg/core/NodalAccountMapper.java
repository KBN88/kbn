package com.kbn.pg.core;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;

public enum NodalAccountMapper {
	
	// Credit Card Acquirer
	AMEX        ("AMEX", "AMERICAN EXPRESS"),
	CITRUS_PAY  ("CITRUS", "Yes Bank"),
	FSS	        ("FSS", "HDFC Bank"),
	DIREC_PAY   ("DIRECPAY", "DIRECPAY"),
	MOBIKWIK    ("MOBIKWIK", "WALLET MOBIKWIK");
	private final String code;
	private final String name;
	
	private NodalAccountMapper(String code, String name){
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
  
	public static NodalAccountMapper getInstancefromCode(String acquirerCode){
		NodalAccountMapper acquirerType = null;
		for (NodalAccountMapper nodalAccountMapper : NodalAccountMapper.values()) {
			if(acquirerCode.equals(nodalAccountMapper.getCode())){
				acquirerType=nodalAccountMapper;
				break;
			}
		}
		return acquirerType;
		
	}
	
	public static NodalAccountMapper getInstancefromName(String acquirerName){
		NodalAccountMapper acquirerType = null;
		for (NodalAccountMapper nodalAccountMapper : NodalAccountMapper.values()) {
			if(acquirerName.equals(nodalAccountMapper.getName())){
				acquirerType=nodalAccountMapper;
				break;
			}
		}
		return acquirerType;
		
	}
	
	public static NodalAccountMapper getDefault(Fields fields) throws SystemException{
		User user = new User();
		UserDao userDao = new UserDao();
		user = userDao.findPayId(fields.get(FieldType.PAY_ID.getName()));

		return getAcquirer(fields.getFields(),user);
	}

	public static NodalAccountMapper getDefault(Fields fields, User user){
		return getAcquirer(fields.getFields(), user);
	}

	private static NodalAccountMapper getAcquirer(Map<String, String> fields,
			User user) {
		String acquirerName = "";
		PropertiesManager propertiesManager = new PropertiesManager();
		PaymentType paymentType = PaymentType.getInstanceUsingCode(fields.get(FieldType.PAYMENT_TYPE.getName()));
		String mopType = fields.get(FieldType.MOP_TYPE.getName());
		if(StringUtils.isEmpty(fields.get(FieldType.PAYMENT_TYPE.getName())) || StringUtils.isEmpty(mopType)){
			return null;
		}
		switch (paymentType){
		case CREDIT_CARD:			
			if(mopType.equals(MopType.AMEX.getCode())){
				acquirerName = AcquirerType.AMEX.getName();
			}else if(mopType.equals(MopType.EZEECLICK.getCode())){
				acquirerName = AcquirerType.EZEECLICK.getName();
			}else if(mopType.equals(MopType.DINERS.getCode())){
				acquirerName = AcquirerType.FSS.getName();
			}else{
				acquirerName = user.getPrimaryAccount();
				if(null == acquirerName || acquirerName.isEmpty()){
					acquirerName = AcquirerType.FSS.getName();
				}
			}
			break;
		case DEBIT_CARD:
			acquirerName = user.getPrimaryAccount();
			if(null == acquirerName || acquirerName.isEmpty()){
				acquirerName = AcquirerType.FSS.getName();
			}
		break;
	case NET_BANKING:				
		if(mopType.equals(MopType.YES_BANK.getCode())){
				acquirerName = AcquirerType.YESBANK.getName();	
		}else if(mopType.equals(MopType.KOTAK_BANK.getCode())){
					acquirerName = AcquirerType.KOTAK.getName();			
		}else if(user.getNetbankingPrimaryAccount().equals(AcquirerType.CITRUS_PAY.getName())){
			if(propertiesManager.getAcquirerMopType(Constants.CITRUS_NETBANKING_SUPPORTED_BANK.getValue()).contains(mopType)){
				acquirerName = AcquirerType.CITRUS_PAY.getName();
			}else{
				acquirerName = AcquirerType.DIREC_PAY.getName();
			}
		}else if(user.getNetbankingPrimaryAccount().equals(AcquirerType.DIREC_PAY.getName())){
			if(propertiesManager.getAcquirerMopType(Constants.DIRECPAY_NETBANKING_SUPPORTED_BANK.getValue()).contains(mopType)){
				acquirerName = AcquirerType.DIREC_PAY.getName();
			}else{
				acquirerName = AcquirerType.CITRUS_PAY.getName();
			}
		}else{
			if(null == acquirerName || acquirerName.isEmpty()){
				acquirerName = AcquirerType.DIREC_PAY.getName();
			}
		}
		break;
	case WALLET:
		if(mopType.equals(MopType.MOBIKWIK_WALLET.getCode())){
			acquirerName = AcquirerType.MOBIKWIK.getName();
		}else if(mopType.equals(MopType.PAYTM_WALLET.getCode())){
			acquirerName = AcquirerType.PAYTM.getName();
		}else{
			acquirerName = user.getPrimaryAccount();
			if(null == acquirerName || acquirerName.isEmpty()){
				acquirerName = AcquirerType.MOBIKWIK.getName();
			}
		}
		break;
	case EMI:
			acquirerName = AcquirerType.CITRUS_PAY.getName();
		break;
	default:
		break;
	
	}
	fields.put(FieldType.ACQUIRER_TYPE.getName(), getInstancefromName(acquirerName).getCode());
	return getInstancefromName(acquirerName);
}
}
