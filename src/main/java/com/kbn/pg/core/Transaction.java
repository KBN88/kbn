package com.kbn.pg.core;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;

public interface Transaction {
	
	public void transact(Fields fields) throws SystemException;
	
}
