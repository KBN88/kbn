package com.kbn.pg.core;

import com.kbn.pg.core.pageintegrator.MerchantHostedProcessor;

public class MerchantHostedProcessorFactory {
	public static Processor getInstance(){
		return new MerchantHostedProcessor();
	}
}
