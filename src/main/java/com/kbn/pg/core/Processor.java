package com.kbn.pg.core;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;

public interface Processor {

	public abstract void preProcess(Fields fields) throws SystemException;

	public abstract void process(Fields fields) throws SystemException;

	public abstract void postProcess(Fields fields) throws SystemException;

}