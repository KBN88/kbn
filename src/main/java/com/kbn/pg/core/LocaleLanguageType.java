package com.kbn.pg.core;

public enum LocaleLanguageType {

	ENGLISH		("English", "en"),
	SPANISH		("Español", "es"),
	FRENCH	    ("Française", "fr"),
	GERMAN		("Deutsch", "ge"),
	HINDI		("हिन्दी", "lo-hn"),
	PUNJABI		("ਪੰਜਾਬੀ", "lo-pn"),
	MARATHI		("मराठी", "lo-ma"),
	GUJRATI	    ("ગુજરાતી", "lo-gj"),
	BENGALI		("বাংলা", "lo-bn"),
	TAMIL		("தமிழ்", "lo-ta"),
	TELGU	    ("తెలుగు", "lo-te"),
	KANNADA		("ಕನ್ನಡ", "lo-ka");
	
	
	private final String name;
	private final String code;
	
	private LocaleLanguageType(String name, String code){
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}
}
