package com.kbn.pg.core;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kbn.amex.AmexProcessorFactory;
import com.kbn.amex.EzeeClickProcessorFactory;
import com.kbn.atom.AtomProcessorFactory;
import com.kbn.axisupi.AxisBankUpiProcessorFactory;
import com.kbn.billdesk.BillDeskProcessorFactory;
import com.kbn.citruspay.CitrusPayProcessorFactory;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.cybersource.CyberSourceProcessorFactory;
import com.kbn.epaylater.EpayLaterFactory;
import com.kbn.fss.FssProcessorFactory;
import com.kbn.gpay.GPAYProcessorFactory;
import com.kbn.isgpay.ISGPayProcessorFactory;
import com.kbn.mobikwik.MobikwikProcessorFactory;
import com.kbn.netbanking.kotak.KotakProcessorFactory;
import com.kbn.netbanking.yesbank.YesBankProcessorFactory;
import com.kbn.paytm.PaytmProcessorFactory;
import com.kbn.pg.fraudPrevention.core.FraudPreventionProcessorFactory;
import com.kbn.pg.history.HistoryProcessorFactory;
import com.kbn.pg.security.SecurityProcessorFactory;
import com.kbn.safexpay.SafexPayProcessorFactory;
import com.kbn.sbi.SBIProcessorFactory;
import com.kbn.timesofmoney.DirecpayProcessorFactory;

public class RequestRouter {

	private Fields fields = null;

	public RequestRouter(Fields fields) {
		this.fields = fields;
	}

	public Map<String, String> route() {

		String shopifyFlag = fields.get(FieldType.INTERNAL_SHOPIFY_YN.getName());

		// Process security
		ProcessManager.flow(SecurityProcessorFactory.getInstance(), fields, false);

		// fraud Prevention processor
		ProcessManager.flow(FraudPreventionProcessorFactory.getInstance(), fields, false);

		// History Processor
		ProcessManager.flow(HistoryProcessorFactory.getInstance(), fields, false);

		// Insert new order
		ProcessManager.flow(MerchantHostedProcessorFactory.getInstance(), fields, false);

		// Process transaction with YesBank CyberSource
		ProcessManager.flow(CyberSourceProcessorFactory.getInstance(), fields, false);

		// Process transaction with SBI Pay
		ProcessManager.flow(SBIProcessorFactory.getInstance(), fields, false);
		// Process transaction with SAFEXPAY Pay
		ProcessManager.flow(SafexPayProcessorFactory.getInstance(), fields, false);

		// Process transaction with EpayLater Pay
		ProcessManager.flow(EpayLaterFactory.getInstance(), fields, false);

		// Process transaction with ISG Pay
		ProcessManager.flow(ISGPayProcessorFactory.getInstance(), fields, false);

		// Process transaction with BillDesk
		ProcessManager.flow(BillDeskProcessorFactory.getInstance(), fields, false);
		// Process transaction with BillDesk
		ProcessManager.flow(AtomProcessorFactory.getInstance(), fields, false);

		// Process transaction with Axis Bank UPI
		ProcessManager.flow(AxisBankUpiProcessorFactory.getInstance(), fields, false);

		// Process transaction with Axis Bank UPI
		ProcessManager.flow(GPAYProcessorFactory.getInstance(), fields, false);

		// Process transaction with FSS
		ProcessManager.flow(FssProcessorFactory.getInstance(), fields, false);

		// Process transaction with CitrusPay
		ProcessManager.flow(CitrusPayProcessorFactory.getInstance(), fields, false);

		// Process transaction with PAYTM
		ProcessManager.flow(PaytmProcessorFactory.getInstance(), fields, false);

		// Process transaction with DirecPay
		ProcessManager.flow(DirecpayProcessorFactory.getInstance(), fields, false);

		// Process transaction with YesBank
		ProcessManager.flow(YesBankProcessorFactory.getInstance(), fields, false);

		// Process transaction with Kotak
		ProcessManager.flow(KotakProcessorFactory.getInstance(), fields, false);

		// Process transaction with AMEX
		ProcessManager.flow(AmexProcessorFactory.getInstance(), fields, false);

		// Process transaction with EzeeClick AMEX
		ProcessManager.flow(EzeeClickProcessorFactory.getInstance(), fields, false);

		// Process transaction with Mobikwik
		ProcessManager.flow(MobikwikProcessorFactory.getInstance(), fields, false);

		// Update processor
		ProcessManager.flow(UpdateProcessorFactory.getInstance(), fields, true);

		// Generate response for user
		createResponse();

		if (!StringUtils.isEmpty(shopifyFlag)) {
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(), shopifyFlag);
		}
		return fields.getFields();
	}

	public void createResponse() {
		(new ResponseCreator()).create(fields);
	}
}
