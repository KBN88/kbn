package com.kbn.pg.core;

import com.kbn.amex.AmexValidator;
import com.kbn.amex.EzeeClickValidator;
import com.kbn.atom.AtomValidator;
import com.kbn.axisupi.AxisBankUpiValidator;
import com.kbn.billdesk.BillDeskValidator;
import com.kbn.citruspay.CitrusPayValidator;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.cybersource.CYBERSOURCEValidator;
import com.kbn.epaylater.EpayLaterValidator;
import com.kbn.fss.FssValidator;
import com.kbn.gpay.GPAYValidator;
import com.kbn.isgpay.ISGPayValidator;
import com.kbn.mobikwik.MobikwikValidator;
import com.kbn.netbanking.kotak.KotakValidator;
import com.kbn.netbanking.yesbank.YesBankValidator;
import com.kbn.pg.security.Validator;
import com.kbn.safexpay.SafexPayValidator;
import com.kbn.sbi.SBIValidator;

public class ProcessorValidatorFactory {
	public static Validator getInstance(Fields fields){

		AcquirerType acquirer = AcquirerType.getInstancefromCode(fields.get(FieldType.ACQUIRER_TYPE.getName()));
		if(null==acquirer){
			return null;
		}
		switch(acquirer){
		case AMEX:
			return new AmexValidator();
		case CITRUS_PAY:
			return new CitrusPayValidator();
		case DIREC_PAY:
			return new DirecPayValidator();
		case EZEECLICK:
			return new EzeeClickValidator();
		case FSS:
			return new FssValidator();
		case CYBER_SOURCE:
			return new CYBERSOURCEValidator();
		case KOTAK:
			return new KotakValidator();
		case MOBIKWIK:
			return new MobikwikValidator();
		case EPAYLATER:
			return new EpayLaterValidator();
		case PAYTM:
			break;
		case YESBANK:
			return new YesBankValidator();
		case BILLDESK:
			return new BillDeskValidator();
		case ISGPAY:
			return new ISGPayValidator();
		case SBI:
			return new SBIValidator();
		case SAFEX_PAY:
			return new SafexPayValidator();
		case AXIS_BANK_UPI:
			return new AxisBankUpiValidator();
		case GPAY:
			return new GPAYValidator();
		case ATOM:
			return new AtomValidator();
		default:
			return null;
		}
		return null;
	}
}
