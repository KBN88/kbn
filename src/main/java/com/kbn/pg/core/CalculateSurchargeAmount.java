package com.kbn.pg.core;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.kbn.commons.dao.ServiceTaxDao;
import com.kbn.commons.user.SurchargeDetails;
import com.kbn.commons.user.SurchargeDetailsDao;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class CalculateSurchargeAmount extends AbstractSecureAction{
	
	private static final long serialVersionUID = -2591149816444060474L;
	
	java.math.BigDecimal actualTransactionAmount;
	java.math.BigDecimal transSurchargeAmount;
	
	BigDecimal surchargeAmount = BigDecimal.ZERO;
	BigDecimal surchargePercentage = BigDecimal.ZERO;
	
	public BigDecimal[] fetchCCSurchargeDetails(String amount, String payId){
		String paymentType = "Credit Card";
		SurchargeDetailsDao surchargeDetailsDao = new SurchargeDetailsDao();
		SurchargeDetails surchargeDetailsFromDb = surchargeDetailsDao.findDetails(payId, paymentType);
		if (surchargeDetailsFromDb != null){
			surchargeAmount = surchargeDetailsFromDb.getSurchargeAmount();
			surchargePercentage = surchargeDetailsFromDb.getSurchargePercentage();
		};
		BigDecimal transAmount = new BigDecimal(amount);
		if (surchargeDetailsFromDb == null){
			actualTransactionAmount = transAmount;
			transSurchargeAmount = BigDecimal.ZERO;
		} else {
		BigDecimal servicetax = getServiceTax(payId);	
		calculateSurcharge(surchargeAmount, surchargePercentage, transAmount,servicetax);
		}
		
		BigDecimal surchargeDetails[] = new BigDecimal[2];
		surchargeDetails[0]= transSurchargeAmount;
		surchargeDetails[1] =  actualTransactionAmount;
        return surchargeDetails; 
				
	}
	
	public BigDecimal[] fetchDCSurchargeDetails(String amount, String payId){
		String paymentType = "Debit Card";
		SurchargeDetailsDao surchargeDetailsDao = new SurchargeDetailsDao();
		SurchargeDetails surchargeDetailsFromDb = surchargeDetailsDao.findDetails(payId, paymentType);
		if (surchargeDetailsFromDb != null){
			surchargeAmount = surchargeDetailsFromDb.getSurchargeAmount();
			surchargePercentage = surchargeDetailsFromDb.getSurchargePercentage();
		} 
		BigDecimal transAmount = new BigDecimal(amount);
		if (surchargeDetailsFromDb == null){
			actualTransactionAmount = transAmount;
			transSurchargeAmount = BigDecimal.ZERO;
		} else {
		BigDecimal servicetax = getServiceTax(payId);	
		calculateSurcharge(surchargeAmount, surchargePercentage, transAmount, servicetax);
		}
		
		BigDecimal surchargeDetails[] = new BigDecimal[2];
		surchargeDetails[0]= transSurchargeAmount;
		surchargeDetails[1] =  actualTransactionAmount;
        return surchargeDetails; 
		
	}
	
	public BigDecimal[] fetchNBSurchargeDetails(String amount, String payId){
		String paymentType = "Net Banking";
		SurchargeDetailsDao surchargeDetailsDao = new SurchargeDetailsDao();
		SurchargeDetails surchargeDetailsFromDb = surchargeDetailsDao.findDetails(payId, paymentType);
		if (surchargeDetailsFromDb != null){
			surchargeAmount = surchargeDetailsFromDb.getSurchargeAmount();
			surchargePercentage = surchargeDetailsFromDb.getSurchargePercentage();
		}
		BigDecimal transAmount = new BigDecimal(amount);
		if (surchargeDetailsFromDb == null){
			actualTransactionAmount = transAmount;
			transSurchargeAmount = BigDecimal.ZERO;
		} else {
		BigDecimal servicetax = getServiceTax(payId);
		calculateSurcharge(surchargeAmount, surchargePercentage, transAmount, servicetax);
		}
		BigDecimal surchargeDetails[] = new BigDecimal[2];
		surchargeDetails[0]= transSurchargeAmount;
		surchargeDetails[1] =  actualTransactionAmount;
        return surchargeDetails; 
		
	}
	
	public BigDecimal getServiceTax(String payid){
		
		BigDecimal servicetax = new ServiceTaxDao().findServiceTaxByPayId(payid);
		if (servicetax == null){
			servicetax = BigDecimal.ZERO;
		}
		return servicetax;
	}
	
	public void calculateSurcharge(BigDecimal surchargeAmount, BigDecimal surchargePercentage, BigDecimal transAmount, BigDecimal servicetax){
		
		DecimalFormat df = new DecimalFormat("0.00");
		BigDecimal percentagevalue = new BigDecimal(100);
		BigDecimal totalsurcharge = ((transAmount.multiply(surchargePercentage).divide(percentagevalue))).add(surchargeAmount);
		BigDecimal calculatedServiceTax = totalsurcharge.multiply(servicetax).divide(percentagevalue);
		actualTransactionAmount = (transAmount.add(totalsurcharge).add(calculatedServiceTax));
		transSurchargeAmount = actualTransactionAmount.subtract(transAmount);
	
		String formattedAmount = df.format(actualTransactionAmount);
		String surAmount = df.format(transSurchargeAmount);
		
		transSurchargeAmount = new BigDecimal(surAmount);
		actualTransactionAmount = new BigDecimal(formattedAmount);
			
	}
	
	
}
