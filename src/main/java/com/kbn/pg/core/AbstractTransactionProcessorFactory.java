package com.kbn.pg.core;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;

public interface AbstractTransactionProcessorFactory {
	
	public TransactionProcessor getInstance(Fields fields) throws SystemException;

}
