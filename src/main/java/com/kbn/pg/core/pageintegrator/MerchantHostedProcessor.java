package com.kbn.pg.core.pageintegrator;

import org.apache.commons.lang3.StringUtils;

import com.kbn.amex.Constants;
import com.kbn.commons.dao.FieldsDao;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionManager;
import com.kbn.pg.core.Processor;

public class MerchantHostedProcessor implements Processor {

	@Override
	public void preProcess(Fields fields) throws SystemException {
	}

	@Override
	public void process(Fields fields) throws SystemException {
		String merchantHostedFlag = fields.get(FieldType.IS_MERCHANT_HOSTED.getName());
		if(StringUtils.isBlank(merchantHostedFlag) || !merchantHostedFlag.equals(Constants.Y)){
			return;
		}
		fields.put(FieldType.OID.getName(),fields.get(FieldType.TXN_ID.getName()));
		
		fields.insertNewOrder();
		//for inserting customer billing details  
		new FieldsDao().insertCustomerInfoQuery(fields);
		
		fields.put(FieldType.ORIG_TXN_ID.getName(),fields.get(FieldType.TXN_ID.getName()));
		fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(),fields.get(FieldType.TXN_ID.getName()));
		fields.put(FieldType.TXN_ID.getName(),TransactionManager.getNewTransactionId());
		fields.remove(FieldType.INTERNAL_REQUEST_FIELDS.getName());
	}

	@Override
	public void postProcess(Fields fields) throws SystemException {
	}

}
