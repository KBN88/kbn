package com.kbn.pg.core.pageintegrator;

import java.io.PrintWriter;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Amount;
import com.opensymphony.xwork2.Action;
//import com.paytm.merchant.CheckSumServiceHelper;

/**
 * @author Sunil
 *
 */
public class PaytmCustomizer implements Customizer {

	private static Logger logger = Logger.getLogger(PaytmCustomizer.class
			.getName());
	private String website;
	private String mid;
	private String merchantKey;
	private String industrytypeId;
	private String channelID;
	private String requestUrl;
	private String requestType;
	private String amount;
	private String custid;
	private String orderid;
	private String checksum;
	private String paymentDetails;
	private String paymentModeOnly;
	private String bankCode;
	private String paymentTypeID;
	private String authMode;
	private String callbackUrl;
	private HttpServletRequest httpRequest;
	private TreeMap<String, String> parameters = new TreeMap<String, String>();

	public PaytmCustomizer() {
	}

	@Override
	public String integrate(Fields fields) {
		try {
			// Fetching Details from Property file
			PropertiesManager propertiesManager = new PropertiesManager();
			website = propertiesManager.getSystemProperty("PaytmWebsite");
			mid = propertiesManager.getSystemProperty("PaytmMID");
			merchantKey = propertiesManager
					.getSystemProperty("PaytmMerchantKey");
			industrytypeId = propertiesManager
					.getSystemProperty("PaytmIndustrytypeId");
			channelID = propertiesManager.getSystemProperty("PaytmChannelID");
			requestType = propertiesManager
					.getSystemProperty("PaytmRequestType");
			requestUrl = propertiesManager.getSystemProperty("PaytmRequestUrl");
			amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
					fields.get(FieldType.CURRENCY_CODE.getName()));
			orderid = fields.get(FieldType.TXN_ID.getName());
			custid = fields.get(FieldType.CUST_ID.getName());
			callbackUrl = propertiesManager
					.getSystemProperty("PaytmCallbackUrl");
			paymentTypeID = fields.get(FieldType.PAYMENT_TYPE.getName());
			if (PaymentType.WALLET.getCode().equals(paymentTypeID)) {
				paymentTypeID = fields.get(FieldType.WALLET_NAME.getName());
			}
			paymentModeOnly = propertiesManager
					.getSystemProperty("PaytmPaymentModeOnly");
	//		bankCode = fields.get(FieldType.BANK_NAME.getName());
			authMode = propertiesManager.getSystemProperty("PaytmAuthMode");
		
			parameters.put("AUTH_MODE", getAuthMode());
			parameters.put("WEBSITE", getWebsite());
			parameters.put("BANK_CODE", getBankCode());
			parameters.put("MID", getMid());
			parameters.put("ORDER_ID", getOrderid());
			parameters.put("PAYMENT_TYPE_ID", getPaymentTypeID());
			parameters.put("INDUSTRY_TYPE_ID", getIndustrytypeId());
			parameters.put("CHANNEL_ID", getChannelID());
			parameters.put("TXN_AMOUNT", getAmount());
			parameters.put("CUST_ID", getCustid());
			parameters.put("PAYMENT_MODE_ONLY", getPaymentModeOnly());
			parameters.put("CALLBACK_URL", getCallbackUrl());

			// Request formation for seemless transaction
			PrintWriter out = ServletActionContext.getResponse().getWriter();
			StringBuilder httpRequest = new StringBuilder();
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(getRequestUrl());
			httpRequest.append("\" method=\"post\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"AUTH_MODE\" value=\"");
			httpRequest.append(getAuthMode());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"WEBSITE\" value=\"");
			httpRequest.append(getWebsite());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"BANK_CODE\" value=\"");
			httpRequest.append(getBankCode());
			httpRequest.append("\">");
			httpRequest.append("<input type=\"hidden\" name=\"MID\" value=\"");
			httpRequest.append(getMid());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"ORDER_ID\" value=\"");
			httpRequest.append(getOrderid());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"PAYMENT_TYPE_ID\" value=\"");
			httpRequest.append(getPaymentTypeID());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"INDUSTRY_TYPE_ID\" value=\"");
			httpRequest.append(getIndustrytypeId());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"CHANNEL_ID\" value=\"");
			httpRequest.append(getChannelID());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"TXN_AMOUNT\" value=\"");
			httpRequest.append(getAmount());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"CUST_ID\" value=\"");
			httpRequest.append(getCustid());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"PAYMENT_MODE_ONLY\" value=\"");
			httpRequest.append(getPaymentModeOnly());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"CALLBACK_URL\" value=\"");
			httpRequest.append(getCallbackUrl());
			httpRequest.append("\">");
			httpRequest
					.append("<input type=\"hidden\" name=\"CHECKSUMHASH\" value=\"");
			httpRequest.append(checkSumCalculator(getMerchantKey(),
					getParameters()));
			httpRequest.append("\">");
			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");

			out.write(httpRequest.toString());
			fields.put(FieldType.ACQUIRER_TYPE.getName(),
					AcquirerType.PAYTM.getCode());
		} catch (Exception exception) {
			logger.error("Exception", exception);

		}
		return Action.NONE;
	}

	public String checkSumCalculator(String merchantKey,
			TreeMap<String, String> parameters) {
		/*CheckSumServiceHelper checkSumServiceHelper = CheckSumServiceHelper
				.getCheckSumServiceHelper();
		try {
			checksum = checkSumServiceHelper.genrateCheckSum(getMerchantKey(),
					getParameters());
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}*/
		return checksum;

	}

	public void generatingCheckSum() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyCheckSum() {

	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMerchantKey() {
		return merchantKey;
	}

	public void setMerchantKey(String merchantKey) {
		this.merchantKey = merchantKey;
	}

	public String getIndustrytypeId() {
		return industrytypeId;
	}

	public void setIndustrytypeId(String industrytypeId) {
		this.industrytypeId = industrytypeId;
	}

	public String getChannelID() {
		return channelID;
	}

	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}

	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public String getPaymentModeOnly() {
		return paymentModeOnly;
	}

	public void setPaymentModeOnly(String paymentModeOnly) {
		this.paymentModeOnly = paymentModeOnly;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getPaymentTypeID() {
		return paymentTypeID;
	}

	public void setPaymentTypeID(String paymentTypeID) {
		this.paymentTypeID = paymentTypeID;
	}

	public String getAuthMode() {
		return authMode;
	}

	public void setAuthMode(String authMode) {
		this.authMode = authMode;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public HttpServletRequest getHttpRequest() {
		return httpRequest;
	}

	public void setHttpRequest(HttpServletRequest httpRequest) {
		this.httpRequest = httpRequest;
	}

	public TreeMap<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(TreeMap<String, String> parameters) {
		this.parameters = parameters;
	}

}
