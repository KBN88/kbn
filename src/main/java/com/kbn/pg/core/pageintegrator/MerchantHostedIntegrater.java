package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Processor;
import com.kbn.pg.security.GeneralValidator;

public class MerchantHostedIntegrater implements Processor{

	@Override
	public void preProcess(Fields fields) throws SystemException {
		GeneralValidator generalValidator = new GeneralValidator();
		generalValidator.validateHash(fields);
		generalValidator.validateReturnUrl(fields);		
	}

	@Override
	public void process(Fields fields) throws SystemException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postProcess(Fields fields) throws SystemException {
		// TODO Auto-generated method stub
		
	}

}
