package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.ResponseCreator;
import com.opensymphony.xwork2.Action;

/**
 * @author Sunil
 *
 */
public class CitrusPayCustomizer implements Customizer {
	@Override
	public String integrate(Fields fields) throws SystemException {

			String responseCode = fields.get(FieldType.RESPONSE_CODE.getName());

			if (null == responseCode|| !responseCode.equals(ErrorType.SUCCESS.getCode())) {
				//change status if not set correctly
				String status = fields.get(FieldType.STATUS.getName());
				if(status.equals(StatusType.PENDING.getName())){
					fields.put(FieldType.STATUS.getName(), StatusType.ERROR.getName());
				}
				ResponseCreator responseCreator = new ResponseCreator();
				responseCreator.ResponsePost(fields);
			}
		return Action.SUCCESS;
	}
}
