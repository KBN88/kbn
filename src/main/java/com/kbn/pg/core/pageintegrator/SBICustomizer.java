package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.opensymphony.xwork2.Action;

public class SBICustomizer implements Customizer{

	@Override
	public String integrate(Fields fields) throws SystemException {
		fields.put(FieldType.TXNTYPE.getName(),TransactionType.SALE.getName());
		fields.logAllFields("All Response fields Recieved");
		return Action.NONE;
	}
	
	

}
