package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.AcquirerType;
import com.opensymphony.xwork2.Action;

/**
 * @author Sunil
 *
 */
public class DirecpayCustomizer implements Customizer {

	@Override
	public String integrate(Fields fields) {
	
		if(fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())){
			//TODO modify code
		}
		
		fields.put(FieldType.ACQUIRER_TYPE.getName(),AcquirerType.DIREC_PAY.getCode());
		
		return Action.NONE;
	}
	
	
}
