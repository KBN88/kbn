package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.AcquirerType;
import com.opensymphony.xwork2.Action;

public class KotakCustomizer implements Customizer {

	public KotakCustomizer() {
	}

	@Override
	public String integrate(Fields fields) {
	
		if(fields.get(FieldType.RESPONSE_CODE.getName()).equals("000")){
		//TODO handle error response
		}

		fields.put(FieldType.ACQUIRER_TYPE.getName(),
				AcquirerType.KOTAK.getCode());
		return Action.NONE;
	}

}
