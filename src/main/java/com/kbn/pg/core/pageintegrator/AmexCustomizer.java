package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.ResponseCreator;
import com.opensymphony.xwork2.Action;

/**
 * @author Sunil
 *
 */
public class AmexCustomizer implements Customizer {

	@Override
	public String integrate(Fields fields) throws SystemException {

		fields.logAllFields("All Response fields Recieved");
		String responseCode = fields.get(FieldType.RESPONSE_CODE.getName());

		//Code find all the possible failure reasons and handle accordingly
		if (null == responseCode || !responseCode.equals(ErrorType.SUCCESS.getCode())) {
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);
		}
		return Action.NONE;
	}
}
