package com.kbn.pg.core.pageintegrator;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.emailer.EmailBuilder;
import com.kbn.pg.core.RequestCreator;
import com.kbn.pg.core.ResponseCreator;
import com.opensymphony.xwork2.Action;

/**
 * @author Sunil
 *
 */
public class FssCustomizer implements Customizer {
	private static Logger logger = Logger.getLogger(FssCustomizer.class
			.getName()); 
	@Override
	public String integrate(Fields fields) {
		fields.logAllFields("All Response fields Recieved");

		String responseCode = fields.get(FieldType.RESPONSE_CODE.getName());

		if (fields.get(FieldType.STATUS.getName()) == StatusType.ENROLLED.getName()) {
			RequestCreator requestCreator = new RequestCreator();
			requestCreator.EnrollRequest(fields);						
			return Action.NONE;
		}

		if (null == responseCode || !responseCode.equals(ErrorType.SUCCESS.getCode())) {			
			RequestCreator requestCreator = new RequestCreator();
			requestCreator.InvalidRequest(fields);
			return Action.NONE;
		}
		else{
			EmailBuilder emailBuilder = new EmailBuilder();
			try {
				emailBuilder.transactionEmailer(fields,UserType.MERCHANT.toString());
			} catch (Exception exception) {
				logger.error("Exception", exception);
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);
		}
		return Action.NONE;
	}
}
