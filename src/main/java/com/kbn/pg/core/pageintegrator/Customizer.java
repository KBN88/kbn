package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;

/**
 * @author Sunil
 *
 */
public interface Customizer {

	public String integrate(Fields fields) throws SystemException;
}
