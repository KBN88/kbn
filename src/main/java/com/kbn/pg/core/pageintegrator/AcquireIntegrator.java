package com.kbn.pg.core.pageintegrator;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;

/**
 * @author Sunil
 *
 */
public class AcquireIntegrator implements Processor {

	public AcquireIntegrator() {
	}

	@Override
	public void preProcess(Fields fields) throws SystemException {
	}

	@Override
	public void process(Fields fields) throws SystemException {
		String responseCode = fields.get(FieldType.RESPONSE_CODE.getName());
		if(!StringUtils.isEmpty(responseCode) && !responseCode.equals(ErrorType.SUCCESS.getCode())){
			if(fields.get(FieldType.STATUS.getName()).equals(StatusType.PENDING.getName())){
				fields.put(FieldType.STATUS.getName(), StatusType.ERROR.getName());
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);
			return;
		}

		Customizer customizer = AcquirerCustomizerFactory.instance(fields);
		if(!fields.get(FieldType.ACQUIRER_TYPE.getName()).equals(AcquirerType.CITRUS_PAY.getCode())){
			fields.remove(FieldType.ACQUIRER_TYPE.getName());
		}
		customizer.integrate(fields);
	}

	@Override
	public void postProcess(Fields fields) throws SystemException {

	}
}
