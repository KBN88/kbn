package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Processor;

/**
 * @author Sunil
 *
 */
public class AcquireIntegratorFactory {

	public static Processor instance(Fields fields){
		return new AcquireIntegrator();
	}
}
