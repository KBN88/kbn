package com.kbn.pg.core.pageintegrator;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.AcquirerType;

/**
 * @author Sunil
 *
 */
public class AcquirerCustomizerFactory {

	public AcquirerCustomizerFactory() {
	}

	public static Customizer instance(Fields fields) throws SystemException {
		AcquirerType acquirer = null;
		String acquirerCode = fields.get(FieldType.ACQUIRER_TYPE.getName());

		if (StringUtils.isBlank(acquirerCode)) {
			acquirer = AcquirerType.getDefault(fields);
			fields.put(FieldType.ACQUIRER_TYPE.getName(), acquirer.getCode());
			fields.put(FieldType.INTERNAL_ACQUIRER_TYPE.getName(), acquirer.getCode());
		} else {
			acquirer = AcquirerType.getInstancefromCode(acquirerCode);
		}

		switch (acquirer) {
		case CITRUS_PAY:
			return new CitrusPayCustomizer();
		case FSS:
			return new FssCustomizer();
		case CYBER_SOURCE:
			return new CYBERSOURCECustomizer();
		case PAYTM:
			return new PaytmCustomizer();
		case DIREC_PAY:
			return new DirecpayCustomizer();
		case AMEX:
			return new AmexCustomizer();
		case MOBIKWIK:
			return new MobikwikCustomizer();
		case EPAYLATER:
			return new EpayLaterCustomizer();
		case EZEECLICK:
			return new EzeeClickCustomizer();
		case YESBANK:
			return new YesBankCustomizer();
		case KOTAK:
			return new KotakCustomizer();
		case ISGPAY:
			return new ISGPayCustomizer();
		case SBI:
			return new SBICustomizer();
		case SAFEX_PAY:
			return new SafexPayCustomizer();
		case AXIS_BANK_UPI:
			return new AxisBankUpiCustomizer();
		case GPAY:
			return new GPAYCustomizer();
		case ATOM:
			return new AtomCustomizer();
		case BILLDESK:
			return new BillDeskCustomizer();
		default:
			break;
		}

		// Ideally non reachable code
		return null;
	}

}
