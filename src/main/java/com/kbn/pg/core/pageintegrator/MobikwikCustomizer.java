package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.opensymphony.xwork2.Action;

/**
 * @author Sunil
 *
 */
public class MobikwikCustomizer implements Customizer {

	@Override
	public String integrate(Fields fields) {
		fields.put(FieldType.TXNTYPE.getName(),TransactionType.SALE.getName());
		fields.logAllFields("All Response fields Recieved");
		fields.put(FieldType.ACQUIRER_TYPE.getName(),AcquirerType.MOBIKWIK.getCode());
		return Action.NONE;
	}
}
