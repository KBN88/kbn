package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Processor;

public class MerchantHostedIntegraterFactory {

	public static Processor instance(Fields fields){
		return new MerchantHostedIntegrater();
	}
}
