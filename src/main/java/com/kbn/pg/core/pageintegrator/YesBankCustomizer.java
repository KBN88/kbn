package com.kbn.pg.core.pageintegrator;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.opensymphony.xwork2.Action;

public class YesBankCustomizer implements Customizer {

	public YesBankCustomizer() {
	}

	@Override
	public String integrate(Fields fields) {
		fields.put(FieldType.TXNTYPE.getName(),TransactionType.SALE.getName());

		if(fields.get(FieldType.RESPONSE_CODE.getName()).equals("000")){
		
		}
		fields.put(FieldType.ACQUIRER_TYPE.getName(),
				AcquirerType.YESBANK.getCode());
		return Action.NONE;
	}
	
	
}


