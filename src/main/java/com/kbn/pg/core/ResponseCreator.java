package com.kbn.pg.core;

import java.io.PrintWriter;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.shopify.ShopifyResponseCreater;
import com.kbn.shopify.TransactionConverter;
import com.kbn.sms.SmsSender;

/**
 * @author Sunil
 */
public class ResponseCreator extends Forwarder {

	private static Logger logger = Logger.getLogger(ResponseCreator.class
			.getName());
	private static final long serialVersionUID = 6021494405007391983L;
	PropertiesManager propertiesManager = new PropertiesManager();

	public void create(Fields fields) {
		try {
			Processor processor = ResponseProcessorFactory.getInstance();
			processor.preProcess(fields);
			processor.process(fields);
			processor.postProcess(fields);

		} catch (SystemException systemException) {
			logger.error("Exception", systemException);
			fields.clear();
		} catch (Exception exception) {
			logger.error("Exception", exception);
			fields.clear();
		}
	}

	public void ResponsePost(Fields fields){
		//send sms
		SmsSender.sendSMS(fields);
		
		String shopifyFlag = fields.get(FieldType.INTERNAL_SHOPIFY_YN.getName());
		
		if(null!=shopifyFlag && shopifyFlag.equals("Y")){		
			Map<String,String> responseMap = new TransactionConverter().prepareResponse(fields);
			new ShopifyResponseCreater().ResponsePost(fields, responseMap);
		}else{
		try {
			fields.removeInternalFields();
			PrintWriter out = ServletActionContext.getResponse().getWriter();
			StringBuilder httpRequest = new StringBuilder();
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(fields.get(FieldType.RETURN_URL.getName()));
			httpRequest.append("\" method=\"post\">");
			for (String key : fields.keySet()) {
				httpRequest.append("<input type=\"hidden\" name=\"");
				httpRequest.append(key);
				httpRequest.append("\" value=\"");
				httpRequest.append(fields.get(key));
				httpRequest.append("\">");
			}
			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");
			logger.info("final request sent "+httpRequest);
			out.write(httpRequest.toString());

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}
   }
}
