package com.kbn.pg.core;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;

public class ResponseProcessor implements Processor {

	public ResponseProcessor() {
	}

	public void preProcess(Fields fields) {
	}

	public void process(Fields fields) throws SystemException {
		
		(new TransactionResponser()).getResponse(fields); 

	}

	public void postProcess(Fields fields) {
		fields.logAllFields("Sending Response to Client");
	}
}
