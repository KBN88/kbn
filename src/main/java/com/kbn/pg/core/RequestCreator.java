
package com.kbn.pg.core;

import java.io.PrintWriter;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.WhitelableBranding;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PropertiesManager;
/**
 * @author Neeraj
 * 
 */
public class RequestCreator{

	private static Logger logger = Logger.getLogger(RequestCreator.class.getName());
	private PropertiesManager propertiesManager = new PropertiesManager();
	UserDao userDao = new UserDao();
	public void EnrollRequest(Fields responseMap) {
		try {
			/************* Enrolled card condition starts here ************/
			String termURL;
			String acsurl = responseMap.get(FieldType.ACS_URL.getName());
			String PAReq = responseMap.get(FieldType.PAREQ.getName());
			String paymentid = responseMap.get(FieldType.PAYMENT_ID.getName());
			String acquireType = responseMap.get(FieldType.INTERNAL_ACQUIRER_TYPE.getName());
			// White-Label Response url
			HttpServletRequest request = ServletActionContext.getRequest();
			String brandingURL = request.getRequestURL().toString();
			URL urlTemp = new URL(brandingURL);
			String brandingHost = urlTemp.getHost();
			logger.info("WhitLabe Brand HostUrl :" + brandingHost);
			WhitelableBranding userReseller = userDao.findByWhitelabelBrandURL(brandingHost);
			if (userReseller != null) {
				if(acquireType.equals("CYBERSOURCE")) {
					 termURL = "https://"+userReseller.getBrandURL()+"/crm/jsp/cyberSourcePaymentResponse";
				}else {
					 termURL = "https://"+userReseller.getBrandURL()+"/crm/jsp/payment3ds";
				}
			}else {
				if(acquireType.equals("CYBERSOURCE")) {
					 termURL = propertiesManager.getSystemProperty("Request3DCyberSourceURL");
				}else {
					 termURL = propertiesManager.getSystemProperty("Request3DSURL");
				}
			}
			
			PrintWriter out = ServletActionContext.getResponse().getWriter();

			StringBuilder httpRequest = new StringBuilder();
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(acsurl);
			httpRequest.append("\" method=\"post\">");

			if(responseMap.get(FieldType.MOP_TYPE.getName()).equals(MopType.RUPAY.getCode())){
				httpRequest.append("<input type=\"hidden\" name=\"PaymentID\" value=\"");
				httpRequest.append(paymentid);
				httpRequest.append("\">");
			}else{

				httpRequest.append("<input type=\"hidden\" name=\"PaReq\" value=\"");
				httpRequest.append(PAReq);
				httpRequest.append("\">");

				httpRequest.append("<input type=\"hidden\" name=\"MD\" value=\"");
				httpRequest.append(paymentid);
				httpRequest.append("\">");
				httpRequest.append("<input type=\"hidden\" name=\"TermUrl\" value=\"");
				httpRequest.append(termURL);
				httpRequest.append("\">");
			}

			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");

			out.write(httpRequest.toString());
			/************* Enrolled card condition Ends here ************/
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	public void InvalidRequest(Fields fields) {
		try {
			/************* Invalid Request ************/

			PrintWriter out = ServletActionContext.getResponse().getWriter();

			//TO remove internal custom MDC
			fields.removeInternalFields();

			StringBuilder httpRequest = new StringBuilder();
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(fields.get(FieldType.RETURN_URL.getName()));
			httpRequest.append("\" method=\"post\">");
			for (String key : fields.keySet()) {
				httpRequest.append("<input type=\"hidden\" name=\"");
				httpRequest.append(key);
				httpRequest.append("\" value=\"");
				httpRequest.append(fields.get(key));
				httpRequest.append("\">");
			}
			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");

			out.write(httpRequest.toString());
			/************* Invalid Request ************/
		} 
		catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	//public void WebsitePackageRequest(String PAY_ID,String ORDER_ID,String AMOUNT,String TXNTYPE,String CUST_NAME,String CUST_EMAIL,String PRODUCT_DESC,String CURRENCY_CODE,String RETURN_URL,String HASH) {
	public void WebsitePackageRequest(Fields fields) {
		try {		
			String requestURL = propertiesManager.getSystemProperty("RequestURL");
			PrintWriter out = ServletActionContext.getResponse().getWriter();
			StringBuilder httpRequest = new StringBuilder();
			httpRequest.append("<HTML>");
			httpRequest.append("<BODY OnLoad=\"OnLoadEvent();\" >");
			httpRequest.append("<form name=\"form1\" action=\"");
			httpRequest.append(requestURL);
			httpRequest.append("\" method=\"post\">");
			for (String key : fields.keySet()) {
				httpRequest.append("<input type=\"hidden\" name=\"");
				httpRequest.append(key);
				httpRequest.append("\" value=\"");
				httpRequest.append(fields.get(key));
				httpRequest.append("\">");
			}

			httpRequest.append("</form>");
			httpRequest.append("<script language=\"JavaScript\">");
			httpRequest.append("function OnLoadEvent()");
			httpRequest.append("{document.form1.submit();}");
			httpRequest.append("</script>");
			httpRequest.append("</BODY>");
			httpRequest.append("</HTML>");

			out.write(httpRequest.toString());
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}
}
