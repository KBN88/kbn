package com.kbn.pg.core;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.security.Authenticator;
import com.kbn.commons.security.AuthenticatorFactory;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionManager;
import com.kbn.commons.util.TransactionType;

public class UpdateProcessor implements Processor {

	private CryptoManager cryptoManager = CryptoManagerFactory
			.getCryptoManager();
	private static Logger logger = Logger.getLogger(UpdateProcessor.class.getName());

	public UpdateProcessor() {

	}

	public void preProcess(Fields fields) throws SystemException {
		cryptoManager.secure(fields);		
	}

	public void process(Fields fields) throws SystemException {
		addOid(fields);

		String transactionType = fields.get(FieldType.TXNTYPE.getName());
		String responseCode = fields.get(FieldType.RESPONSE_CODE.getName());

		if (null == transactionType ) {
			prepareInvalidTransactionForStorage(fields);
		}else if(responseCode.equals(ErrorType.VALIDATION_FAILED.getCode())){
			if(!transactionType.equals(TransactionType.REFUND.getName()) && !transactionType.equals(TransactionType.STATUS.getName()) &&
					!transactionType.equals(TransactionType.CAPTURE.getName()) && !StringUtils.isBlank(fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()))){
				fields.put(FieldType.TXNTYPE.getName(),fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()));
			}
			prepareInvalidTransactionForStorage(fields);
		}else {
			fields.insert();
		}
	}

	public void prepareInvalidTransactionForStorage(Fields fields) throws SystemException{
		String payId = fields.get(FieldType.PAY_ID.getName());
		if(!StringUtils.isEmpty(payId)){
			Authenticator authenticator = AuthenticatorFactory.getAuthenticator();
			authenticator.isUserExists(fields);

			String txnId = fields.get(FieldType.TXN_ID.getName());
			if(StringUtils.isEmpty(txnId)){
				txnId = TransactionManager.getNewTransactionId();
			}
			String responseCode = fields.get(FieldType.RESPONSE_CODE.getName());
			String responseMessage = fields.get(FieldType.RESPONSE_MESSAGE.getName());
			String detailMessage = fields.get(FieldType.PG_TXN_MESSAGE.getName());
			String transactionType = fields.get(FieldType.TXNTYPE.getName());
			String returnUrl = fields.get(FieldType.RETURN_URL.getName());
			String orderId = fields.get(FieldType.ORDER_ID.getName());
			Fields internalFields = fields.removeInternalFields();
			String oid = fields.get(FieldType.OID.getName());
			fields.clear();
			fields.put(internalFields);
			fields.put(FieldType.TXN_ID.getName(), txnId);
			fields.put(FieldType.OID.getName(), oid);
			fields.put(FieldType.RESPONSE_CODE.getName(), responseCode);
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), responseMessage);
			fields.put(FieldType.STATUS.getName(), StatusType.INVALID.getName());
			fields.put(FieldType.PAY_ID.getName(), payId);
			fields.put(FieldType.TXNTYPE.getName(), transactionType);
			fields.put(FieldType.RETURN_URL.getName(), returnUrl);
			fields.put(FieldType.ORDER_ID.getName(), orderId);
			fields.put(FieldType.PG_TXN_MESSAGE.getName(), detailMessage);
			fields.insert();
		}
	}

	public void addOid(Fields fields){
		String oid = fields.get(FieldType.OID.getName());
		//Oid is already present, return
		if(!StringUtils.isEmpty(oid)){
			return;
		}

		String transactionType = fields.get(FieldType.TXNTYPE.getName());
		if (null != transactionType && transactionType.equals(TransactionType.NEWORDER.getName())){
			oid = fields.get(FieldType.TXN_ID.getName());
		} else {
			String origOid = fields.get(FieldType.INTERNAL_ORIG_TXN_ID.getName());
			if(!StringUtils.isEmpty(origOid)){
				oid = origOid;
			}else{
				//to add OID for transactions that are through webservice or invalid
				oid = fields.get(FieldType.TXN_ID.getName());
			}
		}
		if(StringUtils.isEmpty(oid)){
			logger.warn("Unable to add OID, this may cause some issues!");
		} else {
			fields.put(FieldType.OID.getName(), oid);
		}
	}

	public void postProcess(Fields fields) {
	}
}
