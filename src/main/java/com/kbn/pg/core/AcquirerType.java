package com.kbn.pg.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.ChargingDetailsDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;

/**
 * @author Sunil
 *
 */
public enum AcquirerType {

	//Credit Card
	AMEX      ("AMEX", "AMERICAN EXPRESS"),
	CITRUS_PAY ("CITRUS", "Yes Bank"),
	EZEECLICK ("EZEECLICK", "AMEX EZEECLICK"),
	FSS	       ("FSS", "HDFC Bank"),
	CYBER_SOURCE("CYBERSOURCE", "CyberSource"),

	//Wallet
	MOBIKWIK("MOBIKWIK", "WALLET MOBIKWIK"),
	PAYTM      ("PAYTM", "WALLET PAYTM"),

	//NetBanking
	YESBANK ("YESBANK", "NETBANKING YES Bank"),
	KOTAK("KOTAK", "NETBANKING KOTAK Bank"),

	// NetBanking in aggregation mode
	DIREC_PAY("DIRECPAY", "DIRECPAY"),
	
	//
	BILLDESK("BILLDESK", "BILL DESK"),
	ISGPAY("ISGPAY", "ISGPAY"),
	SBI("SBI", "SBI"),
	SAFEX_PAY("SAFEXPAY", "SafexPay"),
	AXIS_BANK_UPI("AXISBANKUPI", "AXIS Bank UPI"),
	GPAY("GPAY", "GPAY"),
	EPAYLATER("EPAYLATER", "WALLET EPAYLATER"),
	ATOM("ATOM", "ATOM");

	private final String code;
	private final String name;

	private AcquirerType(String code, String name){
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static AcquirerType getInstancefromCode(String acquirerCode){	
		AcquirerType acquirerType = null;

		for(AcquirerType acquirer:AcquirerType.values()){

			if(acquirerCode.equals(acquirer.getCode())){
				acquirerType=acquirer;
				break;
			}
		}

		return acquirerType;		
	}	

	public static AcquirerType getInstancefromName(String acquirerName){	
		AcquirerType acquirerType = null;

		for(AcquirerType acquirer:AcquirerType.values()){

			if(acquirerName.equals(acquirer.getName())){
				acquirerType=acquirer;
				break;
			}
		}

		return acquirerType;		
	}

	public static AcquirerType getDefault(Fields fields) throws SystemException{
		User user = new User();
		UserDao userDao = new UserDao();
		user = userDao.findPayId(fields.get(FieldType.PAY_ID.getName()));
		List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
		chargingDetailsList = new ChargingDetailsDao().getAllActiveChargingDetails(user.getPayId());

		return getAcquirer(fields.getFields(),user,chargingDetailsList);
	//	return AcquirerType.FSS;
	}

	public static AcquirerType getDefault(Fields fields,User user){
		List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
		chargingDetailsList = new ChargingDetailsDao().getAllActiveChargingDetails(user.getPayId());
		return getAcquirer(fields.getFields(), user,chargingDetailsList);
	//	return AcquirerType.FSS;
	}
	
	public static AcquirerType getDefault(Fields fields,User user, List<ChargingDetails>  paymentOptions){
		return getAcquirer(fields.getFields(), user,paymentOptions);
	//	return (new RouterRuleImpl()).getAcquirerType("Sample Rule");
	//	return AcquirerType.FSS;
	}

	private static AcquirerType getAcquirer(Map<String,String> fields, User user, List<ChargingDetails>  paymentOptions){
		String acquirerName = "";
		PaymentType paymentType = PaymentType.getInstanceUsingCode(fields.get(FieldType.PAYMENT_TYPE.getName()));
		String mopType = fields.get(FieldType.MOP_TYPE.getName());
		if(StringUtils.isEmpty(fields.get(FieldType.PAYMENT_TYPE.getName())) || StringUtils.isEmpty(mopType)){
			return null;
		}
		switch (paymentType){

		case CREDIT_CARD:
		case DEBIT_CARD:
			if(mopType.equals(MopType.AMEX.getCode())){
				acquirerName = AcquirerType.AMEX.getName();
			}else if(mopType.equals(MopType.EZEECLICK.getCode())){
				acquirerName = AcquirerType.EZEECLICK.getName();
			}else if(mopType.equals(MopType.DINERS.getCode())){
				acquirerName = AcquirerType.FSS.getName();
			}else{
				if(user.getPrimaryAccount().equals(AcquirerType.CITRUS_PAY.getName())){
					for(ChargingDetails detail: paymentOptions){
						if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
								&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
							acquirerName = AcquirerType.CITRUS_PAY.getName();
							break;
						}
					}
					if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.FSS.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.FSS.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CYBER_SOURCE.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CYBER_SOURCE.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SAFEX_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SAFEX_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SBI.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.ISGPAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.BILLDESK.getName();
								break;
							}
						}
					}
					// FSS
				}else if(user.getPrimaryAccount().equals(AcquirerType.FSS.getName())){
					for(ChargingDetails detail: paymentOptions){
						if(detail.getAcquirerName().equals(AcquirerType.FSS.getName()) && mopType.equals(detail.getMopType().getCode())
								&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
							acquirerName = AcquirerType.FSS.getName();
							break;
						}
					}
					if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CITRUS_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CYBER_SOURCE.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CYBER_SOURCE.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SAFEX_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SAFEX_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SBI.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.ISGPAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.BILLDESK.getName();
								break;
							}
						}
					}
				}//CyberSource
				else if(user.getPrimaryAccount().equals(AcquirerType.CYBER_SOURCE.getName())){
					for(ChargingDetails detail: paymentOptions){
						if(detail.getAcquirerName().equals(AcquirerType.CYBER_SOURCE.getName()) && mopType.equals(detail.getMopType().getCode())
								&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
							acquirerName = AcquirerType.CYBER_SOURCE.getName();
							break;
						}
					}
					if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CITRUS_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.FSS.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.FSS.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SAFEX_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SAFEX_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SBI.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.ISGPAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.BILLDESK.getName();
								break;
							}
						}
					}
				}//SafexPay
				else if(user.getPrimaryAccount().equals(AcquirerType.SAFEX_PAY.getName())){
					for(ChargingDetails detail: paymentOptions){
						if(detail.getAcquirerName().equals(AcquirerType.SAFEX_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
								&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
							acquirerName = AcquirerType.SAFEX_PAY.getName();
							break;
						}
					}
					if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CITRUS_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.FSS.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.FSS.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CYBER_SOURCE.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CYBER_SOURCE.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SBI.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.ISGPAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.BILLDESK.getName();
								break;
							}
						}
					}
				}//SBI
				else if(user.getPrimaryAccount().equals(AcquirerType.SBI.getName())){
					for(ChargingDetails detail: paymentOptions){
						if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())
								&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
							acquirerName = AcquirerType.SBI.getName();
							break;
						}
					}
					if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CITRUS_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.FSS.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.FSS.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CYBER_SOURCE.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CYBER_SOURCE.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SAFEX_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SAFEX_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.ISGPAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.BILLDESK.getName();
								break;
							}
						}
					}
				}
				//ISGPAY
				else if(user.getPrimaryAccount().equals(AcquirerType.ISGPAY.getName())){
					for(ChargingDetails detail: paymentOptions){
						if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())
								&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
							acquirerName = AcquirerType.ISGPAY.getName();
							break;
						}
					}
					if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CITRUS_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.FSS.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.FSS.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CYBER_SOURCE.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CYBER_SOURCE.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SAFEX_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SAFEX_PAY.getName();
								break;
							}
						}
					}
					else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SBI.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.BILLDESK.getName();
								break;
							}
						}
					}
				}
				//BillDesk
				else if(user.getPrimaryAccount().equals(AcquirerType.BILLDESK.getName())){
					for(ChargingDetails detail: paymentOptions){
						if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())
								&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
							acquirerName = AcquirerType.BILLDESK.getName();
							break;
						}
					}
					if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CITRUS_PAY.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.FSS.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.FSS.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.CYBER_SOURCE.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.CYBER_SOURCE.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SAFEX_PAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SAFEX_PAY.getName();
								break;
							}
						}
					}
					else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.SBI.getName();
								break;
							}
						}
					}else if(StringUtils.isBlank(acquirerName)){
						for(ChargingDetails detail: paymentOptions){
							if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())
									&& paymentType.getCode().equals(detail.getPaymentType().getCode())){
								acquirerName = AcquirerType.ISGPAY.getName();
								break;
							}
						}
					}
				}
			}
			break;
		case NET_BANKING:

			if(user.getNetbankingPrimaryAccount().equals(AcquirerType.CITRUS_PAY.getName())){
				for(ChargingDetails detail: paymentOptions){
					if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
						continue;
					}
					if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
						acquirerName = AcquirerType.CITRUS_PAY.getName();
						break;
					}
				}
				if(StringUtils.isBlank(acquirerName)){
					for(ChargingDetails detail: paymentOptions){
						if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
							continue;
						}
						if(detail.getAcquirerName().equals(AcquirerType.DIREC_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.DIREC_PAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.YESBANK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.YESBANK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.KOTAK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.KOTAK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.BILLDESK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.ISGPAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.SBI.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.ATOM.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.ATOM.getName();
							break;
						}
					}
				}
			}else if(user.getNetbankingPrimaryAccount().equals(AcquirerType.DIREC_PAY.getName())){
				for(ChargingDetails detail: paymentOptions){
					if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
						continue;
					}
					if(detail.getAcquirerName().equals(AcquirerType.DIREC_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
						acquirerName = AcquirerType.DIREC_PAY.getName();
						break;
					}
				}
				if(StringUtils.isBlank(acquirerName)){
					for(ChargingDetails detail: paymentOptions){
						if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
							continue;
						}
						if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.CITRUS_PAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.YESBANK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.YESBANK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.KOTAK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.KOTAK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.BILLDESK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.ISGPAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.SBI.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.ATOM.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.ATOM.getName();
							break;
						}
					}
				}
				//BillDesk
			}else if(user.getNetbankingPrimaryAccount().equals(AcquirerType.BILLDESK.getName())){
				for(ChargingDetails detail: paymentOptions){
					if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
						continue;
					}
					if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())){
						acquirerName = AcquirerType.BILLDESK.getName();
						break;
					}
				}
				if(StringUtils.isBlank(acquirerName)){
					for(ChargingDetails detail: paymentOptions){
						if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
							continue;
						}
						if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.CITRUS_PAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.YESBANK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.YESBANK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.KOTAK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.KOTAK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.DIREC_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.DIREC_PAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.ISGPAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.SBI.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.ATOM.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.ATOM.getName();
							break;
						}
					}
				}
			}// ISGPAY
			else if(user.getNetbankingPrimaryAccount().equals(AcquirerType.ISGPAY.getName())){
				for(ChargingDetails detail: paymentOptions){
					if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
						continue;
					}
					if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())){
						acquirerName = AcquirerType.ISGPAY.getName();
						break;
					}
				}
				if(StringUtils.isBlank(acquirerName)){
					for(ChargingDetails detail: paymentOptions){
						if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
							continue;
						}
						if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.CITRUS_PAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.YESBANK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.YESBANK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.KOTAK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.KOTAK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.DIREC_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.DIREC_PAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.BILLDESK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.SBI.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.ATOM.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.ATOM.getName();
							break;
						}
					}
				}
			}//Atom
			else if(user.getNetbankingPrimaryAccount().equals(AcquirerType.ATOM.getName())){
				for(ChargingDetails detail: paymentOptions){
					if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
						continue;
					}
					if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())){
						acquirerName = AcquirerType.ATOM.getName();
						break;
					}
				}
				if(StringUtils.isBlank(acquirerName)){
					for(ChargingDetails detail: paymentOptions){
						if(!detail.getPaymentType().getCode().equals(PaymentType.NET_BANKING.getCode())){
							continue;
						}
						if(detail.getAcquirerName().equals(AcquirerType.CITRUS_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.CITRUS_PAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.YESBANK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.YESBANK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.KOTAK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.KOTAK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.DIREC_PAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.DIREC_PAY.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.BILLDESK.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.SBI.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.SBI.getName();
							break;
						}else if(detail.getAcquirerName().equals(AcquirerType.ISGPAY.getName()) && mopType.equals(detail.getMopType().getCode())){
							acquirerName = AcquirerType.ISGPAY.getName();
							break;
						}
					}
				}
			}
			break;
		case WALLET:
			// BillDesk
			if (user.getWalletPrimaryAccount().equals(AcquirerType.BILLDESK.getName())) {
				for (ChargingDetails detail : paymentOptions) {
					if (!detail.getPaymentType().getCode().equals(PaymentType.WALLET.getCode())) {
						continue;
					}
					if (detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName())
							&& mopType.equals(detail.getMopType().getCode())) {
						acquirerName = AcquirerType.BILLDESK.getName();
						break;
					}
				}
				if (StringUtils.isBlank(acquirerName)) {
					for (ChargingDetails detail : paymentOptions) {
						if (!detail.getPaymentType().getCode().equals(PaymentType.WALLET.getCode())) {
							continue;
						}
						if (detail.getAcquirerName().equals(AcquirerType.MOBIKWIK.getName())
								&& mopType.equals(detail.getMopType().getCode())) {
							acquirerName = AcquirerType.MOBIKWIK.getName();
							break;
						} else if (detail.getAcquirerName().equals(AcquirerType.PAYTM.getName())
								&& mopType.equals(detail.getMopType().getCode())) {
							acquirerName = AcquirerType.PAYTM.getName();
							break;
						} else if (detail.getAcquirerName().equals(AcquirerType.EPAYLATER.getName())
								&& mopType.equals(detail.getMopType().getCode())) {
							acquirerName = AcquirerType.EPAYLATER.getName();
							break;
						}else if (detail.getAcquirerName().equals(AcquirerType.SAFEX_PAY.getName())
								&& mopType.equals(detail.getMopType().getCode())) {
							acquirerName = AcquirerType.SAFEX_PAY.getName();
							break;
						}
					}
				}
			} // SafexPay	
			else if (user.getWalletPrimaryAccount().equals(AcquirerType.SAFEX_PAY.getName())) {
				for (ChargingDetails detail : paymentOptions) {
					if (!detail.getPaymentType().getCode().equals(PaymentType.WALLET.getCode())) {
						continue;
					}
					if (detail.getAcquirerName().equals(AcquirerType.SAFEX_PAY.getName())
							&& mopType.equals(detail.getMopType().getCode())) {
						acquirerName = AcquirerType.SAFEX_PAY.getName();
						break;
					}
				}
				if (StringUtils.isBlank(acquirerName)) {
					for (ChargingDetails detail : paymentOptions) {
						if (!detail.getPaymentType().getCode().equals(PaymentType.WALLET.getCode())) {
							continue;
						}
						if (detail.getAcquirerName().equals(AcquirerType.BILLDESK.getName())
								&& mopType.equals(detail.getMopType().getCode())) {
							acquirerName = AcquirerType.BILLDESK.getName();
							break;
						} else if (detail.getAcquirerName().equals(AcquirerType.PAYTM.getName())
								&& mopType.equals(detail.getMopType().getCode())) {
							acquirerName = AcquirerType.PAYTM.getName();
							break;
						} else if (detail.getAcquirerName().equals(AcquirerType.MOBIKWIK.getName())
								&& mopType.equals(detail.getMopType().getCode())) {
							acquirerName = AcquirerType.MOBIKWIK.getName();
							break;
						}else if (detail.getAcquirerName().equals(AcquirerType.EPAYLATER.getName())
								&& mopType.equals(detail.getMopType().getCode())) {
							acquirerName = AcquirerType.EPAYLATER.getName();
							break;
						}
					}
				}
			}
			// END
		/*	if(mopType.equals(MopType.MOBIKWIK_WALLET.getCode())){
				acquirerName = AcquirerType.MOBIKWIK.getName();
			}else if(mopType.equals(MopType.PAYTM_WALLET.getCode())){
				acquirerName = AcquirerType.PAYTM.getName();
			}else{
				if(null == acquirerName || acquirerName.isEmpty()){
					acquirerName = AcquirerType.MOBIKWIK.getName();
				}
			}*/
			break;
		case UPI:
			if (mopType.equals(MopType.AXIS_BANK_UPI.getCode())) {
				acquirerName =AcquirerType.AXIS_BANK_UPI.getName();
			}else if (mopType.equals(MopType.SAFEXPAY_UPI.getCode())) {
				acquirerName = AcquirerType.SAFEX_PAY.getName();
			}else if (mopType.equals(MopType.GPAY.getCode())) {
				acquirerName = AcquirerType.GPAY.getName();
			}
			break;
		case EMI:
			acquirerName = AcquirerType.CITRUS_PAY.getName();
			break;
		case RECURRING_PAYMENT:
			acquirerName = AcquirerType.CITRUS_PAY.getName();
			break;
		default:
			break;
		}
		fields.put(FieldType.ACQUIRER_TYPE.getName(), getInstancefromName(acquirerName).getCode());
		return getInstancefromName(acquirerName);
	}
}
