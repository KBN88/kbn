package com.kbn.pg.core;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.kbn.commons.crypto.CryptoUtil;
import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.SystemProperties;

public class TransactionResponser {
	
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	public TransactionResponser() {
	}
	
	public void getResponse(Fields fields) throws SystemException{
		
		getOriginalTransaction(fields);				
		
		addResponseFields(fields);
		
		removeInvalidResponseFields(fields);
		
		secureResponse(fields);	
		
		updateError(fields);
		
		//Add hash in response, this should be validated by client
		addHash(fields);
	}	
	
	public void addResponseFields(Fields fields){
		addResponseDateTime(fields);
	}
	
	public void addResponseDateTime(Fields fields){
		final Date date = new Date();
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
		fields.put(FieldType.RESPONSE_DATE_TIME.getName(), simpleDateFormat.format(date));
	}
	
	public void updateError(Fields fields){
		String responseCode = fields.get(FieldType.RESPONSE_CODE.getName());
		if(null == responseCode){
			responseCode = ErrorType.UNKNOWN.getCode();
			fields.put(FieldType.RESPONSE_CODE.getName(), responseCode);
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.UNKNOWN.getResponseMessage());
		}

		if(!responseCode.equals(ErrorType.SUCCESS.getCode())){
			//This is applicable when response is not having status or invalid status
			String status = fields.get(FieldType.STATUS.getName());

			if(null == status){
				status = StatusType.ERROR.getName();
			} else if(status.equals(StatusType.APPROVED.getName()) 
					|| status.equals(StatusType.CAPTURED.getName())
					|| status.equals(StatusType.PENDING.getName())){
				fields.put(FieldType.STATUS.getName(), StatusType.ERROR.getName());
			}	
		}
	}
	
	public void secureResponse(Fields fields){
		CryptoUtil.truncateCardNumber(fields);
	}
	
	/*
	 * If this transaction is duplicate of a previous successful authorization, return original transaction response
	 */
	public void getOriginalTransaction(Fields fields){
		//If 
//		if(null == fields.get(FieldType.IS_DUPLICATE.getName()) || fields.get(FieldType.IS_DUPLICATE.getName()).equals("Y")){
//			return;
//		}
//		
//		Fields previousFields = fields.getPrevious();
//		fields.put(FieldType.ORIG_TXN_ID.getName(), previousFields.remove(FieldType.TXN_ID.getName()));
//		
//		for(String key: SystemProperties.getResponseFields()){
//			String value = previousFields.get(key);
//			if(null != value){
//				fields.put(key, value);
//			}
//		}
	}
	
	public void removeInvalidResponseFields(Fields fields){
		Fields responseFields = new Fields();
		
		for(String key: SystemProperties.getResponseFields()){
			String value = fields.get(key);
			if(null != value){
				responseFields.put(key, value);
			}
		}
		fields.clear();
		fields.putAll(responseFields.getFields());
	}
	
	public void addHash(Fields fields) throws SystemException{
		fields.put(FieldType.HASH.getName(), Hasher.getHash(fields));
	}
}
