package com.kbn.pg.core;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class NotificationDetail implements Serializable {

	private static final long serialVersionUID = -1250260298145862759L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String message;
	private String status;
	@CreationTimestamp
	private Date createDate;
	@UpdateTimestamp
	private Date viewDate;
	private String concernedUser;
	private String notifierEmailId;
	private String submittedBy;
	
	@Transient
	private String subject;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getViewDate() {
		return viewDate;
	}

	public void setViewDate(Date viewDate) {
		this.viewDate = viewDate;
	}

	public String getConcernedUser() {
		return concernedUser;
	}

	public void setConcernedUser(String concernedUser) {
		this.concernedUser = concernedUser;
	}

	public String getNotifierEmailId() {
		return notifierEmailId;
	}

	public void setNotifierEmailId(String notifierEmailId) {
		this.notifierEmailId = notifierEmailId;
	}

	public String getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(String submittedBy) {
		this.submittedBy = submittedBy;
	}

	
}
