package com.kbn.pg.core;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;

public class ProcessManager {

	private static Logger logger = Logger.getLogger(ProcessManager.class
			.getName());

	public ProcessManager() {
	}

	public static void flow(Processor processor, Fields fields,
			boolean invalidAllowed) {

		// Process security
		try {
			if (invalidAllowed) {
				processor.preProcess(fields);
				processor.process(fields);
				processor.postProcess(fields);
			} else {
				if (!fields.isValid()) {
					return;
				}
				processor.preProcess(fields);

				if (!fields.isValid()) {
					return;
				}
				processor.process(fields);

				if (!fields.isValid()) {
					return;
				}
				processor.postProcess(fields);
			}
		} catch (SystemException systemException) {
			fields.setValid(false);
			String origTxnType = fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName());

			if(systemException.getErrorType().getResponseCode().equals(ErrorType.VALIDATION_FAILED.getResponseCode())) {  
				String orderId = fields.get(FieldType.ORDER_ID.getName());
				if(StringUtils.isBlank(orderId) || !(new CrmValidator().validateField(CrmFieldType.ORDER_ID, orderId))){
					//Put order id as 000 if its invalid
					fields.put(FieldType.ORDER_ID.getName(), ErrorType.SUCCESS.getCode());
				}
				if(!StringUtils.isBlank(origTxnType)){
					fields.put(FieldType.TXNTYPE.getName(),origTxnType);
				}else{
					fields.put(FieldType.TXNTYPE.getName(),TransactionType.INVALID.getName());
				}
				if(!StringUtils.isBlank(fields.get(FieldType.INTERNAL_TXN_CHANNEL.getName()))){
					fields.put(FieldType.INTERNAL_TXN_CHANNEL.getName(),fields.get(FieldType.INTERNAL_TXN_CHANNEL.getName()));
				}else{
					fields.put(FieldType.INTERNAL_TXN_CHANNEL.getName(),TransactionType.INVALID.getName());
				}
				if(!StringUtils.isBlank(systemException.getMessage())){
					fields.put(FieldType.PG_TXN_MESSAGE.getName(), systemException.getMessage());
				}
			}

			if(fields.get(FieldType.TXNTYPE.getName()).equals(TransactionType.ENROLL.getName()) && !StringUtils.isBlank(origTxnType)){
				fields.put(FieldType.TXNTYPE.getName(), origTxnType);
			}
			fields.put(FieldType.RESPONSE_CODE.getName(), systemException.getErrorType().getCode());
			
			if(systemException.getErrorType().getResponseCode().equals(ErrorType.DENIED_BY_FRAUD.getResponseCode())){
				fields.put(FieldType.RESPONSE_MESSAGE.getName(), systemException.getErrorType().getResponseMessage());
				fields.put(FieldType.PG_TXN_MESSAGE.getName(), fields.get(Constants.PG_FRAUD_TYPE.getValue()));
			}
			
			logger.error("SystemException", systemException);

		} catch (Exception exception) {
			fields.setValid(false);

			fields.put(FieldType.RESPONSE_CODE.getName(),
					ErrorType.UNKNOWN.getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),
					ErrorType.UNKNOWN.getResponseMessage());

			logger.error("Exception", exception);
		}
	}
}
