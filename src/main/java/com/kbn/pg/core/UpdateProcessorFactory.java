package com.kbn.pg.core;

public class UpdateProcessorFactory {

	public UpdateProcessorFactory() {
	}

	public static Processor getInstance(){
		return new UpdateProcessor();
	}
}
