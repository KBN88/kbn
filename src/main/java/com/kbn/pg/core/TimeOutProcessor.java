package com.kbn.pg.core;

import org.apache.log4j.Logger;

import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class TimeOutProcessor implements Processor {
	private static Logger logger = Logger.getLogger(TimeOutProcessor.class
			.getName());

	private CryptoManager cryptoManager = CryptoManagerFactory
			.getCryptoManager();
	private String skipStatusChangeFlag;

	@Override
	public void preProcess(Fields fields) throws SystemException {
		cryptoManager.secure(fields);
		// skip status updation to timeout
		skipStatusChangeFlag = fields.get(Constants.TRANSACTION_COMPLETE_FLAG.getValue());
		if (!skipStatusChangeFlag.equals(Constants.Y_FLAG.getValue())) {
			logger.info("Changing status to time out, transaction Id: " + fields.get(FieldType.TXN_ID.getName()));
			fields.put(FieldType.STATUS.getName(), StatusType.TIMEOUT.getName());
			fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.TIMEOUT.getResponseCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.TIMEOUT.getResponseMessage());
		}
	}

	@Override
	public void process(Fields fields) throws SystemException {
		if(!skipStatusChangeFlag.equals(Constants.Y_FLAG.getValue())){
			fields.updateNewOrderDetails();
			fields.updateCurrentTransaction();
		}
		fields.removeExtraFields();
		fields.removeInternalFields();
	}

	@Override
	public void postProcess(Fields fields) throws SystemException {
		fields.put(FieldType.HASH.getName(), Hasher.getHash(fields));
		}
}
