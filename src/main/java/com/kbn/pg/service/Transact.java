package com.kbn.pg.service;

import java.util.Map;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.RequestRouter;

@WebService(serviceName = "paymentServices", targetNamespace = "http://www.27programs.com/spring-cxf-rest/services")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class Transact {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/transact")
	public Map<String, String> transact(Map<String, String> reqmap) {

		try {
			Fields fields = new Fields(reqmap);
			fields.removeInternalFields();
			//To put request blob
			fields.put(FieldType.INTERNAL_REQUEST_FIELDS.getName(), fields.getFieldsAsString());
			fields.put(FieldType.IS_MERCHANT_HOSTED.getName(),"Y");
			RequestRouter router = new RequestRouter(fields);

			Map<String,String> responseMap = router.route();
			responseMap.remove(FieldType.INTERNAL_CUSTOM_MDC.getName());
			return responseMap;
		} catch (Exception exception) {
			// Ideally this should be a non-reachable code
			return null;
		}
	}
}