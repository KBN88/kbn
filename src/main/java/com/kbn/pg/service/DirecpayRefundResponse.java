package com.kbn.pg.service;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@WebService(serviceName = "paymentServices", targetNamespace = "http://www.27programs.com/spring-cxf-rest/services")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class DirecpayRefundResponse {

	 @RequestMapping("direcpayResponse")
	  public String getResponse(@RequestParam(value="name", defaultValue="Hello World")String name){
		  return "abcd";
	  }
}
