package com.kbn.pg.fraudPrevention.model;

/**
 * @author Harpreet
 *
 */
public class FraudRuleModel {
	private String payId;
	private String email;  
	private String ipAddress;  
	private String subnetMask;
	private String domainName;
	private String status;
	private String negativeBin;
	private String negativeCard;   
	private String hourlyTxnLimit; 
	private String dailyTxnLimit; 
	private String weeklyTxnLimit; 
	private String monthlyTxnLimit; 
	private String currency;
	private String minTransactionAmount;
	private String maxTransactionAmount;
	private String issuerCountry; 
	private String userCountry;

	private String fraudType;
	private String perCardTransactionAllowed;
	
	private String responseCode;
	private String responseMsg;

	public String getPayId() {
		return payId;
	}
	
	public void setPayId(String payId) {
		this.payId = payId;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getSubnetMask() {
		return subnetMask;
	}
	
	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}

	public String getDomainName() {
		return domainName;
	}
	
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getNegativeBin() {
		return negativeBin;
	}

	public void setNegativeBin(String negativeBin) {
		this.negativeBin = negativeBin;
	}
	
	public String getNegativeCard() {
		return negativeCard;
	}
	
	public void setNegativeCard(String negativeCard) {
		this.negativeCard = negativeCard;
	}
	
	public String getHourlyTxnLimit() {
		return hourlyTxnLimit;
	}
	
	public void setHourlyTxnLimit(
			String hourlyTxnLimit) {
		this.hourlyTxnLimit = hourlyTxnLimit;
	}
	
	public String getDailyTxnLimit() {
		return dailyTxnLimit;
	}

	public void setDailyTxnLimit(
			String dailyTxnLimit) {
		this.dailyTxnLimit = dailyTxnLimit;
	}
	
	public String getWeeklyTxnLimit() {
		return weeklyTxnLimit;
	}
	
	public void setWeeklyTxnLimit(
			String weeklyTxnLimit) {
		this.weeklyTxnLimit = weeklyTxnLimit;
	}

	public String getMonthlyTxnLimit() {
		return monthlyTxnLimit;
	}
	
	public void setMonthlyTxnLimit(
			String monthlyTxnLimit) {
		this.monthlyTxnLimit = monthlyTxnLimit;
	}
	
	public String getMinTransactionAmount() {
		return minTransactionAmount;
	}
	
	public void setMinTransactionAmount(String minTransactionAmount) {
		this.minTransactionAmount = minTransactionAmount;
	}

	public String getMaxTransactionAmount() {
		return maxTransactionAmount;
	}
	
	public void setMaxTransactionAmount(String maxTransactionAmount) {
		this.maxTransactionAmount = maxTransactionAmount;
	}
	
	public String getIssuerCountry() {
		return issuerCountry;
	}

	public void setIssuerCountry(String issuerCountry) {
		this.issuerCountry = issuerCountry;
	}

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	
	public String getFraudType() {
		return fraudType;
	}

	public void setFraudType(String fraudType) {
		this.fraudType = fraudType;
	}
	
	public String getPerCardTransactionAllowed() {
		return perCardTransactionAllowed;
	}
	
	public void setPerCardTransactionAllowed(String perCardTransactionAllowed) {
		this.perCardTransactionAllowed = perCardTransactionAllowed;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}