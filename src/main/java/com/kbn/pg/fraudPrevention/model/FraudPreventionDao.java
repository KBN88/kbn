package com.kbn.pg.fraudPrevention.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.exception.SystemException;
import com.kbn.pg.fraudPrevention.util.FraudRuleType;

/**
 * @author Harpreet
 *
 */
 public class FraudPreventionDao extends HibernateAbstractDao {
	private static Logger logger = Logger.getLogger(FraudPreventionDao.class.getName());
	public FraudPreventionDao() {
		super();
	}

	public void create(FraudPrevention fraudPrevention)
			throws DataAccessLayerException {
		super.save(fraudPrevention);
	}

	public void delete(FraudPrevention fraudPrevention)
			throws DataAccessLayerException {
		super.delete(fraudPrevention);
	}

	public void update(FraudPrevention fraudPrevention)
			throws DataAccessLayerException {
		super.saveOrUpdate(fraudPrevention);
	}

	@SuppressWarnings("unchecked")
	public List<FraudPrevention> getFraudRuleList(String payId) throws ParseException {
		List<FraudPrevention> fraudPreventionRuleList = new ArrayList<FraudPrevention>();

		try {
			startOperation();
			String query = "from FraudPrevention where payId = :payId ";
			fraudPreventionRuleList = getSession().createQuery(query)
									  .setParameter("payId", payId).getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return fraudPreventionRuleList;
	}
	
 	// to fetch rules with specific FraudRuleType and payId
	@SuppressWarnings("unchecked")
	public List<FraudPrevention> getFraudRuleList(String payId, FraudRuleType fraudType) throws ParseException {
		List<FraudPrevention> fraudPreventionRuleList = new ArrayList<FraudPrevention>();
		try {
			startOperation();
			String query = "from FraudPrevention where payId = :payId AND fraudType = :fraudType ";
			fraudPreventionRuleList = getSession().createQuery(query)
					.setParameter("payId", payId)
					.setParameter("fraudType", fraudType).getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return fraudPreventionRuleList;
	}
	
	// list of fraud rules including 'ALL MERCHANTS'
	@SuppressWarnings("unchecked")
	public List<FraudPrevention> getFullFraudRuleList(String payId) throws ParseException {
		List<FraudPrevention> fraudPreventionRuleList = new ArrayList<FraudPrevention>();

		try {
			startOperation();
			fraudPreventionRuleList = getSession().createQuery("from FraudPrevention where payId = :payId or payId = 'ALL'")
			.setParameter("payId", payId)
			.setCacheable(true)
			.getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return fraudPreventionRuleList;
	}
	
	//to fetch no of SALE/AUTH txns per card for specific merchant TODO
	public long getPerCardTransactions(String payId, String cardMask) throws SystemException{
		long noOfTransactions =0;
		try(Connection connecton = DataAccessObject.getBasicConnection() ){
			String sqlQuery = "SELECT count(CARD_MASK) FROM transaction where (PAY_ID = ?) and (CARD_MASK = ?) and  (TXNTYPE = 'AUTHORISE' or TXNTYPE = 'SALE')";
			PreparedStatement statement = connecton.prepareStatement(sqlQuery);
			statement.setString(1, payId);
			statement.setString(2, cardMask);
			try(ResultSet resultSet = statement.executeQuery()){
				while(resultSet.next()){
					noOfTransactions = Long.parseLong(resultSet.getString(1));
				}
			}
		}catch (SQLException exception) {
			logger.error("Database Error while fetching getPerCardAllowedTransactions :"+exception);
		}
		return noOfTransactions;
	}
	
	// to fetch total no of txn per merchant 
	public int getPerMerchantTransactions(String payId, String startTimeStamp, String endTimeStamp){
		int noOfTransactions = 0;
		try(Connection connecton = DataAccessObject.getBasicConnection() ){
			String sqlQuery = "SELECT count(TXN_ID) FROM transaction where (PAY_ID = ?) and  (TXNTYPE = 'AUTHORISE' or TXNTYPE = 'SALE') and (CREATE_DATE between ? and ?)";
			PreparedStatement statement = connecton.prepareStatement(sqlQuery);
			statement.setString(1, payId);
			statement.setString(2, startTimeStamp);
			statement.setString(3, endTimeStamp);
			try(ResultSet resultSet = statement.executeQuery()){
				while(resultSet.next()){
					noOfTransactions = Integer.parseInt(resultSet.getString(1));
				}
			}
		}catch (SQLException exception) {
			logger.error("Database Error while fetching getPerCardAllowedTransactions :"+exception);
		}
		return noOfTransactions;
	}
	
	//check txns between specific interval
	public LinkedList<Long> getSpecificIPandIntervalTransactions(String ipAddress, String payId, Map<String, String> timeStampMap){
		LinkedList<Long> noOfTxnList = new LinkedList<Long>();
		try(Connection connecton = DataAccessObject.getBasicConnection() ){
			String sqlQuery = "(SELECT count(TXN_ID) FROM transaction where (PAY_ID = ?) and  (TXNTYPE = 'AUTHORISE' or TXNTYPE = 'SALE') and (CREATE_DATE >= ? and CREATE_DATE < ?) and (INTERNAL_CUST_IP = ?))"
					+ "UNION (SELECT count(TXN_ID) FROM transaction where (PAY_ID = ?) and  (TXNTYPE = 'AUTHORISE' or TXNTYPE = 'SALE') and (CREATE_DATE >= ? and CREATE_DATE < ?) and (INTERNAL_CUST_IP = ?))"
					+ "UNION (SELECT count(TXN_ID) FROM transaction where (PAY_ID = ?) and  (TXNTYPE = 'AUTHORISE' or TXNTYPE = 'SALE') and (CREATE_DATE >= ? and CREATE_DATE < ?) and (INTERNAL_CUST_IP = ?))"
					+ "UNION (SELECT count(TXN_ID) FROM transaction where (PAY_ID = ?) and  (TXNTYPE = 'AUTHORISE' or TXNTYPE = 'SALE') and (CREATE_DATE >= ? and CREATE_DATE < ?) and (INTERNAL_CUST_IP = ?))";
			PreparedStatement statement = connecton.prepareStatement(sqlQuery);
			statement.setString(1, payId);
			statement.setString(2, timeStampMap.get("hrlyStartStamp"));
			statement.setString(3, timeStampMap.get("currentStamp"));
			statement.setString(4, ipAddress);
			
			statement.setString(5, payId);
			statement.setString(6, timeStampMap.get("dailyStartStamp"));
			statement.setString(7, timeStampMap.get("currentStamp"));
			statement.setString(8, ipAddress);
			
			statement.setString(9, payId);
			statement.setString(10, timeStampMap.get("weekhlyStartStamp"));
			statement.setString(11, timeStampMap.get("currentStamp"));
			statement.setString(12, ipAddress);
			
			statement.setString(13, payId);
			statement.setString(14, timeStampMap.get("monthlyStartStamp"));
			statement.setString(15, timeStampMap.get("currentStamp"));
			statement.setString(16, ipAddress);
			
			try(ResultSet resultSet = statement.executeQuery()){
				while(resultSet.next()){
					noOfTxnList.add(Long.parseLong(resultSet.getString(1)));
				}
			}
		}catch (SQLException exception) {
			logger.error("Database Error while fetching getPerCardAllowedTransactions :"+exception);
		}
		return noOfTxnList;
	}

	// duplicate fraud rule checker
	public   boolean duplicateChecker(String query){
		try{
			startOperation();
			List<FraudPrevention> list = getSession().createQuery(query).getResultList();
			getTx().commit();
			if(list.size()>0){
				return true;
			}else{	
				return false;
			}
		}catch(ObjectNotFoundException exception){
			logger.error(exception);
		}catch(HibernateException exception){
			logger.error(exception);
		}finally{
			autoClose();
		}
		// if something went wrong ,it will stop from creating new rule
		return true;
	}
}