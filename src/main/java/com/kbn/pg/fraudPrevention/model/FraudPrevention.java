package com.kbn.pg.fraudPrevention.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.UpdateTimestamp;

import com.kbn.pg.core.Currency;
import com.kbn.pg.fraudPrevention.util.FraudRuleType;

/**
 * @author Sunil,Harpreet
 *
 */

@Entity
@Proxy(lazy= false)@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FraudPrevention implements Serializable,Comparable<FraudPrevention>{

	private static final long serialVersionUID = 3331114640639616608L;
	@Id
	@Column(nullable = false,unique = true)
	private Long id;
	private String payId;
	private String email;  
	private String ipAddress;  
	private String subnetMask;
	private String domainName;
	private String status;
	private String negativeBin;
	private String negativeCard;   
	private String hourlyTxnLimit; 
	private String dailyTxnLimit; 
	private String weeklyTxnLimit; 
	private String monthlyTxnLimit; 
	private String currency;
	private String minTransactionAmount;
	private String maxTransactionAmount;
	private String issuerCountry; 
	private String userCountry;
	@CreationTimestamp
	private Date createDate;
	@UpdateTimestamp
	private Date updateDate;
	
	@Enumerated(EnumType.STRING)
	private FraudRuleType fraudType;
	private String perCardTransactionAllowed;
	@Transient
	private String ruleGroupId;
	@Transient
	private String currencyName;

	@Override
	public int compareTo(FraudPrevention fraudPrevention) {
		return Integer.parseInt(this.getRuleGroupId()) < Integer.parseInt(fraudPrevention.getRuleGroupId()) ? -1:
			Integer.parseInt(this.getRuleGroupId()) == Integer.parseInt(fraudPrevention.getRuleGroupId()) ? 0 : 1;
	}

	public String getPayId() {
		return payId;
	}
	
	public void setPayId(String payId) {
		this.payId = payId;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getDomainName() {
		return domainName;
	}
	
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getNegativeBin() {
		return negativeBin;
	}
	
	public void setNegativeBin(String negativeBin) {
		this.negativeBin = negativeBin;
	}
	
	public String getNegativeCard() {
		return negativeCard;
	}
	
	public void setNegativeCard(String negativeCard) {
		this.negativeCard = negativeCard;
	}
	
	public String getMinTransactionAmount() {
		return minTransactionAmount;
	}
	
	public void setMinTransactionAmount(String minTransactionAmount) {
		this.minTransactionAmount = minTransactionAmount;
	}
	
	public String getMaxTransactionAmount() {
		return maxTransactionAmount;
	}
	
	public void setMaxTransactionAmount(String maxTransactionAmount) {
		this.maxTransactionAmount = maxTransactionAmount;
	}
	
	public String getSubnetMask() {
		return subnetMask;
	}
	
	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}
	
	public String getUserCountry() {
		return userCountry;
	}
	
	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	
	public String getIssuerCountry() {
		return issuerCountry;
	}

	public FraudRuleType getFraudType() {
		return fraudType;
	}

	public void setFraudType(FraudRuleType fraudType) {
		this.fraudType = fraudType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getHourlyTxnLimit() {
		return hourlyTxnLimit;
	}

	public void setHourlyTxnLimit(
			String hourlyTxnLimit) {
		this.hourlyTxnLimit = hourlyTxnLimit;
	}

	public String getDailyTxnLimit() {
		return dailyTxnLimit;
	}

	public void setDailyTxnLimit(
			String dailyTxnLimit) {
		this.dailyTxnLimit = dailyTxnLimit;
	}

	public String getWeeklyTxnLimit() {
		return weeklyTxnLimit;
	}

	public void setWeeklyTxnLimit(
			String weeklyTxnLimit) {
		this.weeklyTxnLimit = weeklyTxnLimit;
	}

	public String getMonthlyTxnLimit() {
		return monthlyTxnLimit;
	}

	public void setMonthlyTxnLimit(
			String monthlyTxnLimit) {
		this.monthlyTxnLimit = monthlyTxnLimit;
	}

	public String getPerCardTransactionAllowed() {
		return perCardTransactionAllowed;
	}

	public void setPerCardTransactionAllowed(String perCardTransactionAllowed) {
		this.perCardTransactionAllowed = perCardTransactionAllowed;
	}

	public void setIssuerCountry(String issuerCountry) {
		this.issuerCountry = issuerCountry;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	//returning ruleGroupId, from FraudRuleType Enum
	public String getRuleGroupId() {
		return fraudType.getCode();
	}
	
	//to return alpha code of currency for front-end
	public String getCurrencyName(){
		return Currency.getAlphabaticCode(this.currency);
	}
}