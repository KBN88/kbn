package com.kbn.pg.fraudPrevention.actionBeans;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.util.TransactionManager;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.pg.fraudPrevention.core.CheckExistingFraudRule;
import com.kbn.pg.fraudPrevention.model.FraudPrevention;
import com.kbn.pg.fraudPrevention.model.FraudPreventionDao;
import com.kbn.pg.fraudPrevention.model.FraudRuleModel;
import com.kbn.pg.fraudPrevention.util.FraudRuleType;

/**
 * @author Harpreet
 *
 */
public class FraudRuleCreator{
	
	private static Logger logger = Logger.getLogger(FraudRuleCreator.class.getName());

	public ResponseObject createFraudRule(FraudRuleModel fraudRuleModel){
		FraudPreventionDao fraudPreventionDao = new FraudPreventionDao();
		FraudPrevention fraudPrevention = new FraudPrevention();
		CheckExistingFraudRule checkExistingFraudRule = new CheckExistingFraudRule();
		ResponseObject ruleCheckResponse = new ResponseObject();
		ResponseObject finalResponse = new ResponseObject();
		
			try {
				//generating  rule id
				fraudPrevention.setId(Long.parseLong(TransactionManager.getNewTransactionId()));
				fraudPrevention.setPayId(fraudRuleModel.getPayId());
				FraudRuleType fraudRuleType = FraudRuleType.getInstance(fraudRuleModel.getFraudType());
				fraudPrevention.setFraudType(fraudRuleType);
				switch(fraudRuleType){
					case BLOCK_NO_OF_TXNS:
						fraudPrevention.setHourlyTxnLimit(fraudRuleModel.getHourlyTxnLimit());
						fraudPrevention.setDailyTxnLimit(fraudRuleModel.getDailyTxnLimit());
						fraudPrevention.setWeeklyTxnLimit(fraudRuleModel.getWeeklyTxnLimit());
						fraudPrevention.setMonthlyTxnLimit(fraudRuleModel.getMonthlyTxnLimit());
					break;
					case BLOCK_TXN_AMOUNT:
						fraudPrevention.setCurrency(fraudRuleModel.getCurrency());
						fraudPrevention.setMinTransactionAmount(fraudRuleModel.getMinTransactionAmount());
						fraudPrevention.setMaxTransactionAmount(fraudRuleModel.getMaxTransactionAmount());
					break;
					case BLOCK_IP_ADDRESS:
						if(!StringUtils.isBlank(fraudRuleModel.getIpAddress())){
							fraudPrevention.setIpAddress(fraudRuleModel.getIpAddress());
							fraudPrevention.setSubnetMask(fraudRuleModel.getSubnetMask());
						}
					break;
					case BLOCK_DOMAIN_NAME:
						fraudPrevention.setDomainName(fraudRuleModel.getDomainName());
						break;
					case BLOCK_EMAIL_ID:
						fraudPrevention.setEmail(fraudRuleModel.getEmail());
						break;
					case BLOCK_USER_COUNTRY:
						fraudPrevention.setUserCountry(fraudRuleModel.getUserCountry());
						break;
					case BLOCK_CARD_ISSUER_COUNTRY:
						fraudPrevention.setIssuerCountry(fraudRuleModel.getIssuerCountry());
					break;
					case BLOCK_CARD_BIN:
						fraudPrevention.setNegativeBin(fraudRuleModel.getNegativeBin());
						break;
					case BLOCK_CARD_NO:
						fraudPrevention.setNegativeCard(fraudRuleModel.getNegativeCard());
						break;
					case BLOCK_CARD_TXN_THRESHOLD:
						fraudPrevention.setPerCardTransactionAllowed(fraudRuleModel.getPerCardTransactionAllowed());
						break;
				}
				
				//checking the existing fraud rules 
				ruleCheckResponse = checkExistingFraudRule.exists(fraudPrevention);
				if(ruleCheckResponse.getResponseCode().equals(ErrorType.FRAUD_RULE_NOT_EXIST.getResponseCode())){
					//by default status is ACTIVE  --> TdrStatusType enum refactor
					fraudPrevention.setStatus("ACTIVE");
					fraudPreventionDao.create(fraudPrevention);	
					finalResponse.setResponseCode(ErrorType.FRAUD_RULE_SUCCESS.getResponseCode());
					finalResponse.setResponseMessage(ErrorType.FRAUD_RULE_SUCCESS.getResponseMessage());
					return finalResponse;
				}
				return ruleCheckResponse;
			} catch (Exception e) {
				logger.error(e);
				return ruleCheckResponse;
			}
	}
}