package com.kbn.pg.fraudPrevention.action;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.crm.actionBeans.SessionUserIdentifier;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.fraudPrevention.actionBeans.FraudRuleCreator;
import com.kbn.pg.fraudPrevention.model.FraudRuleModel;
import com.kbn.pg.fraudPrevention.util.FraudRuleType;
import com.opensymphony.xwork2.ModelDriven;

/**
 * @author Harpreet
 *
 */
public class AddFraudRuleAction extends AbstractSecureAction implements ModelDriven<FraudRuleModel>{

	private static Logger logger = Logger.getLogger(AddFraudRuleAction.class.getName());
	
	private static final long serialVersionUID = 1676422870817349417L;
	private FraudRuleModel fraudRuleModel = new FraudRuleModel();

	@SuppressWarnings("unchecked")
	public String execute() {
		ResponseObject responseObject = new ResponseObject();
		FraudRuleCreator ruleCreator = new FraudRuleCreator();
		try{
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			String merchantPayId = SessionUserIdentifier.getUserPayId(sessionUser, fraudRuleModel.getPayId());
			fraudRuleModel.setPayId(merchantPayId);
			responseObject = ruleCreator.createFraudRule(fraudRuleModel);
			fraudRuleModel.setResponseCode(responseObject.getResponseCode());
			fraudRuleModel.setResponseMsg(responseObject.getResponseMessage());
			return SUCCESS;
		}catch(Exception exception){
			logger.error("Fraud Prevention System - Exception :"+exception);
			return ERROR;
		}
	}

	@Override
	public FraudRuleModel getModel() {
		return fraudRuleModel;
	}
 
	public void validate() {
		CrmValidator validator = new CrmValidator();
		if ((validator.validateBlankField(fraudRuleModel.getPayId()))) {
			addFieldError(CrmFieldType.PAY_ID.getName(), validator
					.getResonseObject().getResponseMessage());
		} 
		//custom validation for payId
		else if (!fraudRuleModel.getPayId().equalsIgnoreCase(CrmFieldConstants.ALL.getValue())
				&& !validator.validateField(CrmFieldType.PAY_ID, fraudRuleModel.getPayId())) {
			addFieldError(CrmFieldType.PAY_ID.getName(), validator
					.getResonseObject().getResponseMessage());
		}
		if(validator.validateBlankField(fraudRuleModel.getFraudType())){
			addFieldError(null, ErrorType.COMMON_ERROR.getResponseMessage());
		}
		
		FraudRuleType fraudType = FraudRuleType.getInstance(fraudRuleModel.getFraudType());
		switch(fraudType){
		case BLOCK_IP_ADDRESS:
			if(!validator.validateField(CrmFieldType.FRAUD_IP_ADDRESS, fraudRuleModel.getIpAddress())){
					addFieldError(CrmFieldType.FRAUD_IP_ADDRESS.getName(), validator
							.getResonseObject().getResponseMessage());
			}
			if(!validator.validateField(CrmFieldType.FRAUD_SUBNET_MASK, fraudRuleModel.getSubnetMask())){
				addFieldError(CrmFieldType.FRAUD_SUBNET_MASK.getName(), validator
						.getResonseObject().getResponseMessage());
			}
			break;
		case BLOCK_USER_COUNTRY:
			if(!validator.validateBlankField(fraudRuleModel.getUserCountry())){
				if(!validator.validateField(CrmFieldType.FRAUD_USER_COUNTRY, fraudRuleModel.getUserCountry())){
					addFieldError(CrmFieldType.FRAUD_USER_COUNTRY.getName(), validator
							.getResonseObject().getResponseMessage());
				}
			}
			break;
		case BLOCK_CARD_BIN:
				if(!validator.validateField(CrmFieldType.FRAUD_NEGATIVE_BIN, fraudRuleModel.getNegativeBin())){
					addFieldError(CrmFieldType.FRAUD_NEGATIVE_BIN.getName(), validator
							.getResonseObject().getResponseMessage());
				}
				break;
		case BLOCK_CARD_ISSUER_COUNTRY:
				if(!validator.validateField(CrmFieldType.FRAUD_ISSUER_COUNTRY, fraudRuleModel.getIssuerCountry())){
					addFieldError(CrmFieldType.FRAUD_ISSUER_COUNTRY.getName(), validator
							.getResonseObject().getResponseMessage());
				}
				break;
		case BLOCK_CARD_NO:
			if(fraudRuleModel.getNegativeCard().length()==16){
				}else{
					addFieldError(CrmFieldType.FRAUD_NEGATIVE_CARD.getName(), validator.getResonseObject().getResponseMessage());
				}	
			break;
		case BLOCK_EMAIL_ID:
				if(!validator.isValidEmailId(fraudRuleModel.getEmail())){
					addFieldError(CrmFieldType.FRAUD_EMAIL.getName(), validator.getResonseObject().getResponseMessage());				
				}
				break;
		case BLOCK_CARD_TXN_THRESHOLD:
			if(!validator.validateField(CrmFieldType.FRAUD_MIN_TRANSACTION_AMOUNT, fraudRuleModel.getPerCardTransactionAllowed())){
				addFieldError(CrmFieldType.FRAUD_MIN_TRANSACTION_AMOUNT.getName(), validator.getResonseObject().getResponseMessage());
			}
			break;
		case BLOCK_DOMAIN_NAME:
				if(!validator.validateField(CrmFieldType.FRAUD_DOMAIN_NAME, fraudRuleModel.getDomainName())){
					addFieldError(CrmFieldType.FRAUD_DOMAIN_NAME.getName(), validator.getResonseObject().getResponseMessage());
				}
				break;
		case BLOCK_NO_OF_TXNS:
			if(!validator.validateField(CrmFieldType.FRAUD_HOURLY_TXN_LIMIT, fraudRuleModel.getHourlyTxnLimit())){
				addFieldError(CrmFieldType.FRAUD_HOURLY_TXN_LIMIT.getName(), validator.getResonseObject().getResponseMessage());
			}
			if(!validator.validateField(CrmFieldType.FRAUD_DAILY_TXN_LIMIT, fraudRuleModel.getDailyTxnLimit())){
				addFieldError(CrmFieldType.FRAUD_DAILY_TXN_LIMIT.getName(), validator.getResonseObject().getResponseMessage());
			}
			if(!validator.validateField(CrmFieldType.FRAUD_WEEKLY_TXN_LIMIT, fraudRuleModel.getWeeklyTxnLimit())){
				addFieldError(CrmFieldType.FRAUD_WEEKLY_TXN_LIMIT.getName(), validator.getResonseObject().getResponseMessage());
			}
			if(!validator.validateField(CrmFieldType.FRAUD_MONTHLY_TXN_LIMIT, fraudRuleModel.getMonthlyTxnLimit())){
				addFieldError(CrmFieldType.FRAUD_MONTHLY_TXN_LIMIT.getName(), validator.getResonseObject().getResponseMessage());
			}
			break;
		case BLOCK_TXN_AMOUNT:
			if(!validator.validateField(CrmFieldType.FRAUD_CURRENCY, fraudRuleModel.getCurrency())){
				addFieldError(CrmFieldType.FRAUD_CURRENCY.getName(), validator.getResonseObject().getResponseMessage());
			}
			if(!validator.validateField(CrmFieldType.FRAUD_MIN_TRANSACTION_AMOUNT, fraudRuleModel.getMinTransactionAmount())){
				addFieldError(CrmFieldType.FRAUD_MIN_TRANSACTION_AMOUNT.getName(), validator.getResonseObject().getResponseMessage());
			}
			if(!validator.validateField( CrmFieldType.FRAUD_MAX_TRANSACTION_AMOUNT, fraudRuleModel.getMaxTransactionAmount())){
				addFieldError(CrmFieldType.FRAUD_MAX_TRANSACTION_AMOUNT.getName(), validator.getResonseObject().getResponseMessage());
			}
			if(!(Float.parseFloat(fraudRuleModel.getMaxTransactionAmount()) >= Float.parseFloat(fraudRuleModel.getMinTransactionAmount()))){
				addFieldError(CrmFieldType.FRAUD_MIN_TRANSACTION_AMOUNT.getName()+Constants.COMMA.getValue()+CrmFieldType.FRAUD_MAX_TRANSACTION_AMOUNT.getName(), validator
						.getResonseObject().getResponseMessage());	
			}
			break;
		}		
	}
}