package com.kbn.pg.fraudPrevention.action;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.user.Merchants;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmValidator;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.fraudPrevention.model.FraudPrevention;
import com.kbn.pg.fraudPrevention.model.FraudPreventionDao;

/**
 * @author Harpreet
 *
 */
public class DeleteFraudRuleAction extends AbstractSecureAction{

	private static final long serialVersionUID = -2472986893677385590L;

	private static final Logger logger = Logger.getLogger(DeleteFraudRuleAction.class);	
	private List<Merchants> merchantList = new ArrayList<Merchants>();	
	//request param for deleteFraudPreventionRules 
	private String payId;
	private String ruleId;
	private String response;
	
	// to delete fraud rule from database
	public String execute(){
		try{
			if(!StringUtils.isBlank(ruleId)){
				User sessionUser = (User)sessionMap.get(Constants.USER.getValue());
				FraudPreventionDao fraudPreventionDao = new FraudPreventionDao();
				FraudPrevention fraudPrevention = new FraudPrevention();
				
				if(sessionUser.getUserType().equals(UserType.ADMIN)){
					fraudPrevention.setPayId(sessionUser.getPayId());
				}else if(sessionUser.getUserType().equals(UserType.MERCHANT)){
					fraudPrevention.setPayId(payId);
				}else{
					setResponse("Try again, Something went wrong!");
					return ERROR;
				}
				fraudPrevention.setId(Long.parseLong(ruleId));
				fraudPrevention.setPayId(sessionUser.getPayId());
				fraudPreventionDao.delete(fraudPrevention);
				setResponse("Fraud rule deleted successfully.");
				return SUCCESS;
			}else{
				setResponse("Try again, Something went wrong!");
				return ERROR;
			}
		}catch(Exception exception){
			logger.error("Fraud Prevention System - Exception :"+exception);
			setResponse("Try again, Something went wrong!");
			return ERROR;
		}
	}

	public void validate(){
		CrmValidator validator = new CrmValidator();
		if(!validator.validateBlankField(ruleId)){
			setResponse("Try again, Something went wrong!");			
		}
		if(!StringUtils.isBlank(payId)){
			if(!validator.validateBlankField(payId)){
				setResponse("Try again, Something went wrong!");			
			}
		}
	}
	
	public List<Merchants> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<Merchants> merchantList) {
		this.merchantList = merchantList;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}