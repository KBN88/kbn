package com.kbn.pg.fraudPrevention.action;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import com.kbn.commons.user.Merchants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.fraudPrevention.model.FraudPrevention;
import com.kbn.pg.fraudPrevention.model.FraudPreventionDao;

/**
 * @author Harpreet
 *
 */
public class ViewFraudRule extends AbstractSecureAction {

	private static final long serialVersionUID = -303816138858895252L;
	private static final Logger logger = Logger.getLogger(DeleteFraudRuleAction.class);	
	private List<FraudPrevention> fraudRuleList = new ArrayList<>();
	private String payId;
	
	//retrieving fraud rule list 
	public String execute(){
		 try{
			FraudPreventionDao fraudPreventionDao = new FraudPreventionDao();
			if(payId.equalsIgnoreCase(CrmFieldConstants.ALL.getValue())){
				fraudRuleList = fraudPreventionDao.getFraudRuleList(CrmFieldConstants.ALL.getValue());
				return SUCCESS;
			}else{
				fraudRuleList = fraudPreventionDao.getFraudRuleList(payId);
				return SUCCESS;
			}
		}catch(Exception excetpion){
			logger.error("Fraud Prevention System - Exception :"+excetpion);
			return ERROR;
		}
	}

	public void validate(){
		//TODO
	}

	public List<FraudPrevention> getFraudRuleList() {
		return fraudRuleList;
	}

	public void setFraudRuleList(List<FraudPrevention> fraudRuleList) {
		this.fraudRuleList = fraudRuleList;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}
}