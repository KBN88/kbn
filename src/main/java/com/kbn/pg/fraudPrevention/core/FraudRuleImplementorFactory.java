/**
 * 
 */
package com.kbn.pg.fraudPrevention.core;


/**
 * @author Harpreet
 *
 */
public class FraudRuleImplementorFactory {

	public static FraudRuleImplementor getInstance(){
		return new FraudRuleImplementor();
	}
}
