package com.kbn.pg.fraudPrevention.core;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.pg.fraudPrevention.model.FraudPrevention;
import com.kbn.pg.fraudPrevention.model.FraudPreventionDao;
import com.kbn.pg.fraudPrevention.util.FraudRuleType;

/**
 * @author Harpreet
 *
 */
public class CheckExistingFraudRule {
private static Logger logger = Logger.getLogger(CheckExistingFraudRule.class);
	private ErrorType responseErrorType ; //TODO need to review variable scope
	
	public ResponseObject exists(FraudPrevention fraudPrevention){	
		StringBuilder querySb = new StringBuilder();
		FraudPreventionDao fraudPreventionDao = new FraudPreventionDao();
		ResponseObject response = new ResponseObject();		
		//setting payId according to type of user
		String payId = fraudPrevention.getPayId();
		FraudRuleType fraudType = fraudPrevention.getFraudType();
		switch(fraudType){
		case BLOCK_NO_OF_TXNS:
			querySb.append("from FraudPrevention where fraudType = '");
			querySb.append(fraudType);
			querySb.append("' and payId = '");
			querySb.append(payId);
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_SINGLE_ENTRY_ERROR;
			break;
		case BLOCK_TXN_AMOUNT:
			querySb.append("from FraudPrevention where");
			querySb.append(" payId = '");
			querySb.append(payId);
			querySb.append("'");
			querySb.append(" and currency = '");
			querySb.append(fraudPrevention.getCurrency());
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_ALREADY_EXISTS;
			break;
		case BLOCK_IP_ADDRESS:
			 querySb.append("from FraudPrevention where ipAddress = '");
			 querySb.append(fraudPrevention.getIpAddress());
			 querySb.append("' and subnetMask ='");
			 querySb.append(fraudPrevention.getSubnetMask()) ;
			querySb.append("' and payId = '");
			querySb.append(payId);
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_ALREADY_EXISTS;
			break;
		case BLOCK_DOMAIN_NAME:
			querySb.append("from FraudPrevention where domainName = '");
			querySb.append(fraudPrevention.getDomainName());
			querySb.append("' and payId = '");
			querySb.append(payId);
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_ALREADY_EXISTS;
			break;
		case BLOCK_EMAIL_ID:
			querySb.append("from FraudPrevention where email = '");
			querySb.append(fraudPrevention.getEmail());
			querySb.append("' and payId = '");
			querySb.append(payId);
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_ALREADY_EXISTS;
			break;
		case BLOCK_USER_COUNTRY:
			querySb.append("from FraudPrevention where userCountry = '");
			querySb.append(fraudPrevention.getUserCountry());
			querySb.append("' and payId = '");
			querySb.append(payId);
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_ALREADY_EXISTS;
			break;
		case BLOCK_CARD_ISSUER_COUNTRY:
			querySb.append("from FraudPrevention where issuerCountry = '");
			querySb.append(fraudPrevention.getIssuerCountry());
			querySb.append("' and payId = '");
			querySb.append(payId);
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_ALREADY_EXISTS;
			break;
		case BLOCK_CARD_BIN:
			querySb.append("from FraudPrevention where negativeBin = '");
			querySb.append(fraudPrevention.getNegativeBin());
			querySb.append("' and payId = '");
			querySb.append(payId);
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_ALREADY_EXISTS;
			break;
		case BLOCK_CARD_NO:
			querySb.append("from FraudPrevention where negativeCard = '");
			querySb.append(fraudPrevention.getNegativeCard());
			querySb.append("' and payId = '");
			querySb.append(payId);
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_ALREADY_EXISTS;
			break;
		case BLOCK_CARD_TXN_THRESHOLD:
			querySb.append("from FraudPrevention where fraudType = '");
			querySb.append(FraudRuleType.BLOCK_CARD_TXN_THRESHOLD);
			querySb.append("' and payId = '");
			querySb.append(payId);
			querySb.append("'");
			responseErrorType = ErrorType.FRAUD_RULE_SINGLE_ENTRY_ERROR;
			break;
		default:
			logger.error("Something went wrong while checking fraud field values");
			break;
		}
		if(fraudPreventionDao.duplicateChecker(querySb.toString())){
			response.setResponseMessage(responseErrorType.getResponseMessage());
			response.setResponseCode(responseErrorType.getCode());
		}else{
			response.setResponseMessage(ErrorType.FRAUD_RULE_NOT_EXIST.getResponseMessage());
			response.setResponseCode(ErrorType.FRAUD_RULE_NOT_EXIST.getCode());
		}
		return response;
	}
}
