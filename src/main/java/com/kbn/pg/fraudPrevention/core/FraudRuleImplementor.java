package com.kbn.pg.fraudPrevention.core;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.DateCreater;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.SystemConstants;
import com.kbn.pg.common.util.FraudPreventionUtil;
import com.kbn.pg.core.Amount;
import com.kbn.pg.fraudPrevention.model.FraudPrevention;
import com.kbn.pg.fraudPrevention.model.FraudPreventionDao;
import com.kbn.pg.fraudPrevention.util.FraudRuleType;

/**
 * @author Harpreet
 *
 */

public class FraudRuleImplementor {

	private static Logger logger = Logger.getLogger(FraudRuleImplementor.class);

	public void applyRule(Fields fields)throws SystemException{
		FraudPreventionDao fraudPreventionDao = new FraudPreventionDao();
		List<FraudPrevention> fraudPreventionRuleList = new ArrayList<FraudPrevention>();
		try{
			fraudPreventionRuleList = fraudPreventionDao.getFullFraudRuleList(fields.get(FieldType.PAY_ID.getName()));
			// sorting the list according to ruleGroupId 
			// performace optimization 
			Collections.sort(fraudPreventionRuleList);
			
			for(FraudPrevention fraudPrevention: fraudPreventionRuleList){
				final String payId = fields.get(FieldType.PAY_ID.getName());
				final FraudRuleType fraudType = fraudPrevention.getFraudType();
				switch(fraudType){
				case BLOCK_TXN_AMOUNT:
					String currencyCode = fields.get(FieldType.CURRENCY_CODE.getName());
					if(currencyCode.equalsIgnoreCase(fraudPrevention.getCurrency())){
						String tempRequestAmount = fields.get(fraudType.getFieldName());
						Double requestAmount = Double.parseDouble(Amount.toDecimal(tempRequestAmount, currencyCode));
						if(!((requestAmount >= Double.parseDouble(fraudPrevention.getMinTransactionAmount())
								&& (requestAmount <= Double.parseDouble(fraudPrevention.getMaxTransactionAmount()))))){
							fields.put(Constants.PG_FRAUD_TYPE.getValue(), fraudPrevention.getFraudType().getValue());
							fields.put(FieldType.STATUS.getName(),StatusType.DENIED_BY_FRAUD.getName());
							throw new SystemException(ErrorType.DENIED_BY_FRAUD, "Fraud transaction with PAY_ID="+fields.get(CrmFieldType.PAY_ID.getName())+" detected and blocked"+"with FRAUD_TYPE: "+fraudType.getValue());
						}	
					}					
					break;
				case BLOCK_NO_OF_TXNS:
					String ipAddress = fields.get(FieldType.INTERNAL_CUST_IP.getName());
					LocalDateTime currentDateTime = DateCreater.now();
					String currentStamp = currentDateTime.format(DateCreater.dateTimeFormat);

					String hrlyStartTimeStamp = DateCreater.subtractHours(currentDateTime, 1);	
					String dailyStartTimeStamp = DateCreater.subtractDays(currentDateTime, 1);
					String weeklyStartTimeStamp = DateCreater.subtractWeeks(currentDateTime, 1);
					String monthlyStartTimeStamp = DateCreater.subtractMonths(currentDateTime, 1);
					Map<String, String> timeStampMap = new HashMap<String, String>();
					timeStampMap.put("currentStamp", currentStamp);
					timeStampMap.put("hrlyStartStamp", hrlyStartTimeStamp);
					timeStampMap.put("dailyStartStamp", dailyStartTimeStamp);
					timeStampMap.put("weekhlyStartStamp", weeklyStartTimeStamp);
					timeStampMap.put("monthlyStartStamp", monthlyStartTimeStamp);
					
					LinkedList<Long> noOfTxnList = fraudPreventionDao.getSpecificIPandIntervalTransactions(ipAddress, payId, timeStampMap);
					LinkedList<Long> noOfTxnAllowedList = new LinkedList<Long>();
					noOfTxnAllowedList.add(Long.parseLong(fraudPrevention.getHourlyTxnLimit()));
					noOfTxnAllowedList.add(Long.parseLong(fraudPrevention.getDailyTxnLimit()));
					noOfTxnAllowedList.add(Long.parseLong(fraudPrevention.getWeeklyTxnLimit()));
					noOfTxnAllowedList.add(Long.parseLong(fraudPrevention.getMonthlyTxnLimit()));

					for(long noOfTxn:noOfTxnList){
						for(long noOfTxnAllowed:noOfTxnAllowedList){
							if(!(noOfTxn < noOfTxnAllowed)){
								fields.put(Constants.PG_FRAUD_TYPE.getValue(), fraudPrevention.getFraudType().getValue());
								fields.put(FieldType.STATUS.getName(),StatusType.DENIED_BY_FRAUD.getName());
								throw new SystemException(ErrorType.DENIED_BY_FRAUD, "Fraud transaction with PAY_ID="+fields.get(CrmFieldType.PAY_ID.getName())+" detected and blocked"+"with FRAUD_TYPE: "+fraudType.getValue());
							}
						}
					}
					break;
				case BLOCK_DOMAIN_NAME:
					break;
				case BLOCK_IP_ADDRESS:
					validateRequestField(fields, fraudPrevention, fraudPrevention.getIpAddress());
					break;
				case BLOCK_EMAIL_ID:
					validateRequestField(fields, fraudPrevention, fraudPrevention.getEmail());
					break;
				case BLOCK_CARD_ISSUER_COUNTRY:
					validateRequestField(fields, fraudPrevention, fraudPrevention.getIssuerCountry());
					break;
				case BLOCK_USER_COUNTRY:
					validateRequestField(fields, fraudPrevention, fraudPrevention.getUserCountry());
					break;
				case BLOCK_CARD_NO:
					//checking fraud status of card
					if(fields.contains(fraudType.getFieldName())){
						String cardMask = FraudPreventionUtil.makeCardMask(fields);
						if(cardMask.equals(fraudPrevention.getNegativeCard())){
							fields.put(Constants.PG_FRAUD_TYPE.getValue(), fraudPrevention.getFraudType().getValue());
							fields.put(FieldType.STATUS.getName(),StatusType.DENIED_BY_FRAUD.getName());
							throw new SystemException(ErrorType.DENIED_BY_FRAUD, "Fraud transaction with PAY_ID="+fields.get(CrmFieldType.PAY_ID.getName())+" detected and blocked"+"with FRAUD_TYPE: "+fraudType.getValue());
						}
					}
					break;
				case BLOCK_CARD_BIN:
					if(fields.contains(fraudType.getFieldName())){
						String reqCardNo = fields.get(FieldType.CARD_NUMBER.getName());
						String reqBin = reqCardNo.substring(0, SystemConstants.CARD_BIN_LENGTH );
						if(reqBin.equals(fraudPrevention.getNegativeBin())){
							fields.put(Constants.PG_FRAUD_TYPE.getValue(), fraudPrevention.getFraudType().getValue());
							fields.put(FieldType.STATUS.getName(),StatusType.DENIED_BY_FRAUD.getName());
							throw new SystemException(ErrorType.DENIED_BY_FRAUD, "Fraud transaction with PAY_ID="+fields.get(CrmFieldType.PAY_ID.getName())+" detected and blocked"+"with FRAUD_TYPE: "+fraudType.getValue());
						}
					}
					break;
				case BLOCK_CARD_TXN_THRESHOLD:
					if(fields.contains(fraudType.getFieldName())){
						String cardMask2 = FraudPreventionUtil.makeCardMask(fields);
						long noOfTransctions = fraudPreventionDao.getPerCardTransactions(fields.get(FieldType.PAY_ID.getName()),cardMask2);
						long perCardTxnAllowed = Long.parseLong(fraudPrevention.getPerCardTransactionAllowed());
						if(!(noOfTransctions < perCardTxnAllowed)){
							fields.put(Constants.PG_FRAUD_TYPE.getValue(), fraudPrevention.getFraudType().getValue());
							fields.put(FieldType.STATUS.getName(),StatusType.DENIED_BY_FRAUD.getName());
							throw new SystemException(ErrorType.DENIED_BY_FRAUD, "Fraud transaction with PAY_ID="+fields.get(CrmFieldType.PAY_ID.getName())+" detected and blocked"+"with FRAUD_TYPE: "+fraudType.getValue());
						}
					}
					break;
				default: //logically not reachable
					logger.error("Something went wrong while checking fraud field values");
					break;
				}
			}
			
		}catch (ParseException e) {
			logger.error(e);
		}
	}
	
	//to validate fields of same category
	private void validateRequestField(Fields fields,final FraudPrevention fraudPrevention, final String fraudFieldValue)throws SystemException{
		//checking other fields
		FraudRuleType fraudType = fraudPrevention.getFraudType();
		if(fields.contains(fraudType.getFieldName())){
			if(fields.get(fraudType.getFieldName()).equalsIgnoreCase(fraudFieldValue)){
				fields.put(Constants.PG_FRAUD_TYPE.getValue(), fraudPrevention.getFraudType().getValue());
				fields.put(FieldType.STATUS.getName(),StatusType.DENIED_BY_FRAUD.getName());
				throw new SystemException(ErrorType.DENIED_BY_FRAUD, "Fraud transaction with PAY_ID="+fields.get(CrmFieldType.PAY_ID.getName())+" detected and blocked"+"with FRAUD_TYPE: "+fraudType.getValue());
			}
		}
	}
}