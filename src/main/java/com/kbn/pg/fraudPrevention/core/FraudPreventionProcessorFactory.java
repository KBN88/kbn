/**
 * 
 */
package com.kbn.pg.fraudPrevention.core;


/**
 * @author Harpreet
 *
 */
public class FraudPreventionProcessorFactory {
	
	public static FraudPreventionProcessor getInstance(){
		return new FraudPreventionProcessor();
	}
}
