/**
 * 
 */
package com.kbn.pg.fraudPrevention.core;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Processor;

/**
 * @author Harpreet
 *
 */
public class FraudPreventionProcessor implements Processor {

	FraudRuleImplementor implementor = FraudRuleImplementorFactory.getInstance();
	
	@Override
	public void preProcess(Fields fields) throws SystemException {
		
	}

	@Override
	public void process(Fields fields) throws SystemException {
		//fraud rule switch 
		implementor.applyRule(fields);
	}

	@Override
	public void postProcess(Fields fields) throws SystemException {
		
	}
}
