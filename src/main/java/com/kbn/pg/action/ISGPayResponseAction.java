package com.kbn.pg.action;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.isgpay.ISGPayDecryption;
import com.kbn.isgpay.ISGPayResponseProcessor;
import com.kbn.isgpay.ISGPayTransactionFetchService;
import com.kbn.isgpay.ISGPayTransformer;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;
import com.opensymphony.xwork2.Action;

public class ISGPayResponseAction extends AbstractSecureAction implements ServletRequestAware {

	/**
	 * @Vikash
	 */
	private static final long serialVersionUID = 8271643232079343646L;
	private static Logger logger = Logger.getLogger(ISGPayResponseAction.class.getName());
	private HttpServletRequest httpRequest;

	@SuppressWarnings("unchecked")
	public String execute() {
		try {

			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			LinkedHashMap<String, String> hmDecryptedValue = new LinkedHashMap<String, String>();

			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					hmDecryptedValue.put(entry.getKey().trim(), ((String[]) entry.getValue())[0].trim());
				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			// Fetching encryption key and secure key from User table
			ISGPayTransactionFetchService isgPayTransactionFetchService = new ISGPayTransactionFetchService();
			Map<String, String> requestMap = isgPayTransactionFetchService
					.fetchISGPayFields(hmDecryptedValue.get("TerminalId"));
			String secureSecret = requestMap.get("secureKey");
			String decryptionKey = requestMap.get("encryptionKey");

			// decrypting ISGPay response
			ISGPayDecryption decObj = new ISGPayDecryption();
			decObj.decrypt(hmDecryptedValue, decryptionKey, secureSecret);

			Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());
			logger.info("Message recieved from ISGPAY  " + hmDecryptedValue);
			// TODO Blob response Fields
			//fields.put(FieldType.RESPONSE_FIELDS.getName(), hmDecryptedValue.toString());
			logger.info("Session fields value" + fields.get(FieldType.PAY_ID.getName()));
			logger.info("Session fields value" + fields.get(FieldType.TXN_ID.getName()));
			fields.logAllFields("session fields for ISGPAY");
			if (null != fields) {
				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);
			}

			String responseString = hmDecryptedValue.get("ErrorMessage");
			logger.info("Message recieved from ISGPAY  " + responseString);

			ISGPayTransformer isgPayTransformer = new ISGPayTransformer(hmDecryptedValue);
			isgPayTransformer.updateISGPayResponse(fields);
			try {
				Processor processor = new ISGPayResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);

				// Fetch user for Re-try Transaction ,Emailer and Sms Processor

				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());
						return "paymentPage";
					}
				}
				// Retry Transaction Block End

				// Sending Email for Transaction Status to merchant
				fields.put(FieldType.PAY_ID.getName(), fields.get(FieldType.PAY_ID.getName()));
				fields.put(FieldType.ACQUIRER_TYPE.getName(),
						(String) sessionMap.get(FieldType.ACQUIRER_TYPE.getName()));

				String countryCode = (String) sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);

				fields.put(FieldType.RETURN_URL.getName(), (String) sessionMap.get(FieldType.RETURN_URL.getName()));

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				// TODO....update fields as error??
				logger.error("Exception", systemException);
			}
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
			if (sessionMap != null) {
				sessionMap.clear();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return Action.NONE;
	}

	@Override
	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;

	}
}
