package com.kbn.pg.action;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.axisupi.AxisBankTransactionService;
import com.kbn.axisupi.AxisBankTransformer;
import com.kbn.axisupi.AxisResponseProcessor;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;
import com.opensymphony.xwork2.Action;

public class AxisBankResponseAction extends AbstractSecureAction implements ServletRequestAware {
	private static Logger logger = Logger.getLogger(AxisBankResponseAction.class.getName());
	/**
	 * @ neeraj
	 */
	private static final long serialVersionUID = -3812859523254750431L;
	private HttpServletRequest httpRequest;
	private AxisBankTransactionService axisBankTransactionService = new AxisBankTransactionService();

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> requestMap = new HashMap<String, String>();

			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					requestMap.put(entry.getKey(), ((String[]) entry.getValue())[0]);

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			String encryptedData = "";
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = httpRequest.getReader();
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			encryptedData = builder.toString();
			logger.info("Response Json Data:" + encryptedData);
			JSONParser parser = new JSONParser();
			Object obj = null;
			try {
				obj = parser.parse(encryptedData);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			JSONObject jObject = (JSONObject) obj;
			logger.info("GET Response Data:" + jObject.get("data").toString());
			//String data = AxisAesUtil.decrypt(jObject.get("data").toString(), "a2UUImt13urnku2H");//UAT
			String data = AxisAesUtil.decrypt(jObject.get("data").toString(), "RCQ8ls2hPQeyXpQt");
			
			logger.info("Message recieved from Axis BANK:" + data);
			JSONParser parser1 = new JSONParser();
			Object obj1 = null;
			try {
				obj1 = parser1.parse(data);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			JSONObject jObject1 = (JSONObject) obj1;
			//{"customerVpa":"9883488526@upi","merchantId":"BHARTIPROD14773257019","merchantChannelId":"BHARTIPRODAPP14773257019","merchantTransactionId":"1912112129086029","transactionTimestamp":"2019-12-11T21:26:42+05:30","transactionAmount":"500.00","gatewayTransactionId":"AXI23f91fbda0124af7aa54a86a816e955c","gatewayResponseCode":"00","gatewayResponseMessage":"Success","rrn":"934521969240","checksum":"3D22B0861CB7752BB0F637FEB20C79D37E84AEC818E178D41B25AE88639DEC75690DF50D3FE9CB713750B51631A3206027FEFB9CFE6B49C3D9F1CD3C63954551C79C4B7AA06115A20405334557F721CB9E40C6AF6CD1599ED413E096F85B9123AE0D9C1EEBFB36319E064D6EF692E54640FE8B12AFE71CD4F0851AF159432FFBBD2166564F5F6EE5ABEAF3606244C84D67BD3993CCEA34CF86E566F131E125C85925B7F47259396BE81B3837546E454ED6DD0201A3F3AA7092B99132436D923D60DC79F4679602AA40DBE44D38A05E4EAA0460605CFECB3F5580BDF5175C8FA6D8E7AFF1C745966D1B8ACDD50821B350AA1811CC138C4DA4AE6B3E6297D1B67C"}
			String Txn_Id = jObject1.get("merchantTransactionId").toString();
			logger.info("Txn Id :" + Txn_Id);
			Map<String, String> field = axisBankTransactionService.fetchOrderID(Txn_Id);
			logger.info("Session fields value1" + field.get(FieldType.PAY_ID.getName()));
			Fields fields = new Fields(field);
		
			// TODO Blob response Fields
			fields.put(FieldType.RESPONSE_FIELDS.getName(), data.toString());
			logger.info("Session fields value" + fields.get(FieldType.PAY_ID.getName()));
			fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(),fields.get(FieldType.TXN_ID.getName()));
			fields.logAllFields("session fields for Axis BANK");
			if (null != fields) {
				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);
			}
			AxisBankTransformer airtelBankTransformer = new AxisBankTransformer(data);
			airtelBankTransformer.updateAxisBankResponse(fields);

			try {
				Processor processor = new AxisResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);

				// Fetch user for Re-try Transaction ,Emailer and Sms Processor

				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());
						return "paymentPage";
					}
				}

				// Retry Transaction Block End

				// Sending Email for Transaction Status to merchant
				fields.put(FieldType.PAY_ID.getName(), fields.get(FieldType.PAY_ID.getName()));
				fields.put(FieldType.ACQUIRER_TYPE.getName(), fields.get(FieldType.ACQUIRER_TYPE.getName()));

				 String countryCode = (String)
				 sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				 if(countryCode==null) {
					 countryCode ="NA";
				 }
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);
				 fields.put(FieldType.RETURN_URL.getName(),"https://merchant.mmadpay.com/crm/jsp/response.jsp");

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				// TODO....update fields as error??
				logger.error("Exception", systemException);
			}
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
			if (sessionMap != null) {
				sessionMap.clear();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
			exception.printStackTrace();
		}
		return Action.NONE;
	}

	@Override
	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

}
