package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.simple.JSONObject;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.RequestRouter;
import com.kbn.pg.core.pageintegrator.AcquireIntegratorFactory;

class GpayUpiRequestAction extends AbstractSecureAction implements ServletResponseAware {
	/**
	 * 
	 * @author bpd0004
	 */
	private static final long serialVersionUID = 4711177695379138512L;
	private static Logger logger = Logger.getLogger(GpayUpiRequestAction.class.getName());
	private static final String pendingTxnStatus = "Sent to Bank-Enrolled";
	private String payId;
	private String amount;
	private String merchantName;
	private String businessName;
	private String orderId;
	private String origTxnType;
	private String redirectUrl;
	private String paymentType;
	private String mopType;
	private String responseCode;
	private String acquirerFlag;
	private String responseUrl;
	private String currencyCode;
	private String hash;
	private String returnUrl;
	private String tokenId;
	private String defaultLanguage;
	private String vpa;
	private String vpaPhone;
	private String encodeURL;
	private Map<String, Object> supportedPaymentTypeMap = new HashMap<String, Object>();
	private Map<String, Object> cardPaymentTypeMap = new HashMap<String, Object>();
	private Map<String, Object> tokenMap = new HashMap<String, Object>();
	HttpServletResponse response;

//GPAY 
	public String upiGpayHandler() {

		try {
			Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());
			if (null != fields)
				logger.info(fields.getFieldsAsString());
			else {
				logger.info("session fields lost");
				return ERROR;
			}

			fields.put(FieldType.PAYMENT_TYPE.getName(), getPaymentType());
			fields.put(FieldType.MOP_TYPE.getName(), getMopType());

			if (PaymentType.UPI.getCode().equals(getPaymentType())) {
				fields.put(FieldType.MOP_TYPE.getName(), "503");
				fields.put(FieldType.UPI.getName(), getVpaPhone());

				sessionMap.put((FieldType.INTERNAL_ORIG_TXN_TYPE.getName()), TransactionType.SALE.getName());
				fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
				fields.put(FieldType.TXNTYPE.getName(), TransactionType.SALE.getName());

			}
			String txnId = (String) sessionMap.get(FieldType.INTERNAL_ORIG_TXN_ID.getName());
			fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), txnId);
			sessionMap.put((FieldType.CARD_NUMBER.getName()), fields.get(FieldType.CARD_NUMBER.getName()));
			sessionMap.put((FieldType.CARD_EXP_DT.getName()), fields.get(FieldType.CARD_EXP_DT.getName()));
			sessionMap.put((FieldType.CVV.getName()), fields.get(FieldType.CVV.getName()));
			sessionMap.put((FieldType.INTERNAL_CARD_ISSUER_BANK.getName()),
					fields.get(FieldType.INTERNAL_CARD_ISSUER_BANK.getName()));
			sessionMap.put((FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()),
					fields.get(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()));
			fields.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(), "N");
			RequestRouter router = new RequestRouter(fields);
			fields.putAll(router.route());
			Processor processor = AcquireIntegratorFactory.instance(fields);

			processor.preProcess(fields);
			processor.process(fields);
			processor.postProcess(fields);

			// Retry Transaction Block Start
			String status = fields.get(FieldType.STATUS.getName());
			if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())
					&& !(status.equals(StatusType.APPROVED.getName())
							|| (status.equals(StatusType.CAPTURED.getName())))) {
				// Fetch User for retryTransaction
				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
				RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
				if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
					addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());

					return "paymentPage";
				}

			} // Retry Transaction Block End
			fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), txnId);
			if (!(pendingTxnStatus.contains(status))) {
				sessionMap.invalidate();
				acquirerFlag = NONE;
			} else {
				acquirerFlag = NONE;
			}

			Map<String, Object> resMap = new HashMap<String, Object>();

			resMap.put("RESPONSE_CODE", fields.get(FieldType.RESPONSE_CODE.getName()));
			resMap.put("STATUS", fields.get(FieldType.STATUS.getName()));
			resMap.put("RESPONSE_MESSAGE", fields.get(FieldType.RESPONSE_MESSAGE.getName()));
			resMap.put("TXNTYPE", fields.get(FieldType.TXNTYPE.getName()));
			resMap.put("TXN_ID", fields.get(FieldType.TXN_ID.getName()));
			resMap.put("RESPONSE_DATE_TIME", fields.get(FieldType.RESPONSE_DATE_TIME.getName()));
			resMap.put("INTERNAL_ACQUIRER_TYPE", fields.get(FieldType.INTERNAL_ACQUIRER_TYPE.getName()));
			resMap.put("CUST_PHONE", fields.get(FieldType.CUST_PHONE.getName()));
			resMap.put("CURRENCY_CODE", fields.get(FieldType.CURRENCY_CODE.getName()));
			resMap.put("PRODUCT_DESC", fields.get(FieldType.PRODUCT_DESC.getName()));
			resMap.put("AMOUNT", fields.get(FieldType.AMOUNT.getName()));
			//resMap.put("TXN_KEY", fields.get(FieldType.TXN_KEY.getName()));
			resMap.put("INTERNAL_ORIG_TXN_ID", fields.get(FieldType.INTERNAL_ORIG_TXN_ID.getName()));
			resMap.put("HASH", fields.get(FieldType.HASH.getName()));
			resMap.put("PAYMENT_TYPE", fields.get(FieldType.PAYMENT_TYPE.getName()));
			resMap.put("RETURN_URL", fields.get(FieldType.RETURN_URL.getName()));
			resMap.put("PAY_ID", fields.get(FieldType.PAY_ID.getName()));
			resMap.put("ORDER_ID", fields.get(FieldType.ORDER_ID.getName()));
			resMap.put("CUST_NAME", fields.get(FieldType.CUST_NAME.getName()));

			Map<String, Object> resMap1 = new HashMap<String, Object>();

			resMap1.put("responseCode", fields.get(FieldType.RESPONSE_CODE.getName()));
			resMap1.put("transactionStatus", fields.get(FieldType.STATUS.getName()));
			resMap1.put("responseMessage", fields.get(FieldType.RESPONSE_MESSAGE.getName()));
			resMap1.put("txnType", fields.get(FieldType.TXNTYPE.getName()));
			resMap1.put("pgRefNum", fields.get(FieldType.TXN_ID.getName()));
			resMap1.put("RETURN_URL", fields.get(FieldType.RETURN_URL.getName()));

			resMap1.put("responseFields", resMap);

			JSONObject jObject = new JSONObject(resMap1);

			response.setContentType("application/json");
			response.getWriter().flush();
			response.getWriter().println(jObject);
			response.getWriter().close();
		} catch (Exception exception) {
			logger.error("Error handling of transaction", exception);
			exception.printStackTrace();
			return ERROR;
		}
		return "success";
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrigTxnType() {
		return origTxnType;
	}

	public void setOrigTxnType(String origTxnType) {
		this.origTxnType = origTxnType;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getMopType() {
		return mopType;
	}

	public void setMopType(String mopType) {
		this.mopType = mopType;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getAcquirerFlag() {
		return acquirerFlag;
	}

	public void setAcquirerFlag(String acquirerFlag) {
		this.acquirerFlag = acquirerFlag;
	}

	public String getResponseUrl() {
		return responseUrl;
	}

	public void setResponseUrl(String responseUrl) {
		this.responseUrl = responseUrl;
	}

	public Map<String, Object> getSupportedPaymentTypeMap() {
		return supportedPaymentTypeMap;
	}

	public void setSupportedPaymentTypeMap(Map<String, Object> supportedPaymentTypeMap) {
		this.supportedPaymentTypeMap = supportedPaymentTypeMap;
	}

	public Map<String, Object> getCardPaymentTypeMap() {
		return cardPaymentTypeMap;
	}

	public void setCardPaymentTypeMap(Map<String, Object> cardPaymentTypeMap) {
		this.cardPaymentTypeMap = cardPaymentTypeMap;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public Map<String, Object> getTokenMap() {
		return tokenMap;
	}

	public void setTokenMap(Map<String, Object> tokenMap) {
		this.tokenMap = tokenMap;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Override
	public void setServletResponse(HttpServletResponse response) {
		// TODO Auto-generated method stub
		this.response = response;
	}

	public String getVpa() {
		return vpa;
	}

	public void setVpa(String vpa) {
		this.vpa = vpa;
	}

	public String getEncodeURL() {
		return encodeURL;
	}

	public void setEncodeURL(String encodeURL) {
		this.encodeURL = encodeURL;
	}

	public String getVpaPhone() {
		return vpaPhone;
	}

	public void setVpaPhone(String vpaPhone) {
		this.vpaPhone = vpaPhone;
	}

}
