package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.amex.ResponseProcessor;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;

/**
 * @author Sunil, Neeraj
 *
 */
public class AmexResponseAction extends AbstractSecureAction implements
		ServletRequestAware {

	private static Logger logger = Logger.getLogger(AmexResponseAction.class
			.getName());
	private static final long serialVersionUID = 2943629615913246334L;

	private HttpServletRequest httpRequest;

	public AmexResponseAction() {
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(),
							((String[]) entry.getValue())[0].trim());

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());
			fields.putAll(responseMap);
			SecurityProcessor securityProcessor = new SecurityProcessor();
			securityProcessor.authenticate(fields);
			securityProcessor.addAcquirerFields(fields);

			fields.logAllFields("Amex 3DS map: ");
			try {

				fields.put(FieldType.CARD_NUMBER.getName(), (String) sessionMap
						.get(FieldType.CARD_NUMBER.getName()));
				fields.put(FieldType.CARD_EXP_DT.getName(), (String) sessionMap
						.get(FieldType.CARD_EXP_DT.getName()));
				fields.put(FieldType.CVV.getName(),
						(String) sessionMap.get(FieldType.CVV.getName()));
				fields.put((FieldType.INTERNAL_CARD_ISSUER_BANK.getName()),
						(String) sessionMap
								.get(FieldType.INTERNAL_CARD_ISSUER_BANK
										.getName()));
				fields.put((FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()),
						(String) sessionMap
								.get(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
										.getName()));

				fields.put(FieldType.OID.getName(),
						fields.get(FieldType.INTERNAL_ORIG_TXN_ID.getName()));

				Processor processor = new ResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);
				// run time transaction failed retry to payment page
				fields.put(FieldType.ACQUIRER_TYPE.getName(),
						(String) sessionMap.get(FieldType.ACQUIRER_TYPE
								.getName()));
				
				// Fetch user for retryTransaction ,SendEmailer and SmsSenser

				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID
						.getName()));
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(
						ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields,
							sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION
								.getValue());
						return "paymentPage";
					}
				}
				// Retry Transaction Block End

				// Sending Email for Transaction Status to merchant

				String countryCode = (String) sessionMap
						.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);

				fields.put(FieldType.RETURN_URL.getName(),
						(String) sessionMap.get(FieldType.RETURN_URL.getName()));
				fields.remove(FieldType.HASH.getName());
				processor.postProcess(fields);
			} catch (SystemException systemException) {
				logger.error("Exception", systemException);
				fields.put(FieldType.STATUS.getName(),
						StatusType.ERROR.getName());
				fields.put(FieldType.RESPONSE_MESSAGE.getName(),
						ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
				fields.put(FieldType.RESPONSE_CODE.getName(),
						ErrorType.INTERNAL_SYSTEM_ERROR.getResponseCode());
				ResponseCreator responseCreator = new ResponseCreator();
				responseCreator.create(fields);
				responseCreator.ResponsePost(fields);
				return NONE;
			}
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN
							.getName()));

			if (sessionMap != null) {
				sessionMap.put(Constants.TRANSACTION_COMPLETE_FLAG.getValue(),
						Constants.Y_FLAG.getValue());
				sessionMap.invalidate();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return NONE;
	}
}
