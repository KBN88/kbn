package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.mobikwik.ResponseFactory;
import com.kbn.mobikwik.ResponseProcessor;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;

/**
 * @author Sunil
 *
 */
public class MobikwikResponseAction extends AbstractSecureAction implements
		ServletRequestAware {

	private static final long serialVersionUID = 7457128715062274247L;

	private static Logger logger = Logger
			.getLogger(DirecpayResponseAction.class.getName());

	private HttpServletRequest httpRequest;

	@SuppressWarnings("unchecked")
	public String execute() {
		try {

			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();
			ResponseProcessor processor = new ResponseProcessor();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(),
							((String[]) entry.getValue())[0].trim());
				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}

			Fields fields = (Fields) sessionMap
					.get(Constants.FIELDS.getValue());
			SecurityProcessor securityProcessor = new SecurityProcessor();
			securityProcessor.authenticate(fields);
			securityProcessor.addAcquirerFields(fields);

			try {
				processor.compareHash(fields, responseMap);

				responseMap.put(FieldType.CURRENCY_CODE.getName(),
						fields.get(FieldType.CURRENCY_CODE.getName()));
				responseMap = ResponseFactory.parseAuthResponse(responseMap);
				fields.putAll(responseMap);
				processor.preProcess(fields);
				processor.process(fields);
				// Fetch user for retryTransaction ,SendEmailer and SmsSenser
				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID
						.getName()));
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(
						ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields,
							sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION
								.getValue());
						return "paymentPage";
					}
				}
				// Retry Transaction Block End
				
				// Sending Email for Transaction Status to merchant
				String countryCode = (String) sessionMap
						.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);
				
				fields.logAllFields("Mobikwik Recieved Map :");
				fields.put(FieldType.RETURN_URL.getName(),
						(String) sessionMap.get(FieldType.RETURN_URL.getName()));

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				logger.error("Exception", systemException);
				ResponseCreator responseCreator = new ResponseCreator();
				responseCreator.ResponsePost(fields);
				sessionMap.clear();
				return NONE;
			}
			if (sessionMap != null) {
				sessionMap.clear();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return NONE;
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}
}
