package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.simple.JSONObject;

import com.kbn.axisupi.AxisBankTransactionService;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class VerifyUpiResponseReceived extends AbstractSecureAction
		implements ServletRequestAware, ServletResponseAware {
	private static Logger logger = Logger.getLogger(VerifyUpiResponseReceived.class.getName());
	/**
	 * @ neeraj
	 */
	private static final long serialVersionUID = -3812859523254750431L;
	private HttpServletRequest httpRequest;
	private AxisBankTransactionService axisBankTransactionService = new AxisBankTransactionService();
	HttpServletResponse response;

	@SuppressWarnings("unchecked")
	public String verifyStatusMethod() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> requestMap = new HashMap<String, String>();

			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					requestMap.put(entry.getKey(), ((String[]) entry.getValue())[0]);

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			logger.info("Response Json Data:" + requestMap);
			String Txn_Id = requestMap.get("pgRefNum");
			String RETURN_URL = requestMap.get("RETURN_URL");
			logger.info("Txn Id :" + Txn_Id);
			Map<String, String> field = axisBankTransactionService.fetchOrderID(Txn_Id);
			logger.info("Session fields value1" + field.get(FieldType.PAY_ID.getName()));
			Fields fields = new Fields(field);
			Map<String, Object> resMap = new HashMap<String, Object>();
			resMap.put("RESPONSE_CODE", fields.get(FieldType.RESPONSE_CODE.getName()));
			resMap.put("STATUS", fields.get(FieldType.STATUS.getName()));
			resMap.put("RESPONSE_MESSAGE", fields.get(FieldType.RESPONSE_MESSAGE.getName()));
			resMap.put("RETURN_URL", fields.get(FieldType.RETURN_URL.getName()));
			resMap.put("TXNTYPE", fields.get(FieldType.TXNTYPE.getName()));
			resMap.put("TXN_ID", fields.get(FieldType.TXN_ID.getName()));
			resMap.put("INTERNAL_ACQUIRER_TYPE", fields.get(FieldType.ACQUIRER_TYPE.getName()));
			resMap.put("RESPONSE_DATE_TIME", fields.get(FieldType.RESPONSE_DATE_TIME.getName()));
			resMap.put("CUST_EMAIL", fields.get(FieldType.CUST_EMAIL.getName()));
			resMap.put("CURRENCY_CODE", fields.get(FieldType.CURRENCY_CODE.getName()));
			resMap.put("AMOUNT", fields.get(FieldType.AMOUNT.getName()));
			resMap.put("PAYMENT_TYPE", fields.get(FieldType.PAYMENT_TYPE.getName()));
			resMap.put("PAY_ID", fields.get(FieldType.PAY_ID.getName()));
			resMap.put("ORDER_ID", fields.get(FieldType.ORDER_ID.getName()));
			resMap.put("CUST_NAME", fields.get(FieldType.CUST_NAME.getName()));
			resMap.put("RRN", fields.get(FieldType.RRN.getName()));
			resMap.put("RETURN_URL", RETURN_URL);

			Map<String, Object> resMap1 = new HashMap<String, Object>();

			resMap1.put("responseCode", fields.get(FieldType.RESPONSE_CODE.getName()));
			resMap1.put("transactionStatus", fields.get(FieldType.STATUS.getName()));
			resMap1.put("txnType", fields.get(FieldType.TXNTYPE.getName()));
			resMap1.put("pgRefNum", fields.get(FieldType.TXN_ID.getName()));
			resMap1.put("RETURN_URL", RETURN_URL);
			resMap1.put("RESPONSE_DATE_TIME", fields.get(FieldType.RESPONSE_DATE_TIME.getName()));
			resMap1.put("CUST_NAME", fields.get(FieldType.CUST_NAME.getName()));
			resMap1.put("CUST_EMAIL", fields.get(FieldType.CUST_EMAIL.getName()));
			resMap1.put("responseMessage", fields.get(FieldType.RESPONSE_MESSAGE.getName()));

			resMap1.put("responseFields", resMap);

			JSONObject jObject = new JSONObject(resMap1);

			response.setContentType("application/json");
			response.getWriter().flush();
			response.getWriter().println(jObject);
			response.getWriter().close();

		} catch (Exception exception) {
			logger.error("Exception", exception);
			exception.printStackTrace();
		}
		return SUCCESS;
	}


	// upi://pay?pa=8860705801@paytm&pn=mmadpay&mc=0000&tid=cxnkjcnkjdfdvjndkjfvn&tr=4894398cndhcd23&tn=Pay%to%rohit%stores&am=1010&cu=INR&refUrl=https://rohit.com/orderid=9298yw
	// 89e8973e87389e78923ue892

	@Override
	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

}
