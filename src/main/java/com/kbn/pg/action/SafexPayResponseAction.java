package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;
import com.kbn.safexpay.PayGateCryptoUtils;
import com.kbn.safexpay.SafexPayResponseProcessor;
import com.kbn.safexpay.SafexPayTransformer;
import com.opensymphony.xwork2.Action;

public class SafexPayResponseAction extends AbstractSecureAction implements ServletRequestAware{

	/**
	 * @author Neeraj
	 */
	private static final long serialVersionUID = -2639344102727615914L;
	private static Logger logger = Logger.getLogger(SafexPayResponseAction.class.getName());
	private HttpServletRequest httpRequest;


	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(), ((String[]) entry.getValue())[0].trim());
				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			Fields fields = new Fields();
			Object fieldsObj = sessionMap.get("FIELDS");
			if (null != fieldsObj) {
				fields.put((Fields) fieldsObj);
			}
			logger.info("SafexPay SessionMap Fields :" + fields);
			logger.info("Session fields PAY_ID :" + fields.get(FieldType.PAY_ID.getName()));
			logger.info("Session fields TXN_KEY :" + fields.get(FieldType.TXN_KEY.getName()));
			logger.info("Session fields INTERNAL_ACQUIRER_TYPE :" + fields.get(FieldType.INTERNAL_ACQUIRER_TYPE.getName()));
			String merchantKey = fields.get(FieldType.TXN_KEY.getName());
			String payId = fields.get(FieldType.PAY_ID.getName());
			String acquirer = fields.get(FieldType.INTERNAL_ACQUIRER_TYPE.getName());
			/**
			 *  implements get TXN_KEY neeraj 17-05-2019 
			 */
			if(merchantKey == null) {
				UserDao userDao = new UserDao();
				User user = userDao.findBySafexpayUser(payId);
				Account account = user.getAccountUsingAcquirerCode(acquirer);
				String currencyCode = fields.get(FieldType.CURRENCY_CODE.getName());
				AccountCurrency accountCurrency = account.getAccountCurrency(currencyCode);	
				//Get key
				merchantKey = accountCurrency.getTxnKey();
			   logger.info("Null Txn_Key :" + merchantKey);
			}
			logger.info("SafexPay merchantKey :" + merchantKey);
			logger.info("SafexPay responseMap :" + responseMap);
			String txn_response = responseMap.get("txn_response");
			String pg_details = responseMap.get("pg_details");
			String fraud_details = responseMap.get("fraud_details");
			String other_details = responseMap.get("other_details");
			String decryptedTxn_response = PayGateCryptoUtils.decrypt(txn_response, merchantKey);
			String decryptedPg_details = PayGateCryptoUtils.decrypt(pg_details, merchantKey);
			String decryptedFraud_details = PayGateCryptoUtils.decrypt(fraud_details,merchantKey);
			String decryptedOther_details = PayGateCryptoUtils.decrypt(other_details,merchantKey);
			
			fields.logAllFields("session fields for SafexPay");
			if (null != fields) {
				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);
			}
			logger.info("Message recieved from decryptedTxn_response :" + decryptedTxn_response);
			logger.info("Message recieved from decryptedPg_details :" + decryptedPg_details);
			logger.info("Message recieved from decryptedFraud_details :" + decryptedFraud_details);
			logger.info("Message recieved from decryptedOther_details :" + decryptedOther_details);
			SafexPayTransformer safexPayTransformer = new SafexPayTransformer(decryptedTxn_response);
			safexPayTransformer.updateSafexPayResponse(fields);
			try {
				Processor processor = new SafexPayResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);
				// Fetch user for Re-try Transaction ,Emailer and Sms Processor
				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());
						return "paymentPage";
					}
				}
				// Retry Transaction Block End

				// Sending Email for Transaction Status to merchant
				fields.put(FieldType.PAY_ID.getName(), fields.get(FieldType.PAY_ID.getName()));
				fields.put(FieldType.ACQUIRER_TYPE.getName(),
						(String) sessionMap.get(FieldType.ACQUIRER_TYPE.getName()));

				String countryCode = (String) sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);

				fields.put(FieldType.RETURN_URL.getName(), (String) sessionMap.get(FieldType.RETURN_URL.getName()));

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				// TODO....update fields as error??
				logger.error("Exception", systemException);
			}
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
			if (sessionMap != null) {
				sessionMap.clear();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return Action.NONE;
	}


	@Override
	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;

	}

}