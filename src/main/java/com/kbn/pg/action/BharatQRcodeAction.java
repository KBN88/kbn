package com.kbn.pg.action;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Amount;
public class BharatQRcodeAction {
	 UserDao userDao = new UserDao();
		public String genrateBhartQRCode(Fields fields) {
			User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
			StringBuilder request = new StringBuilder();
			request.append("upi://pay?");
			request.append("pa=");
			request.append(user.getMerchantVpa());
			request.append("&");
			request.append("pn=");
			request.append(user.getBusinessName());
			request.append("&");
			request.append("mc=");
			request.append("1520");
			request.append("&");
			request.append("tid=");
			request.append(fields.get(FieldType.TXN_ID.getName()));
			request.append("&");
			request.append("tr=");
			request.append(fields.get(FieldType.ORDER_ID.getName()));
			request.append("&");
			request.append("tn=");
			request.append(fields.get(FieldType.PRODUCT_DESC.getName()));
			request.append("&");
			request.append("am=");
		    String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),fields.get(FieldType.CURRENCY_CODE.getName()));
			request.append(amount.substring(0,amount.indexOf(".")));
			request.append("&");
			request.append("cu=");
			request.append(fields.get(FieldType.CURRENCY_CODE.getName()));
			request.append("&");
			request.append("refUrl=");
			request.append(fields.get(FieldType.RETURN_URL.getName()));
			return request.toString();
		}

}
