package com.kbn.pg.action;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.owasp.esapi.ESAPI;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.BinRangeProvider;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CustTransactionAuthentication;
import com.kbn.commons.util.DynamicCurrencyConvertor;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.LocateCountryName;
import com.kbn.commons.util.ModeType;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.SystemConstants;
import com.kbn.commons.util.TransactionType;
import com.kbn.crm.actionBeans.SessionCleaner;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.oneclick.TokenManager;
import com.kbn.pg.action.beans.PaymentTypeProvider;
import com.kbn.pg.action.service.ActionService;
import com.kbn.pg.action.service.PgActionServiceFactory;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.CalculateSurchargeAmount;
import com.kbn.pg.core.Currency;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.RequestRouter;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.core.TransactionResponser;
import com.kbn.pg.core.UpdateProcessor;
import com.kbn.pg.core.pageintegrator.AcquireIntegratorFactory;
import com.kbn.pg.security.GeneralValidator;
import com.opensymphony.xwork2.Action;
/**
 * @author Neeraj
 *
 */
public class RequestAction extends AbstractSecureAction implements ServletRequestAware{

	private static Logger logger = Logger.getLogger(RequestAction.class.getName());
	private static final long serialVersionUID = 6526733830656374186L;
	private static final String pendingTxnStatus = "Sent to Bank-Enrolled";

	private HttpServletRequest request;
	private String payId;
	private String amount;
	private String merchantName;
	private String businessName;
	private String orderId;
	private String cardNumber;
	private String expiryYear;
	private String expiryMonth;
	private String cvvNumber;
	private String storeCardFlag;
	private String origTxnType;
	private String redirectUrl;
	private String paymentType;
	private String mopType;
	private String bankName;
	private String walletName;
	private String responseCode;
	private String acquirerFlag;
	private String responseUrl;
	private String cardName;
	private String currencyCode;
	private String hash;
	private String returnUrl;
	private boolean cardsaveflag;
	private String tokenId;
	private String recurringFlag;
	private String defaultLanguage;

	private Map<String, Object> supportedPaymentTypeMap = new HashMap<String, Object>();
	private Map<String, Object> cardPaymentTypeMap = new HashMap<String, Object>();
	private Map<String, Object> tokenMap = new HashMap<String, Object>();
	private TokenManager tokenManager = new TokenManager();
	
	@SuppressWarnings("unchecked")
	public String execute() {
		try {

			//clean session
			sessionMap.invalidate();

			//  create fields for transaction
			DynamicCurrencyConvertor dcc = new DynamicCurrencyConvertor();
			LocateCountryName httpURLConnectionCountry=new LocateCountryName();
			ActionService service = PgActionServiceFactory.getActionService();
			Fields	fields = service.prepareFields(request.getParameterMap());
			String fieldsAsString = fields.getFieldsAsString();
			sessionMap.put(Constants.FIELDS.getValue(),fields);
			fields.put(FieldType.INTERNAL_REQUEST_FIELDS.getName(), fieldsAsString);

			GeneralValidator generalValidator = new GeneralValidator();
			if(StringUtils.isBlank(fields.get(FieldType.HASH.getName()))){
				throw new SystemException(ErrorType.VALIDATION_FAILED, "Invalid "
						+ FieldType.HASH.getName());	
			}			
			generalValidator.validateHash(fields);																									
			generalValidator.validateReturnUrl(fields);

			User user = new UserDao().getUserClass(fields.get(FieldType.PAY_ID.getName()));

			//replace the user supplied transaction type with the mode type assigned to the merchant
			setOrigTxnType(ModeType.getDefaultPurchaseTransaction(user.getModeType()).getName());
			fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), getOrigTxnType());

			sessionMap.put((FieldType.INTERNAL_ORIG_TXN_TYPE.getName()),getOrigTxnType());
			sessionMap.put((FieldType.INTERNAL_SHOPIFY_YN.getName()),fields.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
			sessionMap.put((FieldType.CANCEL_URL.getName()),fields.get(FieldType.CANCEL_URL.getName()));
			sessionMap.put((FieldType.RETURN_URL.getName()),fields.get(FieldType.RETURN_URL.getName()));
			sessionMap.put((FieldType.RETRY_FLAG.getName()), ((user.isRetryTransactionCustomeFlag()) ? "Y":"N"));
			sessionMap.put((FieldType.NUMBER_OF_RETRY.getName()), user.getAttemptTrasacation());
			sessionMap.put((FieldType.SURCHARGE_FLAG.getName()), ((user.isSurchargeFlag()) ? "Y":"N"));	
			sessionMap.put((FieldType.MERCHANT_PAYMENT_TYPE.getName()), fields.get(FieldType.MERCHANT_PAYMENT_TYPE.getName()));
			
			fields.put((FieldType.INTERNAL_CUST_IP.getName()), request.getRemoteAddr());

			//Fetch country name using IP Address
			String countryCode = httpURLConnectionCountry.findCountryCode(request.getRemoteAddr());
			if(StringUtils.isBlank(countryCode)){
				countryCode= "NA";
			}

			fields.put((FieldType.INTERNAL_CUST_COUNTRY_NAME.getName()), countryCode);
			sessionMap.put(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName(), countryCode);

			if(countryCode.equals(CrmFieldConstants.INDIA_REGION_CODE.getValue())) {
				fields.put((FieldType.INTERNAL_TXN_AUTHENTICATION.getName()), CustTransactionAuthentication.SUCCESS.getAuthenticationName());
			}
			else {
				fields.put((FieldType.INTERNAL_TXN_AUTHENTICATION.getName()), CustTransactionAuthentication.PENDING.getAuthenticationName());
			}
			// Dynamic Currency Exchange 15-07-2019 @ Neeraj
		   String dynamincCurrecnyExchange = dcc.findDynamicCurrencyExchange(Currency.getAlphabaticCode(fields.get(FieldType.CURRENCY_CODE.getName())), fields);
		   logger.info(" dynamincCurrecnyConvert :" + dcc);
			if (StringUtils.isBlank(dynamincCurrecnyExchange)) {
				dynamincCurrecnyExchange = "NA";
				}
			fields.put((FieldType.INTERNAL_CURRENCY_CHANGE_RATE.getName()), dynamincCurrecnyExchange);
			sessionMap.put(FieldType.INTERNAL_CURRENCY_CHANGE_RATE.getName(), dynamincCurrecnyExchange);
			
			setBusinessName(user.getBusinessName());
			requestCreator(fields);
			responseCode = fields.get(FieldType.RESPONSE_CODE.getName());

			if (StringUtils.isEmpty(getResponseCode())) {
				return Action.ERROR;
			} else if (getResponseCode().equals(ErrorType.SUCCESS.getCode())) {
				//These fields are being used at PaymentPage
				sessionMap.put((FieldType.PAY_ID.getName()), fields.get(FieldType.PAY_ID.getName()));
				sessionMap.put(FieldType.ORDER_ID.getName(), fields.get(FieldType.ORDER_ID.getName()));
				sessionMap.put(FieldType.CUST_NAME.getName(), fields.get(FieldType.CUST_NAME.getName()));
				sessionMap.put(FieldType.CURRENCY_CODE.getName(), fields.get(FieldType.CURRENCY_CODE.getName()));
				sessionMap.put(FieldType.AMOUNT.getName(), fields.get(FieldType.AMOUNT.getName()));
				sessionMap.put(FieldType.CUST_PHONE.getName(), fields.get(FieldType.CUST_PHONE.getName()));
				sessionMap.put((FieldType.MERCHANT_PAYMENT_TYPE.getName()), fields.get(FieldType.MERCHANT_PAYMENT_TYPE.getName()));

				
				//Check for recurring payment flag
				recurringFlag = fields.get(FieldType.IS_RECURRING.getName());
				if ( StringUtils.isBlank(recurringFlag)  || !recurringFlag.equals("Y")){
					sessionMap.put(FieldType.IS_RECURRING.getName(), false);
				}else{
					sessionMap.put(FieldType.IS_RECURRING.getName(), true);
				}
				//Merchant Payment Option at Payment page Dynamically ...17-02-2020
				//get String of supported payment types
                String merchantPaymentType = fields.get(FieldType.MERCHANT_PAYMENT_TYPE.getName());
				PaymentTypeProvider paymentTypeProvider = new PaymentTypeProvider(user.getPayId());
				
				if (merchantPaymentType == null || merchantPaymentType.equals("NA")) {
					setSupportedPaymentTypeMap(paymentTypeProvider.getSupportedPaymentTypeMap());
					setCardPaymentTypeMap(paymentTypeProvider.getSupportedCardTypeMap());
				}
				else{
				String[] selectedTypes = merchantPaymentType.split(",");
				Map<String, Object> newCardType = new HashMap<String, Object>();
				Map<String, Object> cardType = paymentTypeProvider.getSupportedPaymentTypeMap();
					for(int i=0;i<selectedTypes.length;i++){
						String newMerchantPaymentType = selectedTypes[i];
						if (newMerchantPaymentType.equals("CC")) {
							if(cardType.get("CC")!=null)
								newCardType.put("CC", cardType.get("CC"));
						} else if (newMerchantPaymentType.equals("DC")) {
							if(cardType.get("DC")!=null)
								newCardType.put("DC", cardType.get("DC"));
						} else if (newMerchantPaymentType.equals("NB")) {
							if(cardType.get("NB")!=null)
								newCardType.put("NB", cardType.get("NB"));
						} else if (newMerchantPaymentType.equals("WL")) {
							if(cardType.get("WL")!=null)
								newCardType.put("WL", cardType.get("WL"));
						}else if (newMerchantPaymentType.equals("UP")) {
							if(cardType.get("UP")!=null)
								newCardType.put("UP", cardType.get("UP"));
						}
					}
					setSupportedPaymentTypeMap(newCardType);
					setCardPaymentTypeMap(paymentTypeProvider.getSupportedCardTypeMap());
				}				
				/*//get String of supported payment types
				PaymentTypeProvider paymentTypeProvider = new PaymentTypeProvider(user.getPayId());
				setSupportedPaymentTypeMap(paymentTypeProvider.getSupportedPaymentTypeMap());
				setCardPaymentTypeMap(paymentTypeProvider.getSupportedCardTypeMap());*/
				
				sessionMap.put(Constants.PAYMENT_TYPE_MOP.getValue(), getSupportedPaymentTypeMap());
				sessionMap.put(Constants.CARD_PAYMENT_TYPE_MOP.getValue(), getCardPaymentTypeMap());
				sessionMap.put(Constants.EXPRESS_PAY_FLAG.getValue(), user.isExpressPayFlag());
				sessionMap.put(Constants.IFRAME_PAYMENT_PAGE.getValue(), user.isIframePaymentFlag());
				sessionMap.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.TXN_ID.getName()));
				//to set default language of payment page 
				setDefaultLanguage(user.getDefaultLanguage());
				tokenMap = tokenManager.getAll(fields);
				if (tokenMap.isEmpty()){
					sessionMap.put(Constants.TOKEN.getValue(), "NA");
				}
				else{
					sessionPut(Constants.TOKEN.getValue(), tokenMap);
				}
				
				//fetch design for payment page	
				FetchDynamicPaymentPage fetchDynamicPage = new FetchDynamicPaymentPage();
				fetchDynamicPage.FetchDynamicPayID(fields.get(FieldType.PAY_ID
						.getName()),sessionMap);
				
				String surchargeFlag = sessionMap.get("SURCHARGE_FLAG").toString();
				if (surchargeFlag.equals("Y")){
					String convertedamount = sessionMap.get("AMOUNT").toString();
					String currency = sessionMap.get("CURRENCY_CODE").toString();
					
					String amount = ESAPI.encoder().encodeForHTML(
							Amount.toDecimal(convertedamount, currency)); 
					String payId = sessionMap.get("PAY_ID").toString();
					
					CalculateSurchargeAmount calculateSurchargeAmount = new CalculateSurchargeAmount();
					BigDecimal[] surCCAmount = calculateSurchargeAmount.fetchCCSurchargeDetails(amount, payId);
					BigDecimal[] surDCAmount = calculateSurchargeAmount.fetchDCSurchargeDetails(amount, payId);
					BigDecimal[] surNBAmount = calculateSurchargeAmount.fetchNBSurchargeDetails(amount, payId);
					//BigDecimal[] surWLAmount = calculateSurchargeAmount.fetchWLSurchargeDetails(amount, payId);
					//BigDecimal[] surUPAmount = calculateSurchargeAmount.fetchUPISurchargeDetails(amount, payId);

					BigDecimal ccTransSurcharge = surCCAmount[0];
					BigDecimal surchargeCCAmount = surCCAmount[1];

					BigDecimal dcTransSurcharge = surDCAmount[0];
					BigDecimal surchargeDCAmount = surDCAmount[1];

					BigDecimal nbTransSurcharge = surNBAmount[0];
					BigDecimal surchargeNBAmount = surNBAmount[1];

					/*BigDecimal wlTransSurcharge = surWLAmount[0];
					BigDecimal surchargeWLAmount = surWLAmount[1];

					BigDecimal upiTransSurcharge = surUPAmount[0];
					BigDecimal surchargeUPAmount = surUPAmount[1];*/

					sessionMap.put(FieldType.SURCHARGE_CC_AMOUNT.getName(), surchargeCCAmount);
					sessionMap.put(FieldType.SURCHARGE_DC_AMOUNT.getName(), surchargeDCAmount);
					sessionMap.put(FieldType.SURCHARGE_NB_AMOUNT.getName(), surchargeNBAmount);
					/*sessionMap.put(FieldType.SURCHARGE_WL_AMOUNT.getName(), surchargeWLAmount);
					sessionMap.put(FieldType.SURCHARGE_UPI_AMOUNT.getName(), surchargeUPAmount);*/

					sessionMap.put(FieldType.CC_SURCHARGE.getName(), ccTransSurcharge);
					sessionMap.put(FieldType.DC_SURCHARGE.getName(), dcTransSurcharge);
					sessionMap.put(FieldType.NB_SURCHARGE.getName(), nbTransSurcharge);
					/*sessionMap.put(FieldType.WL_SURCHARGE.getName(), wlTransSurcharge);
					sessionMap.put(FieldType.UPI_SURCHARGE.getName(), upiTransSurcharge);*/
					
					return "surchargePaymentPage";
				} else {
					return "paymentPage";
				}
			} else {
				fields.put(FieldType.TXNTYPE.getName(), getOrigTxnType());
				ResponseCreator responseCreator = new ResponseCreator();
				responseCreator.ResponsePost(fields);
				return Action.NONE;
			}

		}catch (SystemException systemException) {
			logger.error("Exception", systemException);
			Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());

			if(null==fields){				
				return "invalidRequest";
			}

			String salt = (new PropertiesManager()).getSalt(fields
					.get(FieldType.PAY_ID.getName()));

			if(null==salt){
				return "invalidRequest";
			}

			if (StringUtils.isBlank(getOrigTxnType())) {
				User user = new UserDao().getUserClass(fields
						.get(FieldType.PAY_ID.getName()));
				setOrigTxnType(ModeType.getDefaultPurchaseTransaction(
						user.getModeType()).getName());
			}

			fields.put(FieldType.TXNTYPE.getName(), getOrigTxnType());
			fields.put(FieldType.RESPONSE_CODE.getName(), systemException
					.getErrorType().getCode());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), systemException
					.getErrorType().getResponseMessage());	
			
			saveInvalidTransaction(fields);
			//If an invalid request of valid merchant save it				
			if(fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.INVALID_RETURN_URL.getCode())){
				return "invalidRequest";
			}
			fields.put(FieldType.TXNTYPE.getName(), getOrigTxnType());
			new ResponseCreator().create(fields);			
			fields.removeInternalFields();
			ResponseCreator responseCreator = new ResponseCreator();

			responseCreator.ResponsePost(fields);
			sessionMap.invalidate();
			return Action.NONE;	
		}
		catch (Exception exception) {
			SessionCleaner.cleanSession(sessionMap);
			logger.error("Exception", exception);
			return Action.ERROR;
		}
	}

	public Fields requestCreator(Fields fields) {

		fields.put(FieldType.TXNTYPE.getName(), TransactionType.NEWORDER.getName());
		fields.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(), "N");
		fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), getOrigTxnType());
		fields.logAllFields("All request fields :");

		RequestRouter router = new RequestRouter(fields);
		fields = new Fields(router.route());	
		return fields;
	}

	private void saveInvalidTransaction(Fields fields){
		//TODO... validate and sanitize fields
		setOrigTxnType(fields.get(FieldType.TXNTYPE.getName()));
		fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), getOrigTxnType());

		sessionMap.put((FieldType.INTERNAL_ORIG_TXN_TYPE.getName()),getOrigTxnType());
		UpdateProcessor updateProcessor = new UpdateProcessor();
		try {
			updateProcessor.preProcess(fields);
			updateProcessor.prepareInvalidTransactionForStorage(fields);
		}catch (SystemException systemException) {
			logger.error("Unable to save invalid transaction", systemException);
		}catch (Exception exception){
			logger.error("Unhandaled error",exception);
			//Non reachable code for safety
		}
	}

	public String cancelByUser() {
		try{
			Fields fields = (Fields) sessionMap
					.get(Constants.FIELDS.getValue());			
			ResponseCreator responseCreator = new ResponseCreator();
			if (null == fields) {
				fields = new Fields();
				fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.TIMEOUT.getResponseMessage());
				fields.put(FieldType.RESPONSE_CODE.getName(),ErrorType.TIMEOUT.getCode());
				fields.put(FieldType.STATUS.getName(),StatusType.TIMEOUT.getName());
				fields.put(FieldType.RETURN_URL.getName(),ConfigurationConstants.DEFAULT_RETURN_URL.getValue());
				sessionMap.clear();
				responseCreator.ResponsePost(fields);
			} else {

				//Remove hash generated earlier
				fields.remove(FieldType.HASH.getName());

				fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.CANCELLED.getResponseMessage());
				fields.put(FieldType.RESPONSE_CODE.getName(),ErrorType.CANCELLED.getCode());
				fields.put(FieldType.STATUS.getName(),StatusType.CANCELLED.getName());
				fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
				String shopifyFlag = (String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName());

				fields.put((FieldType.CANCEL_URL.getName()),(String) sessionMap.get(FieldType.CANCEL_URL.getName()));
				Object txnTypeObject = sessionMap.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName());
				if(null!=shopifyFlag && shopifyFlag.equals("Y")){
					fields.put((FieldType.RETURN_URL.getName()),(String) sessionMap.get(FieldType.CANCEL_URL.getName()));
				}

				if (null != txnTypeObject) {
					String txnType = (String) txnTypeObject;
					fields.put(FieldType.TXNTYPE.getName(), txnType);
				}
				fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(),
						fields.get(FieldType.TXN_ID.getName()));
				try {
					fields.updateNewOrderDetails();
				} catch (SystemException systemException) {
					logger.error("Unable to update cancelled transaction",
							systemException);
				}
				responseCreator.create(fields);
				responseCreator.ResponsePost(fields);
				sessionMap.invalidate();
			}
			return NONE;
		} catch (Exception exception){
			logger.error("Error handling cancellation of transaction", exception);
			return ERROR;
		}
	}

	public String acquirerHandler() {		
		try {

			Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());
			if(null!=fields){
				logger.info(fields.getFieldsAsString());
			}else{
				logger.info("session fields lost");
				return ERROR;
			}
			fields.put(FieldType.PAYMENT_TYPE.getName(), getPaymentType());
			fields.put(FieldType.MOP_TYPE.getName(), getMopType());
			
			String surchargeFlag = sessionMap.get("SURCHARGE_FLAG").toString();
			if (surchargeFlag.equals("Y")) {
				fields.put((FieldType.SURCHARGE_FLAG.getName()), surchargeFlag);
				String convertedamount = fields.get(FieldType.AMOUNT.getName());
				String currency = sessionMap.get("CURRENCY_CODE").toString();
				
				String amount = ESAPI.encoder().encodeForHTML(
						Amount.toDecimal(convertedamount, currency)); 
				String payId = sessionMap.get("PAY_ID").toString();
				String paymentType = fields.get(FieldType.PAYMENT_TYPE.getName());
				CalculateSurchargeAmount calculateSurchargeAmount = new CalculateSurchargeAmount();
				DecimalFormat df = new DecimalFormat("0.00");
				switch (paymentType) {
				case "CC":
					BigDecimal[] surCCAmount = calculateSurchargeAmount.fetchCCSurchargeDetails(amount,payId);
					String ccTransSurcharge = df.format(surCCAmount[0]);
					String surchargeCCAmount = df.format(surCCAmount[1]);
					
					ccTransSurcharge = Amount.formatAmount(ccTransSurcharge, currency);
					surchargeCCAmount = Amount.formatAmount(surchargeCCAmount, currency);
					
					fields.put(FieldType.AMOUNT.getName(), surchargeCCAmount);
					fields.put(FieldType.SURCHARGE_AMOUNT.getName(), ccTransSurcharge);
					break;
				case "DC":
					BigDecimal[] surDCAmount = calculateSurchargeAmount.fetchDCSurchargeDetails(amount,payId);
					String dcTransSurcharge = df.format(surDCAmount[0]);
					String surchargeDCAmount = df.format(surDCAmount[1]);
					
					fields.put(FieldType.AMOUNT.getName(), surchargeDCAmount);
					fields.put(FieldType.SURCHARGE_AMOUNT.getName(), dcTransSurcharge);
					break;
				case "NB":
					BigDecimal[] surNBAmount = calculateSurchargeAmount.fetchNBSurchargeDetails(amount,payId);
					String nbTransSurcharge = df.format(surNBAmount[0]);
					String surchargeNBAmount = df.format(surNBAmount[1]);
					
					fields.put(FieldType.AMOUNT.getName(), surchargeNBAmount);
					fields.put(FieldType.SURCHARGE_AMOUNT.getName(), nbTransSurcharge);
					break;
				default:
					throw new SystemException(ErrorType.PAYMENT_OPTION_NOT_SUPPORTED,"Unsupported payment type");
				}
			}

			 if (PaymentType.NET_BANKING.getCode().equals(getPaymentType())) {
				fields.put(FieldType.MOP_TYPE.getName(), getBankName());
				sessionMap.put((FieldType.INTERNAL_ORIG_TXN_TYPE.getName()),TransactionType.SALE.getName());
				fields.put(FieldType.TXNTYPE.getName(), TransactionType.SALE.getName());
			} else if(PaymentType.WALLET.getCode().equals(getPaymentType())){
				fields.put(FieldType.MOP_TYPE.getName(), getWalletName());
				sessionMap.put((FieldType.INTERNAL_ORIG_TXN_TYPE.getName()),TransactionType.SALE.getName());
				//fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), TransactionType.SALE.getName());
				fields.put(FieldType.TXNTYPE.getName(), TransactionType.SALE.getName());
			} else if (PaymentType.EXPRESS_PAY.getCode().equals(getPaymentType())){
				if(null != getTokenId()){
					fields.put(FieldType.TOKEN_ID.getName(), getTokenId());
					fields.put(FieldType.KEY_ID.getName(), SystemConstants.DEFAULT_KEY_ID);
					Map<String, String> tokenMap = new HashMap<String, String>();
					tokenMap = tokenManager.getToken(fields);
					fields.put(FieldType.TXNTYPE.getName(),TransactionType.ENROLL.getName());
					fields.put(FieldType.CARD_NUMBER.getName(), tokenMap.get(FieldType.CARD_NUMBER.getName()).replace(" ", "").replace(",", ""));
					fields.put(FieldType.CARD_EXP_DT.getName(), tokenMap.get(FieldType.CARD_EXP_DT.getName()));
					fields.put(FieldType.MOP_TYPE.getName(), tokenMap.get(FieldType.MOP_TYPE.getName()));
					fields.put(FieldType.PAYMENT_TYPE.getName(), tokenMap.get(FieldType.PAYMENT_TYPE.getName()));
					fields.put(FieldType.CUST_NAME.getName(), tokenMap.get(FieldType.CUST_NAME.getName()));
					fields.put(FieldType.CVV.getName(), getCvvNumber().replace(" ", "").replace(",", ""));
					fields.put(FieldType.INTERNAL_CARD_ISSUER_BANK.getName(), tokenMap.get(FieldType.INTERNAL_CARD_ISSUER_BANK.getName()));
					fields.put(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName(), tokenMap.get(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()));
				}
			}else{
				fields.put(FieldType.TXNTYPE.getName(),TransactionType.ENROLL.getName());
				if(!getMopType().endsWith(MopType.EZEECLICK.getCode())){
					fields.put(FieldType.CARD_NUMBER.getName(), getCardNumber().replace(" ", "").replace(",", ""));
					fields.put(FieldType.CARD_EXP_DT.getName(), getExpiryMonth()+ getExpiryYear());
					fields.put(FieldType.CVV.getName(), getCvvNumber().replace(" ", "").replace(",", ""));
					fields.put(FieldType.CUST_NAME.getName(), getCardName());
					//BIN Range API Call
					BinRangeProvider binRangeProvider= new BinRangeProvider();
					binRangeProvider.findBinRange(fields);
				}
			}
			// Save Card detail for Express Payment
			if(isCardsaveflag()){
				saveCard(fields);
			}

			// Merchant Payment Option at Payment page Dynamically ...05-03-2019
						String merchantPaymentType = fields.get(FieldType.MERCHANT_PAYMENT_TYPE.getName());
						if (merchantPaymentType != null && (merchantPaymentType.equals("CC") || merchantPaymentType.equals("DC"))) {
							UserDao userDao = new UserDao();
							User user = userDao.getUserClass(sessionMap.get(FieldType.PAY_ID.getName()).toString());
							if (!(merchantPaymentType.equals(fields.get(FieldType.PAYMENT_TYPE.getName())))) {
								RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
								retryTransactionProcessor.retryCardTransaction(fields, sessionMap, user);
								if (merchantPaymentType.equals("DC")) {
									addActionMessage(CrmFieldConstants.PAYMENT_TYPE_DC.getValue());
								} else if (merchantPaymentType.equals("CC")) {
									addActionMessage(CrmFieldConstants.PAYMENT_TYPE_CC.getValue());
								}
								return "paymentPage";
							}
						}
			
			String txnId = (String) sessionMap.get(FieldType.INTERNAL_ORIG_TXN_ID.getName());
			fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), txnId);

			sessionMap.put((FieldType.CARD_NUMBER.getName()),fields.get(FieldType.CARD_NUMBER.getName()));
			sessionMap.put((FieldType.CARD_EXP_DT.getName()),fields.get(FieldType.CARD_EXP_DT.getName()));
			sessionMap.put((FieldType.CVV.getName()),fields.get(FieldType.CVV.getName()));
			sessionMap.put((FieldType.INTERNAL_CARD_ISSUER_BANK.getName()),fields.get(FieldType.INTERNAL_CARD_ISSUER_BANK.getName()));
			sessionMap.put((FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()),fields.get(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()));
			fields.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(),"N");

			RequestRouter router = new RequestRouter(fields);
			fields.putAll(router.route());
			Processor processor = AcquireIntegratorFactory.instance(fields);
			processor.preProcess(fields);
			processor.process(fields);
			processor.postProcess(fields);

            

			// Retry Transaction Block Start
			String status = fields.get(FieldType.STATUS.getName());
			if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode()) 
					&& !(status.equals(StatusType.APPROVED.getName()) || (status.equals(StatusType.CAPTURED.getName())))) {
				//Fetch User for retryTransaction
				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
				RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
				if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
					addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());
					if (surchargeFlag.equals("Y")){
						return "surchargePaymentPage";
					} else {
						return "paymentPage";
					}
				}
			}// Retry Transaction Block End

			// Put transaction id as original txnId for subsequent transactions
			fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), txnId);
			//To check if acquirer is Citrus if acquirer null return nothing and check capture status			
			String acquirer = fields.get(FieldType.ACQUIRER_TYPE.getName());

			if ( (!StringUtils.isEmpty(acquirer)) && acquirer.equals(AcquirerType.CITRUS_PAY.getCode())) {
				setRedirectUrl(fields.get(FieldType.ACS_URL.getName()));
				if(!StringUtils.isBlank(getRedirectUrl())){
					acquirerFlag = "redirecturl";
				}else{
					sessionMap.invalidate();
					acquirerFlag = NONE;
				}
			} else if(!(pendingTxnStatus.contains(status))){
				sessionMap.invalidate();
				acquirerFlag = NONE;
			} else{
				acquirerFlag = NONE;
			}
		} catch (Exception exception) {
			logger.error("Error handling of transaction", exception);
			return ERROR;
		}
		return getAcquirerFlag();
	}
	// Save Card detail for Express Payment
	public void saveCard(Fields fields){
		TokenManager tokenManager = new TokenManager();
		fields.put(FieldType.KEY_ID.getName(), SystemConstants.DEFAULT_KEY_ID);
		try {
			tokenManager.addToken(fields);
		} catch (SystemException exception) {
			logger.error("Exception", exception);
		}
	}

	// Remove Saved Card
	public String deleteSavedCard(){
		Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());
		if(null != tokenId){
			fields.put(FieldType.TOKEN_ID.getName(), getTokenId());
			tokenManager.removeSavedCard(fields);
			tokenMap = tokenManager.getAll(fields);
			if (tokenMap.isEmpty()){
				sessionMap.put(Constants.TOKEN.getValue(), "NA");
			}
			else{
				sessionPut(Constants.TOKEN.getValue(), tokenMap);
			}
		}
		return SUCCESS;
	}

	public String directPay() {
		try {
			//clean session
			sessionMap.invalidate();

			// create new fields for direct transaction
			ActionService service = PgActionServiceFactory.getActionService();

			@SuppressWarnings("unchecked")
			Fields fields = service.prepareFields(request.getParameterMap());

			String fieldsAsString = fields.getFieldsAsBlobString();
			sessionMap.put(Constants.FIELDS.getValue(),fields);	
			fields.put(FieldType.INTERNAL_REQUEST_FIELDS.getName(), fieldsAsString);
			GeneralValidator generalValidator = new GeneralValidator();
			if(StringUtils.isBlank(fields.get(FieldType.HASH.getName()))){
				throw new SystemException(ErrorType.VALIDATION_FAILED, "Invalid " + FieldType.HASH.getName());	
			}
			generalValidator.validateHash(fields);
			generalValidator.validateReturnUrl(fields);

			User user = new UserDao().getUserClass(fields.get(FieldType.PAY_ID.getName()));

			if (PaymentType.CREDIT_CARD.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))
					|| PaymentType.DEBIT_CARD.getCode()
					.equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {
				//BIN Range API Call
				BinRangeProvider binRangeProvider= new BinRangeProvider();
				binRangeProvider.findBinRange(fields);
			}
			setOrigTxnType(ModeType.getDefaultPurchaseTransaction(user.getModeType()).getName());
			sessionMap.put((FieldType.INTERNAL_ORIG_TXN_TYPE.getName()),
					ModeType.getDefaultPurchaseTransaction(user.getModeType())
					.getName());
			fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), getOrigTxnType());

			generalValidator.validatePaymentType(fields);

			//put original transaction type
			sessionMap.put((FieldType.RETURN_URL.getName()),fields.get(FieldType.RETURN_URL.getName()));
			sessionMap.put((FieldType.CARD_NUMBER.getName()),fields.get(FieldType.CARD_NUMBER.getName()));
			sessionMap.put((FieldType.CARD_EXP_DT.getName()),fields.get(FieldType.CARD_EXP_DT.getName()));
			sessionMap.put((FieldType.CVV.getName()),fields.get(FieldType.CVV.getName()));
			sessionMap.put((FieldType.INTERNAL_CARD_ISSUER_BANK.getName()),fields.get(FieldType.INTERNAL_CARD_ISSUER_BANK.getName()));
			sessionMap.put((FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()),fields.get(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()));

			fields.put((FieldType.INTERNAL_CUST_IP.getName()), request.getRemoteAddr());

			//Fetch country name using IP Address
			LocateCountryName httpURLConnectionCountry = new LocateCountryName();
			String countryCode = httpURLConnectionCountry.findCountryCode(request.getRemoteAddr());
			if(StringUtils.isBlank(countryCode)){
				countryCode= "NA";
			}

			fields.put((FieldType.INTERNAL_CUST_COUNTRY_NAME.getName()), countryCode);
			sessionMap.put(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName(), fields.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName()));

			if(countryCode.equals(CrmFieldConstants.INDIA_REGION_CODE.getValue())) {
				fields.put((FieldType.INTERNAL_TXN_AUTHENTICATION.getName()), CustTransactionAuthentication.SUCCESS.getAuthenticationName());
			}
			else {
				fields.put((FieldType.INTERNAL_TXN_AUTHENTICATION.getName()), CustTransactionAuthentication.PENDING.getAuthenticationName());
			}

			setBusinessName(user.getBusinessName());
			fields.put(FieldType.TXNTYPE.getName(),TransactionType.ENROLL.getName());
			fields.clean();
			fields.removeExtraFields();
			fields.put((FieldType.INTERNAL_CARD_ISSUER_BANK.getName()),(String) sessionMap.get(FieldType.INTERNAL_CARD_ISSUER_BANK.getName()));
			fields.put((FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()),(String) sessionMap.get(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()));	
			fields.put(FieldType.INTERNAL_REQUEST_FIELDS.getName(), fieldsAsString);
			fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), getOrigTxnType());
			fields.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(),"N");
			//Put merchant hosted page flag
			fields.put(FieldType.IS_MERCHANT_HOSTED.getName(),"Y");

			RequestRouter router = new RequestRouter(fields);
			fields.putAll(router.route());
			
			Processor processor = AcquireIntegratorFactory.instance(fields);
			processor.preProcess(fields);
			processor.process(fields);
			processor.postProcess(fields);

			// Put transaction id as original txnId for subsequent transactions
			fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(),fields.get(FieldType.ORIG_TXN_ID.getName()));
			fields.remove(FieldType.ORIG_TXN_ID.getName());

			//Put ORIG_TXN_ID in session for updating transaction status in case of timeout
			sessionMap.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.INTERNAL_ORIG_TXN_ID.getName()));

			//To check if acquirer is Citrus if acquirer null return nothing
			String acquirer = fields.get(FieldType.ACQUIRER_TYPE.getName());
			if ( (!StringUtils.isEmpty(acquirer)) && acquirer.equals(AcquirerType.CITRUS_PAY.getCode())) {
				setRedirectUrl(fields.get(FieldType.ACS_URL.getName()));
				if(!StringUtils.isBlank(getRedirectUrl())){
					acquirerFlag = "redirecturl";
				}else{
					acquirerFlag = NONE;
				}
			} else {
				acquirerFlag = NONE;
			}
		}catch (SystemException systemException) {
			logger.error("Exception", systemException);
			Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());

			if(null==fields){	
				return "invalidRequest";
			}

			String salt = (new PropertiesManager()).getSalt(fields
					.get(FieldType.PAY_ID.getName()));

			if(null==salt){
				return "invalidRequest";
			}

			if(StringUtils.isBlank(getOrigTxnType())){
				User user = new UserDao().getUserClass(fields.get(FieldType.PAY_ID.getName()));
				setOrigTxnType(ModeType.getDefaultPurchaseTransaction(user.getModeType()).getName());
			}

			fields.put(FieldType.TXNTYPE.getName(), getOrigTxnType());
			String responseCode = systemException.getErrorType().getCode();

			fields.put(FieldType.RESPONSE_CODE.getName(), responseCode);
			fields.put(FieldType.RESPONSE_MESSAGE.getName(), systemException
					.getErrorType().getResponseMessage());

			if(responseCode.equals(ErrorType.VALIDATION_FAILED.getCode())){
				saveInvalidTransaction(fields);
			}
			//If an invalid request of valid merchant save it
			if(fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.INVALID_RETURN_URL.getCode())){
				return "invalidRequest";
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.create(fields);
			fields.removeInternalFields();
			responseCreator.ResponsePost(fields);
			sessionMap.invalidate();
			return NONE;
		}catch (Exception exception) {
			//TODO... params still accessible at jsp overwrite them
			sessionMap.invalidate();
			logger.error("Error handling of transaction", exception);
			ServletActionContext.getContext().getParameters().clear();
			return ERROR;
		}
		return getAcquirerFlag();
	}

	//for payment modes that are not supported
	public void prepareUnsupportedPaymnetResponse(Fields fields) throws SystemException{
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.PAYMENT_OPTION_NOT_SUPPORTED.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.PAYMENT_OPTION_NOT_SUPPORTED.getCode());
		ResponseCreator responseCreator = new ResponseCreator();
		TransactionResponser transactionResponser = new TransactionResponser();
		transactionResponser.removeInvalidResponseFields(fields);
		transactionResponser.addResponseDateTime(fields);
		fields.removeSecureFields();
		transactionResponser.addHash(fields);
		responseCreator.ResponsePost(fields);
	}

	public HttpServletRequest getHttpRequest() {
		return request;
	}

	public void setHttpRequest(HttpServletRequest httpRequest) {
		this.request = httpRequest;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiryYear() {
		return expiryYear;
	}

	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}

	public String getExpiryMonth() {
		return expiryMonth;
	}

	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}

	public String getCvvNumber() {
		return cvvNumber;
	}

	public void setCvvNumber(String cvvNumber) {
		this.cvvNumber = cvvNumber;
	}

	public String getStoreCardFlag() {
		return storeCardFlag;
	}

	public void setStoreCardFlag(String storeCardFlag) {
		this.storeCardFlag = storeCardFlag;
	}

	public String getOrigTxnType() {
		return origTxnType;
	}

	public void setOrigTxnType(String origTxnType) {
		this.origTxnType = origTxnType;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getMopType() {
		return mopType;
	}

	public void setMopType(String mopType) {
		this.mopType = mopType;
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.request = hReq;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAcquirerFlag() {
		return acquirerFlag;
	}

	public void setAcquirerFlag(String acquirerFlag) {
		this.acquirerFlag = acquirerFlag;
	}

	public String getResponseUrl() {
		return responseUrl;
	}

	public void setResponseUrl(String responseUrl) {
		this.responseUrl = responseUrl;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public Map<String, Object> getSupportedPaymentTypeMap() {
		return supportedPaymentTypeMap;
	}

	public void setSupportedPaymentTypeMap(
			Map<String, Object> supportedPaymentTypeMap) {
		this.supportedPaymentTypeMap = supportedPaymentTypeMap;
	}

	public Map<String, Object> getCardPaymentTypeMap() {
		return cardPaymentTypeMap;
	}

	public void setCardPaymentTypeMap(Map<String, Object> cardPaymentTypeMap) {
		this.cardPaymentTypeMap = cardPaymentTypeMap;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public boolean isCardsaveflag() {
		return cardsaveflag;
	}

	public void setCardsaveflag(boolean cardsaveflag) {
		this.cardsaveflag = cardsaveflag;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public Map<String, Object> getTokenMap() {
		return tokenMap;
	}

	public void setTokenMap(Map<String, Object> tokenMap) {
		this.tokenMap = tokenMap;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public String getWalletName() {
		return walletName;
	}

	public void setWalletName(String walletName) {
		this.walletName = walletName;
	}
	
	
}