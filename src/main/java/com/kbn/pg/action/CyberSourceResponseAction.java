package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.RequestRouter;
import com.kbn.pg.core.ResponseCreator;
import com.opensymphony.xwork2.Action;

public class CyberSourceResponseAction extends AbstractSecureAction implements ServletRequestAware {

	/**
	 * @ neeraj
	 */
	private static final long serialVersionUID = 3075991549431075853L;
	private static Logger logger = Logger.getLogger(CyberSourceResponseAction.class.getName());
	private Fields responseMap = null;
	private HttpServletRequest httpRequest;
	private String redirectUrl;
	private Integer count;

	public CyberSourceResponseAction() {
	}

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, String[]> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> requestMap = new HashMap<String, String>();

			for (Entry<String, String[]> entry : fieldMapObj.entrySet()) {
				try {
					requestMap.put(entry.getKey(), ((String[]) entry.getValue())[0]);

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}

			String paRes = requestMap.get("PaRes");
			String md = requestMap.get("MD");

			Fields fields = new Fields();
			fields.put(FieldType.MD.getName(), md);
			fields.put(FieldType.PARES.getName(), paRes);

			fields.logAllFields("3DS Recieved Map :");

			Object fieldsObj = sessionMap.get("FIELDS");
			if (null != fieldsObj) {
				fields.put((Fields) fieldsObj);
			}
			logger.info("Get CyberSourcer Response : " + requestMap);
			fields.logAllFields("Updated 3DS Recieved Map TxnType = " + fields.get(FieldType.TXNTYPE.getName()) + " "
					+ "Txn id = " + fields.get(FieldType.TXN_ID.getName()));
			fields.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.CYBER_SOURCE.getCode());
			fields.put(FieldType.TXNTYPE.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()));
			fields.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(), "N");
			fields.put((FieldType.PAYMENTS_REGION.getName()),
					(String) sessionMap.get(FieldType.PAYMENTS_REGION.getName()));
			fields.put((FieldType.INTERNAL_ORIG_TXN_ID.getName()),
					(String) sessionMap.get(FieldType.INTERNAL_ORIG_TXN_ID.getName()));
			fields.put((FieldType.CVV.getName()), (String) sessionMap.get(FieldType.CVV.getName()));
			fields.put((FieldType.CARD_NUMBER.getName()), (String) sessionMap.get(FieldType.CARD_NUMBER.getName()));
			fields.put((FieldType.CARD_EXP_DT.getName()), (String) sessionMap.get(FieldType.CARD_EXP_DT.getName()));
			fields.remove(FieldType.TXN_ID.getName());
			fields.put((FieldType.OID.getName()), (String) sessionMap.get(FieldType.OID.getName()));
			RequestRouter router = new RequestRouter(fields);
			responseMap = new Fields(router.route());

			// Fetch user for retryTransaction ,SendEmailer and SmsSenser
			UserDao userDao = new UserDao();
			User user = userDao.getUserClass(responseMap.get(FieldType.PAY_ID.getName()));

			Object previousFields = sessionMap.get(Constants.FIELDS.getValue());
			Fields sessionFields = null;
			if (null != previousFields) {
				sessionFields = (Fields) previousFields;
			} else {
			}

			// Sending Email for Transaction Status to merchant

			String countryCode = (String) sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
			EmailSender emailSender = new EmailSender();
			emailSender.postMan(responseMap, countryCode, user);

			sessionFields.put(responseMap);
			fields.put(FieldType.RETURN_URL.getName(), (String) sessionMap.get(FieldType.RETURN_URL.getName()));
			String cardIssuerBank = (String) sessionMap.get(FieldType.INTERNAL_CARD_ISSUER_BANK.getName());
			String cardIssuerCountry = (String) sessionMap.get(FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName());
			if (StringUtils.isNotBlank(cardIssuerBank)) {
				responseMap.put(FieldType.CARD_ISSUER_BANK.getName(), cardIssuerBank);
			}
			if (StringUtils.isNotBlank(cardIssuerCountry)) {
				responseMap.put(FieldType.CARD_ISSUER_COUNTRY.getName(), cardIssuerCountry);
			}

			responseMap.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
			if (sessionMap != null) {
				sessionMap.put(Constants.TRANSACTION_COMPLETE_FLAG.getValue(), Constants.Y_FLAG.getValue());
				sessionMap.invalidate();
			}
			responseMap.remove(FieldType.ACQUIRER_TYPE.getName());
			responseMap.remove(FieldType.MD.getName());
			responseMap.remove(FieldType.MERCHANT_ID.getName());
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(responseMap);

		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return Action.NONE;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

}