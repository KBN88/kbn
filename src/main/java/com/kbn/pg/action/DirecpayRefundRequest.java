package com.kbn.pg.action;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.DateCreater;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.RequestRouter;

public class DirecpayRefundRequest extends AbstractSecureAction {

	private static final long serialVersionUID = -2664470473183928054L;
	private static Logger logger = Logger.getLogger(DirecpayRefundRequest.class.getName());
	private String origTxnId;
	private String amount;
	private String payId;
	private String currencyCode;
	private String orderId;
	private String createDate;
	private String txnId;
	private String message;

	public String execute() {
		try {
			HttpServletRequest request = ServletActionContext.getRequest();;
			
		     User user=(User) sessionMap.get(Constants.USER.getValue());
			Fields responseMap = null;
			Map<String, String> requestMap = new HashMap<String, String>();

			// format amount first
			requestMap.put(FieldType.AMOUNT.getName(),
					Amount.formatAmount(amount, currencyCode));

			requestMap.put(FieldType.ORIG_TXN_ID.getName(), origTxnId);
			requestMap.put(FieldType.PAY_ID.getName(), payId);
			requestMap.put(FieldType.TXNTYPE.getName(),
					TransactionType.REFUND.getName());
			requestMap.put(FieldType.CURRENCY_CODE.getName(),
					currencyCode);	
			requestMap.put(FieldType.INTERNAL_USER_EMAIL.getName(), user.getEmailId());
			requestMap
					.put(FieldType.HASH.getName(),
							"1234567890123456789012345678901234567890123456789012345678901234"); 																									
			requestMap
			.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(),
					"N"); 
																							
			// Preparing fields
			Fields fields = new Fields(requestMap);
			fields.put((FieldType.INTERNAL_CUST_IP.getName()), request.getRemoteAddr());
			fields.logAllFields("All request fields :");
			RequestRouter router = new RequestRouter(fields);
			responseMap = new Fields(router.route());
			sessionMap.put(Constants.FIELDS.getValue(), fields);
			
			String responseCode = responseMap.get(FieldType.RESPONSE_CODE
					.getName());
			//send SMS
			//SmsSender.sendSMS(fields);
				
			return NONE;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return INPUT;
		}
	}
		
	public void validate() {		
		 Date currentDate = new Date();		
		 GregorianCalendar cal = new GregorianCalendar();
		 CrmValidator validator = new CrmValidator();
		 cal.setTime(DateCreater.formatStringToDate(getCreateDate()));
		 cal.add(Calendar.DATE, 2);
		 Long dateDifference =  currentDate.getTime() - cal.getTime().getTime();
		if(dateDifference < 0 || dateDifference > 2419200000L) {
			addFieldError("error", CrmFieldConstants.NETBANKING_VALIDATION.getValue());
			setMessage(CrmFieldConstants.NETBANKING_VALIDATION.getValue());
		}
		
		if (validator.validateBlankField(getOrigTxnId())) {
		} else if (!validator.validateField(CrmFieldType.TRANSACTION_ID,
				getOrigTxnId())) {
			addFieldError(CrmFieldConstants.ORIG_TXN_ID.getValue(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getAmount())) {
		} else if (!validator.validateField(CrmFieldType.AMOUNT, getAmount())) {
			addFieldError(CrmFieldType.CURRENCY.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getPayId())) {
		} else if (!validator.validateField(CrmFieldType.PAY_ID, getPayId())) {
			addFieldError(CrmFieldType.PAY_ID.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getOrderId())) {
		} else if (!validator
				.validateField(CrmFieldType.ORDER_ID, getOrderId())) {
			addFieldError(CrmFieldType.ORDER_ID.getName(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getCurrencyCode())) {
		} else if (!validator.validateField(CrmFieldType.CURRENCY,
				getCurrencyCode())) {
			addFieldError(CrmFieldConstants.CURRENCY_CODE.getValue(),
					ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrigTxnId() {
		return origTxnId;
	}

	public void setOrigTxnId(String origTxnId) {
		this.origTxnId = origTxnId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
