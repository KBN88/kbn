package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;
import com.kbn.timesofmoney.DirecpayUtil;
import com.kbn.timesofmoney.ResponseFactory;
import com.kbn.timesofmoney.ResponseProcessor;
import com.opensymphony.xwork2.Action;

/**
 * @author Sunil
 *
 */
public class DirecpayResponseAction extends AbstractSecureAction implements
		ServletRequestAware {

	private static Logger logger = Logger
			.getLogger(DirecpayResponseAction.class.getName());
	private static final long serialVersionUID = 2943629615913246334L;

	private static final String failedResponse = "00000000000000|FAIL|IND|INR|null|";

	private HttpServletRequest httpRequest;

	@SuppressWarnings("unchecked")
	public String execute() {
		try {

			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(),
							((String[]) entry.getValue())[0].trim());
				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}

			Fields fields = (Fields) sessionMap
					.get(Constants.FIELDS.getValue());
			String password = "";
			if (null != fields) {
				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);
				password = fields.get(FieldType.PASSWORD.getName());
			}

			String responseString = responseMap
					.get(com.kbn.timesofmoney.Constants.RESPONSE_PARAMS);
			logger.info("Message recieved from direcpay  " + responseString);
			responseMap.clear();
			String flag = ConfigurationConstants.DIRECPAY_LIVE_FLAG.getValue();
			if (!StringUtils.isEmpty(responseString)) {
				if (Integer.parseInt(flag) == 1) {
					responseString = DirecpayUtil.decrypt(responseString,
							password);
					logger.info("Decrypted message recieved from direcpay  "
							+ responseString);
				}
			} else {
				responseString = failedResponse
						+ fields.get(FieldType.TXN_ID.getName())
						+ Constants.DIRECPAY_SEPARATOR.getValue()
						+ Amount.toDecimal(
								fields.get(FieldType.AMOUNT.getName()),
								fields.get(FieldType.CURRENCY_CODE.getName()));
				logger.info("Invalid response from direcpay because of incompatible request"
						+ fields.get(FieldType.TXN_ID.getName()));
			}

			responseMap = ResponseFactory.parse(responseString);

			fields.putAll(responseMap);

			try {
				Processor processor = new ResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);
				// Fetch user for retryTransaction ,SendEmailer and SmsSenser

				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID
						.getName()));
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(
						ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields,
							sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION
								.getValue());
						return "paymentPage";
					}
				}
				// Retry Transaction Block End

				// Sending Email for Transaction Status to merchant

				String countryCode = (String) sessionMap
						.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);
				
				fields.logAllFields("Direcpay Recieved Map :");
				fields.put(FieldType.RETURN_URL.getName(),
						(String) sessionMap.get(FieldType.RETURN_URL.getName()));

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				logger.error("Exception", systemException);
				fields.put(FieldType.STATUS.getName(),
						StatusType.ERROR.getName());
				fields.put(FieldType.RESPONSE_MESSAGE.getName(),
						ErrorType.INTERNAL_SYSTEM_ERROR.getResponseMessage());
				fields.put(FieldType.RESPONSE_CODE.getName(),
						ErrorType.INTERNAL_SYSTEM_ERROR.getResponseCode());
				ResponseCreator responseCreator = new ResponseCreator();
				responseCreator.create(fields);
				responseCreator.ResponsePost(fields);
				sessionMap.clear();
				return NONE;
			}

			if (sessionMap != null) {
				sessionMap.put(Constants.TRANSACTION_COMPLETE_FLAG.getValue(),
						Constants.Y_FLAG.getValue());
				sessionMap.invalidate();
			}
			// Temp
			// fields.put(FieldType.HASH.getName(), Hasher.getHash(fields));
			fields.remove(FieldType.HASH.getName());
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.create(fields);
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN
							.getName()));
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return Action.NONE;
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}
}
