package com.kbn.pg.action;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.fss.plugin.sbi.iPayPipe;
import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;
import com.kbn.sbi.SBIEncryptionUtil;
import com.kbn.sbi.SBINetbankingTransformer;
import com.kbn.sbi.SBIResponseProcessor;
import com.kbn.sbi.SBITransformer;
import com.kbn.sbi.TransactionCommunicator;
import com.kbn.sbi.TransactionConverter;
import com.opensymphony.xwork2.Action;

public class SBIResponseAction extends AbstractSecureAction implements ServletRequestAware {

	/**
	 * @author neeraj
	 */
	private static final long serialVersionUID = 5770373671505815189L;
	private static Logger logger = Logger.getLogger(SBIResponseAction.class.getName());
	private HttpServletRequest httpRequest;

	public SBIResponseAction() {
	}

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(), ((String[]) entry.getValue())[0].trim());
				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());
			// Net banking response handling
			if (PaymentType.NET_BANKING.getCode().equals(fields.get(FieldType.PAYMENT_TYPE.getName()))) {

				String keyPath = new PropertiesManager().getSystemProperty("SBINetbankingKeyPath");
				String netBankingResponse = SBIEncryptionUtil.decrypt(responseMap.get("encdata").toString(), keyPath);

				Map<String, String> mapResponse = Stream.of(netBankingResponse.split("\\|"))
						.collect(Collectors.toMap(t -> t.toString().split("=")[0], t -> t.toString().split("=")[1]));

				// DoubleVerification
				TransactionConverter converter = new TransactionConverter();
				TransactionCommunicator communicator = new TransactionCommunicator();

				Map<String, String> verificationRequest = converter
						.createDoubleVerificationRequest(mapResponse.get("Amount"), mapResponse.get("Ref_no"));
				logger.info("SBI Double Verification Request:" + verificationRequest);

				String response = communicator.transactStatus(ConfigurationConstants.SBI_DOUBLE_VERIFICATION_URL.getValue(),
						verificationRequest.get("encdata"), verificationRequest.get("merchant_code"));

				// decrypting response from SBI
				String doubleVerificationResponse = SBIEncryptionUtil.decrypt(response, keyPath);
				logger.info("SBI Double Verification Decrypted Response:" + doubleVerificationResponse);

				Map<String, String> doubleVerificationmap = Stream.of(doubleVerificationResponse.split("\\|"))
						.collect(Collectors.toMap(t -> t.toString().split("=")[0], t -> t.toString().split("=")[1]));

				logger.info("Session fields value" + fields.get(FieldType.PAY_ID.getName()));
				logger.info("Session fields value" + fields.get(FieldType.TXN_ID.getName()));
				fields.logAllFields("session fields for SBI");
				if (null != fields) {
					SecurityProcessor securityProcessor = new SecurityProcessor();
					securityProcessor.authenticate(fields);
					securityProcessor.addAcquirerFields(fields);
				}

				// updating SBI Net Banking transaction in database
				SBINetbankingTransformer sbiNetbankingTransformer = new SBINetbankingTransformer();
				sbiNetbankingTransformer.updateSBINetbankingResponse(fields, doubleVerificationmap);

			} else {
				String merchantKey = fields.get(FieldType.MERCHANT_ID.getName());
				String password = fields.get(FieldType.PASSWORD.getName());
				String payId = fields.get(FieldType.PAY_ID.getName());
				String acquirer = fields.get(FieldType.INTERNAL_ACQUIRER_TYPE.getName());
				String alias =null;
				/**
				 *  implements get TXN_KEY neeraj 17-05-2019 
				 */
				if(merchantKey == null || password==null) {
					UserDao userDao = new UserDao();
					User user = userDao.findBySafexpayUser(payId);
					Account account = user.getAccountUsingAcquirerCode(acquirer);
					String currencyCode = fields.get(FieldType.CURRENCY_CODE.getName());
					AccountCurrency accountCurrency = account.getAccountCurrency(currencyCode);	
					//Get key
					merchantKey = accountCurrency.getMerchantId();
					password = accountCurrency.getPassword();
					alias =AccountPasswordScrambler.decryptPassword(password);
					
				   logger.info("Null Txn_Key :" + merchantKey);
				}
				iPayPipe pipe = new iPayPipe();
				String folderName = merchantKey ;
				String driverImagePath = folderName;
				File FileLocation1 =null;
				String str ="";
				String file = new PropertiesManager().getSystemProperty("ResourcePath");        
			    FileLocation1 = new File(file + File.separator + driverImagePath + File.separator +"cgn");
			    str = FileLocation1.toString();
			    
				//String resourcePath = new PropertiesManager().getSystemProperty("ResourcePath");
				//String keystorePath = new PropertiesManager().getSystemProperty("ResourcePath");
				//String aliasName = "BhartiPay";//Live
				String aliasName = alias;
				/*pipe.setResourcePath(resourcePath);
				pipe.setKeystorePath(keystorePath);*/
				pipe.setResourcePath(str);
				pipe.setKeystorePath(str);
				pipe.setAlias(aliasName); // The method to be called to decrypt the response sent by Payment Gateway
				int result = pipe.parseEncryptedResult(httpRequest.getParameter("trandata"));
				// logger.info(httpRequest.getParameter("trandata"));
				logger.info("SBI parseEncryptedResult:" + result);
				/*
				 * String trandata = responseMap.get("trandata"); int result =
				 * pipe.parseEncryptedResult(trandata);
				 */
				logger.info(httpRequest.getParameter("ErrorText"));
				if (httpRequest.getParameter("ErrorText") == null) {
					System.out.println(httpRequest.getParameter("ErrorText"));
					logger.info(httpRequest.getParameter("ErrorText"));
				}
				String Result = pipe.getResult(); // gives the value in the tag
				String PostDate = pipe.getDate(); // contains post date
				String refNum = pipe.getRef(); // contains RRN no generated by PG
				String trackId = pipe.getTrackId(); // contains merchant track ID
				String tranId = pipe.getTransId(); // contains PG Transaction ID
				String amt = pipe.getAmt(); // contains transaction amount
				String paymentId = pipe.getPaymentId(); // contains payment ID
				String auth = pipe.getAuth(); // contains Auth code
				String errorText = pipe.getError_text(); // contains get Error Text
				String error = pipe.getError(); // contains get Error Text

				// Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());
				logger.info("Message recieved from SBI  " + "Result=" + Result + "PostDate=" + PostDate + "refNum="
						+ refNum + "trackI=" + trackId + "tranId=" + tranId + "amt=" + amt + "paymentId=" + paymentId
						+ "auth=" + auth + "errorText=" + errorText + "error=" + error);

				// TODO Blob response Fields
				// fields.put(FieldType.RESPONSE_FIELDS.getName(), hmDecryptedValue.toString());
				logger.info("Session fields value" + fields.get(FieldType.PAY_ID.getName()));
				logger.info("Session fields value" + fields.get(FieldType.TXN_ID.getName()));
				fields.logAllFields("session fields for SBI");
				if (null != fields) {
					SecurityProcessor securityProcessor = new SecurityProcessor();
					securityProcessor.authenticate(fields);
					securityProcessor.addAcquirerFields(fields);
				}
				logger.info("Message recieved from SBI  " + error);

				SBITransformer sbiTransformer = new SBITransformer(pipe);
				sbiTransformer.updateSBIResponse(fields);

			}
			try {
				Processor processor = new SBIResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);

				// Fetch user for Re-try Transaction ,Emailer and Sms Processor

				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());
						return "paymentPage";
					}
				}
				// Retry Transaction Block End
				logger.info("After Retry Block");

				// Sending Email for Transaction Status to merchant
				fields.put(FieldType.PAY_ID.getName(), fields.get(FieldType.PAY_ID.getName()));
				fields.put(FieldType.ACQUIRER_TYPE.getName(),
						(String) sessionMap.get(FieldType.ACQUIRER_TYPE.getName()));

				String countryCode = (String) sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);

				fields.put(FieldType.RETURN_URL.getName(), (String) sessionMap.get(FieldType.RETURN_URL.getName()));

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				// TODO....update fields as error??
				logger.error("Exception", systemException);
			}
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
			if (sessionMap != null) {
				sessionMap.clear();
			}

			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			// non reachable ideally
			logger.error("Exception", exception);
			return ERROR;
		}
		if (sessionMap != null) {
			sessionMap.put(Constants.TRANSACTION_COMPLETE_FLAG.getValue(), Constants.Y_FLAG.getValue());
			sessionMap.invalidate();
		}
		logger.info("after action None");
		return Action.NONE;

	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}
}