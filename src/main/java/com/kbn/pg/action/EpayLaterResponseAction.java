package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.epaylater.EPayLaterTransformer;
import com.kbn.epaylater.EpayLaterEncryptDecryptUtil;
import com.kbn.epaylater.EpayLaterResponseProcessor;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;
import com.opensymphony.xwork2.Action;

public class EpayLaterResponseAction extends AbstractSecureAction implements ServletRequestAware {

	/**
	 * @ neeraj
	 */
	private static final long serialVersionUID = -7859470605454020782L;
	private static Logger logger = Logger.getLogger(EpayLaterResponseAction.class.getName());

	private HttpServletRequest httpRequest;

	@Override
	@SuppressWarnings({ "unchecked", "static-access" })
	public String execute() {
		try {
			EpayLaterEncryptDecryptUtil epayLaterEncryptDecryptUtil = new EpayLaterEncryptDecryptUtil();
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(), ((String[]) entry.getValue())[0].trim());
				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			String encode = responseMap.get("encdata");
			String responseField = epayLaterEncryptDecryptUtil.decryptBase64EncodedAES256(
					ConfigurationConstants.EPAYLATER_AES_KEY.getValue(), ConfigurationConstants.EPAYLATER_IV.getValue(),
					encode);
			Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());
			logger.info("Message recieved from EPAYLATER  " + responseField);
			// TODO Blob response Fields
			fields.put(FieldType.RESPONSE_FIELDS.getName(), responseField);
			logger.info("Session fields value" + fields.get(FieldType.PAY_ID.getName()));
			logger.info("Session fields value" + fields.get(FieldType.TXN_ID.getName()));
			fields.logAllFields("session fields for EPAYLATER");
			if (null != fields) {
				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);
			}
			EPayLaterTransformer epayLaterTransformer = new EPayLaterTransformer(responseField);
			epayLaterTransformer.updateEpayLaterResponse(fields);

			try {
				Processor processor = new EpayLaterResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);

				// Fetch user for Re-try Transaction ,Emailer and Sms Processor

				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());
						return "paymentPage";
					}
				}
				// Retry Transaction Block End

				// Sending Email for Transaction Status to merchant
				fields.put(FieldType.PAY_ID.getName(), fields.get(FieldType.PAY_ID.getName()));
				fields.put(FieldType.ACQUIRER_TYPE.getName(),
						(String) sessionMap.get(FieldType.ACQUIRER_TYPE.getName()));

				String countryCode = (String) sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);

				fields.put(FieldType.RETURN_URL.getName(), (String) sessionMap.get(FieldType.RETURN_URL.getName()));

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				// TODO....update fields as error??
				logger.error("Exception", systemException);
			}
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
			if (sessionMap != null) {
				sessionMap.clear();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return Action.NONE;
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;

	}

}
