package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.citruspay.ResponseProcessor;
import com.kbn.commons.crypto.CryptoManager;
import com.kbn.commons.crypto.CryptoManagerFactory;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.StatusType;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.recurringPayments.RecurringPaymentProcessor;
import com.kbn.pg.security.SecurityProcessor;
import com.opensymphony.xwork2.Action;

/**
 * @author Sunil, Neeraj
 *
 */
public class CitrusPayResponseAction extends AbstractSecureAction implements
		ServletRequestAware {

	private static Logger logger = Logger
			.getLogger(CitrusPayResponseAction.class.getName());
	private static final long serialVersionUID = 6890461481198123932L;
	private HttpServletRequest httpRequest;

	public CitrusPayResponseAction() {
	}

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(),
							((String[]) entry.getValue())[0].trim());
				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			Fields fields = (Fields) sessionMap
					.get(Constants.FIELDS.getValue());

			SecurityProcessor securityProcessor = new SecurityProcessor();
			securityProcessor.authenticate(fields);
			securityProcessor.addAcquirerFields(fields);
			fields.putAll(responseMap);
			fields.logAllFields("CitrusPay MPI Recieved Map :");

			try {
				Processor processor = new ResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);
			} catch (SystemException systemException) {

				// if error code is for hash failed
				if (systemException.getErrorType().getCode()
						.equals(ErrorType.SIGNATURE_MISMATCH.getCode())) {
					fields.put(FieldType.RESPONSE_CODE.getName(),
							ErrorType.SIGNATURE_MISMATCH.getCode());
					fields.put(FieldType.RESPONSE_MESSAGE.getName(),
							ErrorType.SIGNATURE_MISMATCH.getResponseMessage());
					fields.put(FieldType.STATUS.getName(),
							StatusType.DENIED.getName());
				} else {
					fields.put(FieldType.RESPONSE_CODE.getName(),
							ErrorType.INTERNAL_SYSTEM_ERROR.getResponseCode());
					fields.put(FieldType.RESPONSE_MESSAGE.getName(),
							ErrorType.INTERNAL_SYSTEM_ERROR
									.getResponseMessage());
					fields.put(FieldType.STATUS.getName(),
							StatusType.ERROR.getName());
				}
				fields.updateTransactionDetails();
				fields.updateNewOrderDetails();
				// if something else add different codes
				logger.error("Exception", systemException);
			}
			
			// Fetch user for retryTransaction ,SendEmailer and SmsSenser
			UserDao userDao = new UserDao();
			User user = userDao.getUserClass(fields.get(FieldType.PAY_ID
					.getName()));
			// Retry Transaction Block Start
			if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(
					ErrorType.SUCCESS.getCode())) {
				RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
				if (retryTransactionProcessor.retryTransaction(fields,
						sessionMap, user)) {
					addActionMessage(CrmFieldConstants.RETRY_TRANSACTION
							.getValue());
					return "paymentPage";
				}
			}
			// Retry Transaction Block End

			// Recurring payment processor check conditions and then process
			String recurringTxnFlag = fields.get(FieldType.IS_RECURRING
					.getName());
			if (StringUtils.isNoneBlank(recurringTxnFlag)
					&& recurringTxnFlag.equals(Constants.Y_FLAG.getValue())
					&& (fields.get(FieldType.RESPONSE_CODE.getName()))
							.equals(ErrorType.SUCCESS.getCode())
					&& (fields.get(FieldType.PAYMENT_TYPE.getName()))
							.equals(PaymentType.CREDIT_CARD.getCode())) {
				logger.info("recurring payment block started:::::::::::");
				fields.put(FieldType.CARD_NUMBER.getName(), (String) sessionMap
						.get(FieldType.CARD_NUMBER.getName()));
				fields.put(FieldType.CARD_EXP_DT.getName(), (String) sessionMap
						.get(FieldType.CARD_EXP_DT.getName()));
				Processor processor = new RecurringPaymentProcessor();
				processor.preProcess(fields);
				processor.process(fields);
				processor.postProcess(fields);
				CryptoManager cryptoManager = CryptoManagerFactory
						.getCryptoManager();
				cryptoManager.secure(fields);
			}

			// Sending Email for Transaction Status to merchant

			String countryCode = (String) sessionMap
					.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
			EmailSender emailSender = new EmailSender();
			emailSender.postMan(fields, countryCode, user);

			fields.put(FieldType.RETURN_URL.getName(),
					(String) sessionMap.get(FieldType.RETURN_URL.getName()));
			ResponseCreator responseCreator = new ResponseCreator();
			// remove fields not required by merchant
			fields.remove(FieldType.HASH.getName());
			fields.remove(FieldType.ACS_URL.getName());
			responseCreator.create(fields);
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN
							.getName()));
			responseCreator.ResponsePost(fields);
		} catch (Exception exception) {
			// non reachable ideally
			logger.error("Exception", exception);
			return ERROR;
		}
		if (sessionMap != null) {
			sessionMap.put(Constants.TRANSACTION_COMPLETE_FLAG.getValue(),
					Constants.Y_FLAG.getValue());
			sessionMap.invalidate();
		}
		return Action.NONE;
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}
}
