package com.kbn.pg.action;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.axisupi.AxisBankTransformer;
import com.kbn.axisupi.AxisResponseProcessor;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.gpay.GpayTransactionService;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;
import com.opensymphony.xwork2.Action;

public class GpayResponseAction extends AbstractSecureAction implements ServletRequestAware {
	private static Logger logger = Logger.getLogger(GpayResponseAction.class.getName());
	/**
	 * @ neeraj
	 */
	private static final long serialVersionUID = -3812859523254750431L;
	private HttpServletRequest httpRequest;
	private GpayTransactionService gpayTransactionService = new GpayTransactionService();

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> requestMap = new HashMap<String, String>();

			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					requestMap.put(entry.getKey(), ((String[]) entry.getValue())[0]);

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			String encryptedData = "";
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = httpRequest.getReader();
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			encryptedData = builder.toString();
			logger.info("Response Json Data:" + encryptedData);
			JSONParser parser = new JSONParser();
			Object obj = null;
			try {
				obj = parser.parse(encryptedData);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			JSONObject jObject = (JSONObject) obj;
			logger.info("GET Response Data:" + jObject.get("data").toString());
			String data = AxisAesUtil.decrypt(jObject.get("data").toString(), "a2UUImt13urnku2H");
			logger.info("Message recieved from Axis BANK:" + data);
			JSONParser parser1 = new JSONParser();
			Object obj1 = null;
			try {
				obj1 = parser1.parse(data);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			JSONObject jObject1 = (JSONObject) obj1;

			String Txn_Id = jObject1.get("merchantTransactionId").toString();
			logger.info("Txn Id :" + Txn_Id);
			Map<String, String> field = gpayTransactionService.fetchOrderID(Txn_Id);
			logger.info("Session fields value1" + field.get(FieldType.PAY_ID.getName()));
			Fields fields = new Fields(field);

			// TODO Blob response Fields
			fields.put(FieldType.RESPONSE_FIELDS.getName(), data.toString());
			logger.info("Session fields value" + fields.get(FieldType.PAY_ID.getName()));
			logger.info("Session fields value" + fields.get(FieldType.TXN_ID.getName()));
			fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.TXN_ID.getName()));
			fields.logAllFields("session fields for Axis BANK");
			if (null != fields) {
				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);
			}
			AxisBankTransformer airtelBankTransformer = new AxisBankTransformer(data);
			airtelBankTransformer.updateAxisBankResponse(fields);

			try {
				Processor processor = new AxisResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);

				// Fetch user for Re-try Transaction ,Emailer and Sms Processor

				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());
						return "paymentPage";
					}
				}

				// Retry Transaction Block End

				// Sending Email for Transaction Status to merchant
				fields.put(FieldType.PAY_ID.getName(), fields.get(FieldType.PAY_ID.getName()));
				fields.put(FieldType.ACQUIRER_TYPE.getName(), fields.get(FieldType.ACQUIRER_TYPE.getName()));

				String countryCode = (String) sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				if (countryCode == null) {
					countryCode = "NA";
				}
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);
				fields.put(FieldType.RETURN_URL.getName(), "https://uat.mmadpay.com/crm/jsp/response.jsp");

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				// TODO....update fields as error??
				logger.error("Exception", systemException);
			}
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
			if (sessionMap != null) {
				sessionMap.clear();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
			exception.printStackTrace();
		}
		return Action.NONE;
	}

	@Override
	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

}
