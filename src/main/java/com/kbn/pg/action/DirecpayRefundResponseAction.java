package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailBuilder;
import com.kbn.pg.core.Processor;
import com.kbn.timesofmoney.ResponseFactory;
import com.kbn.timesofmoney.ResponseProcessor;
/**
 * @author Sunil
 *
 */
public class DirecpayRefundResponseAction extends AbstractSecureAction implements	ServletRequestAware {
	private static final long serialVersionUID = -5680048437344503094L;

	private static Logger logger = Logger.getLogger(DirecpayRefundResponseAction.class.getName());

	private HttpServletRequest httpRequest;
	private String transactionId;
	private String orderId;
	private String payId;

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(), ((String[]) entry.getValue())[0].trim());

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			String responseString = responseMap.get(com.kbn.timesofmoney.Constants.RESPONSE_PARAMS);
			logger.info("Response message from direcpay: " + responseString);
			Fields fields =(Fields) sessionMap.get(Constants.FIELDS.getValue());
			transactionId = fields.get(FieldType.ORIG_TXN_ID.getName());
			payId= fields.get(FieldType.PAY_ID.getName());
			orderId = fields.get(FieldType.ORDER_ID.getName());
			responseMap.clear();

			ResponseFactory.parseRefundResponse(responseString ,fields);

			fields.logAllFields("Response fields from direcpay ");
			try{
				Processor processor = new ResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);
			} catch (SystemException systemException){
				logger.error("Exception", systemException);//TODO handle exception
			}
			// Sending Email for Transaction Status to merchant
			EmailBuilder emailBuilder = new EmailBuilder();
			emailBuilder.transactionEmailer(fields,UserType.MERCHANT.toString());	

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}
}
