package com.kbn.pg.action;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.json.simple.JSONObject;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.gpay.GPAYTransformer;
import com.kbn.gpay.GpayResponseProcessor;
import com.kbn.gpay.GpayTransactionService;
import com.kbn.gpay.TransactionCommunicator;
import com.kbn.gpay.TransactionConverter;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Processor;

public class VirefiyGpayUpiResponse extends AbstractSecureAction implements ServletRequestAware, ServletResponseAware {
	private static Logger logger = Logger.getLogger(VirefiyGpayUpiResponse.class.getName());
	/**
	 * @ neeraj
	 */
	private static final long serialVersionUID = -3812859523254750431L;
	private HttpServletRequest httpRequest;
	private GpayTransactionService gpayTransactionService = new GpayTransactionService();
	HttpServletResponse response;
	TransactionConverter converter = new TransactionConverter();
	TransactionCommunicator communicator = new TransactionCommunicator();

	@SuppressWarnings("unchecked")
	public String checkApiCallToGpay() throws SystemException {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> requestMap = new HashMap<String, String>();

			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					requestMap.put(entry.getKey(), ((String[]) entry.getValue())[0]);

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}

			logger.info("Response Json Data:" + requestMap);
			String Txn_Id = requestMap.get("pgRefNum");
			String RETURN_URL = requestMap.get("RETURN_URL");
			logger.info("Txn Id :" + Txn_Id);
			Map<String, String> field = gpayTransactionService.fetchOrderID(Txn_Id);
			logger.info("Session fields value1 :" + field.get(FieldType.PAY_ID.getName()));
			Fields fields = new Fields(field);

			// Status API
			String getGpayStatus = converter.createGpayStatus(fields);
			logger.info("Gpay Status Verfiy Create request :" + getGpayStatus);
			// Code to generate accessToken
			String accessToken1 = createAccessToken();
			logger.info("Gpay accessToken:" + accessToken1);
			String response1 = null;

			try {
				response1 = TransactionCommunicator.getTransactionDetails(getGpayStatus, accessToken1);
				GPAYTransformer gpayTransformer = new GPAYTransformer(response1);
				gpayTransformer.gpayUpdate(fields);
				fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));
				fields.put(FieldType.AMOUNT.getName(), Amount.formatAmount(fields.get(FieldType.AMOUNT.getName()),fields.get(FieldType.CURRENCY_CODE.getName())));
				try {
					Processor processor = new GpayResponseProcessor();
					processor.preProcess(fields);
					processor.process(fields);

					// Fetch user for Re-try Transaction ,Emailer and Sms Processor

					UserDao userDao = new UserDao();
					User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
					// Retry Transaction Block Start
					if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())) {
						RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
						if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
							addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());
							return "paymentPage";
						}
					}

					// Retry Transaction Block End

					// Sending Email for Transaction Status to merchant
					fields.put(FieldType.PAY_ID.getName(), fields.get(FieldType.PAY_ID.getName()));
					fields.put(FieldType.ACQUIRER_TYPE.getName(), fields.get(FieldType.ACQUIRER_TYPE.getName()));

					String countryCode = (String) sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
					if (countryCode == null) {
						countryCode = "NA";
					}
	
					processor.postProcess(fields);
				} catch (SystemException systemException) {
					// TODO....update fields as error??
					logger.error("Exception", systemException);
				}

			} catch (IOException exception) {
				exception.printStackTrace();
				logger.error("GPAY UPI Response:" + exception);
			}
			//
			Map<String, Object> resMap = new HashMap<String, Object>();
			resMap.put("RESPONSE_CODE", fields.get(FieldType.RESPONSE_CODE.getName()));
			resMap.put("STATUS", fields.get(FieldType.STATUS.getName()));
			resMap.put("RESPONSE_MESSAGE", fields.get(FieldType.RESPONSE_MESSAGE.getName()));
			resMap.put("RETURN_URL", fields.get(FieldType.RETURN_URL.getName()));
			resMap.put("TXNTYPE", fields.get(FieldType.TXNTYPE.getName()));
			resMap.put("TXN_ID", fields.get(FieldType.TXN_ID.getName()));
			resMap.put("INTERNAL_ACQUIRER_TYPE", fields.get(FieldType.ACQUIRER_TYPE.getName()));
			resMap.put("RESPONSE_DATE_TIME", fields.get(FieldType.RESPONSE_DATE_TIME.getName()));
			resMap.put("CUST_EMAIL", fields.get(FieldType.CUST_EMAIL.getName()));
			resMap.put("CURRENCY_CODE", fields.get(FieldType.CURRENCY_CODE.getName()));
			resMap.put("AMOUNT", fields.get(FieldType.AMOUNT.getName()));
			resMap.put("PAYMENT_TYPE", fields.get(FieldType.PAYMENT_TYPE.getName()));
			resMap.put("PAY_ID", fields.get(FieldType.PAY_ID.getName()));
			resMap.put("ORDER_ID", fields.get(FieldType.ORDER_ID.getName()));
			resMap.put("CUST_NAME", fields.get(FieldType.CUST_NAME.getName()));
			resMap.put("RRN", fields.get(FieldType.RRN.getName()));
			resMap.put("RETURN_URL", RETURN_URL);

			Map<String, Object> resMap1 = new HashMap<String, Object>();

			resMap1.put("responseCode", fields.get(FieldType.RESPONSE_CODE.getName()));
			resMap1.put("transactionStatus", fields.get(FieldType.STATUS.getName()));
			resMap1.put("txnType", fields.get(FieldType.TXNTYPE.getName()));
			resMap1.put("pgRefNum", fields.get(FieldType.TXN_ID.getName()));
			resMap1.put("RETURN_URL", RETURN_URL);
			resMap1.put("RESPONSE_DATE_TIME", fields.get(FieldType.RESPONSE_DATE_TIME.getName()));
			resMap1.put("CUST_NAME", fields.get(FieldType.CUST_NAME.getName()));
			resMap1.put("CUST_EMAIL", fields.get(FieldType.CUST_EMAIL.getName()));
			resMap1.put("responseMessage", fields.get(FieldType.RESPONSE_MESSAGE.getName()));
			resMap1.put("responseFields", resMap);

			JSONObject jObject = new JSONObject(resMap1);

			response.setContentType("application/json");
			response.getWriter().flush();
			response.getWriter().println(jObject);
			response.getWriter().close();

		} catch (Exception exception) {
			logger.error("Exception", exception);
			exception.printStackTrace();
		}

		return SUCCESS;
	}

	@Override
	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

	// token
	public static String createAccessToken() {
		FileInputStream serviceAccount = null;
		try {
			String keyPath = new PropertiesManager().getSystemProperty("GpayUpiPath");
			serviceAccount = new FileInputStream(keyPath);// Live
			//serviceAccount = new FileInputStream("D://GPAY//BpayGpay-7702a95e1a11.json");// local
			// Authenticate a Google credential with the service account
			logger.info("Gpay path:"+serviceAccount);
			GoogleCredential googleCred = GoogleCredential.fromStream(serviceAccount);

			// Add the required scopes to the Google credential
			GoogleCredential scoped = googleCred.createScoped(Arrays.asList("https://www.googleapis.com/auth/nbupaymentsmerchants"));

			// Use the Google credential to generate an access token
			scoped.refreshToken();
			String token = scoped.getAccessToken();

			return token;

		} catch (FileNotFoundException exception) {
			exception.printStackTrace();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		return null;
	}

}
