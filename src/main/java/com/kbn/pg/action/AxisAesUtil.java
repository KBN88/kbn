package com.kbn.pg.action;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
public class AxisAesUtil {
	
	public static String decrypt(String encryptedData, String keyStr) throws Exception {
		Key key = generateKey1(keyStr);
		Cipher c = Cipher.getInstance("AES");
		c.init(2, key);
		byte[] decordedValue = java.util.Base64.getDecoder().decode(encryptedData);
		byte[] decValue = c.doFinal(decordedValue);
		return new String(decValue);
	}
	
	private static Key generateKey1(String key) throws Exception {
		byte[] keyValue = key.getBytes();
		return new SecretKeySpec(keyValue, "AES");
	}

}
