package com.kbn.pg.action.service;

import java.util.Map;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;

public interface ActionService {

	public Fields prepareFields(Map<String,Object> map) throws SystemException;
	
}
