package com.kbn.pg.action.service;

import org.apache.struts2.dispatcher.SessionMap;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;

public class RetryTransactionProcessor {

	public boolean retryTransaction(Fields responseMap, SessionMap<String, Object> sessionMap, User user) throws SystemException {
		//handling fraud txns
		if(routeFraudTransactions(responseMap)){
			return false;
		}
		RetryTransaction retryTransaction = new RetryTransaction();
		retryTransaction.retryPayment(responseMap, sessionMap, user);
		if (retryTransaction.isTransactionFailFlag() == true) {
			return true;
		}
		return false;
	}
	
	//to handle txn failed due by fraud prevention system
	private boolean routeFraudTransactions(Fields fields)throws SystemException{
		if(fields.get(FieldType.STATUS.getName()).equals(StatusType.DENIED_BY_FRAUD.getName())){
			return true;
		}
		return false;
	}
	
	
	public boolean retryCardTransaction(Fields responseMap, SessionMap<String, Object> sessionMap, User user) throws SystemException {
		//handling fraud txns
		if(routeFraudTransactions(responseMap)){
			return false;
		}
		RetryTransaction retryTransaction = new RetryTransaction();
		retryTransaction.retryCardPayment(responseMap, sessionMap, user);
		if (retryTransaction.isTransactionFailFlag() == true) {
			return true;
		}
		return false;
	}
}
