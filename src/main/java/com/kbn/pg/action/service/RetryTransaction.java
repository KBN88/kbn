package com.kbn.pg.action.service;

import org.apache.log4j.Logger;
import org.apache.struts2.dispatcher.SessionMap;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;

/**
 * @author Neeraj
 *
 */
public class RetryTransaction {

	private Integer count;
	private boolean TransactionFailFlag;
	private static Logger logger = Logger.getLogger(RetryTransaction.class
			.getName());

	public void retryPayment(Fields responseMap,
			SessionMap<String, Object> sessionMap, User user) {
		try {
			String respCode = responseMap
					.get(FieldType.RESPONSE_CODE.getName());
			if (user.isRetryTransactionCustomeFlag()
					&& !respCode.equals(ErrorType.SUCCESS.getCode())) {
				Object obj = user.getAttemptTrasacation();
				if (null == obj) {
					setTransactionFailFlag(false);
					return;
				}
				Integer counter = Integer
						.parseInt(user.getAttemptTrasacation());
				Integer attemptCounter = (Integer) sessionMap
						.get(Constants.COUNT.getValue());
				if (null == attemptCounter) {
					count = 1;
					sessionMap.put(Constants.COUNT.getValue(), count);
					setTransactionFailFlag(true);
				} else if (attemptCounter < counter) {
					attemptCounter = attemptCounter + 1;
					sessionMap.put(Constants.COUNT.getValue(), attemptCounter);
					setTransactionFailFlag(true);

				}
				// Call method for remove secureField
				removeFields(responseMap, sessionMap);
			}
		} catch (Exception exception) {
			logger.error("Exception in retry payment: ", exception);

		}

	}

	// remove secureField
	public void removeFields(Fields responseMap,
			SessionMap<String, Object> sessionMap) {
		responseMap.remove(FieldType.CARD_EXP_DT.getName());
		responseMap.remove(FieldType.CVV.getName());
		responseMap.remove(FieldType.CARD_MASK.getName());
		responseMap.remove(FieldType.MOP_TYPE.getName());
		responseMap.remove(FieldType.ACQUIRER_TYPE.getName());
		responseMap.remove(FieldType.RESPONSE_CODE.getName());
		responseMap.put((FieldType.CARD_MASK.getName()),
				responseMap.get(FieldType.CARD_MASK.getName()));
		responseMap.put((FieldType.CARD_EXP_DT.getName()),
				responseMap.get(FieldType.CARD_EXP_DT.getName()));
		responseMap.put((FieldType.CVV.getName()),
				responseMap.get(FieldType.CVV.getName()));
		responseMap.put((FieldType.RESPONSE_CODE.getName()),
				responseMap.get(FieldType.RESPONSE_CODE.getName()));
		responseMap.put((FieldType.ACQUIRER_TYPE.getName()),
				responseMap.get(FieldType.ACQUIRER_TYPE.getName()));
		Object previousFields = sessionMap.get(Constants.FIELDS.getValue());
		Fields sessionFields = null;
		if (null != previousFields) {
			sessionFields = (Fields) previousFields;
		} else {
		}
		sessionFields.put(responseMap);
	}

	public boolean isTransactionFailFlag() {
		return TransactionFailFlag;
	}

	public void setTransactionFailFlag(boolean TransactionFailFlag) {
		this.TransactionFailFlag = TransactionFailFlag;
	}

	
	public void retryCardPayment(Fields responseMap,
			SessionMap<String, Object> sessionMap, User user) {
		try {
			String respCode = responseMap.get(FieldType.RESPONSE_CODE.getName());
			if (respCode.equals(ErrorType.SUCCESS.getCode())) {
				Object obj = user.getAttemptTrasacation();
				if (null == obj) {
					setTransactionFailFlag(false);
					return;
				}
				Integer counter = Integer
						.parseInt(user.getAttemptTrasacation());
				Integer attemptCounter = (Integer) sessionMap
						.get(Constants.COUNT.getValue());
				if (null == attemptCounter) {
					count = 1;
					sessionMap.put(Constants.COUNT.getValue(), count);
					setTransactionFailFlag(true);
				} else if (attemptCounter < counter) {
					attemptCounter = attemptCounter + 1;
					sessionMap.put(Constants.COUNT.getValue(), attemptCounter);
					setTransactionFailFlag(true);

				}
				// Call method for remove secureField
				removeFields(responseMap, sessionMap);
			}
		} catch (Exception exception) {
			logger.error("Exception in retry payment: ", exception);

		}
	}
}
