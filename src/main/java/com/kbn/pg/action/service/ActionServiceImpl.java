package com.kbn.pg.action.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.Currency;
import com.kbn.shopify.ShopifyFieldType;
import com.kbn.shopify.ShopifyUtil;

/**
 * @author Puneet
 *
 */
public class ActionServiceImpl implements ActionService {
	private static Logger logger = Logger.getLogger(ActionServiceImpl.class
			.getName());
	public static final String SHOPIFYREQUEST = "x_";

	@Override
	public Fields prepareFields(Map<String, Object> map) throws SystemException {

		Map<String, String> requestMap = new HashMap<String, String>();
		Map<String, String> shopifyRequestMap = new HashMap<String, String>();
		Map<String, String> hashMap = new HashMap<String, String>();
		Fields fields = null;

		boolean shopifyRequest = false;
		String custName="";
		String firstName="";
		String lastName="";
		try {
			for (Entry<String, Object> entry : map.entrySet()) {
				if (entry.getKey().startsWith(SHOPIFYREQUEST)) {
					hashMap.put(entry.getKey(),
							((String[]) entry.getValue())[0]);
					
					logger.info("Input fields  " + entry.getKey() +"  " +((String[]) entry.getValue())[0]);
					
					ShopifyFieldType shopifyFieldType = ShopifyFieldType
							.getInstance(entry.getKey());
					if(shopifyFieldType==null){
						continue;
					}
					if(entry.getKey().equals("x_amount")){
						shopifyRequestMap.put(shopifyFieldType.getPgName(),Amount.formatAmount((((String[]) entry.getValue())[0]),"356"));
					}else if(entry.getKey().equals("x_currency")){
						shopifyRequestMap.put(shopifyFieldType.getPgName(),Currency.getNumericCode(((String[]) entry.getValue())[0]));
					}else if(entry.getKey().equals(ShopifyFieldType.CUST_FIRST_NAME.getRequestName())){
						firstName = ((String[]) entry.getValue())[0];
					}else if(entry.getKey().equals(ShopifyFieldType.CUST_LAST_NAME.getRequestName())){
						lastName = ((String[]) entry.getValue())[0];
					}else{
					shopifyRequestMap.put(shopifyFieldType.getPgName(),
							((String[]) entry.getValue())[0]);
					}
					shopifyRequest = true;
				} else {
					requestMap.put(entry.getKey(),
							((String[]) entry.getValue())[0]);
				}
			}
		} catch (ClassCastException classCastException) {
			logger.error("Exception while preparing fields", classCastException);
		}

		if (shopifyRequest) {
			if(!(firstName.equals(""))){
				custName = firstName;
				if(!(lastName.equals(""))){
					custName = custName + lastName;
				}
				shopifyRequestMap.put(FieldType.CUST_NAME.getName(),custName);
			}
			
			ShopifyUtil.matchHash(hashMap);
			fields = new Fields(shopifyRequestMap);
			fields.removeInternalFields();

			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(), "Y");
			fields.remove(FieldType.HASH.getName());
			fields.put(FieldType.HASH.getName(), Hasher.getHash(fields));
		}else{
			fields = new Fields(requestMap);
			fields.removeInternalFields();
		}

		return fields;
	}
}
