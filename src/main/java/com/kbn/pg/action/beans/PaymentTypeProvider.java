package com.kbn.pg.action.beans;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.kbn.commons.user.ChargingDetails;
import com.kbn.commons.user.ChargingDetailsDao;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.TransactionType;

/**
 * @author Neeraj
 *
 */
public class PaymentTypeProvider {

	public PaymentTypeProvider(String payId){
		setSupportedPaymentOptions(payId);
	}

	private Map<String,Object> supportedCardTypeMap= new TreeMap<String,Object>();
	private Map<String,Object> supportedPaymentTypeMap= new TreeMap<String,Object>();
	private List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();	

	public void setSupportedPaymentOptions(String payId){
		List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
		chargingDetailsList = new ChargingDetailsDao().getAllActiveChargingDetails(payId);
		this.chargingDetailsList = chargingDetailsList;

		Set<MopType> mopListCC= new HashSet<MopType>();		
		Set<MopType> mopListDC= new HashSet<MopType>();
		TreeSet<MopType> mopListNB=new TreeSet<MopType>(new BankName());
		Set<MopType> mopListWL= new HashSet<MopType>();
		Set<MopType> mopListCards= new HashSet<MopType>();

		for(ChargingDetails chargingDetails:chargingDetailsList){
			//to exclude refund entries
			if(chargingDetails.getTransactionType()!=null && chargingDetails.getTransactionType().getCode().equals(TransactionType.REFUND.getCode())){
				continue;
			}
			PaymentType paymentType = chargingDetails.getPaymentType();
			switch(paymentType){
			case CREDIT_CARD:
				mopListCC.add(chargingDetails.getMopType());
				mopListCards.add(chargingDetails.getMopType());
				break;
			case DEBIT_CARD:
				mopListDC.add(chargingDetails.getMopType());
				mopListCards.add(chargingDetails.getMopType());
				break;
			case NET_BANKING:
				mopListNB.add(chargingDetails.getMopType());
				break;
			case WALLET:
				mopListWL.add(chargingDetails.getMopType());
				break;
			default:
				break;
			}
		}
		Map<String, Object> treeMap = new TreeMap<String, Object>();
		if(mopListCC.size()!=0){
			treeMap.put(PaymentType.CREDIT_CARD.getCode(), mopListCC);			
		}
		if(mopListDC.size()!=0){
			treeMap.put(PaymentType.DEBIT_CARD.getCode(), mopListDC);
		}
		if(mopListNB.size()!=0){
			treeMap.put(PaymentType.NET_BANKING.getCode(), mopListNB);
		}
		if(mopListWL.size()!=0){
			treeMap.put(PaymentType.WALLET.getCode(), mopListWL);
		}
		if(mopListCards.size()!=0){
			supportedCardTypeMap.put(PaymentType.CREDIT_CARD.getCode(), mopListCards);
		}
		setSupportedPaymentTypeMap(treeMap); 
	}

	public Map<String,Object> getSupportedPaymentTypeMap() {
		return supportedPaymentTypeMap;
	}

	public void setSupportedPaymentTypeMap(Map<String,Object> supportedPaymentTypeMap) {
		this.supportedPaymentTypeMap = supportedPaymentTypeMap;
	}

	public Map<String, Object> getSupportedCardTypeMap() {
		return supportedCardTypeMap;
	}

	public void setSupportedCardTypeMap(Map<String, Object> supportedCardTypeMap) {
		this.supportedCardTypeMap = supportedCardTypeMap;
	}

	public List<ChargingDetails> getChargingDetailsList() {
		return chargingDetailsList;
	}

	public void setChargingDetailsList(List<ChargingDetails> chargingDetailsList) {
		this.chargingDetailsList = chargingDetailsList;
	}
}
/*
public Map<String, Object> getPaymentBankType(String payId){

	List<ChargingDetails> chargingDetailsList = new ArrayList<ChargingDetails>();
	chargingDetailsList = new ChargingDetailsDao().getAllActiveChargingDetails(payId);

	Set<MopType> mopListCC= new HashSet<MopType>();		
	Set<MopType> mopListDC= new HashSet<MopType>();
	TreeSet<MopType> mopListNB=new TreeSet<MopType>(new BankName());
	Set<MopType> mopListWL= new HashSet<MopType>();
	Set<MopType> mopListCards= new HashSet<MopType>();

	for(ChargingDetails chargingDetails:chargingDetailsList){
		//to exclude refund entries
		if(chargingDetails.getTransactionType()!=null && chargingDetails.getTransactionType().getCode().equals(TransactionType.REFUND.getCode())){
			continue;
		}
		PaymentType paymentType = chargingDetails.getPaymentType();
		switch(paymentType){
		case CREDIT_CARD:
			mopListCC.add(chargingDetails.getMopType());
			mopListCards.add(chargingDetails.getMopType());
			break;
		case DEBIT_CARD:
			mopListDC.add(chargingDetails.getMopType());
			mopListCards.add(chargingDetails.getMopType());
			break;
		case NET_BANKING:
			mopListNB.add(chargingDetails.getMopType());
			break;
		case WALLET:
			mopListWL.add(chargingDetails.getMopType());
			break;
		default:
			break;
		}
	}
	Map<String, Object> treeMap = new TreeMap<String, Object>();
	if(mopListCC.size()!=0){
		treeMap.put(PaymentType.CREDIT_CARD.getCode(), mopListCC);			
	}
	if(mopListDC.size()!=0){
		treeMap.put(PaymentType.DEBIT_CARD.getCode(), mopListDC);
	}
	if(mopListNB.size()!=0){
		treeMap.put(PaymentType.NET_BANKING.getCode(), mopListNB);
	}
	if(mopListWL.size()!=0){
		treeMap.put(PaymentType.WALLET.getCode(), mopListWL);
	}
	if(mopListCards.size()!=0){
		supportedCardTypeMap.put(PaymentType.CREDIT_CARD.getCode(), mopListCards);
	}
	return  treeMap;
}
*/
class BankName implements Comparator<MopType>{ 
	@Override
	public int compare(MopType bank1, MopType bank2) {
		return bank1.getName().compareTo(bank2.getName());
	}
}  

