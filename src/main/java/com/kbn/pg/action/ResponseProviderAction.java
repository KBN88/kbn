package com.kbn.pg.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author Sunil
 *
 */
public class ResponseProviderAction extends ActionSupport  implements ServletRequestAware, SessionAware {

	private static final long serialVersionUID = 7490654831743750216L;


	private HttpServletRequest httpRequest ;
	private Map<String, Object> sessionMap;
	
	public HttpServletRequest getHttpRequest() {
		return httpRequest;
	}

	public void setHttpRequest(HttpServletRequest httpRequest) {
		this.httpRequest = httpRequest;
	}

	public void setSession(Map<String, Object> sessionMap) {
		this.setSessionMap(sessionMap);
		
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq ;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
