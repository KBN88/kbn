package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.CrmValidator;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.crm.actionBeans.RefundChecker;
import com.kbn.crm.actionBeans.RefundLimitUpdater;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailBodyCreator;
import com.kbn.emailer.EmailBuilder;
import com.kbn.emailer.Emailer;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.RequestRouter;
import com.kbn.sms.SmsSender;

/**
 * @author Chandan
 *
 */
public class RefundRequest extends AbstractSecureAction {

	private static Logger logger = Logger.getLogger(RefundRequest.class.getName());
	private static final long serialVersionUID = -1955264811888217325L;
	private HttpServletRequest request;
	private String origTxnId;
	private String amount;
	private String payId;
	private String currencyCode;
	private String response;
	private String orderId;
	private String custEmail;
	private boolean refundFlag;
	private boolean emailExceptionHandlerFlag;

	public String execute() {
		try {

			HttpServletRequest request = ServletActionContext.getRequest();
			;
			User sessionUser = (User) sessionMap.get(Constants.USER.getValue());
			Fields responseMap = null;
			Map<String, String> requestMap = new HashMap<String, String>();
			// format amount first
			requestMap.put(FieldType.AMOUNT.getName(), Amount.formatAmount(getAmount(), getCurrencyCode()));
			requestMap.put(FieldType.PAY_ID.getName(), getPayId());
			UserDao userDao = new UserDao();
			User user = userDao.findPayId(payId);
			// Check Refund Amount
			RefundChecker refundChecker = new RefundChecker();
			refundFlag = refundChecker.setAllRefundValidation(currencyCode, requestMap, user, getOrigTxnId());
			if (refundFlag == true) {
				setResponse(ErrorType.REFUND_FAILED.getResponseMessage() + getOrderId());
				return SUCCESS;
			}
			requestMap.put(FieldType.ORIG_TXN_ID.getName(), getOrigTxnId());
			requestMap.put(FieldType.PAY_ID.getName(), getPayId());
			requestMap.put(FieldType.TXNTYPE.getName(), TransactionType.REFUND.getName());
			requestMap.put(FieldType.CURRENCY_CODE.getName(), currencyCode);
			requestMap.put(FieldType.CUST_EMAIL.getName(), getCustEmail());
			requestMap.put(FieldType.INTERNAL_USER_EMAIL.getName(), sessionUser.getEmailId());
			requestMap.put(FieldType.HASH.getName(),
					"1234567890123456789012345678901234567890123456789012345678901234");
			requestMap.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(), "N");
			// Preparing fields
			Fields fields = new Fields(requestMap);
			fields.put((FieldType.INTERNAL_CUST_IP.getName()), request.getRemoteAddr());
			fields.logAllFields("All request fields :");
			RequestRouter router = new RequestRouter(fields);
			responseMap = new Fields(router.route());

			String responseCode = responseMap.get(FieldType.RESPONSE_CODE.getName());
			// send SMS
			SmsSender.sendSMS(fields);

			// Sending Email for Transaction Status to merchant or customer
			if (responseCode.equals("000")) {
				if (user.isRefundTransactionCustomerEmailFlag()) {
					EmailBuilder emailBuilder = new EmailBuilder();
					emailBuilder.transactionRefundEmail(responseMap, CrmFieldConstants.CUSTOMER.toString(),
							fields.get(FieldType.CUST_EMAIL.getName()), user.getBusinessName());
				}
				if (user.isRefundTransactionMerchantEmailFlag()) {
					String transactionEmailIdString = user.getTransactionEmailId();
					String defaultMerchatEmail = user.getEmailId();
					EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
					String heading = CrmFieldConstants.MERCHANT_HEADING.getValue();
					String message = user.getBusinessName();
					String body = emailBodyCreator.bodyTransactionEmail(responseMap, heading, message);
					Emailer.sendEmail(body, message, defaultMerchatEmail, transactionEmailIdString,isEmailExceptionHandlerFlag());
				}
			}
			if (null == responseCode || !responseCode.equals(ErrorType.SUCCESS.getCode())) {
				setResponse(ErrorType.REFUND_NOT_SUCCESSFULL.getResponseMessage() + getOrderId());
				return SUCCESS;
			}
			// Refund Limit Update
			RefundLimitUpdater refundLimitUpdater = new RefundLimitUpdater();
			refundLimitUpdater.extraRefundLimitUpdate(currencyCode, requestMap, user, getOrigTxnId(), getPayId(),
					refundChecker.getTodayRefundedAmount(), refundChecker.getTodayTotalCapturedAmount());
			setResponse(ErrorType.REFUND_SUCCESSFULL.getResponseMessage() + getOrderId());
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			setResponse(ErrorType.REFUND_NOT_SUCCESSFULL.getResponseMessage() + getOrderId());
			return SUCCESS;
		}
	}

	public void validate() {
		CrmValidator validator = new CrmValidator();

		if (validator.validateBlankField(getOrigTxnId())) {
		} else if (!validator.validateField(CrmFieldType.TRANSACTION_ID, getOrigTxnId())) {
			addFieldError(CrmFieldConstants.ORIG_TXN_ID.getValue(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getAmount())) {
		} else if (!validator.validateField(CrmFieldType.AMOUNT, getAmount())) {
			addFieldError(CrmFieldType.CURRENCY.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getPayId())) {
		} else if (!validator.validateField(CrmFieldType.PAY_ID, getPayId())) {
			addFieldError(CrmFieldType.PAY_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getOrderId())) {
		} else if (!validator.validateField(CrmFieldType.ORDER_ID, getOrderId())) {
			addFieldError(CrmFieldType.ORDER_ID.getName(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getResponse())) {
		} else if (!validator.validateField(CrmFieldType.SUCCESS_MESSAGE, getResponse())) {
			addFieldError(CrmFieldConstants.RESPONSE.getValue(), ErrorType.INVALID_FIELD.getResponseMessage());
		}

		if (validator.validateBlankField(getCurrencyCode())) {
		} else if (!validator.validateField(CrmFieldType.CURRENCY, getCurrencyCode())) {
			addFieldError(CrmFieldConstants.CURRENCY_CODE.getValue(), ErrorType.INVALID_FIELD.getResponseMessage());
		}
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getOrigTxnId() {
		return origTxnId;
	}

	public void setOrigTxnId(String origTxnId) {
		this.origTxnId = origTxnId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCustEmail() {
		return custEmail;
	}

	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public boolean isEmailExceptionHandlerFlag() {
		return emailExceptionHandlerFlag;
	}

	public void setEmailExceptionHandlerFlag(boolean emailExceptionHandlerFlag) {
		this.emailExceptionHandlerFlag = emailExceptionHandlerFlag;
	}

}
