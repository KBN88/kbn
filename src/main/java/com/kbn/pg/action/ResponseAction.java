package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.RequestRouter;
import com.kbn.pg.core.ResponseCreator;
import com.opensymphony.xwork2.Action;

/**
 * @author Sunil, Neeraj
 *
 */
public class ResponseAction extends AbstractSecureAction implements
		ServletRequestAware {

	private static Logger logger = Logger.getLogger(ResponseAction.class
			.getName());
	private static final long serialVersionUID = 2943629615913246334L;

	private Fields responseMap = null;
	private HttpServletRequest httpRequest;
	private String redirectUrl;
	private Integer count;

	public ResponseAction() {
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> requestMap = new HashMap<String, String>();

			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					requestMap.put(entry.getKey(),
							((String[]) entry.getValue())[0]);

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}

			String paRes = requestMap.get("PaRes");
			String md = requestMap.get("MD");

			Fields fields = new Fields();
			fields.put(FieldType.MD.getName(), md);
			fields.put(FieldType.PARES.getName(), paRes);

			fields.logAllFields("3DS Recieved Map :");

			Object fieldsObj = sessionMap.get("FIELDS");

			if (null != fieldsObj) {
				fields.put((Fields) fieldsObj);
			}

			fields.logAllFields("Updated 3DS Recieved Map :");
			fields.put(FieldType.ACQUIRER_TYPE.getName(),
					(String) sessionMap.get(FieldType.ACQUIRER_TYPE.getName()));
			fields.put(FieldType.TXNTYPE.getName(), (String) sessionMap
					.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName()));
			fields.put(FieldType.INTERNAL_VALIDATE_HASH_YN.getName(), "N");
			fields.put((FieldType.INTERNAL_CARD_ISSUER_BANK.getName()),
					(String) sessionMap.get(FieldType.INTERNAL_CARD_ISSUER_BANK
							.getName()));
			fields.put((FieldType.INTERNAL_CARD_ISSUER_COUNTRY.getName()),
					(String) sessionMap
							.get(FieldType.INTERNAL_CARD_ISSUER_COUNTRY
									.getName()));
			RequestRouter router = new RequestRouter(fields);
			responseMap = new Fields(router.route());
			
			// Fetch user for retryTransaction ,SendEmailer and  SmsSenser
			UserDao userDao = new UserDao();
			User user = userDao.getUserClass(responseMap.get(FieldType.PAY_ID
					.getName()));
			
			// Retry Transaction Block Start
			if (!responseMap.get(FieldType.RESPONSE_CODE.getName()).equals(
					ErrorType.SUCCESS.getCode())) {
				RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
				if (retryTransactionProcessor.retryTransaction(responseMap,
						sessionMap, user)) {
					addActionMessage(CrmFieldConstants.RETRY_TRANSACTION
							.getValue());
					return "paymentPage";
				}

			}
			// Retry Transaction Block End

			Object previousFields = sessionMap.get(Constants.FIELDS.getValue());
			Fields sessionFields = null;
			if (null != previousFields) {
				sessionFields = (Fields) previousFields;
			} else {
				// TODO: Handle
			}
			
			// Sending Email for Transaction Status to merchant
			
			String countryCode = (String) sessionMap
					.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
			
			responseMap.put(countryCode,(String) sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName()));
			EmailSender emailSender = new EmailSender();
			emailSender.postMan(responseMap, countryCode, user);
			
			sessionFields.put(responseMap);
			fields.put(FieldType.RETURN_URL.getName(),
					(String) sessionMap.get(FieldType.RETURN_URL.getName()));
			responseMap.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN
							.getName()));
			if (sessionMap != null) {
				sessionMap.put(Constants.TRANSACTION_COMPLETE_FLAG.getValue(),
						Constants.Y_FLAG.getValue());
				sessionMap.invalidate();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(responseMap);

		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return Action.NONE;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
