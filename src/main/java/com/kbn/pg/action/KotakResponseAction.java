package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.commons.crypto.Hasher;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.netbanking.kotak.KotakTransformer;
import com.kbn.netbanking.kotak.KotakUtil;
import com.kbn.netbanking.kotak.ResponseProcessor;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;
import com.opensymphony.xwork2.Action;

public class KotakResponseAction extends AbstractSecureAction implements
		ServletRequestAware {

	private static Logger logger = Logger.getLogger(KotakResponseAction.class
			.getName());

	private static final long serialVersionUID = 4569351699981579168L;
	private HttpServletRequest httpRequest;

	@SuppressWarnings("unchecked")
	public String execute() {
		try {

			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();

			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(),
							((String[]) entry.getValue())[0].trim());
				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}

			Fields fields = (Fields) sessionMap
					.get(Constants.FIELDS.getValue());
			logger.info("Message recieved from kotak  " + responseMap);

			logger.info("Session fields value"
					+ fields.get(FieldType.PAY_ID.getName()));
			logger.info("Session fields value"
					+ fields.get(FieldType.TXN_ID.getName()));
			fields.logAllFields("session fields for kotak");
			String password = "";
			if (null != fields) {
				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);
				password = fields.get(FieldType.PASSWORD.getName());
			}

			String responseString = responseMap.get("msg");
			logger.info("Message recieved from kotak  " + responseString);
			responseMap.clear();

			KotakUtil kotakutil = new KotakUtil();
			kotakutil.compareResponseHash(fields, responseString);

			KotakTransformer transformer = new KotakTransformer(responseString);
			transformer.updateKotakResponse(fields);

			try {
				Processor processor = new ResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);
				
				// Fetch user for retryTransaction ,SendEmailer and SmsSenser

				UserDao userDao = new UserDao();
				User user = userDao.getUserClass(fields.get(FieldType.PAY_ID
						.getName()));
				
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(
						ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields,
							sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION
								.getValue());
						return "paymentPage";
					}
				}
				// Retry Transaction Block End

				// Sending Email for Transaction Status to merchant
				String countryCode = (String) sessionMap
						.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);
				
				fields.logAllFields("Kotak Recieved Map :");
				fields.put(FieldType.RETURN_URL.getName(),
						(String) sessionMap.get(FieldType.RETURN_URL.getName()));

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				logger.error("Exception", systemException);
				return ERROR;
			}

			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN
							.getName()));

			if (sessionMap != null) {
				sessionMap.clear();
			}
			// Temp
			fields.put(FieldType.HASH.getName(), Hasher.getHash(fields));

			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return Action.NONE;
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

}
