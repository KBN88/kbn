package com.kbn.pg.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.emailer.EmailBuilder;
import com.kbn.paytm.ResponseProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @author Sunil
 *
 */
public class PaytmResponseAction extends ActionSupport implements
		ServletRequestAware, SessionAware {

	private static Logger logger = Logger.getLogger(PaytmResponseAction.class.getName());
	private static final long serialVersionUID = 2943629615913246334L;

	private HttpServletRequest httpRequest;

	private Map<String, Object> sessionMap;

	public PaytmResponseAction() {
	}

	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;
	}

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			Map<String, String> responseMap = new HashMap<String, String>();
			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					responseMap.put(entry.getKey().trim(), ((String[]) entry.getValue())[0].trim());

				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}

			Fields fields = new Fields();
			fields.putAll(responseMap);
			
			try{
				Processor processor = new ResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);
			} catch (SystemException systemException){
				logger.error("Exception", systemException);
			}
			
			// Sending Email for Transaction Status to merchant
			fields.put(FieldType.PAY_ID.getName(), (String) sessionMap.get(FieldType.PAY_ID.getName()));
			fields.put(FieldType.ACQUIRER_TYPE.getName(), (String) sessionMap.get(FieldType.ACQUIRER_TYPE.getName()));
			EmailBuilder emailBuilder = new EmailBuilder();
			emailBuilder.transactionEmailer(fields,UserType.MERCHANT.toString());	
			
			fields.logAllFields("Paytm Recieved Map :");
			fields.put(FieldType.RETURN_URL.getName(), (String) sessionMap.get(FieldType.RETURN_URL.getName()));
			if (sessionMap != null) {
				sessionMap.clear();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return Action.NONE;
	}

	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
