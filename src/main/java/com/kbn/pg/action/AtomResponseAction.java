package com.kbn.pg.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.kbn.atom.AtomAES;
import com.kbn.atom.AtomResponseProcessor;
import com.kbn.atom.AtomTransformer;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.emailer.EmailSender;
import com.kbn.pg.action.service.RetryTransactionProcessor;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.ResponseCreator;
import com.kbn.pg.security.SecurityProcessor;
import com.opensymphony.xwork2.Action;

public class AtomResponseAction extends AbstractSecureAction implements ServletRequestAware {

	/**
	 * @neeraj
	 */
	private static final long serialVersionUID = 8271643232079343646L;
	private static Logger logger = Logger.getLogger(AtomResponseAction.class.getName());
	private HttpServletRequest httpRequest;
	UserDao dao = new UserDao();

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Map<String, Object> fieldMapObj = httpRequest.getParameterMap();
			LinkedHashMap<String, String> hmDecryptedValue = new LinkedHashMap<String, String>();

			for (Entry<String, Object> entry : fieldMapObj.entrySet()) {
				try {
					hmDecryptedValue.put(entry.getKey().trim(), ((String[]) entry.getValue())[0].trim());
				} catch (ClassCastException classCastException) {
					logger.error("Exception", classCastException);
				}
			}
			Fields fields = (Fields) sessionMap.get(Constants.FIELDS.getValue());
			// Fetching decrption key and secure key from User table
			User user = dao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
		// decrypting Atom response
			String responseString = hmDecryptedValue.get("encdata");
			logger.info("Message recieved from ATOM  " + responseString);
			// Atom decrypt
			AtomAES decObj = new AtomAES();
			String[] atomresponse = decObj.decrypt(responseString, user.getTerminalId(), user.getTerminalId()).split("\\&");
			Map<String, String> map = new HashMap<String, String>();
			for (String entry : atomresponse) {
				String[] keyValue = entry.split("=");
				try {
					if (!keyValue[1].equals("")) {
						map.put(keyValue[0], keyValue[1]);
						System.out.println(map.toString());
					}
				} catch (Exception e) {
					map.put(keyValue[0], "");
					e.printStackTrace();
				}

			}
			logger.info("Message recieved from ATOM  " + hmDecryptedValue);
			// TODO Blob response Fields
			// fields.put(FieldType.RESPONSE_FIELDS.getName(), hmDecryptedValue.toString());
			logger.info("Session fields value" + fields.get(FieldType.PAY_ID.getName()));
			logger.info("Session fields value" + fields.get(FieldType.TXN_ID.getName()));
			fields.logAllFields("session fields for ATOM");
			if (null != fields) {
				SecurityProcessor securityProcessor = new SecurityProcessor();
				securityProcessor.authenticate(fields);
				securityProcessor.addAcquirerFields(fields);
			}

			AtomTransformer atomTransformer = new AtomTransformer(map);
			atomTransformer.updateAtomResponse(fields);
			try {
				Processor processor = new AtomResponseProcessor();
				processor.preProcess(fields);
				processor.process(fields);
				// Retry Transaction Block Start
				if (!fields.get(FieldType.RESPONSE_CODE.getName()).equals(ErrorType.SUCCESS.getCode())) {
					RetryTransactionProcessor retryTransactionProcessor = new RetryTransactionProcessor();
					if (retryTransactionProcessor.retryTransaction(fields, sessionMap, user)) {
						addActionMessage(CrmFieldConstants.RETRY_TRANSACTION.getValue());
						return "paymentPage";
					}
				}
				// Retry Transaction Block End

				// Sending Email for Transaction Status to merchant
				fields.put(FieldType.PAY_ID.getName(), fields.get(FieldType.PAY_ID.getName()));
				fields.put(FieldType.ACQUIRER_TYPE.getName(),
						(String) sessionMap.get(FieldType.ACQUIRER_TYPE.getName()));
				String countryCode = (String) sessionMap.get(FieldType.INTERNAL_CUST_COUNTRY_NAME.getName());
				EmailSender emailSender = new EmailSender();
				emailSender.postMan(fields, countryCode, user);
				fields.put(FieldType.RETURN_URL.getName(), (String) sessionMap.get(FieldType.RETURN_URL.getName()));

				processor.postProcess(fields);
			} catch (SystemException systemException) {
				logger.error("Exception", systemException);
			}
			fields.put(FieldType.INTERNAL_SHOPIFY_YN.getName(),
					(String) sessionMap.get(FieldType.INTERNAL_SHOPIFY_YN.getName()));
			if (sessionMap != null) {
				sessionMap.clear();
			}
			ResponseCreator responseCreator = new ResponseCreator();
			responseCreator.ResponsePost(fields);

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return Action.NONE;
	}

	@Override
	public void setServletRequest(HttpServletRequest hReq) {
		this.httpRequest = hReq;

	}

}
