package com.kbn.pg.recurringPayments;

import org.apache.log4j.Logger;

import com.kbn.citruspay.CitrusPayTransaction;
import com.kbn.citruspay.CitrusPayTransformer;
import com.kbn.citruspay.Response;
import com.kbn.citruspay.ResponseFactory;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.CitrusPaySubscription;
import com.kbn.commons.user.CitrusPaySubscriptionDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.TransactionManager;
import com.kbn.pg.core.Processor;
import com.kbn.pg.recurringPayments.citruspay.SubscriptionCommunicator;

/**
 * @author Puneet
 *
 */
public class RecurringPaymentProcessor implements Processor {

	private static Logger logger = Logger
			.getLogger(RecurringPaymentProcessor.class.getName());
	@Override
	public void preProcess(Fields fields) throws SystemException {

	}

	@Override
	public void process(Fields fields) throws SystemException {
		try {
			SubscriptionCommunicator communicator = new SubscriptionCommunicator();
			CitrusPaySubscriptionDao citrusPaySubscriptionDao = new CitrusPaySubscriptionDao();
			CitrusPayTransaction transaction = new CitrusPayTransaction();
			String[] merchantDetails = (new PropertiesManager().getCitrusRecurringPaymentProperty(fields
							.get(FieldType.PAY_ID.getName()))).split(",");
			String responseString1 = "{ \"merchantId\": \"mmadpay\",\"amount\": {\"value\": 1,\"currency\": \"INR\"},\"subscriberEmail\": \"puneet@mmadpay.com\",\"subscriptionId\": \"573f0653e4b0afa92be5ed20\","+
						 "\"lastPaymentDate\": \"\",\"nextBillingDate\": \"2016-05-20T12:42:59.995Z\",\"schedule\": {\"billingPeriodUnit\": 1,\"billingPeriodCycle\": \"monthly\"}}";
			
			//only for UAT demo
			if(fields.get(FieldType.CARD_NUMBER.getName()).equals("4012001037141112")){
				logger.info("subscription dummy code started");
				logger.info("Properties: " + new PropertiesManager().getCitrusRecurringPaymentProperty(fields.get(FieldType.PAY_ID.getName())));
				Response response = ResponseFactory
						.createSubscriptionResponse(responseString1);
				response.setSubscriptionId(TransactionManager.getNewTransactionId());
				CitrusPayTransformer citrusPayTransformer = new CitrusPayTransformer(
						response);
				CitrusPaySubscription citrusPaySubscription = citrusPayTransformer
						.createSubscription(fields, merchantDetails[1]);
				citrusPaySubscriptionDao.create(citrusPaySubscription);
				logger.info("subscription dummy code finished and subscription should be inserted");
				return;
			}
			// get OAuth token from properties file
			String oAuthToken = "Bearer " + merchantDetails[0];

			String vaultTokenRequest = transaction.createVaultTokenRequest(fields, merchantDetails[2]);
			String tokenResponse = communicator.communicate(fields,	vaultTokenRequest, ConfigurationConstants.CITRUSPAY_SUBSCRIPTION_VAULT_TOKEN_URL
									.getValue(), oAuthToken);
			String vaultToken = ResponseFactory.parseVaultToken(tokenResponse);

			String request = transaction.createSubscriptionRequest(fields,
					merchantDetails[1], merchantDetails[2], vaultToken);

			String subscriptionUrl = ConfigurationConstants.CITRUSPAY_CREATE_SUBSCRIPTION_URL
					.getValue();

			String responseString = communicator.communicate(fields, request,
					subscriptionUrl, oAuthToken);

			Response response = ResponseFactory
					.createSubscriptionResponse(responseString);
			CitrusPayTransformer citrusPayTransformer = new CitrusPayTransformer(
					response);
			CitrusPaySubscription citrusPaySubscription = citrusPayTransformer
					.createSubscription(fields, merchantDetails[1]);
			citrusPaySubscriptionDao.create(citrusPaySubscription);
		} catch (Exception exception) {
			fields.put(FieldType.IS_RECURRING.getName(),com.kbn.commons.util.Constants.N_FLAG.getValue());
			fields.put(FieldType.RESPONSE_MESSAGE.getName(),ErrorType.RECURRING_PAYMENT_UNSUCCESSFULL.getResponseMessage());
			logger.error("Error creating subscription" + exception);
			//TODO... alert email or alter response
		}
	}

	@Override
	public void postProcess(Fields fields) throws SystemException {

	}
}
