package com.kbn.pg.recurringPayments;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.kbn.pg.recurringPayments.citruspay.SubscriptionStatusUpdater;

public class RecurringPaymentsJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		SubscriptionStatusUpdater subscriptionStatusUpdater = new SubscriptionStatusUpdater();
		subscriptionStatusUpdater.deactivateCompletedSubscriptions();
	}
}
