package com.kbn.pg.recurringPayments.citruspay;

public class TransactionReceipt {

	private String transactionId;
	private String reason;
	private String acquirerId;
	private String acquirerAuthId;
	private String issuerAuthId;
	private String authDate;
	private String errorMessage;
	private boolean authorized;
	private boolean captured;
	private long errorCode;

	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getAcquirerId() {
		return acquirerId;
	}
	public void setAcquirerId(String acquirerId) {
		this.acquirerId = acquirerId;
	}
	public String getAcquirerAuthId() {
		return acquirerAuthId;
	}
	public void setAcquirerAuthId(String acquirerAuthId) {
		this.acquirerAuthId = acquirerAuthId;
	}
	public String getIssuerAuthId() {
		return issuerAuthId;
	}
	public void setIssuerAuthId(String issuerAuthId) {
		this.issuerAuthId = issuerAuthId;
	}
	public String getAuthDate() {
		return authDate;
	}
	public void setAuthDate(String authDate) {
		this.authDate = authDate;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public boolean isAuthorized() {
		return authorized;
	}
	public void setAuthorized(boolean authorized) {
		this.authorized = authorized;
	}
	public boolean isCaptured() {
		return captured;
	}
	public void setCaptured(boolean captured) {
		this.captured = captured;
	}
	public long getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(long errorCode) {
		this.errorCode = errorCode;
	}
}
