package com.kbn.pg.recurringPayments.citruspay;

import java.util.Map;

import com.kbn.amex.Constants;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.CitrusPaySubscription;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionManager;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Amount;

/**
 * @author Puneet
 *
 */
public class TransactionProcessor {

	@SuppressWarnings("rawtypes")
	public Fields prepareFields(CitrusPaySubscription subscription, TransactionReceipt transactionReceipt, Map amount, String subscriberEmail){
		Fields fields = new Fields();
		String txnId= TransactionManager.getNewTransactionId();
		fields.put(FieldType.TXN_ID.getName(), txnId);
	    fields.put(FieldType.ORIG_TXN_ID.getName(), txnId);
	    fields.put(FieldType.OID.getName(), subscription.getTransactionOid());
	    fields.put(FieldType.PAY_ID.getName(), subscription.getPayId());
	    fields.put(FieldType.ACQUIRER_TYPE.getName(), AcquirerType.CITRUS_PAY.getCode());
	    fields.put(FieldType.ORDER_ID.getName(), subscription.getOrderId());
	    fields.put(FieldType.CUST_NAME.getName(), subscription.getCardHolderName());
	    fields.put(FieldType.CURRENCY_CODE.getName(), subscription.getCurrency());

	    fields.put(FieldType.TXNTYPE.getName(), TransactionType.SALE.getName());
	    fields.put(FieldType.PAYMENT_TYPE.getName(), PaymentType.RECURRING_PAYMENT.getCode());
	    fields.put(FieldType.MOP_TYPE.getName(), MopType.RECURRING_AUTO_DEBIT.getCode());
	    fields.put(FieldType.AMOUNT.getName(), Amount.formatAmount(String.valueOf(amount.get("value")),subscription.getCurrency()));
	    fields.put(FieldType.CUST_EMAIL.getName(), subscriberEmail);
	    fields.put(FieldType.AUTH_CODE.getName(), transactionReceipt.getIssuerAuthId());
	    fields.put(FieldType.PG_REF_NUM.getName(), transactionReceipt.getTransactionId());
	    fields.put(FieldType.ACQ_ID.getName(), transactionReceipt.getAcquirerAuthId());
	    fields.put(FieldType.RRN.getName(), transactionReceipt.getAcquirerId());
	    fields.put(FieldType.PG_RESP_CODE.getName(), String.valueOf(transactionReceipt.getErrorCode()));
	    fields.put(FieldType.RECURRING_TRANSACTION_INTERVAL.getName(), subscription.getRecurringTransactionInterval().name());
	    fields.put(FieldType.RECURRING_TRANSACTION_COUNT.getName(), String.valueOf(subscription.getRecurringTransactionCount()));
	    fields.put(FieldType.IS_RECURRING.getName(), Constants.Y);

	    if(transactionReceipt.getErrorCode()==0 && subscription!=null){
		    fields.put(FieldType.STATUS.getName(), StatusType.CAPTURED.getName());
		    fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getResponseCode());
		    fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
		    fields.put(FieldType.PG_TXN_MESSAGE.getName(), transactionReceipt.getReason());
		}else{
		    fields.put(FieldType.STATUS.getName(), StatusType.ERROR.getName());
		    fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.ACQUIRER_ERROR.getResponseCode());
		    fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.ACQUIRER_ERROR.getResponseMessage());
		    fields.put(FieldType.PG_TXN_MESSAGE.getName(), transactionReceipt.getReason() + transactionReceipt.getErrorMessage() );   
		}
		    return fields;
	}
}
