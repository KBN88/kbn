package com.kbn.pg.recurringPayments.citruspay;

import java.util.Map;
import org.apache.log4j.Logger;

import com.kbn.crm.commons.action.AbstractSecureAction;

/**
 * @author Puneet
 *
 */
public class SubscriptionWebHook extends AbstractSecureAction{

	private static final long serialVersionUID = -6836062351929935964L;
	private static Logger logger = Logger.getLogger(SubscriptionWebHook.class.getName());
	private String merchantId;
	private String subscriptionId;
	private String invoiceId;
	private String refId;
	private String paymentStatus;
	private String subscriberEmail;
	private String method;
	private TransactionReceipt transactionReceipt;
	private TransactionReceipt refundReceipt;
	@SuppressWarnings("rawtypes")
	private Map amount;

	public String execute(){
		//TODO validate and log incoming request, prepare for refund, handle invalid request and validating merchant
		try {
			WebHookProcessor webHookProcessor = new WebHookProcessor(subscriptionId, transactionReceipt);
			webHookProcessor.preProcess();
			webHookProcessor.process(invoiceId, refId, paymentStatus,
					subscriberEmail, method, refundReceipt, amount);
		} catch (Exception exception) {
			logger.error("Error handling request in citrus webhook" + exception);
		}
		return null;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getSubscriberEmail() {
		return subscriberEmail;
	}

	public void setSubscriberEmail(String subscriberEmail) {
		this.subscriberEmail = subscriberEmail;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public TransactionReceipt getTransactionReceipt() {
		return transactionReceipt;
	}

	public void setTransactionReceipt(TransactionReceipt transactionReceipt) {
		this.transactionReceipt = transactionReceipt;
	}

	public TransactionReceipt getRefundReceipt() {
		return refundReceipt;
	}

	public void setRefundReceipt(TransactionReceipt refundReceipt) {
		this.refundReceipt = refundReceipt;
	}

	public Map getAmount() {
		return amount;
	}

	public void setAmount(Map amount) {
		this.amount = amount;
	}
}
