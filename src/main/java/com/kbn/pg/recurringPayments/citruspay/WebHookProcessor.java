package com.kbn.pg.recurringPayments.citruspay;

import java.util.Date;
import java.util.Map;

import com.kbn.commons.dao.TransactionSearchService;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.CitrusPaySubscription;
import com.kbn.commons.user.CitrusPaySubscriptionDao;
import com.kbn.commons.user.TransactionSummaryReport;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionManager;
import com.kbn.pg.recurringPayments.MerchantWebhookInvoker;

/**
 * @author Puneet
 *
 */
public class WebHookProcessor {

	private String subscriptionId;
	private TransactionReceipt transactionReceipt;
	private boolean isduplicateFlag;
	private CitrusPaySubscription subscription;

	public WebHookProcessor(String subscriptionId, TransactionReceipt transactionReceipt){
		this.subscriptionId = subscriptionId;
		this.transactionReceipt = transactionReceipt;
	}

	public void preProcess() throws SystemException{
		CitrusPaySubscriptionDao citrusPaySubscriptionDao = new CitrusPaySubscriptionDao();
		CitrusPaySubscription subscription = citrusPaySubscriptionDao.findBySubscriptionId(subscriptionId);
		//check if duplicate request
		TransactionSummaryReport transaction;
		transaction = new TransactionSearchService().getSubscriptionTransaction(transactionReceipt.getTransactionId() , subscription.getTransactionOid());
		if(null!=transaction){
			isduplicateFlag = true;
		}else{
			this.subscription = subscription;
		}
	}

	@SuppressWarnings("rawtypes")
	public void process(String invoiceId, String refId, String paymentStatus, String subscriberEmail, String method, TransactionReceipt refundReceipt, Map amountObject) throws SystemException{
		if(isduplicateFlag){
			return;
		}
		if (transactionReceipt.getErrorCode() == 0) {
			// update subscription
			subscription.setRecurringTransactionsProcessed(subscription
					.getRecurringTransactionsProcessed() + 1L);
			subscription.setLastPaymentDate(new Date().toString());
			new CitrusPaySubscriptionDao().update(subscription);
		}
		TransactionProcessor transactionProcessor = new TransactionProcessor();
		Fields fields = transactionProcessor.prepareFields(subscription, transactionReceipt, amountObject, subscriberEmail);
		fields.insertNewOrder();
		fields.put(FieldType.TXN_ID.getName(),TransactionManager.getNewTransactionId());
		fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));
		fields.insert();
		//TODO invoke merchant webhook and decide abt placement of callbackUrl		
		new MerchantWebhookInvoker().invoke(fields, transactionReceipt, subscription,"");
	}
}
