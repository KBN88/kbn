package com.kbn.pg.recurringPayments.citruspay;

import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.SystemConstants;

/**
 * @author Puneet
 *
 */
public class SubscriptionCommunicator {
	private static Logger logger = Logger
			.getLogger(SubscriptionCommunicator.class.getName());
	public  String communicate(Fields fields, String request, String hostUrl, String oAuthToken) throws SystemException{
			String response = "";

			logRequest(request, hostUrl, fields);
			try {
				StringRequestEntity requestEntity = new StringRequestEntity(  request, "application/json", SystemConstants.DEFAULT_ENCODING_UTF_8);
				PostMethod postMethod = new PostMethod(hostUrl);
				postMethod.setRequestHeader("Accept", "application/json");
				postMethod.setRequestHeader("Authorization", oAuthToken);
				postMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
				postMethod.setRequestEntity(requestEntity);
				HttpClient httpClient = new HttpClient();
				httpClient.executeMethod(postMethod);

				response = postMethod.getResponseBodyAsString();

			} catch (IOException ioException) {			
				throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
						ioException, "Network Exception with CitrusPay "
								+ hostUrl.toString());
				}
			logResponse(response, fields);
			return response;
		}

	public  String communicate(String request, String hostUrl, String oAuthToken) throws SystemException{
		String response = "";

		logRequest(request, hostUrl , null);
		try {
			StringRequestEntity requestEntity = new StringRequestEntity(  request, "application/json", SystemConstants.DEFAULT_ENCODING_UTF_8);
			PutMethod putMethod = new PutMethod(hostUrl);
			putMethod.setRequestHeader("Accept", "application/json");
			putMethod.setRequestHeader("Authorization", oAuthToken);
			putMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			putMethod.setRequestEntity(requestEntity);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(putMethod);

			response = putMethod.getResponseBodyAsString();

		} catch (IOException ioException) {			
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR,
					ioException, "Network Exception with CitrusPay "
							+ hostUrl.toString());
			}
		logResponse(response, null);
		return response;
	}

	public void logRequest(String requestMessage, String url,Fields fields){
		log("Request message to Citrus: Url= "+url +" "+ requestMessage, fields);
	}

   public void logResponse(String requestMessage, Fields fields){
		log("Response message from Citrus: "+ requestMessage, fields);
	}

	private void log(String message, Fields fields){
		message = Pattern.compile("(\"pan\":\")([\\s\\S]*?)(\")").matcher(message).replaceAll("$1$3");
		message = Pattern.compile("(\"expiry\":\")([\\s\\S]*?)(\")").matcher(message).replaceAll("$1$3");
		if(null!=fields){
			MDC.put(FieldType.INTERNAL_CUSTOM_MDC.getName(), fields.getCustomMDC());
		}
		logger.info(message);
	}
}
