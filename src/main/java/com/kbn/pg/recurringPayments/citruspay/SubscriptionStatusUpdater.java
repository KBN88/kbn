package com.kbn.pg.recurringPayments.citruspay;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.kbn.citruspay.CitrusPayTransaction;
import com.kbn.citruspay.ResponseFactory;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.CitrusPaySubscription;
import com.kbn.commons.user.CitrusPaySubscriptionDao;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.SubscriptionStatus;

/**
 * @author Puneet
 *
 */
public class SubscriptionStatusUpdater {

	private static Logger logger = Logger
			.getLogger(SubscriptionStatusUpdater.class.getName());
	public void deactivateCompletedSubscriptions(){
		CitrusPaySubscriptionDao citrusPaySubscriptionDao= new CitrusPaySubscriptionDao();
		List<CitrusPaySubscription> completedSubscriptionList = citrusPaySubscriptionDao.findCompletedSubs();
		for(CitrusPaySubscription citrusPaySubscription: completedSubscriptionList){
			try{
				deactivateSubscription(citrusPaySubscription);
			}catch(Exception exception){
				//TODO.. send email
				logger.error("Error updating subscription with recurring txnId: " + citrusPaySubscription.getRecurringTransactionId() + exception);
				
			}
		}
	}

	public void deactivateSubscription(CitrusPaySubscription citrusPaySubscription) throws SystemException{
		String request = new CitrusPayTransaction().createAlterSubscriptionRequest(citrusPaySubscription, SubscriptionStatus.INACTIVE.getName());		
		changeSubscriptionStatus(citrusPaySubscription, request);
	}

	//RFU to activate/alter a subscription
	public void activateSubscription(CitrusPaySubscription citrusPaySubscription) throws SystemException{
		String request = new CitrusPayTransaction().createAlterSubscriptionRequest(citrusPaySubscription, SubscriptionStatus.ACTIVE.getName());		
		changeSubscriptionStatus(citrusPaySubscription, request);
	}

	public void changeSubscriptionStatus(CitrusPaySubscription citrusPaySubscription, String request) throws SystemException{

		String[] merchantDetails = (new PropertiesManager().getCitrusRecurringPaymentProperty((citrusPaySubscription.getPayId()))).split(",");

		String hostUrl = ConfigurationConstants.CITRUSPAY_CREATE_SUBSCRIPTION_URL.getValue()+ "/" + citrusPaySubscription.getSubscriptionId(); 
		//call citrus api
		String response = new SubscriptionCommunicator().communicate(request, hostUrl, merchantDetails[0]);
		String responseStatus = ResponseFactory.parseAlterTokenResponse(response);
		//update DB
		if(StringUtils.isNoneBlank(responseStatus) && responseStatus.equals(SubscriptionStatus.INACTIVE.getName())){
			citrusPaySubscription.setStatus(SubscriptionStatus.INACTIVE);
			new CitrusPaySubscriptionDao().update(citrusPaySubscription);
			//send post to merchant
			//TODO....
		}else if(StringUtils.isNoneBlank(responseStatus) && responseStatus.equals(SubscriptionStatus.INACTIVE.getName())){
			citrusPaySubscription.setStatus(SubscriptionStatus.ACTIVE);
			new CitrusPaySubscriptionDao().update(citrusPaySubscription);
		}
		//TODO.. map other responses.....
	}
}
