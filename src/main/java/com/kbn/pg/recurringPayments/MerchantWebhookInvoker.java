package com.kbn.pg.recurringPayments;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.user.CitrusPaySubscription;
import com.kbn.commons.util.AsyncPostCreater;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Amount;
import com.kbn.pg.recurringPayments.citruspay.TransactionReceipt;



public class MerchantWebhookInvoker{

	private static Logger logger = Logger.getLogger(MerchantWebhookInvoker.class.getName());	
	/*
	 * 
	 * 
	 *  1.       RECURRING_TRANSACTION_ID: Identifies the recurring transaction
		2.       RECURRING_TRANSACTION_COUNT: The number of transactions that are supposed to be captured
		3.       RECURRING_TRANSACTION_INTERVAL: Interval monthly etc
		4.       RECURRING_TRANSACTIONS_PROCESSED: Number of transactions that have been processed
		5.       STATUS: Status of the recurring txn (Active, Inactive)
		6.       AMOUNT: Amount of txn
		7.       LAST_PAYMENT_DATE: Date on which last payment was charged
		8.       NEXT_BILLING_DATE : Date on which next payment should be charged (If the recurring txn is active)
		9.       CUST_NAME: Card holder name
		10.   	 RESPONSE_MESSAGE
		11.   	 RESPONSE_CODE
		12.   	 TRANSACTION_ID: Id of the transaction if the webhook updation is for card charging  */

	public void invoke(Fields fields, TransactionReceipt transactionReceipt, CitrusPaySubscription subscription, String hostUrl){
		//TODO.. put hostUrl for callback in subscription or not
		Map<String,String> requestMap = new HashMap<String,String>();

		requestMap.put("RECURRING_TRANSACTION_ID", subscription.getRecurringTransactionId());
		requestMap.put("RECURRING_TRANSACTION_COUNT",subscription.getRecurringTransactionCount().toString());
		requestMap.put("RECURRING_TRANSACTION_INTERVAL",subscription.getRecurringTransactionInterval().getName());
		requestMap.put("RECURRING_TRANSACTIONS_PROCESSED",subscription.getRecurringTransactionsProcessed().toString());
		requestMap.put("STATUS",subscription.getStatus().getName());
		requestMap.put("AMOUNT",Amount.formatAmount(subscription.getAmount(),subscription.getCurrency()));
		requestMap.put("LAST_PAYMENT_DATE",subscription.getLastPaymentDate());
		requestMap.put("NEXT_BILLING_DATE",subscription.getNextBillingDate());
		requestMap.put("CUST_NAME",subscription.getCardHolderName());
		requestMap.put("RESPONSE_MESSAGE",fields.get(FieldType.RESPONSE_MESSAGE.getName()));
		requestMap.put("RESPONSE_CODE",fields.get(FieldType.RESPONSE_CODE.getName()));
		requestMap.put("TRANSACTION_ID",fields.get(FieldType.TXN_ID.getName()));
		new AsyncPostCreater().sendPost(requestMap,hostUrl);
	}
}
