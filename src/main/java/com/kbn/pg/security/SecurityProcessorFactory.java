package com.kbn.pg.security;

import com.kbn.pg.core.Processor;

public class SecurityProcessorFactory {

	public SecurityProcessorFactory() {
	}

	public static Processor getInstance(){
		return new SecurityProcessor();
	}
}
