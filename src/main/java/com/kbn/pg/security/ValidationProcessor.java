package com.kbn.pg.security;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Processor;

public class ValidationProcessor implements Processor {

	public ValidationProcessor() {
	}

	public void preProcess(Fields fields) {
	}

	public void process(Fields fields) throws SystemException {
		Validator validator = ValidatorFactory.getValidator();
		validator.validate(fields);
	}

	public void postProcess(Fields fields) {
	}
}
