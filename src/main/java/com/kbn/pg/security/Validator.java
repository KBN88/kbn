package com.kbn.pg.security;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.Fields;

public interface Validator {
	public void validate(Fields fields) throws SystemException;
}
