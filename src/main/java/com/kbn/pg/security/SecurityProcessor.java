package com.kbn.pg.security;

import org.apache.commons.lang3.StringUtils;

import com.kbn.commons.crypto.AccountPasswordScrambler;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.security.Authenticator;
import com.kbn.commons.security.AuthenticatorFactory;
import com.kbn.commons.user.Account;
import com.kbn.commons.user.AccountCurrency;
import com.kbn.commons.user.User;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.ModeType;
import com.kbn.commons.util.StatusType;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AcquirerType;
import com.kbn.pg.core.Processor;
import com.kbn.pg.history.Historian;

public class SecurityProcessor implements Processor {
	private Authenticator authenticator = AuthenticatorFactory.getAuthenticator();
	private Historian historian = new Historian();

	public void preProcess(Fields fields) throws SystemException {
		fields.logAllFields("Raw Request:");

		fields.clean();

		Fields internalFields = fields.removeInternalFields();

		// The fields sent by client, which are not allowed should be removed
		fields.removeExtraFields();

		//put Internal fields back
		fields.put(internalFields);

		//Add txn Id etc
		fields.addDefaultFields();
	}

	public void process(Fields fields) throws SystemException {
		// Process validations
		validate(fields);//

		fields.logAllFields("Refined Request:");
		// Process authenticity
		authenticate(fields);

		addPreviousFields(fields);

		//Get applicable acquirer and respective fields
		addAcquirerFields(fields);

		//TODO......make an interface for acquirer specific validation as its taken out of ValidationProcessor
		GeneralValidator generalValidator = new GeneralValidator();
		generalValidator.processorValidations(fields);
	}

	public void validate(Fields fields) throws SystemException {
		Processor validationProcessor = new ValidationProcessor();
		validationProcessor.preProcess(fields);
		validationProcessor.process(fields);
		validationProcessor.postProcess(fields);
	}

	public void addPreviousFields(Fields fields) throws SystemException{
		//Ideally previous fields are responsibility of history processor, 
		// but we are putting it here for smart router
		historian.findPrevious(fields);
		historian.populateFieldsFromPrevious(fields);	
	}

	public void addAcquirerFields(Fields fields) throws SystemException{

		if (fields.get(FieldType.TXNTYPE.getName()).equals(
				TransactionType.NEWORDER.getName())) {
			return;
		}

		User user = authenticator.getUser();

		String acquirer = fields.get(FieldType.ACQUIRER_TYPE.getName());
		if(StringUtils.isEmpty(acquirer)){
			acquirer = fields.get(FieldType.INTERNAL_ACQUIRER_TYPE.getName());			
			if(StringUtils.isEmpty(acquirer)){
				authenticator.validatePaymentOptions(fields);
				AcquirerType acquirerType = AcquirerType.getDefault(fields,user,authenticator.getSupportedChargingDetailsList());
				acquirer = acquirerType.getCode();
				fields.put(FieldType.ACQUIRER_TYPE.getName(), acquirer);
				fields.put(FieldType.INTERNAL_ACQUIRER_TYPE.getName(), acquirer);
			}else{
				fields.put(FieldType.ACQUIRER_TYPE.getName(), acquirer);
			}
		}

		Account account = user.getAccountUsingAcquirerCode(acquirer);
		String currencyCode = fields.get(FieldType.CURRENCY_CODE.getName());

		if(null == account ){
			String payId = fields.get(FieldType.PAY_ID.getName());
			throw new SystemException(ErrorType.NOT_APPROVED_FROM_ACQUIRER, "User is not approved from acquirer, PayId=" + payId + " , Acquirer = " + acquirer);
		}

		AccountCurrency accountCurrency = account.getAccountCurrency(currencyCode);		
		if(null==accountCurrency){
			fields.put(FieldType.STATUS.getName(), StatusType.INVALID.getName());
			throw new SystemException(ErrorType.CURRENCY_NOT_MAPPED,ErrorType.CURRENCY_NOT_MAPPED.getResponseMessage() + user.getPayId() + " and currency code "+fields.get(FieldType.CURRENCY_CODE.getName()));
		}

		//Get MerchantId from account
		String merchantId = accountCurrency.getMerchantId();
		if(!StringUtils.isEmpty(merchantId)){
			fields.put(FieldType.MERCHANT_ID.getName(), merchantId);
		}

		boolean nonsecure = accountCurrency.isDirectTxn();
		String txnType = fields.get(FieldType.TXNTYPE.getName());
		if(nonsecure && txnType.equals(TransactionType.ENROLL.getName())){
			fields.put(FieldType.TXNTYPE.getName(),ModeType.getDefaultPurchaseTransaction(user.getModeType()).getName());
		}
		//Get Password
		if(!StringUtils.isEmpty(accountCurrency.getPassword())){
			String decryptedPassword = AccountPasswordScrambler.decryptPassword(accountCurrency.getPassword());
			fields.put(FieldType.PASSWORD.getName(), decryptedPassword);	
		}

		//Get key
		String encryptionKey = accountCurrency.getTxnKey();
		if(!StringUtils.isEmpty(encryptionKey)){
			fields.put(FieldType.TXN_KEY.getName(), encryptionKey);
		}

		String internalTxnType = fields.get(FieldType.INTERNAL_ORIG_TXN_TYPE.getName());
		if(null == internalTxnType){
			fields.put(FieldType.INTERNAL_ORIG_TXN_TYPE.getName(), ModeType.getDefaultPurchaseTransaction(user.getModeType()).getName());
		}
	}

	public void authenticate(Fields fields) throws SystemException {
		User user = authenticator.getUserFromPayId(fields);
		authenticator.setUser(user);
		authenticator.authenticate(fields);
	}

	public void postProcess(Fields fields) {		
		//Some secure fields are not required internally
		fields.removeSecureFieldsSubmitted();
	}

	public Authenticator getAuthenticator() {
		return authenticator;
	}

	public void setAuthenticator(Authenticator authenticator) {
		this.authenticator = authenticator;
	}
}
