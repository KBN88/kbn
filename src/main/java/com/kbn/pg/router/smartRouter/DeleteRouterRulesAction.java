package com.kbn.pg.router.smartRouter;

import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.router.RouterRuleDao;

public class DeleteRouterRulesAction extends AbstractSecureAction {

	/**
	 * @author neeraj
	 */
	private static final long serialVersionUID = 3488145720669570160L;
	public String id;

	public String execute() {
		RouterRuleDao routerRuleDao = new RouterRuleDao();

		routerRuleDao.deleteRule(id);

		return SUCCESS;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
