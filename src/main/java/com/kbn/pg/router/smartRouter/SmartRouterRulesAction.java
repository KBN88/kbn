package com.kbn.pg.router.smartRouter;

import java.util.Date;
import java.util.List;

import com.kbn.commons.util.TDRStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.router.RouterRule;
import com.kbn.pg.router.RouterRuleDao;

public class SmartRouterRulesAction extends AbstractSecureAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7887391109916945928L;
	private List<RouterRule> listData;
	private RouterRuleDao routerRuleDao = new RouterRuleDao();

	public String execute() {
		Date createDate = new Date();
		for (RouterRule routerRule : listData) {
			routerRule.setCreateDate(createDate);
			routerRule.setStatus(TDRStatus.ACTIVE);
			routerRuleDao.create(routerRule);

		}

		return SUCCESS;
	}


	public List<RouterRule> getListData() {
		return listData;
	}


	public void setListData(List<RouterRule> listData) {
		this.listData = listData;
	}


	public RouterRuleDao getRouterRuleDao() {
		return routerRuleDao;
	}

	public void setRouterRuleDao(RouterRuleDao routerRuleDao) {
		this.routerRuleDao = routerRuleDao;
	}

}
