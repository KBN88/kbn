package com.kbn.pg.router.smartRouter;

import java.util.ArrayList;
import java.util.List;

import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.pg.router.RouterRule;
import com.kbn.pg.router.RouterRuleDao;

public class GetRouterRulesAction extends AbstractSecureAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7425508366954982184L;
	private List<RouterRule> routerRules = new ArrayList<RouterRule>();

	public String execute() {
		RouterRuleDao routerRuleDao = new RouterRuleDao();
		routerRules = routerRuleDao.getActiveRules();

		return SUCCESS;

	}

	public List<RouterRule> getRouterRules() {
		return routerRules;
	}

	public void setRouterRules(List<RouterRule> routerRules) {
		this.routerRules = routerRules;
	}

}
