package com.kbn.pg.router;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;

public class RouterRuleDao extends HibernateAbstractDao {
	public RouterRuleDao() {
		super();
	}

	public RouterRule find(String name) throws DataAccessLayerException {
		return (RouterRule) super.find(RouterRuleImpl.class, name);
	}

	public void create(RouterRule routerRule) throws DataAccessLayerException {
		super.save(routerRule);
	}

	public void delete(RouterRule routerRule) throws DataAccessLayerException {
		super.delete(routerRule);
	}

	@SuppressWarnings("unchecked")
	public List<RouterRule> getActiveRules() {
		List<RouterRule> rulesList = new ArrayList<RouterRule>();
		rulesList = null;
		try {
			startOperation();
			rulesList = getSession().createQuery("from RouterRule RR where  RR.status='ACTIVE' ").setCacheable(true)
					.getResultList();
			getTx().commit();

			return rulesList;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return rulesList;
	}

	public void deleteRule(String id) {
		try {
			startOperation();
			getSession().createQuery("delete from RouterRule RR where  RR.id= :id").setParameter("id", new Long(id))
					.executeUpdate();
			getTx().commit();

		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}

	}

}
