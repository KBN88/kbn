package com.kbn.pg.router;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.kbn.commons.util.TDRStatus;

@Entity
@Proxy(lazy = false)
@Table
public class RouterRule implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5849051277144455673L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String currency;
	private String paymentType;
	private String mopType;
	private String transactionType;
	@Enumerated(EnumType.STRING)
	private TDRStatus status;
	private boolean onUsFlag;
	private Date createDate;
	private Date updateDate;
	private String merchant;
	private String acquirerMap;
    private String paymentsRegion;
    private String cardHolderType;
   
	public RouterRule() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getMopType() {
		return mopType;
	}

	public void setMopType(String mopType) {
		this.mopType = mopType;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public TDRStatus getStatus() {
		return status;
	}

	public void setStatus(TDRStatus status) {
		this.status = status;
	}


	public boolean isOnUsFlag() {
		return onUsFlag;
	}

	public void setOnUsFlag(boolean onUsFlag) {
		this.onUsFlag = onUsFlag;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getAcquirerMap() {
		return acquirerMap;
	}

	public void setAcquirerMap(String acquirerMap) {
		this.acquirerMap = acquirerMap;
	}



	public String getPaymentsRegion() {
		return paymentsRegion;
	}

	public void setPaymentsRegion(String paymentsRegion) {
		this.paymentsRegion = paymentsRegion;
	}

	public String getCardHolderType() {
		return cardHolderType;
	}

	public void setCardHolderType(String cardHolderType) {
		this.cardHolderType = cardHolderType;
	}

}
