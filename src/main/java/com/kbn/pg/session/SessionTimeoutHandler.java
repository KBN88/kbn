package com.kbn.pg.session;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.PostBackCreator;
import com.kbn.pg.core.Processor;
import com.kbn.pg.core.TimeOutProcessor;

public class SessionTimeoutHandler {

	private static final String transactionTypes = "NEWORDER-SALE-AUTHORISE-ENROLL";
	private static Logger logger = Logger.getLogger(SessionTimeoutHandler.class
			.getName());

	public void handleTimeOut(Fields fields) {
		PostBackCreator postBackCreator = new PostBackCreator();
		String status = fields.get(FieldType.STATUS.getName());
		if (transactionTypes.contains(fields.get(FieldType.TXNTYPE.getName()))
				&& status.equals(StatusType.PENDING.getName())
				|| status.equals(StatusType.SENT_TO_BANK.getName())
				|| status.equals(StatusType.ENROLLED.getName())) {
			// call timeout processor
			try {
				Processor processor = new TimeOutProcessor();
				processor.preProcess(fields);
				processor.process(fields);
				processor.postProcess(fields);
				if(TaskSchedulerListener.postBackFlag){
					postBackCreator.sendPostBack(fields);
				}else{
					System.out.println("postback not run");
				}
			} catch (SystemException systemException) {
				logger.error("Error handling timeout and updating transaction" + systemException);
			} catch (Exception exception) {
				logger.error("Unmapped exception handling timeout" + exception);
			}
		} else {			
				// send post back
			if(TaskSchedulerListener.postBackFlag){
				postBackCreator.sendPostBack(fields);
			}else{
				System.out.println("postback not run");
			}
		}
	}
}
