package com.kbn.pg.session;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.commons.util.TaskSchedular;
import com.kbn.pg.recurringPayments.RecurringPaymentsJob;

/**
 * @author Neeraj
 */

public class TaskSchedulerListener implements ServletContextListener {	
	private static Logger logger = Logger.getLogger(TaskSchedulerListener.class.getName());
	public static boolean postBackFlag;

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		PropertiesManager propertiesManager=new PropertiesManager();
		String flag = ConfigurationConstants.SEND_POSTBACK_FLAG.getValue();
		if(flag.equals(Constants.TRUE.getValue())){
			postBackFlag = true;
		}
		if (propertiesManager.getSystemProperty(CrmFieldConstants.RUN_SCHEDULAR_FALG.getValue()).equals(Constants.TRUE.getValue())) {
			logger.info("Schedular started");
			TaskSchedular task = new TaskSchedular();
			Timer timer = new Timer();
			timer.scheduleAtFixedRate(task,Long.valueOf(ConfigurationConstants.TASK_SCHEDULAR_RUNNING_DELAY.getValue()),
					Long.valueOf(ConfigurationConstants.TASK_SCHEDULAR_RUNNING_DELAY.getValue()));

			//quartz job for recurring payments
			try{
				JobDetail recurringPaymentJob = JobBuilder.newJob(RecurringPaymentsJob.class).withIdentity(Constants.RECURRING_PAYMENT_JOB.getValue(), Constants.CITRUSPAY.getValue()).build();
				Trigger recurringPaymentJobTrigger = TriggerBuilder.newTrigger().withIdentity(Constants.RECURRING_PAYMENT_JOB_TRIGGER.getValue(),Constants.CITRUSPAY_TRIGGER.getValue()).
																withSchedule(CronScheduleBuilder.cronSchedule(propertiesManager.getSystemProperty(Constants.DAILY_CRON_STRING.getValue()))).build();
				Scheduler scheduler = new StdSchedulerFactory().getScheduler();
				scheduler.start();
				scheduler.scheduleJob(recurringPaymentJob, recurringPaymentJobTrigger);
			}catch(Exception exception){
				logger.error("Error running schedular for recurring payments " + exception);
			}
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		logger.info("Schedular end");
	}
}