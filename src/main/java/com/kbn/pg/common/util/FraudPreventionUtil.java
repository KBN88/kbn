package com.kbn.pg.common.util;

import com.kbn.commons.util.Constants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.SystemConstants;

/**
 * @author Harpreet
 *
 */
public class FraudPreventionUtil {
	// to create truncated/masked card no
	public static String makeCardMask(Fields fields){
		String reqCardNo = fields.get(FieldType.CARD_NUMBER.getName());
		StringBuilder sb = new StringBuilder();
		sb.append(reqCardNo.substring(0, SystemConstants.CARD_BIN_LENGTH ));
		sb.append(Constants.CARD_STARS.getValue());
		sb.append(reqCardNo.subSequence(reqCardNo.length() - SystemConstants.CARD_BIN_LENGTH + 2, reqCardNo.length()));
		return sb.toString();
	}
}
