package com.kbn.chargeback;

import org.apache.log4j.Logger;

import com.kbn.chargeback.action.beans.Chargeback;
import com.kbn.chargeback.action.beans.ChargebackDao;
import com.kbn.chargeback.utils.CaseStatus;
import com.kbn.chargeback.utils.ChargebackStatus;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class UpdateChargebackDetails extends AbstractSecureAction {

	private static final long serialVersionUID = 3981325447017559786L;
	private static Logger logger = Logger.getLogger(UpdateChargebackDetails.class.getName());
	private String Id;
	private String caseId;

	private ChargebackDao chargebackDao = new ChargebackDao();
	private Chargeback chargeback = new Chargeback();

	public String execute() {

		try {
			setChargeback(chargebackDao.findByCaseId(getCaseId()));
			Chargeback chargeback = new Chargeback();
			chargeback = getChargeback();
			chargeback.setChargebackStatus(ChargebackStatus.ACCEPTED_BY_MERCHANT.getName());
			chargeback.setStatus(CaseStatus.CLOSE.getName());
			chargebackDao.update(chargeback);
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Chargeback getChargeback() {
		return chargeback;
	}

	public void setChargeback(Chargeback chargeback) {
		this.chargeback = chargeback;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

}
