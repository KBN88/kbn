package com.kbn.chargeback.action.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.transaction.SystemException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;

import com.kbn.chargeback.action.beans.Chargeback;
import com.kbn.chargeback.utils.ChargebackType;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.DataAccessLayerException;
import com.kbn.commons.exception.ErrorType;
import com.kbn.ticketing.core.HelpTicket;

public class ChargebackDao extends HibernateAbstractDao {

	private static Logger logger = Logger.getLogger(ChargebackDao.class.getName());

	public ChargebackDao() {
		super();
	}

	public void create(Chargeback chargeback) throws DataAccessLayerException {
		super.save(chargeback);
	}

	public void delete(Chargeback chargeback) throws DataAccessLayerException {
		super.delete(chargeback);
	}

	public void update(Chargeback chargeback) throws DataAccessLayerException {
		super.saveOrUpdate(chargeback);
	}
	
	public Chargeback find(String name) throws DataAccessLayerException {
		return (Chargeback) super.find(Chargeback.class, name);
	}

	public List<Chargeback> findChargebackByPayid(String payId, String fromDate, String toDate) {
		List<Chargeback> userChargeback = new ArrayList<Chargeback>();

		try {
			startOperation();
			
			String sqlQuery = "select ch.* from Chargeback ch where ch.createDate >= :fromDate and ch.createDate <= :toDate and payId like :payId";
			if (payId.equals("ALL")) {
				payId = "%";
			}
			
			userChargeback = getSession().createNativeQuery(sqlQuery, Chargeback.class)
										   .setParameter("payId", payId)
										    .setParameter("fromDate", fromDate)
										     .setParameter("toDate", toDate+"23:59:59")
										   .getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return userChargeback;
	}

	public List<Chargeback> findAllChargeback(String dateFrom, String dateTo, String payId, String chargebackType,
			String chargebackStatus) {
		List<Chargeback> userChargeback = new ArrayList<Chargeback>();

		try {
			startOperation();

			String sqlQuery = "select ch.* from Chargeback ch where ch.createDate >= :fromDate and ch.createDate <= :toDate and payId like :payId  and chargebackType like :chargebackType and chargebackStatus like :chargebackStatus ORDER BY ID DESC";
			if (payId.equals("ALL")) {
				payId = "%";
			}
			if (chargebackType.equals("ALL")) {
				chargebackType = "%";
			}
			if (chargebackStatus.equals("ALL")) {
				chargebackStatus = "%";
			}

			userChargeback = getSession().createNativeQuery(sqlQuery, Chargeback.class)
					.setParameter("fromDate", dateFrom).setParameter("toDate", dateTo + " 23:59:59")
					.setParameter("payId", payId).setParameter("chargebackType", chargebackType)
					.setParameter("chargebackStatus", chargebackStatus).getResultList();
			getTx().commit();
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return userChargeback;
	}

	public Chargeback findByCaseId(String caseID) {

		Chargeback responseUser = null;
		try {
			startOperation();
			responseUser = (Chargeback) getSession().createQuery("from Chargeback CH where CH.caseId = :caseId")
					.setParameter("caseId", caseID).setCacheable(true).getSingleResult();
			getTx().commit();
			return responseUser;
		} catch (NoResultException noResultException) {
			return null;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			logger.error(hibernateException);
			handleException(hibernateException);
		} finally {
			autoClose();
		}
		return responseUser;
	}
	
	public Chargeback findByTxnId(String txnID , String status , String chargebackType) {

		Chargeback chargeback = null;
		
		try {
			startOperation();
			if (chargebackType.equals(ChargebackType.PRE_ARBITRATION.getName())){
				chargeback = (Chargeback) getSession().createQuery("from Chargeback CH where CH.transactionId = :txnID and CH.status != 'Closed' ")
						.setParameter("txnID", txnID).setCacheable(true).getSingleResult();
				getTx().commit();
				return chargeback;
			}
			else{
				chargeback = (Chargeback) getSession().createQuery("from Chargeback CH where CH.transactionId = :txnID ")
						.setParameter("txnID", txnID).setCacheable(true).getSingleResult();
				getTx().commit();
				return chargeback;
			}
		} catch (NoResultException noResultException) {
			return null;
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			logger.error(hibernateException);
			handleException(hibernateException);
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		finally {
			autoClose();
		}
		return chargeback;
	}

	public void updateComment(String caseId, byte[] comments) throws SystemException {
		try {
			startOperation();
			int updateStatus = getSession()
					.createQuery("update Chargeback C set C.comments = :comments where C.caseId = :caseId")
					.setParameter("caseId", caseId).setParameter("comments", comments).executeUpdate();
			getTx().commit();
			if (1 != updateStatus) {
				throw new SystemException(
						"Error updating comments with Chargeback: " + caseId + ErrorType.DATABASE_ERROR);
			}
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
	}

	public void updateStatus(String chargebackStatus, String caseId) throws SystemException {
		try {
			startOperation();
			int updateStatus = getSession()
					.createQuery(
							"update Chargeback C set C.chargebackStatus = :chargebackStatus where C.caseId = :caseId")
					.setParameter("caseId", caseId).setParameter("chargebackStatus", chargebackStatus).executeUpdate();
			getTx().commit();
			if (1 != updateStatus) {
				throw new SystemException(
						"Error updating Status with Chargeback: " + caseId + ErrorType.DATABASE_ERROR);
			}
		} catch (ObjectNotFoundException objectNotFound) {
			handleException(objectNotFound);
		} catch (HibernateException hibernateException) {
			handleException(hibernateException);
		} finally {
			autoClose();
		}
	}

}
