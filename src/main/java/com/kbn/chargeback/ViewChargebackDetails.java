package com.kbn.chargeback;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.kbn.chargeback.action.beans.Chargeback;
import com.kbn.chargeback.action.beans.ChargebackComment;
import com.kbn.chargeback.action.beans.ChargebackDao;
import com.kbn.crm.commons.action.AbstractSecureAction;
import com.kbn.ticketing.core.TicketComment;

public class ViewChargebackDetails extends AbstractSecureAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 576762458042745734L;
	private static Logger logger = Logger.getLogger(ViewChargebackDetails.class.getName());
	private Chargeback chargeback = new Chargeback();
	private String Id;
	private String caseId;
	private String commentsString;
	private String commentId;
	private String createDate;
	private String messageBody;
	private String response;
	private String caseStatus;
	private List<ChargebackComment> commentList = new LinkedList<>();

	
	public String execute() {
		ChargebackDao chargebackDao = new ChargebackDao();

		try {

			setChargeback(chargebackDao.findByCaseId(getCaseId()));
			if (chargeback.getComments() != null){
				/*byte[] allFields = Base64.decodeBase64(chargeback.getComments());
				commentsString = new String(allFields);*/
				commentsString = new String(chargeback.getComments());
				
			}
			Set<ChargebackComment> chargebackCommentSet = chargeback.getChargebackComments();
			Iterator<ChargebackComment> iterator = chargebackCommentSet.iterator();
			while (iterator.hasNext()) {
				ChargebackComment chargebackComment = iterator.next();
				commentList.add(chargebackComment);
			}
			/* date$time wise sort */
			Collections.sort(commentList, new Comparator<ChargebackComment>() {
				@Override
				public int compare(ChargebackComment o1, ChargebackComment o2) {
					if (o1.getCreateDate() == null
							|| o2.getCreateDate() == null) {
						return 0;
					}
					return o1.getCreateDate().compareTo(o2.getCreateDate());
				}
			});
			
			return SUCCESS;
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;

		}

	}

	public Chargeback getChargeback() {
		return chargeback;
	}

	public void setChargeback(Chargeback chargeback) {
		this.chargeback = chargeback;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getCommentsString() {
		return commentsString;
	}

	public void setCommentsString(String commentsString) {
		this.commentsString = commentsString;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<ChargebackComment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<ChargebackComment> commentList) {
		this.commentList = commentList;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getCaseStatus() {
		return caseStatus;
	}

	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}

}
