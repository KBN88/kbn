package com.kbn.chargeback;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.user.TransactionHistory;
import com.kbn.commons.util.MopType;
import com.kbn.commons.util.PaymentType;
import com.kbn.crm.actionBeans.RefundDetailsProvider;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class GenerateChargebackAction extends AbstractSecureAction {

	private static final long serialVersionUID = -5759531242556258421L;
	private static Logger logger = Logger.getLogger(GenerateChargebackAction.class.getName());
	private TransactionHistory transDetails = new TransactionHistory();
	
	private String txnId;
	private String orderId;
	private String payId;
	
	public String execute() {
		
		try{
			getTransactionDetails();	
			return SUCCESS;
			}
			catch(Exception exception){
				logger.error("Exception", exception);
				return ERROR;
			}
	}
	private void getTransactionDetails() throws SystemException{
		RefundDetailsProvider refundDetailsProvider = new RefundDetailsProvider(orderId, payId, txnId);
		refundDetailsProvider.getAllTransactions();
		transDetails = refundDetailsProvider.getTransDetails();
		transDetails.setMopType(MopType.getmopName(transDetails.getMopType()));
		transDetails.setPaymentType(PaymentType.getpaymentName(transDetails.getPaymentType()));
	}
	
	public TransactionHistory getTransDetails() {
		return transDetails;
	}
	public void setTransDetails(TransactionHistory transDetails) {
		this.transDetails = transDetails;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	
}
