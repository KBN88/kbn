package com.kbn.chargeback;

import java.util.List;

import org.apache.log4j.Logger;

import com.kbn.chargeback.action.beans.Chargeback;
import com.kbn.chargeback.action.beans.ChargebackDao;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.Constants;
import com.kbn.commons.util.DateCreater;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class ViewChargeback extends AbstractSecureAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4208045338885337001L;
	private static Logger logger = Logger.getLogger(ViewChargeback.class.getName());
	private Chargeback chargeback = new Chargeback();
	private List<Chargeback> aaData;
	private String payId;
	
	private String chargebackType;
	private String chargebackStatus;
	private String dateTo;
	private String dateFrom;
	
	public String execute(){
		ChargebackDao chargebackDao = new ChargebackDao();
		
		try{
			User user = (User) sessionMap.get(Constants.USER);
			
			setDateFrom(DateCreater.formatDateforChargeback(dateFrom));
			setDateTo(DateCreater.formatDateforChargeback(dateTo));
			
			if (user.getUserType().equals(UserType.ADMIN) || user.getUserType().equals(UserType.SUBADMIN) || user.getUserType().equals(UserType.SUPERADMIN)) {
				setAaData(chargebackDao.findAllChargeback(getDateFrom(),getDateTo(),getPayId(),getChargebackType(),getChargebackStatus()));
			}
			else if (user.getUserType().equals(UserType.MERCHANT)) {
				setAaData(chargebackDao.findChargebackByPayid(user.getPayId(),getDateFrom(),getDateTo()));
			}
			
			
			return SUCCESS;
			
		}
		catch(Exception exception){
			logger.error("Exception", exception);
			return ERROR;
			
		}
		
		
	}


	public Chargeback getChargeback() {
		return chargeback;
	}


	public void setChargeback(Chargeback chargeback) {
		this.chargeback = chargeback;
	}


	public List<Chargeback> getAaData() {
		return aaData;
	}


	public void setAaData(List<Chargeback> aaData) {
		this.aaData = aaData;
	}


	public String getPayId() {
		return payId;
	}


	public void setPayId(String payId) {
		this.payId = payId;
	}



	public String getChargebackType() {
		return chargebackType;
	}


	public void setChargebackType(String chargebackType) {
		this.chargebackType = chargebackType;
	}


	public String getChargebackStatus() {
		return chargebackStatus;
	}


	public void setChargebackStatus(String chargebackStatus) {
		this.chargebackStatus = chargebackStatus;
	}


	public String getDateTo() {
		return dateTo;
	}


	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}


	public String getDateFrom() {
		return dateFrom;
	}


	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
}
