package com.kbn.chargeback;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class SaveChargebackDocument extends AbstractSecureAction {

	private static final long serialVersionUID = 5656742582073736246L;
	private static Logger logger = Logger.getLogger(SaveChargebackDocument.class.getName());
	
	public String SaveFile(String saveName, String filename, File controlFile,String payId,String documentId ) {
		String destPath;
		String saveFilename;
		
		PropertiesManager propertiesManager = new PropertiesManager();
		/// format for storing doc is payid/caseid/document.jpg
		destPath = propertiesManager.getSystemProperty("DocumentPath") + payId +"/"+ saveName;
		
		saveFilename = documentId + getFileExtension(filename);
		File destFile = new File(destPath, saveFilename);
		try {
			FileUtils.copyFile(controlFile, destFile); 
			
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return SUCCESS;

	}
	private String getFileExtension(String name) {
		if (name.toLowerCase().endsWith(".pdf")) {
			return ".pdf";
		} else {
			return ".jpg";
		}
	}


}
