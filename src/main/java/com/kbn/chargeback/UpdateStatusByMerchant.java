package com.kbn.chargeback;

import org.apache.log4j.Logger;

import com.kbn.chargeback.action.beans.ChargebackDao;
import com.kbn.crm.commons.action.AbstractSecureAction;

public class UpdateStatusByMerchant extends AbstractSecureAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8763650020568453333L;
	private static Logger logger = Logger.getLogger(UpdateStatusByMerchant.class.getName());
	
	private String chargebackStatus;
	private String caseId;
	public String execute(){
		 ChargebackDao chargebackDao= new ChargebackDao();
		 try {
			chargebackDao.updateStatus(getChargebackStatus(),getCaseId());
		} catch (Exception exception) {
			logger.error("Exception", exception);
			return ERROR;
		}
		return SUCCESS;
		
	}
	public String getChargebackStatus() {
		return chargebackStatus;
	}
	public void setChargebackStatus(String chargebackStatus) {
		this.chargebackStatus = chargebackStatus;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

}
