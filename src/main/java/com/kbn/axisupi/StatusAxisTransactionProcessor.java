package com.kbn.axisupi;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class StatusAxisTransactionProcessor  implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(StatusAxisTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException{
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createStatusTransaction(fields);
		logger.info(" Form  Status Request to Axis-BANK UPI: " + request);
		String response = communicator.sendAxisSatus(request,ConfigurationConstants.AXIS_TRANSACTION_STATUS_URL.getValue());
		logger.info("Form Axis-BANK UPI Statust Response:" + request);
		AxisBankTransformer transformer = new AxisBankTransformer(response);
		transformer.updateAxisBankStatusResponse(fields);
		fields.put(FieldType.INTERNAL_ORIG_TXN_ID.getName(), fields.get(FieldType.ORIG_TXN_ID.getName()));
	
		
	}

}
