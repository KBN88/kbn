package com.kbn.axisupi;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.SystemConstants;

public class TransactionCommunicator {
	private static Logger logger = Logger.getLogger(TransactionCommunicator.class.getName());

	// AXIS Request
	public String sendAuthorization(String request, String hostUrl) throws SystemException {
		logger.info("Request Upi request" + request);
		logger.info("Request Upi hostUrl" + hostUrl);
		String response = null;
		try {

			StringRequestEntity requestEntity = new StringRequestEntity(request, "application/json",
					SystemConstants.DEFAULT_ENCODING_UTF_8);

			PutMethod putMethod = new PutMethod(hostUrl);
			putMethod.setRequestHeader("Content-Type", "application/json");
			putMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			putMethod.setRequestEntity(requestEntity);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(putMethod);
			response = putMethod.getResponseBodyAsString();
			logger.info("Request Frist data:" + response);
		} catch (IOException ioException) {
			logger.error(ioException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with Request  Axis Bank API " + hostUrl.toString());
		}

		return response;
	}

	// AXIS Collect Request
	public String collectRequest(String request, String hostUrl) throws SystemException {
		logger.info("Collect Upi request2" + request);
		logger.info("Collect Upi hostUrl2" + hostUrl);
		String response = null;
		try {

			GetMethod getMethod = new GetMethod(URIUtil.encodePath(hostUrl + request));
			getMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(getMethod);
			response = getMethod.getResponseBodyAsString();
			logger.info("Collect second data:" + response);
		} catch (IOException ioException) {
			logger.error(ioException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with Collect Axis Bank for " + " txn " + hostUrl.toString());
		}

		return response;
	}

	// AXIS Refund Request
	public String transactRefundAPI(String request, String hostUrl) throws SystemException {
		logger.info("Refund Request Upi request" + request);
		logger.info("Refund Request Upi hostUrl" + hostUrl);
		String response = null;
		try {

			StringRequestEntity requestEntity = new StringRequestEntity(request, "application/json",
					SystemConstants.DEFAULT_ENCODING_UTF_8);

			PutMethod putMethod = new PutMethod(hostUrl);
			putMethod.setRequestHeader("Content-Type", "application/json");
			putMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			putMethod.setRequestEntity(requestEntity);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(putMethod);
			response = putMethod.getResponseBodyAsString();
			logger.info("REFUND AXIS RESOPNSE:" + response);
		} catch (IOException ioException) {
			logger.error(ioException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with Refund Request  Axis Bank API " + hostUrl.toString());
		}

		return response;
	}

	// Status
	public String sendAxisSatus(String request, String hostUrl) throws SystemException {
		logger.info("Status Collect Upi request" + request);
		logger.info("Status Collect Upi hostUrl" + hostUrl);
		String response = null;
		try {

			StringRequestEntity requestEntity = new StringRequestEntity(request, "application/json",
					SystemConstants.DEFAULT_ENCODING_UTF_8);

			PutMethod putMethod = new PutMethod(hostUrl);
			putMethod.setRequestHeader("Content-Type", "application/json");
			putMethod.setRequestHeader("Accept-Charset", SystemConstants.DEFAULT_ENCODING_UTF_8);
			putMethod.setRequestEntity(requestEntity);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(putMethod);
			response = putMethod.getResponseBodyAsString();
			logger.info("Status Collect Frist data:" + response);
		} catch (IOException ioException) {
			logger.error(ioException);
			throw new SystemException(ErrorType.INTERNAL_SYSTEM_ERROR, ioException,
					"Network Exception with  Status Axis Bank API:" + hostUrl.toString());
		}

		return response;
	}

}
