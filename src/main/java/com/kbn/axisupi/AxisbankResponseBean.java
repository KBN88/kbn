package com.kbn.axisupi;

public class AxisbankResponseBean {
	
	private String multicard;
	private String amount;
	private String unqTxnId;
	private String merchId;
	private String merchhChanId;
	private String merchantVPA;
	private String tokenURL;
	private String collectRequest;
	private String checkTxnStatus;
	private String refundTxn;
	private String currency;
	private String expiry;
	private String submitTo;
	private String orderId;
	private String unqCustId;
	private String statusCheckSum;
	private String result;
	private String dataCode;
	private String merchantTransactionId;
	private String rrn;
	private String gatewayResponseMessage;
	private String gatewayTransactionId;
	private String gatewayResponseCode;
	private String transactionTimestamp;
	public String getMulticard() {
		return multicard;
	}
	public void setMulticard(String multicard) {
		this.multicard = multicard;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getUnqTxnId() {
		return unqTxnId;
	}
	public void setUnqTxnId(String unqTxnId) {
		this.unqTxnId = unqTxnId;
	}
	public String getMerchId() {
		return merchId;
	}
	public void setMerchId(String merchId) {
		this.merchId = merchId;
	}
	public String getMerchhChanId() {
		return merchhChanId;
	}
	public void setMerchhChanId(String merchhChanId) {
		this.merchhChanId = merchhChanId;
	}
	public String getMerchantVPA() {
		return merchantVPA;
	}
	public void setMerchantVPA(String merchantVPA) {
		this.merchantVPA = merchantVPA;
	}
	public String getTokenURL() {
		return tokenURL;
	}
	public void setTokenURL(String tokenURL) {
		this.tokenURL = tokenURL;
	}
	public String getCollectRequest() {
		return collectRequest;
	}
	public void setCollectRequest(String collectRequest) {
		this.collectRequest = collectRequest;
	}
	public String getCheckTxnStatus() {
		return checkTxnStatus;
	}
	public void setCheckTxnStatus(String checkTxnStatus) {
		this.checkTxnStatus = checkTxnStatus;
	}
	public String getRefundTxn() {
		return refundTxn;
	}
	public void setRefundTxn(String refundTxn) {
		this.refundTxn = refundTxn;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getExpiry() {
		return expiry;
	}
	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}
	public String getSubmitTo() {
		return submitTo;
	}
	public void setSubmitTo(String submitTo) {
		this.submitTo = submitTo;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getUnqCustId() {
		return unqCustId;
	}
	public void setUnqCustId(String unqCustId) {
		this.unqCustId = unqCustId;
	}
	public String getStatusCheckSum() {
		return statusCheckSum;
	}
	public void setStatusCheckSum(String statusCheckSum) {
		this.statusCheckSum = statusCheckSum;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getDataCode() {
		return dataCode;
	}
	public void setDataCode(String dataCode) {
		this.dataCode = dataCode;
	}
	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}
	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}
	public String getRrn() {
		return rrn;
	}
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	public String getGatewayResponseCode() {
		return gatewayResponseCode;
	}
	public void setGatewayResponseCode(String gatewayResponseCode) {
		this.gatewayResponseCode = gatewayResponseCode;
	}
	public String getGatewayTransactionId() {
		return gatewayTransactionId;
	}
	public void setGatewayTransactionId(String gatewayTransactionId) {
		this.gatewayTransactionId = gatewayTransactionId;
	}
	public String getGatewayResponseMessage() {
		return gatewayResponseMessage;
	}
	public void setGatewayResponseMessage(String gatewayResponseMessage) {
		this.gatewayResponseMessage = gatewayResponseMessage;
	}
	public String getTransactionTimestamp() {
		return transactionTimestamp;
	}
	public void setTransactionTimestamp(String transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}
	
	
}
