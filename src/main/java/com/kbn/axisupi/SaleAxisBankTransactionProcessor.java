package com.kbn.axisupi;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.TransactionProcessor;

public class SaleAxisBankTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(SaleAxisBankTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createSaleTransaction(fields);
		logger.info("Request to Axis Bank UPI:"+request);
		String axisresponse = communicator.sendAuthorization(request,ConfigurationConstants.AXIS_TRANSACTION_URL.getValue());
		logger.info("Axis UPI Response11:"+axisresponse);
		AxisBankTransformer axisBankTransformer =new  AxisBankTransformer(axisresponse);
		axisBankTransformer.axisCollectToken();
		fields.put(FieldType.RESPONSE_CODE.getName(), ErrorType.SUCCESS.getCode());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), ErrorType.SUCCESS.getResponseMessage());
		fields.put(FieldType.STATUS.getName(), StatusType.SENT_TO_BANK.getName());

	}

}
