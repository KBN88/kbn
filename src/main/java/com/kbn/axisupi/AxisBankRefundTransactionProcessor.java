package com.kbn.axisupi;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.TransactionProcessor;

public class AxisBankRefundTransactionProcessor implements TransactionProcessor {
	private static Logger logger = Logger.getLogger(AxisBankRefundTransactionProcessor.class.getName());

	@Override
	public void transact(Fields fields) throws SystemException {
		TransactionConverter converter = new TransactionConverter();
		TransactionCommunicator communicator = new TransactionCommunicator();
		String request = converter.createRefundTransaction(fields);
		logger.info("Refund Request to Axis UPI: " + request);
		String response = communicator.transactRefundAPI(request,ConfigurationConstants.AXIS_REFUND_URL.getValue());
		AxisBankTransformer transformer = new AxisBankTransformer(response);
		transformer.updateAxisBankRefundResponse(fields);

	}

}
