package com.kbn.axisupi;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.TransactionType;
import com.kbn.pg.core.AbstractTransactionProcessorFactory;
import com.kbn.pg.core.TransactionProcessor;

public class AxisBankTransactionProcessorFactory implements AbstractTransactionProcessorFactory {
	private TransactionProcessor transactionProcessor;

	@Override
	public TransactionProcessor getInstance(Fields fields) throws SystemException {
		switch (TransactionType.getInstance(fields.get(FieldType.TXNTYPE.getName()))) {
		case REFUND:
			transactionProcessor = new AxisBankRefundTransactionProcessor();
			break;
		case SALE:
		case AUTHORISE:
		case ENROLL:
			transactionProcessor = new SaleAxisBankTransactionProcessor();
			break;
		case STATUS:
			transactionProcessor = new StatusAxisTransactionProcessor();
			break;
		default:
			throw new SystemException(ErrorType.ACQUIRER_ERROR, "Unsupported transaction type for AxisBank");
		}
		return transactionProcessor;
	}

}
