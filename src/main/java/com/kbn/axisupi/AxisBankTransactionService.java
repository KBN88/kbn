package com.kbn.axisupi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kbn.commons.dao.DataAccessObject;
import com.kbn.commons.dao.HibernateAbstractDao;
import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;

public class AxisBankTransactionService extends HibernateAbstractDao {

	private static final String orderIdQuery = "SELECT RESPONSE_CODE,AMOUNT,CUST_EMAIL,TXN_ID,ORIG_TXN_ID,ORDER_ID,PAY_ID,TXNTYPE ,ACQUIRER_TYPE,MOP_TYPE,PAYMENT_TYPE,CURRENCY_CODE,STATUS,CREATE_DATE,CUST_NAME,RESPONSE_MESSAGE,RRN from TRANSACTION where TXN_ID=? and TXNTYPE='SALE'";
	private Logger logger = Logger.getLogger(AxisBankTransactionService.class.getName());

	private Connection getConnection() throws SQLException {
		return DataAccessObject.getBasicConnection();
	}

	public Map<String, String> fetchOrderID(String txnID) throws SystemException {
		Map<String, String> requestMap = new HashMap<String, String>();
		try (Connection connection = getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(orderIdQuery)) {
				preparedStatement.setString(1, txnID);
				try (ResultSet resultSet = preparedStatement.executeQuery()) {
					while (resultSet.next()) {
						requestMap.put("TXN_ID", resultSet.getString("TXN_ID"));
						requestMap.put("ORIG_TXN_ID", resultSet.getString("ORIG_TXN_ID"));
						requestMap.put("PAY_ID", resultSet.getString("PAY_ID"));
						requestMap.put("TXNTYPE", resultSet.getString("TXNTYPE"));
						requestMap.put("ACQUIRER_TYPE", resultSet.getString("ACQUIRER_TYPE"));
						requestMap.put("MOP_TYPE", resultSet.getString("MOP_TYPE"));
						requestMap.put("PAYMENT_TYPE", resultSet.getString("PAYMENT_TYPE"));
						requestMap.put("CURRENCY_CODE", resultSet.getString("CURRENCY_CODE"));
						requestMap.put("STATUS", resultSet.getString("STATUS"));
						requestMap.put("ORDER_ID", resultSet.getString("ORDER_ID"));
						requestMap.put("AMOUNT", resultSet.getString("AMOUNT"));
						requestMap.put("RESPONSE_CODE", resultSet.getString("RESPONSE_CODE"));
						requestMap.put("RESPONSE_DATE_TIME", resultSet.getString("CREATE_DATE"));
						requestMap.put("CUST_NAME", resultSet.getString("CUST_NAME"));
						requestMap.put("RESPONSE_MESSAGE", resultSet.getString("RESPONSE_MESSAGE"));
						requestMap.put("CUST_EMAIL", resultSet.getString("CUST_EMAIL"));
						requestMap.put("RRN", resultSet.getString("RRN"));

					}
				}
			}
		} catch (SQLException sqlException) {
			logger.error("Database error while fetching Original Transaction Id for Axis Manual status enquery "
					+ sqlException);
			throw new SystemException(ErrorType.DATABASE_ERROR, ErrorType.DATABASE_ERROR.getResponseMessage());
		}finally {
			autoClose();
		}
		logger.info("Get Field object" + requestMap);
		return requestMap;
	}

}
