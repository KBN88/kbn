package com.kbn.axisupi;

public class Constants {
	public static final String OPENING_BRACE = "{";
	public static final String CLOSING_BRACE = "}";
	public static final String ADDITION = "+";

	public static final String AMOUNT = "amount";
	public static final String CURRENCY_CODE = "currency";
	public static final String MERCHANT_ID = "merchId";
	public static final String MERCHCHAN_ID = "merchChanId";	
	public static final String UNQTXNID = "unqTxnId";
	public static final String UNQCUSTID = "unqCustId";
	public static final String TXNDTL = "txnDtl";
	public static final String ORDER_ID = "orderId";
	public static final String EXPIRYTYPE = "expiry";
	public static final String CUSTOMERVPA = "customerVpa";
	
	public static final String S_ID ="sId";
	public static final String CHECK_SUM ="checkSum";
	public static final String MOB_NO ="mobNo";
	public static final String MOBILE_NUMBER ="mobilenumber";
	public static final String TXN_REFUND_ID ="txnRefundId";
	public static final String TXN_REFUND_AMOUNT = "txnRefundAmount";
	public static final String REFUND_REASON = "refundReason";
	public static final String TRAN_ID = "tranid";
    // Axis Upi status Fields	
	public static final String MERCHANTID = "merchid";
	public static final String MERCHCHANID = "merchchanid";
	public static final String CHECK_SUM_STATUS ="checksum";
	
	
	

}
