package com.kbn.axisupi;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.exception.SystemException;
import com.kbn.commons.util.ConfigurationConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.StatusType;
import com.kbn.pg.core.Amount;

public class AxisBankTransformer {
	private static Logger logger = Logger.getLogger(AxisBankTransformer.class.getName());
	private String responseMap;

	public AxisBankTransformer(String responseField) {
		this.responseMap = responseField;

	}

	public void axisCollectToken() throws SystemException {
		TransactionCommunicator communicator = new TransactionCommunicator();
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(responseMap);
		} catch (ParseException exception) {
			exception.printStackTrace();
		}
		JSONObject jObject = (JSONObject) obj;
		logger.info("Axis UPI Collect Response:" + jObject);
		String code = jObject.get("code").toString();
		String result = jObject.get("result").toString();
		if (code.equalsIgnoreCase("000") || result.equalsIgnoreCase("Success")) {
			String data = jObject.get("data").toString();
			communicator.collectRequest(data, ConfigurationConstants.AXIS_TRANSACTION_COLLECT_URL.getValue());
		} else if (code.equalsIgnoreCase("222")) {
			axisCollectToken();
			// status="pending";
		} else if (code.equalsIgnoreCase("303")) {
			// status="Rejected";

		}

	}

//{"customerVpa":"bhartitest@axis","merchantId":"BHARTIUAT23125216446","merchantChannelId":"BHARTIUATAPP23125216446",
	// "merchantTransactionId":"1910041135411001","transactionTimestamp":"2019-10-04T11:37:14+05:30","transactionAmount":"1.00",
	// "gatewayTransactionId":"AXI6b2eb627fc8745039f968b71ee19a4a1","gatewayResponseCode":"00","gatewayResponseMessage":"Success",
	// "rrn":"927711694726","checksum":"5EA495E3777337EC66AA9AAA2FE957731B3564EC719568F9D4F32C256A0EED8B894BDE826DD4FB32879C21FED9E9DCDF6C5D84C77C39B4F81E9891F57D21D3CC279DE89970016B5E59B566B672CD717B3763935C06C6593F1D2C8086EBBB6AF8AC7D24BD1ACCB55A6DFFD06119E762FE45BED68584141EBEABFA413A5FB4CE36AFB2A68863EA0BDE097A82C7F641CD2C4B8F84957A85626294F3E640E2B8C43D8EA8C6308B7080A918B488EE64611711D441BCCF01875483D40ECF3A4FD191CAB4AFC2B7CD5CFA60346C556FC4AABC6FE08004743E3036942AE8FE769A88CE27C5453EE5F1A0E669E48ABD1804A9E17B34A301882AF2C6E012E060156E0452F1"}
	public void updateAxisBankResponse(Fields fields) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(responseMap);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jObject = (JSONObject) obj;

		StatusType status = getSaleStatus(jObject.get("gatewayResponseCode").toString(),
				jObject.get("gatewayResponseMessage").toString());
		ErrorType errorType = getSaleResponse(jObject.get("gatewayResponseCode").toString(),
				jObject.get("gatewayResponseMessage").toString());
		fields.put(FieldType.PG_TXN_MESSAGE.getName(), jObject.get("gatewayResponseMessage").toString());
		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getCode());
		fields.put(FieldType.PG_REF_NUM.getName(), jObject.get("gatewayTransactionId").toString());
		fields.put(FieldType.RRN.getName(), jObject.get("rrn").toString());
		fields.put(FieldType.PG_RESP_CODE.getName(), jObject.get("gatewayResponseCode").toString());
		fields.put(FieldType.PG_DATE_TIME.getName(), jObject.get("transactionTimestamp").toString());
		fields.put(FieldType.MERCHANT_ID.getName(), jObject.get("merchantId").toString());
		fields.put(FieldType.AMOUNT.getName(), Amount.formatAmount((jObject.get("transactionAmount").toString()),
				fields.get(FieldType.CURRENCY_CODE.getName())));

	}

	private ErrorType getSaleResponse(String respCode, String result) {

		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("00") || result.equalsIgnoreCase("Success")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("303")) {
			errorType = ErrorType.REJECTED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;

	}

	private StatusType getSaleStatus(String respCode, String result) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("00") || result.equalsIgnoreCase("Success")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("303")) {
			status = StatusType.REJECTED;
		} else {
			status = StatusType.FAILED;
		}

		return status;
	}

	public void updateAxisBankRefundResponse(Fields fields) {

	}

	public void updateAxisBankStatusResponse(Fields fields) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(responseMap);
		} catch (ParseException exception) {
			exception.printStackTrace();
		}
		JSONObject jObject = (JSONObject) obj;

		String Data = jObject.get("data").toString();
		// JSONArray Value
		JSONArray json = null;
		try {
			json = (JSONArray) parser.parse(Data);
		} catch (ParseException exception) {

			exception.printStackTrace();
		}
		JSONObject jsobject = (org.json.simple.JSONObject) json.get(0);

		StatusType status = getStatus(jsobject.get("code").toString(), jsobject.get("result").toString());
		ErrorType errorType = getResponse(jsobject.get("code").toString(), jsobject.get("result").toString());

		fields.put(FieldType.STATUS.getName(), status.getName());
		fields.put(FieldType.RESPONSE_MESSAGE.getName(), errorType.getResponseMessage());
		fields.put(FieldType.RESPONSE_CODE.getName(), errorType.getCode());
		fields.put(FieldType.PG_DATE_TIME.getName(), jsobject.get("dateTime").toString());
		fields.put(FieldType.PG_REF_NUM.getName(), jsobject.get("txnid").toString());
		fields.put(FieldType.RRN.getName(), jsobject.get("refid").toString());
		fields.put(FieldType.UPI.getName(), jsobject.get("debitVpa").toString());
		fields.put(FieldType.AMOUNT.getName(), Amount.formatAmount((jsobject.get("amount").toString()),
				fields.get(FieldType.CURRENCY_CODE.getName())));

	}

	private ErrorType getResponse(String respCode, String result) {
		ErrorType errorType = null;

		if (null == respCode) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("00") || result.equalsIgnoreCase("S")) {
			errorType = ErrorType.SUCCESS;
		} else if (respCode.equals("01") || result.equalsIgnoreCase("P")) {
			errorType = ErrorType.REJECTED;
		} else if (respCode.equals("U48") || result.equalsIgnoreCase("F")) {
			errorType = ErrorType.FAILED_AT_ACQUIRER;
		} else if (respCode.equals("U69") || result.equalsIgnoreCase("E")) {
			errorType = ErrorType.ACQUIRER_ERROR;
		} else if (respCode.equals("ZA") || result.equalsIgnoreCase("R")) {
			errorType = ErrorType.REJECTED;
		} else {
			errorType = ErrorType.ACQUIRER_ERROR;
		}

		return errorType;
	}

	private StatusType getStatus(String respCode, String result) {
		StatusType status = null;

		if (null == respCode) {
			status = StatusType.ERROR;
		} else if (respCode.equals("00") || result.equalsIgnoreCase("S")) {
			status = StatusType.CAPTURED;
		} else if (respCode.equals("01") || result.equalsIgnoreCase("P")) {
			status = StatusType.REJECTED;
		} else if (respCode.equals("U48") || result.equalsIgnoreCase("F")) {
			status = StatusType.FAILED;
		} else if (respCode.equals("U69") || result.equalsIgnoreCase("E")) {
			status = StatusType.ERROR;
		} else if (respCode.equals("ZA") || result.equalsIgnoreCase("R")) {
			status = StatusType.REJECTED;
		} else {
			status = StatusType.FAILED;
		}

		return status;
	}

}
