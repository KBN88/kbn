package com.kbn.axisupi;

import org.apache.log4j.Logger;

import com.kbn.commons.crypto.AxisEncrypt;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.pg.core.Amount;
import com.kbn.pg.core.pageintegrator.Transaction;

public class TransactionConverter extends Transaction {

	private static Logger logger = Logger.getLogger(TransactionConverter.class.getName());

	public String createSaleTransaction(Fields fields) {
		StringBuilder request = new StringBuilder();
		appendUPIInfo(fields, request);
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);
		logger.info("Axis Bank Upi request:" + jsonRequest.toString());
		return jsonRequest.toString();
	}

	private void appendUPIInfo(Fields fields, StringBuilder request) {
		String CheckSum = null;
		String hashRequest = addCheckSum(fields);
		try {
			CheckSum = AxisEncrypt.encryptKEY2(hashRequest.toString(),
					"D:\\IntergrationKit\\AxixUPI\\BHARTIPAYkey.txt");
		} catch (Exception exception) {
			logger.error("Axis Bank Hash :" + exception);
		}

		appendJsonField(Constants.MERCHANT_ID, fields.get(FieldType.MERCHANT_ID.getName()), request);
		appendJsonField(Constants.MERCHCHAN_ID, fields.get(FieldType.TXN_KEY.getName()), request);
		appendJsonField(Constants.UNQTXNID, fields.get(FieldType.TXN_ID.getName()), request);
		appendJsonField(Constants.UNQCUSTID, fields.get(FieldType.TXN_ID.getName()), request);
		appendJsonField(Constants.AMOUNT,
				Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()), fields.get(FieldType.CURRENCY_CODE.getName())),
				request);
		appendJsonField(Constants.TXNDTL, "GRT", request);
		appendJsonField(Constants.CURRENCY_CODE, "INR", request);
		appendJsonField(Constants.ORDER_ID, fields.get(FieldType.ORDER_ID.getName()), request);
		appendJsonField(Constants.CUSTOMERVPA, fields.get(FieldType.UPI.getName()), request);
		appendJsonField(Constants.EXPIRYTYPE, "20", request);
		appendJsonField(Constants.S_ID, "123", request);
		appendJsonField(Constants.CHECK_SUM, CheckSum, request);

	}

	private String addCheckSum(Fields fields) {
		String checksum = "";
		String merchId = fields.get(FieldType.MERCHANT_ID.getName());
		String merchChanId = fields.get(FieldType.TXN_KEY.getName());
		String unqTxnId = fields.get(FieldType.TXN_ID.getName());
		String unqCustId = fields.get(FieldType.TXN_ID.getName());
		String amount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));
		String txnDtl = "GRT";
		String currency = "INR";
		String orderId = fields.get(FieldType.ORDER_ID.getName());
		String customerVpa = fields.get(FieldType.UPI.getName());
		String expiry = "20";
		String sId = "123";
		checksum = (merchId+merchChanId+unqTxnId+unqCustId+amount+txnDtl+currency+orderId+customerVpa+expiry+sId);

		return checksum;
	}
// Refund API
	public String createRefundTransaction(Fields fields) {
		StringBuilder request = new StringBuilder();
		appendRefundAPI(fields, request);
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);
		logger.info("Axis Bank Refund Upi request:" + jsonRequest.toString());
		return jsonRequest.toString();
	}

	private void appendRefundAPI(Fields fields, StringBuilder request) {
		String CheckSum = null;
		String hashRequest = addRefundCheckSum(fields);
		try {
			CheckSum = AxisEncrypt.encryptKEY2(hashRequest.toString(),"D:\\IntergrationKit\\AxixUPI\\BHARTIPAYkey.txt");
		} catch (Exception exception) {
			logger.error("Axis Bank Hash :" + exception);
		}

		appendJsonField(Constants.MERCHANT_ID, fields.get(FieldType.MERCHANT_ID.getName()), request);
		appendJsonField(Constants.MERCHCHAN_ID, fields.get(FieldType.TXN_KEY.getName()), request);
		appendJsonField(Constants.UNQTXNID, fields.get(FieldType.TXN_ID.getName()), request);
		appendJsonField(Constants.MOB_NO, "918860705801", request);
		appendJsonField(Constants.TXN_REFUND_AMOUNT,
				Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()), fields.get(FieldType.CURRENCY_CODE.getName())),
				request);
		appendJsonField(Constants.TXN_REFUND_ID, fields.get(FieldType.TXN_ID.getName()), request);
		appendJsonField(Constants.REFUND_REASON, "not insterested", request);
		appendJsonField(Constants.S_ID, "123", request);
		appendJsonField(Constants.CHECK_SUM, CheckSum, request);

	}

	private String addRefundCheckSum(Fields fields) {
		String checksum = "";
		String merchId = fields.get(FieldType.MERCHANT_ID.getName());
		String merchChanId = fields.get(FieldType.TXN_KEY.getName());
		String txnRefundId = fields.get(FieldType.TXN_ID.getName());
		String mobNo = "918860705801";
		String txnRefundAmount = Amount.toDecimal(fields.get(FieldType.AMOUNT.getName()),
				fields.get(FieldType.CURRENCY_CODE.getName()));
		String unqTxnId = fields.get(FieldType.TXN_ID.getName());
		String refundReason = "not insterested";
		String sId = "123";
		checksum = (merchId+merchChanId+txnRefundId+mobNo+txnRefundAmount+unqTxnId+refundReason+sId);
		return checksum;
	}
	
	
	public String createStatusTransaction(Fields fields) {
		StringBuilder request = new StringBuilder();
		appendSatusAPI(fields, request);
		StringBuilder jsonRequest = new StringBuilder();
		jsonRequest.append(Constants.OPENING_BRACE);
		jsonRequest.append(request);
		jsonRequest.append(Constants.CLOSING_BRACE);
		logger.info("Axis Bank Status Upi request:" + jsonRequest.toString());
		return jsonRequest.toString();
	}

	private void appendSatusAPI(Fields fields, StringBuilder request) {
		String CheckSum = null;
		String hashRequest = addStatusCheckSum(fields);
		try {
			CheckSum = AxisEncrypt.encryptKEY2(hashRequest.toString(),
					"D:\\IntergrationKit\\AxixUPI\\BHARTIPAYkey.txt");
		} catch (Exception exception) {
			logger.error("Axis Bank Hash :" + exception);
		}
		appendJsonField(Constants.MERCHANTID, fields.get(FieldType.MERCHANT_ID.getName()), request);
		appendJsonField(Constants.MERCHCHANID, fields.get(FieldType.TXN_KEY.getName()), request);
		appendJsonField(Constants.TRAN_ID, fields.get(FieldType.TXN_ID.getName()), request);
		appendJsonField(Constants.MOBILE_NUMBER, "918605456414", request);
		appendJsonField(Constants.CHECK_SUM_STATUS, CheckSum, request);
		
	}

	private String addStatusCheckSum(Fields fields) {
		String checksum = "";
		String merchid = fields.get(FieldType.MERCHANT_ID.getName());
		String merchchanid = fields.get(FieldType.TXN_KEY.getName());
		String tranid = fields.get(FieldType.TXN_ID.getName());
		String mobilenumber = "918605456414";
		checksum = (merchid+merchchanid+tranid+mobilenumber);
		return checksum;
	}
	
}
