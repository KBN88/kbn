package com.kbn.axisupi;

import com.kbn.pg.core.Processor;

public class AxisBankUpiProcessorFactory {
	
	public static Processor getInstance() {

		return new AxisBankProcessor();
	}

}
