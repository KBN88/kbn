package com.kbn.emailer;

import org.apache.log4j.Logger;

import com.kbn.commons.util.CrmFieldType;
import com.kbn.commons.util.EmailerConstants;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.pg.core.Amount;

/*
 @author Sunil 
 */
public class EmailBodyCreator {
	private static Logger logger = Logger.getLogger(EmailBodyCreator.class
			.getName());

	private String body;
	private PropertiesManager propertiesManager = new PropertiesManager();
	private StringBuilder content = new StringBuilder();
	
	public String bodyTransactionEmail(Fields responseMap, String heading,String message) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear  " + heading + ",</td>");
			content.append("<br /><br />" + message + "</em></p>");
			content.append("<p><table width='96%' border='0' align='center' cellpadding='0' cellspacing='0' class='product-spec'>");
			content.append("<tr>");
			content.append("<th colspan='2' align='left' valign='middle'>Payment Summary</th>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td width='27%'>Payment Amount :</td>");
			content.append("<td width='73%'>"
					+ Amount.toDecimal(responseMap.get("AMOUNT"),
							responseMap.get("CURRENCY_CODE")) + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Order ID:</td>");
			content.append("<td>" + responseMap.get("ORDER_ID") + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Transaction ID: 	</td>");
			content.append("<td>" + responseMap.get("TXN_ID") + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Transaction Status:</td>");
			content.append("<td>" + responseMap.get("STATUS") + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Transaction Response:</td>");
			content.append("<td>" + responseMap.get("RESPONSE_MESSAGE")
					+ "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Transaction Date &amp; Time:</td>");
			content.append("<td>" + responseMap.get("RESPONSE_DATE_TIME")
					+ "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Customer Name:</td>");
			content.append("<td>" + responseMap.get("CUST_NAME") + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Customer Email:</td>");
			content.append("<td>" + responseMap.get("CUST_EMAIL") + "</td>");
			content.append("</tr>");
			content.append("</table>");
			content.append("</p>");
			makeFooter();
			body = content.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String transactionFailled(Fields responseMap) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear Merchant,<br />");
			content.append("<br />");
			content.append("Thank you for paying with ");
			content.append(EmailerConstants.GATEWAY.getValue());
			content.append(". Your Payment has been Failled. If you wish to pay again for the services please click on the below given button, which will redirect you to our payment page.</em></p>");
			content.append("<p><table width='20%' border='0' align='center' cellpadding='0.' cellspacing='0' class='product-spec1'>");
			content.append("<tr>");
			content.append("<td height='30' align='center' valign='middle' bgcolor='#2b6dd1'><a href='#'>Reprocess Pay</a></td>");
			content.append("</tr>");
			content.append("</table>");
			content.append("<p><table width='96%' border='0' align='center' cellpadding='0.' cellspacing='0' class='product-spec'>");
			content.append("<tr>");
			content.append("<th colspan='2' align='left' valign='middle'>Payment Summary</th>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td width='27%'>Payment Amount (Rs):</td>");
			content.append("<td width='73%'>"
					+ Amount.toDecimal(responseMap.get("AMOUNT"),
							responseMap.get("CURRENCY_CODE")) + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Order ID:</td>");
			content.append("<td>" + responseMap.get("ORDER_ID") + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Transaction ID: 	</td>");
			content.append("<td>" + responseMap.get("TXN_ID") + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Transaction Status:</td>");
			content.append("<td>" + responseMap.get("STATUS") + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Transaction Response:</td>");
			content.append("<td>" + responseMap.get("RESPONSE_MESSAGE")
					+ "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Transaction Date &amp; Time:</td>");
			content.append("<td>" + responseMap.get("RESPONSE_DATE_TIME")
					+ "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Customer Name:</td>");
			content.append("<td>" + responseMap.get("CUST_NAME") + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Customer Email:</td>");
			content.append("<td>" + responseMap.get("CUST_EMAIL") + "</td>");
			content.append("</tr>");
			content.append("</table>");
			content.append("</p>");
			makeFooter();

			body = content.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String bodyPaytm(Fields responseMap) {
		try {
			StringBuilder contant = new StringBuilder();
			contant.append("Dear Merchant" + "," + "\n\n");
			contant.append("Thank you for transaction with ");
			contant.append(EmailerConstants.COMPANY.getValue());
			contant.append(".\n\nPayment Amount (Rs)		: "
					+ responseMap.get("TXNAMOUNT") + "\n");
			contant.append("Transaction Auth Code	: "
					+ responseMap.get("AUTH_CODE") + "\n");
			contant.append("Transaction ID			: " + responseMap.get("TXN_ID")
					+ "\n");
			contant.append("Transaction Status		: "
					+ responseMap.get("RESPONSE_MESSAGE") + "\n");
			contant.append("Transaction Response	: "
					+ responseMap.get("RESPMSG") + "\n");
			contant.append("Transaction Date & Time	: "
					+ responseMap.get("TXNDATE") + "\n");
			contant.append("Assuring you of our best service at all times.\n");
			contant.append("In case any Further query, please do tell us at ");
			contant.append(EmailerConstants.CONTACT_US_EMAIL.getValue());
			contant.append(" or by phone at ");
			contant.append(EmailerConstants.PHONE_NO.getValue());
			contant.append(" \n\n We Care!\n");
			contant.append(EmailerConstants.GATEWAY.getValue());
			contant.append(" Team");

			body = contant.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String bodyDirecPay(Fields responseMap) {
		try {
			StringBuilder contant = new StringBuilder();
			contant.append("Dear Merchant" + "," + "\n\n");
			contant.append("Thank you for transaction with ");
			contant.append(EmailerConstants.COMPANY.getValue());
			contant.append(".\n\n");
			contant.append("Payment Amount (Rs)		: "
					+ responseMap.get("TXNAMOUNT") + "\n");
			contant.append("Transaction Auth Code	: "
					+ responseMap.get("AUTH_CODE") + "\n");
			contant.append("Transaction ID			: " + responseMap.get("TXN_ID")
					+ "\n");
			contant.append("Transaction Status		: "
					+ responseMap.get("RESPONSE_MESSAGE") + "\n");
			contant.append("Transaction Response	: "
					+ responseMap.get("RESPMSG") + "\n");
			contant.append("Transaction Date & Time	: "
					+ responseMap.get("TXNDATE") + "\n");
			contant.append("Assuring you of our best service at all times."
					+ "\n");
			contant.append("In case any Further query, please do tell us at"); 
			contant.append(EmailerConstants.CONTACT_US_EMAIL.getValue());
			contant.append(" or by phone at"); 
			contant.append(EmailerConstants.PHONE_NO.getValue());
			contant.append(" \n\n We Care!" + "\n");
			contant.append(EmailerConstants.GATEWAY.getValue());
			contant.append(" Team");

			body = contant.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String refundEmailBody(Fields fields,String senderType,String businessName) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'>");
			content.append("<p><em>Dear " + senderType +",</em></p>");
			content.append("<p><table width='96%' border='0' align='center' cellpadding='0.' cellspacing='0' class='product-spec'>");
			content.append("<tr>");
			content.append("<th colspan='2' align='left' valign='middle'>Refund Processed Successful. </th>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td width='27%'>Merchant Name :- </td>");
			content.append("<td width='73%'>"
					+ businessName
					+ " </td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Order Id :-  </td>");
			content.append("<td>"
					+ fields.get(CrmFieldType.ORDER_ID.toString()) + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Amount :- </td>");
			content.append("<td>"
					+ Amount.toDecimal(fields.get("AMOUNT"),
							fields.get("CURRENCY_CODE")) + "</td>");
			content.append("</tr>");
			content.append("</table></p>");
			makeFooter();

			body = content.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String accountValidation(String accountValidationID, String url) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear Merchant,<br /><br />");
			content.append("Welcome to ");
			content.append(EmailerConstants.GATEWAY.getValue());
			content.append(", you have been successfully registered with us.<br /><br />");
			content.append("Please click on below button to verify and activate your ");
			content.append(EmailerConstants.GATEWAY.getValue());
			content.append(" Account:<br />");
			content.append("</em></p>");
			content.append("<table width='20%' border='0' align='left' cellpadding='0.' cellspacing='0' class='product-spec1'>");
			content.append("<tr>");
			content.append("<td height='30' align='center' valign='middle' bgcolor='#2b6dd1'><a href='"
					+ url
					+ "?id="
					+ accountValidationID
					+ "'>Verify Account</a></td>");
			content.append("</tr>");
			content.append("</table>");
			content.append("<br /><br />");
			makeFooter();

			body = content.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String signupConfirmation(String businessName, String emailId,
			String mobile) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear Merchant,<br /><br />");
			content.append("Congratulations for Signing-up with ");
			content.append(EmailerConstants.GATEWAY.getValue());
			content.append(".<br /><table width='96%' border='0' align='center' cellpadding='0.' cellspacing='0' class='product-spec'>");
			content.append("<tr>");
			content.append("<th colspan='2' align='left' valign='middle'>Merchant Signup Details</th>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td width='27%'>Business  Name :- ");
			content.append("<br /></td>");
			content.append("<td width='73%'>" + businessName + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Email  Id :-</td>");
			content.append("<td> " + emailId + " </td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Phone  No :- </td>");
			content.append("<td>" + mobile + "</td>");
			content.append("</tr>");
			content.append("</table>");
			content.append("</p>");
			makeFooter();

			body = content.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}
	
	public String addUser(String firstName, String accountValidationID,
			String url) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><em><p>Dear "
					+ firstName + ",<br /><br />");
			// contant.append("There was recently a request to change the password for your account.<br /><br />");
			content.append("Please click on below button to set your account password.<br />");
			content.append("</p></em>");
			content.append("<table width='20%' border='0' align='left' cellpadding='0.' cellspacing='0' class='product-spec1'>");
			content.append("<tr>");
			content.append("<td height='30' align='center' valign='middle' bgcolor='#2b6dd1'><a href='"
					+ url
					+ "?id="
					+ accountValidationID
					+ "'>Set Password</a></td>");
			content.append("</tr>");
			content.append("</table>");
			content.append("<br />");
			content.append("<br />");
			makeFooter();

			body = content.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String passwordReset(String accountValidationID, String url) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><em><p>Dear Merchant,<br /><br />");
			content.append("There was recently a request to change the password for your account.<br /><br />");
			content.append("If you requested this password change, please click on below button to reset your account password.<br />");
			content.append("</p></em>");
			content.append("<table width='20%' border='0' align='left' cellpadding='0.' cellspacing='0' class='product-spec1'>");
			content.append("<tr>");
			content.append("<td height='30' align='center' valign='middle' bgcolor='#2b6dd1'><a href='"
					+ url
					+ "?id="
					+ accountValidationID
					+ "'>Reset Password</a></td>");
			content.append("</tr>");
			content.append("</table>");
			content.append("<br />");
			content.append("<br />");
			makeFooter();

			body = content.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String passwordUpdate() {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear Merchant,<br /><br />");
			content.append("Your CRM password has been updated successfully.<br />");
			content.append("</p>");
			makeFooter();
			body = content.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String invoiceLink(String url, String customerName) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear Customer,<br /><br />");
			content.append("Thanks, for giving us the opportunity to serve you.<br /><br />");
			content.append("Please click on the below link to pay:</em><br /></p>");
			content.append("<table width='20%' border='0' align='left' cellpadding='0.' cellspacing='0' class='product-spec1'>");
			content.append("<tr>");
			content.append("<td height='30' align='center' valign='middle' bgcolor='#2b6dd1'><a style='color:#fff' href='"
					+ url + "'>Click here to pay</a></td>");
			content.append("</tr>");
			content.append("</table>");
			makeFooter();
			body = content.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String documentUpload() {
		try {
			StringBuilder contant = new StringBuilder();
			contant.append("Dear Merchant," + "\n\n");
			contant.append("Your Document uploaded successfully" + "\n\n");
			contant.append("Assuring you of our best service at all times.\n");
			contant.append("In case any Further query, please do tell us at ");
			contant.append(EmailerConstants.CONTACT_US_EMAIL.getValue());
			contant.append(" or ");
			contant.append(EmailerConstants.PHONE_NO.getValue());
			contant.append(" \n\n We Care!" + "\n");
			contant.append(EmailerConstants.GATEWAY.getValue());
			contant.append(" Team");

			body = contant.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String transactionAuthenticationLink(Fields fields, String url,
			String txnId) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear Customer,<br />");
			content.append("<br />");
			content.append("Thank you for paying with ");
			content.append(EmailerConstants.GATEWAY.getValue());
			content.append(".<br /><br />");
			content.append("Please click on the below button to authenticate your recent transaction at ");
			content.append(EmailerConstants.GATEWAY.getValue());
			content.append(".</em><br />");
			content.append("<table width='20%' border='0' align='left' cellpadding='0.' cellspacing='0' class='product-spec1'>");
			content.append("<tr>");
			content.append("<td height='30' align='center' valign='middle' bgcolor='#2b6dd1'><a href='"
					+ url + "?id=" + txnId + "'>Authentication</a></td>");
			content.append("</tr>");
			content.append("</table></p>");
			content.append("<br /><br />");
			makeFooter();

			body = content.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String remittanceEmailBody(String utr, String payId,
			String merchant, String datefrom, String netAmount,
			String remittedDate, String remittedAmount, String status) {
		try {
			makeHeader();
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'>");
			content.append("<p><em>Dear Customer,</em></p>");
			content.append("<p><table width='96%' border='0' align='center' cellpadding='0.' cellspacing='0' class='product-spec'>");
			content.append("<tr>");
			content.append("<th colspan='2' align='left' valign='middle'>Remittance Processed Successful. </th>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td width='27%'>Merchant Name :- </td>");
			content.append("<td width='73%'>" + merchant + " </td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Transaction Date :-  </td>");
			content.append("<td>" + datefrom + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Remitted Amount :- </td>");
			content.append("<td>" + remittedAmount + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>Remitted Date :- </td>");
			content.append("<td>" + remittedDate + "</td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td>UTR :- </td>");
			content.append("<td>" + utr + "</td>");
			content.append("</tr>");
			content.append("</table></p>");
			makeFooter();

			body = content.toString();
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}
	private StringBuilder makeHeader(){
		
		content.append("<html xmlns='http://www.w3.org/1999/xhtml'>");
		content.append("<head>");
		content.append("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>");
		content.append("<title>Payment Acknowledgement</title>");
		content.append("<style>");
		content.append("table.product-spec{border:1px solid #eaeaea; border-bottom:1px solid");
		content.append("#dedede;font-family:Arial,Helvetica,sans-serif;background:#eeeeee}table.product-spec th{font-size:16px;");
		content.append("font-weight:bold;padding:5px;border-right:1px solid #dedede; border-bottom:1px solid #dedede; background:#0271bb;");
		content.append("color:#ffffff;}table.product-spec td{font-size:12px;padding:6px; border-right:1px solid #dedede; border-bottom:1px solid");
		content.append("#dedede;background-color:#ffffff;}");
		content.append("table.product-spec td p { font:normal 12px Arial; color:#6f6f6f; padding:0px; margin:0px; line-height:12px; }");
		content.append("</style>");
		content.append("</head>");
		content.append("<body>");
		content.append("<br /><br />");
		content.append("<table width='700' border='0' align='center' cellpadding='7' cellspacing='0' style='border:1px solid #d4d4d4;");
		content.append("background-color:#ffffff; border-radius:10px;'>");
		content.append("<tr>");
		content.append("<td height='60' align='center' valign='middle' bgcolor='#fbfbfb' style='border-bottom:1px solid #d4d4d4;");
		content.append("border-top-left-radius:10px; border-top-right-radius:10px;'><img src='"
				+ propertiesManager.getSystemProperty("emailerLogoURL")//TODO.......
				+ "' width='277' height='45' /></td>");
		content.append("</tr>");
		return content;		
	}
	 private StringBuilder makeFooter(){
		
		 	content.append("<p><em>Assuring you of our best services at all times.</em></p>");
			content.append("<p><em>If you have any questions about your transaction or any other matter, please feel free to contact us at ");
			content.append(EmailerConstants.CONTACT_US_EMAIL.getValue());
			content.append(" or by phone at ");
			 content.append(EmailerConstants.PHONE_NO.getValue());
			 content.append(".</em></p>");
			content.append("<p></p></td>");
			content.append("</tr>");
			content.append("<tr>");
			content.append("<td align='left' valign='middle' bgcolor='#fbfbfb' style='border-top:1px solid #d4d4d4; border-bottom-left-radius:10px;");
			content.append("border-bottom-right-radius:10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0.'>");
			content.append("<tr>");
			content.append("<td align='left' valign='middle' style='font-family:Arial;'><strong>We Care!<br />");
			content.append(EmailerConstants.GATEWAY.getValue());
			content.append(" Team</strong></td>");
			content.append("<td align='right' valign='bottom' style='font:normal 12px Arial;'>© 2016 ");
			content.append(EmailerConstants.WEBSITE.getValue()); 
			content.append(" All rights reserved.</td>");
			content.append("</tr>");
			content.append("</table></td>");
			content.append("</tr>");
			content.append("</table>");
			content.append("</body>");
			content.append("</html>");
			return content;
	 }

	public String serviceTaxUpdate(String emailContent) {
		try {
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear Merchant,<br /><br />");
			content.append("<table width='20%' border='0' align='left' cellpadding='0.' cellspacing='0' class='product-spec1'>");
			content.append("<tr>");
			content.append("<br /><br />" +emailContent + "</em></p>");
			content.append("</tr>");
			makeHeader();
			content.append("</table>");
			content.append("</td>");
			content.append("</tr>");
			content.append("<br /><br />");
			makeFooter();
			body = content.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
	}

	public String AttachmentFileEmail(String fileName) {
		try {
			content.append("<tr>");
			content.append("<td align='left' style='font:normal 14px Arial;'><p><em>Dear Bank,<br /><br />");
			content.append("<table width='20%' border='0' align='left' cellpadding='0.' cellspacing='0' class='product-spec1'>");
			content.append("<tr>");
			content.append("<br /><br />" +fileName + "</em></p>");
			content.append("</tr>");
			makeHeader();
			content.append("</table>");
			content.append("</td>");
			content.append("</tr>");
			content.append("<br /><br />");
			makeFooter();
			body = content.toString();

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		return body;
}
}