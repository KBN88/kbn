package com.kbn.emailer;

import org.apache.log4j.Logger;

import com.kbn.commons.exception.ErrorType;
import com.kbn.commons.user.User;
import com.kbn.commons.user.UserDao;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.EmailerConstants;
import com.kbn.commons.util.FieldType;
import com.kbn.commons.util.Fields;
import com.kbn.commons.util.PropertiesManager;
import com.kbn.crm.actionBeans.ResponseObject;
import com.kbn.jmsscheduler.JmsQueueClient;

/**
 * @author Sunil
 *
 */
public class EmailBuilder {

	private static Logger logger = Logger.getLogger(EmailBuilder.class.getName());
	private String body;
	private String subject;
	private String toEmail;
	private String emailToBcc;
	private boolean emailExceptionHandlerFlag;
	private StringBuilder responseMessage = new StringBuilder();

	// Preparing Transaction Email for Merchant and Customer 
	public void transactionEmailer(Fields fields, String senderType) throws Exception {
		String heading = null;
		String message = null;
		String sendToEmail = null;
		String defaultMerchantEmail = null;
		try {
			UserDao userDao = new UserDao();
			User user = userDao.getUserClass(fields.get(FieldType.PAY_ID.getName()));
			if (senderType.equals(UserType.MERCHANT.toString())) {
				heading = CrmFieldConstants.MERCHANT_HEADING.getValue();
				message = CrmFieldConstants.MERCHANT_MESSAGE.getValue();
				sendToEmail = user.getTransactionEmailId();
				defaultMerchantEmail = user.getEmailId();
			} else {
				heading = CrmFieldConstants.CUSTOMER_HEADING.getValue();
				message = CrmFieldConstants.CUSTOMER_MESSAGE.getValue();
				sendToEmail = fields.get(FieldType.CUST_EMAIL.getName());
			}
			// check for merchant
			if (senderType.equals(UserType.MERCHANT.toString())) {
				if (user.isTransactionEmailerFlag() == true) {
					EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
					body = emailBodyCreator.bodyTransactionEmail(fields, heading, message);
					//Emailer.sendEmail(body, message, defaultMerchantEmail, sendToEmail, isEmailExceptionHandlerFlag());
					JmsQueueClient.postMsgToJms(message, body, sendToEmail, defaultMerchantEmail,isEmailExceptionHandlerFlag());
					// NEED TO ASK TO NEERAJ ABOUT MAILTO AND BCC IS CONFUSION
				}
			}
			// check for customer
			else if (senderType.equals(CrmFieldConstants.CUSTOMER.toString())) {
				createEmail(fields, sendToEmail, heading, message);
			}

		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	private void createEmail(Fields fields, String sendToEmail, String heading, String message) throws Exception {
		EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
		if (fields.get(FieldType.RESPONSE_CODE.getName()).equals("000")) {
			body = emailBodyCreator.bodyTransactionEmail(fields, heading, message);
			subject = EmailerConstants.COMPANY.getValue() + ": Payment Received Acknowledgement";
			toEmail = sendToEmail;
			setEmailExceptionHandlerFlag(false);
			//Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc(),isEmailExceptionHandlerFlag());
		} else {
			body = emailBodyCreator.transactionFailled(fields);
			subject = EmailerConstants.COMPANY.getValue() + ": Payment Received Acknowledgement";
			toEmail = sendToEmail;
			setEmailExceptionHandlerFlag(false);
			//Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc(),isEmailExceptionHandlerFlag());
		}

	}

	// Preparing Refund Transaction Email for Customer
	public void transactionRefundEmail(Fields fields, String senderType, String getToEmail, String businessName)
			throws Exception {
		String headingRefund = null;
		try {
			EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			if (senderType.equals(UserType.MERCHANT.toString())) {
				headingRefund = CrmFieldConstants.MERCHANT_HEADING.getValue();
			} else {
				headingRefund = CrmFieldConstants.CUSTOMER_HEADING.getValue();
			}
			body = emailBodyCreator.refundEmailBody(fields, headingRefund, businessName);
			subject = EmailerConstants.COMPANY.getValue() + ": Refunded for Order Id :- "
					+ fields.get(FieldType.ORDER_ID.getName());
			toEmail = getToEmail;
			setEmailExceptionHandlerFlag(false);
			//Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc(),isEmailExceptionHandlerFlag());
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	// Preparing Email for merchant registration validation
	public void emailValidator(ResponseObject responseObject) throws Exception {
		try {
			PropertiesManager propertiesManager = new PropertiesManager();
			EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			body = emailBodyCreator.accountValidation(responseObject.getAccountValidationID(),
					propertiesManager.getEmailProperty("EmailValidatorUrl"));
			subject = "Account Validation Email -" + EmailerConstants.COMPANY.getValue();// TODO......
			toEmail = responseObject.getEmail();
			setEmailExceptionHandlerFlag(true);
			Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			//JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc(),isEmailExceptionHandlerFlag());
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	// Preparing Email for SubUser add
	public void emailAddUser(ResponseObject responseObject, String firstName) throws Exception {
		try {
			PropertiesManager propertiesManager = new PropertiesManager();
			EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			body = emailBodyCreator.addUser(firstName, responseObject.getAccountValidationID(),
					propertiesManager.getResetPasswordProperty("ResetPasswordUrl"));
			subject = "Account validation email: " + EmailerConstants.COMPANY.getValue();
			toEmail = responseObject.getEmail();
			setEmailExceptionHandlerFlag(true);
			Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			//JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc(),isEmailExceptionHandlerFlag());
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	// Preparing Email for password change to merchant
	public void emailPasswordChange(ResponseObject responseObject, String emailId) throws Exception {
		try {
			EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			body = emailBodyCreator.passwordUpdate();
			subject = EmailerConstants.COMPANY.getValue() + ": CRM password change Acknowledgement";
			toEmail = emailId;
			setEmailExceptionHandlerFlag(false);
			Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			//JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc(),isEmailExceptionHandlerFlag());
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	// Preparing Email for password reset to merchant
	public void passwordResetEmail(String accountValidationKey, String emailId) throws Exception {
		try {
			PropertiesManager propertiesManager = new PropertiesManager();
			EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			body = emailBodyCreator.passwordReset(accountValidationKey,
					propertiesManager.getResetPasswordProperty("ResetPasswordUrl"));
			subject = "Reset Password Email - " + EmailerConstants.COMPANY.getValue();
			toEmail = emailId;
			setEmailExceptionHandlerFlag(true);
			Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			//JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc());
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	// Preparing Email for invoice
	public void invoiceLinkEmail(String url, String emailId, String customerName) {
		try {

			EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			body = emailBodyCreator.invoiceLink(url, customerName);
			subject = "Invoice Link";
			toEmail = emailId;
			setEmailExceptionHandlerFlag(true);
			try{
			Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			//JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc(),isEmailExceptionHandlerFlag());
			}catch(Exception exception){
				responseMessage.append(ErrorType.EMAIL_ERROR.getResponseMessage());
				responseMessage.append(emailId);
				responseMessage.append("\n");
				logger.error("Error!! Unable to send email Emailer fail: " + exception);
				return;
			}
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	// Preparing Transaction Email
	public void transactionAuthenticationEmail(Fields fields) throws Exception {
		try {
			PropertiesManager propertiesManager = new PropertiesManager();

			EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			body = emailBodyCreator.transactionAuthenticationLink(fields,
					propertiesManager.getEmailProperty("EmailTransactionAuthenticationUrl"),
					fields.get(FieldType.TXN_ID.getName()));
			subject = EmailerConstants.COMPANY.getValue() + ": Payment Authentication Acknowledgement -"
					+ fields.get(FieldType.TXN_ID.getName());
			toEmail = fields.get(FieldType.CUST_EMAIL.getName());
			setEmailExceptionHandlerFlag(false);
			Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			//JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc(),isEmailExceptionHandlerFlag());
			// }
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	// Remittance Email to merchant
	public void remittanceProcessEmail(UserDao userDao, String utr, String payId, String merchant, String datefrom,
			String netAmount, String remittedDate, String remittedAmount, String status) {
		try {

			EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			User user = userDao.getUserClass(payId);
			body = emailBodyCreator.remittanceEmailBody(utr, payId, merchant, datefrom, netAmount, remittedDate,
					remittedAmount, status);
			subject = EmailerConstants.COMPANY.getValue() + ": Remittance Processed for :- " + payId;
			toEmail = user.getEmailId();
			setEmailExceptionHandlerFlag(false);
			Emailer.sendEmail(getBody(), getSubject(), getToEmail(), getEmailToBcc(), isEmailExceptionHandlerFlag());
			//JmsQueueClient.postMsgToJms(getSubject(), getBody(), getToEmail(), getEmailToBcc(), false);
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}

	}

	public void sendBulkEmailServiceTaxUpdate(String emailID, String subject, String messageBody) {
		try {

			EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			messageBody = emailBodyCreator.serviceTaxUpdate(messageBody);
			Emailer.sendEmail(messageBody, subject, "pgdemo@mmadpay.com", emailID, isEmailExceptionHandlerFlag()); // Sending
			//JmsQueueClient.postMsgToJms(getSubject(), getBody(), "pgdemo@mmadpay.com", emailID,false);
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
	}

	public void sendMisReport(String emailID, String subject, String fileName) {
		try {
			/*EmailBodyCreator emailBodyCreator = new EmailBodyCreator();
			fileName = emailBodyCreator.AttachmentFileEmail(fileName);*/
			Emailer.sendAttachmentFileEmail(fileName, subject, "", emailID, isEmailExceptionHandlerFlag());
		} catch (Exception exception) {
			logger.error("Exception", exception);
		}
		
	}
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getToEmail() {
		return toEmail;
	}
	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	public boolean isEmailExceptionHandlerFlag() {
		return emailExceptionHandlerFlag;
	}
	public void setEmailExceptionHandlerFlag(boolean emailExceptionHandlerFlag) {
		this.emailExceptionHandlerFlag = emailExceptionHandlerFlag;

	}
	public String getEmailToBcc() {
		return emailToBcc;
	}
	public void setEmailToBcc(String mailBcc) {
		this.emailToBcc = mailBcc;
	}
	public StringBuilder getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(StringBuilder responseMessage) {
		this.responseMessage = responseMessage;
	}
}
