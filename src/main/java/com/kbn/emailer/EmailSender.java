package com.kbn.emailer;

import org.apache.log4j.Logger;

import com.kbn.commons.user.User;
import com.kbn.commons.user.UserType;
import com.kbn.commons.util.CrmFieldConstants;
import com.kbn.commons.util.Fields;

public class EmailSender {
	private static Logger logger = Logger.getLogger(EmailSender.class.getName());

	public void postMan(Fields responseMap, String countryCode, User user) {
		// Sending Transactional email to merchant
		EmailBuilder emailBuilder = new EmailBuilder();
		try {
			emailBuilder.transactionEmailer(responseMap, UserType.MERCHANT.toString());
		} catch (Exception exception) {
			logger.info("Transactional Emailer Failed for Merchant:" + exception);
		}

		// Sending Transactional email for Authentication of customer
		if (!countryCode.equals(CrmFieldConstants.INDIA_REGION_CODE.getValue())) {

			if (user.isTransactionAuthenticationEmailFlag()) {
				try {
					emailBuilder.transactionAuthenticationEmail(responseMap);
				} catch (Exception exception) {

					logger.info("Authentication Emailer Failed for customer:" + exception);
				}
			}
		}
		// Sending Transactional Email to Customer
		if (user.isTransactionCustomerEmailFlag()) {
			try {
				emailBuilder.transactionEmailer(responseMap, CrmFieldConstants.CUSTOMER.toString());
			} catch (Exception exception) {

				logger.info("Customer Emailer Failed for customer:" + exception);
			}
		}
	}
}
