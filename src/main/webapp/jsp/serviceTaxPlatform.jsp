<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Service Tax Platform</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popup.css" />
<script type="text/javascript">

$(document).ready(function() {
	
	   $('#businessType').change(function(event){
		   var businessType = document.getElementById("businessType").value;

		   if( businessType==null||businessType=="" ){
			   document.getElementById("datatable").style.display="none";
			  return false;
			  
			 }
		   else{
			   document.getElementById("datatable").style.display="block";
			   
		   }
		   document.getElementById("serviceTaxDetailsForm").submit();
		   
	   });

});

var editMode;

function editCurrentRow(divId,curr_row,ele){
	var div = document.getElementById(divId);

	var table = div.getElementsByTagName("table")[0];

	var businessType = document.getElementById("businessType").value;
	var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
	var loginEmailId = "<s:property value='%{#session.USER.EmailId}'/>";
	
	var rows = table.rows;
	var currentRowNum = Number(curr_row);
	var currentRow = rows[currentRowNum];
	var cells = currentRow.cells;
	var cell0 = cells[0];
	var cell1 = cells[1];
	var cell2 = cells[2];
	var cell3 =  cells[3];
	
	var cell0Val = cell0.innerText;
	var cell1Val = cell1.innerText;
	var cell2Val = cell2.innerText;
	var cell3Val = cell3.innerText;

	if(ele.value=="edit"){
		if(editMode) 
		{
				alert('Please edit the current row to proceed');
				return;
		}
		ele.value="save";
		ele.className ="btn btn-success btn-xs";
		cell2.innerHTML = "<input type='number' id='cell2Val'   class='serviceTaxPlatfrom' min='1' step='0.0' value="+cell2Val+"></input>";
		editMode = true;
	}
	else{
		var businessType = cell1Val;
		var serviceTax = document.getElementById('cell2Val').value;
		var status = cell3Val;

		cell1.innerHTML = businessType;
		cell2.innerHTML = serviceTax;
		cell3.innerHTML = status;
		editMode = false;

		ele.value="edit";
		ele.className ="btn btn-info btn-xs";		
		var token  = document.getElementsByName("token")[0].value;
		$.ajax({
			type: "POST",
			url:"editServiceTax",
			data:{"businessType":cell1Val, "serviceTax":serviceTax, "status":status, "userType":userType, "loginEmailId":loginEmailId, "token":token,"struts.token.name": "token",},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				//TODO....clean values......using script to avoid page refresh
				window.location.reload();
		    },
			error:function(data){
				alert("Network error, service tax may not be saved");
			}
		});
	}
}


function cancel(curr_row,ele){
	var parentEle = ele.parentNode;
	
	if(editMode){
	 	window.location.reload();
	}
}


</script>
<style>
.product-spec input[type=text] {
	width: 35px;
}
</style>
</head>
<body>
	<s:actionmessage class="error error-new-text" />
	<div class="container-fluid">
		<div id="loading" style="text-align: center; display: none;">
			<img id="loading-image" style="width: 70px; height: 70px;"
				src="../image/sand-clock-loader.gif" alt="Sending SMS...">
		</div>

		<div id="loader-wrapper"
			style="width: 100%; height: 100%; display: none;">
			<div id="loader"></div>
		</div>



		<s:form id="serviceTaxDetailsForm" action="serviceTaxPlatformAction"
			method="post">

			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="txnf" style="margin-top: 1%;">
				<tbody>
					<tr>
						<!-- <td width="21%"><h2>Goods and Services Tax</h2></td> -->
					</tr>
					<tr>
						<td align="center" valign="top"><div class="col-md-12">
								<div class="card ">
									<div class="card-header card-header-rose card-header-text">
										<div class="card-text">
											<h4 class="card-title">Goods and Services Tax</h4>
										</div>
									</div>
									<div class="card-body ">
										<div class="container">
											<div class="row">

												<div class="col-sm-6 col-lg-3">
													<label style="float: left;">Select Industry : </label><br>
													<div class="txtnew">
														<div id="wwgrp_businessType" class="wwgrp">
															<div id="wwctrl_businessType" class="wwctrl">

																<s:select headerKey="" headerValue="Select Industry"
																	name="businessType" id="businessType"
																	list="industryCategory" class="form-control"
																	autocomplete="off" />

															</div>
														</div>
													</div>
												</div>
												<tr>
													<td align="left"><div id="datatable" class="scrollD">
															<s:iterator value="serviceTaxData" status="pay">
																<br>
																<span class="text-primary" id="test"><strong><s:property
																			value="key" /></strong></span>
																<br>
																<div class="scrollD">
																	<s:div id="%{key +'Div'}" value="key">
																		<table width="100%" border="0" align="center" style=""
																			class="product-spec">
																			<tr class="boxheading">
																				<th width="5%" height="25" valign="middle"
																					style="display: none">Payment</th>
																				<th width="4%" align="left" valign="middle">Business
																					Type</th>
																				<th width="6%" align="left" valign="middle">GST
																					</th>
																				<th width="4%" align="left" valign="middle">Status</th>
																				<th width="5%" align="left" valign="middle">Update</th>
																				<th width="2%" align="left" valign="middle"
																					style="display: none">id</th>
																				<th width="5%" align="left" valign="middle"><span
																					id="cancelLabel">Cancel</span></th>
																			</tr>
																			<s:iterator value="value" status="itStatus">
																				<tr class="boxtext">
																					<td align="left" valign="middle"
																						style="display: none"><s:property
																							value="businessType" /></td>
																					<td align="left" valign="middle"><s:property
																							value="businessType" /></td>
																					<td align="left" valign="middle"><s:property
																							value="serviceTax" /></td>
																					<td align="left" valign="middle"><s:property
																							value="status" /></td>

																					<td align="center" valign="middle"><s:div>
																							<s:textfield id="edit%{#itStatus.count}"
																								value="edit" type="button"
																								onclick="editCurrentRow('%{key +'Div'}','%{#itStatus.count}', this)"
																								class="btn btn-info btn-xs" autocomplete="off"></s:textfield>

																							<s:textfield id="cancelBtn%{#itStatus.count}"
																								value="cancel" type="button"
																								onclick="cancel('%{#itStatus.count}',this)"
																								style="display:none" autocomplete="off"></s:textfield>
																						</s:div></td>
																					<td align="center" valign="middle"
																						style="display: none"><s:property value="id" /></td>
																					<td align="center" valign="middle"><s:textfield
																							id="cancelBtn%{#itStatus.count}" value="cancel"
																							type="button"
																							onclick="cancel('%{#itStatus.count}',this)"
																							class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
																				</tr>
																			</s:iterator>
																		</table>
																	</s:div>
																</div>
															</s:iterator>
														</div></td>
												</tr>





											</div>
										</div>


									</div>
								</div>
							</div>
							<table width="98%" border="0" cellspacing="0" cellpadding="0">


								<tbody>
									<tr>
										<td align="left"><div id="datatable" class="scrollD">

											</div></td>
									</tr>
								</tbody>
							</table></td>
					</tr>


				</tbody>
			</table>


			<s:hidden name="token" value="%{#session.customToken}"></s:hidden>


		</s:form>
	</div>




</body>
</html>