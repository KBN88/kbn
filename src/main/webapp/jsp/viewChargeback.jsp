<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Chargeback</title>

<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/jquery.popupoverlay.js"></script> 
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>  
<script type="text/javascript" src="../js/pdfmake.js"></script>
<script type="text/javascript">
function handleChange() {
	reloadTable();
}

	$(document).ready(function() {
		
		$(function() {
			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});
		$(function() {
			var today = new Date();
			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			renderTable();

		});
		
		
			$(function() {
			var datepick = $.datepicker;
			var table = $('#chargebackDataTable').DataTable();
			$('#chargebackDataTable tbody').on('click', 'td', function() {
				submitForm(table, this);
				
			});
		});
	});
	
	function submitForm(table, index) {
		var rowIndex = table.cell(index).index().row;
		var caseId = table.cell(rowIndex, 0).data();
		document.getElementById('caseId').value = caseId;
		document.viewChargebackDetails.submit();
	}
	
	function renderTable() {
		var table = new $.fn.dataTable.Api('#chargebackDataTable');
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				toggleAjaxLoader();
			},
			complete : function() {
				toggleAjaxLoader();
			}
		});
		var transFrom = $.datepicker.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
       	$('#chargebackDataTable').DataTable({
			dom : 'BTftlpi',
			buttons : [ {
				extend : 'copyHtml5',
				exportOptions : {
					columns : [ ':visible' ]
				}
			}, {
				extend : 'csvHtml5',
				title : 'Chargeback',
				exportOptions : {
					columns : [ ':visible' ]
				}
			}, {
				extend : 'pdfHtml5',
				title : 'Chargeback',
				exportOptions : {
					columns : [ ':visible' ]
				}
			}, {
				extend : 'print',
				title : 'Chargeback',
				exportOptions : {
					columns : [ 0, 1, 2, 3, 4, 5, 6 ]
				}
			},{
				extend : 'colvis',
				//           collectionLayout: 'fixed two-column',
				columns : [ 1, 2, 3, 4, 5, 6,]
			} ],
			"ajax" : {
				type : "POST",
				url : "viewChargebackAction",
				data :function (d){
					return generatePostData(d);
				}
			},
			    "processing": false,
		        "paginationType": "full_numbers", 
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		        "order" : [ [ 1, "desc" ] ], 
			"columns" : [
			{
				"data" : "caseId",
				"className" : "my_class"
				
			}, {
				"data" : "orderId",
				"width" : '14%'
			}, {
				"data" : "chargebackType",
				"width" : '13%'
			}, {
				"data" : "chargebackStatus",
				"width" : '10%'
			},{
				"data" : "capturedAmount",
			
			}, {
				"data" : "targetDate",
			}, {
				"data" : "createDate"
			} ]
		});
/*  popup=dtRender;
dtRender.on( 'xhr', function () {
var data = dtRender.ajax.json();
dataSet=JSON.stringify(data);
} );  */
	}
	function generatePostData(d) {
		var token = document.getElementsByName("token")[0].value;
		var obj = {
				payId : document.getElementById("merchant").value,
				chargebackType : document.getElementById("chargebackType").value,
				chargebackStatus : document.getElementById("chargebackStatus").value,
				dateTo : document.getElementById("dateTo").value,
				dateFrom : document.getElementById("dateFrom").value,
				
			//draw : d.draw,
			//length :d.length,
			//start : d.start, 
			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}
	
	function reloadTable() {
		var datepick = $.datepicker;
		var transFrom = $.datepicker.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#chargebackDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}
</script>


</head>
<body>
<form>
<table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left"><h2>Chargeback Case</h2>
				<div class="container">


				

					<div class="form-group col-md-2 txtnew col-sm-3 col-xs-6">
						<label for="merchant">Merchant:</label> <br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'  || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
							<s:select name="merchant" class="form-control" id="merchant"
								headerKey="ALL" headerValue="ALL" list="merchantList"
								listKey="payId" listValue="businessName" onchange="handleChange();" autocomplete="off" />
						</s:if>
						<s:else>
							<s:select name="merchant" class="form-control" id="merchant"
								headerKey="ALL" headerValue="ALL" list="merchantList"
								listKey="payId" listValue="businessName" onchange="handleChange();" autocomplete="off" />
						</s:else>
					</div>


					<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
						<label for="email">Type:</label> <br />
						<s:select headerKey="ALL" headerValue="ALL" id="chargebackType"
									name="chargebackType" class="form-control"
									list="@com.kbn.chargeback.utils.ChargebackType@values()"
									listKey="code" listValue="name" onchange="handleChange();"  autocomplete="off" />
					</div>
					<div class="form-group  col-md-1 col-sm-4 txtnew  col-xs-6">
						<label for="email">Status:</label> <br />
						<s:select headerKey="ALL" headerValue="ALL" id="chargebackStatus"
									name="chargebackStatus" class="form-control"
									list="@com.kbn.chargeback.utils.ChargebackStatus@values()"
									listKey="code" listValue="name" onchange="handleChange();"  autocomplete="off" />
					</div>

				
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" readonly="true" id="dateFrom"
							name="dateFrom" class="form-control" onchange="handleChange();" 
							autocomplete="off" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" onchange="handleChange();" autocomplete="off" />
					</div>
				</div></td>
		</tr>
		<tr>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" style="padding: 10px;"><div class="scrollD">
					<table width="100%" align="left" cellpadding="0" cellspacing="0" class="txnf">

		<tr>
			<td align="left" colspan="3" style="padding: 10px;"><br> <br>
            <div class="scrollD">
				<table id="chargebackDataTable" class="display table" cellspacing="0" width="100%">
					<thead>
						<tr class="boxheadingsmall">
							<th>Case Id</th>
							<th>Order Id</th>
							<th>Type</th>
							<th>Status</th>
							 <th>Amount</th>
							<th>Target Date</th>
							<th>Create Date</th>
						</tr>
					</thead>
				</table>
                </div>
                </td>
		</tr>
</table>
				</div></td>
		</tr>
	</table>
	

</form>
<s:form name="viewChargebackDetails" action="viewChargebackDetailsAction">
		<s:hidden name="caseId" id="caseId" value="" />
		
		<s:hidden name="token" value="%{#session.customToken}" />
	</s:form>
</body>
</html>