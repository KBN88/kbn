<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html dir="ltr" lang="en-US">
<head>

<title>Transaction Results</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all"
	href="../css/daterangepicker-bs3.css" />
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<script src="../js/jquery.min.js" type="text/javascript"></script>
<script src="../js/moment.js" type="text/javascript"></script>
<script src="../js/daterangepicker.js" type="text/javascript"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/commanValidate.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script src="../js/dataTables.buttons.js" type="text/javascript"></script>
<script src="../js/pdfmake.js" type="text/javascript"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
<script src="../js/jquery.select2.js" type="text/javascript"></script>
<%-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script> --%>

<script type="text/javascript">
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}

	$(document).ready(function() {

		$(function() {
			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});

		$(function() {
			var today = new Date();
			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			renderTable();
		});

		$("#submit").click(function(env) {
			reloadTable();
		});

		$(function() {
			var datepick = $.datepicker;
			var table = $('#txnResultDataTable').DataTable();
			$('#txnResultDataTable tbody').on('click', 'td', function() {

				popup(table, this);
			});
		});
	});

	function renderTable() {
		  var merchantEmailId = document.getElementById("merchants").value;
		var table = new $.fn.dataTable.Api('#txnResultDataTable');
		//to show new loader -Harpreet
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				toggleAjaxLoader();
			},
			complete : function() {
				toggleAjaxLoader();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;

		$('#txnResultDataTable')
				.dataTable(
						{
							"footerCallback" : function(row, data, start, end,
									display) {
								var api = this.api(), data;

								// Remove the formatting to get integer data for summation
								var intVal = function(i) {
									return typeof i === 'string' ? i.replace(
											/[\,]/g, '') * 1
											: typeof i === 'number' ? i : 0;
								};

								// Total over all pages
								total = api.column(12).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(12, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(12).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');
							},
							"columnDefs": [{ className: "dt-body-right","targets": [1,2,3,4,5,6,7,8,9,10,11,12]}],
							dom : 'BTftlpi',
							buttons : [
									{
										extend : 'copyHtml5',
										//footer : true,
										exportOptions : {
											columns : [ 15, 1, 2, 3, 4,5, 7, 8, 9, 10, 11,12 ]
										}
									},
									{
										extend : 'csvHtml5',
										//footer : true,
										title : 'Search Transactions',
										exportOptions : {
											columns : [ 15, 1, 2, 3, 4,5, 7, 8, 9, 10, 11,12]
										}
									},
									{
										extend : 'pdfHtml5',
										orientation : 'landscape',
										//footer : true,
										title : 'Search Transactions',
										exportOptions : {
											columns : [ ':visible' ]
										}
									},
									{
										extend : 'print',
										//footer : true,
										title : 'Search Transactions',
										exportOptions : {
											columns : [ 0, 1, 2, 3, 4,5, 7, 8, 9, 10, 11,12]
										}
									},
									{
										extend : 'colvis',
										columns : [ 1, 2, 3, 4, 5, 6, 7, 8, 9,
												10, 15 ]
									} ],

							"ajax" : {
								"url" : "transactionSearchAction",
								"type" : "POST",
								"data" : function (d){
									return generatePostData(d);
								}
							},
							 "searching": false,
							 "processing": true,
						        "serverSide": true,
						        "paginationType": "full_numbers", 
						        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
						      //"order" : [ [ 1, "desc" ] ],
						        "order": [],
						        "columnDefs": [
						            {
						            "type": "html-num-fmt", "targets": 4,
						            "orderable": false, "targets": [0,1,2,3,4,5,6,7,8,9,10,11,12]
						            }
						        ], 

							"columns" : [ {
								"data" : "transactionId",
								"className" : "my_class",
							}, {
								"data" : "merchants"
							}, {
								"data" : "dateFrom"
							}, {
								"data" : "orderId"
							}, {
								"data" : "customerEmail"
							}, {
								"data" : "customerName",
								
							},{
								"data" : "mopType",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "paymentMethods",
								"render" : function(data, type, full) {
									return full['paymentMethods'] + ' ' + '-'
											+ ' ' + full['mopType'];
								}
							}, {
								"data" : "txnType"
							}, {
								"data" : "cardNumber"
							}, {
								"data" : "currency"
							}, {
								"data" : "status"
							}, {
								"data" : "amount"
							}, {
								"data" : "payId",
								"visible" : false
							}, {
								"data" : "productDesc",
								"visible" : false
							}, /* {
								"data" : "customerName",
								"visible" : false
							}, */ {
								"data" : null,
								"visible" : false,
								"className" : "displayNone",
								"mRender" : function(row) {
									return "\u0027" + row.transactionId;
								}
							}, {
								"data" : "internalCardIssusserBank",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "internalCardIssusserCountry",
								"visible" : false,
								"className" : "displayNone"
							} ]
						});
		$("#merchants").select2({
			//  data: merchantEmailId
			});
	}

	function reloadTable() {
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#txnResultDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}

	function generatePostData(d) {
		var token = document.getElementsByName("token")[0].value;
		var merchantEmailId = document.getElementById("merchants").value;
		if(merchantEmailId==''){
			merchantEmailId='ALL'
		}
		var obj = {
			transactionId : document.getElementById("transactionId").value,
			orderId : document.getElementById("orderId").value,
			customerEmail : document.getElementById("customerEmail").value,
			merchantEmailId : merchantEmailId,
			paymentType : document.getElementById("paymentMethods").value,
			status : document.getElementById("status").value,
			currency : document.getElementById("currency").value,
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			draw : d.draw,
			length :d.length,
			start : d.start, 
			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}

	function popup(table, index) {
		var rows = table.rows();
		var columnVisible = table.cell(index).index().columnVisible;
		if (columnVisible == 0) {

			var rowIndex = table.cell(index).index().row;
			var transactionId = table.cell(rowIndex, 0).data();
			var orderId = table.cell(rowIndex, 3).data();
			var cust_email = table.cell(rowIndex, 4).data();
			if (cust_email == null) {
				cust_email = "Not available";
			}
			var merchant = table.cell(rowIndex, 1).data();
			var amount = table.cell(rowIndex, 12).data();
			var currency = table.cell(rowIndex, 10).data();
			var paymentMethod = table.cell(rowIndex, 7).data();
			var card_number = table.cell(rowIndex, 9).data();
			var status = table.cell(rowIndex, 11).data();
			var date_from = table.cell(rowIndex, 2).data();
			var payId = table.cell(rowIndex, 13).data();
			var productDesc = table.cell(rowIndex, 14).data();
			var customerName = table.cell(rowIndex, 5).data();
			var mopType = table.cell(rowIndex, 6).data();
			var internalCardIssusserBank = table.cell(rowIndex, 16).data();
			var internalCardIssusserCountry = table.cell(rowIndex, 17).data();
			var token = document.getElementsByName("token")[0].value;
			$
					.post(
							"customerAddressAction",
							{
								orderId : decodeVal(orderId),
								custEmail : decodeVal(cust_email),
								datefrom : decodeVal(date_from),
								amount : decodeVal(amount),
								currency : decodeVal(currency),
								productDesc : decodeVal(productDesc),
								transactionId : decodeVal(transactionId),
								cardNumber : decodeVal(card_number),
								paymentMethod : decodeVal(paymentMethod),
								mopType : decodeVal(mopType),
								internalCardIssusserBank : decodeVal(internalCardIssusserBank),
								internalCardIssusserCountry : decodeVal(internalCardIssusserCountry),
								status : decodeVal(status),
								token : token,
								"struts.token.name" : "token"
							}).done(function(data) {
						var popupDiv = document.getElementById("popup");
						popupDiv.innerHTML = data;
						decodeDiv();
						$('#popup').popup('show');
					});
		}
	};

	function decodeDiv() {
		var divArray = document.getElementsByTagName('div');
		for (var i = 0; i < divArray.length; ++i) {
			var div = divArray[i];
			if (div.id.indexOf('param-') > -1) {
				var val = div.innerHTML;
				div.innerHTML = decodeVal(val);
			}
		}
	}

	function decodeVal(value) {
		var txt = document.createElement("textarea");
		txt.innerHTML = value;
		return txt.value;
	}
</script>
</head>
<body id="mainBody">
	<div id="popup"></div>
	<table id="mainTable" width="100%" border="0" align="center"
		cellpadding="0" cellspacing="0" class="txnf">
		<tr>
			<td colspan="5" align="left"><h2>Transaction Search</h2></td>
		</tr>
		<tr>
			<td colspan="5" align="left" valign="top"><div class="MerchBx">
					<div class="samefnew">
						Transaction ID:<br>
						<div class="txtnew">
							<s:textfield id="transactionId" class="form-control"
								name="transactionId" type="text" value="" autocomplete="off"
								onkeypress="javascript:return isNumber (event)" maxlength="16"></s:textfield>
						</div>
					</div>

					<div class="samefnew">
						Order ID:<br>
						<div class="txtnew">
							<s:textfield id="orderId" class="form-control" name="orderId"
								type="text" value="" autocomplete="off"
								onkeypress="return Validate(event);"></s:textfield>
						</div>
					</div>

					<div class="cust">
						Customer Email:<br>
						<div class="txtnew">
							<s:textfield id="customerEmail" class="form-control"
								name="customerEmail" type="text" value="" autocomplete="off"
								onblur="validateEmail(this);"></s:textfield>
						</div>
					</div>

					<div class="samefnew">
						Merchants:<br>
						<div class="txtnew">
							<s:if
								test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
								<s:select name="merchants" class="form-control" id="merchants"
									headerKey="" headerValue="ALL" list="merchantList"
									listKey="emailId" listValue="businessName" autocomplete="off" />
							</s:if>
							<s:else>
								<s:select name="merchants" class="form-control" id="merchants"
									headerKey="" headerValue="ALL" list="merchantList"
									listKey="emailId" listValue="businessName" autocomplete="off" />
							</s:else>
						</div>
					</div>


					<div class="cust">
						Payment Methods:<br>
						<div class="txtnew">
							<s:select headerKey="" headerValue="ALL" class="form-control"
								list="@com.kbn.commons.util.PaymentType@values()"
								listValue="name" listKey="code" name="paymentMethods"
								id="paymentMethods" autocomplete="off" value="" />
						</div>
					</div>

					<div class="samefnew">
						Status:<br>
						<div class="txtnew">
							<s:select headerKey="" headerValue="ALL" class="form-control"
								list="lst" name="status" id="status" value="name" listKey="name"
								listValue="name" autocomplete="off" />
						</div>
					</div>


					<div class="samefnew">
						Currency:<br>
						<div class="txtnew">
							<s:select name="currency" id="currency" headerValue="ALL"
								headerKey="" list="currencyMap" class="form-control" />
						</div>
					</div>

					<div class="samefnew">
						Date From:<br>
						<div class="txtnew">
							<s:textfield type="text" id="dateFrom" name="dateFrom"
								class="form-control" autocomplete="off" />
						</div>
					</div>

					<div class="samefnew">
						Date To:<br>
						<div class="txtnew">
							<s:textfield type="text" id="dateTo" name="dateTo"
								class="form-control" autocomplete="off" />
						</div>
					</div>

					<div class="samefnew">
						&nbsp;<br>
						<div class="txtnew">
							<s:submit id="submit" value="Submit"
								class="btn btn-sm btn-block btn-success" />
						</div>
					</div>
				</div></td>
		</tr>
		<tr>
			<td colspan="5" align="left"><h2>&nbsp;</h2></td>
		</tr>
		<tr>
			<td align="left" style="padding: 10px;">
				<div class="scrollD">
					<table id="txnResultDataTable" class="" cellspacing="0"
						width="100%">
						<thead>
							<tr class="boxheadingsmall">
								<th style='text-align: center'>Txn Id</th>
								<th style='text-align: center'>Merchants</th>
								<th style='text-align: center'>Date</th>
								<th style='text-align: center'>Order Id</th>
								<th style='text-align: center'>Customer Email</th>
								<th style='text-align: center'>Customer Name</th>
								<th style='text-align: center'>Payment Methods</th>
								<th style='text-align: center'>Payment Methods</th>
								<th style='text-align: center'>Txn Type</th>
								<th style='text-align: center'>Card Number</th>
								<th style='text-align: center'>Currency</th>
								<th style='text-align: center'>Status</th>
								<th style='text-align: center'>Amount</th>
								<th></th>
								<th></th>
								<th>Txn Id</th>
								<th></th>
							</tr>
						</thead>
						<tfoot>
							<tr class="boxheadingsmall">
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th style='text-align: right'>Total Amount:</th>
								<th style='text-align: right; float: right; padding-right: 8px;'></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>