<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html dir="ltr" lang="en-US">
<head>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<link href="../css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<table width="80%" align="left" cellpadding="0" cellspacing="0" class="formbox">		
		<tr>
		<td align="left" style="padding:10px;"><br /><br />
        <div class="scrollD" >
        <table id="datatable" class="display" cellspacing="0" width="80%">
		<thead>
			<tr class="boxheadingsmall">
				<th>Ticket Id</th>
				<th>Subject</th>
				<th>Ticket Type</th>
				
				<th>Ticket Status</th>
				<th>View </th>
				<th>message</th>
				<th></th>
        </tr>
		</thead>
         </table>
          </div>
    </td></tr></table>
    <s:form name ="AgentAssignedTicket" action="AgentAssignedTicket" >
    <s:hidden name = "ticketId" id= "ticketId" value = ""> </s:hidden>
     <s:hidden name = "subject" id= "subject" value = ""> </s:hidden>
     <s:hidden name = "ticketType" id= "ticketType" value = ""> </s:hidden>
     <s:hidden name = "messageBody" id= "messageBody" value = ""> </s:hidden>
   <s:hidden name="token" value="%{#session.customToken}" />
    </s:form>
    <script type="text/javascript">
    $(document).ready(function(){
    	var token  = document.getElementsByName("token")[0].value;
    	$('#datatable').DataTable({
    		dom:'lfrtip',
    		 "processing": true,
  "ajax" : {
	  "url":"agentViewTickets",
		  "type":"POST",
		  "data":{
			  token:token,
			  "struts.token.name":"token",
			  
		  }
			  
  },
  "columns" :[ {
	  "data" : "ticketId",
  	},{
	  "data" : "subject",
	  },{
		  "data" : "ticketType",
	  },{
		  "data" : "ticketStatus",
	  },{
		  "data" :null,
		  "class":"center",
		  "width":'8%',
		  "orderable": false,
		  "mRender"  : function(){
			  return '<button class="btn btn-info btn-xs">View</button>';
		  }
	  },{
			"data" : "messageBody",
			"visible" :false,
			"className" : "displayNone",
			
		} ]
    	});
    	
    });
    
    $(document).ready(function(){
    	
    	var table = $('#datatable').DataTable();
    	$('#datatable tbody').on('click','td',function(){
    		var rows = table.rows();
    		var columnVisible = table.cell(this).index().columnVisible;
    		var rowIndex = table. cell(this). index().row;
    		if(columnVisible == 4){
    			var ticketId= table.cell(rowIndex,0).data();
    			var subject = table.cell (rowIndex,1).data();
    			var ticketType = table.cell(rowIndex,2).data();
    			var messageBody = table.cell(rowIndex,5).data();
    			
    			if(ticketId.length> 20){
    				return false;
    			}
    			 document.getElementById('ticketId').value = ticketId;
    			 document.getElementById('subject').value = subject;
    			 document.getElementById('ticketType').value = ticketType;
    			 document.getElementById('messageBody').value = messageBody;
    			 document.AgentAssignedTicket.submit();
    		}
    		
    		
    		
    		
    	});
    });
    
    
    </script>

</body>
</html>