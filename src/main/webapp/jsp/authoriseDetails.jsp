<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator"%>
<%@taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<title>Authorize Details</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link href="../css/jquery-ui.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../css/popup.css" />
<script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
</script>
<script type="text/javascript">
    $(document).ready(function() {		                 	 
//    	$('#tabs') .tabs() .addClass('ui-tabs-vertical ui-helper-clearfix');

	                			  //show hide void button
		                			/* if (<s:property value="voidEnable"/>){
		                					document.getElementById('void').disabled=false;
		                				}else {
		                					document.getElementById('void').disabled=true;
		                					document.getElementById('void').className = "";
		                					document.getElementById('void').style.display = "none";
		                				} */
    });
	 $("#void").click(
	  function(evt) {
		  var confirmationFlag = confirm("Do you really want to cancel the transaction!!");
		  if(!confirmationFlag){
				event.preventDefault();
				return false;
		  }
		//to show new loader -Harpreet
			 $.ajaxSetup({
		            global: false,
		            beforeSend: function () {
		            	toggleAjaxLoader();
		            },
		            complete: function () {
		            	toggleAjaxLoader();
		            }
		        });
		  var token  = document.getElementsByName("token")[0].value;
                   $.ajax({
			 			type: "POST",
			 			url:"voidRequestAction",
			 			data : {
							origTxnId : '<s:property value="transactionSummary.transactionId"/>' ,						
							currencyCode:'<s:property value="transactionSummary.currencyCode"/>',
							amount : '<s:property value="transactionSummary.approvedAmount"/>',
							payId : '<s:property value="transactionSummary.payId"/>',
							orderId: '<s:property value="transactionSummary.orderId" />',							
							txnDate: '<s:property value="transactionSummary.date" />',
							token:token,
						    "struts.token.name": "token",
						},	
						success : function(data,status) {
							var response = data.response;
							document.getElementById('message').innerHTML = response;
							document.getElementById('message').className = "success-new success-new-text";
							document.getElementById('void').className = "";
							document.getElementById('void').style.display = "none";
						     },
						error : function(status) {
							document.getElementById('message').innerHTML = "Network exception, unable to process";
							document.getElementById('message').style.display = "block";
						    }
					});	  
  });
</script>
<script type="text/javascript">
function decodeVal(text){	
	  return $('<div/>').html(text).text();
}

$(document).ready(
		function() {
	
		 var divArray = document.getElementsByTagName('div');
			for (var i= 0; i < divArray.length; ++i) {
				var div=divArray[i];
				if(div.id.indexOf('param-') > -1){
	                  var  val = div.innerHTML;
	                  div.innerHTML = decodeVal(val);
				}
			}
			});

</script>
<style>
/* Component containers
----------------------------------*/
.ui-widget {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.1em;
}
.ui-widget .ui-widget {
	font-size: 1em;
}
.ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
	font-family: Verdana, Arial, sans-serif;
	font-size: 1em;
}
.ui-widget-content {
	border: 1px solid #aaaaaa;
	background: #ffffff url("../image/ui-bg_flat_75_ffffff_40x100.png") 50% 50% repeat-x;
	color: #222222;
}
.ui-widget-content a {
	color: #222222;
}
.ui-widget-header {
	border: 1px solid #4db0f4;
	background-color:#4db0f4;
	background-image:-webkit-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
	background-image:linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
	background-size:5px 5px;
	color: #ffffff;
	font-weight: bold;
}
.ui-widget-header a {
	color: #222222;
}
/* Interaction states
----------------------------------*/
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
	border: 1px solid #d3d3d3;
	background: #e6e6e6 url("../image/ui-bg_glass_75_e6e6e6_1x400.png") 50% 50% repeat-x;
	font-weight: normal;
	color: #555555;
}
.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited {
	color: #555555;
	text-decoration: none;
}
.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
	border: 1px solid #999999;
	background: #dadada url("../image/ui-bg_glass_75_dadada_1x400.png") 50% 50% repeat-x;
	font-weight: normal;
	color: #212121;
}
.ui-state-hover a, .ui-state-hover a:hover, .ui-state-hover a:link, .ui-state-hover a:visited, .ui-state-focus a, .ui-state-focus a:hover, .ui-state-focus a:link, .ui-state-focus a:visited {
	color: #212121;
	text-decoration: none;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
	border: 1px solid #aaaaaa;
	background: #ffffff url("../image/ui-bg_glass_65_ffffff_1x400.png") 50% 50% repeat-x;
	font-weight: normal;
	color: #212121;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
	color: #212121;
	text-decoration: none;
}
/* Interaction Cues
----------------------------------*/
.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
	border: 1px solid #fcefa1;
	background: #fbf9ee url("../image/ui-bg_glass_55_fbf9ee_1x400.png") 50% 50% repeat-x;
	color: #363636;
}
.ui-state-highlight a, .ui-widget-content .ui-state-highlight a, .ui-widget-header .ui-state-highlight a {
	color: #363636;
}
.ui-state-error, .ui-widget-content .ui-state-error, .ui-widget-header .ui-state-error {
	border: 1px solid #cd0a0a;
	background: #fef1ec url("../image/ui-bg_glass_95_fef1ec_1x400.png") 50% 50% repeat-x;
	color: #cd0a0a;
}
.ui-state-error a, .ui-widget-content .ui-state-error a, .ui-widget-header .ui-state-error a {
	color: #cd0a0a;
}
.ui-state-error-text, .ui-widget-content .ui-state-error-text, .ui-widget-header .ui-state-error-text {
	color: #cd0a0a;
}
.ui-priority-primary, .ui-widget-content .ui-priority-primary, .ui-widget-header .ui-priority-primary {
	font-weight: bold;
}
.ui-priority-secondary, .ui-widget-content .ui-priority-secondary, .ui-widget-header .ui-priority-secondary {
	opacity: .7;
	filter:Alpha(Opacity=70); /* support: IE8 */
	font-weight: normal;
}
.ui-state-disabled, .ui-widget-content .ui-state-disabled, .ui-widget-header .ui-state-disabled {
	opacity: .35;
	filter:Alpha(Opacity=35); /* support: IE8 */
	background-image: none;
}
.ui-state-disabled .ui-icon {
	filter:Alpha(Opacity=35); /* support: IE8 - See #6059 */
}
/* Icons
----------------------------------*/

/* states and images */
.ui-icon {
	width: 16px;
	height: 16px;
}
.ui-icon, .ui-widget-content .ui-icon {
	background-image: url("../image/ui-icons_222222_256x240.png");
}
.ui-widget-header .ui-icon {
	background-image: url("../image/ui-icons_222222_256x240.png");
}
.ui-state-default .ui-icon {
	background-image: url("../image/ui-icons_888888_256x240.png");
}
.ui-state-hover .ui-icon, .ui-state-focus .ui-icon {
	background-image: url("../image/ui-icons_454545_256x240.png");
}
.ui-state-active .ui-icon {
	background-image: url("../image/ui-icons_454545_256x240.png");
}
.ui-state-highlight .ui-icon {
	background-image: url("../image/ui-icons_2e83ff_256x240.png");
}
.ui-state-error .ui-icon, .ui-state-error-text .ui-icon {
	background-image: url("../image/ui-icons_cd0a0a_256x240.png");
}
</style>
</head>
<body>
<div class="scrollDR">
  <div style="width:100%; margin:0 auto;">
    <div id="fadeandscale" class="well">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="middle" class="headgpop">Transaction
            <s:property value="transactionSummary.status"/></td>
        </tr>
        <tr>
          <td align="left" valign="top"><div id="tabs"> 
              <!--   <ul>
              <li><a href="#a">Summary</a></li>
              <li><a href="#b">Additional details</a></li>
              <li><a href="#c">Authorisation details</a></li>
            </ul> -->
              <div id="a">
                <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
                    <td colspan="3" align="center" valign="top" height="8"></td>
                  </tr>
                  <tr>
                    <td valign="top" width="96%" align="center"><table width="100%" bgcolor="#dedede" cellspacing="0" cellpadding="5" class="product-spec">
                        <tr>
                          <td colspan="2" align="left" valign="middle" class="newpopuphead" style="background:#ccdce7">Customer Details</td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Customer Name:</strong></td>
                          <td bgcolor="#FFFFFF"><div id ="param-custName" >
                              <p>
                                <s:property value="%{aaData.custName}"/>
                              </p>
                            </div></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Phone:</strong></td>
                          <td bgcolor="#FFFFFF"><div id ="param-custPhone" >
                              <p>
                                <s:property value="%{aaData.custPhone}"/>
                              </p>
                            </div></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Email:</strong></td>
                          <td bgcolor="#FFFFFF"><span class="bluetext">
                            <s:property value="transactionSummary.customerEmail"/>
                            </span></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Customer Authentication:</strong></td>
                          <td bgcolor="#FFFFFF"><s:property value="transactionSummary.internalTxnAuthentication"/></td>
                        </tr>
                        <tr>
                          <td colspan="2" align="left" valign="middle" class="newpopuphead" style="background:#ccdce7">Transaction Detials</td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Amount:</strong></td>
                          <td bgcolor="#FFFFFF"><s:property value="transactionSummary.approvedAmount"/>-<s:property
										value="transactionSummary.currencyName" /></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Date:</strong></td>
                          <td bgcolor="#FFFFFF"><s:property value="transactionSummary.date"/></td>
                        </tr>
                        <tr>
                          <td width="29%" align="left" valign="middle" bgcolor="#FFFFFF"><strong>Status:</strong></td>
                          <td width="71%" bgcolor="#FFFFFF"><s:property value="transactionSummary.status"/></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Transaction ID:</strong></td>
                          <td bgcolor="#FFFFFF"><s:property value="transactionSummary.transactionId"/></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Payment Method:</strong></td>
                          <td bgcolor="#FFFFFF"><s:property value="transactionSummary.paymentMethod"/>
                            &nbsp;(
                            <s:property value="transactionSummary.mopType"/>
                            )</td>
                        </tr>
                        	<tr>
								<td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Card
										Issues Info</strong></td>
								<td bgcolor="#FFFFFF"><s:property
										value="internalCardIssusserBank" />&nbsp;(<s:property
										value="internalCardIssusserCountry" />)</td>
							</tr>
                        <tr>
                          <td colspan="2" align="left" valign="middle" class="newpopuphead" style="background:#ccdce7">Order Details</td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Order ID:</strong></td>
                          <td bgcolor="#FFFFFF"><s:property value="transactionSummary.orderId"/></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" bgcolor="#FFFFFF"><strong>Description:</strong></td>
                          <td bgcolor="#FFFFFF"><s:property value="transactionSummary.productDesc"/></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="8" colspan="3" align="center" valign="top"></td>
                  </tr>
                  <tr>
                    <td align="left"><table width="100%" border="0" cellspacing="1" cellpadding="0">
                        <tr>
                          <td width="33%" align="left" valign="top" bgcolor="#FFFFFF"><table width="98%" border="0" cellspacing="0" cellpadding="5" class="orgco1">
                              <tr>
                                <th align="left" valign="middle">Billing Address</th>
                              </tr>
                              <tr>
                                <td height="100" align="left" valign="top"><p>
                                  <div id ="param-custStreetAddress1" >
                                    <p>
                                      <s:property value="%{aaData.custStreetAddress1}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custStreetAddress2" >
                                    <p>
                                      <s:property value="%{aaData.custStreetAddress2}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custCity" >
                                    <p>
                                      <s:property value="%{aaData.custCity}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custState" >
                                    <p>
                                      <s:property value="%{aaData.custState}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custZip" >
                                    <p>
                                      <s:property value="%{aaData.custZip}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custCountry" >
                                    <p>
                                      <s:property value="%{aaData.custCountry}"/>
                                    </p>
                                  </div></td>
                              </tr>
                            </table></td>
                          <td width="33%" align="left" valign="top" bgcolor="#FFFFFF"><table width="98%" border="0" cellspacing="0" cellpadding="5" class="orgco1">
                              <tr>
                                <th align="left" valign="middle">Delivery Address</th>
                              </tr>
                              <tr>
                                <td height="100" align="left" valign="top"><p>
                                  <div id ="param-custShipStreetAddress1" >
                                    <p>
                                      <s:property value="%{aaData.custShipStreetAddress1}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custShipStreetAddress2" >
                                    <p>
                                      <s:property value="%{aaData.custShipStreetAddress2}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custShipCity" >
                                    <p>
                                      <s:property value="%{aaData.custShipCity}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custShipState" >
                                    <p>
                                      <s:property value="%{aaData.custShipState}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custShipZip" >
                                    <p>
                                      <s:property value="%{aaData.custShipZip}"/>
                                    </p>
                                  </div>
                                  <div id ="param-custShipCountry" >
                                    <p>
                                      <s:property value="%{aaData.custShipCountry}"/>
                                    </p>
                                  </div></td>
                              </tr>
                            </table></td>
                          <td width="33%" align="left" valign="top" bgcolor="#FFFFFF"><table width="98%" border="0" cellspacing="0" cellpadding="5" class="orgco1">
                              <tr>
                                <th align="left" valign="middle">Cardholder Details</th>
                              </tr>
                              <tr>
                                <td height="100" align="left" valign="top" bgcolor="#FFFFFF"><p>
                                    <s:property value="transactionSummary.cardNo"/>
                                    <br />
                                    <s:property value="custName"/>
                                    <br />
                                  </p>
                                  <s:property value="transactionSummary.paymentMethod"/>
                            &nbsp;(
                            <s:property value="transactionSummary.mopType"/>
                            )</td>
                              </tr>
                            </table></td>
                        </tr>
                   <%--      <tr>
                          <td align="right" valign="middle" colspan="4"><div id ="message"></div>
                            <s:if test="%{#session.USER.UserType.name()=='SUBUSER'}">
                              <s:if test="%{#session['USER_PERMISSION'].contains('Void/Refund')}">
                                <button class="btn btn-xs btn-success" id="void">Void</button>
                              </s:if>
                              <s:else>
                                <button class="btn btn-xs btn-success" id="void" style="display: none;"></button>
                              </s:else>
                            </s:if>
                            <s:else>
                              <button class="btn btn-xs btn-success" id="void">Void</button>
                            </s:else></td>
                        </tr> --%>
                      </table></td>
                  </tr>
                </table>
              </div>
            </div></td>
        </tr>
        <tr>
          <td align="left" valign="top"><div class="button-position2">
              <button class="popup_close closepopupbtn"></button>
            </div></td>
        </tr>
      </table>
    </div>
  </div>
</div>
</body>
</html>