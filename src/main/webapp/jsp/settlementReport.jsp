<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Settlement Report</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script type="text/javascript" src="../js/pdfmake.js"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>

<script type="text/javascript">
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	function handleChange() {
		reloadTable();
	}
	$(document).ready(function() {
		$(function() {

			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});
		$(function() {
			var today = new Date();
			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			renderTable();
		});
	});
	function renderTable() {
		var payId = document.getElementById("merchants").value;
		var table = new $.fn.dataTable.Api('#settlementReportDataTable');
		//to show new loader -Harpreet
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				toggleAjaxLoader();
			},
			complete : function() {
				toggleAjaxLoader();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#settlementReportDataTable')
				.dataTable(
						{
							"footerCallback" : function(row, data, start, end,
									display) {
								var api = this.api(), data;

								// Remove the formatting to get integer data for summation
								var intVal = function(i) {
									return typeof i === 'string' ? i.replace(
											/[\,]/g, '') * 1
											: typeof i === 'number' ? i : 0;
								};

								// Total over all pages
								total = api.column(7).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(7, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(7).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								//merchant Fix charge
								total = api.column(8).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(8, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(8).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								//TRD total Amount

								total = api.column(9).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(9, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(9).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								//Service Tax

								total = api.column(10).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(10, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(10).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');
								//Net Amount

								total = api.column(11).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(11, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(11).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

							},
							"columnDefs" : [ {
								className : "dt-body-right",
								"targets" : [1,2,3,4,5,6,7,8,9,10,11]
							} ],
							dom : 'BTftlpi',
							buttons : [
									{
										extend : 'copyHtml5',
										exportOptions : {
											columns : [ 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
										}
									},
									{
										extend : 'csvHtml5',
										title : 'Settlement Report',
										exportOptions : {
											columns : [ 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
										}
									},
									{
										extend : 'pdfHtml5',
										title : 'Settlement Report',
										orientation : 'landscape',
										exportOptions : {
											columns : [ 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
										}
									},
									{
										extend : 'print',
										title : 'Settlement Report',
										exportOptions : {
											columns : [ 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
										}
									},
									{
										extend : 'colvis',
										columns : [ 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
									} ],
							"ajax" : {
								"url" : "settlementReportAction",
								"type" : "POST",
								"data" : generatePostData
							},
							"processing" : true,
							"lengthMenu" : [ 10, 25, 50, 100 ],
							"order" : [ [ 2, "desc" ] ],
							"columns" : [
									{
										"data" : "txnId",
										"width" : '8%'
									}, {
										"data" : "txnDate",
										"width" : '9%'
									}, {
										"data" : "createDate",
										"width" : '8%'
									}, {
										"data" : "merchant",
										"width" : '8%'
									}, {
										"data" : "txnType",
										"width" : '8%'

									}, {
										"data" : "paymentMethod",
										"render" : function(data, type, full) {
											return full['paymentMethod'] + ' '
													+ '-' + ' ' + full['mop'];
										}
									}, {
										"data" : "currencyCode",
										"width" : '8%'

									}, {
										"data" : "txnAmount",
										"width" : '8%'

									}, {
										"data" : "merchantFixCharge",
										"width" : '8%'

									}, {
										"data" : "tdr",
										"width" : '8%'

									}, {
										"data" : "serviceTax",
										"width" : '8%'
									}, {
										"data" : "netAmount",
										"width" : '8%'
										 
									}, {
										"data" : null,
										"width" : '14%',
										"visible" : false,
										"mRender" : function(row) {
											return "\u0027" + row.txnId;
										}
									}, ]
						});
		$("#merchants").select2({
			//data: payId
			});
	}
	function reloadTable() {
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#settlementReportDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}
	function decodeDiv() {
		var divArray = document.getElementsByTagName('div');
		for (var i = 0; i < divArray.length; ++i) {
			var div = divArray[i];
			if (div.id.indexOf('param-') > -1) {
				var val = div.innerHTML;
				div.innerHTML = decodeVal(val);
			}
		}
	}

	function decodeVal(value) {
		var txt = document.createElement("textarea");
		txt.innerHTML = value;
		return txt.value;
	}

	function generatePostData() {
		var token = document.getElementsByName("token")[0].value;
		var payId = document.getElementById("merchants").value;
		if (payId == '') {
			payId = 'ALL'
		}
		var obj = {
			paymentType : document.getElementById("paymentMethods").value,
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			payId : payId,
			currency : document.getElementById("currency").value,
			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}
</script>
<style>
.dataTables_wrapper {
	position: relative;
	clear: both;
	*zoom: 1;
	zoom: 1;
	margin-top: -30px;
}
</style>
</head>
<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" valign="top"><table width="100%" border="0"
					align="center" cellpadding="0" cellspacing="0" class="txnf">
					<tr>
						<td colspan="5"><h2>Settlement Report</h2>
							<div class="container">


								<s:if
									test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
									<div class="form-group col-md-3 col-xs-6 col-sm-3 txtnew">

										<div class="txtnew">
											<label for="acquirer">Merchant:</label><br />
											<s:select name="merchants" class="form-control"
												id="merchants" headerKey="" headerValue="ALL"
												list="merchantList" listKey="payId"
												listValue="businessName" onchange="handleChange();" autocomplete="off" />
										</div>
									</div>
								</s:if>

								<s:else>
									<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">


										<div class="txtnew">
											<label for="acquirer">Merchant:</label><br />
											<s:select name="merchants" class="form-control"
												id="merchants" headerKey="" headerValue="ALL"
												list="merchantList" listKey="emailId"
												listValue="businessName" onchange="handleChange();" autocomplete="off" />
										</div>

									</div>
								</s:else>

								<div class="form-group  col-md-3 col-sm-4 txtnew  col-xs-6">
									<label for="email">Payment Method:</label> <br />
									<s:select headerKey="ALL" headerValue="ALL"
										class="form-control"
										list="@com.kbn.commons.util.PaymentType@values()"
										name="paymentMethods" id="paymentMethods" onchange="handleChange();" autocomplete="off"
										value="code" listKey="name" listValue="name" />
								</div>
								<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
									<label for="email">Currency:</label> <br />
									<s:select name="currency" id="currency" headerValue="ALL"
										headerKey="ALL" list="currencyMap" class="form-control" onchange="handleChange();" />
								</div>

								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateFrom">Date From:</label> <br />
									<s:textfield type="text" readonly="true" id="dateFrom"
										name="dateFrom" class="form-control" autocomplete="off"
										onchange="handleChange();" />
								</div>
								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateTo">Date To:</label> <br />
									<s:textfield type="text" readonly="true" id="dateTo"
										name="dateTo" class="form-control" onchange="handleChange();" autocomplete="off" />
								</div>
							</div></td>
					</tr>
					<tr>
						<td colspan="5" align="left" valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td align="left" valign="top" style="padding: 10px;">
							<div class="scrollD">
								<table id="settlementReportDataTable" align="center"
									cellspacing="0" width="100%">
									<thead>
										<tr class="boxheadingsmall">
											<th style='text-align: center'>Txn Id</th>
											<th style='text-align: center'>Txn Date</th>
											<th style='text-align: center'>Settlement Date</th>
											<th style='text-align: center'>Merchant</th>
											<th style='text-align: center'>Txn Type</th>											
											<th style='text-align: center'>Payment Method</th>
											<th style='text-align: center'>Currency</th>
											<th style='text-align: center'>Txn Amount</th>
											<th style='text-align: center'>Fixed Charge</th>
											<th style='text-align: center'>TDR</th>
											<th style='text-align: center'>Service Tax</th>
											<th style='text-align: center'>Net Amount</th>
                                             <th>Txn Id</th>
										</tr>
									</thead>
									<tfoot>
										<tr class="boxheadingsmall">
											<th style='text-align:left; '>Total Amount</th>
											<th style='text-align:right;'></th>
											<th style='text-align:right;'></th>
											<th style='text-align:right;'></th>
											<th style='text-align:right;'></th>
											<th style='text-align:right;'></th>
											<th style='text-align:right;  padding-right: 5px;'></th>
											<th style='text-align:right;  padding-right: 5px;'></th>
											<th style='text-align:right;  padding-right: 5px;'></th>
											<th style='text-align:right;  padding-right: 5px;'></th>
											<th style='text-align:right;  padding-right: 5px;'></th>
											<th style='text-align:right; padding-right: 5px;'></th>	
											 <th></th>								
										</tr>
									</tfoot>

								</table>
							</div>
						</td>
					</tr>
				</table></td>
		</tr>
	</table>
</body>
</html>