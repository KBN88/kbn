<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>BinRange Summary</title>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<link href="../css/font-awesome.min.css" rel="stylesheet" />

<script type="text/javascript">
	$(document).ready(function() {
		$(function() {
			renderTable();
		});
	});
	function handleChange() {
		reloadTable();
	}
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	function renderTable() {
		var token = document.getElementsByName("token")[0].value;
		$('#BinRange').dataTable({
			dom : 'BTftlpi',
			buttons : [ {
				extend : 'copyHtml5',
				title : 'BinRange',
				exportOptions : {
					columns : [ ':visible' ]
				}
			}, {
				extend : 'csvHtml5',
				title : 'BinRange',
				exportOptions : {
					columns : [ ':visible' ]
				}
			} ],
			"ajax" : {
				"url" : "binRangeAction",
				"type" : "POST",
				"data" : function(d) {
					return generatePostData(d);
				}
			},
			"searching" : false,
			"processing" : true,
			"serverSide" : true,
			"paginationType" : "full_numbers",
			"lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
			"order" : [],
			"aoColumns" : [ {
				"mData" : "binCode"
			}, {
				"mData" : "mopType"
			}, {
				"mData" : "cardType"
			}, {
				"mData" : "issuerBankName"
			}, {
				"mData" : "issuerCountry"
			}, {
				"mData" : "productName"
			} ]
		});
	}

	$(document).ready(function() {
		$('#example').DataTable({
			dom : 'B',
			buttons : [ 'csv' ]
		});
	});

	function reloadTable() {
		var tableObj = $('#BinRange');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}
	function generatePostData(d) {
		var token = document.getElementsByName("token")[0].value;
		var obj = {
			cardType : document.getElementById("paymentMethods").value,
			mopType : document.getElementById("mopType").value,
			draw : d.draw,
			length : d.length,
			start : d.start,
			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}
</script>
</head>
<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<td align="center" valign="top"><table width="100%" border="0"
				align="center" cellpadding="0" cellspacing="0" class="txnf">
				<s:actionmessage class="success success-text" />
				<tr>
				<tr>

					<td colspan="5"><h2>BinRange Details</h2>

						<div class="container">
							<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
								<label for="email">CardType:</label> <br />
								<s:select headerKey="ALL" headerValue="ALL" class="form-control"
									list="@com.kbn.commons.util.CardsType@values()"
									name="paymentMethods" id="paymentMethods"
									onchange="handleChange();" autocomplete="off" value="code"
									listKey="code" listValue="name" />
							</div>

							<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
								<label for="acquirer">MopType:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" class="form-control"
									list="@com.kbn.commons.util.BinRangeMopType@values()"
									name="mopType" id="mopType" onchange="handleChange();"
									autocomplete="off" value="code" listKey="code" listValue="name" />
							</div>
							<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
								<label for="acquirer">IssuingBank:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" class="form-control"
									list="#{'1':'HDFC'}"
									name="issuerBankName" id="issuerBankName" onchange="handleChange();"
									autocomplete="off"/>
							</div>
						</div></td>
				</tr>
				<tr>
					<td colspan="5" align="left" valign="top">&nbsp;</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding: 10px;">
						<div class="scrollD">
							<table id="BinRange" align="center" cellspacing="0" width="100%">
								<thead>
									<tr class="boxheadingsmall" style="font-size: 11px;">
										<th style='text-align: center'>binCode</th>
										<th style='text-align: center'>mopType</th>
										<th style='text-align: center'>cardType</th>
										<th style='text-align: center'>issuerBankName</th>
										<th style='text-align: center'>issuerCountry</th>
										<th style='text-align: center'>productName</th>
									</tr>
								</thead>
							</table>
						</div>
					</td>
				</tr>
			</table></td>
	</table>
	<div>
		<table width="100%" align="left" cellpadding="0" cellspacing="0"
			class="txnf">
			<tr>
				<td align="left" colspan="3"><div class="container">
						<div class="row">
							<div class="col-md-7 col-xs-12 text-left">
								<h2>Bin Range Upload</h2>
							</div>
						</div>
					</div></td>
			</tr>
		</table>
	</div>
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0" class="txnf product-spec">
		<tr>
			<th colspan="3" align="left">UPLOAD REFUND FILE</th>
		</tr>
		<tr>
			<td width="46%" height="50" align="left" valign="bottom"><table
					id="example" style="display: none;">
					<thead>
						<tr>
							<th>binCode</th>
							<th>cardType</th>
							<th>groupCode</th>
							<th>issuerBankName</th>
							<th>issuerCountry</th>
							<th>mopType</th>
							<th>productName</th>
							<th>rfu1</th>
							<th>rfu2</th>
						</tr>
					</thead>
				</table> Simple CSV File Format</td>
			<s:form action="binRangeManeger" method="POST"
				enctype="multipart/form-data">
				<script type="text/javascript">
					$("body")
							.on(
									"click",
									"#btnUpload",
									function() {
										var allowedFiles = [ ".csv" ];
										var fileUpload = $("#fileUpload");
										var lblError = $("#lblError");
										var regex = new RegExp(
												"([a-zA-Z0-9\s_\\.\-:])+("
														+ allowedFiles
																.join('|')
														+ ")$");
										if (!regex.test(fileUpload.val()
												.toLowerCase())) {
											lblError
													.html("Please upload files having extensions: <b>"
															+ allowedFiles
																	.join(', ')
															+ "</b> only.");
											return false;
										}
										lblError.html('');
										return true;
									});
				</script>
				<td width="29%" align="center" valign="middle"><div
						class="input-group">
						<span class="input-group-btn"> <input type="text"
							class="inputfieldsmall" id="fileUpload" readonly><span
							class="file-input btn btn-success btn-file btn-small"> <span
								class="glyphicon glyphicon-folder-open"></span>
								&nbsp;&nbsp;Browse <s:file name="fileName" />
						</span>
						</span><span id="lblError" style="color: red;"></span> <br />
					</div></td>
				<td width="25%" align="center" valign="middle"><s:submit
						value="Upload" name="fileName" id="btnUpload"
						class="btn btn-success btn-sm" /></td>
				<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
			</s:form>
		</tr>
	</table>
	<script>
		$(document).on(
				'change',
				'.btn-file :file',
				function() {
					var input = $(this), numFiles = input.get(0).files ? input
							.get(0).files.length : 1, label = input.val()
							.replace(/\\/g, '/').replace(/.*\//, '');
					input.trigger('fileselect', [ numFiles, label ]);
				});

		$(document)
				.ready(
						function() {
							$('.btn-file :file')
									.on(
											'fileselect',
											function(event, numFiles, label) {

												var input = $(this).parents(
														'.input-group').find(
														':text'), log = numFiles > 1 ? numFiles
														+ ' files selected'
														: label;

												if (input.length) {
													input.val(log);
												} else {
													if (log)
														alert(log);
												}

											});
						});
	
		
	</script>
</body>
</html>