<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Authorize Transaction </title>

<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../css/popup.css" />
<script src="../js/jquery.min.js"  type="text/javascript"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/dataTables.buttons.js" type="text/javascript"></script>
<script src="../js/pdfmake.js" type="text/javascript"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/authorizeReport.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script>
	$('#tabs').tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
</script>
<script type="text/javascript">
	function confirmComplete() {
		var answer = confirm("Are you sure you want to Capture the transaction?");
		if (answer != true) {
			return false;
		}
	}
	
	function decodeVal(text){	
		  return $('<div/>').html(text).text();
	}
</script>

<script type="text/javascript">
	var checkedRows = [];
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	function handleChange() {
		reloadTable();
	}
	$(document).ready(function() {
		$(function() {
			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});
		$(function() {
			renderTable();
			var selectedServer = "<s:property value='%{#session.USER.UserType.name()}'/>";			
			if (selectedServer== "SUBUSER") {
				var permission = "<s:property value='%{#session.USER_PERMISSION}'/>"
				 if (permission.indexOf("Capture") < 0) {		
					 var table = $('#authorizeDataTable').DataTable();
						table.column(9).visible( false );
				 }
				 
			}else if(selectedServer== "RESELLER" || selectedServer== "ACQUIRER" ){
				var permission = "<s:property value='%{#session.USER_PERMISSION}'/>"
					 if (permission.indexOf("Capture") < 0) {		
						 var table = $('#authorizeDataTable').DataTable();
						table.column(9).visible( false );
			}
			}else if(selectedServer== "SUBACQUIRER"){
				var permission = "<s:property value='%{#session.USER_PERMISSION}'/>"
					 if (permission.indexOf("Capture") < 0) {		
						 var table = $('#authorizeDataTable').DataTable();
						table.column(9).visible( false );
			}
			}
		});

		$(function() {
			var table = $('#authorizeDataTable').DataTable();
			$('#authorizeDataTable tbody').on('click', 'td', function() {
				showPopup(table, this);
			});
		});
		
		$("#processAllButton").click(function(env) {
			if(checkedRows.length==0){
				alert("Please select elements to proceed");
				return false;
			}
			 var retVal = confirm("Do you want to Capture Selected Transactions ?");
	         if( retVal == false ){
	          return false;
	         }
	         else{
			}
			processAll();
		});
		$("#selectAllCheckBox").click(function(env) {
			handleSelectALLClick(this);
		});
		
		$(function() {
			var table = $('#authorizeDataTable').DataTable();
			 $('#authorizeDataTable tbody').on('click', 'td', function() {
				var columnIndex = table.cell(this).index().column;
				var rowIndex = table.cell(this).index().row;
				var rowNodes = table.row(rowIndex).node();
				var rowData = table.row(rowIndex).data();
				if(columnIndex==0){
					handleSingleCheckBoxClick(rowNodes, rowData);
				}
			});

			$('#authorizeDataTable tbody').on('click', 'button[type="button"]', function() {
				var columnIndex = table.cell(this.parentNode).index().column;
				var rowIndex = table.cell(this.parentNode).index().row;
				var rowNodes = table.row(rowIndex).node();
				var rowData = table.row(rowIndex).data();
				if(columnIndex == 17){
					process(table, this.parentNode);
				}
			});

			$('#authorizeDataTable').on( 'page.dt', function (event) {
				var tableObj = event.currentTarget;
				uncheckAllCheckBoxes(tableObj);
			});
		});

	});

	function renderTable() {
		var payId = document.getElementById("merchant").value;
		var table = new $.fn.dataTable.Api('#authorizeDataTable');
		//to show new loader
		 $.ajaxSetup({
	            global: false,
	            beforeSend: function () {
	            	toggleAjaxLoader();
	            },
	            complete: function () {
	            	toggleAjaxLoader();
	            }
	        });
		
		var token = document.getElementsByName("token")[0].value;

		$('#authorizeDataTable').DataTable({
			"footerCallback" : function(row, data, start, end,
					display) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function(i) {
					return typeof i === 'string' ? i.replace(
							/[\,]/g, '') * 1
							: typeof i === 'number' ? i : 0;
				};

				// Total over all pages
				total = api.column(9).data().reduce(
						function(a, b) {
							return intVal(a) + intVal(b);
						}, 0);

				// Total over this page
				pageTotal = api.column(9, {
					page : 'current'
				}).data().reduce(function(a, b) {
					return intVal(a) + intVal(b);
				}, 0);

				// Update footer
				$(api.column(9).footer()).html(
						'' + pageTotal.toFixed(2) + ' ' + ' ');
			},
			 "columnDefs": [{ className: "dt-body-right","targets": [8]}],
			dom : 'BTftlpi',
			buttons : [ {
				extend : 'copyHtml5',
				//footer : true,
				exportOptions : {
					columns : [ 18, 2, 3, 4, 5, 6, 7, 8, 9  ]
				}
			}, {
				extend : 'csvHtml5',
				//footer : true,
				title : 'Authorized	Transactions',
				exportOptions : {
					columns : [ 18, 2, 3, 4, 5, 6, 7, 8, 9 ]
				}
			}, {
				extend : 'pdfHtml5',
				//footer : true,
				title : 'Authorized Transactions',
				orientation : 'landscape',
				exportOptions : {
					columns : [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
				}
			}, {
				extend : 'print',
				//footer : true,
				title : 'Authorized Transactions',
				exportOptions : {
					columns : [ 1, 2, 3, 4, 5, 6, 7, 8, 9  ]
				}
			}, {
				extend : 'colvis',
				columns : [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
			} ],
			"ajax" : {
				"url" : "authorizeTransactionAction",
				"type" : "POST",
				"data" : generatePostData
			},
			"processing" : true,
			"lengthMenu" : [ 10, 25, 50, 100 ],
			"order" : [ [ 1, "desc" ] ],
			"columns" : [ {
				"targets" : 0,
				"searchable" : false,
				"orderable" : false,
				"width" : '1%',
				"data":null,
				/*   'className': 'dt-body-center', */
			    'render' : function(data, type, row, meta) {
					var checkbox = ''; 
					checkbox = '<input type="checkbox" id='+row.transactionId+'>';
					return checkbox;
			}},{
				"data" : "transactionId",
				"className" : "my_class",
			}, {
				"data" : "txnDate",
				"width" : '15%'
			}, {
				"data" : "orderId",
				"width" : '10%'
			}, {
				"data" : "businessName",
				"width" : '13%'
			}, {
				"data" : "customerEmail",
				"width" : '10%'
			}, {
				"data" : "paymentMethod",
				"render" : function(data, type, full) {
					return full['paymentMethod'] + ' ' + '-'
							+ ' ' + full['mopType'];
				}
			}, {
				"data" : "cardNo",
				"visible" : true
			}, {
				"data" : "currencyName",
				"width" : '7%'
			}, {
				"data" : "approvedAmount",
				"width" : '8%'
			}, {
				"data" : null,
				"className" : "center",
				"orderable" : false,
				"render" : function() {
					return '<button class="greenrefundbtn" onclick="ajaxindicatorstart()">Capture</button>';
				}
			}, {
				"data" : "mopType",
				"visible" : false,
				"className" : "displayNone"
			}, {
				"data" : "payId",
				"visible" : false,
				"className" : "displayNone"
			}, {
				"data" : "currencyCode",
				"visible" : false,
				"className" : "displayNone"
			}, {
				"data" : "status",
				"visible" : false,
				"className" : "displayNone"
			}, {
				"data" : "customerName",
				"visible" : false,
				"className" : "displayNone"
			}, {
				"data" : "responseMsg",
				"visible" : false,
				"className" : "displayNone"
			}, {
				"data" : "productDesc",
				"visible" : false,
				"className" : "displayNone"
			},{
				"data" : null,
				"visible" : false,
				"className" : "displayNone",
				"mRender" : function(row) {
		              return "\u0027" + row.transactionId;
		       }
			} ,{
				"data" : "internalCardIssusserBank",
				"visible" : false,
				"className" : "displayNone"
			}, {
				"data" : "internalCardIssusserCountry",
				"visible" : false,
				"className" : "displayNone"
			}]
		});
		$("#merchant").select2({
			//data: payId
			});
	}

	function reloadTable(retainPageFlag) {
		$('#selectAllCheckBox').attr('checked', false);
		checkedRows = [];
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		/* if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		} */

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#authorizeDataTable');
		var table = tableObj.DataTable();
		if (retainPageFlag) {
			//set page dummy callback function
			table.ajax.reload(setPage(), false);
		} else {
			table.ajax.reload();
		}

	}
	//dummy callback
	function setPage() {
	}

	function generatePostData() {
		var token = document.getElementsByName("token")[0].value;
		var payId = document.getElementById("merchant").value;
		if (payId == '') {
			payId = 'ALL'
		}
		var obj = {
			acquirer : document.getElementById("acquirer").value,
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			paymentType : document.getElementById("paymentMethods").value,
			payId : payId,
			currency : document.getElementById("currency").value,
			token : token,
			"struts.token.name" : "token",
		};
		return obj;
	}
	function showPopup(table, index) {
		var token = document.getElementsByName("token")[0].value;
		
		var rows = table.rows();

		var columnVisible = table.cell(index).index().columnVisible;
		var columnNumber = table.cell(index).index().column;
		
		var rowIndex = table.cell(index).index().row;		
		var row = table.row(rowIndex).data();
		var payId = row.payId;

		var txnId = table.cell(rowIndex, 1).data();
		var date_from = table.cell(rowIndex, 2).data();
		var currency = table.cell(rowIndex, 8).data();
		var amount = table.cell(rowIndex, 9).data();
		var mopType = table.cell(rowIndex, 11).data();
		var orderId = table.cell(rowIndex, 3).data();
		var currencyCode = table.cell(rowIndex, 13).data();
		var status = table.cell(rowIndex, 14).data();
		var cust_email = table.cell(rowIndex, 5).data();
		if (cust_email == null){
			cust_email = "Not available";
		}
		var cust_name = table.cell(rowIndex, 15).data();
		var message = table.cell(rowIndex, 16).data();
		var card_number = table.cell(rowIndex, 7).data();
		var productDesc = table.cell(rowIndex, 17).data();
		var internalCardIssusserBank = table.cell(rowIndex, 19).data();
		var internalCardIssusserCountry = table.cell(rowIndex, 20).data();

		if (columnNumber == 10) {
			var token = document.getElementsByName("token")[0].value;
			$.ajax({
						url : 'captureConfirmAction',
						type : "POST",
						data : {
							origTxnId : decodeVal(txnId),
							amount : decodeVal(amount),
							payId : decodeVal(payId),
							orderId : decodeVal(orderId),
							currencyCode : decodeVal(currencyCode),
							mopType : decodeVal(mopType),
							token : token,
							"struts.token.name" : "token",
						},
						success : function(data) {
							var res = data.response;
							alert(res);
							//window.location.reload();
							reloadTable(true);
						},
						error : function(data) {
							alert("Capture not processed successfully please try again later");
							//window.location.reload();
						}
					});
		} else if(columnVisible == 1){
			
			$.ajax({
				url : "authorisePopupAction",
				type : "POST",
				data : {
					orderId : decodeVal(orderId),
					transactionId : decodeVal(txnId),
					custName : decodeVal(cust_name),
					date : decodeVal(date_from),
					currencyCode : decodeVal(currency),
					internalCardIssusserBank : decodeVal(internalCardIssusserBank),
					internalCardIssusserCountry : decodeVal(internalCardIssusserCountry),
					token : token,
					"struts.token.name" : "token",

				},
				success : function(data) {
					var popupDiv = document.getElementById("popup");
					popupDiv.innerHTML = data;
					decodeDiv();
					$('#popup').popup('show');
		        }		
	        });
		 }
	}

	function decodeDiv() {
		var divArray = document.getElementsByTagName('div');
		for (var i = 0; i < divArray.length; ++i) {
			var div = divArray[i];
			if (div.id.indexOf('param-') > -1) {
				var val = div.innerHTML;
				div.innerHTML = decodeVal(val);
			}
		}
	}

	function decodeVal(value) {
		var txt = document.createElement("textarea");
		txt.innerHTML = value;
		return txt.value;
	}
</script>
<style>
.dataTables_wrapper {
	position: relative;
	clear: both;
	*zoom: 1;
	zoom: 1;
	margin-top: -30px;
}
</style>
</head>

<body>
<div id="popup"></div>
	<table width="100%" align="left" cellpadding="0" cellspacing="0" class="txnf">
		<tr>
			<td align="left"><h2>Authorized
				Transactions</h2>
				<div class="container">
  
                                          
                                        <s:if
										test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
										<div class="form-group col-md-2 col-xs-6 col-sm-4 txtnew">
                                        
										<div class="txtnew">
												<label for="acquirer">Account:</label><br /><s:select headerKey="ALL" headerValue="ALL" id="acquirer"
													name="acquirer" class="form-control"
													list="@com.kbn.pg.core.AcquirerType@values()"
													listKey="code" listValue="name" onchange="handleChange();" autocomplete="off" />
											</div>
									</div></s:if>
									
									 <s:else> <div class="form-group col-md-2 col-xs-6 txtnew" style="display: none;">
									
									
										<div class="txtnew">
											<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
												name="acquirer" class=""
												list="@com.kbn.pg.core.AcquirerType@values()"
												listKey="code" listValue="name" onchange="handleChange();" autocomplete="off" />
										</div>
									
                                    </div></s:else>
                                    <div class="form-group col-md-2 txtnew col-sm-4 col-xs-6">
    <label for="merchant">Merchant:</label>
    <br />
    <s:if test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
	<s:select name="merchant" class="form-control" id="merchant" headerKey="" headerValue="ALL" list="merchantList" listKey="payId" listValue="businessName" onchange="handleChange();" autocomplete="off" />
	</s:if>
    <s:else>
	<s:select name="merchant" class="form-control" id="merchant"  headerKey="" headerValue="ALL"  list="merchantList" listKey="payId" listValue="businessName" onchange="handleChange();" autocomplete="off" />
	</s:else>
  </div>
    
  
  <div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
    <label for="email">Payment Method:</label>
    <br />
    <s:select headerKey="All" headerValue="All" class="form-control" list="@com.kbn.commons.util.PaymentType@values()" name="paymentMethods" id="paymentMethods" onchange="handleChange();" autocomplete="off" value="code" listKey="code" listValue="name" />
  </div>
  
  <div class="form-group  col-md-2 col-sm-4 txtnew col-xs-6">
    <label for="currency">Currency:</label>
    <br />
    <s:select name="currency" id="currency" headerValue="ALL" headerKey="ALL" list="currencyMap" class="form-control" onchange="handleChange();" />
  </div>
  <div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
    <label for="dateFrom">Date From:</label>
    <br />
    <s:textfield type="text" readonly="true" id="dateFrom" name="dateFrom" class="form-control" onchange="handleChange();" autocomplete="off" />
  </div>
  <div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
    <label for="dateTo">Date To:</label>
    <br />
    <s:textfield type="text" readonly="true"  id="dateTo" name="dateTo" class="form-control" onchange="handleChange();" autocomplete="off"/>
  </div>
</div>

</td>
		</tr>		
		<tr>
		  <td align="left">&nbsp;</td>
	  </tr>
		<tr>
			<td align="left" style="padding: 10px;"><div class="scrollD">
            <table
					id="authorizeDataTable" class="display" cellspacing="0"
					width="100%">
					<thead>
						<tr class="boxheadingsmall">
						<s:if test ="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER.UserType.name()=='MERCHANT'}">
											<th style='text-align: left'><input type="checkbox" name="selectAllCheckBox" id="selectAllCheckBox"></input></th>
										</s:if>
										<s:else>
										<th style='text-align: center'></th>
										</s:else>
							<th>Txn Id</th>
							<th>Date</th>
							<th>Order ID</th>
							<th>Business Name</th>
							<th>Customer Email</th>
							<th>Payment Method</th>
							<th>Card Detail</th>
							<th>Currency</th>
							<th>Txn Amount</th>
							<s:if test ="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER.UserType.name()=='MERCHANT'}">
												<th style='text-align: right'><input type="button" id="processAllButton" value="Capture All" class="greenrefundbtn" style="width:70px"></input></th>
											</s:if>
											<s:else>
												<th style='text-align: center'></th>
											</s:else>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>Txn Id</th> 
							  <th></th>
                              <th></th>
						</tr>
					</thead>
						<tfoot>
							<tr class="boxheadingsmall">
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th style='text-align:center'>Total</th>
								<th style='text-align:right; float:right; padding-right:5px;'></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
				</table>
                </div></td>
		</tr>
	</table>
</body>
</html>