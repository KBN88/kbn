<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Acquirer Routing rules</title>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/ruleAlert.js"></script>
<script type="text/javascript" src="../js/offus.js"></script>
<link href="../css/ruleCreator.css" rel="stylesheet" type="text/css" />

</head>
<script type="text/javascript">
	function mydropdown(id) {
		console.log(id)
		var element = document.getElementById(id);
		element.classList.toggle("hide");

	}
</script>
<body>
	<h1>Rules Engin</h1>
	<div>
		<div class="form-group col-md-2 col-sm-4 col-xs-12 txtnew">
			<label for="merchant">Merchant:</label> <br />
			<s:select label="Merchant" name="merchantEmailId"
				class="form-control" id="merchant" headerKey="ALL MERCHANTS"
				headerValue="ALL MERCHANTS" list="listMerchant" listKey="emailId"
				listValue="businessName" value="%{merchantEmailId}"
				autocomplete="off" />
		</div>
		<s:div style="float:left;width:100%;">

			<div class="toggleHeader" style=""
				onclick="mydropdown('onussection');">OnUs</div>

			<div id="onussection" class="hide" style="float: left; width: 100%;">
				<div id="onus_section" style="float: left; width: 100%;">

					<div class="form-group col-md-2 col-sm-4 col-xs-12 txtnew">
						<label for="merchant">Merchant:</label> <br />
						<s:select label="Merchant" name="merchantEmailId"
							class="form-control" id="onus_merchant" headerKey="ALL MERCHANTS"
							headerValue="ALL MERCHANTS" list="listMerchant" listKey="payId"
							listValue="businessName" value="%{merchantEmailId}"
							autocomplete="off" />
					</div>

					<table width="100%" border="0" align="center" class="product-spec">
						<tr class="boxheading">
							<th width="6%" align="left" valign="middle">Currency</th>
							<th width="8%" align="left" valign="middle">Transaction Type</th>
							<th width="4%" align="left" valign="middle">Mop</th>
							<th width="8%" align="left" valign="middle">Payment Type</th>
							<th width="8%" align="left" valign="middle">Acquirer</th>
							<th width="8%" align="left" valign="middle">Edit/Create</th>
						</tr>
						<tr class="boxtext">
							<td align="left" valign="top"><s:iterator
									value="currencyMap">
									<div class="checkbox">
										<label> <s:checkbox name="currency"
												fieldValue="%{value}" value="false"></s:checkbox> <s:property
												value="%{value}" />
										</label>
									</div>
								</s:iterator></td>

							<td align="left" valign="top"><s:iterator
									value="@com.kbn.commons.util.PaymentType@values()"
									status="piteratorstatus" var="payment">
									<s:div class="checkbox">
										<label><s:div>
												<s:checkbox name="paymentType" fieldValue="%{code}"></s:checkbox>
												<s:property />
											</s:div></label>
									</s:div>
								</s:iterator></td>
							<td align="left" valign="top"><s:iterator
									value="@com.kbn.commons.util.MopType@values()">
									<s:div class="checkbox">
										<label> <s:div>
												<s:checkbox name="mopType" id="%{top+'box'}"
													fieldValue="%{top}"></s:checkbox>
												<s:property />
											</s:div>
										</label>
									</s:div>
								</s:iterator></td>
							<td align="left" valign="top"><s:iterator value="transList"
									status="iteratorstatus" var="txn">
									<div class="checkbox">
										<label> <s:div>
												<s:checkbox name="txnType" fieldValue="%{top}"></s:checkbox>
												<s:property />

											</s:div>
										</label>

									</div>

								</s:iterator></td>
							<td align="left" valign="top">
								<div class="Acquirer1">

									<label> <s:radio
											list="@com.kbn.pg.core.AcquirerType@values()"
											name="Acquirer" key="Acquirer1" class="AcquirerList"
											onchange="showAdd(Acquirer1);" />
									</label>

								</div>
								<button id="addButton" onclick="addacq();"
									style="background-color: green; color: #fff;">add</button> <!-- 	<div id="wwctrl_Acquirer2">
				
				
				</div> -->

							</td>
							<td align="left" valign="top">
								<button type="submit" id="onus_submit" class="btn btn-primary"
									onclick="getOnUs()">Save</button>

								<button type="reset" class="btn btn-success" onclick="reset()">Reset</button>

							</td>
						</tr>
					</table>


					<div>

						<table width="100%" border="0" align="center"
							class="product-spec onus_table">
							<tr class="boxheading">

								<th width="100" align="left" valign="middle">Currency</th>
								<th align="left" valign="middle">Payment Type</th>
								<th align="left" valign="middle">Mop</th>
								<th align="left" valign="middle">Transaction Type</th>
								<th width="200" align="left" valign="middle">Acquirer</th>
								<th width="100" align="left" valign="middle">remove</th>
							</tr>
							<tr class="boxtext">
								<s:iterator value="routerRules">
									<s:property />
								</s:iterator>
							</tr>
						</table>
		</s:div>

		<br /> <br />


	</div>
	</div>




	<div>
		<s:div>
			<div class="toggleHeader" style=""
				onclick="mydropdown('offussection');">OFF US</div>
			<div id="offussection" style="float: left; width: 100%;">

				<div class="form-group col-md-2 col-sm-4 col-xs-12 txtnew">
					<label for="merchant">Merchant:</label> <br />
					<s:select label="Merchant" name="merchantEmailId"
						class="form-control" id="offus_merchant" headerKey="ALL MERCHANTS"
						headerValue="ALL MERCHANTS" list="listMerchant" listKey="payId"
						listValue="businessName" value="%{merchantEmailId}"
						autocomplete="off" />
				</div>
				<div id="offus_section">
					<form>
						<table width="100%" border="0" align="center" class="product-spec">
							<tr class="boxheading">
								<th width="6%" align="left" valign="middle">Currency</th>
								<th width="8%" align="left" valign="middle">Transaction
									Type</th>
								<th width="4%" align="left" valign="middle">Mop</th>
								<th width="8%" align="left" valign="middle">Payment Type</th>
								<th width="8%" align="left" valign="middle">Acquirer</th>
								<th width="8%" align="left" valign="middle">Region</th>
								<th width="8%" align="left" valign="middle">Txn Type</th>


								<th width="8%" align="left" valign="middle">Edit/Create</th>
							</tr>
							<tr class="boxtext">
								<td align="left" valign="top"><s:iterator
										value="currencyMap">
										<div class="checkbox">
											<label> <s:checkbox name="currency"
													fieldValue="%{value}" value="false"></s:checkbox> <s:property
													value="%{value}" />
											</label>
										</div>
									</s:iterator></td>

								<td align="left" valign="top"><s:iterator
										value="@com.kbn.commons.util.PaymentType@values()"
										status="piteratorstatus" var="payment">
										<s:div class="checkbox">
											<label><s:div>
													<s:checkbox name="paymentType" fieldValue="%{code}"></s:checkbox>
													<s:property />
												</s:div></label>
										</s:div>
									</s:iterator></td>
								<td align="left" valign="top"><s:iterator
										value="@com.kbn.commons.util.MopType@values()">
										<s:div class="checkbox">
											<label> <s:div>
													<s:checkbox name="mopType" id="%{top+'box'}"
														fieldValue="%{top}"></s:checkbox>
													<s:property />
												</s:div>
											</label>
										</s:div>
									</s:iterator></td>
								<td align="left" valign="top"><s:iterator value="transList"
										status="iteratorstatus" var="txn">
										<div class="checkbox">
											<label> <s:div>
													<s:checkbox name="txnType" fieldValue="%{top}"
														onclick="enableSavaButton(event);"></s:checkbox>
													<s:property />

												</s:div>
											</label>

										</div>

									</s:iterator></td>
								<td align="left" valign="top">
									<div class="Acquirer1">

										<label> <s:radio
												list="@com.kbn.pg.core.AcquirerType@values()"
												name="Acquirer" key="Acquirer1" class="AcquirerList"
												onchange="showAdd(Acquirer1);" />
										</label>

									</div>
									<button id="addButton" onclick="addacq();"
										style="background-color: green; color: #fff;">add</button> <!-- 	<div id="wwctrl_Acquirer2">
				
				
				</div> -->

								</td>

								<td align="left" valign="middle"><s:iterator
										value="{'International', 'Domestic'}" status="piteratorstatus"
										var="cardHolderType">
										<div class="checkbox">
											<label> <s:div class="stick-top">
													<s:checkbox name="cardHolderType"
														fieldValue="%{cardHolderType}" id="%{top+'box'}"></s:checkbox>
													<s:property />
												</s:div>
											</label>
										</div>
									</s:iterator></td>

								<td align="left" valign="middle"><s:iterator
										value="{'B2B', 'B2C'}" status="piteratorstatus"
										var="paymentsRegion">
										<div class="checkbox">
											<label> <s:div class="stick-top">
													<s:checkbox name="paymentsRegion"
														fieldValue="%{paymentsRegion}" id="%{top+'box'}"></s:checkbox>
													<s:property />
												</s:div>
											</label>
										</div>
									</s:iterator></td>


								<td align="left" valign="top">
									<button type="submit" id="offus_submit" class="btn btn-primary"
										onclick="getOffUs()" disabled="disabled">Save</button>

									<button type="reset" class="btn btn-success" onclick="reset()">Reset</button>

								</td>

							</tr>
						</table>
					</form>
				</div>
				<br />

				<table width="100%" border="0" align="center"
					class="product-spec offus_table">
					<tr class="boxheading">
						<th width="200" align="left" valign="middle">Acquirer</th>
						<th width="100" align="left" valign="middle">Currency</th>
						<th align="left" valign="middle">Payment Type</th>
						<th align="left" valign="middle">Mop</th>
						<th align="left" valign="middle">Transaction Type</th>
						<th width="8%" align="left" valign="middle">Region</th>
						<th width="8%" align="left" valign="middle">Business Type</th>
						<th width="100" align="left" valign="middle">remove</th>
					</tr>
					<tr class="boxtext">
						<s:iterator value="routerRules">
							<s:property />
						</s:iterator>
					</tr>
				</table>
		</s:div>


	</div>

</body>
</html>