<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Chargeback</title>
<script src="../js/jquery.min.js"></script>
 <script src="../js/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	
	if ($(document.getElementById("caseStatus")).length){
	$(caseStatus).find("option").eq(0).remove();
	}	
	$("form#files").submit(function(){

	    var formData = new FormData($(this)[0]);

	    $.ajax({
	        url:'updateChargebackDetailsAction',
	        type: 'POST',
	        data: formData,
	        async: false,
	        success: function (data) {
	        	alert("Chargeback accepted !");
	        	location.reload();
	    	//	document.getElementById("saveMessage").innerHTML="Comments added successfully.";
	        },
	        error: function (data) {
	            alert(data)
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });

	    return false;
	});

});
 function updateStatus(){
	$.ajax({
		url : 'updateStatusAction',
		type : 'post',
		data : {
			caseId : '<s:property value="chargeback.caseId" />',
			chargebackStatus : 'Accepted by merchant',
			
			//token : token
		},
		success : function(data) {
		//alert("Details added successfully.");
		location.reload();
	//	document.getElementById("saveMessage").innerHTML="Details added successfully.";
			
		},
		error : function(data) {
			alert("Something went wrong, so please try again.");
		}
	});
}
 function viewComments(){
		var token = document.getElementsByName("token")[0].value;
		
		var caseId = document.getElementById("caseId").value;;
		var agentMessage= document.getElementById("comments").value;
		var selectedServer = "<s:property value='%{#session.USER.UserType.name()}'/>";
		if (selectedServer== "ADMIN") {
			var chargebackstatus = document.getElementById("chargebackStatus").value;
		} else {
			var chargebackstatus ="<s:property value="chargeback.chargebackStatus" />";
		}
		if (selectedServer== "ADMIN") {
			var status = document.getElementById("caseStatus").value;
		} else {
			var status ="<s:property value="chargeback.status" />";
		}
		
		 $.ajax({
			 url : 'chargebackCommentCreaterAction',
				 type : 'post',
					 data :{
						 	caseId: caseId,
							comment : agentMessage,
							chargebackstatus : chargebackstatus,
							caseStatus : status,
						token : token,
					 } ,
					 success : function(data){
						 var responseDiv = document.getElementById("response");
							responseDiv.innerHTML = data.response;
							responseDiv.style.display = "block";
							var responseData = data.response;
							 if(responseData == null){
								/*  responseDiv.innerHTML = "Operation not successfull, please try again later!!"
										responseDiv.style.display = "block";
								 responseDiv.className = "error error-new-text";
								 event.preventDefault(); */
								 
								 alert("Details not updated.");
								 
							 } 
						 var commentFetchTextField = document.getElementById("allComments"); 
							 responseDiv.className = "success success-text";
							 alert("Comment added Successfully.");
							 window.location.reload();
							
			 },
					 error : function(data) {
							var responseDiv = document.getElementById("response");
							responseDiv.innerHTML = "Details not updated.!!"
							responseDiv.style.display = "block";
							responseDiv.className = "error error-new-text";
						}
				 });
		}

</script>
</head>
<body>
<s:form id="files" method="post" enctype="multipart/form-data">
<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0" class="txnf">
		<tr>
		<td></td>
		</tr>
		<tr>
		<td align="left"><h2>Case ID : <s:property value="chargeback.caseId" /> - <s:property value="chargeback.chargebackStatus" /> </h2>
		  </td>
        </tr>
        <tr>
          <td  align="left">
        		<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						  <div class="addfildn" style="width: 30%;margin-left:16px;">ChargeBack Status <s:select headerKey="ALL" headerValue="ALL" id="chargebackStatus"
									name="chargebackStatus" class="textFL_merch"
									list="@com.kbn.chargeback.utils.ChargebackStatus@values()"
									listKey="code" listValue="name"  autocomplete="off" value="chargeback.chargebackStatus"  /></div>
				
							
				</s:if>
				<s:elseif test="%{chargeback.chargebackStatus=='Accepted by merchant'}">
				
				</s:elseif>
				<s:else>
 		<!-- <input type="submit" id="btnAccept" name="btnAccept" value="Accept Chargeback"  onClick="updateStatus()" class="btn btn-success btn-md" style="margin-left: 17px;
    margin-top: -57px;"> -->
			   </s:else>
 
          </td>
          
           <td  align="left">
        		<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						  <div class="addfildn" style="width: 50%;margin-left:16px;">Case Status <s:select headerKey="ALL" headerValue="ALL" id="caseStatus"
									name="caseStatus" class="textFL_merch"
									list="@com.kbn.chargeback.utils.CaseStatus@values()"
									listKey="code" listValue="name"  autocomplete="off" value="chargeback.status"  /></div>
				
							
				</s:if>
				<s:elseif test="%{chargeback.chargebackStatus=='Accepted by merchant'}">
				
				</s:elseif>
				<s:else>
 		<input type="submit" id="btnAccept" name="btnAccept" value="Accept Chargeback"  onClick="updateStatus()" class="btn btn-success btn-md" style="margin-left: 17px;
    margin-top: -57px;">
			   </s:else>
 
          </td>
        </tr>
		
		<tr>
		   <td> 
		<div id="saveMessage">
		</div>
		  </td>
		</tr>
        <tr>
          <td colspan="3" align="left" valign="top">
           <div class="adduT">
             <div class="readchargeback">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="product-spec">
							<tr>
								<th height="30" colspan="4" align="left">Chargeback Details</th>
							</tr>
							<tr>
								<td  height="30" width="23%" align="left">Case ID:</td>
								<td  align="left" class="txtnew"><s:property
										value="chargeback.caseId" />
											<s:hidden name="caseId" id = "caseId"	value="%{chargeback.caseId}" />
										</td>
							</tr>
							<tr>
								<td height="30" align="left" bgcolor="#F2F2F2">Chargeback Status:</td>
								<td align="left" bgcolor="#F2F2F2" width="28%"><s:property
										value="chargeback.chargebackStatus" />
										
										</td>
								<td height="30" align="left">Chargeback Type:</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.chargebackType" />
										
										</td>
							</tr>
						
							<tr>
								<td height="30" align="left" bgcolor="#F2F2F2">Target Date:</td>
								<td align="left" bgcolor="#F2F2F2">
										<s:property value="chargeback.targetDate" />
											
									</td>
									<td height="30" align="left">Last Updated Date:</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.updateDate" />
										
										
										</td>
							</tr>
							
							
						</table>
						</div>
						<div><tr>
						<td>
						<div id ="response"> </div>
	</td>							</tr></div>
                </div>
          </td>
        </tr>
        <tr>

			<td colspan="3" align="left" valign="top"><div class="adduT">
					<div class="readdiv">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="product-spec">
							<tr>
								<th height="30" colspan="2" align="left">Order Details</th>
							</tr>
							<tr>
								<td  height="30" align="left" width="46%">Order ID:</td>
								<td  align="left" class="txtnew"><s:property
										value="chargeback.orderId" />
										</td>
							</tr>
							<tr>
								<td height="30" align="left" bgcolor="#F2F2F2">Date:</td>
								<td align="left" bgcolor="#F2F2F2"><s:property
										value="chargeback.createDate" />
									
										</td>
							</tr>
							<tr>
								<td height="30" align="left">Merchant Pay ID:</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.payId" />
									
										</td>
							</tr>
							<tr>
								<td height="30" align="left" bgcolor="#F2F2F2">Card Number
									Mask:</td>
								<td align="left" bgcolor="#F2F2F2"><s:if
										test="%{transDetails.cardNumber !=null}">
										<s:property value="chargeback.cardNumber" />
									</s:if>
									<s:else>Not applicable</s:else>
								
									</td>
							</tr>							<tr>
								<td height="30" align="left">Payment Method:</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.paymentType" />&nbsp;(<s:property
										value="chargeback.mopType" />)
									
										</td>
							</tr>
							<tr>
								<td height="30" align="left">Card Issuer Info :</td>
								<td align="left" class="txtnew"><s:if
										test="%{transDetails.internalCardIssusserBank !=null}">
										<s:property value="chargeback.internalCardIssusserBank" />
										<s:if
											test="%{transDetails.internalCardIssusserCountry !=null}">&nbsp;(<s:property
												value="chargeback.internalCardIssusserCountry" />)</s:if>
									</s:if>
									<s:else>Not applicable</s:else>
									
									</td>
							</tr>
							<tr>
								<td height="30" align="left" bgcolor="#F2F2F2">Customer
									Email:</td>
								<td align="left" bgcolor="#F2F2F2"><s:property
										value="chargeback.custEmail" />
										
										</td>
							</tr>
							<tr>
								<td height="30" align="left">IP Address:</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.internalCustIP" />
										
										</td>
							</tr>
							<tr>
								<td height="30" align="left" class='greytdbg'>Country</td>
								<td class='greytdbg' align="left"><s:property
										value="chargeback.internalCustCountryName" />
										
										</td>
							</tr>
						</table>
						<div class="clear"></div>
					</div>

					<div class="readdiv">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="product-spec">
							<tr>
								<th colspan="2" height="30" align="left" bgcolor="#eef8ff">Amount
									Summary</th>
							</tr>
							<tr>
								<td height="30" align="left" width="46%">Currency:</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.currencyNameCode" />
										
										</td>
							</tr>
							<tr>
								<td height="30" align="left" bgcolor="#F2F2F2">Authorized
									Amount [A]</td>
								<td align="left" bgcolor="#F2F2F2"><s:property
										value="chargeback.authorizedAmount" />
										
										</td>


							</tr>
							<tr>
								<td height="30" align="left">Captured Amount [B]</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.capturedAmount" />
										
										</td>
							<tr>
								<td height="30" align="left" bgcolor="#F2F2F2">Chargeback
									Amount [C]</td>
								<td align="left" bgcolor="#F2F2F2"><s:property
										value="chargeback.chargebackAmount" />
									
										</td>
							</tr>
							<tr>
								<td height="30" align="left">Fixed Txn Fee [D]</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.fixedTxnFee" />
										
										</td>
							</tr>
							<tr>
								<td height="30" align="left" bgcolor="#F2F2F2">TDR (<s:property
										value="chargeback.merchantTDR" />% of B + D) [F]
										
								</td>
								<td align="left" bgcolor="#F2F2F2"><s:property
										value="chargeback.tdr" />
										
										</td>
									
							</tr>
							<tr>
								<td height="30" align="left">Service Tax (<s:property
										value="chargeback.percentecServiceTax" />% of F) [G]
										
								</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.serviceTax" />
										
										</td>
									
							</tr>
							<tr>
								<td height="30" align="left" bgcolor="#F2F2F2">Net Amount[B
									- (D+F+G)]
									</td>
								<td align="left" bgcolor="#F2F2F2"><s:property
										value="chargeback.netAmount" />
										
										</td>
							</tr>
							<tr>
								<td height="30" align="left">Refunded Amount</td>
								<td align="left" class="txtnew"><s:property
										value="chargeback.refundedAmount" />
										
										</td>
							</tr>
						</table>
					</div>
					
				</div></td>
		</tr>
		<tr>

			<td colspan="3" align="left" valign="top"><div class="adduT">
					
						<div class="clear"></div>
					</div>

				
					
				</td>
		</tr>
<%--         <tr>


			<td colspan="3" align="left" valign="top"><div class="adduT">
					<div class="readchargeback">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="product-spec" style="width:1064px;">
							<tr>
								<th height="30" colspan="2" align="left">History Details</th>
							</tr>
							<tr>
								
								<td width="66%" align="left" class="txtnew">
								<s:property value="fileName"/>
								<div id="ArticleOfAssociation" >                                                                                                   
                                 <s:url id="fileDownload" namespace="/" action="jsp/downloadChargebackDocument" escapeAmp="false" >
                                  <s:param name="payId"><s:property value="chargeback.payId"/></s:param>
                                   <s:param name="fileName"><s:property value="fileName"/></s:param>
                                  </s:url><s:a href="%{fileDownload}" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-download-alt"></i></s:a>
                                  </div>
                                  
								<s:property escapeHtml="false" value='commentsString.replace(\"\n\", \" <br /> \")'  />
									
										
										</td>
							</tr>
						</table>
						<div class="clear"></div>
					</div>

				
					
				</div></td>
		</tr>
	 <tr>
		 <td>
				<table width="94%" border="0" cellspacing="0" cellpadding="0" class="product-spec" style="margin-left: 17px; padding-top: 27px;width:1064px;">
  				  <tr>
  				  <th height="30" colspan="2" align="left">Add Comment</th>
				 </tr>
			   </table>
		 </td>				
	</tr>	
  	<tr>
	    <td>
		  <s:textarea type="text" class="textFL_merchcomment" id="comments" name="comments" autocomplete="off" cols="50" rows="6" style="width:1064px;"/>
		</td>
    </tr>
  <tr>
	<td>
	   <div class="input-group" style="width: 21%;margin-bottom: 14px;margin-top: -30px;margin-left: 16px;"> <span class="input-group-btn">
			<input type="text" class="inputfieldsmall" >
			  <span class="file-input btn btn-success btn-file btn-small"> 
			      <span class="glyphicon glyphicon-folder-open"></span> &nbsp;&nbsp;Choose file
						<s:file name="image" id ="image" />
			 </span></span> 
	  </div>
	</td>
  </tr>
					
        <tr>
          <td><input type="submit" id="btnSave" name="btnSave" value="Add Comment"  class="btn btn-success btn-md" style="margin-left: 17px;margin-bottom:10px;"></td>
        
        </tr> --%>
</table>
  	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td align="left" valign="middle">
		<s:form action="commantUpdater" id="formid" method="post" theme="css_xhtml" class="FlowupLabels">
		    <s:div cssClass="indent">
			    <table width="100%" border="0" cellspacing="0" cellpadding="7" class="formboxRR">
			       <div class="addfildn" style="width:209%;">
                                        <div class="fl_wrap">
                                             <label class='fl_label'>Chargeback Type</label>
													<s:textfield type="text" name="subject" value="%{chargeback.chargebackType}" id="subject" class="fl_input" readonly="true" autocomplete="off" />
										</div>
                                   </div>
                     <div class="addfildn" style="width:70%;">
                                        <div class="fl_wrap">
                                             <label class='fl_label'>Message Body</label>
													<s:textfield type="text" name="subject" value="%{commentsString.replace(\"\n\", \" <br /> \")}" id="subject" class="fl_input" readonly="true" autocomplete="off" />
										</div>
                                   </div>              
                                   <tr>
			         <td align="left" valign="top">&nbsp;</td>
			       </tr>
			       <tr>
						<td align="left" valign="top">
							 <div class="addfildn">
                                <div class="rkb">

                     			  <div class="addfildn" >
                     			  <span style="font-size: 15px;font-weight: normal;color: #1ba6af;">Comment</span>
                                     <div class="" style="border: 1px solid #a9a9a9;width:208%;overflow-y:scroll;height:314px;" >
                                      <s:iterator value="commentList" >
                                        <table style="border: 1px solid #a9a9a9;width:48%;padding:5px;background-color:white;margin-top:5px;margin-left:5px;margin-bottom:5px;">
                                           <tr>
                                             <td>
                                            	<span style="margin-left:1%;color:#47a447;"><s:property value="commentSenderEmailId"/></span>
                                            </td>
                                           </tr>
                                           <tr>
                                        
                                             <td>
                                             <div  style="word-break : break-all;">
                                                 <span style="margin-left:1%;color:Black;"><s:property value="commentBody"/></span>
                                             </div>
                                            </td>
                                         
                                           </tr>
                                           <tr>
                                              <td>
                                              	<span style="margin-left:76%;font-size:10px;"><s:property value= "createDate"/></span>
                                             </td>
                                           </tr>
                                        </table>
                                     </s:iterator>
                                   </div>
                                    </div>
                        
                                     <div class="addfildn" style="width:208%;">
                                        <div class="fl_wrap">
                                            <label class='fl_label'>Add Comment</label>
                                           <s:textfield type="text" name="comments"  id="comments" class="fl_input" />
										</div>
                                     </div>
                                     
                                       <div class="addfildn">
                                        <input type="button" id="btnSave" name="btnSave" class="btn btn-success btn-md" value="Submit" onclick="viewComments()" style="display: inline;margin-left:199%;">
                                     </div>   
                                  </div>
                          
							 </div>	                                                                                           
                          </td>
                    </tr>
                    
	         </table>
	     </s:div>
	   </s:form>
	  </td>
	 </tr>
	 </table>

</s:form>
</body>
</html>