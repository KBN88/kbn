<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Merchant Profile</title>
<link href="../css/profile-page.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.dimensions.js"></script>
<script type="text/javascript" src="../js/jquery.accordion.js"></script>
<script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
</script>
<script type="text/javascript">
	jQuery().ready(function(){
		// simple accordion
		jQuery('#list1a').accordion();
		jQuery('#list1b').accordion({
			autoheight: false
		});
		
		// second simple accordion with special markup
		jQuery('#navigation').accordion({
			active: false,
			header: '.head',
			navigation: true,
			event: 'click',
			fillSpace: false,
			animated: 'easeslide'
		});
	});
	</script>
    <script type="text/javascript">
function showDivs(prefix,chooser) {
        for(var i=0;i<chooser.options.length;i++) {
                var div = document.getElementById(prefix+chooser.options[i].value);
                div.style.display = 'none';
        }
 
		var selectedvalue = chooser.options[chooser.selectedIndex].value;
 
		if(selectedvalue == "PL")
		{
			displayDivs(prefix,"PL");
		}
		else if(selectedvalue == "PF")
		{
			displayDivs(prefix,"PF");
		}
		else if(selectedvalue == "PR")
		{
			displayDivs(prefix,"PR");
		}
		else if(selectedvalue == "CSA")
		{
			displayDivs(prefix,"CSA");
		}
		else if(selectedvalue == "LLL")
		{
			displayDivs(prefix,"LLL");
		}
		else if(selectedvalue == "RI")
		{
			displayDivs(prefix,"RI");
		}
		else if(selectedvalue == "AP")
		{
			displayDivs(prefix,"AP");
		}
		else if(selectedvalue == "T")
		{
			displayDivs(prefix,"T");
		}
 
}
 
function displayDivs(prefix,suffix) {
 
        var div = document.getElementById(prefix+suffix);
        div.style.display = 'block';
}
 
window.onload=function() {
  document.getElementById('select1').value='a';//set value to your default
}

</script>
<script>
var _validFileExtensions = [".jpg", ".pdf", ".png"];    
function Validate(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                    return false;
                }
            }
        }
    }
  
    return true;
}



</script>

<script>

$( document ).ready(function() {
 
	//var token = document.getElementsByName("token")[0].value;
	$.ajax({
		url : 'checkFileExist',
		type : "POST",
			data : {
				 payId : document.getElementById("payId").value ,						
				 
				/*  token:token,
			    "struts.token.name": "token",  */
			},
				
				success:function(data){		       	    		       	    		
       	   		
       	   		var fileList = new Array;
       	   		filelist =data.fileName;	
       	        filelist= filelist.split(",");
       	     for (i = 0; i < filelist.length; i++) { 
       	    	document.getElementById(filelist[i]).style.visibility = "visible";
       	    	
       	     }
          		},
				
          		
				});		 


});
	</script>
</head>

<body class="profilebg">

     <div class="blueback">
     <div class="bluebackL"><table class="table98 padding0">
          <tr class="tdhide">
            <td align="center" valign="top"><br /><img src="../image/profile-logo.png" /></td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top"><div id="main">
  <div>
    <ul id="navigation">
      <li><s:a action='home' class="head1 myprofile">My Profile</s:a></li>
      <li><s:a action='passwordChangeSignUp' class="head1 changepassword" >Change Password</s:a></li>
        </ul>
  </div>
</div></td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table></div>
     <div class="rightblu"><table class="table98 padding0">
          <tr>
            <td align="left" valign="top"><table class="table98 padding0">
              <tr>
                <td align="left" valign="top" class="welcometext">Welcome <s:property value="%{user.businessName}" /></td>
                <td align="right" valign="top">
      <s:a action="logout" class="btn btn-danger"><span class="glyphicon glyphicon-log-out"></span> Log out</s:a>
    
                               </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="left" valign="top" class="borderbottomgrey">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">
      <table class="table98 padding0">
        
        <tr>
          <td align="center">&nbsp;</td>
          <td height="10" align="center">
        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="border-bottom:none;">
        <li class="active"><a href="#MyPersonalDetails" data-toggle="tab">My Personal Details</a></li>
        <li><a href="#MyContactDetails" data-toggle="tab">My Contact Details</a></li>
        <li><a href="#MyBankDetails" data-toggle="tab">My Bank Details</a></li>
        <li><a href="#MyBusinessDetails" data-toggle="tab">My Business Details</a></li>
        <li><a href="#DocumentsUploads" data-toggle="tab">Upload Documents</a></li>
        <li><a href="#LogoUpload" data-toggle="tab">Upload Logo</a></li>       
    </ul>
    </td>
        </tr>
 
  <tr>
          <td align="center">&nbsp;</td>
          <td height="10" align="center"><s:form action="newMerchantSaveAction" autocomplete="off">
    <div id="my-tab-content" class="tab-content">
      
        <div class="tab-pane active" id="MyPersonalDetails">
            <br/><s:div  >
               
                  <table class="table98 padding0 profilepage">
                          <tr>
                          <td width="19%" height="25" align="left" class="bluetdbg"><strong>Business name:</strong></td>
                          <td width="81%" align="left"> <s:textfield name="businessName"  id="businessName"  cssClass="inputfield" type="text" value="%{user.businessName}" readonly="true" autocomplete="off"></s:textfield>
                          <s:hidden id="payId" name="payId" value="%{user.payId}"/>
                          </td>
                        </tr>
                          <tr>
                          <td height="30" align="left" class="bluetdbg"><strong>Email ID:</strong></td>
                          <td align="left"><s:textfield name="emailId" id="emailId" class="inputfield" type="text" value="%{user.emailId}" readonly="true" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" class="bluetdbg"><strong>First Name:</strong></td>
                          <td align="left">
                          <s:textfield name="firstName" id="firstName"  class="inputfield" type="text" value="%{user.firstName}" autocomplete="off"></s:textfield>
                         </td>
                        </tr>
                          <tr>
                          <td height="30" align="left" class="bluetdbg"><strong>Last Name:</strong></td>
                          <td align="left"><s:textfield name="lastName" id="lastName"  class="inputfield" type="text" value="%{user.lastName}" autocomplete="off"></s:textfield></td>
                        </tr>
                                             <tr class="addfildn">
                                               <td class="fl_wrap">
                                                              <label class='fl_label'>Industry Category</label>
										<s:select class="fl_input" id="industryCategory" name="industryCategory" list="industryTypes" 
				value="%{user.industryCategory}"  autocomplete="off" onKeyPress="return ValidateAlpha(event);"></s:select></td>
</tr>
                                                              
	
															  <tr class="addfildn">
                                                              <td class="fl_wrap">
                                                              <label class='fl_label'>Industry Sub Category</label>
																					<s:textfield class="fl_input" id="industrySubCategory" name="industrySubCategory"
																						type="text" value="%{user.industrySubCategory}" autocomplete="off" onKeyPress="return ValidateAlpha(event);"></s:textfield>
																				</td>
</tr>
                          <tr>
                          <td height="30" align="left"><strong>Company Name:</strong></td>
                          <td align="left">    <s:textfield name="companyName" class="inputfield" id="companyName" type="text" value="%{user.companyName}" autocomplete="off"></s:textfield></td>
                        </tr>
                        <tr>
                          <td height="30" align="left"><strong>Business Type:</strong></td>
                          <td align="left"><s:textfield name="businessType" id="businessType" class="inputfield" type="text" value="%{user.businessType}" autocomplete="off"></s:textfield></td>
                        </tr>
                        <tr>
                          <td height="30" align="left"></td>
                          <td align="left" style="float:left;"><s:submit  value="Save" class="btn btn-success"> </s:submit></td>
                        </tr>
                                               
                        </table>
                </s:div>
                </div>
        </div>
        <div class="tab-pane" id="MyContactDetails">
            <br/><s:div  >
             <table class="table98 padding0 profilepage">
                          <tr>
                          <td width="18%" height="30" align="left" valign="middle" class="bluetdbg"><strong>Mobile:</strong></td>
                          <td width="82%" align="left" ><s:textfield name="mobile" id="mobile" class="inputfield" type="text" value="%{user.mobile}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>Telephone No.:</strong></td>
                          <td align="left" ><s:textfield name="telephoneNo" id="telephoneNo" class="inputfield" type="text" value="%{user.telephoneNo}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>Address:</strong></td>
                          <td align="left" ><s:textfield name="address" id="address" class="inputfield" type="text" value="%{user.address}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>City:</strong></td>
                          <td align="left" ><s:textfield name="city" id="city" class="inputfield" type="text" value="%{user.city}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>State:</strong></td>
                          <td align="left" ><s:textfield name="state" id="state" class="inputfield" type="text" value="%{user.state}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>Country:</strong></td>
                          <td align="left" ><s:textfield name="country" id="country" class="inputfield" type="text" value="%{user.country}" autocomplete="off"></s:textfield></td>
                        </tr>
                        <tr>
                            <td height="30" align="left" valign="middle" class="bluetdbg"><strong>Postal Code:</strong></td>
                            <td align="left" ><s:textfield name="postalCode" id="postalCode" class="inputfield"  type="text" value="%{user.postalCode}" autocomplete="off"></s:textfield></td>
  </tr>
                          <tr>
                          <td height="30" align="left"></td>
                          <td align="left" style="float:left;"><s:submit  value="Save" class="btn btn-success"> </s:submit></td>
                        </tr>
         
                        </table>
                </s:div>
        </div>
        <div class="tab-pane" id="MyBankDetails">
            <br /><s:div  >
                <table class="table98 padding0 profilepage">
                          <tr>
                          <td width="18%" height="30" align="left" valign="middle" class="bluetdbg"><strong>Bank Name:</strong></td>
                          <td width="82%" align="left"><s:textfield name="bankName" class="inputfield" id="bankName" type="text" value="%{user.bankName}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>IFSC Code:&nbsp;</strong></td>
                          <td align="left"><s:textfield name="ifscCode" id="ifscCode" class="inputfield" type="text" value="%{user.ifscCode}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>Acc Holder Name:&nbsp;&nbsp;</strong></td>
                          <td align="left"><s:textfield name="accHolderName" id="accHolderName" class="inputfield" type="text" value="%{user.accHolderName}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>Currency:&nbsp;&nbsp;</strong></td>
                          <td align="left"><s:textfield name="currency" id="currency" type="text" class="inputfield" value="%{user.currency}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>Branch Name:&nbsp;&nbsp;</strong></td>
                          <td align="left"><s:textfield name="branchName" id="branchName" type="text" class="inputfield" value="%{user.branchName}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                          <td height="30" align="left" valign="middle" class="bluetdbg"><strong>Pan Card:&nbsp;&nbsp;</strong></td>
                          <td align="left"><s:textfield name="panCard" id="panCard" type="text" class="inputfield" value="%{user.panCard}" autocomplete="off"></s:textfield></td>
                        </tr>
                        <tr>
                            <td height="30" align="left" valign="middle" class="bluetdbg"><strong>Account No.:&nbsp;&nbsp;</strong></td>
                            <td align="left"><s:textfield name="accountNo" id="accountNo" class="inputfield" type="text" value="%{user.accountNo}" autocomplete="off"></s:textfield></td>
  </tr>
  <tr>
                          <td height="30" align="left"></td>
                          <td align="left" style="float:left;"><s:submit  value="Save" class="btn btn-success"> </s:submit></td>
                        </tr>
                      
                        </table>
                </s:div>
        </div>     
        <div class="tab-pane" id="MyBusinessDetails">
            <br /><s:div>
             <table class="table98 padding0 profilepage">
                          <tr>
                            <td width="32%" height="50" align="left" valign="middle" class="bluetdbg"><strong>Organisation Type:&nbsp;&nbsp;</strong></td>
                            <td width="68%" align="left" class="text1">
                           <s:select headerKey="" headerValue="Select Title" cssClass="dropdownfield" list="#{'Proprietship':'Proprietship','Indivisual':'Indivisual','Partnership':'Partnership','Private Limited':'Private Limited','Public Limited':'Public Limited','LLP':'LLP','NGO':'NGO','Educational Institutes':'Educational Institutes','Trust':'Trust','Society':'Society'}"
																name="organisationType" id="organisationType" value="" autocomplete="off"/></td>
                        </tr>
                          
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Website URL:&nbsp;&nbsp;</strong></td>
                            <td align="left" class="text1">
                           <s:textfield name="website" id="website" class="inputfield" type="text" value="%{user.website}" autocomplete="off"></s:textfield></td>
                        </tr>
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Multicurrency Payments Required?:&nbsp;&nbsp;</strong></td>
                            <td align="left" class="text1">
                            
                          <s:select headerKey=""
																headerValue="Select" cssClass="dropdownfield"
																list="#{'YES':'YES','NO':'NO'}"
																name="multiCurrency" id="multiCurrency" value="" autocomplete="off"/>
</td>
                        </tr>
                         
                        <tr>
                          <td height="50" align="left" valign="middle" class="bluetdbg"><strong> Business Model:&nbsp;&nbsp;</strong></td>
                          <td align="left" valign="middle">
                            <s:textfield name="businessModel" class="inputfield" id="businessModel" type="text" value="%{user.businessModel}" autocomplete="off"></s:textfield>
                            <span class="redsmalltext">Please give a brief explanation of your business model and future plans (Essential for startups)</span></td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Operation Address:&nbsp;&nbsp;</strong></td>
                            <td align="left" class="text1">
                            <s:textfield name="operationAddress" class="inputfield" id="operationAddress" type="text" value="%{user.operationAddress}" autocomplete="off"></s:textfield></td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Operation Address State:&nbsp;&nbsp;</strong></td>
                            <td align="left" class="text1">
                            
                          <s:textfield name="operationState" id="operationState" type="text" class="inputfield" value="%{user.operationState}" autocomplete="off"></s:textfield></td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Operation Address City:&nbsp;&nbsp;</strong><strong></strong></td>
                            <td align="left" class="text1">
                            
                           <s:textfield name="operationCity" id="operationCity" type="text" class="inputfield" value="%{user.operationCity}" autocomplete="off"></s:textfield></td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Operation Address Pincode:&nbsp;&nbsp;</strong><strong></strong></td>
                            <td align="left" class="text1">
                            <s:textfield name="operationPostalCode" id="operationPostalCode" class="inputfield" type="text" value="%{user.operationPostalCode}" autocomplete="off"></s:textfield></td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Date of Establishment</strong>:<strong>&nbsp;&nbsp;</strong><strong></strong></td>
                            <td align="left" class="text1">
                            
                           <s:textfield name="dateOfEstablishment" id="dateOfEstablishment" class="inputfield" type="text" value="%{user.dateOfEstablishment}" autocomplete="off"></s:textfield></td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="top" class="bluetdbg"><strong> CIN:&nbsp;&nbsp;</strong></td>
                            <td align="left" class="text1">
                              
                          <s:textfield name="cin" id="cin" type="text" class="inputfield" value="%{user.cin}" autocomplete="off"></s:textfield>
                            <span class="redsmalltext">Mandatory for Companies</span></td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="top" class="bluetdbg"><strong> PAN</strong>:<strong>&nbsp;&nbsp;</strong><strong></strong></td>
                            <td align="left" class="text1">
                              <s:textfield name="pan" id="pan" class="inputfield" type="text" value="%{user.pan}" autocomplete="off"></s:textfield>
                            <span class="redsmalltext">Mandatory for Companies</span></td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="top" class="bluetdbg"><strong>Name on PAN Card:&nbsp;&nbsp;</strong></td>
                            <td align="left"><s:textfield name="panName" class="inputfield" id="panName" type="text" value="%{user.panName}" autocomplete="off"></s:textfield>
                              <span class="redsmalltext">Mandatory for Companies</span>
                            
                            </td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Expected number of transaction:</strong></td>
                            <td align="left" class="text1">
                          <s:textfield name="noOfTransactions" id="noOfTransactions" class="inputfield" type="text" value="%{user.noOfTransactions}" autocomplete="off"></s:textfield></td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Expected amount of transaction:&nbsp;&nbsp;</strong></td>
                            <td align="left" class="text1"><s:textfield name="amountOfTransactions" class="inputfield" id="amountOfTransactions" type="text" value="%{user.amountOfTransactions}" autocomplete="off"></s:textfield></td>
  </tr>
  <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>Disable/Enable Transaction Email:</strong></td>
                            <td align="left" class="text1"> <s:checkbox name="transactionEmailerFlag" value="%{user.transactionEmailerFlag}" autocomplete="off"/>
                        </td>
  </tr>
                          <tr>
                            <td height="50" align="left" valign="middle" class="bluetdbg"><strong>TransactionEmail:&nbsp;&nbsp;</strong></td>
                            <td align="left" class="text1"><s:textfield name="transactionEmailId" class="inputfield" id="transactionEmailId" type="text" value="%{user.transactionEmailId}" autocomplete="off"></s:textfield></td>
  </tr>
                          <tr>
                            <td valign="bottom" colspan="2" align="center"><table class="table98 padding0">
  <tr>
    <td width="40%"><s:submit  value="Save" class="btn btn-success"> </s:submit></td>
    <td width="58%"></td>
  </tr> 
</table>

              </table>

                </s:div>
        </div>             
     </s:form>
        <div class="tab-pane" id="DocumentsUploads">
            <br/>
            <s:div>              
<s:form action="upload" enctype="multipart/form-data" onsubmit="return Validate(this);">
 <table class="table98 padding0">
                          <tr>
                            <td width="70%" height="30" align="left" style="padding:7px;"><div class="txtnew">
                            
                            <s:select  name="ddlBusinessType" onChange="showDivs('div',this)" class="dropdownfieldbig" list="#{'PL':'Private Ltd/Public Ltd Companies','PF':'Partnership Firms','PR':'Proprietorship Firms','CSA':'Clubs / Societies / Associations','LLL':'LLP-Limited Liability Partnership','RI':'Resident Individuals','AP':'Address Proofs','T':'Trust'}" autocomplete="off"></s:select>
                           </div></td>
                          </tr>
                          <tr>
                            <td align="right" height="30" class="redsmalltext">Upload only .pdf, .png and .jpg formats</td>
                          </tr>
                          <tr>
                            <td align="left" class="text1">
                              <s:div id="divPL" style="display:block;">
                              <table class="table98 padding0 profilepage">
                                <tr>
                                  <th colspan="3" align="left" valign="middle"><strong>Certified Copies Of:</strong></th>
                                </tr>
                                <tr>
                                  <td width="65%" align="left" valign="middle" class="bluetdbg"><strong>Articles  Of Association (AOA) </strong></td>
                                  <td width="28%" align="right" valign="middle"><div class="input-group">
                <div class="input-group-btn">
                    <s:textfield type="text" class="inputfieldsmall" theme="simple" readonly="true" autocomplete="off"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file <s:file name="PL_AOA" type="file"/>
                    </span>
                </div>                 
            </div>
</td>
                                  <td width="5%" align="right" valign="top"> 
                                  
                                  <div id="ArticleOfAssociation" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                          
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Memorandum  Of Association (MOA)</strong></td>
                                  <td align="right" valign="middle"><div class="input-group">
                <span class="input-group-btn">
                    <s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true" autocomplete="off"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PL_MOA" type="file"/>
                                           
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="MemorandumOfAssociation" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Certification  Of Incorporation </strong></td>
                                  <td align="right" valign="middle"><div class="input-group">
                <span class="input-group-btn">
                    <s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true" autocomplete="off"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                             <s:file name="PL_COI" type="file"/>
                                           
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="CertificationOfIncorporation" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Certification  Of Commencement of Business (in case of Public Ltd Cos) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group">
                <span class="input-group-btn">
                    <s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true" autocomplete="off"/><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file

                                            <s:file name="PL_COCOB" type="file" />
                                            
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="CertificationOfCommencement" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr> 
                                  <td align="left" valign="middle" class="bluetdbg"><strong>PAN  of the Company.(In case PAN not furnished, a separate declaration for its non  allotment to be obtained.) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group">
                <span class="input-group-btn">
                    <s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true" autocomplete="off"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                              <s:file name="PL_POC"  type="file"/>
                                            
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="PANoftheCompany" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Identification  Documents of Authorized Signatories (as listed separately under &ldquo;Resident  Individuals&rdquo;) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group">
                <span class="input-group-btn">
                    <s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true" autocomplete="off"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                              <s:file name="PL_IDOAS" type="file"/>
                                            
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="IdentificationDocuments" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Board  resolution</strong></td>
                                  <td align="right" valign="middle"><div class="input-group">
                <span class="input-group-btn">
                    <s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true" autocomplete="off"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PL_BR" type="file"/>
                                           
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="Boardresolution" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>List  &amp; Personal Details of Directors </strong></td>
                                  <td align="right" valign="middle"><div class="input-group">
                <span class="input-group-btn">
                    <s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true" autocomplete="off"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                             <s:file name="PL_LPDOD" type="file" />
                                             
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="ListPersonalDetails" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Address  Proofs ( As listed under &ldquo;Address Proof&rdquo;) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group">
                <span class="input-group-btn">
                    <s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true" autocomplete="off"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                             <s:file name="PL_AP"  type="file"/>
                                             
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="AddressProofs" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Bank  statement of last 6 months/Income Tax return</strong></td>
                                  <td align="right" valign="middle"><div class="input-group">
                <span class="input-group-btn">
                    <s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true" autocomplete="off"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PL_BS"  type="file"/>
                                            
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="BankStatement" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                              </table>
                              </s:div>
                              <s:div id="divPF" style="display:none;">
                              <table class="table98 padding0 profilepage">
                                <tr>
                                  <th colspan="3" align="left" valign="middle"><strong>Copies Of:</strong></th>
                                </tr>
                                <tr>
                                  <td width="65%" align="left" valign="middle" class="bluetdbg"><strong>Identification Documents of all partners. (as listed separately under "Resident Individuals") </strong></td>
                                  <td width="28%" align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PF_ID" type="file"/>
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="IdentificationDocumentsAllPartner" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Certified true Copy of Stamped Partnership Deed / Form "A" ( Firm's Registration Details with Registrar of Firms / Form " E" (List of Partners filed with Registrar of Firms) / </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PF_CTC" type="file"/>
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="CertifiedTrueCopy" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>List of Partners </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PF_LOP" type="file"/>
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="ListOfPartners" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Partnership Letter signed by all partners.</strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PF_PLS" type="file"/>
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="PartnershipLetterSigned" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>PAN Card / Allotment Letter (In case PAN not furnished, a separate declaration for its non allotment to be obtained.) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PF_PC" type="file"/>
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="PANCard" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Address Proofs ( As listed under "Address Proof") </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PF_AP" type="file"/>
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="AddressProofsPartnership" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Identification Documents of Authorized Signatories (as listed separately under "Resident Individuals") along with authority letter to sign the agreement</strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PF_IDOAS" type="file"/>
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="IdentificationDocumentsAuthorized" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Bank statement of last 6 months/Income Tax return </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PF_BS" type="file" />
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="BankStatementPartnership" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>License under Shops &amp; Establishments Act / Registration for Sales / Service tax / VAT / Excise Registration / Import-Export Certificate etc / Business License </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PF_LUS" type="file" />
                                  </span></span></div></td>
                                  <td align="right" valign="middle"><div id="LicenseUnderShop" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                              </table></s:div>
                              <div id="divPR" style="display:none;"><table class="table98 padding0 profilepage">
                                <tr>
                                  <th colspan="3" align="left" valign="middle"><strong>Copies Of:</strong></th>
                                </tr>
                                <tr>
                                  <td width="65%" align="left" valign="middle" class="bluetdbg"><strong>Identification document of the Proprietor (as listed separately under &quot;Resident Individuals&quot;) </strong></td>
                                  <td width="28%" align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PR_ID" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="IdentificationDocumentsProprietor" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>PAN Card of Proprietor </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PR_PAN" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="PANCardProprietor" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Proof of Entity : License under Shops &amp; Establishments Act / Registration for Sales / Service tax / VAT / Excise Registration / Import-Export Certificate etc / Business License / Utility Bill in the name of the Firm indicating name of the Proprietor / PAN Card or PAN Allotment Letter (in the name of the Firm) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PR_POE" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="ProofofEntity" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Address Proofs ( As listed under "Address Proof") </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PR_AP" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="AddressProofsProprietor" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Bank statement of last 6 months/Income Tax return </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PR_BS" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="BankStatementProprietor" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>License under Shops &amp; Establishments Act / Registration for Sales / Service tax / VAT / Excise Registration / Import-Export Certificate etc / Business License </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="PR_LUS" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="LicenseUnderShopProprietor" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                              </table></div>
                              <div id="divCSA" style="display:none;"><table class="table98 padding0 profilepage">
                                <tr>
                                  <th colspan="3" align="left" valign="middle"><strong>Certified Copies Of:</strong></th>
                                </tr>
                                <tr>
                                  <td width="65%" align="left" valign="middle" class="bluetdbg"><strong>Bye Laws </strong></td>
                                  <td width="28%" align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="CSA_BL" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="Laws" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>General Body Resolution for Appointment of Office Bearers. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="CSA_GBR" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="GeneralBodyResolution" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>PAN Card </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="CSA_PC" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="PANCardClub" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                              </table></div>
                              <div id="divLLL" style="display:none;"><table class="table98 padding0 profilepage">
                                <tr>
                                  <th colspan="3" align="left" valign="middle"><strong>Copies Of:</strong></th>
                                </tr>
                                <tr>
                                  <td width="65%" align="left" valign="middle" class="bluetdbg"><strong>Registration Certificate under LLP Act,2008</strong></td>
                                  <td width="28%" align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_RC" type="file"/>
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="RegistrationCertificate" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>LLP Agreement Deed </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_LAD" type="file" />
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="LLPAgreement" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>List of Partners </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_LOP" type="file"/>
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="ListOfPartners" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Identification documents of all partners/designated partners </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_ID" type="file" />
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="IdentificationDocumentsLLP" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Authorization letter signed by all partners </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_AL" type="file"/>
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="AuthorizationLetter" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>PAN card of company, If designated partner is company </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_PCC" type="file" />
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="PANCardCompany" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>DIN registration form </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_DR" type="file"/>
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="DINRegistration" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Address Proof </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_AP" type="file" />
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="AddressProofsLLP" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Bank Statement of last 6 months/IT return </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_BS" type="file"/>
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="BankStatementLLP" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>PAN card of LLP </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_PCL" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="PANOfLLP" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>License under Shops &amp; Establishments Act / Registration for Sales / Service tax / VAT / Excise Registration / Import-Export Certificate etc / Business License / </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="LLL_LUS" type="file"/>
                                  </span></span></div></td>
                                   <td width="5%" align="right" valign="middle"><div id="LicenseUnderShopLLP" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                              </table></div>
                              <div id="divRI" style="display:none;"><table class="table98 padding0 profilepage">
                                <tr>
                                  <th colspan="3" align="left" valign="middle"><strong>Certified Copies of any of the following:</strong></th>
                                </tr>
                                <tr>
                                  <td width="65%" align="left" valign="middle" class="bluetdbg"><strong>Passport (with name, photograph &amp; specimen signatures) </strong></td>
                                  <td width="28%" align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="RI_P" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="Passport" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>                                  
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>PAN Card (with name, photograph &amp; specimen signatures) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="RI_PC" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="PANCardResident" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Driving License (with name, photograph &amp; specimen signatures) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="RI_DL" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="DrivingLicense" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Bankers Verification (photograph &amp; signatures) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="RI_BV" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="BankersVerification" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td>
                                </tr>
                              </table></div>
                              <div id="divAP" style="display:none;"><table class="table98 padding0 profilepage">
                                <tr>
                                  <th colspan="3" align="left" valign="middle"><strong>Certified Copies of any of the following:</strong></th>
                                </tr>
                                <tr>
                                  <td width="65%" align="left" valign="middle" class="bluetdbg"><strong>PAN Intimation Letter </strong></td>
                                  <td width="28%" align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="AP_PIL" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="PANIntimation" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Current Utility Bill (Electricity / Telephone / Water Bill (not more than 3 months old) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="AP_CUB" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="CurrentUtilityBill" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Municipal Tax (not more than 3 months old) </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="AP_MT" type="file" />
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="MunicipalTax" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Existing Bank's Statement with at least six months of operation. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="AP_EBS" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="ExistingBanksStatement" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Existing Bank's Certificate confirming name, account no, date of account opening and address. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="AP_EBC" type="file" />
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="ExistingBanksCertificate" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Insurance Policy. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="AP_IP" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="InsurancePolicy" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>Any other document as covered under &quot;Proof of Entity&quot; for Proprietorship. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="AP_AOD" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="AnyOtherDocument" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                              </table></div>
                              <div id="divT" style="display:none;"><table class="table98 padding0 profilepage">
                                <tr>
                                  <th colspan="3" align="left" valign="middle"><strong>Copies Of:</strong></th>
                                </tr>
                                <tr>
                                  <td width="65%" align="left" valign="middle" class="bluetdbg"><strong>	Resolution from the board of Directors/Trustees on organization's letter head &amp; duly signed by atleast two trustees. </strong></td>
                                  <td width="28%" align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="T_RFB" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="ResolutionFromBoard" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>	Signature &amp; photograph verified by your banker </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="T_SP" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="SignatureAndPhoto" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>	Certificate issued under Companies Act or registration with Charity Commissioners office duly attested by Authorized signatory. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="T_CI" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="CertificateIssued" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>	Attested copy of Trust Deed. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="T_AC" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="AttestedCopyDeed" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>	Duly attested PAN card of Trust &amp; Trustees. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="T_DTP" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="DutyAttestedPan" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>	Audited balance sheet &amp; P/L statement for last 2 years (with a special mention of donation received) &amp; Current a/c statement for last one year (Trust) duly attested. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="T_ABS" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="AuditedBalanceSheet" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>	Sales Tax or Income Tax registration &amp; /or local Municipal registration duly attested. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="T_ST" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="SalesTax" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>	Voided check </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="T_VC" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="VoidedCheck" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                                <tr>
                                  <td align="left" valign="middle" class="bluetdbg"><strong>	Percentage of credit card turnover as donations. </strong></td>
                                  <td align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><s:textfield type="text" theme="simple" class="inputfieldsmall" readonly="true"></s:textfield><span class="file-input btn btn-success btn-file btn-small"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                            <s:file name="T_POC" type="file"/>
                                  </span></span></div></td>
                                  <td width="5%" align="right" valign="middle"><div id="CreditCardTurnover" style="visibility: hidden">
                                  <div class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-ok"></span></div>
                                    </div></td> 
                                </tr>
                              </table></div>
                             
                            
                          <tr>
                            <td align="left" class="text1">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="left" class="text1">
                            <table class="table98 padding0 greyroundtble">
                             <tr>
                            <td align="left" width="30%" class="text1">&nbsp;</td>
                             <td align="left" width="30%"><s:submit  value="Save" class="btn btn-success" > </s:submit></td>
                              <td align="left" width="40%" class="text1">&nbsp;</td>
                          </tr>
                            </table>
                           </td>
                           </tr>
                           <tr>
                            <td align="left" class="text1">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="left" class="text1"  bgcolor="#e3ecf2"><table class="table98 padding0">
                              <tr>
	                                <td width="13%" align="left" valign="top"><p><strong>Please Note :</strong></p></td>
                                <td width="86%"><ul>
                                 <li>All photocopies  should be signed &amp; verfied by Authorized Signatory/Director</li>
                                  <li>All photocopies  to be self certified by the client. </li>
                                  <li>Originals  to be sighted &amp; verified </li>
                                  <li>Submitted  Documents must be current &amp; valid (i.e. not expired)</li>
                                </ul></td>
                              </tr>
                            </table>
</td>
</tr>
                        </table>
</s:form>
                </s:div>
        </div>          
  <div class="tab-pane" id="LogoUpload">
            <br/><s:div>              
<s:form action="uploadLogo" enctype="multipart/form-data" >
 <table class="table98 padding0">
                          <tr>
                            <td width="70%" height="30" align="left" style="padding:7px;"><table class="table98 padding0 profilepage">
                                <tr>
                                  <td width="60%" align="left" valign="middle" class="bluetdbg"><strong>This logo will be shown to user on your payment page </strong></td>
                                  <td width="30%" align="right" valign="middle"><div class="input-group"><span class="input-group-btn"><input type="text" class="inputfieldsmall" readonly="readonly" /> <span class="file-input btn btn-success btn-file btn-small"> <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Choose file
                                 
       <s:file name="UserLogo" />
                      </span></span></div></td>
                                  <td width="9%" align="center" valign="middle"><s:submit  value="Save" class="btn btn-success btn-small floatleft"> </s:submit>
                      </td>
                                </tr>
                                <tr>
                                  <td height="40" colspan="3" align="right"  class="redsmalltext"  valign="bottom">Upload only .png and .jpg formats</td>
       </tr>
                                
                                </table>
                               </td>
                               </tr>
                            </table>
                            </s:form>
                            </s:div>
    </div></td>
        </tr>       
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        
      </table>
  </td></tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table></div>
     <div class="clear"></div>
     </div>

<script type="text/javascript"> jQuery(document).ready(function ($) {  $('#tabs').tab(); });</script>
  <script src="../js/bootstrap.min.js"></script>
<script>$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});</script>
</body>
</html>
