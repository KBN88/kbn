<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
	<head>
	<style>
body {font-family: "Lato", sans-serif;}

/* Style the tab */
div.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
div.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
#UserProfile
{
    overflow:scroll;
}
</style>
	<title>Pending Request</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link rel="stylesheet" href="../css/loader.css">
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script type="text/javascript" src="../js/pdfmake.js"></script>
<link href="../css/font-awesome.min.css" rel="stylesheet">
	<script src="../js/messi.js"></script>
	<link href="../css/messi.css" rel="stylesheet" />
	<script src="../js/commanValidate.js"></script>
	<script type="text/javascript">
	
	$(document).ready(function() {
		 document.getElementById("surchargeDatatable1").style.display="none";
		 document.getElementById("surchargeDatatable2").style.display="none";
		 document.getElementById("serviceTaxTable").style.display="none";
		 document.getElementById("tdrData").style.display="none";
	});
	
	function fetchPendingTdrList() {
		 document.getElementById("surchargeDatatable1").style.display="none";
		 document.getElementById("surchargeDatatable2").style.display="none";
		 document.getElementById("serviceTaxTable").style.display="none";
		 document.getElementById("tdrData").style.display="block";
		 
	}
	
	function fetchPendingServiceTaxList() {
		 document.getElementById("surchargeDatatable1").style.display="none";
		 document.getElementById("surchargeDatatable2").style.display="none";
		 document.getElementById("serviceTaxTable").style.display="block";
		 document.getElementById("tdrData").style.display="none";
	}
	
	function fetchPendingMappingList() {
		 document.getElementById("surchargeDatatable1").style.display="none";
		 document.getElementById("surchargeDatatable2").style.display="none";
		 document.getElementById("serviceTaxTable").style.display="none";
		 document.getElementById("tdrData").style.display="none";
	}
	
	function fetchPendingSurchargeList() {
		 document.getElementById("surchargeDatatable1").style.display="block";
		 document.getElementById("surchargeDatatable2").style.display="block";
		 document.getElementById("serviceTaxTable").style.display="none";
		 document.getElementById("tdrData").style.display="none";
	}
	
	function updateSurchargeDetail(divId,curr_row,ele,operation) {
		
		var div = document.getElementById(divId);

		var table = div.getElementsByTagName("table")[0];
		
	    var payId= table.rows[curr_row].cells[2].innerHTML;
		var paymentType = table.rows[curr_row].cells[3].innerHTML;
		var token = document.getElementsByName("token")[0].value;
		var emailId = "<s:property value='%{#session.USER.EmailId}'/>";
		var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		var permission = "<s:property value='%{#session.USER_PERMISSION}'/>";
		var isGranted = permission.includes("Create Surcharge");
		if (isGranted == true || userType == "ADMIN"){
		}
		else{
			alert("Permission denied ! Contact admin for support");
			return;
		}
		
		var operationType = div;
		
		 $.ajax({
			type: "POST",
			url:"updateMerchantSurcharge",
			data:{"payId":payId, "paymentType":paymentType, "operation":operation ,"emailId":emailId, "userType":userType,"token":token,"struts.token.name": "token",},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				//TODO....clean values......using script to avoid page refresh
				window.location.reload();
		    },
			error:function(data){
				alert("Network error, service tax may not be saved");
			}
		    
		}); 
	}
	
	function updateUserProfileDetail(divId,curr_row,ele,operation) {
		
		var div = document.getElementById(divId);

		var table = div.getElementsByTagName("table")[0];
		var rows = table.rows;
		var currentRowNum = Number(curr_row);
		var currentRow = rows[currentRowNum];
		var cells = currentRow.cells;
		
		var emailId= cells[0].innerText;
		var accHolderName = cells[1].innerText;
		var accountNo =cells[2].innerText;
		var address=cells[3].innerText;
		var amexSellerId=cells[4].innerText;
		var amountOfTransactions=cells[5].innerText;
		var attemptTrasacation=cells[6].innerText;
		var bankName=cells[7].innerText;
		var branchName=cells[8].innerText;
		var businessModel=cells[9].innerText;
		var businessName=cells[10].innerText;
		var cin=cells[11].innerText;
		var city=cells[12].innerText;
		var companyName=cells[13].innerText;
		var comments=cells[14].innerText;
		var contactPerson=cells[15].innerText;
		var country=cells[16].innerText;
		var currency=cells[17].innerText;
		var dateOfEstablishment=cells[18].innerText;
		var defaultCurrency=cells[19].innerText;
		var defaultLanguage=cells[20].innerText;
		var emailValidationFlag=cells[21].innerText;
		var expressPayFlag=cells[22].innerText;
		var extraRefundLimit=cells[23].innerText;
		var fax=cells[24].innerText;
		var firstName=cells[25].innerText;
		var iframePaymentFlag=cells[26].innerText;
		var ifscCode=cells[27].innerText;
		var industryCategory=cells[28].innerText;
		var industrySubCategory=cells[29].innerText;
		var lastName=cells[30].innerText;
		var mCC=cells[31].innerText;
		var merchantHostedFlag=cells[32].innerText;
		var merchantType=cells[33].innerText;
		var mobile=cells[34].innerText;
		var modeType=cells[35].innerText;
		var multiCurrency=cells[36].innerText;
		var noOfTransactions=cells[37].innerText;
		var operationAddress=cells[38].innerText;
		var operationCity =cells[39].innerText;
		var operationPostalCode=cells[40].innerText;
		var operationState=cells[41].innerText;
		var organisationType=cells[42].innerText;
		var pan=cells[43].innerText;
		var panCard=cells[44].innerText;
		var panName=cells[45].innerText;
		var payId=cells[46].innerText;
		var postalCode=cells[47].innerText;
		var productDetail=cells[48].innerText;
		var refundTransactionCustomerEmailFlag=cells[49].innerText;
		var refundTransactionMerchantEmailFlag=cells[50].innerText;
		var registrationDate=cells[51].innerText;
		var requestedBy=cells[52].innerText;
		var resellerId=cells[53].innerText;
		var retryTransactionCustomeFlag=cells[54].innerText;
		var state=cells[55].innerText;
		var surchargeFlag=cells[56].innerText;
		var telephoneNo=cells[57].innerText;
		var transactionAuthenticationEmailFlag=cells[58].innerText;
		var transactionCustomerEmailFlag=cells[59].innerText;
		var transactionEmailId=cells[60].innerText;
		var transactionEmailerFlag=cells[61].innerText;
		var transactionSmsFlag=cells[62].innerText;
		var userStatus=cells[63].innerText;
		var website=cells[64].innerText;
		var whiteListIpAddress=cells[65].innerText;
		var operation = operation;
		
		var token = document.getElementsByName("token")[0].value;
		var loginUserType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		var loginEmailId = "<s:property value='%{#session.USER.EmailId}'/>";
		
		var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		var permission = "<s:property value='%{#session.USER_PERMISSION}'/>";
		var isGranted = permission.includes("Create Surcharge");
		if (isGranted == true || userType == "ADMIN"){
		}
		else{
			alert("Permission denied ! Contact admin for support");
			return;
		}
		
		
		//var operationType = div;
		
		 $.ajax({
			type: "POST",
			url:"updateUserProfile",
			data:{"emailId":emailId, "accHolderName":accHolderName, "accountNo":accountNo, "address":address, "amexSellerId":amexSellerId, "amountOfTransactions":amountOfTransactions, "attemptTrasacation":attemptTrasacation ,"bankName":bankName, "branchName":branchName, "businessModel":businessModel, "businessName":businessName, "cin":cin ,"city":city, "companyName":companyName, "comments":comments, "contactPerson":contactPerson, "country":country ,"currency":currency, "dateOfEstablishment":dateOfEstablishment, "defaultCurrency":defaultCurrency, "defaultLanguage":defaultLanguage, "emailValidationFlag":emailValidationFlag ,"expressPayFlag":expressPayFlag, "extraRefundLimit":extraRefundLimit, "fax":fax, "firstName":firstName, "iframePaymentFlag":iframePaymentFlag ,"ifscCode":ifscCode, "industryCategory":industryCategory, "industrySubCategory":industrySubCategory, "lastName":lastName, "mCC":mCC ,"merchantHostedFlag":merchantHostedFlag, "merchantType":merchantType, "mobile":mobile, "modeType":modeType, "multiCurrency":multiCurrency ,"noOfTransactions":noOfTransactions, "operationAddress":operationAddress, "operationCity":operationCity, "operationPostalCode":operationPostalCode, "operationState":operationState ,"organisationType":organisationType, "pan":pan, "panCard":panCard, "panName":panName, "payId":payId ,"postalCode":postalCode, "productDetail":productDetail, "refundTransactionCustomerEmailFlag":refundTransactionCustomerEmailFlag, "refundTransactionMerchantEmailFlag":refundTransactionMerchantEmailFlag, "registrationDate":registrationDate ,"requestedBy":requestedBy,"resellerId":resellerId, "retryTransactionCustomeFlag":retryTransactionCustomeFlag, "state":state ,"surchargeFlag":surchargeFlag, "telephoneNo":telephoneNo, "transactionAuthenticationEmailFlag":transactionAuthenticationEmailFlag, "transactionCustomerEmailFlag":transactionCustomerEmailFlag, "transactionEmailId":transactionEmailId ,"transactionEmailerFlag":transactionEmailerFlag, "transactionSmsFlag":transactionSmsFlag, "userStatus":userStatus, "website":website, "whiteListIpAddress":whiteListIpAddress, "loginUserType":loginUserType, "loginEmailId":loginEmailId, "operation":operation, "token":token,"struts.token.name": "token",},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				//TODO....clean values......using script to avoid page refresh
				window.location.reload();
		    },
			error:function(data){
				alert("Network error, user details may not be saved");
			}
		    
		}); 
	}
					
	
	function updateServiceTax(divId,curr_row,ele,operation) {
		
		var div = document.getElementById(divId);

		var table = div.getElementsByTagName("table")[0];
		
	    var businessType= table.rows[curr_row].cells[1].innerHTML;
		var token = document.getElementsByName("token")[0].value;
		var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		var emailId = "<s:property value='%{#session.USER.EmailId}'/>";
		var permission = "<s:property value='%{#session.USER_PERMISSION}'/>";
		var isGranted = permission.includes("Create Service Tax");
		if (isGranted == true || userType == "ADMIN"){
		}
		else{
			alert("Permission denied ! Contact admin for support");
			return;
		}
		var operation = operation;
		
		 $.ajax({
			type: "POST",
			url:"updateMerchantServiceTax",
			data:{"businessType":businessType, "operation":operation ,"emailId":emailId, "userType":userType,"token":token,"struts.token.name": "token",},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				//TODO....clean values......using script to avoid page refresh
				window.location.reload();
		    },
			error:function(data){
				alert("Network error, service tax may not be saved");
			}
		    
		}); 
	}
	
	function updateResellerMapping(divId,curr_row,ele,operation) {
		
		var div = document.getElementById(divId);

		var table = div.getElementsByTagName("table")[0];
		var rows = table.rows;
		var currentRowNum = Number(curr_row);
		var currentRow = rows[currentRowNum];
		var cells = currentRow.cells;
		
	    var merchantEmailId = cells[0].innerText;
	    var resellerId = cells[1].innerText;
	    var requestedBy = cells[2].innerText;
		var token = document.getElementsByName("token")[0].value;
		var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		var loginemailId = "<s:property value='%{#session.USER.EmailId}'/>";
		var operation = operation;
		var permission = "<s:property value='%{#session.USER_PERMISSION}'/>";
		var isGranted = permission.includes("Create Reseller Mapping");
		if (isGranted == true || userType == "ADMIN"){
		}
		else{
			alert("Permission denied ! Contact admin for support");
			return;
		}
		
		 $.ajax({
			type: "POST",
			url: "updateResellerMapping",
			data:{"merchantEmailId":merchantEmailId, "resellerId":resellerId, "requestedBy":requestedBy, "loginemailId":loginemailId, "operation":operation, "userType":userType, "token":token,"struts.token.name": "token",},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				//TODO....clean values......using script to avoid page refresh
				window.location.reload();
		    },
			error:function(data){
				alert("Network error, details may not be saved");
			}
		    
		}); 
	}
	
function updateBankSurcharge(divId,curr_row,ele,operation) {
	
	var div = document.getElementById(divId);

	var table = div.getElementsByTagName("table")[0];
	
    var paymentType= table.rows[curr_row].cells[0].innerHTML;
    var payId= table.rows[curr_row].cells[4].innerHTML;
    var mopType= table.rows[curr_row].cells[5].innerHTML;
    var acquirer= table.rows[curr_row].cells[6].innerHTML;
	var token = document.getElementsByName("token")[0].value;
	var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
	var emailId = "<s:property value='%{#session.USER.EmailId}'/>";
	var operation = operation;
	var permission = "<s:property value='%{#session.USER_PERMISSION}'/>";
	var isGranted = permission.includes("Create Surcharge");
	if (isGranted == true || userType == "ADMIN"){
	}
	else{
		alert("Permission denied ! Contact admin for support");
		return;
	}
	
	  $.ajax({
		type: "POST",
		url:"updateBankSurcharge",
		data:{"paymentType":paymentType,"payId":payId,"mopType":mopType,"acquirer":acquirer, "operation":operation ,"emailId":emailId, "userType":userType,"token":token,"struts.token.name": "token",},
		success:function(data){
			var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
			if(null!=response){
				alert(response);			
			}
			//TODO....clean values......using script to avoid page refresh
			window.location.reload();
	    },
		error:function(data){
			alert("Network error, service tax may not be saved");
		}
	    
	});  
}

function updateTDR(divId,curr_row,ele,operation) {
	
	var div = document.getElementById(divId);

	var table = div.getElementsByTagName("table")[0];
	
    var id= table.rows[curr_row].cells[13].innerHTML
    var token = document.getElementsByName("token")[0].value;
	var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
	var emailId = "<s:property value='%{#session.USER.EmailId}'/>";
	var operation = operation;
	var permission = "<s:property value='%{#session.USER_PERMISSION}'/>";
	var isGranted = permission.includes("Create TDR");
	if (isGranted == true || userType == "ADMIN"){
	}
	else{
		alert("Permission denied ! Contact admin for support");
		return;
	}
	   $.ajax({
		type: "POST",
		url:"updateBankTDR",
		data:{"id":id, "operation":operation ,"emailId":emailId, "userType":userType,"token":token,"struts.token.name": "token",},
		success:function(data){
			var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
			if(null!=response){
				alert(response);			
			}
			//TODO....clean values......using script to avoid page refresh
			window.location.reload();
	    },
		error:function(data){
			alert("Network error, service tax may not be saved");
		}
	    
	});  
}


function updateMapping(divId,curr_row,ele,operation) {
	
	var fields = divId.split(/ --- /);
	var merchant = fields[0];
	var acquirer = fields[1];
	
    var token = document.getElementsByName("token")[0].value;
	var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
	var emailId = "<s:property value='%{#session.USER.EmailId}'/>";
	var operation = operation;
	var permission = "<s:property value='%{#session.USER_PERMISSION}'/>";
	var isGranted = permission.includes("Create Merchant Mapping");
	if (isGranted == true || userType == "ADMIN"){
	}
	else{
		alert("Permission denied ! Contact admin for support");
		return;
	}
	   $.ajax({
		type: "POST",
		url:"updateBankMapping",
		data:{"operation":operation ,"merchant":merchant,"acquirer":acquirer,"emailId":emailId, "userType":userType,"token":token,"struts.token.name": "token",},
		success:function(data){
			var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
			if(null!=response){
				alert(response);			
			}
			//TODO....clean values......using script to avoid page refresh
			window.location.reload();
	    },
		error:function(data){
			alert("Network error, service tax may not be saved");
		}
	    
	});  
}
	
	</script>
	</head>
	<body id="mainBody">
	
	<p><h2>Pending Requests:</h2></p>

<div class="tab">
  <button class="tablinks" onclick="selectTab(event, 'Mapping')">Merchant Mapping</button>
  <button class="tablinks" onclick="selectTab(event, 'ResellerMapping')">Reseller Mapping</button>
  <button class="tablinks" onclick="selectTab(event, 'TDR')">TDR</button>
  <button class="tablinks" onclick="selectTab(event, 'MerchantSurcharge')">Merchant Surcharge</button>
  <button class="tablinks" onclick="selectTab(event, 'BankSurcharge')">Bank Surcharge</button>
  <button class="tablinks" onclick="selectTab(event, 'ServiceTax')">Service Tax</button>
  <button class="tablinks" onclick="selectTab(event, 'UserProfile')">User Profile</button>
</div>


<script>
function selectTab(evt, opName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(opName).style.display = "block";
    evt.currentTarget.className += " active";
}

function checkUserPermission(){
	var permission = "<s:property value='%{#session.USER_PERMISSION.EmailId}'/>";
}
</script>
	
    <s:form id="pendingDetailsForm" action="pendingDetailsFormAction"
		method="post">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="txnf" style="margin-top:1%;">
			<tr>
				<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
							<td align="left"><div id="MerchantSurcharge" class="tabcontent">
									<s:iterator value="surchargeMerchantData" status="pay">
										<span class="text-primary"><strong><s:property
													value="key" /></strong></span>
											<s:div id="%{key +'Div'}">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
															<th width="8%" align="left" valign="middle">Requested By</th>
															<th width="8%" align="left" valign="middle">Merchant Name</th>
															<th width="8%" align="left" valign="middle">pay Id</th>
															<th width="8%" align="left" valign="middle">Payment Type</th>
														<th width="8%" align="left" valign="middle">Surcharge %</th>
														<th width="7%" align="left" valign="middle">Surcharge FC</th>
														<th width="7%" align="left" valign="middle">Status</th>
														<th width="5%" align="left" valign="middle">Accept</th>
														<th width="2%" align="left" valign="middle"
															style="display: none">id</th>
														<th width="5%" align="left" valign="middle"><span
															id="cancelLabel">Reject</span></th>
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
														<td align="left" valign="middle"><s:property
																	value="requestedBy" /></td>
															<td align="left" valign="middle"><s:property
																	value="merchantName" /></td>
															<td align="left" valign="middle"><s:property
																	value="payId" /></td>
															<td align="left" valign="middle"><s:property
																	value="paymentType" /></td>		
															<td align="left" valign="middle"><s:property
																	value="surchargePercentage" /></td>
															<td align="left" valign="middle"><s:property
																	value="surchargeAmount" /></td>
															<td align="left" valign="middle"><s:property
																	value="status" /></td>
															<td align="center" valign="middle"><s:div>
																	<s:textfield id="edit%{#itStatus.count}" value="Accept"
																		type="button"
																		onclick="updateSurchargeDetail('%{key +'Div'}','%{#itStatus.count}', this ,'accept')"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>
																</s:div></td>
															<td align="center" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="Reject"
																	type="button"
																	onclick="updateSurchargeDetail('%{key +'Div'}','%{#itStatus.count}', this , 'reject')"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</s:iterator>
												</table>
											</s:div>
									</s:iterator>
								</div></td>
						</tr>
					</table></td>
			</tr>
			
			<tr>
				<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
						
							<td align="left">
							<br><br>
							<div id="BankSurcharge" class="tabcontent" >
									<s:iterator value="surchargePGData" status="pay">
										<br>
										<span class="text-primary" id = "test"><strong><s:property
												value="key" /></strong></span>
										<br>
										<div class="scrollD">
											<s:div id="%{key +'Div'}" value = "key">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
														<th width="4%" align="left" valign="middle">Requested By</th>
														<th width="4%" align="left" valign="middle">Payment Type</th>
														<th width="6%" align="left" valign="middle">Merchant Name</th>
														<th width="6%" align="left" valign="middle">Pay Id</th>
														<th width="4%" align="left" valign="middle">MOP Type</th>
														<th width="4%" align="left" valign="middle">Acquirer</th>
														<th width="4%" align="left" valign="middle">Bank %</th>
														<th width="4%" align="left" valign="middle">Bank FC</th>
															 <th width="4%" align="left" valign="middle">OnUs/OffUs</th>
															 <th width="4%" align="left" valign="middle">Status</th>	
															 <th width="5%" align="left" valign="middle">Accept</th>
														<th width="2%" align="left" valign="middle"
															style="display: none">id</th>
														<th width="5%" align="left" valign="middle"><span
															id="cancelLabel">Reject</span></th>
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
															<td align="left" valign="middle" style="display: none"><s:property
																	value="paymentType" /></td>
																	<td align="left" valign="middle"><s:property
																	value="requestedBy" /></td>
															<td align="left" valign="middle"><s:property
																	value="paymentType" /></td>
															<td align="left" valign="middle"><s:property
																	value="merchantName" /></td>
																	<td align="left" valign="middle"><s:property
																	value="payId" /></td>
																	<td align="left" valign="middle"><s:property
																	value="mopType" /></td>
																	<td align="left" valign="middle"><s:property
																	value="acquirerName" /></td>
																	<td align="left" valign="middle" class="nomarpadng"><div
																	title="Surcharge on us">
																	&nbsp;
																	<s:property value="bankSurchargePercentageOn" />
																</div>
																<div class="cellborder" title="Surcharge off us">
																	&nbsp;
																	<s:property value="bankSurchargePercentageOff" />
																</div></td>
																	
																		<td align="left" valign="middle" class="nomarpadng"><div
																	title="Surcharge on us">
																	&nbsp;
																	<s:property value="bankSurchargeAmountOn" />
																</div>
																<div class="cellborder" title="Surcharge off us">
																	&nbsp;
																	<s:property value="bankSurchargeAmountOff" />
																</div></td>
																
																
															<td align="center" valign="middle"><s:checkbox disabled="disabled"
																	name="allowOnOff" value="allowOnOff"
																	onclick="return false" /></td>
																	
																	<td align="left" valign="middle"><s:property
																	value="status" /></td>
																	<td align="center" valign="middle"><s:div>
																	
																	<s:textfield id="edit%{#itStatus.count}" value="Accept"
																		type="button"
																		onclick="updateBankSurcharge('%{key +'Div'}','%{#itStatus.count}', this,'accept')"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>

																	<s:textfield id="cancelBtn%{#itStatus.count}"
																		value="reject" type="button"
																		onclick="cancel('%{#itStatus.count}',this)"
																		style="display:none" autocomplete="off"></s:textfield>
																</s:div></td>
															<td align="center" valign="middle" style="display: none"><s:property
																	value="id" /></td>
															<td align="center" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="Reject"
																	type="button"
																	onclick="updateBankSurcharge('%{key +'Div'}','%{#itStatus.count}', this,'reject')"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</s:iterator>
												</table>
											</s:div>
										</div>
									</s:iterator>
								</div></td>
						</tr>
					</table></td>
			</tr>
		</table>
				<table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
							<td align="left"><div id="ServiceTax" class="tabcontent">
									<s:iterator value="serviceTaxData" status="pay">
										<span class="text-primary" id = "test"><strong><s:property
												value="key" /></strong></span>
											<s:div id="%{key +'Div'}" value = "key">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
														<th width="4%" align="left" valign="middle">Business Type</th>	
														<th width="6%" align="left" valign="middle">Service Tax</th>
														<th width="6%" align="left" valign="middle">Requested By</th>
														<th width="4%" align="left" valign="middle">Status</th>
															 <th width="5%" align="left" valign="middle">Accept</th>
														<th width="2%" align="left" valign="middle"
															style="display: none">id</th>
														<th width="5%" align="left" valign="middle"><span
															id="cancelLabel">Reject</span></th>
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
															<td align="left" valign="middle" style="display: none"><s:property
																	value="businessType" /></td>
															<td align="left" valign="middle"><s:property
																	value="businessType" /></td>
															<td align="left" valign="middle"><s:property
																	value="serviceTax" /></td>
															<td align="left" valign="middle"><s:property
																	value="requestedBy" /></td>
															<td align="left" valign="middle"><s:property
																	value="status" /></td>
																
															<td align="center" valign="middle"><s:div>
																<s:textfield id="edit%{#itStatus.count}" value="Accept"
																	type="button"
																	onclick="updateServiceTax('%{key +'Div'}','%{#itStatus.count}',this,'accept')"
																	class="btn btn-info btn-xs" autocomplete="off"></s:textfield>
															</s:div></td>
															<td align="center" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="Reject"
																	type="button"
																	onclick="updateServiceTax('%{key +'Div'}','%{#itStatus.count}', this,'reject')"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</s:iterator>
												</table>
											</s:div>
									</s:iterator>
								</div></td>
						</tr>
					</table>
					
					<table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
							<td align="left"><div id="Mapping" class="tabcontent">
									<s:iterator value="testData" status="pay">
									<br> <br>
										<span class="text-primary" id = "test"><strong><s:property
												value="key" /></strong></span>
												
											<s:div id="%{key +'Div'}" value = "key">
											<span class="text-primary" id="btn">
												 <s:textfield id="edit%{#itStatus.count}" value="Accept"
																	type="button"
																	onclick="updateMapping('%{key}','%{#itStatus.count}',this,'accept')"
																	class="btn btn-info btn-xs" autocomplete="off"></s:textfield>
												 
												 <s:textfield
																	id="cancelBtn%{#itStatus.count}" value="Reject"
																	type="button"
																	onclick="updateMapping('%{key}','%{#itStatus.count}', this,'reject')"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield>
												</span>
												<table width="100%" border="0" align="center"
													class="product-spec">
													<s:iterator value="value" >
													<tr class="boxheading">
														<th width="4%" align="left" valign="middle">Email Id</th>	
														<th width="6%" align="left" valign="middle">Currency</th>
														<th width="6%" align="left" valign="middle">Merchant Id</th>
														<th width="6%" align="left" valign="middle">Txn Key</th>
														<th width="6%" align="left" valign="middle">Password</th>
														<th width="6%" align="left" valign="middle">Non 3DS</th>
														<th width="4%" align="left" valign="middle">Status</th>
													</tr>
													<s:iterator value="mcpList">
													
														<tr class="boxtext">
															<td align="left" valign="middle"><s:property
																	value="businessType" /></td>
															<td align="left" valign="middle"><s:property
																	value="currency" /></td>
															<td align="left" valign="middle"><s:property
																	value="merchantId" /></td>
															<td align="left" valign="middle"><s:property
																	value="txnKey" /></td>
															<td align="left" valign="middle"><s:property
																	value="password" /></td>
															<td align="center" valign="middle"><s:checkbox disabled="disabled"
																	name="allow3ds" value="allow3ds"
																	/></td>
															<td align="left" valign="middle"><s:property
																	value="status" /></td>
														</tr>
														</s:iterator>
														
														<tr>
														<th>Payment Type</th>
														<th>Mop Type</th>
														<th>Sale</th>
														<th>Authorise</th>
														<th>Refund</th>
														<th>NB Bank</th>
														<th>Status</th>
														</tr>
														<s:iterator value="mmpList" >
														<tr>
														<td align="left" valign="middle"><s:property
																		value="paymentType" /></td>
														<td align="left" valign="middle"><s:property
																		value="mopType" /></td> 
														<td align="center" valign="middle"><s:checkbox disabled="disabled"
																	name="sale" value="sale"/></td>
														<td align="center" valign="middle"><s:checkbox disabled="disabled"
																	name="auth" value="auth"/></td>
														<td align="center" valign="middle"><s:checkbox disabled="disabled"
																	name="refund" value="refund"/></td>
														<td align="left" valign="middle"><s:property
																		value="nbBank" /></td>
														<td align="left" valign="middle"><s:property
																		value="status" /></td>
														</tr>
														</s:iterator>
													</s:iterator>
												</table>
											</s:div>
									</s:iterator>
								</div></td>
						</tr>
					</table>
					
					
					<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
							<td align="left"><div id="UserProfile" class="tabcontent" style='width: 1080px; min-height:200px; overflow:scroll;' >
									<s:iterator value="userProfileData" status="pay">
										<span class="text-primary"><strong><s:property
													value="key" /></strong></span>
											<s:div id="%{key +'Div'}">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
														<th width="8%" align="left" valign="middle">Email Id</th>
														<th width="8%" align="left" valign="middle">Account Holder Name</th>
														<th width="8%" align="left" valign="middle">Account No.</th>
														<th width="8%" align="left" valign="middle">Address</th>
														<th width="7%" align="left" valign="middle">Amex Seller Id</th>
														<th width="7%" align="left" valign="middle">Amount of Transaction</th>
														<th width="5%" align="left" valign="middle">Attempt Transaction</th>
														<th width="2%" align="left" valign="middle">Bank Name</th>
														<th width="8%" align="left" valign="middle">Branch</th>
														<th width="8%" align="left" valign="middle">Business Model</th>
														<th width="8%" align="left" valign="middle">Business Name</th>
														<th width="8%" align="left" valign="middle">CIN</th>
														<th width="7%" align="left" valign="middle">City</th>
														<th width="7%" align="left" valign="middle">Company Name</th>
														<th width="7%" align="left" valign="middle">Comments</th>
														<th width="5%" align="left" valign="middle">Contact Person</th>
														<th width="2%" align="left" valign="middle">Country</th>
														<th width="8%" align="left" valign="middle">Currency</th>
														<th width="8%" align="left" valign="middle">Date of Establishment</th>
														<th width="8%" align="left" valign="middle">Default Currency</th>
														<th width="8%" align="left" valign="middle">Default Language</th>
														<th width="8%" align="left" valign="middle">Email Validation Flag</th>
														<th width="7%" align="left" valign="middle">ExpressPay Flag</th>
														<th width="7%" align="left" valign="middle">Extra Refund Limit</th>
														<th width="5%" align="left" valign="middle">Fax</th>
														<th width="2%" align="left" valign="middle">First Name</th>
														<th width="8%" align="left" valign="middle">IFrame Flag</th>
														<th width="8%" align="left" valign="middle">IFSC</th>
														<th width="8%" align="left" valign="middle">Industry</th>
														<th width="8%" align="left" valign="middle">Industry Sub category</th>
														<th width="7%" align="left" valign="middle">Last Name</th>
														<th width="7%" align="left" valign="middle">MCC</th>
														<th width="5%" align="left" valign="middle">Merchant Hosted Flag</th>
														<th width="7%" align="left" valign="middle">Merchant Type</th>
														<th width="2%" align="left" valign="middle">Mobile No.</th>
														<th width="8%" align="left" valign="middle">Mode</th>
														<th width="8%" align="left" valign="middle">MultiCurrency</th>
														<th width="8%" align="left" valign="middle">No. of transactions</th>
														<th width="8%" align="left" valign="middle">Operation Address</th>
														<th width="8%" align="left" valign="middle">Operation City</th>
														<th width="7%" align="left" valign="middle">Operation Pincode</th>
														<th width="7%" align="left" valign="middle">Operation State</th>
														<th width="5%" align="left" valign="middle">Organisation Type</th>
														<th width="2%" align="left" valign="middle">PAN</th>
														<th width="5%" align="left" valign="middle">PAN No.</th>
														<th width="8%" align="left" valign="middle">Name on PAN</th>
														<th width="8%" align="left" valign="middle">Pay Id</th>
														<th width="8%" align="left" valign="middle">Pincode</th>
														<th width="8%" align="left" valign="middle">Product Details</th>
														<th width="7%" align="left" valign="middle">Refund Transaction Customer Email Flag</th>
														<th width="7%" align="left" valign="middle">Refund transaction Merchant Email Flag</th>
														<th width="7%" align="left" valign="middle">Registration Date</th>
														<th width="7%" align="left" valign="middle">Request By</th>
														<th width="7%" align="left" valign="middle">Reseller Id</th>
														<th width="5%" align="left" valign="middle">Retry Transaction Flag</th>
														<th width="2%" align="left" valign="middle">State</th>
														<th width="8%" align="left" valign="middle">Surcharge Flag</th>
														<th width="8%" align="left" valign="middle">Telephone No.</th>
														<th width="8%" align="left" valign="middle">Transaction Authentication Email Flag</th>
														<th width="8%" align="left" valign="middle">Transaction Customer Email Flag</th>
														<th width="7%" align="left" valign="middle">Transaction EmailId</th>
														<th width="7%" align="left" valign="middle">Transaction Emailer Flag</th>
														<th width="5%" align="left" valign="middle">Transaction Sms Flag</th>
														<th width="2%" align="left" valign="middle">User Status</th>
														<th width="8%" align="left" valign="middle">Website</th>
														<th width="8%" align="left" valign="middle">Whitelist IP Address</th>
														<th width="5%" align="left" valign="middle">Accept</th>
														<th width="5%" align="left" valign="middle"><span
															id="cancelLabel">Reject</span></th>
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
															<td align="left" valign="middle"><s:property
																	value="emailId" /></td>
															<td align="left" valign="middle"><s:property
																	value="accHolderName" /></td>		
															<td align="left" valign="middle"><s:property
																	value="accountNo" /></td>
															<td align="left" valign="middle"><s:property
																	value="address" /></td>
															<td align="left" valign="middle"><s:property
																	value="amexSellerId" /></td>
															<td align="left" valign="middle"><s:property
																	value="amountOfTransactions" /></td>
															<td align="left" valign="middle"><s:property
																	value="attemptTrasacation" /></td>		
															<td align="left" valign="middle"><s:property
																	value="bankName" /></td>
															<td align="left" valign="middle"><s:property
																	value="branchName" /></td>
															<td align="left" valign="middle"><s:property
																	value="businessModel" /></td>
															<td align="left" valign="middle"><s:property
																	value="businessName" /></td>
															<td align="left" valign="middle"><s:property
																	value="cin" /></td>
															<td align="left" valign="middle"><s:property
																	value="city" /></td>		
															<td align="left" valign="middle"><s:property
																	value="companyName" /></td>
															<td align="left" valign="middle"><s:property
																	value="comments" /></td>
															<td align="left" valign="middle"><s:property
																	value="contactPerson" /></td>
															<td align="left" valign="middle"><s:property
																	value="country" /></td>
															<td align="left" valign="middle"><s:property
																	value="currency" /></td>
															<td align="left" valign="middle"><s:property
																	value="dateOfEstablishment" /></td>		
															<td align="left" valign="middle"><s:property
																	value="defaultCurrency" /></td>
															<td align="left" valign="middle"><s:property
																	value="defaultLanguage" /></td>
															<td align="left" valign="middle"><s:property
																	value="emailValidationFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="expressPayFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="extraRefundLimit" /></td>
															<td align="left" valign="middle"><s:property
																	value="fax" /></td>		
															<td align="left" valign="middle"><s:property
																	value="firstName" /></td>
															<td align="left" valign="middle"><s:property
																	value="iframePaymentFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="ifscCode" /></td>
															<td align="left" valign="middle"><s:property
																	value="industryCategory" /></td>
															<td align="left" valign="middle"><s:property
																	value="industrySubCategory" /></td>
															<td align="left" valign="middle"><s:property
																	value="lastName" /></td>		
															<td align="left" valign="middle"><s:property
																	value="mCC" /></td>
															<td align="left" valign="middle"><s:property
																	value="merchantHostedFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="merchantType" /></td>		
															<td align="left" valign="middle"><s:property
																	value="mobile" /></td>
															<td align="left" valign="middle"><s:property
																	value="modeType" /></td>
															<td align="left" valign="middle"><s:property
																	value="multiCurrency" /></td>		
															<td align="left" valign="middle"><s:property
																	value="noOfTransactions" /></td>
															<td align="left" valign="middle"><s:property
																	value="operationAddress" /></td>
															<td align="left" valign="middle"><s:property
																	value="operationCity" /></td>
															<td align="left" valign="middle"><s:property
																	value="operationPostalCode" /></td>
															<td align="left" valign="middle"><s:property
																	value="operationState" /></td>
															<td align="left" valign="middle"><s:property
																	value="organisationType" /></td>		
															<td align="left" valign="middle"><s:property
																	value="pan" /></td>
															<td align="left" valign="middle"><s:property
																	value="panCard" /></td>
															<td align="left" valign="middle"><s:property
																	value="panName" /></td>
															<td align="left" valign="middle"><s:property
																	value="payId" /></td>
															<td align="left" valign="middle"><s:property
																	value="postalCode" /></td>
															<td align="left" valign="middle"><s:property
																	value="productDetail" /></td>		
															<td align="left" valign="middle"><s:property
																	value="refundTransactionCustomerEmailFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="refundTransactionMerchantEmailFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="registrationDate" /></td>
															<td align="left" valign="middle"><s:property
																	value="requestedBy" /></td>
															<td align="left" valign="middle"><s:property
																	value="resellerId" /></td>
															<td align="left" valign="middle"><s:property
																	value="retryTransactionCustomeFlag" /></td>		
															<td align="left" valign="middle"><s:property
																	value="state" /></td>
															<td align="left" valign="middle"><s:property
																	value="surchargeFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="telephoneNo" /></td>
															<td align="left" valign="middle"><s:property
																	value="transactionAuthenticationEmailFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="transactionCustomerEmailFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="transactionEmailId" /></td>
															<td align="left" valign="middle"><s:property
																	value="transactionEmailerFlag" /></td>
															<td align="left" valign="middle"><s:property
																	value="transactionSmsFlag" /></td>		
															<td align="left" valign="middle"><s:property
																	value="userStatus" /></td>
															<td align="left" valign="middle"><s:property
																	value="website" /></td>
															<td align="left" valign="middle"><s:property
																	value="whiteListIpAddress" /></td>
															<td align="center" valign="middle"><s:div>
																	<s:textfield id="edit%{#itStatus.count}" value="Accept"
																		type="button"
																		onclick="updateUserProfileDetail('%{key +'Div'}','%{#itStatus.count}', this ,'accept')"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>
																</s:div></td>
															<td align="center" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="Reject"
																	type="button"
																	onclick="updateUserProfileDetail('%{key +'Div'}','%{#itStatus.count}', this , 'reject')"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</s:iterator>
												</table>
											</s:div>
									</s:iterator>
								</div></td>
						</tr>
					</table></td>
					<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
							<td align="left"><div id="ResellerMapping" class="tabcontent" >
									<s:iterator value="resellerMappingData" status="pay">
										<span class="text-primary"><strong><s:property
													value="key" /></strong></span>
											<s:div id="%{key +'Div'}">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
														<th width="6%" align="left" valign="middle">Merchant Email Id</th>
														<th width="8%" align="left" valign="middle">Reseller Id</th>
														<th width="8%" align="left" valign="middle">Requested By</th>
														<th width="5%" align="left" valign="middle">Accept</th>
														<th width="5%" align="left" valign="middle"><span
															id="cancelLabel">Reject</span></th>
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
															<td align="left" valign="middle"><s:property
																	value="emailId" /></td>
															<td align="left" valign="middle"><s:property
																	value="resellerId" /></td>		
															<td align="left" valign="middle"><s:property
																	value="requestedBy" /></td>
															
															<td align="center" valign="middle"><s:div>
																	<s:textfield id="edit%{#itStatus.count}" value="Accept"
																		type="button"
																		onclick="updateResellerMapping('%{key +'Div'}','%{#itStatus.count}', this ,'accept')"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>
																</s:div></td>
															<td align="center" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="Reject"
																	type="button"
																	onclick="updateResellerMapping('%{key +'Div'}','%{#itStatus.count}', this , 'reject')"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</s:iterator>
												</table>
											</s:div>
									</s:iterator>
								</div></td>
						</tr>
					</table></td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="txnf" style="margin-top:1%;">
			<tr>
				<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
							<td align="left"><div id="TDR" class="tabcontent">
									<s:iterator value="tdrData" status="pay">
										<br>
										<span class="text-primary"><strong><s:property
													value="key" /></strong></span>
										<br>
										<s:property value="paymentType" />
										<div style="margin-bottom: 0px;">										
											<s:if test='key.equals("Net Banking")'>
											<div>Update All</div>
												<s:div id="divAll" class="scrollD">
													<table width="100%" border="0" align="center"
														class="product-spec" id="editAllTable">
														<tr class="boxheading">
															<th width="5%" align="left" valign="middle"
																style="display: none">Payment</th>
															<th width="6%" align="left" valign="middle">Currency</th>
															<th width="4%" align="left" valign="middle">Mop</th>
															<th width="8%" align="left" valign="middle">Transaction</th>
															<th width="7%" align="left" valign="middle">PG
																TDR</th>
															<th width="6%" align="left" valign="middle">PG
																FC</th>
															<th width="7%" align="left" valign="middle">Bank TDR</th>
															<th width="6%" align="left" valign="middle">Bank FC</th>
															<th width="9%" align="left" valign="middle">Merchant
																TDR</th>
															<th width="5%" align="left" valign="middle">Merchant
																FC</th>
															<th width="7%" align="left" valign="middle">Merchant
																S Tax</th>
															<th width="6%" align="left" valign="middle">FC Limit</th>
															<th width="6%" align="left" valign="middle">Allow FC</th>
															<th width="5%" align="left" valign="middle">Update</th>
															<th width="2%" align="left" valign="middle"
																style="display: none">id</th>
															<th width="5%" align="left" valign="middle">Cancel</th>
														</tr>
														<tr class="boxtext">
															<td rowspan="2" align="left" valign="middle"
																style="display: none">NET_BANKING</td>
															<td width="60" rowspan="2" align="left" valign="middle">356</td>
															<td width="310" rowspan="2" align="left" valign="middle">ALL
																BANKS</td>
															<td width="70" rowspan="2" align="left" valign="middle">Transaction</td>
															<td width="50" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="32" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="32" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="58" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="55" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="60" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="70" rowspan="2" align="left" valign="middle">0.0</td>
															<td width="30" rowspan="2" align="left" valign="middle">0.0</td>
															<td width="40" rowspan="2" align="left" valign="middle"><s:checkbox disabled="disabled"
																	name="allowFixCharge" value="allowFixCharge"
																	onclick="clickOnOff" /></td>
															<td width="40" rowspan="2" align="left" valign="middle"><s:div>
																	<s:textfield id="edit" value="Edit All" type="button"
																		onclick="editAllRows('%{key +'Div'}',1, this)"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>

																	<s:textfield id="cancelBtn" value="cancel"
																		type="button" onclick="cancel(this)"
																		style="display:none" autocomplete="off"></s:textfield>
																</s:div></td>
															<td rowspan="2" align="left" valign="middle"
																style="display: none"><s:property value="id" /></td>
															<td rowspan="2" align="left" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="cancel"
																	type="button"
																	onclick="cancel('%{#itStatus.count}',this)"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</table>
												</s:div>
											</s:if>
										</div>
										
										<div class="scrollD">
											<s:div id="%{key +'Div'}">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
														<th width="5%" height="25" valign="middle"
															style="display: none">Payment</th>
														<th width="9%" align="left" valign="middle">Merchant Name</th>
														<th width="4%" align="left" valign="middle">Mop</th>
														<th width="7%" align="left" valign="middle">Transaction</th>
														<th width="7%" align="left" valign="middle">PG
															TDR</th>
														<th width="6%" align="left" valign="middle">PG FC</th>
														<th width="7%" align="left" valign="middle">Bank TDR</th>
														<th width="6%" align="left" valign="middle">Bank FC</th>
														<th width="9%" align="left" valign="middle">Merchant
															TDR</th>
														<th width="8%" align="left" valign="middle">Merchant
															FC</th>
														<th width="6%" align="left" valign="middle">FC Limit</th>
														<th width="6%" align="left" valign="middle">Allow FC</th>
														<th width="5%" align="left" valign="middle">Accept</th>
														<th width="2%" align="left" valign="middle"
															style="display: none">id</th>
														<th width="5%" align="left" valign="middle"><span
															id="cancelLabel">Reject</span></th>
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
															<td align="left" valign="middle" style="display: none"><s:property
																	value="paymentType" /></td>
															<td align="left" valign="middle"><s:property
																	value="businessName" /></td>
															<td align="left" valign="middle"><s:property
																	value="mopType" /></td>
															<td align="left" valign="middle"><s:property
																	value="transactionType" /></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="pgTDR" />
																</div>
																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="pgTDRAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="pgFixCharge" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="pgFixChargeAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="bankTDR" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="bankTDRAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="bankFixCharge" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="bankFixChargeAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="merchantTDR" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="merchantTDRAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="merchantFixCharge" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="merchantFixChargeAFC" />
																</div></td>
															<td align="center" valign="middle"><div>
																	<s:property value="fixChargeLimit" />
																</div></td>
															<td align="center" valign="middle"><s:checkbox disabled="disabled"
																	name="allowFixCharge" value="allowFixCharge"
																	onclick="return false" /></td>
															<td align="center" valign="middle"><s:div>
																	<s:textfield id="edit%{#itStatus.count}" value="Accept"
																		type="button"
																		onclick="updateTDR('%{key +'Div'}','%{#itStatus.count}', this,'accept')"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>
																</s:div></td>
															<td align="center" valign="middle" style="display: none"><s:property
																	value="id" /></td>
															<td align="center" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="Reject"
																	type="button"
																	onclick="updateTDR('%{key +'Div'}','%{#itStatus.count}', this,'reject')"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</s:iterator>
												</table>
											</s:div>
										</div>
									</s:iterator>
								</div></td>
						</tr>
					</table></td>
			</tr>
		</table>
		
		<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
	</s:form>
	
</body>
</html>