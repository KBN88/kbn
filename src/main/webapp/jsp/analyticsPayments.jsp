<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Analytics</title>
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery-ui.js"></script>
<<script src="../js/amcharts.js"></script>
<script src="../js/pie.js"></script>
<script src="../js/light.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<link href="../css/welcomePage.css" rel="stylesheet">
<style type="text/css">
#chartdiv {
	width: 100%;
	height: 100px;
	font-size: 1px;
}

.amcharts-pie-slice {
	transform: scale(1);
	transform-origin: 50% 50%;
	transition-duration: 0.3s;
	transition: all .3s ease-out;
	-webkit-transition: all .3s ease-out;
	-moz-transition: all .3s ease-out;
	-o-transition: all .3s ease-out;
	cursor: pointer;
	box-shadow: 0 0 30px 0 #000;
}

.amcharts-pie-slice:hover {
	transform: scale(1.1);
	filter: url(#shadow);
}
</style>
<script>
	$(document).ready(function() {
		handleChange();
		$("#buttonDay").click(function(env) {
			drawChart();

		});
		$(function() {

			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});

		});

		$(function() {
			var today = new Date();
			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			drawChart();
			analyticsAllTypesTransaction();
		});
		$("#submit").click(function(env) {
			drawChart();
			analyticsAllTypesTransaction();
		});
	});
	function handleChange() {
		drawChart();
		analyticsAllTypesTransaction();
	}
</script>
<script type="text/javascript">
	//to show new loader -Harpreet
	$.ajaxSetup({
		global : false,
		beforeSend : function() {
			toggleAjaxLoader();
		},
		complete : function() {
			toggleAjaxLoader();
		}
	});
	function drawChart() {
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			//alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$
				.ajax({
					url : "paymentMethodChartAction",
					type : "POST",
					data : {
						dateFrom : document.getElementById("dateFrom").value,
						dateTo : document.getElementById("dateTo").value,
						emailId : document.getElementById("merchant").value,
						currency : document.getElementById("currency").value,
						token : token,
						"struts.token.name" : "token",
					},
					success : function(data) {
						var cridet = parseInt(data.pieChart.totalCreditCardsTransaction)
						var debit = parseInt(data.pieChart.totalDebitCardsTransaction)
						var netbaking = parseInt(data.pieChart.totalNetBankingTransaction)
						var wallet = parseInt(data.pieChart.totalWalletTransaction)
						var chart = AmCharts.makeChart("chartdiv", {
							"type" : "pie",
							"startDuration" : 0,
							"theme" : "light",
							"addClassNames" : true,
							"legend" : {
								"position" : "right",
								"marginRight" : 0,
								"autoMargins" : false
							},
							"innerRadius" : "50%",
							"defs" : {
								"filter" : [ {
									"id" : "shadow",
									"width" : "400%",
									"height" : "400%",
									"feOffset" : {
										"result" : "offOut",
										"in" : "SourceAlpha",
										"dx" : 10,
										"dy" : 10
									},
									"feGaussianBlur" : {
										"result" : "blurOut",
										"in" : "offOut",
										"stdDeviation" : 5
									},
									"feBlend" : {
										"in" : "SourceGraphic",
										"in2" : "blurOut",
										"mode" : "normal"
									}
								} ]
							},
							"dataProvider" : [ {
								"country" : "CREDIT CARDS",
								"litres" : cridet
							}, {
								"country" : "DEBIT CARDS",
								"litres" : debit
							}, {
								"country" : "NET BANKING",
								"litres" : netbaking
							}, {
								"country" : "WALLET ",
								"litres" : wallet
							} ],
							"valueField" : "litres",
							"titleField" : "country",
							"export" : {
								"enabled" : true
							}
						});

					
					}
				});
	}
</script>
<script type="text/javascript">
	function analyticsAllTypesTransaction() {
		var token = document.getElementsByName("token")[0].value;
		$
				.ajax({
					url : "statisticsCrediCardSummary",
					type : "POST",
					data : {
						dateFrom : document.getElementById("dateFrom").value,
						dateTo : document.getElementById("dateTo").value,
						emailId : document.getElementById("merchant").value,
						currency : document.getElementById("currency").value,
						token : token,
						"struts.token.name" : "token",
					},
					success : function(data) {
						var totalTransaction = parseInt(data.statistics.totalCreditTransaction)
						var totalSuccess = parseInt(data.statistics.totalCreditSuccess)

						var creditCardsRatio = parseFloat(
								totalSuccess * 100 / totalTransaction).toFixed(
								2)
						if (isNaN(creditCardsRatio))
							creditCardsRatio = 0.00;
						document.getElementById("dvCreditCardsRatio").innerHTML = creditCardsRatio;

						var totalTransaction = parseInt(data.statistics.totalNetBankTransaction)
						var totalSuccess = parseInt(data.statistics.totalNetBankSuccess)
						var netbankingRatio = parseFloat(
								totalSuccess * 100 / totalTransaction).toFixed(
								2)
						if (isNaN(netbankingRatio))
							netbankingRatio = 0.00;
						document.getElementById("dvNetBankingRation").innerHTML = netbankingRatio;

						var totalTransaction = parseInt(data.statistics.totalWalletTransaction)
						var totalSuccess = parseInt(data.statistics.totalWalletSuccess)
						var walletRatio = parseFloat(
								totalSuccess * 100 / totalTransaction).toFixed(
								2)
						if (isNaN(walletRatio))
							walletRatio = 0.00;
						document.getElementById("dvwalletRatio").innerHTML = walletRatio;

						var totalTransaction = parseInt(data.statistics.totalDebitTransaction)
						var totalSuccess = parseInt(data.statistics.totalDebitSuccess)
						var debitCardsRatio = parseFloat(
								totalSuccess * 100 / totalTransaction).toFixed(
								2)
						if (isNaN(debitCardsRatio))
							debitCardsRatio = 0.00;
						document.getElementById("dvDebitCardsRatio").innerHTML = debitCardsRatio;

						var cridet = parseInt(data.statistics.totalCreditTransaction)
						var debit = parseInt(data.statistics.totalCreditSuccess)
						document.getElementById("dvCreditTotalTransaction").innerHTML = data.statistics.totalCreditTransaction;
						document.getElementById("dvCreditTotalSuccess").innerHTML = data.statistics.totalCreditSuccess;
						document.getElementById("dCreditTotalSuccess").innerHTML = data.statistics.totalCreditSuccess;
						document.getElementById("dvCreditTotalFailed").innerHTML = data.statistics.totalCreditFailed;
						document.getElementById("dvCreditTotalCancelled").innerHTML = data.statistics.totalCreditCancelled;
						document.getElementById("dvCreditTotalDropped").innerHTML = data.statistics.totalCreditDropped;

						document.getElementById("dvDebitTotalTransaction").innerHTML = data.statistics.totalDebitTransaction;
						document.getElementById("dvDebitTotalSuccess").innerHTML = data.statistics.totalDebitSuccess;
						document.getElementById("dDebitTotalSuccess").innerHTML = data.statistics.totalDebitSuccess;
						document.getElementById("dvDebitTotalFailed").innerHTML = data.statistics.totalDebitFailed;
						document.getElementById("dvDebitTotalCancelled").innerHTML = data.statistics.totalDebitCancelled;
						document.getElementById("dvDebitTotalDropped").innerHTML = data.statistics.totalDebitDropped;

						document.getElementById("dvTotalTransaction").innerHTML = data.statistics.totalNetBankTransaction;
						document.getElementById("dvTotalSuccess").innerHTML = data.statistics.totalNetBankSuccess;
						document.getElementById("dTotalSuccess").innerHTML = data.statistics.totalNetBankSuccess;
						document.getElementById("dvTotalFailed").innerHTML = data.statistics.totalNetBankFailed;
						document.getElementById("dvTotalCancelled").innerHTML = data.statistics.totalNetBankCancelled;
						document.getElementById("dvTotalDropped").innerHTML = data.statistics.totalNetBankDropped;

						document.getElementById("welletTotalTransaction").innerHTML = data.statistics.totalWalletTransaction;
						document.getElementById("welletTotalSuccess").innerHTML = data.statistics.totalWalletSuccess;
						document.getElementById("welletsTotalSuccess").innerHTML = data.statistics.totalWalletSuccess;
						document.getElementById("welletTotalFailed").innerHTML = data.statistics.totalWalletFailed;
						document.getElementById("welletTotalCancelled").innerHTML = data.statistics.totalWalletCancelled;
						document.getElementById("welletTotalDropped").innerHTML = data.statistics.totalWalletDropped;
					}
				});

	}
</script>


</head>
<body onload="handleChange();handlePiChart();"
	style="margin: 0px; padding: 0px;">
					
	
	<table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left"><!-- <h2>Payment Methods</h2> -->
			<div class="col-md-12">
			<div class="card ">
			  <div class="card-header card-header-rose card-header-text">
				<div class="card-text" id="cardIcon">
				  <h4 class="card-title">Payment Methods</h4>
				</div>
			  </div>
			
			 <div class="card-body ">
			 <div class="container">
			  <div class="row">
			  	<div class="col-sm-6 col-lg-3">
						<label for="dateFrom">Date From:</label> <br />
						
						<s:textfield type="text" readonly="true" id="dateFrom"
							name="dateFrom" class="form-control" autocomplete="off"
							onchange="handleChange();" />
					</div>
					<div class="col-sm-6 col-lg-3">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" onchange="handleChange();"
							autocomplete="off" />
					</div>
					<div class="col-sm-6 col-lg-3">
					   <label for="merchants">Merchants:</label><br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' ||  #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
							<s:select name="merchants" class="form-control" id="merchant"
								headerKey="ALL MERCHANTS" headerValue="ALL MERCHANTS"
								listKey="emailId" listValue="businessName" list="merchantList"
								autocomplete="off" onchange="handleChange();" />
						</s:if>
						<s:else>
							<s:select name="merchant" class="form-control" id="merchant"
								list="merchantList" listKey="emailId" headerKey="ALL"
								listValue="businessName" headerValue="ALL" autocomplete="off" />
						</s:else>
					</div>
					
					<div class="form-group col-md-2 col-xs-6 col-sm-4 txtnew">
						<label for="currency">Currency:</label><br />
						<s:select name="currency" id="currency" list="currencyMap"
							class="form-control" onchange="handleChange();" />
					</div>
			  
			  
			  </div>
			 </div>
			 </div>
			</div>
			</div>
			
			<div class="row box1">
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card card-stats">
				  <div class="card-header card-header-rose card-header-icon">
					<div class="card-icon">
						<i class="fa fa-check fa-5x" id="materialIcons"></i>
					</div>
					<p class="card-category">Total Transactions (Hits)</p>
					<h3 class="card-title"><p id="totalTxnCount" class="media-heading">4</p></h3>
				  </div>
				  <!-- <div class="card-footer">
					<div class="stats">
						<i class="fa fa-check">Total Transactions</i>
					
					</div>
				  </div> -->
				</div>
			  </div>
			<!-- <div class="col-sm-6 col-lg-3">
				<div class="card card-stats">
				  <div class="card-header card-header-warning card-header-icon">
					<div class="card-icon">
						<img class="media-object" src="../image/total-icon.png" alt="total">
					</div>
					<p class="card-category">Total Transactions (Hits)</p>
					<h3 class="card-title"><p id = "totalTxnCount" class="media-heading"></p></h3>
				  </div>
				</div>
			  </div> -->
			<!-- <div class="col-sm-3">
				<div class="media">
				  <div class="media-left blueBg">
					
					  <img class="media-object" src="../image/total-icon.png" alt="total">
					
				  </div>
				  <div class="media-body">
					<p class="media-heading">Total Transactions (Hits)</p>
					<p id = "totalTxnCount" class="media-heading"></p>
					
				  </div>
				</div>
			</div> -->
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card card-stats">
				  <div class="card-header card-header-success card-header-icon">
					<div class="card-icon">
						<i class="fa fa-usd fa-5x"></i>
					</div>
					<p class="card-category">Total Transactions (Captured)</p>
					<h3 class="card-title"><p id="successTxnCount" class="media-heading">0</p></h3>
				  </div>
				  <!-- <div class="card-footer">
					<div class="stats">
						<i class="fa fa-reply-all ">Total Transactions</i>
					 
					</div>
				  </div> -->
				</div>
			  </div>
			<!-- <div class="col-sm-6 col-lg-3">
				<div class="card card-stats">
				  <div class="card-header card-header-success card-header-icon">
					<div class="card-icon">
						<img class="media-object" src="../image/success-icon.png" alt="total">
					</div>
					<p class="card-category">Total Transactions (Captured)</p>
					<h3 class="card-title"><p id = "successTxnCount" class="media-heading"></p></h3>
				  </div>
				</div>
			  </div> -->
			<!-- <div class="col-sm-3">
				<div class="media">
				  <div class="media-left orangeBg">
					  <img class="media-object" src="../image/success-icon.png" alt="total">
				  </div>
				  <div class="media-body">
					<p class="media-heading">Total Transactions (Captured)</p>
					<p id = "successTxnCount" class="media-heading"></p>
				  </div>
				</div>
			</div> -->
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card card-stats">
				  <div class="card-header card-header-info card-header-icon">
					<div class="card-icon">
						<i class="fa fa-percent fa-5x"></i>
					</div>
					<p class="card-category">Captured Percentage</p>
					<h3 class="card-title"><p id="successTxnPercent" class="media-heading">0.00</p></h3>
				  </div>
				  <!-- <div class="card-footer">
					<div class="stats">
						<i class="fa fa-reply">Captured Percentage</i>
					
					</div>
				  </div> -->
				</div>
			  </div>
			<!-- <div class="col-sm-6 col-lg-3">
				<div class="card card-stats">
				  <div class="card-header card-header-success card-header-icon">
					<div class="card-icon">
						<img class="media-object" src="../image/successp-icon.png" alt="total">
					</div>
					<p class="card-category">Captured Percentage</p>
					<h3 class="card-title"><p id = "successTxnPercent"class="media-heading"></p></h3>
				  </div>
				</div>
			  </div> -->
			<!-- <div class="col-sm-3">
				<div class="media">
				  <div class="media-left redBg">
					  <img class="media-object" src="../image/successp-icon.png" alt="total">
				  </div>
				  <div class="media-body">
					<p class="media-heading">Captured Percentage</p>
					<p id = "successTxnPercent"class="media-heading"></p>
				  </div>
				</div>
			</div> -->
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="card card-stats">
				  <div class="card-header card-header-warning card-header-icon">
					<div class="card-icon">
					  <!-- <i class="material-icons">thumb_up</i> -->
					  <i class="fa fa-ticket fa-5x"></i>
					</div>
					<p class="card-category">Average Ticket Size</p>
					<h3 class="card-title">	<p id="avgTkt" class="media-heading">0</p></h3>
				  </div>
				  <!-- <div class="card-footer">
					<div class="stats">
						<i class="fa fa-thumbs-up">Avg. Tkt Size</i>
					 
					</div>
				  </div> -->
				</div>
			  </div>

			<!-- <div class="col-sm-6 col-lg-3">
				<div class="card card-stats">
				  <div class="card-header card-header-warning card-header-icon">
					<div class="card-icon">
						<img class="media-object" src="../image/avgtkt-icon.png" alt="total">
					</div>
					<p class="card-category">Avg. Tkt Size</p>
					<h3 class="card-title">	<p id = "avgTkt" class="media-heading"></p></h3>
				  </div>
				</div>
			  </div> -->
			<!-- <div class="col-sm-3">
				<div class="media">
				  <div class="media-left drkBlueBg">
					  <img class="media-object" src="../image/avgtkt-icon.png" alt="total">
				  </div>
				  <div class="media-body">
					<p class="media-heading">Avg. Tkt Size</p>
					<p id = "avgTkt" class="media-heading"></p>
				  </div>
				</div>
			</div> -->
		</div>
	
				<div class="container">
					<div class="row">
						<div class="col-md-8" style="padding: 10px 25px">
							<div id="chartdiv" style="height: 300px"></div>
						</div>
						<div class="col-md-3" style="float: right;">
							<div class="clearfix">&nbsp;</div>
							<div class="clearfix">&nbsp;</div>
							<br /> <br />
                              <table class="table mytable table-striped table-hover table-responsive">
								<thead style="background: linear-gradient(60deg, #425185, #4a9b9b); color:#fff">
									<tr>
									
							       <th>Success Transaction</th>
							      <th>Success Rate%</th>
									
									</tr>
								</thead>
								<tr>
									<td height="25" align="right" valign="top"
										id="dCreditTotalSuccess"
										style="background-color:; color: #333333;"><s:property
											value="%{statistics.totalCreditSuccess}" /></td>
								
									<td height="15" align="right" valign="top"
										id="dvCreditCardsRatio"
										style="background-color:; color: #333333;"><s:property
											value="%{statistics.totalTransaction}" /></td>
										

								</tr>
								<tr>
									<td height="30" align="right" valign="top"
										id="dDebitTotalSuccess"
										style="background-color:; color: #333333;"><s:property
											value="%{statistics.totalDebitSuccess}" /></td>
											
									
									<td height="30" align="right" valign="top"
										id="dvDebitCardsRatio"
										style="background-color:; color: #333333;"><s:property
											value="%{statistics.totalTransaction}" />
										       </td>

								</tr>
								<tr>
									<td height="30" align="right" valign="top" id="dTotalSuccess"
										style="background-color:; color: #333333;"><s:property
											value="%{statistics.totalSuccess}" /></td>
								
									<td height="30" align="right" valign="top"
										id="dvNetBankingRation"
										style="background-color:; color: #333333;"><s:property
											value="%{statistics.totalTransaction}" />
										</td>
								</tr>
								<tr>
									<td height="30" align="right" valign="top"
										id="welletsTotalSuccess"
										style="background-color:; color: #333333;"><s:property
											value="%{statistics.totalSuccess}" /></td>
									
									<td height="30" align="right" valign="top" id="dvwalletRatio"
										style="background-color:; color: #333333;"><s:property
											value="%{statistics.totalTransaction}" />
										</td>
								</tr>
							</table>
						</div>
						<div class="col-md-1">&nbsp;</div>

					</div>
				</div>
				</td>
		</tr>

		
		<div id="PaymentTypePerformance">
				

				<div class="paymentPerfomance">
					<table class="table mytable table-striped table-hover table-responsive">
						<thead style="background: linear-gradient(60deg, #425185, #4a9b9b); color:#fff">
						  <tr>
							<th>Payment Type</th>
							<th>Total TXN</th>
							<th>Captured(%)</th>
							<th>Failed(%)</th>
							<th>Cancelled(%)</th>
							<th>Invalid(%)</th>
							<th>Dropped(%)</th>
							<!-- <th>Fraud(%)</th>
							<th>Rejected(%)</th> -->
						  </tr>
						</thead>
						 <h3>Payment Type Performance</h3>
						<tbody>
					
						  
						  	<tr>
						  		<td>Credit Card</td>
							
							<td height="30" align="left"><b id="dvCreditTotalTransaction"><s:property value="%{statistics.totalCreditTransaction}" /></b></td>
							<td height="30" align="left"><b id="dvCreditTotalSuccess">
									<s:property value="%{statistics.totalCreditSuccess}" />
							</b></td>
							<td align="left"><b id="dvCreditTotalFailed"> <s:property
										value="%{statistics.totalCreditFailed}" />
							</b></td>
							<td align="left"><b id="dvCreditTotalCancelled"> <s:property
										value="%{statistics.totalCreditCancelled}" />
							</b></td>
								<td><p id="totalUPInvalidTxnPercent" class="media-heading">0.00</p></td>
							<td height="30" align="left"><b id="dvCreditTotalDropped">
									<s:property value="%{statistics.totalCreditDropped}" />
							</b></td>
						</tr>
						  
						  
						  
						  <tr>
						<td>Debit Card</td>
							<td height="30" align="left"><b id="dvDebitTotalTransaction"><s:property
										value="%{statistics.totalDebitTransaction}" /></b></td>
							<td height="30" align="left"><b id="dvDebitTotalSuccess">
									<s:property value="%{statistics.totalDebitSuccess}" />
							</b></td>
							<td align="left"><b id="dvDebitTotalFailed"> <s:property
										value="%{statistics.totalDebitFailed}" />
							</b></td>
							<td align="left"><b id="dvDebitTotalCancelled"> <s:property
										value="%{statistics.totalDebitCancelled}" />
							</b></td>
								<td><p id="totalUPInvalidTxnPercent" class="media-heading">0.00</p></td>
							<td height="30" align="left"><b id="dvDebitTotalDropped">
									<s:property value="%{statistics.totalDebitDropped}" />
							</b></td>
						</tr>
						  
						  <tr>
						<td>Net Banking</td>
							<td height="30" align="left"><b id="dvTotalTransaction"><s:property
										value="%{statistics.totalTransaction}" /></b></td>
							<td height="30" align="left"><b id="dvTotalSuccess"> <s:property
										value="%{statistics.totalSuccess}" />
							</b></td>
							<td align="left"><b id="dvTotalFailed"> <s:property
										value="%{statistics.totalFailed}" />
							</b></td>
							<td align="left"><b id="dvTotalCancelled"> <s:property
										value="%{statistics.totalCancelled}" />
							</b></td>
								<td><p id="totalUPInvalidTxnPercent" class="media-heading">0.00</p></td>
							<td height="30" align="left"><b id="dvTotalDropped"> <s:property
										value="%{statistics.totalDropped}" />
							</b></td>
						</tr>
						  		<tr>
							<td>Wallet</td>
							<td height="30" align="left"><b id="welletTotalTransaction"><s:property
										value="%{statistics.totalTransaction}" /></b></td>
							<td height="30" align="left"><b id="welletTotalSuccess">
									<s:property value="%{statistics.totalSuccess}" />
							</b></td>
							<td align="left"><b id="welletTotalFailed"> <s:property
										value="%{statistics.totalFailed}" />
							</b></td>
							<td align="left"><b id="welletTotalCancelled"> <s:property
										value="%{statistics.totalCancelled}" />
							</b></td>
								<td><p id="totalUPInvalidTxnPercent" class="media-heading">0.00</p></td>
							<td height="30" align="left"><b id="welletTotalDropped">
									<s:property value="%{statistics.totalDropped}" />
							</b></td>
						</tr>
						  
						
						 <tr>
							<td>UPI</td>
							<td><p id="totalUPTxn" class="media-heading">0</p></td>
							<td><p id="totalUPSuccessTxnPercent" class="media-heading">0.00</p></td>
							<td><p id="totalUPFailedTxnPercent" class="media-heading">0.00</p></td>
							<td><p id="totalUPCancelledTxnPercent" class="media-heading">0.00</p></td>
							<td><p id="totalUPInvalidTxnPercent" class="media-heading">0.00</p></td>
							<td><p id="totalUPDroppedTxnPercent" class="media-heading">0.00</p></td>
						<!-- 	<td><p id="totalUPFraudTxnPercent" class="media-heading">0.00</p></td>
						
							<td><p id="totalUPRejectedTxnPercent" class="media-heading">0.00</p></td> -->
						  </tr>
						  

						</tbody>
					  </table>
					 
				</div>
			</div>
			
		
	</table>

</body>
</html>