<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Surcharge Platform</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popup.css" />
<script type="text/javascript">

$(document).ready(function() {
	   $('#paymentType').change(function(event){
		   
		   var merchants = $("select#merchants").val();
		   var acquirer = document.getElementById("paymentType").value;
		   var emailId = merchants;
		   var paymentType = acquirer;

		   if(merchants==null ||merchants=="" || acquirer==null||acquirer==""){
			   document.getElementById("datatable").style.display="none";
			   document.getElementById("datatable1").style.display="none";
			   document.getElementById("datatable2").style.display="none";
			  return false;
			  
			 }
		   else{
			   document.getElementById("datatable").style.display="block";
			   document.getElementById("datatable1").style.display="block";
			   document.getElementById("datatable2").style.display="none";
			   
		   }
		   document.getElementById("surchargedetailform").submit();
		   
	   });

   $('#merchants').change(function(event){
	   var acquirer = document.getElementById("paymentType").value;
	   var merchants = $("select#merchants").val();
	   var emailId = merchants;
	   var paymentType = acquirer;
	   
	   if(merchants==null ||merchants=="" || acquirer==null||acquirer==""){
		   document.getElementById("datatable").style.display="none";
		   document.getElementById("datatable1").style.display="none";
		   document.getElementById("datatable2").style.display="none";
		  return false;
		  
		 }
	   else{
		   document.getElementById("datatable").style.display="block";
		   document.getElementById("datatable1").style.display="block";
		   document.getElementById("datatable2").style.display="none";
		   
	   }
	   document.getElementById("surchargedetailform").submit();	
    });
  
});

var editMode;

function editCurrentRow(divId,curr_row,ele){
	
	var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
	var div = document.getElementById(divId);

	var table = div.getElementsByTagName("table")[0];

	var merchantId = document.getElementById("merchants").value;
	var paymentType = document.getElementById("paymentType").value;
	var rows = table.rows;
	var currentRowNum = Number(curr_row);
	var currentRow = rows[currentRowNum];
	var cells = currentRow.cells;
	var cell0 = cells[0];
	var cell1 = cells[1];
	var cell2 = cells[2];
	var cell3 = cells[3]; 

	var cell4 =  cells[4];
	var cell5 =  cells[5];
	
	
	var cell0Val = cell0.innerText;
	var cell1Val = cell1.innerText;
	var cell2Val = cell2.innerText;
	var cell3Val = cell3.innerText;
	var cell4Val = cell4.innerText;
	var cell5Val = cell5.innerText;

	if(ele.value=="edit"){
		if(editMode) 
		{
				alert('Please edit the current row to proceed');
				return;
		}
		ele.value="save";
		ele.className ="btn btn-success btn-xs";
		cell2.innerHTML = "<input type='number' id='cell2Val'   class='chargingplatform' min='1' step='0.0' value="+cell2Val+"></input>";
		cell3.innerHTML = "<input type='number' id='cell3Val'   class='chargingplatform' min='1' step='0.0' value="+cell3Val+"></input>";
		editMode = true;
	}
	else{
		var surchargePercentage = document.getElementById('cell2Val').value;
		var surchargeAmount = document.getElementById('cell3Val').value;
		var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		var loginEmailId = "<s:property value='%{#session.USER.EmailId}'/>";
		cell2.innerHTML = surchargePercentage;
		cell3.innerHTML = surchargeAmount;
		var merchantId = document.getElementById("merchants").value;
		var paymentType = document.getElementById("paymentType").value;
		editMode = false;

		ele.value="edit";
		ele.className ="btn btn-info btn-xs";		
		var token  = document.getElementsByName("token")[0].value;
		$.ajax({
			type: "POST",
			url:"editSurchargeDetail",
			data:{"emailId":merchantId, "paymentType":paymentType,"surchargePercentage":surchargePercentage, "surchargeAmount":surchargeAmount, "userType":userType, "loginEmailId":loginEmailId,"token":token,"struts.token.name": "token",},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				//TODO....clean values......using script to avoid page refresh
				window.location.reload();
		    },
			error:function(data){
				window.location.reload();
				alert("Invalid Input , surcharge data not saved");
			},
			input:function(data){
				alert("Invalid Input , please correct and try again");
				
			}
		});
	}
}



function editSurchargeCurrentRow(divId,curr_row,ele){
	
	var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
	var loginUserEmailId = "<s:property value='%{#session.USER.EmailId}'/>";
	var div = document.getElementById(divId);

	var table = div.getElementsByTagName("table")[0];

	var merchantId = document.getElementById("merchants").value;
	var paymentType = document.getElementById("paymentType").value;
	
	var acq = divId;
	var acquirer =  acq.substring(0, acq.length - 3);
	
	var rows = table.rows;
	var currentRowNum = Number(curr_row);
	var currentRow = rows[currentRowNum];
	var cells = currentRow.cells;
	var cell0 = cells[0];
	var cell1 = cells[1];
	var cell2 = cells[2];
	var cell3 =  cells[3].children[0];
	var cell4 =  cells[3].children[1];
	var cell5 =  cells[4].children[0];
	var cell6 =  cells[4].children[1];
	var cell7 =  cells[5];
	var cell8 =  cells[6];
	
	
	var cell0Val = cell0.innerText;
	var cell1Val = cell1.innerText;
	var cell2Val = cell2.innerText;
	var cell3Val = cell3.innerText.trim();
	var cell4Val = cell4.innerText.trim();
	var cell5Val = cell5.innerText.trim();
	var cell6Val = cell6.innerText.trim();
	var cell7Val = cell7.querySelector('input[type=checkbox]').checked;
	var cell8Val = cell8.innerText;

	if(ele.value=="edit"){
		if(editMode) 
		{
				alert('Please edit the current row to proceed');
				return;
		}
		ele.value="save";
		ele.className ="btn btn-success btn-xs";
		cell3.innerHTML = "<input type='number' id='cell3Val'   class='chargingplatform' min='1' step='0.0' value="+cell3Val+"></input>";
		cell4.innerHTML = "<input type='number' id='cell4Val'   class='chargingplatform' min='1' step='0.0' value="+cell4Val+"></input>";
		cell5.innerHTML = "<input type='number' id='cell5Val'   class='chargingplatform' min='1' step='0.0' value="+cell5Val+"></input>";
		cell6.innerHTML = "<input type='number' id='cell6Val'   class='chargingplatform' min='1' step='0.0' value="+cell6Val+"></input>";
		
		cell7.innerHTML = "";
		if(cell7Val){
			cell7.innerHTML = "<input type='checkbox' id='cell7Val' checked='true'></input>";
		}else{
		    cell7.innerHTML = "<input type='checkbox' id='cell7Val'></input>";
		}
		editMode = true;
	}
	else{
		var bankSurchargePercentageOn = document.getElementById('cell3Val').value;
		var bankSurchargePercentageOff = document.getElementById('cell4Val').value;
		var bankSurchargeAmountOn = document.getElementById('cell5Val').value;
		var bankSurchargeAmountOff = document.getElementById('cell6Val').value;
		var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		cell3.innerHTML = bankSurchargePercentageOn;
		cell4.innerHTML = bankSurchargePercentageOff;
		cell5.innerHTML = bankSurchargeAmountOn;
		cell6.innerHTML = bankSurchargeAmountOff;
		var merchantId = document.getElementById("merchants").value;
		var paymentType = document.getElementById("paymentType").value;
		editMode = false;

		ele.value="edit";
		ele.className ="btn btn-info btn-xs";		
		var token  = document.getElementsByName("token")[0].value;
		$.ajax({
			type: "POST",
			url:"editSurchargeMappingDetail",
			data:{"emailId":merchantId, "paymentType":paymentType, "bankSurchargePercentageOn":bankSurchargePercentageOn, "bankSurchargePercentageOff":bankSurchargePercentageOff,
				"bankSurchargeAmountOn":bankSurchargeAmountOn,"bankSurchargeAmountOff":bankSurchargeAmountOff,"allowOnOff":cell7Val,"allowOnOff":cell7Val,"acquirer":acquirer,
				"merchantIndustryType":cell2Val,"status":cell8Val,"mopType":cell1Val,"userType":userType,"loginUserEmailId":loginUserEmailId,"token":token,"struts.token.name": "token",},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				//TODO....clean values......using script to avoid page refresh
				window.location.reload();
		    },
			error:function(data){
				window.location.reload();
				alert("Invalid Input , surcharge data not saved");
			}
		});
	}
}


function cancel(curr_row,ele){
	var parentEle = ele.parentNode;
	
	if(editMode){
	 	window.location.reload();
	}
}

</script>
<style>
.product-spec input[type=text] {
	width: 35px;
}
</style>
</head>
<body>
<s:actionmessage class="error error-new-text" />

	<s:form id="surchargedetailform" action="surchargePlatformAction"
		method="post">
				<div class="card ">
			  <div class="card-header card-header-rose card-header-text">
					<div class="card-text">
					  <h4 class="card-title">Surcharge Platform</h4>
					</div>
				  </div>
				    <div class="card-body ">
				    <div class="container">
				      <div class="row">
				      <div class="col-sm-6 col-lg-3">
				      <label style="float: left;">Select User : </label><br>
				      <div class="txtnew">
                        <s:select headerKey="-1" headerValue="Select User"
						list="#{'1':'Merchant'}" id="user" name="user" value="1"
						class="form-control" autocomplete="off" />
								</div>
				      
				    </div>
				      <div class="col-sm-6 col-lg-3">
				      <label style="float: left;">Select Merchant :</label><br>
				      <div class="txtnew">
				    <s:select headerValue="Select Merchant" headerKey=""
											name="emailId" class="form-control" id="merchants"
											list="listMerchant" listKey="emailId"
											listValue="businessName" autocomplete="off" />
				      </div>
				    </div>
				      <div class="col-sm-6 col-lg-3">
								<label style="float: left;">Select Payment Type :</label><br>
								 <div class="txtnew">
								<s:select class="form-control" headerKey=""
											headerValue="Payment Type"
											list="@com.kbn.commons.util.PaymentType@values()"
											listKey="code" listValue="name" name="paymentType" id="paymentType"
											autocomplete="off" />
								</div>
								</div>
				    </div>
				    </div>
				    
				    </div>

		
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="txnf" style="margin-top:1%;">
			<tr>
				<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
							<td align="left"><div id="datatable" class="scrollD">
									<s:iterator value="aaData" status="pay">
										<br>
										<span class="text-primary"><strong><s:property
													value="key" /></strong></span>
										<br>
										<div class="scrollD">
											<s:div id="%{key +'Div'}">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
														<th width="5%" height="25" valign="middle"
															style="display: none">Payment</th>
														<th width="6%" align="left" valign="middle">Service Tax</th>
														<th width="8%" align="left" valign="middle">Surcharge %</th>
														<th width="7%" align="left" valign="middle">Surcharge FC</th>
														<th width="5%" align="left" valign="middle">Update</th>
														<th width="2%" align="left" valign="middle"
															style="display: none">id</th>
														<th width="5%" align="left" valign="middle"><span
															id="cancelLabel">Cancel</span></th>
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
															<td align="left" valign="middle" style="display: none"><s:property
																	value="paymentType" /></td>
															<td align="left" valign="middle"><s:property
																	value="serviceTax" /></td>
															<td align="left" valign="middle"><s:property
																	value="surchargePercentage" /></td>
															<td align="left" valign="middle"><s:property
																	value="surchargeAmount" /></td>
															<td align="center" valign="middle"><s:div>
																	<s:textfield id="edit%{#itStatus.count}" value="edit"
																		type="button"
																		onclick="editCurrentRow('%{key +'Div'}','%{#itStatus.count}', this)"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>

																	<s:textfield id="cancelBtn%{#itStatus.count}"
																		value="cancel" type="button"
																		onclick="cancel('%{#itStatus.count}',this)"
																		style="display:none" autocomplete="off"></s:textfield>
																</s:div></td>
															<td align="center" valign="middle" style="display: none"><s:property
																	value="id" /></td>
															<td align="center" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="cancel"
																	type="button"
																	onclick="cancel('%{#itStatus.count}',this)"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</s:iterator>
												</table>
											</s:div>
										</div>
									</s:iterator>
								</div></td>
						</tr>
					</table></td>
			</tr>
			
			<tr>
				<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
						
							<td align="left">
							<br><br>
							<div id="datatable2" class="scrollD">
							<br><br><br>
							<p align = "center"><b>Set Surcharge for Bank</b></p>
									<s:iterator value="surchargeMapData" status="pay">
										<br>
										<span class="text-primary" id = "test"><strong><s:property
												value="key" /></strong></span>
										<br>
										<div class="scrollD">
											<s:div id="%{key +'Div'}" value = "key">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
														<th width="5%" height="25" valign="middle"
															style="display: none">Payment</th>
														<th width="4%" align="left" valign="middle">MOP Type</th>	
														<th width="6%" align="left" valign="middle">Merchant Type</th>
														<th width="4%" align="left" valign="middle">Bank %</th>
														<th width="4%" align="left" valign="middle">Bank FC</th>
															 <th width="4%" align="left" valign="middle">OnUs/OffUs</th>
															 <th width="4%" align="left" valign="middle">Status</th>	
															 <th width="5%" align="left" valign="middle">Update</th>
														<th width="2%" align="left" valign="middle"
															style="display: none">id</th>
														<th width="5%" align="left" valign="middle"><span
															id="cancelLabel">Cancel</span></th>
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
															<td align="left" valign="middle" style="display: none"><s:property
																	value="paymentType" /></td>
															<td align="left" valign="middle"><s:property
																	value="mopType" /></td>
															<td align="left" valign="middle"><s:property
																	value="merchantIndustryType" /></td>
																	
																		<td align="left" valign="middle" class="nomarpadng"><div
																	title="Surcharge on us">
																	&nbsp;
																	<s:property value="bankSurchargePercentageOn" />
																</div>
																<div class="cellborder" title="Surcharge off us">
																	&nbsp;
																	<s:property value="bankSurchargePercentageOff" />
																</div></td>
																	
																		<td align="left" valign="middle" class="nomarpadng"><div
																	title="Surcharge on us">
																	&nbsp;
																	<s:property value="bankSurchargeAmountOn" />
																</div>
																<div class="cellborder" title="Surcharge off us">
																	&nbsp;
																	<s:property value="bankSurchargeAmountOff" />
																</div></td>
																
																
															<td align="center" valign="middle"><s:checkbox
																	name="allowOnOff" value="allowOnOff"
																	onclick="return false" /></td>
																	
																	<td align="left" valign="middle"><s:property
																	value="status" /></td>
																	<td align="center" valign="middle"><s:div>
																	
																	<s:textfield id="edit%{#itStatus.count}" value="edit"
																		type="button"
																		onclick="editSurchargeCurrentRow('%{key +'Div'}','%{#itStatus.count}', this)"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>

																	<s:textfield id="cancelBtn%{#itStatus.count}"
																		value="cancel" type="button"
																		onclick="cancel('%{#itStatus.count}',this)"
																		style="display:none" autocomplete="off"></s:textfield>
																</s:div></td>
															<td align="center" valign="middle" style="display: none"><s:property
																	value="id" /></td>
															<td align="center" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="cancel"
																	type="button"
																	onclick="cancel('%{#itStatus.count}',this)"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</s:iterator>
												</table>
											</s:div>
										</div>
									</s:iterator>
								</div></td>
						</tr>
					</table></td>
			</tr>
			
			
			
			<tr>
				<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
							<td align="left">
							<div id="datatable1" class="scrollD">
							<br><br><br>
								<div><p align="center"><b>Surcharge Report</b></p></div>
									<s:iterator value="acquirerData" status="pay">
										<br>
										<span class="text-primary"><strong><s:property
													value="key" /></strong></span>
										<br>
										<div class="scrollD">
											<s:div id="%{key +'Div'}">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
														<th width="5%" height="25" valign="middle"
															style="display: none">Payment</th>
														
														<th width="7%" align="left" valign="middle">MOP Type</th>	
														<th width="7%" align="left" valign="middle">ON US/ OFF US</th>
														<th width="6%" align="left" valign="middle">Merchant Type</th>
														<th width="7%" align="left" valign="middle">PG %</th>
														<th width="6%" align="left" valign="middle">PG FC</th>
														<th width="7%" align="left" valign="middle">Bank %</th>
														<th width="6%" align="left" valign="middle">Bank FC</th>
														<th width="7%" align="left" valign="middle">Merchant %</th>
														<th width="7%" align="left" valign="middle">Merchant FC</th>
														
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
															<td align="left" valign="middle" style="display: none"><s:property
																	value="paymentType" /></td>
															<td align="left" valign="middle"><s:property
																	value="mopType" /></td>
															<td align="left" valign="middle"><s:property
																	value="onOff" /></td>
															<td align="left" valign="middle"><s:property
																	value="merchantIndustryType" /></td>
															<td align="left" valign="middle"><s:property
																	value="pgSurchargePercentage" /></td>
															<td align="left" valign="middle"><s:property
																	value="pgSurchargeAmount" /></td>
															<td align="left" valign="middle"><s:property
																	value="bankSurchargePercentage" /></td>
															<td align="left" valign="middle"><s:property
																	value="bankSurchargeAmount" /></td>
															<td align="left" valign="middle"><s:property
																	value="merchantSurchargePercentage" /></td>
															<td align="left" valign="middle"><s:property
																	value="merchantSurchargeAmount" /></td>
															
														
														</tr>
													</s:iterator>
												</table>
											</s:div>
										</div>
									</s:iterator>
								</div></td>
						</tr>
					</table></td>
			</tr>
		</table>
		
		<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
	</s:form>
	
</body>
</html>