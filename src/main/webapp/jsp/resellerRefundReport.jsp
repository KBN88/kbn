<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Refund Report</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />

<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery.dataTables.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<script src="../js/jquery.popupoverlay.js" type="text/javascript"></script>
<script src="../js/dataTables.buttons.js" type="text/javascript"></script>
<script src="../js/pdfmake.js" type="text/javascript"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function handleChange() {
		reloadTable();
	}
	$(document).ready(function() {

		$(function() {
			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});

		$(function() {
			var today = new Date();
			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			renderTable();
		});

		$(function() {
			var table = $('#refundReportDataTable').DataTable();
			$('#refundReportDataTable tbody').on('click', 'td', function() {

				refundDetailsFunc(table, this);
			});
		});
	});
	function renderTable() {
		var table = new $.fn.dataTable.Api('#refundReportDataTable');
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				$(".modal").show();
			},
			complete : function() {
				$(".modal").hide();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#refundReportDataTable').dataTable({
			dom : 'BTftlpi',
			buttons : [ {
				extend : 'copyHtml5',
				exportOptions : {
					columns : [ ':visible' ]
				}
			}, {
				extend : 'csvHtml5',
				title : 'Refund Report',
				exportOptions : {
					columns : [ 0, 11, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13 ]
				}
			}, {
				extend : 'pdfHtml5',
				title : 'Refund Report',
				exportOptions : {
					columns : [ ':visible' ]
				}
			}, {
				extend : 'print',
				title : 'Refund Report',
				exportOptions : {
					columns : [ 0, 1, 2, 3, 4, 5, 6, 7 ]
				}
			}, {
				extend : 'colvis',
				//           collectionLayout: 'fixed two-column',
				columns : [ 2, 3, 6, 7, 9 ]
			} ],
			"ajax" : {
				"url" : "refundReportAction",
				"type" : "POST",
				"data" : generatePostData
			},
			"processing" : true,
			"lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
			"order" : [ [ 5, "desc" ] ],
			"aoColumns" : [ {
				"data" : "orderId",

			}, {
				"data" : "transactionId",
			}, {
				"data" : "businessName",
			}, {
				"data" : "currencyName"
			}, {
				"data" : "txnDate"
			}, {
				"data" : "approvedAmount"
			}, {
				"data" : "refundDate"
			}, {
				"data" : "refundedAmount"
			}, {
				"data" : "refundableAmount"
			}, {
				"data" : "paymentMethod"
			}, {
				"data" : "payId",
				"visible" : false,
				"className" : "displayNone"

			}, {
				"data" : null,
				"visible" : false,
				"mRender" : function(row) {
					return "\u0027" + row.transactionId;
				}
			}, {
				"data" : "internalCustIp"
			}, {
				"data" : "internalUserEmail"
			}, ]
		});
	}

	function reloadTable() {
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#refundReportDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}
	function generatePostData() {
		var token = document.getElementsByName("token")[0].value;
		var obj = {
			acquirer : document.getElementById("account").value,
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			paymentType : document.getElementById("paymentMethods").value,
			merchantEmailId : document.getElementById("merchants").value,
			currency : document.getElementById("currency").value,
			token : token,
			"struts.token.name" : "token",
		};
		return obj;
	}

	function refundDetailsFunc(table, index) {
		var rows = table.rows();
		var columnNumber = table.cell(index).index().column;

		var token = document.getElementsByName("token")[0].value;
		var rowIndex = table.cell(index).index().row;
		var transactionId = table.cell(rowIndex, 1).data();
		document.getElementById('txnId').value = table.cell(rowIndex, 1).data();
		var orderId = table.cell(rowIndex, 0).data();
		document.getElementById('orderId').value = table.cell(rowIndex, 0)
				.data();
		document.getElementById('payId').value = table.cell(rowIndex, 10)
				.data();
		/*  if(columnNumber==0){
		document.refundDetails.submit();
		} */
	}
</script>
</head>
<body>
	<div class="modal" style="display: none">
		<div class="center">
			<img alt="" src="../image/loader.gif" />
		</div>
	</div>
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0" class="txnf">
		<tr>
			<td colspan="5" align="left"><h2>Refund Report</h2>
				<div class="container">
					<div class="form-group col-md-2 txtnew col-sm-4 col-xs-6">
						<label for="merchant">Merchant:</label> <br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER_TYPE.name()=='SUPERADMIN' #session.USER_TYPE.name()=='ACQUIRER'}">
							<s:select name="merchants" class="form-control" id="merchants"
								headerKey="ALL" headerValue="ALL" list="merchantList"
								listKey="emailId" listValue="businessName"
								onchange="handleChange();" autocomplete="off" />
						</s:if>
						<s:else>
							<s:select name="merchants" class="form-control" id="merchants"
								list="merchantList" listKey="emailId" headerKey="ALL"
								headerValue="ALL" listValue="businessName"
								onchange="handleChange();" autocomplete="off" />
						</s:else>
					</div>

					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">
							<div class="txtnew">
								<label for="acquirer">Account:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" id="account"
									name="account" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" onchange="handleChange();"
									autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-2 col-xs-6 txtnew"
							style="display: none;">


							<div class="txtnew">
								<s:select headerKey="ALL" headerValue="ALL" id="account"
									name="account" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" onchange="handleChange();"
									autocomplete="off" />
							</div>

						</div>
					</s:else>


					<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
						<label for="email">Payment Method:</label> <br />
						<s:select headerKey="ALL" headerValue="All" class="form-control"
							list="@com.kbn.commons.util.PaymentType@values()"
							name="paymentMethods" id="paymentMethods"
							onchange="handleChange();" autocomplete="off" value="code"
							listKey="code" listValue="name" />
					</div>
					<div class="form-group  col-md-1 col-sm-4 txtnew  col-xs-6">
						<label for="email">Currency:</label> <br />
						<s:select name="currency" id="currency" headerValue="ALL"
							headerKey="ALL" list="currencyMap" class="form-control"
							onchange="handleChange();" />
					</div>

					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" class="form-control" readonly="true"
							id="dateFrom" name="dateFrom" autocomplete="off"
							onchange="handleChange(); validate();" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" onchange="handleChange();"
							autocomplete="off" />
					</div>
				</div></td>
		</tr>
		<tr>
			<td colspan="5" align="left" valign="top">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" valign="top" style="padding: 10px;">
				<div class="scrollD">
					<table id="refundReportDataTable" class="display" cellspacing="0"
						width="100%">
						<thead>
							<tr class="boxheadingsmall">
								<th>Order Id</th>
								<th>Txn Id</th>
								<th>Business Name</th>
								<th>Currency</th>
								<th>Txn Date</th>
								<th>Txn Amt</th>
								<th>Refund Date</th>
								<th>Refunded Amt</th>
								<th>Available Amt</th>
								<th>Payment Method</th>
								<th></th>
								<th>Txn Id</th>
								<th>Customer IP</th>
								<th>Merchant EmailID</th>
							</tr>
						</thead>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<s:form name="refundDetails" action="refundConfirmAction">
		<s:hidden name="orderId" id="orderId" value="" />
		<s:hidden name="payId" id="payId" value="" />
		<s:hidden name="transactionId" id="txnId" value="" />
		<s:hidden name="token" value="%{#session.customToken}" />
	</s:form>
</body>
</html>