<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Failed Transaction</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>

<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script type="text/javascript" src="../js/pdfmake.js"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>

<script type="text/javascript">
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	function handleChange() {
		reloadTable();
	}
	$(document).ready(function() {

		$(function() {

			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});

			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});

		});

		$(function() {
			var today = new Date();
			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			renderTable();
		});

		$(function() {
			var datepick = $.datepicker;
			var table = $('#failedDataTable').DataTable();
			$('#failedDataTable tbody').on('click', 'td', function() {

				popup(table, this);
			});
		});
	});

	function renderTable() {
		var table = new $.fn.dataTable.Api('#failedDataTable');
		 var payId = document.getElementById("merchant").value;
		//to show new loader -Harpreet
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				toggleAjaxLoader();
			},
			complete : function() {
				toggleAjaxLoader();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#failedDataTable')
				.dataTable(
						{
							"footerCallback" : function(row, data, start, end,
									display) {
								var api = this.api(), data;

								// Remove the formatting to get integer data for summation
								var intVal = function(i) {
									return typeof i === 'string' ? i.replace(
											/[\,]/g, '') * 1
											: typeof i === 'number' ? i : 0;
								};

								// Total over all pages
								total = api.column(9).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(9, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(9).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');
							},
							"columnDefs" : [ { className : "dt-body-right",	"targets" : [8,9]
							} ],
							dom : 'BTftlpi',
							buttons : [ {
								extend : 'copyHtml5',
								//footer : true,
								exportOptions : {
									columns : [ 15, 1, 2, 3, 4, 5, 7, 8,9 ]
								}
							}, {
								extend : 'csvHtml5',
								//footer : true,
								title : 'Failed Transactions',
								exportOptions : {
									columns : [ 15, 1, 2, 3, 4, 5, 7, 8,9 ]
								}
							}, {
								extend : 'pdfHtml5',
								orientation : 'landscape',
								//footer : true,
								title : 'Failed Transactions',
								exportOptions : {
									columns : [ 0, 1, 2, 3, 4, 5, 7, 8 ,9]
								}
							}, {
								extend : 'print',
								//footer : true,
								title : 'Failed Transactions',
								exportOptions : {
									columns : [ 0, 1, 2, 3, 4, 5, 7, 8,9 ]
								}
							}, {
								extend : 'colvis',
								//           collectionLayout: 'fixed two-column',
								columns : [1,2,3,4,5,6,7,8]
							} ],

							"ajax" : {
								"url" : "failedTransactionAction",
								"type" : "POST",
								"data" : function (d){
									return generatePostData(d);
								}
							},
							    "searching": false,
							    "processing": true,
						        "serverSide": true,
						        "paginationType": "full_numbers", 
						        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
						        //"order" : [ [ 1, "desc" ] ],
						        "order": [],
						        "columnDefs": [
						            {
						            "type": "html-num-fmt", "targets": 4,
						            "orderable": false, "targets": [0,1,2,3,4,5,6,7,8,9]
						            }
						        ],
							"columns" : [

							{
								"data" : "transactionId",
								"className" : "my_class",
							}, {
								"data" : "txnDate",
								"width" : '10%'
							}, {
								"data" : "orderId",
								"width" : '10%'
							}, {
								"data" : "businessName",
								"width" : '10%'
							}, {
								"data" : "customerEmail"
							}, {
								"data" : "customerName",
								"width" : '10%'
								
							}, {
								"data" : "paymentMethod",
								"render" : function(data, type, full) {
									return full['paymentMethod'] + ' ' + '-'
											+ ' ' + full['mopType'];
								}
							}, {
								"data" : "mopType",
								"visible" : false,
								"className" : "displayNone"
							},{
								"data" : "currencyName",
								"width" : '9%'
							}, {
								"data" : "approvedAmount",
								"width" : '9%'
							}, {
								"data" : "status",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "responseMsg",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "cardNo",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "productDesc",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "internalRequestDesc",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : null,
								"visible" : false,
								"className" : "displayNone",
								"mRender" : function(row) {
									return "\u0027" + row.transactionId;
								}
							}, {
								"data" : "pgTxnMessage",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "internalCardIssusserBank",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "internalCardIssusserCountry",
								"visible" : false,
								"className" : "displayNone"
							} ]
						});
		 $("#merchant").select2({
			  //data: payId
			});
	}
	function reloadTable() {
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#failedDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}
	function popup(table, index) {
		var rows = table.rows();
		var datepick = $.datepicker;
		var columnVisible = table.cell(index).index().columnVisible;
		if (columnVisible == 0) {
			var token = document.getElementsByName("token")[0].value;
			var rowIndex = table.cell(index).index().row;
			var transactionId = table.cell(rowIndex, 0).data();
			var date_from = table.cell(rowIndex, 1).data();
			var currency = table.cell(rowIndex, 8).data();
			var amount = table.cell(rowIndex, 9).data();
			var paymentMethod = table.cell(rowIndex, 6).data();
			var mopType = table.cell(rowIndex, 7).data();
			var orderId = table.cell(rowIndex, 2).data();
			var cust_email = table.cell(rowIndex, 4).data();
			if (cust_email == null) {
				cust_email = "Not available";
			}
			var cust_name = table.cell(rowIndex, 5).data();
			var status = table.cell(rowIndex, 10).data();
			var message = table.cell(rowIndex, 11).data();
			var card_number = table.cell(rowIndex, 12).data();
			var productDesc = table.cell(rowIndex, 13).data();
			var internalRequestDesc = table.cell(rowIndex, 14).data();
			var pgTxnMessage = table.cell(rowIndex, 16).data();
			var internalCardIssusserBank = table.cell(rowIndex, 17).data();
			var internalCardIssusserCountry = table.cell(rowIndex, 18).data();

			$
					.post(
							"customerAddressAction",
							{
								orderId : decodeVal(orderId),
								custEmail : decodeVal(cust_email),
								datefrom : decodeVal(date_from),
								amount : decodeVal(amount),
								currency : decodeVal(currency),
								productDesc : decodeVal(productDesc),
								internalRequestDesc : decodeVal(internalRequestDesc),
								transactionId : decodeVal(transactionId),
								cardNumber : decodeVal(card_number),
								paymentMethod : decodeVal(paymentMethod),
								mopType : decodeVal(mopType),
								internalCardIssusserBank : decodeVal(internalCardIssusserBank),
								internalCardIssusserCountry : decodeVal(internalCardIssusserCountry),
								status : decodeVal(status),
								token : token,
								pgTxnMessage : decodeVal(pgTxnMessage),
								"struts.token.name" : "token"
							}).done(function(data) {
						var popupDiv = document.getElementById("popup");
						popupDiv.innerHTML = data;
						decodeDiv();
						$('#popup').popup('show');
					});
		}
	}

	function decodeDiv() {
		var divArray = document.getElementsByTagName('div');
		for (var i = 0; i < divArray.length; ++i) {
			var div = divArray[i];
			if (div.id.indexOf('param-') > -1) {
				var val = div.innerHTML;
				div.innerHTML = decodeVal(val);
			}
		}
	}

	function decodeVal(value) {
		var txt = document.createElement("textarea");
		txt.innerHTML = value;
		return txt.value;
	}

	function generatePostData(d) {
		var token = document.getElementsByName("token")[0].value;
		var payId = document.getElementById("merchant").value;
		if(payId==''){
			payId='ALL'
		}
		var obj = {
			acquirer : document.getElementById("acquirer").value,
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			paymentType : document.getElementById("paymentMethods").value,
			payId : payId,
			currency : document.getElementById("currency").value,
			draw : d.draw,
			length :d.length,
			start : d.start, 
			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}
</script>
<style>
.dataTables_wrapper {
	position: relative;
	clear: both;
	*zoom: 1;
	zoom: 1;
	margin-top: -30px;
}
</style>
</head>

<body>
	<div id="popup"></div>
	<div class="card ">
		<div class="card-header card-header-rose card-header-text">
			<div class="card-text">
				<h4 class="card-title">Failed Transactions</h4>
			</div>
			<div class="card-body ">
				<div class="container">

					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						<div class="form-group col-md-2 col-xs-6 col-sm-3 txtnew">

							<div class="txtnew">
								<label for="acquirer">Account:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()" listKey="code"
									listValue="name" onchange="handleChange();" autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-2 col-xs-3 txtnew"
							style="display: none;">


							<div class="txtnew">
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()" listKey="code"
									listValue="name" onchange="handleChange();" autocomplete="off" />
							</div>

						</div>
					</s:else>
					<div class="form-group col-md-2 txtnew col-sm-3 col-xs-6">
						<label for="merchant">Merchant:</label> <br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN' || #session.USER_TYPE.name()=='ACQUIRER'}">
							<s:select name="merchant" class="form-control" id="merchant"
								headerKey="" headerValue="ALL" list="merchantList"
								listKey="payId" listValue="businessName"
								onchange="handleChange();" autocomplete="off" />
						</s:if>
						<s:else>
							<s:select name="merchant" class="form-control" id="merchant"
								list="merchantList" listKey="payId" headerKey=""
								headerValue="ALL" listValue="businessName"
								onchange="handleChange();" autocomplete="off" />
						</s:else>
					</div>


					<div class="form-group  col-md-2 col-sm-3 txtnew  col-xs-6">
						<label for="email">Payment Method:</label> <br />
						<s:select headerKey="All" headerValue="All" class="form-control"
							list="@com.kbn.commons.util.PaymentType@values()"
							name="paymentMethods" id="paymentMethods"
							onchange="handleChange();" autocomplete="off" value="code"
							listKey="code" listValue="name" />
					</div>

					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="currency">Currency:</label> <br />
						<s:select name="currency" id="currency" headerValue="ALL"
							headerKey="ALL" list="currencyMap" class="form-control"
							onchange="handleChange();" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" readonly="true" id="dateFrom"
							name="dateFrom" class="form-control" autocomplete="off"
							onchange="handleChange();" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" onchange="handleChange();"
							autocomplete="off" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">

		<tr>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" style="padding: 10px;"><div class="scrollD">
					<table id="failedDataTable" class="display" cellspacing="0"
						width="100%">
						<thead>
							<tr class="boxheadingsmall">
								<th style='text-align: center'>Txn Id</th>
								<th style='text-align: center'>Date</th>
								<th style='text-align: center'>Order Id</th>
								<th style='text-align: center'>Business Name</th>
								<th style='text-align: center'>Customer Email</th>
								<th style='text-align: center'>Customer Name</th>
								<th style='text-align: center'>Payment Method</th>
								<th style='text-align: center'></th>
								<th style='text-align: center'>Currency</th>
								<th style='text-align: right'>Txn Amount</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th>Txn Id</th>
								<th></th>
								<th></th>

							</tr>
						</thead>
						<tfoot>
							<tr class="boxheadingsmall">
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th style='text-align: center'>Total</th>
								<th style='text-align: right; float: right; padding-right: 5px;'></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>

							</tr>
						</tfoot>
					</table>
				</div></td>
		</tr>
	</table>
</body>
</html>