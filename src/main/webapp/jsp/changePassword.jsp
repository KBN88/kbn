<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change Password</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.minshowpop.js"></script>
<script src="../js/jquery.formshowpop.js"></script>
<script src="../js/commanValidate.js"></script> 
<script type="text/javascript">
$.ajaxSetup({
	beforeSend : function(){
		jQuery('body').toggleClass('loaded');
	},
	complete : function(){
		jQuery('body').toggleClass('loaded');
	}
});	  
function changePasswordRequest(){
	$.ajax({
		url : 'changePassword',
		type : 'post',
		data : {
			token : document.getElementsByName("token")[0].value,
			oldPassword : document.getElementById('oldPassword').value,
			newPassword : document.getElementById('newPassword').value,
			confirmNewPassword : document.getElementById('confirmNewPassword').value
		},
		success : function(data){
			alert(data.response)
			if(data.response == "Password changed successfully, login to continue"){
				window.location = "loginResult";
			}
		},
		error : function(data){
			alert('Something went wrong!');
		}
	});
}
</script>
</head>
<body>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" valign="top"><table width="100%" border="0"
						align="center" cellpadding="0" cellspacing="0" class="txnf">
          <tr>
            <td align="left"><h2>Change Password</h2></td>
          </tr>
          <tr>
            <td class="txtnew" align="left" style="padding: 10px;"><em><strong>Password
              Criteria:</strong> Password must be minimum 8 and maximum 32 characters
              long, with special characters (! @ , _ + / =) , at least one
              upper case and one lower case alphabet. Your new password must
              not be the same as any of your last four passwords.</em></td>
          </tr>
          <tr>
            <td align="left" valign="top"><div class="adduR">
                <div class="adduTR">Old Password<br>
                  <div class="txtnew">
                    <s:textfield name="oldPassword" id ="oldPassword" type="password" autocomplete="off" 
													cssClass="form-control" />
                  </div>
                </div>
                <div class="adduTR">New Password<br>
                  <div class="txtnew">
                    <s:textfield name="newPassword" id="newPassword" type="password" autocomplete="off"
													cssClass="form-control" />
                  </div>
                </div>
                <div class="adduTR">Confirm New Password<br>
                  <div class="txtnew">
                    <s:textfield name="confirmNewPassword" id="confirmNewPassword" type="password" autocomplete="off"
													cssClass="form-control" />
                  </div>
                </div>
                <div class="adduTR" style="text-align:center; padding:14px 0 0 0;">
                  <input type="submit" name=""	value="Save" onclick="changePasswordRequest()" class="btn btn-success btn-md" />
                  <br>
                </div>
                <div class="clear"></div>
              </div></td>
          </tr>
          <tr>
            <td align="center" valign="top">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>

</body>
</html>