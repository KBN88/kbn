<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html dir="ltr" lang="en-US">
<head>
<title>Capture Transactions</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script type="text/javascript" src="../js/pdfmake.js"></script>
<link href="../css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="../css/daterangepicker-bs3.css" />
<!-- <link href="../css/loader.css" rel="stylesheet" type="text/css" /> -->
<script type="text/javascript" src="../js/refund.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script type="text/javascript">
	var checkedRows = [];
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	function handleChange() {
		reloadTable();
	}
	$(document)
			.ready(
					function() {

						$(function() {

							$("#dateFrom").datepicker({
								prevText : "click for previous months",
								nextText : "click for next months",
								showOtherMonths : true,
								dateFormat : 'dd-mm-yy',
								selectOtherMonths : false,
								maxDate : new Date()
							});
							$("#dateTo").datepicker({
								prevText : "click for previous months",
								nextText : "click for next months",
								showOtherMonths : true,
								dateFormat : 'dd-mm-yy',
								selectOtherMonths : false,
								maxDate : new Date()
							});

						});

						$(function() {
							var today = new Date();
							$('#dateTo').val(
									$.datepicker.formatDate('dd-mm-yy', today));
							$('#dateFrom').val(
									$.datepicker.formatDate('dd-mm-yy', today));
							renderTable();
							var selectedServer = "<s:property value='%{#session.USER.UserType.name()}'/>";
							if (selectedServer == "SUBUSER") {
								var permission = "<s:property value='%{#session.USER_PERMISSION}'/>"
								if (permission.indexOf("Void/Refund") < 0) {
									var table = $('#captureDataTable')
											.DataTable();
									table.column(11).visible(false);
								}
							} else if (selectedServer == "RESELLER") {
								var permission = "<s:property value='%{#session.USER_PERMISSION}'/>"
								if (permission.indexOf("Void/Refund") < 0) {
									var table = $('#captureDataTable')
											.DataTable();
									table.column(11).visible(false);
								}
							} else if (selectedServer == "ACQUIRER"
									|| selectedServer == "SUBACQUIRER") {
								var permission = "<s:property value='%{#session.USER_PERMISSION}'/>"
								if (permission.indexOf("Void/Refund") < 0) {
									var table = $('#captureDataTable')
											.DataTable();
									table.column(11).visible(false);
								}
							}
						});
						$(function() {
							var table = $('#captureDataTable').DataTable();
							$('#captureDataTable tbody').on('click', 'td',
									function() {
										popup(table, this);
									});
						});

						$("#processAllButton").click(function(env) {
							if (checkedRows.length == 0) {
								alert("Please select elements to proceed");
								return false;
							}
							processAll();
						});
						$("#selectAllCheckBox").click(function(env) {
							handleSelectALLClick(this);
						});

						$(function() {
							var table = $('#captureDataTable').DataTable();
							$('#captureDataTable tbody')
									.on(
											'click',
											'td',
											function() {
												var columnIndex = table.cell(
														this).index().column;
												var rowIndex = table.cell(this)
														.index().row;
												var rowNodes = table.row(
														rowIndex).node();
												var rowData = table.row(
														rowIndex).data();
												if (columnIndex == 0) {
													handleSingleCheckBoxClick(
															rowNodes, rowData);
												}
											});
							$(document).ready(function() {
								$('#example').DataTable({
									dom : 'B',
									buttons : [ 'csv' ]
								});
							});

							$('#captureDataTable tbody')
									.on(
											'click',
											'button[type="button"]',
											function() {
												var columnIndex = table.cell(
														this.parentNode)
														.index().column;
												var rowIndex = table.cell(
														this.parentNode)
														.index().row;
												var rowNodes = table.row(
														rowIndex).node();
												var rowData = table.row(
														rowIndex).data();
												if (columnIndex == 17) {
													process(table,
															this.parentNode);
												}
											});

							$('#captureDataTable').on('page.dt',
									function(event) {
										var tableObj = event.currentTarget;
										uncheckAllCheckBoxes(tableObj);
									});
						});

						$(function() {
							var table = $('#captureDataTable').DataTable();
							$('#captureDataTable tbody').on('click', 'td',
									function() {

										popup(table, this);
									});
						});
					});
	function renderTable() {
		var payId = document.getElementById("merchant").value;
		var table = new $.fn.dataTable.Api('#captureDataTable');
		//to show new loader --harpreet
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				toggleAjaxLoader();
			},
			complete : function() {
				toggleAjaxLoader();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#captureDataTable')
				.dataTable(
						{
							"footerCallback" : function(row, data, start, end,
									display) {
								var api = this.api(), data;

								// Remove the formatting to get integer data for summation
								var intVal = function(i) {
									return typeof i === 'string' ? i.replace(
											/[\,]/g, '') * 1
											: typeof i === 'number' ? i : 0;
								};

								// Total over all pages
								total = api.column(10).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(10, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(10).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');
							},
							"columnDefs" : [ {
								className : "dt-body-right",
								"targets" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
							} ],
							dom : 'BTftlpi',
							buttons : [
									{
										extend : 'copyHtml5',
										//footer : true,
										exportOptions : {
											columns : [ 19, 2, 3, 4, 5, 6, 7,
													8, 9, 10 ]
										}
									},
									{
										extend : 'csvHtml5',
										title : 'Capture Transactions',
										//footer : true,
										exportOptions : {
											columns : [ 19, 2, 3, 4, 5, 6, 7,
													8, 9, 10 ]
										}
									},
									{
										extend : 'pdfHtml5',
										orientation : 'landscape',
										title : 'Capture Transactions',
										//footer : true,
										exportOptions : {
											columns : [ 1, 2, 3, 4, 5, 6, 7, 8,
													9, 10 ]
										}
									},
									{
										extend : 'print',
										title : 'Capture Transactions',
										//footer : true,
										exportOptions : {
											columns : [ 1, 2, 3, 4, 5, 6, 7, 8,
													9, 10 ]
										}
									},
									{
										extend : 'colvis',
										//           collectionLayout: 'fixed two-column',
										columns : [ 1, 2, 3, 4, 5, 6, 7, 8, 9,
												10 ]
									} ],
							"ajax" : {
								"url" : "captureTransactionAction",
								"type" : "POST",
								data : function(d) {
									return generatePostData(d);
								}
							},
							"searching" : false,
							"processing" : true,
							"serverSide" : true,
							"paginationType" : "full_numbers",
							"lengthMenu" : [ [ 10, 25, 50, -1 ],
									[ 10, 25, 50, "All" ] ],
							//"order" : [ [ 1, "desc" ] ],
							"order" : [],
							"columnDefs" : [ {
								"type" : "html-num-fmt",
								"targets" : 4,
								"orderable" : false,
								"targets" : [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
							} ],
							"columns" : [
									{
										"targets" : 0,
										"searchable" : false,
										"orderable" : false,
										"width" : '1%',
										"data" : null,
										/*   'className': 'dt-body-center', */
										'render' : function(data, type, row,
												meta) {
											var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
											if (userType == "ADMIN"
													|| userType == "SUBADMIN"
													|| userType == "MERCHANT") {
												var acquirer = row.acquirer;
												var checkbox = '';
												if (acquirer == "DIRECPAY") {
													checkbox = '<input type="checkbox" id='+row.transactionId+' disabled="true">';
												} else {
													var refundableAmount = row.refundableAmount;
													var txnAmount = row.approvedAmount;
													if (txnAmount == refundableAmount) {
														checkbox = '<input type="checkbox" id='+row.transactionId+' >';
														//implement some css also
													} else {
														checkbox = '<input type="checkbox" id='+row.transactionId+' disabled="true" >';
													}

												}
											} else {
												return "";
											}
											return checkbox;
										}
									},
									{
										"data" : "transactionId",
										"width" : '9%',
										"className" : "my_class"
									},
									{
										"data" : "txnDate",
										"width" : '10%'
									},
									{
										"data" : "orderId",
										"width" : '10%'
									},
									{
										"data" : "businessName"
									},
									{
										"data" : "customerEmail"
									},
									{
										"data" : "customerName",
										"visible" : true
									},
									{
										"data" : "paymentMethod",
										"render" : function(data, type, full) {
											return full['paymentMethod'] + ' '
													+ '-' + ' '
													+ full['mopType'];
										}
									},
									{
										"data" : "cardNo",
										"visible" : true
									},
									{
										"data" : "currencyName",
										"width" : '6%'
									},
									{
										"data" : "approvedAmount",
										"width" : '9%',
										"float" : "right",
										"padding-right" : "0px"
									},

									{
										"data" : null,
										"className" : "center",
										"width" : '8%',
										"orderable" : false,
										"mRender" : function(row) {
											if (row.refundableAmount == "0.00") {
												return '<button class="btn btn-info btn-xs btn-block" style="font-size:10px; background: linear-gradient(60deg, #425185, #4a9b9b);" >Refunded</button>';
											} else if (parseFloat(row.refundableAmount) < parseFloat(row.approvedAmount)) {
												return '<button class="btn btn-info btn-xs btn-block" style="font-size:10px; background: linear-gradient(60deg, #425185, #4a9b9b);" >Partially Refunded</button>';
											} else {
												return '<button class="btn btn-info btn-xs btn-block" style="font-size:10px;background: linear-gradient(60deg, #425185, #4a9b9b);">Refund</button>';
											}
										}
									},
									{
										"data" : "payId",
										"visible" : false,
										"className" : "displayNone"
									},
									{
										"data" : "txnType",
										"visible" : false,
										"className" : "displayNone"
									},
									{
										"data" : "status",
										"visible" : false,
										"className" : "displayNone"
									},
									{
										"data" : "responseMsg",
										"visible" : false,
										"className" : "displayNone"
									},
									{
										"data" : "productDesc",
										"visible" : false,
										"className" : "displayNone"
									},
									{
										"data" : "refundableAmount",
										"visible" : false,
										"className" : "displayNone"
									},
									{
										"data" : "mopType",
										"visible" : false,
										"className" : "displayNone"
									},

									{
										"data" : null,
										"visible" : false,
										"className" : "displayNone",
										"mRender" : function(row) {
											return "\u0027" + row.transactionId;
										}
									}, {
										"data" : "internalCardIssusserBank",
										"visible" : false,
										"className" : "displayNone"
									}, {
										"data" : "internalCardIssusserCountry",
										"visible" : false,
										"className" : "displayNone"
									} ]
						});
		$("#merchant").select2({
		//data: payId
		});
	}

	function reloadTable(retainPageFlag) {
		$('#selectAllCheckBox').attr('checked', false);
		checkedRows = [];
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#captureDataTable');
		var table = tableObj.DataTable();
		if (retainPageFlag) {
			//set page dummy callback function
			table.ajax.reload(setPage(), false);
		} else {
			table.ajax.reload();
		}

	}
	//dummy callback
	function setPage() {
	}

	function popup(table, index) {
		var rows = table.rows();
		var columnVisible = table.cell(index).index().columnVisible;
		var columnNumber = table.cell(index).index().column;
		var token = document.getElementsByName("token")[0].value;
		var rowIndex = table.cell(index).index().row;
		var rowData = table.row(rowIndex).data();
		var transactionId = table.cell(rowIndex, 1).data();
		document.getElementById('txnId').value = table.cell(rowIndex, 1).data();
		var date_from = table.cell(rowIndex, 2).data();
		var currency = table.cell(rowIndex, 9).data();
		var amount = table.cell(rowIndex, 10).data();
		var paymentMethod = table.cell(rowIndex, 7).data();
		var orderId = table.cell(rowIndex, 3).data();
		document.getElementById('orderId').value = table.cell(rowIndex, 3)
				.data();
		var cust_email = table.cell(rowIndex, 5).data();
		if (cust_email == null) {
			cust_email = "Not available";
		}
		document.getElementById('payId').value = table.cell(rowIndex, 12)
				.data();
		var txnType = table.cell(rowIndex, 13).data();
		var status = table.cell(rowIndex, 14).data();
		var cust_name = table.cell(rowIndex, 6).data();
		var message = table.cell(rowIndex, 15).data();
		var card_number = table.cell(rowIndex, 8).data();
		var productDesc = table.cell(rowIndex, 16).data();
		var mopType = table.cell(rowIndex, 18).data();
		var internalCardIssusserBank = table.cell(rowIndex, 20).data();
		var internalCardIssusserCountry = table.cell(rowIndex, 21).data();

		if (columnVisible == 1) {
			$
					.post(
							"customerAddressAction",
							{
								orderId : decodeVal(orderId),
								custEmail : decodeVal(cust_email),
								datefrom : decodeVal(date_from),
								amount : decodeVal(amount),
								currency : decodeVal(currency),
								productDesc : decodeVal(productDesc),
								transactionId : decodeVal(transactionId),
								cardNumber : decodeVal(card_number),
								paymentMethod : decodeVal(paymentMethod),
								mopType : decodeVal(mopType),
								internalCardIssusserBank : decodeVal(internalCardIssusserBank),
								internalCardIssusserCountry : decodeVal(internalCardIssusserCountry),
								status : decodeVal(status),
								token : token,
								transactionAuthenticationFlag : true,
								"struts.token.name" : "token"
							}).done(function(data) {
						var popupDiv = document.getElementById("popup");
						popupDiv.innerHTML = data;
						decodeDiv();
						$('#popup').popup('show');
					});
		} else if (columnNumber == 11) {
			document.refundDetails.submit();
		}
	}

	function decodeDiv() {
		var divArray = document.getElementsByTagName('div');
		for (var i = 0; i < divArray.length; ++i) {
			var div = divArray[i];
			if (div.id.indexOf('param-') > -1) {
				var val = div.innerHTML;
				div.innerHTML = decodeVal(val);
			}
		}
	}

	function decodeVal(value) {
		var txt = document.createElement("textarea");
		txt.innerHTML = value;
		return txt.value;
	}

	function generatePostData(a) {
		var token = document.getElementsByName("token")[0].value;
		var payId = document.getElementById("merchant").value;
		if (payId == '') {
			payId = 'ALL'
		}
		var obj = {
			acquirer : document.getElementById("acquirer").value,
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			paymentType : document.getElementById("paymentMethods").value,
			payId : payId,
			currency : document.getElementById("currency").value,
			draw : a.draw,
			length : a.length,
			start : a.start,
			token : token,
			"struts.token.name" : "token",
		}

		return obj;
	}
</script>
<style>
.dataTables_wrapper {
	position: relative;
	clear: both;
	*zoom: 1;
	zoom: 1;
	margin-top: -30px;
}
</style>
</head>
<body>
	<div id="popup"></div>
	<div class="card ">
		<div class="card-header card-header-rose card-header-text">
			<div class="card-text">
				<h4 class="card-title">Sale Transactions</h4>
			</div>
			<div class="card-body ">
				<div class="container">

					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						<div class="form-group col-md-2 col-xs-6 col-sm-3 txtnew">

							<div class="txtnew">
								<label for="acquirer">Account:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()" listKey="code"
									listValue="name" onchange="handleChange();" autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-2 col-xs-3 txtnew"
							style="display: none;">


							<div class="txtnew">
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()" listKey="code"
									listValue="name" onchange="handleChange();" autocomplete="off" />
							</div>

						</div>
					</s:else>
					<div class="form-group col-md-2 txtnew col-sm-3 col-xs-6">
						<label for="merchant">Merchant:</label> <br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN' || #session.USER_TYPE.name()=='ACQUIRER'}">
							<s:select name="merchant" class="form-control" id="merchant"
								headerKey="" headerValue="ALL" list="merchantList"
								listKey="payId" listValue="businessName"
								onchange="handleChange();" autocomplete="off" />
						</s:if>
						<s:else>
							<s:select name="merchant" class="form-control" id="merchant"
								list="merchantList" listKey="payId" headerKey=""
								headerValue="ALL" listValue="businessName"
								onchange="handleChange();" autocomplete="off" />
						</s:else>
					</div>


					<div class="form-group  col-md-2 col-sm-3 txtnew  col-xs-6">
						<label for="email">Payment Method:</label> <br />
						<s:select headerKey="All" headerValue="All" class="form-control"
							list="@com.kbn.commons.util.PaymentType@values()"
							name="paymentMethods" id="paymentMethods"
							onchange="handleChange();" autocomplete="off" value="code"
							listKey="code" listValue="name" />
					</div>

					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="currency">Currency:</label> <br />
						<s:select name="currency" id="currency" headerValue="ALL"
							headerKey="ALL" list="currencyMap" class="form-control"
							onchange="handleChange();" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" readonly="true" id="dateFrom"
							name="dateFrom" class="form-control" autocomplete="off"
							onchange="handleChange();" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" onchange="handleChange();"
							autocomplete="off" />
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="card ">		
	<table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left"><s:actionmessage /></td>
		</tr>

		<tr>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td style="padding: 10px;"><div class="scrollD">
					<table id="captureDataTable" class="display compact"
						cellspacing="0" width="100%">
						<thead>
							<tr class="boxheadingsmall">
								<s:if
									test="%{(#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER.UserType.name()=='MERCHANT')}">
									<th style='text-align: left'><input type="checkbox"
										name="selectAllCheckBox" id="selectAllCheckBox"></input></th>
								</s:if>
								<s:else>
									<th style='text-align: center'></th>
								</s:else>
								<th style='text-align: center'>Txn Id</th>
								<th style='text-align: center'>Date</th>
								<th style='text-align: center'>Order Id</th>
								<th style='text-align: center'>Business Name</th>
								<th style='text-align: center'>Customer Email</th>
								<th style='text-align: center'>Customer Name</th>
								<th style='text-align: center'>Payment Method</th>
								<th style='text-align: center'>Card Detail</th>
								<th style='text-align: center'>Currency</th>
								<th style='text-align: right'>Txn Amount</th>
								<s:if
									test="%{(#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='MERCHANT')}">
									<th style='text-align: right'><input type="button"
										id="processAllButton" value="Refund All"
										class="greenrefundbtn"
										style="width: 70px; background: linear-gradient(60deg, #425185, #4a9b9b);"></input></th>
								</s:if>
								<s:else>
									<th style='text-align: center'></th>
								</s:else>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th>Txn Id</th>
								<th></th>
								<th></th>


							</tr>
						</thead>
						<tfoot>
							<tr class="boxheadingsmall">
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th style='text-align: right; padding-right: 5px;'>Total</th>
								<th style='text-align: right; float: right; padding-right: 5px;'></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>

							</tr>
						</tfoot>
					</table>

				</div></td>
		</tr>
	</table>
	</div>
	<s:form name="refundDetails" action="refundConfirmAction">
		<s:hidden name="orderId" id="orderId" value="" />
		<s:hidden name="payId" id="payId" value="" />
		<s:hidden name="transactionId" id="txnId" value="" />
		<s:hidden name="token" value="%{#session.customToken}" />
	</s:form>

	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0" class="txnf product-spec">
		<tr>
			<th colspan="3" align="left">UPLOAD REFUND FILE</th>
		</tr>
		<tr>
			<td width="46%" height="50" align="left" valign="bottom"><table
					id="example" style="display: none;">
					<thead>

						<tr>
							<th>origTxnId</th>
							<th>txnType</th>
							<th>amount</th>
							<th>payId</th>
							<th>currencyCode</th>
							<th>orderId</th>

						</tr>
					</thead>
				</table> Simple CSV File Format</td>
			<s:form action="uploadRefund" method="POST"
				enctype="multipart/form-data">
				<script type="text/javascript">
					$("body")
							.on(
									"click",
									"#btnUpload",
									function() {
										var allowedFiles = [ ".csv" ];
										var fileUpload = $("#fileUpload");
										var lblError = $("#lblError");
										var regex = new RegExp(
												"([a-zA-Z0-9\s_\\.\-:])+("
														+ allowedFiles
																.join('|')
														+ ")$");
										if (!regex.test(fileUpload.val()
												.toLowerCase())) {
											lblError
													.html("Please upload files having extensions: <b>"
															+ allowedFiles
																	.join(', ')
															+ "</b> only.");
											return false;
										}
										lblError.html('');
										return true;
									});
				</script>
				<td width="29%" align="center" valign="middle"><div
						class="input-group">
						<span class="input-group-btn"> <input type="text"
							class="inputfieldsmall" id="fileUpload" readonly> <span
							class="file-input btn btn-success btn-file btn-small"> <span
								class="glyphicon glyphicon-folder-open"
								style="background: linear-gradient(60deg, #425185, #4a9b9b);"></span>
								&nbsp;&nbsp;Browse <s:file name="fileName" />
						</span>
						</span><span id="lblError" style="color: red;"></span> <br />
					</div></td>
				<td width="25%" align="center" valign="middle"><s:submit
						value="Upload" name="fileName" id="btnUpload"
						class="btn btn-success btn-sm" /></td>
				<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
			</s:form>
		</tr>
	</table>
	<script>
		$(document).on(
				'change',
				'.btn-file :file',
				function() {
					var input = $(this), numFiles = input.get(0).files ? input
							.get(0).files.length : 1, label = input.val()
							.replace(/\\/g, '/').replace(/.*\//, '');
					input.trigger('fileselect', [ numFiles, label ]);
				});

		$(document)
				.ready(
						function() {
							$('.btn-file :file')
									.on(
											'fileselect',
											function(event, numFiles, label) {

												var input = $(this).parents(
														'.input-group').find(
														':text'), log = numFiles > 1 ? numFiles
														+ ' files selected'
														: label;

												if (input.length) {
													input.val(log);
												} else {
													if (log)
														alert(log);
												}

											});
						});
	</script>
</body>
</html>