<%@taglib prefix="s" uri="/struts-tags"%>
<link href="../css/bootstrap.minr.css" rel="stylesheet">
<link href="../fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="../css/default.css" rel="stylesheet">
<link href="../css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="../css/navigation.css">
<link href="../css/welcomePage.css" rel="stylesheet">
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.dimensions.js"></script>
<script type="text/javascript" src="../js/jquery.accordion.js"></script>
<link rel="stylesheet" href="../css/loader.css">
<link rel="stylesheet" href="../css/newtheam.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<script>
	$.noConflict();
	// Code that uses other library's $ can follow here.
</script>
<script type="text/javascript">
	jQuery().ready(function() {
		// simple accordion
		jQuery('#list1a').accordion();
		jQuery('#list1b').accordion({
			autoheight : false
		});

		// second simple accordion with special markup
		jQuery('#navigation').accordion({
			active : false,
			header : '.head',
			navigation : true,
			event : 'click',
			fillSpace : false,
			animated : 'easeslide'
		});
	});
</script>

<div id="fade"></div>
<div id="modal" class="lodinggif">
	<img id="loader" src="../image/loader.gif" />
</div>
<div class="row leftnavigation" >
	<div class="col-md-12 col-xs-12">
		<nav class="navbar navbar-default top-navbar" role="navigation">
		
		<div class="headerImage">
		<a class="" href="home"> <img
				src="../image/logo-newr.png" class=""></a>
				</div>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				
			</div>

			<ul class="nav navbar-top-links navbar-right">

				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#" aria-expanded="false"> <i
						class="fa fa-user fa-fw"></i><b>Welcome <s:property
								value="%{#session.USER.businessName}" /></b> <i
						class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><s:a action="logout">
								<i class="fa fa-sign-out fa-fw"></i> Logout</s:a></li>
					</ul> <!-- /.dropdown-user --></li>
				<!-- /.dropdown -->
			</ul>
		</nav>
	</div>
</div>
<div class="col-md-2 left_col col-xs-12" id="wrapperr">

	<!-- sidebar menu -->

	<div>
		<!--/. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation">
         

			<div id="main" style="height: 100%">
				<div class="sidebar-collapse" style="height: 100%">
					<ul id="navigation" class="nav side-menu side-menubottom">
						<li><s:a action='home' class="head1">
								<i class="material-icons">dashboard</i> Dashboard</s:a></li>
							 <s:if test="%{#session['USER_PERMISSION'].contains('View MerchantSetup') || #session['USER_PERMISSION'].contains('Create Mapping')}">	
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons">account_balance_wallet</i> Merchant Setup <span
								class="fa fa-chevron-down"></span></a> <!--<ul class="nav child_menu" style="display: none">-->

							<ul>
							<s:if test="%{#session['USER_PERMISSION'].contains('View MerchantSetup')}">
								<li><s:a action='merchantCrmSignup'>User Registration</s:a></li>
								<li><s:a action='merchantList'
										>Merchant Account</s:a></li>
								<li><s:a action='merchantSetup' class="sublinks"
										onclick='return false'>Merchant Setup</s:a></li>
								<li><s:a action='adminResellers'>Reseller Account</s:a></li>
								<li><s:a action='merchantSubUsers'>Merchant SubUsers</s:a></li>
									<li><s:a action='ruleEngine'>Rule Engine</s:a></li>
								<li><s:a action='routerConfiguration'>Smart router</s:a></li>
								<li><s:a action='#'>Smart router Audit Trail</s:a></li>
								<li><s:a action='#'>Rule Engine Audit Trail</s:a></li>
								</s:if>
							</ul></li>
							<li><a style="cursor: pointer" class="head"><i
								class="material-icons">account_balance</i> Merchant Billing <span
								class="fa fa-chevron-down"></span></a> 

							<ul>
							<s:if test="%{#session['USER_PERMISSION'].contains('View Merchant Billing')}">
							<li><s:a action='mopSetUpAction'>Merchant Mapping</s:a></li>
								<li><s:a action='serviceTaxPlatform'>GST</s:a></li>
								<li><s:a action='chargingPlatform'>TDR Setting</s:a></li>
								<li><s:a action='surchargePlatform'>Surcharge Setting</s:a></li>
								<li><s:a action='resellerMappingAction'>Reseller Mapping</s:a></li>
								</s:if>
								 <li><s:a action='pendingRequest'
										>Pending Request</s:a></li> 	
							</ul></li>
							</s:if>
								<s:if test="%{#session['USER_PERMISSION'].contains('View Analytics')}">
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons">timeline</i>Analytics<span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="analyticsReports" >Payment Methods</s:a></li>
								<li><s:a action="weeklyAnalytics"
										onclick="ajaxindicatorstart1()">Weekly Analytics</s:a></li>
							</ul></li>
							</s:if>
						<s:if test="%{#session['USER_PERMISSION'].contains('View Transactions')}">
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons">repeat</i>Transaction Reports <span
								class="fa fa-chevron-down"></span></a>
							<ul>
							<li><s:a action='authorizeTransaction'>Authorized Transactions</s:a></li>
								<li><s:a action='captureTransaction'> Sale Transactions</s:a></li>
								<li><s:a action='failedTransaction'>Failed Transactions</s:a></li>
								<li><s:a action='refundReport'>Refund Transactions</s:a></li>
								<li><s:a action='fraudTransaction'>Risk Transactions</s:a></li>
								<li><s:a action='summaryReport'>Summary Reports</s:a></li>
								<li><s:a action="settlementReport">Settled Report </s:a></li>
							</ul></li>
							</s:if>
							   <s:if test="%{#session['USER_PERMISSION'].contains('View SearchPayment')}">
						<li><s:a action='transactionSearch' class="head1">
								<i class="material-icons">search</i>  Search Payment </s:a></li>
	                         </s:if>
	                      <s:if test="%{#session['USER_PERMISSION'].contains('View Reports')}">
						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-pencil-square-o"></i> Account & Finance <span
								class="fa fa-chevron-down"></span></a>
							<ul>
							
						<li><s:a action='misReports'
										>MIS Reports</s:a></li>	
						<li><s:a action='downloadReports'>Download reports</s:a></li>
							</ul></li>
                          </s:if>
                    <%--           <s:if test="%{#session['USER_PERMISSION'].contains('View Reconciliation')}">
						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-compress"></i> Reconciliation <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="settlementReport"
										>Settlement Report </s:a></li>
							</ul></li>
							</s:if> --%>
                       <s:if test="%{#session['USER_PERMISSION'].contains('Create Invoice') || #session['USER_PERMISSION'].contains('View Invoice')}">
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons">link</i>Link Payment<span
								class="fa fa-chevron-down"></span></a>
							<ul>
							<s:if test="%{#session['USER_PERMISSION'].contains('Create Invoice')}">
							<li><s:a action="createPaymentLink">Create Payment Link</s:a></li>
							
								</s:if>
					         <s:if test="%{#session['USER_PERMISSION'].contains('View Invoice')}">
								<li><s:a action="invoiceSearch">Search PaymentLink</s:a></li>
										</s:if>
							</ul></li>
							</s:if>
							<li><a style="cursor:pointer" class="head"><i class="fa fa-users"></i> Manage SubAdmin <span class="fa fa-chevron-down"></span></a>
                                    <ul>
                                      <li><s:a action="addUser" >Add SubAdmin </s:a>
                                      </li>
                                      <li><s:a action="searchSubAdmin" >SubAdmin List</s:a>
                                      </li> 
                                    </ul>
                                </li>
							   <s:if test="%{#session['USER_PERMISSION'].contains('View Remittance')}">
						        <li><a style="cursor: pointer" class="head"><i
								class="fa fa-compress"></i> Remittance <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="viewRemittance"
										>View Remittance </s:a></li>
								<li><s:a action="addRemittance"
										>Add Remittance</s:a></li>
							</ul></li>
                            </s:if>
                           <s:if test="%{#session['USER_PERMISSION'].contains('Create BatchOperation')}">
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons">apps</i> Batch Operations <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="kotakRefund"
										>Kotak </s:a></li>
								<li><s:a action="yesBankRefund"
										>Yes Bank</s:a></li>
							</ul></li>
							</s:if>
					<s:if test="%{#session['USER_PERMISSION'].contains('Fraud Prevention')}">
					<li><a style="cursor: pointer" class="head"><i
								class="material-icons">location_disabled</i> Fraud Prevention System <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="adminRestrictions"	>Restrictions</s:a></li>								
							</ul></li> 
							</s:if>
							<s:if test="%{#session['USER_PERMISSION'].contains('Create BulkEmail')}">
					 <li><a style="cursor:pointer" class="head"><i class="fa fa-users"></i>Bulk Email/Sms <span class="fa fa-chevron-down"></span></a>
                                    <ul>
                                      <li><s:a action="bulkEmail" >Send Bulk Email</s:a>
                                        </li>
                                         <li><s:a action="bulkSms" >Send Bulk Sms</s:a>
                                        </li>
                                    </ul>
                                </li>
                                </s:if>
                             <s:if test="%{#session['USER_PERMISSION'].contains('View CashBack')}">
							<li><a style="cursor: pointer" class="head"><i
								class="fa fa-circle-o-notch"></i> Chargeback Case<span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="viewChargeback">View Chargeback</s:a></li>								
							</ul></li>
							</s:if>  
							<li><a style="cursor: pointer" class="head"><i
								class="fa fa-user"></i> My Account <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="adminProfile"
										>My Profile </s:a></li>
								<li><s:a action="loginHistoryRedirect"
										>Login History</s:a></li>
								<li><s:a action='passwordChange'
										>Change Password</s:a></li>
							</ul></li>
							<!-- ticketing -->
							 <s:if test="%{#session['USER_PERMISSION'].contains('Create HelpTicket')}">
								<li><a style="cursor: pointer" class="head"><i
								class="fa fa-list-alt"></i>Help Ticket <span
								class="fa fa-chevron-down"></span></a>
							<ul>
							<li><s:a action="createTicket" >
								 Create Tickets </s:a>
								</li>
							<li><s:a action="viewAllTickets" >
								 View All Tickets </s:a>
								</li>
								</ul>
								</li>
								</s:if>
					</ul>
				</div>
			</div>




		</nav>
		<!-- /. NAV SIDE  -->
		<!-- /. PAGE WRAPPER  -->
	</div>


	<!-- /sidebar menu -->

</div>

<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->

<script src="../js/bootstrap.min.js"></script>
<script>
	;
	(function($, window, document, undefined) {

		var pluginName = "metisMenu", defaults = {
			toggle : true
		};

		function Plugin(element, options) {
			this.element = element;
			this.settings = $.extend({}, defaults, options);
			this._defaults = defaults;
			this._name = pluginName;
			this.init();
		}

		Plugin.prototype = {
			init : function() {

				var $this = $(this.element), $toggle = this.settings.toggle;

				$this.find('li.active').has('ul').children('ul').addClass(
						'collapse in');
				$this.find('li').not('.active').has('ul').children('ul')
						.addClass('collapse');

				$this.find('li').has('ul').children('a').on(
						'click',
						function(e) {
							e.preventDefault();

							$(this).parent('li').toggleClass('active')
									.children('ul').collapse('toggle');

							if ($toggle) {
								$(this).parent('li').siblings().removeClass(
										'active').children('ul.in').collapse(
										'hide');
							}
						});
			}
		};

		$.fn[pluginName] = function(options) {
			return this.each(function() {
				if (!$.data(this, "plugin_" + pluginName)) {
					$.data(this, "plugin_" + pluginName, new Plugin(this,
							options));
				}
			});
		};

	})(jQuery, window, document);
</script>
<script>
	(function($) {
		"use strict";
		var mainApp = {

			initFunction : function() {
				/*MENU 
				------------------------------------*/
				$('#main-menu').metisMenu();

				$(window).bind("load resize", function() {
					if ($(this).width() < 768) {
						$('div.sidebar-collapse').addClass('collapse')
					} else {
						$('div.sidebar-collapse').removeClass('collapse')
					}
				});

			},

			initialization : function() {
				mainApp.initFunction();

			}

		}
		// Initializing ///

		$(document).ready(function() {
			mainApp.initFunction();
		});

	}(jQuery));
</script>
