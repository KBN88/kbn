<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Invoice Search</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link rel="stylesheet" href="../css/loader.css">
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script type="text/javascript" src="../js/pdfmake.js"></script>
<link href="../css/font-awesome.min.css" rel="stylesheet">
<link href="../css/linkpayment.css" rel="stylesheet">

	<script src="../js/messi.js"></script>
	<link href="../css/messi.css" rel="stylesheet" />
	<script src="../js/commanValidate.js"></script>
	<script type="text/javascript">
	function handleChange() {
		reloadTable();
	}
	$(document).ready(function() {
						$(function() {

							$("#dateFrom").datepicker({
								prevText : "click for previous months",
								nextText : "click for next months",
								showOtherMonths : true,
								dateFormat : 'dd-mm-yy',
								selectOtherMonths : false,
								maxDate : new Date()
							});
							$("#dateTo").datepicker({
								prevText : "click for previous months",
								nextText : "click for next months",
								showOtherMonths : true,
								dateFormat : 'dd-mm-yy',
								selectOtherMonths : false,
								maxDate : new Date()
							});
						});
						$(function() {
							var today = new Date();
							$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
							$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
							renderTable();
						});

						$(function() {
							var table = $('#invoiceDataTable').DataTable();
							$('#invoiceDataTable tbody').on('click', 'td', function() {

								popup(table, this);
							});
						});
	});
	function renderTable() {
		//to show new loader -Harpreet
		 $.ajaxSetup({
	            global: false,
	            beforeSend: function () {
	            	toggleAjaxLoader();
	            },
	            complete: function () {
	            	toggleAjaxLoader();
	            }
	        });
		var table = new $.fn.dataTable.Api('#invoiceDataTable');
		 $.ajaxSetup({
	            global: false,
	            beforeSend: function () {
	               $(".modal").show();
	            },
	            complete: function () {
	                $(".modal").hide();
	            }
	        });
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		
		$('#invoiceDataTable')
		.dataTable(
				{
					dom : 'BTftlpi',
					buttons : [ {
						extend : 'copyHtml5',
						exportOptions : {
							columns : [ 11, 1, 2, 3, 4, 5, 6, 7, 8 ]
						}
					}, {
						extend : 'csvHtml5',
						title : 'Invoice Search',
						exportOptions : {
							columns : [ 11, 1, 2, 3, 4, 5, 6, 7, 8 ]
						}
					}, {
						extend : 'pdfHtml5',
						orientation : 'landscape',
						title : 'Invoice Search',
						exportOptions : {
							columns : [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
						}
					}, {
						extend : 'print',
						title : 'Invoice Search',
						exportOptions : {
							columns : [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
						}
					}, {
						extend : 'colvis',
						//           collectionLayout: 'fixed two-column',
						columns : [ 1, 2, 3, 4, 5, 6, 7, 8 ,9]
					} ],			
					"ajax" : {
						"url" : "invoiceSearchAction",
						"type" : "POST",
						"data" : generatePostData
					},
					"bProcessing" : true,
					"bDestroy" : true,
					"bLengthChange" : true,
					"iDisplayLength" : 10,
					"order" : [ [
							1,
							"desc" ] ],
					"aoColumns" : [
							{
								"data" : "invoiceId",
								"sClass" : "my_class",
							},
							{
								"data" : "createDate",
								"sWidth" : '15%',
								/* "render" : function(
										Data) {
									var date = new Date(
											Data);
									var month = date
											.getMonth() + 1;
									var hour = date.getHours();
									hour = hour.toString();
									var mint = date.getMinutes();
									var sec = date.getSeconds();
									return (date
											.getDate()
											+ "-"
											+ (month.length > 1 ? month
													: "0"
															+ month)
											+ "-" + date
											.getFullYear()+ "T" + hour + ":" + mint + ":" + sec);
								}   */
							},
							{
								"data" : "businessName",
								"sWidth" : '15%'
							},
							{
								"data" : "name",
								"sWidth" : '15%'
							},
							{
								"data" : "email",
								"sWidth" : '15%'
							},
							{
								"data" : "invoiceNo",
								"sWidth" : '10%'
							},
							{
								"data" : "currencyCode",
								"sWidth" : '5%'
							},
							{
								"data" : "totalAmount",
								"sWidth" : '10%'
							},
							{
								"data" : "invoiceType",
								"sWidth" : '10%'
							},
							{
								"mData" : null,
								"sClass" : "center",
								"bSortable" : false,
								"mRender" : function() {
									return '<button class="btn btn-info btn-xs">SMS</button>';
								}
							},
							{
								"mData" : null,
								"sClass" : "center",
								"bSortable" : false,
								"mRender" : function() {
									return '<button class="btn btn-info btn-xs">Download QR</button>';
								}
							},{
								"data" : null,
								"className" : "center",
								"orderable" : false,
								"mRender" : function(row) {
									if (row.invoiceType == "INVOICE PAYMENT"){
										return '<button class="btn btn-info btn-xs">Email</button>';
									}
									else {
										return '<button  hidden="hidden"></button>';
									}
								}
							},{
								"data" : null,
								"sClass" : "my_class",
								"sWidth" : '10%',
								"visible" : false,
								"className" : "displayNone",
								"mRender" : function(row) {
						              return "\u0027" + row.invoiceId;
						       }
							}, ]
				});
	}
	function reloadTable() {		
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#invoiceDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}
	function popup(table, index) {
		var rows = table.rows();
		var columnVisible = table.cell(index).index().columnVisible;
		var columnNumber = table.cell(index).index().column;
		var token = document.getElementsByName("token")[0].value;
		var rowIndex = table.cell(index).index().row;
		var invId = table.cell(rowIndex, 0).data();
		var invType = table.cell(rowIndex, 8).data();
		
		if (columnVisible == 0) {
			var token  = document.getElementsByName("token")[0].value;
			$.ajax({
				url : "invoicePopup",
				type : "POST",
				data : {
					svalue : invId,
					token:token,
				    "struts.token.name": "token",
				},
				success : function(data) {
					var message = data;
					new Messi(message, {
						center : true,
						buttons : [ {
							id : 0,
							label : 'Close',
							val : 'X'
						} ],
						callback : function(val) {
							$('.messi,.messi-modal')
									.remove();
							var table = $('#invoiceDataTable')
							.DataTable();
							table.ajax.reload();
						},
						//width : '820px',
						modal : true
					});
				}
			});
		}else if(columnVisible == 9){
			var answer = confirm("Are you sure you want to send invoice link?");
			if (answer != true) {
				return false;
			}
				/* var answer = confirm("Are you sure you want to send invoice link?");
				if (answer != true) {
					return false;
				} */
				var token  = document.getElementsByName("token")[0].value;
				 $.ajaxSetup({
			            global: false,
			            beforeSend: function () {
			               $(".modal").show();
			            },
			            complete: function () {
			                $(".modal").hide();
			            }
			        });
				$.ajax({
					url : "invoiceSMSAction",
					type : "POST",
					data : {
						invoiceId : invId,
						token:token,
					    "struts.token.name": "token",
					},
					success : function(data) {
						alert('Invoice link has been send successfully through SMS !!');																		
					},
					error : function(data) {
						alert('Unable to send invoice link to Phone !!');																		
					}
				});
			
		}
		 		
		 if (columnVisible == 10) {
			var answer = confirm("Are you sure you want to download QR Code");
			if (answer != true) {
				return false;
			}
			 				 
				document.getElementById('invoiceId').value = invId;
			    document.downloadqrcode.submit();			   
	
			}
		 else if(columnVisible == 11){
				var answer = confirm("Are you sure you want to send invoice link?");
				if (answer != true) {
					return false;
				}
				if(invType == "INVOICE PAYMENT") {
					/* var answer = confirm("Are you sure you want to send invoice link?");
					if (answer != true) {
						return false;
					} */
					var token  = document.getElementsByName("token")[0].value;
					 $.ajaxSetup({
				            global: false,
				            beforeSend: function () {
				               $(".modal").show();
				            },
				            complete: function () {
				                $(".modal").hide();
				            }
				        });
					$.ajax({
						url : "invoiceEmailAction",
						type : "POST",
						data : {
							invoiceId : invId,
							token:token,
						    "struts.token.name": "token",
						},
						success : function(data) {
							alert('Invoice link has been send successfully !!');																		
						},
						error : function(data) {
							alert('Unable to send invoice link !!');																		
						}
					});
				}

			}
	}
	
		function decodeDiv() {
			var divArray = document.getElementsByTagName('div');
			for (var i = 0; i < divArray.length; ++i) {
				var div = divArray[i];
				if (div.id.indexOf('param-') > -1) {
					var val = div.innerHTML;
					div.innerHTML = decodeVal(val);
				}
			}
		}

		function decodeVal(value) {
			var txt = document.createElement("textarea");
			txt.innerHTML = value;
			return txt.value;
		}
	function generatePostData() {
		var token = document.getElementsByName("token")[0].value;
		var obj = {
				invoiceNo : document.getElementById("invoiceNo").value,
		customerEmail : document.getElementById("customerEmail").value,
		merchantEmailId : document.getElementById("merchants").value,
		currency : document.getElementById("currency").value,
		invoiceType : document.getElementById("invoiceType").value,
		dateFrom : document.getElementById("dateFrom").value,
		dateTo : document.getElementById("dateTo").value,
		token:token,
	    "struts.token.name": "token",
		};

		return obj;
	}
					
	</script>
	</head>
	<body id="mainBody">
	<div style="float:right;">
	<button id="myBtn" class="linkpayment" style="background: linear-gradient(60deg, #425185, #4a9b9b); color:#fff;font-size:14px"><i class="material-icons md-48">note_add</i>Create Payment Link</button>
<%-- 	<a class="btn btn-primary linkpayment" id="myBtn"><i class="material-icons md-48">note_add</i><span>Create Payment Link</span></a>
 --%>	</div>
    <table id="mainTable" width="100%" border="0" align="center"

		cellpadding="0" cellspacing="0" class="txnf">
      <tr>
        <td colspan="5" align="left"><h2>Invoice Search</h2>
        <div class="container">

<div class="form-group  col-md-1 col-sm-4 txtnew  col-xs-6">
    <label for="email">Invoice No.:</label>
    <br />
   <s:textfield id="invoiceNo" class="form-control"
									name="invoiceNo" type="text" value="" autocomplete="off" onkeypress="return Validate(event);"></s:textfield>
  </div>
  <div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
    <label for="email">Customer Email:</label>
    <br />
   <s:textfield id="customerEmail" class="form-control" name="customerEmail" type="text" value="" autocomplete="off" onblur="emailCheckSerachInvoice()"></s:textfield>
                                    
 <span id="error2"></span> </div>
  
<s:if
										test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
                                        <div class="form-group col-md-2 col-sm-4 col-xs-6 txtnew">
										<div class="txtnew">
												<label for="acquirer">Merchants:</label><br />
                                                <s:select name="merchants" class="form-control"
										id="merchants" headerKey="ALL" headerValue="ALL"
										list="merchantList" listKey="emailId" listValue="businessName"
										onchange="handleChange();" autocomplete="off" />
											</div>
									</div></s:if>
									
									 <s:else> <div class="form-group col-md-2 col-sm-4 col-xs-6 txtnew" style="display: none;">
									
									
										<div class="txtnew">
											<s:select name="merchants" class="form-control"
										id="merchants" list="merchantList" listKey="emailId"
										listValue="businessName" onchange="handleChange();" autocomplete="off" />
										</div>
									
                                    </div></s:else>
    
  
  <div class="form-group  col-md-1 col-sm-4 txtnew  col-xs-6">
    <label for="email">Currency:</label>
    <br />
    <s:select name="currency"
								id="currency" headerValue="ALL" headerKey="ALL"
								list="currencyMap" class="form-control" onchange="handleChange();" autocomplete="off"/>
  </div>
  <div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
    <label for="email">Payment Type:</label>
    <br />
    <s:select name="invoiceType"
								id="invoiceType" headerValue="ALL" headerKey="ALL"
									list="@com.kbn.commons.util.PromotionalPaymentType@values()" 
									listKey="name" listValue="name" class="form-control" onchange="handleChange();" autocomplete="off"/>
  </div>  
  <div class="form-group  col-md-2 col-sm-3 txtnew  col-xs-6">
    <label for="email">Date From:</label>
    <br />
   <s:textfield type="text" id="dateFrom" name="dateFrom"
									class="form-control" onchange="handleChange();" autocomplete="off" />
  </div>
    
  <div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
    <label for="dateFrom">Date To:</label>
    <br />
    <s:textfield type="text" id="dateTo" name="dateTo"
									class="form-control" onchange="handleChange();" autocomplete="off" />
  </div>  
</div>
</td>
      </tr>      
      <tr>
        <td colspan="5" align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" style="padding:10px;"><div class="scrollD">
            <table id="invoiceDataTable" class="display" cellspacing="0"
								width="100%">
            <thead>
                <tr class="boxheadingsmall">
                <th>Invoice Id</th>
                <th>Date</th>
                <th>Merchant</th>
                <th>Name</th>
                <th>Email</th>
                <th>Invoice No.</th>
                <th>Currency</th>
                <th>Amount</th>
                 <th>Type</th>
                <th>Action</th>
                <th>Action</th>
                <th>Action</th>
                <th>Invoice Id</th>
              </tr>
              </thead>
          </table>
          </div></td>
      </tr>
    </table>
    <s:form name="downloadqrcode" action="invoiceQRCodeAction">
		<s:hidden name="invoiceId" id="invoiceId" value="" />	
		<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
	</s:form> 
	<div id="myModal" class="modal modelPopup" style="display: block;padding-top:53px;">

  <!-- Modal content -->
  <div style="width:50%; margin: auto;">

    <div class="modal-body">
     	<s:form name="f1" action="saveInvoice" id='frmInvoice'
		autocomplete="off">
		<input type="hidden" id="emailCheck" name="emailCheck" value="false"></input>
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="txnf">
			<tr>
				<td align="left">
					<div style="display: none" id="response"></div>
					<table width="100%" border="0">
						<tr>
							<td width="82%" align="left"><h2>create Payment Link</h2>
							
							</td>
							<td width="25%" align="left">
								<div class="txtnew">

									<s:if
										test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER.UserType.name()=='RESELLER' || #session.USER_TYPE.name()=='SUPERADMIN'}">
										<s:select name="merchant" theme="simple" class="form-control"
											id="merchant" headerKey="Select Merchant" onchange="FieldValidator.valdMerchant()"
											headerValue="Select Merchant" list="merchantList"
											listKey="emailId" listValue="businessName" autocomplete="off" />
									</s:if>
								</div> <span id="merchantErr" class="invocspan"></span>
							</td>
							<td width="2%" align="center">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td height="30" colspan="5" align="left" valign="middle"><h3>
						Detail Information</h3></td>
			</tr>
			<tr>
				<td colspan="5" align="left" valign="top"><div class="MerchBx"
						style="background: #f2f2f2; border-radius: 5px; padding: 10px;float: left; width: calc(100% - 30px); margin: 0px 15px;">
					<div class="invo6">Invoice Number* <br />
					<div class="txtnew">
					<s:textfield type="text" class="textFL_merch" id="invoiceNo"
										name="invoiceNo" autocomplete="off"
										onkeyup="FieldValidator.valdInvoiceNo(false)" />
									<span id="invoiceNoErr" class="invocspan"></span>
								</div>
							</div>
					
				<div class="invo6">Customer Phone*<br />
				<div class="txtnew">
				<s:textfield type="text" id="phone" name="phone" maxlength="15"
										class="textFL_merch" autocomplete="off" onkeyup="FieldValidator.valdPhoneNo(false)"/>
								</div>
								<span id="phoneErr" class="invocspan"></span>
							</div>
			<div class="invo6">Customer Email*<br />
			<div class="txtnew">
			<s:textfield type="text" id="emailId" name="email"
										value="%{invoice.email}" class="textFL_merch" onkeyup="FieldValidator.valdEmail(false)"
										autocomplete="off" />
								</div>
								<span id="emailIdErr" class="invocspan"></span>
							</div>
					
						<div class="clear"></div>
					</div></td>
			</tr>
			<tr>
				<td colspan="5" align="left" valign="top">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="5" align="left" valign="top"><h3>Product Information</h3></td>
			</tr>
			<tr>
				<td colspan="5" align="left" valign="top">
				<div class="MerchBx"
						style="background: #f2f2f2; border-radius: 5px; padding: 10px">
						<div class="invoCont">
					
							<div class="productIn">
								Description<br />
								<div class="txtnew">
									<s:textfield type="text" class="textFL_merch" id="productDesc" onkeyup="FieldValidator.valdProductDesc(false)"
										name="productDesc" autocomplete="off" />
									<span id="productDescErr" class="invocspan"></span>
								</div>
							</div>
							<div class="productIn">
								Expire in days*<br />
								<div class="txtnew">
									<s:textfield type="number" class="textFL_merch" id="expiresDay"
										name="expiresDay" autocomplete="off" value="0" maxlenth="2" onkeyup="FieldValidator.valdExpDayAndHour(false)"/>
								</div>
								<span id="expiresDayErr" class="invocspan"></span>
							</div>
							<div class="productIn">
								Expire in hours* <br />
								<div class="txtnew">
									<s:textfield type="text" class="textFL_merch" id="expiresHour"
										name="expiresHour" autocomplete="off" value="1" maxlength="2" onkeyup="FieldValidator.valdExpDayAndHour(false)" />
								</div>
								<span id="expiresHourErr" class="invocspan"></span>
							</div>
							<div class="productIn">
								All prices are in*<br />
								<div class="txtnew">
									<s:select name="currencyCode" id="currencyCode"
										headerValue="Select Currency" headerKey="Select Currency"
										list="currencyMap" listKey="key" listValue="value"
										class="form-control" onchange="FieldValidator.valdCurrCode(false)"
										autocomplete="off" />
								</div>
								<span id="currencyCodeErr" class="invocspan"></span>
							</div>
							<div class="invoC1">
								<table width="97%" border="0" cellpadding="0" cellspacing="0"
									class="greyroundtble">
									<tr>
										<td width="6%" align="left" valign="middle"><h6>Quantity</h6></td>
										<td width="2%" align="center" valign="middle">&nbsp;</td>
										<td width="15%" align="left" valign="top"><h6>
												Amount*</h6></td>
										<td width="2%" align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="left" valign="top"><div class="txtnew">
												<s:textfield type="number" class="textFL_merch" value="1"
													id="quantity" name="quantity" onkeyup="sum();FieldValidator.valdQty(false);"
													autocomplete="off" />
											</div>
												<span id="quantityErr" class="invocspan"></span></td>
										<td align="center" valign="middle"><strong>x</strong></td>
										<td height="37" align="left" valign="top"><div
												class="txtnew">
												<s:textfield type="number" class="textFL_merch"
													onkeyup="sum();FieldValidator.valdAmount(false);" id="amount" name="amount"
													value="%{invoice.amount}"
													autocomplete="off" />
											</div> <span id="amountErr" class="invocspan"></span></td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="left" valign="top">&nbsp;</td>
										<td align="left" valign="middle">&nbsp;</td>
										<td align="left" valign="top" class="labelfont">Service
											Charge</td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td height="37" align="left" valign="top"><div
												class="txtnew">
												<s:textfield type="text" class="textFL_merch" value="0.00"
													id="serviceCharge" name="serviceCharge"
													onkeyup="sum();FieldValidator.valdSrvcChrg(false);"
													autocomplete="off" />
											</div>
											<span id="serviceChargeErr" class="invocspan"></span></td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="left" valign="top" class="labelfont">Total
											Amount</td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="left" valign="middle"><div class="txtnew">
												<s:textfield type="text" class="textFL_merch"
													readonly="true" placeholder="0.00" id="totalAmount"
													name="totalAmount" autocomplete="off" />
											</div></td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="clear"></div>
					</div></td>
			</tr>
		<!-- 	<tr>
				<td><h3>SMS Information</h3></td>
			</tr> -->
<%-- 			<tr>
				<td><div class="MerchBx"
						style="background: #f2f2f2; border-radius: 5px; padding: 10px">

						<div class="invoC">
							Recipient Mobile*<br />
							<s:textfield type="text" class="textFL_merch2"
								id="recipientMobile" name="recipientMobile" onkeyup="FieldValidator.valdRecptMobileNo(false)" autocomplete="off" />
							<span id="recipientMobileErr" class="invocspan"></span>
						</div>
					
						<div class="invo8">
							Message Body* <br />
							<s:textarea type="text" class="textFL_merch2" id="messageBody"
								name="messageBody" onkeyup="FieldValidator.valdRecptMsg(false)" autocomplete="off"
								placeholder="maximum 160 character" maxlength="160" />
						</div>
						<span id="messageBodyErr" class="invocspan"></span>
						<div class="clear"></div>
					</div></td>
			</tr> --%>
			<tr>
				<td align="center" valign="middle"><table width="97%"
						border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" valign="middle" height="40" class="bluelinkbig">
							<input id="invoiceLink" onkeydown="document.getElementById('copyBtn').focus();" type="text" style="display:none" class="textFL_merch" value= <s:property value="url" />>
							</td>
							
						</tr>
						<tr>
						<td align="center" valign="middle" height="40">
						<input type="button" style="margin-top:5px;display:none;" id="copyBtn" class="btn btn-success btn-medium" value="Copy Payment Link "/>
						</td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td align="center" valign="top"><table width="100%" border="0"
						cellpadding="0" cellspacing="0">
						<tr>
							<td width="15%" align="left" valign="middle"></td>
							<td width="5%" align="right" valign="middle"><s:submit
									id="btnSave" name="btnSave"
									value="create Payment Link" style="background: linear-gradient(60deg, #425185, #4a9b9b);color: #fff;font-size: 14px;width: 233px;height: 40px;">
								</s:submit></td>
							<td width="3%" align="left" valign="middle"></td>
							<td width="15%" align="left" valign="middle"></td>
						</tr>
					</table></td>
			</tr>
		</table>
		<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
	</s:form>
      
    </div>
  
  </div>
  
  <script type="text/javascript">
  var modal = document.getElementById("myModal");

//Get the button that opens the modal
var btn = document.getElementById("myBtn");

//Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

//When the user clicks the button, open the modal 
btn.onclick = function() {
 modal.style.display = "block";
}

//When the user clicks on <span> (x), close the modal
span.onclick = function() {
 modal.style.display = "none";
}

//When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
 if (event.target == modal) {
   modal.style.display = "none";
 }
}
  
  </script>

</div>
	
</body>
</html>