<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Error Page</title>
<style>
.oopsbg { background-color:#2ea3f2;}
.widthR {min-width:295px; max-width:970px; margin:0 auto; padding:0; background:#ffffff; border-radius:7px; padding:0px; color:#636363;}
.widthRimg { width:100%; height:auto;}
@media only screen and (max-width:480px){.widthR{ margin:0 auto; padding:0;}
.widthRimg2 { width:60%; height:auto;}
}

</style>
<script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
</script>
</head>
<body class="oopsbg">
	<br />
	<br />
    <div class="widthR">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="whiteroundtble">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center"><img src="../image/oops.jpg" class="widthRimg"></td>
		</tr>
		<tr>
			<td><img src="../image/oops2.jpg" class="widthRimg"></td>
		</tr>
		<tr>
			<td align="center" valign="middle"><img src="../image/pg_logo_big1.png" width="280" height="47" vspace="15px" class="widthRimg2"></td>
		</tr>
	</table>
    </div>
</body>
</html>