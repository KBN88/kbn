<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Invoice Details</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/custom.css" rel="stylesheet" type="text/css" />
<script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
	function copyInvoiceLink(copyLinkElement){
		document.getElementById("invoiceShortLinkBtn").disabled = !document.queryCommandSupported('copy');
		document.getElementById("invoiceLinkBtn").disabled = !document.queryCommandSupported('copy');
		var copiedLink = document.getElementById(copyLinkElement);
		copiedLink.select();
		document.execCommand('copy');
	}
</script>
<style>
.btn-custom{
    margin-top: 5px;
	height:27px;
	border: 1px solid #5e68ab;
    width: 73px;
    padding: 5px;
    background: url(../image/textF.jpg) repeat-x bottom #5e68ab;
    font: bold 12px Tahoma;
    color: #fff;
    cursor: pointer;
    border-radius: 5px;
    z-index: 999;
    position: relative;
}
</style>
</head>
<body>
<div class="scrollDR">
<div style="width:876px">
	<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
          <tr>
			<td align="center" valign="top">
	  			<table width="600" border="0" align="left" cellpadding="0" cellspacing="0" margin-top="-7px">				
					<!--<tr>
						<td colspan="5" align="center" valign="middle"  class="orgco"
							height="30">View Invoice Information</td>
					</tr>-->
					<tr>
						<td height="15" colspan="5" align="left" valign="middle">&nbsp;&nbsp;<strong>Detail Information</strong></td>
			         </tr>
					<tr>
						<td colspan="5" align="center" valign="top"><table width="97%" border="0" cellpadding="0" cellspacing="0" class="invoicetable">
						<tr>
									<th width="20%" height="25" align="left" valign="middle">Invoice no</th>
									<th width="20%" align="left" valign="middle"><strong>Name</strong></th>
									<th width="20%" align="left" valign="middle"><strong>City</strong></th>
									<th width="20%" align="left" valign="middle"><strong>Country</strong></th>
						 </tr>
						 <tr>
									<td height="25" align="left" valign="middle"><s:property value="%{invoice.invoiceNo}"  /></td>
									<td align="left" valign="middle"><s:property value="%{invoice.name}" /></td>
									<td align="left" valign="middle"><s:property value="%{invoice.city}" /></td>
									<td align="left" valign="middle"><s:property value="%{invoice.country}" /></td>
						</tr>
								<tr>
								  <th height="25" align="left" valign="middle"><strong><span>State</span></strong></th>
								  <th align="left" valign="middle"><strong>Zip</strong></th>
								  <th align="left" valign="middle"><strong><span>Phone</span></strong></th>
								  <th align="left" valign="middle"><strong><span>Email</span></strong></th>
						  </tr>
								<tr>
								  <td height="25" align="left" valign="middle"><s:property value="%{invoice.state}" /></td>
								  <td align="left" valign="middle"><s:property value="%{invoice.zip}"/></td>
								  <td align="left" valign="middle"><s:property value="%{invoice.phone}" /></td>
								  <td align="left" valign="middle"><s:property value="%{invoice.email}" /></td>
						  </tr>
								<tr>
								  <th height="25" colspan="4" align="left" valign="middle"><span><strong>Address</strong></span></th>
						  </tr>
								<tr>
								  <td height="25" colspan="4" align="left" valign="middle"><s:property value="%{invoice.address}" /></td>
						    </tr>
								</table>
			  </td></tr>					
					<tr>
						<td height="15" colspan="5" align="left" valign="middle">&nbsp;&nbsp;<strong>Product Information</strong></td>
			  </tr>
                    <tr>
						<td align="center" valign="top"><table width="97%" border="0" cellpadding="0" cellspacing="0" class="invoicetable">
  <tr>
         <th width="22%" height="25" align="left" valign="middle"> Name</th>
         <th colspan="2" align="left" valign="middle"> Description</th>
         <th width="12%" align="left" valign="middle"><span>Quantity</span></th>
         <th width="21%" align="left" valign="middle"><span>Amount</span></th>
  </tr>

  <tr>
         <td height="25" align="left" valign="middle"><div class="txtnew">
           <s:property value="%{invoice.productName}" />
         </div></td>
         <td colspan="2" align="left" valign="middle"><div class="txtnew">
           <s:property value="%{invoice.productDesc}" />
         </div></td>
         <td align="left" valign="middle"><div class="txtnew">
           <s:property value="%{invoice.quantity}" />
         </div></td>
         <td align="left" valign="middle"><div class="txtnew">
          <s:property value="%{invoice.amount}" />
        </div></td>
  </tr>
  <tr>
    <td height="25" align="left" valign="middle">&nbsp;</td>
    <td width="23%" align="left" valign="middle">&nbsp;</td>
    <td width="22%" align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle">&nbsp;</td>
    <th align="left" valign="middle">Service Charge</th>
  </tr>
  <tr>
    <td height="25" align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="right" valign="middle">&nbsp;</td>
    <td align="right" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><div class="txtnew">
          <s:property value="%{invoice.serviceCharge}" />
    </div></td>
  </tr> 
  <tr>
    <th height="25" align="left" valign="middle">All prices are in</th>
    <th align="left" valign="middle">Expire in days</th>
    <th align="right" valign="middle">&nbsp;</th>
    <th align="right" valign="middle">&nbsp;</th>
    <th align="left" valign="middle">Total Amount</th>
  </tr>
  <tr>
    <td height="25" align="left" valign="middle"><div class="txtnew">
      <s:property value="%{currencyName}" />
    </div></td>
    <td align="left" valign="middle"><div class="txtnew">
      <s:property value="%{invoice.expiresDay}" />
    </div></td>
    <td align="right" valign="middle">&nbsp;</td>
    <td align="right" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><div class="txtnew">
      <strong><s:property value="%{invoice.totalAmount}" /></strong>
    </div></td>
  </tr>
      </table></td>
					</tr>
					
					<s:if test="%{invoice.invoiceType=='PROMOTIONAL PAYMENT'}">
					<tr>
						<td height="15" colspan="5" align="left" valign="middle">&nbsp;&nbsp;<strong>Sender Information</strong></td>
			        </tr>
			         <tr>
						<td colspan="5" align="center" valign="top"><table width="97%" border="0" cellpadding="0" cellspacing="0" class="invoicetable">
					     <tr>
							<th width="20%" height="25" align="left" valign="middle"><strong>Recipient Mobile</strong></th>
							<th width="80%" align="left" valign="middle"><strong>Message Body</strong></th>
						</tr>
						<tr>
						  <td height="25" align="left" valign="middle"><s:property value="%{invoice.recipientMobile}"  /></td>
						  <td align="left" valign="middle" ><s:property value="%{invoice.messageBody}" /></td>
						</tr>
			</table>
			  </td></tr>
			  </s:if> 
                    <tr>
						<td colspan="5" align="center" valign="top">
						<table width="97%" border="0" cellpadding="0" cellspacing="0" class="invoicetable">
						  <tr>
                              <th colspan="2" width="20%" height="15" align="left" valign="middle"><strong>Payment Links</strong></th>
					      </tr>
						  <tr>
							<td  align="left" valign="middle" class="bluelinkbig" style="font-size:12px; width:19%;" >
							<input id="invoiceLink" onkeydown="document.getElementById('invoiceLinkBtn').focus();" type="text" style="display:block" class="textFL_merch" value=<s:property value="invoiceUrl"/>></input></td>
							<td align="left" valign="middle" class="bluelinkbig" style="font-size:12px;"><button id="invoiceLinkBtn" onclick="copyInvoiceLink('invoiceLink')"  href="#" class="btn-custom" value="X">Copy Link</button></td>									
						 </tr>
				   		<tr>
							<td align="left" valign="middle" class="bluelinkbig" style="font-size:12px;">
							<input id="invoiceShortLink" onkeydown="document.getElementById('invoiceLinkBtn').focus();" type="text" style="display:block" class="textFL_merch" value=<s:property value="%{invoice.shortUrl}" />></input></td>
							<td align="left" valign="middle" class="bluelinkbig" style="font-size:12px;"><button id="invoiceShortLinkBtn" onclick="copyInvoiceLink('invoiceShortLink')" href="#" class="btn-custom" value="X">Copy Link</button></td>
						</tr>
								
						
								</table>
			  </td></tr>
			  
                    <tr>
                      <td height="45" align="center" valign="top">&nbsp;</td>
                    </tr>
        </table></td>
  </tr>
	        </table>
</div>
    </div>
</body>
</html>