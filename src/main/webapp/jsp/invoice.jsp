<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Invoice Payment</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.min.js"></script>
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript">
		
	function changeCurrencyMap() {
		var token = document.getElementsByName("token")[0].value;
		var emailId = document.getElementById("merchant").value;
		$.ajax({
			url : 'setMerchantCurrency',
			type : 'post',
			data : {
				emailId : emailId,
				currency : document.getElementById("currencyCode").value,
				token : token
			},
			success : function(data) {
				var dataValue = data.currencyMap;
				var currenyMapDropDown = document.getElementById("currencyCode");
				var test = "";
				var parseResponse = '<select>';
				for (index in dataValue) {
					var key = dataValue[index];
					parseResponse += "<option value = "+index+">" + key + "</option> ";
					//console.log(parseResponse);
				}
				parseResponse += '</select>';
				test += key;
				currenyMapDropDown.innerHTML = parseResponse;
			},
			error : function(data) {
				alert("Something went wrong, so please try again.");
			}
		});
	}
	class FieldValidator{
		constructor(x){
			this.x = x;
		}
		
		static valdInvoiceNo(errMsgFlag){
			var invoiceexp = /^[0-9a-zA-Z-/\_]+$/;;
		    var invoiceElement = document.getElementById("invoiceNo");
		    var invoiceValue = invoiceElement.value;
		    if (invoiceValue.trim() != "") {
		        if (!invoiceValue.match(invoiceexp)) {
		        	FieldValidator.addFieldError("invoiceNo", "Enter valid Invoice no.", errMsgFlag);
		            return false;
		        } else {
		            FieldValidator.removeFieldError('invoiceNo');
		            return true;
		        }
		    } else {
		    	FieldValidator.addFieldError("invoiceNo", "Please enter Invoice No.", errMsgFlag);
	            return false;
		    }	
		}
		static valdPhoneNo(errMsgFlag){
			var phoneElement = document.getElementById("phone");
			var value = phoneElement.value.trim();
			if ( value.length >0) {
				var phone = phoneElement.value;
				var phoneexp =  /^[0-9]{10,15}$/;
				if (!phone.match(phoneexp)) {
				FieldValidator.addFieldError("phone", "Enter valid phone no.", errMsgFlag);
					return false;
				}else{
					FieldValidator.removeFieldError('phone');
					return true;
				}
			}else{FieldValidator.addFieldError("phone", "Enter phone no.", errMsgFlag);
				return false;
			}
		}
		
		
		
		static valdProductDesc(errMsgFlag){
			var productDescElement = document.getElementById("productDesc");
			var value = productDescElement.value.trim(); 
			if ( value.length>0) {
				var productDesc = productDescElement.value;
				var regex = /^[ A-Za-z]/;
				if (!productDesc.match(regex)) {
					FieldValidator.addFieldError("productDesc", "Enter valid product description", errMsgFlag);
					return false;
				}else{
					FieldValidator.removeFieldError('productDesc');
					return true;
				}
			}else{FieldValidator.removeFieldError('productDesc');
			return true;	
			}	
		}
		
		static valdCurrCode(errMsgFlag){
			var currencyCodeElement = document.getElementById("currencyCode");
			if (currencyCodeElement.value == "Select Currency") {
				FieldValidator.addFieldError("currencyCode", "Select Currency Type", errMsgFlag)
				return false;
			} else {
				FieldValidator.removeFieldError('currencyCode');
				return true;
			}
			
		}
		static valdRecptMobileNo(errMsgFlag){
			var recipientMobileElement = document.getElementById("recipientMobile");
			if (recipientMobileElement.value.trim().length >0) {
				var recipientMobile = recipientMobileElement.value;
				var phoneexp =  /^[0-9]{10,15}$/;
				if (!recipientMobile.match(phoneexp)) {
					FieldValidator.addFieldError("recipientMobile", "Enter valid mobile no", errMsgFlag);
					return false;
				}else{
					FieldValidator.removeFieldError('recipientMobile');
					return true;
				}
			} else {
				FieldValidator.addFieldError("recipientMobile", "Enter recipient mobile no", errMsgFlag);
				return false;
			}
		} 
		
		static valdRecptMsg(errMsgFlag){
			var messageBodyElement = document.getElementById("messageBody");
			if (messageBodyElement.value.trim().length >0) {
				if (!messageBodyElement.value.length > 2) {
					FieldValidator.addFieldError("messageBody", "Please enter valid message", errMsgFlag);
					return false;
				}else{
					FieldValidator.removeFieldError('messageBody');
					return true;
				}
			} else {
				FieldValidator.addFieldError("messageBody", "Enter message", errMsgFlag);
				return false;
			} 			
		}
		
		static valdExpDayAndHour(errMsgFlag){
			var expDayElement = document.getElementById("expiresDay");
			var expHorElement = document.getElementById("expiresHour");
			var days = expDayElement.value.trim();
			var hors = expHorElement.value.trim();
			if (days.length >0 && parseInt(days)>=0) {
				if(parseInt(days) > 31){
					FieldValidator.addFieldError("expiresDay", "Enter valid no. of days (Max:31)", errMsgFlag);
					return false;
				}
				FieldValidator.removeFieldError('expiresDay');
				if(hors.length > 0 && parseInt(hors)>=0){
					if(parseInt(hors) > 24 || parseInt(hors)<1){
						FieldValidator.addFieldError("expiresHour", "Enter valid no. of hours (Max:24)", errMsgFlag);
						return false;
					}
					FieldValidator.removeFieldError('expiresHour');
					return true;
				}else{FieldValidator.addFieldError("expiresHour", "Enter valid no. of hours", errMsgFlag);
				return false;
				}
			}else{FieldValidator.addFieldError("expiresDay", "Enter valid no. of days", errMsgFlag);
				return false;
			}
		}
		
		static valdMerchant(errMsgFlag){
			var element =document.getElementById("merchant") 
			if ((element) != null) {
				if (element.value != "Select Merchant") {
					FieldValidator.removeFieldError('merchant');
					return true;
				} else {
					FieldValidator.addFieldError("merchant", "Select Merchant", errMsgFlag)
					return false;
				}
			}else{
				return true;
			}
		}
		
		static valdSrvcChrg(errMsgFlag){
			var element = document.getElementById('serviceCharge');
			var value = parseFloat(element.value.trim());
			
			if(element.value.indexOf(".") > -1){
				var index = element.value.indexOf(".");
				if((element.value.substr(index, element.value.length)).length>3){
					FieldValidator.addFieldError("serviceCharge", "Enter valid Service Charge", errMsgFlag)
					return false;
				}
			}
			if(parseFloat(value)>= parseFloat(0)){
				FieldValidator.removeFieldError('serviceCharge');
				return true;
			}else{
				FieldValidator.addFieldError("serviceCharge", "Enter valid Service Charge", errMsgFlag)
				return false;
			}
		}
		
		//valdiating the amount of the product
		static valdAmount(errMsgFlag){
			var element = document.getElementById('amount');
			var value = parseFloat(element.value.trim());
		
				if(element.value.indexOf(".") > -1){
					var index = element.value.indexOf(".");
					if((element.value.substr(index, element.value.length)).length>3){
						FieldValidator.addFieldError("amount", "Enter valid amount", errMsgFlag)
						return false;
					}
				}
			if(!value<parseFloat(value)){ // change here for custom amount
				FieldValidator.removeFieldError('amount');
				return true;
			}else{
				FieldValidator.addFieldError("amount", "Enter amount", errMsgFlag)
				return false;
			}
		}
		
		static valdEmail(errMsgFlag){
		
			var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
			var element = document.getElementById("emailId");
			var value = element.value.trim();
			if(value.length>0){
				if(!value.match(emailRegex)){FieldValidator.addFieldError('emailId', "Enter valid email address", errMsgFlag);
				return false;
				}else{FieldValidator.removeFieldError('emailId');
				return true;
				}
			}else{FieldValidator.addFieldError('emailId', "Enter email address", errMsgFlag);
			return false;
			}
		}

		
		
		
		
		
		
		
		
		
		static valdQty(errMsgFlag){
			var qtyRgx = "^([1-9][0-9]+|[1-9])$";
			var element = document.getElementById("quantity");
			var value = element.value.trim();
			if(value==0){
				FieldValidator.addFieldError('quantity', "Enter valid quantity", errMsgFlag);
				return false;
			}
			if(value.length	>0){
				if(!(value).match(qtyRgx)){
					FieldValidator.addFieldError('quantity', "Enter valid quantity", errMsgFlag);
					return false;
				}else{FieldValidator.removeFieldError('quantity');
				return true;
				}
			}else{FieldValidator.addFieldError('quantity',"Enter valid quantity", errMsgFlag);
			return false;
			}	
		}
		
		
		static valdAllFields(){
			 var flag = FieldValidator.valdMerchant(true);
			flag = flag && FieldValidator.valdInvoiceNo(true);
			flag = flag && FieldValidator.valdPhoneNo(true);
			flag = flag && FieldValidator.valdEmail(true);
		
			flag = flag && FieldValidator.valdProductDesc(true);
			flag = flag && FieldValidator.valdExpDayAndHour(true) ;
			flag = flag && FieldValidator.valdCurrCode(true);
			flag = flag && FieldValidator.valdQty(true);
			flag = flag && FieldValidator.valdAmount(true);
			flag = flag && FieldValidator.valdSrvcChrg(true);
			flag = flag && FieldValidator.valdRecptMobileNo(true);
			flag = flag && FieldValidator.valdRecptMsg(true);
			//submitting form
			if(flag){
			    $("#footer-html").dialog({
				    modal: true,
				    draggable: false,
				    resizable: false,
				    position: ['center', 'center'],
				    show: 'blind',
				    hide: 'blind',
				    width: 328,
				    height:72,
				    buttons: [
				              {
				                  text: "Send Email",
				                  click: function() {
				                	  document.getElementById("emailCheck").value = "true";
										document.forms["frmInvoice"].submit();
										$( this ).dialog( "close" );
				                  }
				              },
				              {
				                  text: "Don't Send",
				                  click: function() {
				                	  document.getElementById("emailCheck").value = "false";
										document.forms["frmInvoice"].submit();
										$( this ).dialog( "close" );
				                  }
				              }
				          ],
				    open: function(){
				    	$("#footer-html").css("overflow","hidden");
				    },
				    dialogClass: 'ui-dialog-osx',
				});
			}
		}
		
		//to show error in the fields
		static addFieldError(fieldId, errMsg, errMsgFlag){
			var errSpanId = fieldId+"Err";
			var elmnt = document.getElementById(fieldId);
			elmnt.className = "textFL_merch_invalid";
			elmnt.focus();
			if(errMsgFlag){
				document.getElementById(errSpanId).innerHTML = errMsg;	
			}
		}
		
		// to remove the error 
		static removeFieldError(fieldId){
			var errSpanId = fieldId+"Err";
			document.getElementById(errSpanId).innerHTML = "";
			document.getElementById(fieldId).className = "textFL_merch";
		}
	}

	function sum() {
	    var txtFirstNumberValue = document.getElementById('amount').value;
	    if(txtFirstNumberValue == "") {
	    	txtFirstNumberValue = "0.00";
	    }
	    var txtSecondNumberValue = document.getElementById('serviceCharge').value;
	    if(txtSecondNumberValue == "" || txtSecondNumberValue == ".") {
	    	txtSecondNumberValue = "0.00";
	    }
	    var txtQuantity = document.getElementById('quantity').value;
	    var result =  parseInt(txtQuantity)* parseFloat(txtFirstNumberValue);
	    if (!isNaN(result)) {
	       document.getElementById('totalAmount').value =(result + parseFloat(txtSecondNumberValue)).toFixed(2);
	    }
	}

	$(document).ready(function() {
		var windowsPath = window.location.pathname;
		var isSaleInvoice = windowsPath.includes("saveInvoice");
		if(isSaleInvoice == true){
			document.getElementById('invoiceLink').style="display:block";
			document.getElementById('copyBtn').style="display:block";
			document.getElementById('btnSave').style="display:none";
		}
		copyBtn.disabled = !document.queryCommandSupported('copy');
		document.getElementById("copyBtn").addEventListener("click", function(event){
			var copiedLink = document.getElementById('invoiceLink');
			copiedLink.select();
			document.execCommand('copy');
		});

		
		$('#btnSave').click(function() {
			event.preventDefault();
			
			FieldValidator.valdAllFields();
		});

		$('#merchant').change(function() {
			changeCurrencyMap();
			$('#spanMerchant').hide();
			$('#currencyCodeloc').hide();
		});

		$('#serviceCharge').on('keyup', function() {
			if (this.value[0] === '.') {
				this.value = '0' + this.value;
			}
		});
	});
</script>
<style>	
	.textFL_merch {
    border: 1px solid #c0c0c0;
    background: #fff;
    padding: 8px;
    width: 100%;
    color: #000;
    border-radius: 3px;
}

.textFL_merch:hover {
    border: 1px solid #d5d0a3;
    background: #fffce4;
    padding: 8px;
    width: 100%;
    color: #ff0000;
    border-radius: 3px;
}
	.textFL_merch_invalid {
    border: 1px solid #c0c0c0;
    background: #fff;
    padding: 8px;
    width: 100%;
    border-color: #a94442;
    border-radius: 1px;
}
.ui-dialog-osx {
    -moz-border-radius: 0 0 8px 8px;
    -webkit-border-radius: 0 0 8px 8px;
    border-radius: 0 0 8px 8px; border-width: 3px 3px 3px 3px;
}
.ui-widget-header {
    background: #2b6dd1;
    border: 0;
    color: #fff;
    font-weight: normal;
}
.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset {
    text-align: center;
    float: none !important;
} 
.ui-dialog-content {
display:none;
}
.btn-custom{
width:95px;
background-color: #2b6dd1;
margin-left:20px;
margin-right:20px;
border-color:#5d96ec;
color:#fff;
}
.btn-custom:hover{
background-color: #2b6dd1;
border-color:#5d96ec;
color:#fff;
}
.ui-state-default {
    border: 1px solid #5d96ec;
    background-color: #2b6dd1;
    font-weight: bold;
    color:#fff;
    font-size: 11px;
    height: auto;
}
</style>
</head>
<body>
<div id="footer-html" title="Send invoice link to customer via Email">
</div>
	<s:form name="f1" action="saveInvoice" id='frmInvoice'
		autocomplete="off">
		<input type="hidden" id="emailCheck" name="emailCheck" value="false"></input>
		<table width="100%" border="0" align="center" cellpadding="0"
			cellspacing="0" class="txnf">
			<tr>
				<td align="left">
					<div style="display: none" id="response"></div>
					<table width="100%" border="0">
						<tr>
							<td width="82%" align="left"><h2>create Payment Link</h2>
							
							</td>
							<td width="25%" align="left">
								<div class="txtnew">

									<s:if
										test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER.UserType.name()=='RESELLER' || #session.USER_TYPE.name()=='SUPERADMIN'}">
										<s:select name="merchant" theme="simple" class="form-control"
											id="merchant" headerKey="Select Merchant" onchange="FieldValidator.valdMerchant()"
											headerValue="Select Merchant" list="merchantList"
											listKey="emailId" listValue="businessName" autocomplete="off" />
									</s:if>
								</div> <span id="merchantErr" class="invocspan"></span>
							</td>
							<td width="2%" align="center">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td height="30" colspan="5" align="left" valign="middle"><h3>
						Detail Information</h3></td>
			</tr>
			<tr>
				<td colspan="5" align="left" valign="top"><div class="MerchBx"
						style="background: #f2f2f2; border-radius: 5px; padding: 10px;float: left; width: calc(100% - 30px); margin: 0px 15px;">
							<div class="invo6">
								Invoice no* <br />
								<div class="txtnew">
									<s:textfield type="text" class="textFL_merch" id="invoiceNo"
										name="invoiceNo" autocomplete="off"
										onkeyup="FieldValidator.valdInvoiceNo(false)" />
									<span id="invoiceNoErr" class="invocspan"></span>
								</div>
							</div>
					
							<div class="invo6">
								Phone*<br />
								<div class="txtnew">
									<s:textfield type="text" id="phone" name="phone" maxlength="15"
										class="textFL_merch" autocomplete="off" onkeyup="FieldValidator.valdPhoneNo(false)"/>
								</div>
								<span id="phoneErr" class="invocspan"></span>
							</div>
							<div class="invo6">
								Email*<br />
								<div class="txtnew">
									<s:textfield type="text" id="emailId" name="email"
										value="%{invoice.email}" class="textFL_merch" onkeyup="FieldValidator.valdEmail(false)"
										autocomplete="off" />
								</div>
								<span id="emailIdErr" class="invocspan"></span>
							</div>
					
						<div class="clear"></div>
					</div></td>
			</tr>
			<tr>
				<td colspan="5" align="left" valign="top">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="5" align="left" valign="top"><h3>Product Information</h3></td>
			</tr>
			<tr>
				<td colspan="5" align="left" valign="top">
				<div class="MerchBx"
						style="background: #f2f2f2; border-radius: 5px; padding: 10px">
						<div class="invoCont">
					
							<div class="productIn">
								Description<br />
								<div class="txtnew">
									<s:textfield type="text" class="textFL_merch" id="productDesc" onkeyup="FieldValidator.valdProductDesc(false)"
										name="productDesc" autocomplete="off" />
									<span id="productDescErr" class="invocspan"></span>
								</div>
							</div>
							<div class="productIn">
								Expire in days*<br />
								<div class="txtnew">
									<s:textfield type="number" class="textFL_merch" id="expiresDay"
										name="expiresDay" autocomplete="off" value="0" maxlenth="2" onkeyup="FieldValidator.valdExpDayAndHour(false)"/>
								</div>
								<span id="expiresDayErr" class="invocspan"></span>
							</div>
							<div class="productIn">
								Expire in hours* <br />
								<div class="txtnew">
									<s:textfield type="text" class="textFL_merch" id="expiresHour"
										name="expiresHour" autocomplete="off" value="1" maxlength="2" onkeyup="FieldValidator.valdExpDayAndHour(false)" />
								</div>
								<span id="expiresHourErr" class="invocspan"></span>
							</div>
							<div class="productIn">
								All prices are in*<br />
								<div class="txtnew">
									<s:select name="currencyCode" id="currencyCode"
										headerValue="Select Currency" headerKey="Select Currency"
										list="currencyMap" listKey="key" listValue="value"
										class="form-control" onchange="FieldValidator.valdCurrCode(false)"
										autocomplete="off" />
								</div>
								<span id="currencyCodeErr" class="invocspan"></span>
							</div>
							<div class="invoC1">
								<table width="97%" border="0" cellpadding="0" cellspacing="0"
									class="greyroundtble">
									<tr>
										<td width="6%" align="left" valign="middle"><h6>Quantity</h6></td>
										<td width="2%" align="center" valign="middle">&nbsp;</td>
										<td width="15%" align="left" valign="top"><h6>
												Amount*</h6></td>
										<td width="2%" align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="left" valign="top"><div class="txtnew">
												<s:textfield type="number" class="textFL_merch" value="1"
													id="quantity" name="quantity" onkeyup="sum();FieldValidator.valdQty(false);"
													autocomplete="off" />
											</div>
												<span id="quantityErr" class="invocspan"></span></td>
										<td align="center" valign="middle"><strong>x</strong></td>
										<td height="37" align="left" valign="top"><div
												class="txtnew">
												<s:textfield type="number" class="textFL_merch"
													onkeyup="sum();FieldValidator.valdAmount(false);" id="amount" name="amount"
													value="%{invoice.amount}"
													autocomplete="off" />
											</div> <span id="amountErr" class="invocspan"></span></td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="left" valign="top">&nbsp;</td>
										<td align="left" valign="middle">&nbsp;</td>
										<td align="left" valign="top" class="labelfont">Service
											Charge</td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td height="37" align="left" valign="top"><div
												class="txtnew">
												<s:textfield type="text" class="textFL_merch" value="0.00"
													id="serviceCharge" name="serviceCharge"
													onkeyup="sum();FieldValidator.valdSrvcChrg(false);"
													autocomplete="off" />
											</div>
											<span id="serviceChargeErr" class="invocspan"></span></td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="left" valign="top" class="labelfont">Total
											Amount</td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
									<tr>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="right" valign="middle" class="labelfont">&nbsp;</td>
										<td align="left" valign="middle"><div class="txtnew">
												<s:textfield type="text" class="textFL_merch"
													readonly="true" placeholder="0.00" id="totalAmount"
													name="totalAmount" autocomplete="off" />
											</div></td>
										<td align="left" valign="middle">&nbsp;</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="clear"></div>
					</div></td>
			</tr>
			<tr>
				<td><h3>SMS Information</h3></td>
			</tr>
			<tr>
				<td><div class="MerchBx"
						style="background: #f2f2f2; border-radius: 5px; padding: 10px">

						<div class="invoC">
							Recipient Mobile*<br />
							<s:textfield type="text" class="textFL_merch2"
								id="recipientMobile" name="recipientMobile" onkeyup="FieldValidator.valdRecptMobileNo(false)" autocomplete="off" />
							<span id="recipientMobileErr" class="invocspan"></span>
						</div>
					
						<div class="invo8">
							Message Body* <br />
							<s:textarea type="text" class="textFL_merch2" id="messageBody"
								name="messageBody" onkeyup="FieldValidator.valdRecptMsg(false)" autocomplete="off"
								placeholder="maximum 160 character" maxlength="160" />
						</div>
						<span id="messageBodyErr" class="invocspan"></span>
						<div class="clear"></div>
					</div></td>
			</tr>
			<tr>
				<td align="center" valign="middle"><table width="97%"
						border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" valign="middle" height="40" class="bluelinkbig">
							<input id="invoiceLink" onkeydown="document.getElementById('copyBtn').focus();" type="text" style="display:none" class="textFL_merch" value= <s:property value="url" />>
							</td>
							
						</tr>
						<tr>
						<td align="center" valign="middle" height="40">
						<input type="button" style="margin-top:5px;display:none;" id="copyBtn" class="btn btn-success btn-medium" value="Copy Payment Link "/>
						</td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td align="center" valign="top"><table width="100%" border="0"
						cellpadding="0" cellspacing="0">
						<tr>
							<td width="15%" align="left" valign="middle"></td>
							<td width="5%" align="right" valign="middle"><s:submit
									id="btnSave" name="btnSave"
									value="create Payment Link" style="background: linear-gradient(60deg, #425185, #4a9b9b);color: #fff;font-size: 14px;width: 233px;height: 40px;">
								</s:submit></td>
							<td width="3%" align="left" valign="middle"></td>
							<td width="15%" align="left" valign="middle"></td>
						</tr>
					</table></td>
			</tr>
		</table>
		<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
	</s:form>
</div>
</body>
</html>