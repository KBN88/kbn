<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Ticket Details</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Sign Up</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.minshowpop.js"></script>
<script src="../js/jquery.formshowpop.js"></script>
<script src="../js/commanValidate.js"></script>
<script src="../js/captcha.js"></script>
<link href="http://fonts.googleapis.com/css?family=Crafty+Girls" rel="stylesheet" type="text/css" />
<script>
if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
</script>
</head>

<body>

<script>
function saveTicket() {
	var subj = document.getElementById("subject").value;
	var email = document.getElementById("email").value;	
	var mobile = document.getElementById("mobileNo").value;
	var ttype= document.getElementById("tickettype").value;
	var cemail = document.getElementById("cemail").value;
	var ticketbody = document.getElementById("messageBody").value;
 if(Validate()){ 
	 document.getElementById("formid").submit();
		alert("save form");
	 } 
}


 function validation() {
	var email= document.getElementById("email").value;
	var no = document.getElementById("mobileNo").value;
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var noReg = /^(\+\d{1,3}[- ]?)?\d{10}$/;
	if(email== '' || no ==''){
		alert("Please fill all fields...!");
		return false;
	} 
 	else if (!((email).match(emailReg) || (no).match(noReg))) {
		alert("enter valid values...");
		return false;
	}
	return true;
}
	
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
     <s:form action ="helpTicket" id="formid" method ="post">
     <tr>
 			<td align="center" valign="top">
  				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="txnf">
  					<tr>
 						 <td align="left" colspan="4"><h2 style="margin-bottom:0px;margin-left:52%">Create A New Ticket </h2></td>
  					</tr>
  					<tr>
						<td style="width:20%" colspan="2"> <div class="adduTvi">Ticket Type</div> </td>
						
						<td colspan="3">
						<!--<s:textfield type="text" name="ticketType" value="" id="tickettype"  cssClass="form-controlv"  autocomplete="off" />-->
					    <select name="payId" id="payId" class="form-control" autocomplete="off" style="width:35%;">
   							 <option value="ALL MERCHANTS">Select Ticket Type</option>
    					</select>
						</td>
					</tr>
				
					
  					<tr>
						<td colspan="2">  <div class="adduTvi">Subject</div></td>
						<td colspan="3"><s:textfield type="text" name="subject" value="" id="subject"  cssClass="form-controlv" placeholder="Subject" style="width:65%;"  ></s:textfield></td>
					</tr>
					<tr>
						<td colspan="2"><div class="adduTvi">Ticket Body</div></td>
						<td colspan="3">
						<!--<s:textfield type="text" name="messageBody" value="" id="messageBody"  cssClass="form-controlv"  autocomplete="off" placeholder=" Message" />-->
						<s:textarea type="text" class="textFL_merch2" id="messageBody"
								name="messageBody" onkeyup="FieldValidator.valdRecptMsg(false)" autocomplete="off"
								placeholder="maximum 160 character" maxlength="160" style="MARGIN-LEFT: 2%;
    MARGIN-TOP: 1%; width:65%!important;height:100px!important;" />
						</td>
					</tr>
					<tr>
						<td colspan="2">  <div class="adduTvi"> Concerned User </div> </td>
						<!--<s:textfield type="text" name="createdForEmail" value="" id="cemail"  cssClass="form-controlv"  autocomplete="off" placeholder="Email id" />-->
						<td colspan="3">
						<select name="payId" id="payId" class="form-control" autocomplete="off" style="width:38%;MARGIN-TOP: 1%;">
   							 <option value="ALL MERCHANTS">Select User</option>
    					</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">  <div class="adduTvi">Contact Email Id </div> </td>
						<td><s:textfield type="text" name="contentEmailId" value="" id="email"  cssClass="form-controlv"  autocomplete="off" placeholder="Email Id" style=" margin-left: 5%;
    margin-top: 2%;" /></td>
						<td>  <div class="adduTvi">Contact Mobile No </div> </td>
						<td><s:textfield type="text" name="contentMobileNo" value="" id="mobileNo"  cssClass="form-controlv"  autocomplete="off" placeholder="Mobile No" /></td>
					</tr>
					<tr>
						<td colspan="4">
						<div class="adduTR" style="text-align:left; padding:14px 0 0 0;margin-left:-8%">
						<s:submit  id="submit" name="submit" value="Submit" class="btn btn-success btn-md" style="width:32%;height:30px;"/>
						</div>
						</td>
					</tr>
  					
			</table>
		</td>
	</tr>
</s:form>

</table>
</body>
</html>