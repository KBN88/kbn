<%@page import="com.kbn.ticketing.core.HelpTicket"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="" rel="stylesheet" type="text/css" />
<script src="../js/continents.js" type="text/javascript"></script>
<script src="../js/jquery.js"></script>
<script src="../js/follw.js"></script>
<script src="../js/commanValidate.js"></script>
<script src="../js/main.js"></script>
<link href="../css/bootstrap.minr.css" rel="stylesheet">
<link href="../fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="../css/default.css" rel="stylesheet">
<link href="../css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="../css/navigation.css">
<link href="../css/welcomePage.css" rel="stylesheet">
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.dimensions.js"></script>
<script type="text/javascript" src="../js/jquery.accordion.js"></script>
<link rel="stylesheet" href="../css/loader.css">

<script type="text/javascript">
	/* function sendAssignedAgent(){
	 var agentId = document.getElementById("agents").value;
	 var token = document.getElementsByName("token")[0].value;
	 var assignedToDropDown = document.getElementById("agents").options;
	 var selectedIndex = document.getElementById("agents").options.selectedIndex;
	 var ticketId = document.getElementById("ticketId").value;
	
	 $.ajax({
	 url  :"agentsList",
	 type :"POST",
	 data : {
	 ticketId : ticketId,
	 allAgents : agentId ,
	 token:token,
	 "struts.token.name": "token",
	 },
	 success:function(data){		       	    		
	 var responseDiv = document.getElementById("response");
	 responseDiv.innerHTML = data.response;
	 responseDiv.style.display = "block";
	 var responseData = data.response;
	 if(responseData == null){
	 responseDiv.innerHTML = "Operation not successfull, please try again later!!"
	 responseDiv.style.display = "block";
	 responseDiv.className = "error error-new-text";
	 event.preventDefault();
	 }
	 var DropDown = document
	 .getElementById("agents");
	 responseDiv.className = "success success-text";
	 },
	 error:function(data){	
	 var responseDiv = document.getElementById("response");
	 responseDiv.innerHTML = "Error updating assigned to dropdown please try again later!!"
	 responseDiv.style.display = "block";
	 responseDiv.className = "error error-new-text";
	 }
	
	 });
	 } */
	function saveAction() {
		document.editTicketUpdater.submit();

	}
</script>
<style>
.errorMessage {
	color: #ff0000;
	text-align: right;
}
</style>

</head>
<body>
  


	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	   <s:form action="editTicketUpdater" id="formid" method="post">
	     <s:hidden name="token" value="%{#session.customToken}"></s:hidden>
	      <tr>
	      <td>
	          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="txnf">
	            <tr>
 				  <td align="center" colspan="4"><h2 style="margin-bottom:0px;margin-left:-8%">Edit Ticket</h2></td>
  			   </tr>
	           <tr>
	             <td><div class="adduTvi">Ticket Id</div></td>
	             <td><s:textfield type="text" name="ticketId" value="%{HelpTicket.ticketId}" id="ticketId" cssClass="form-controlv"></s:textfield></td>
	             <td><div class="adduTvi">Merchant PayID</div></td>
	             <td><s:textfield type="text" name="merchantPayId" value="%{HelpTicket.merchantPayId}" id="merchantPayId" cssClass="form-controlv"></s:textfield></td>
	           </tr>
	             <tr>
	               <td><div class="adduTvi">Contact Email Id</div></td>
	               <td><s:textfield type="text" name="contentEmailId" value="%{HelpTicket.contentEmailId}" id="email" cssClass="form-controlv" autocomplete="off" /></td>
	               <td><div class="adduTvi">Contact Mobile No. </div></td>
	         	   <td><s:textfield type="text" name="contentMobileNo" value="%{HelpTicket.contentMobileNo}" id="mobileNo"  cssClass="form-controlv" autocomplete="off" /></td>
	            </tr>
	            
	            <tr>
	              <td><div class="adduTvi">Ticket type</div></td>
	              <td colspan="3">
	             		 <select name="payId" id="payId" class="form-controlv" autocomplete="off" style="margin-left:1%;width:98%!important">
   							 <option value="ALL MERCHANTS">Select Ticket Type</option>
    					</select>
    			  </td>
	            <tr>
	            <tr>
	               <td><div class="adduTvi">Subject</div></td>
	               <td colspan="3">
	                 <s:textfield type="text" name="Subject" value="%{HelpTicket.Subject}" id="cemail" cssClass="form-controlv" autocomplete="off" style="margin-left:1%;width:98%!important"/>
	               </td>
	            </tr>
	            <tr>
	              <td><div class="adduTvi">Ticket Status</div></td>
	              <td colspan="3">
	             		 <select name="payId" id="payId" class="form-controlv" autocomplete="off" style="margin-left:1%;width:98%!important">
   							 <option value="ALL MERCHANTS">Select Ticket Status</option>
    					</select>
    			  </td>
	            <tr>
	             <tr>
	              <td><div class="adduTvi">Assigned To</div></td>
	              <td colspan="3">
	             		 <select name="payId" id="payId" class="form-controlv" autocomplete="off" style="margin-left:1%;width:98%!important">
   							 <option value="ALL MERCHANTS">Select Assigned To</option>
    					</select>
    			  </td>
	            <tr>
	            <tr>
						<td colspan="4">
						<div class="adduTR" style="text-align:left; padding:14px 0 0 0;margin-left:-38%">
						<s:submit id="btnSave" name="btnSave" class="btn btn-success btn-md" value="Save" onclick="saveAction();" style="width:20%;height:30px;"></s:submit>
						</div>
						</td>
			 </tr>
	            
	          </table>
	         
	     
	    </s:form>
	</table>
</body>
</html>