
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Weekly Transactions</title>
<script src="../js/jquery.min.js"></script>
<link href="../css/custom.css" rel="stylesheet">
<link href="../css/welcomePage.css" rel="stylesheet">
<script src="../js/highcharts.js"></script>
<script src="../js/highchart.exporting.js"></script>
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>

<link rel="stylesheet" type="text/css" media="all"
	href="../css/daterangepicker-bs3.css" />
<script>
	$(document).ready(function() {
		var today = new Date();
		$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));

		$(function() {
			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});
		dateArray();

		$(function() {
			var today = new Date();
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			dateArray();
			drawBarChart();
		});

	});
	function handleChange() {
		dateArray();
		drawBarChart();
	}
</script>
<script type="text/javascript">
	var dateTo, dateFrom;
	var array = [];
	var currentDate;

	function dateArray() {
		var calDate = document.getElementById("dateFrom").value;
		var res = calDate.split("-");
		var calDay = res[0];
		var calMonth = res[1];
		var calYear = res[2];

		currentDate = new Date(calMonth + "/" + calDay + "/" + calYear)

		var days = 6; // Days you want to subtract
		var date = currentDate;
		var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
		var day = last.getDate();
		var month = last.getMonth() + 1;
		var year = last.getFullYear();

		dateTo = currentDate;
		dateFrom = new Date(month + "/" + day + "/" + year)
		dateForArray = currentDate;

		var i = 0;
		array = [];
		var selectedDate = dateForArray;
		for (i = 0; i <= 6; i++) {

			var last1 = new Date(selectedDate.getTime()
					- (i * 24 * 60 * 60 * 1000));
			var newdate = last1.toDateString();
			array.push(newdate);
		}
		array.reverse();
	}

	function drawBarChart() {
		var token = document.getElementsByName("token")[0].value;
		$
				.ajax({
					url : "weekDay",
					type : "POST",
					data : {
						dateFrom : dateFrom,
						dateTo : dateTo,
						emailId : document.getElementById("merchant").value,
						currency : document.getElementById("currency").value,
						token : token,
						"struts.token.name" : "token",
					},
					success : function(data) {
						var creditarray = [];
						var debitarray = [];
						var nbarray = [];
						var wlarray = [];

						var pieChartList = data.pieChart;
						for (var i = 0; i < pieChartList.length; i++) {
							var piechart = pieChartList[i];
							var credit = parseInt(piechart.cc);
							var debit = parseInt(piechart.dc);
							var netbanking = parseInt(piechart.nb);
							var wallet = parseInt(piechart.wl);

							creditarray.push(credit);
							debitarray.push(debit);
							nbarray.push(netbanking);
							wlarray.push(wallet);

						}
						$(function() {
							$('#container_bar')
									.highcharts(
											{
												chart : {
													type : 'column'
												},
												title : {
													text : ''
												},
												subtitle : {
													text : ''
												},
												xAxis : {
													categories : array,
													title : {
														text : null
													}
												},
												yAxis : {
													min : 0,
													title : {
														text : 'Number Of Transaction',
														align : 'high'
													},
													labels : {
														overflow : 'justify'
													}
												},

												legend : {
													align : 'right',
													x : -30,
													verticalAlign : 'top',
													y : 25,
													floating : true,
													backgroundColor : (Highcharts.theme && Highcharts.theme.background2)
															|| 'white',
													borderColor : '#CCC',
													borderWidth : 1,
													shadow : false
												},
												tooltip : {
													headerFormat : '<b>{point.x}</b><br/>',
													pointFormat : '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
												},
												plotOptions : {
													column : {
														stacking : 'normal',
														dataLabels : {
															enabled : true,
															color : (Highcharts.theme && Highcharts.theme.dataLabelsColor)
																	|| 'white',
															style : {
																textShadow : '0 0 3px black'
															}
														}
													}
												},

												series : [ {
													name : 'Credit Card',
													data : creditarray
												}, {
													name : 'Debit Card',
													data : debitarray
												}, {
													name : 'Net Banking',
													data : nbarray
												}, {
													name : 'Wallet',
													data : wlarray
												} ]
											});
						});
					}

				});
	}
</script>

</head>
<body onload="handleChange();drawBarChart();"
	style="margin: 0px; padding: 0px;">
	<div id="page-inner">

		<div class="container">
			<div class="form-group col-md-3 col-xs-6 col-sm-4 txtnew">
				<label for="merchants">Merchants</label><br />
				<s:select name="merchants" class="form-control" id="merchant"
					headerKey="ALL MERCHANTS" headerValue="ALL MERCHANTS"
					listKey="emailId" listValue="businessName" list="merchantList"
					autocomplete="off" onchange="handleChange();" />
			</div>
			<div class="form-group col-md-1 col-xs-6 col-sm-4 txtnew"></div>
			<div class="form-group  col-md-3 col-sm-6 txtnew col-xs-4">
				<label for="currency">Currency</label> <br />
				<s:select name="currency" id="currency" headerValue="ALL"
					headerKey="ALL" list="currencyMap" class="form-control"
					autocomplete="off" onchange="handleChange();" />
			</div>
			<div class="form-group col-md-1 col-xs-6 col-sm-4 txtnew"></div>
			<div class="form-group col-md-3 col-xs-6 col-sm-4 txtnew">
				<label for="dateFrom">Last 7 Days</label> <br />
				<s:textfield type="text" readonly="true" id="dateFrom"
					name="dateFrom" class="form-control" autocomplete="off"
					onchange="handleChange();" onsubmit="validate()" />
			</div>
		</div>

		<!-- /. ROW  -->
		<div class="paddrr">
			<div class="row">

				<div class="panel panel-default">
					<div class="borderbottom">Weekly Transactions</div>
					<div class="panel-body scrollD">
						<div id="container_bar"
							style="min-width: 800px; max-width: 80000px; height: 480px; margin: 0 auto"></div>
					</div>
				</div>

				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>

</body>
</html>