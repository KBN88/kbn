<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html dir="ltr" lang="en-US">
<head>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>

<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<link href="../css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
	<table width="80%" align="left" cellpadding="0" cellspacing="0"
		class="formbox">
		<tr>
			<td align="left" style="padding: 10px;"><br /> <br />
				<div class="scrollD">
					<table id="datatable" class="display" cellspacing="0" width="100%">
						<thead>
							<tr class="boxheadingsmall">
								<th>Ticket Id</th>
								<th>Session User PayId</th>
								<th>Ticket Type</th>
								<th>Contact Email Id</th>
								<th>Contact Mobile No</th>
								<th>Concerned User</th>
								<th>Subject</th>
								<th>Ticket Status</th>
								<th>Message Body</th>
								<th>Assigned To</th>
								<th>Assigned Agent PayID</th>
								<th>Edit</th>
								<th>View Comments</th>
							</tr>
						</thead>
					</table>
				</div></td>
		</tr>
	</table>
	<s:form name="ticketDetail" action="ticketDetail">

		<s:hidden name="ticketId" id="ticketId" value="">
		</s:hidden>
		<s:hidden name="subject" id="subject" value="">
		</s:hidden>
		<s:hidden name="ticketType" id="ticketType" value="">
		</s:hidden>
		<s:hidden name="messageBody" id="messageBody" value="">
		</s:hidden>
		<s:hidden name="ticketStatus" id="ticketStatus" value="">
		</s:hidden>
		<s:hidden name="assignedAgent" id="assignedAgent" value="">
		</s:hidden>
		<s:hidden name="assignedAgentPayId" id="assignedAgentPayId" value="">
		</s:hidden>
		<s:hidden name="merchantPayId" id="mPayId" value="" />
		<s:hidden name="token" value="%{#session.customToken}" />
	</s:form>




	<script type="text/javascript">

$(document).ready(function() {
			
		
			var token  = document.getElementsByName("token")[0].value;
			$('#datatable').DataTable({
			dom : 'lfrtip',
				"processing": true,
				 "ajax" : {
					"url" : "allTickets",
					"type" : "POST",		
					"data" : {
						 token:token,
					    "struts.token.name": "token",
					}
				},
	
				
			"columns" : [ 	{
							
					"data" : "ticketId",
					"width" : '1%',
				}, {
					"data" : "sessionUserPayId",
					"width" : '12%',
				}, {
					"data": "ticketType",
					"width" : '9%',
					
				}, {
					"data": "contactEmailId",
					"width" : '12%',
				},{
					"data" : "contactMobileNo",
					"width" : '13%',

				},{
					"data" : "concernedUserEmailId",
					"width" : '13%',
				},{
					"data" : "subject",
					"width" : '9%',
				},{
					"data" : "ticketStatus",
					"width" : '11%',
				} ,{
					"data" : "messageBody",
					"width" : '11%',

				},{
					"data" : "agentEmailId",
					"width" : '11%',

				},{
					"data" : "assignedTo",
					"width" : '11%',

				},{
					"data" : null,
					"class" : "center",
					"width" : '4%',
					"orderable" : false,
					"mRender" : function() {
						return '<button class="btn btn-info btn-xs" /* onclick="ajaxindicatorstart1()" */>Edit</button>';
					
					}
				},{
					"data" : null,
					"visible" : false,
					"className" : "displayNone",
					"mRender" : function(row) {
			              return "\u0027" + row.merchantPayId;
			       }
				}]
			});
		});
		
			
		$(document).ready(function() {
			var table = $('#datatable').DataTable();
				$('#datatable tbody').on('click','td',function(){
					var rows = table.rows();
					var columnVisible = table.cell(this).index().columnVisible;
					var rowIndex = table.cell(this).index().row;
					if (columnVisible == 11) {
						var ticketId = table.cell(rowIndex, 0).data();
						var merchantPayId = table.cell(rowIndex, 1).data();
						var subject = table.cell (rowIndex,6).data();
		    			var ticketType = table.cell(rowIndex,2).data();
		    			var ticketStatus = table.cell(rowIndex,7).data();
		    			var messageBody = table.cell(rowIndex,8).data();
		    			var assignedAgent = table.cell(rowIndex,9).data();
		    			var assignedAgentPayId = table.cell(rowIndex,10).data();
						if(merchantPayId.length>20 ){
							return false;
						}
						for(var i =0;i<merchantPayId.length;i++){
							var code  = merchantPayId.charCodeAt(i);
							if (code < 48 || code > 57){
								return false;
							}
					    }
						
						 document.getElementById('mPayId').value = merchantPayId;
						 document.getElementById('ticketId').value = ticketId;
		    			 document.getElementById('subject').value = subject;
		    			 document.getElementById('ticketType').value = ticketType;
		    			 document.getElementById('ticketStatus').value = ticketStatus;
		    			 document.getElementById('messageBody').value = messageBody;
		    			 document.getElementById('assignedAgent').value = assignedAgent;
		    			 document.getElementById('assignedAgentPayId').value = assignedAgentPayId;
				    	 document.ticketDetail.submit();
				    	
				    	
					}
			});
		});

	</script>
</body>
</html>