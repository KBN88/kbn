<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Pay Invoice</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
</script>
<script>
	function formLoad() {
		var enablePayNow= '<s:property value="%{enablePay}" />';
		if(enablePayNow== "TRUE") {
			document.getElementById('btnPay').disabled = false;
			document.getElementById('lblMsg').style.display= "none";
		}	
		else {
			document.getElementById('btnPay').disabled = true;
			document.getElementById('lblMsg').style.display= "block";			
		}
	};
</script>
</head>
<body onload="formLoad();">

<s:form  action="paymentrequest" method="post">
	<br /><table width="70%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
					  <td align="center" valign="middle" height="40"><img src="../image/logo-signup.png" width="220" /></td>
  </tr>
  <tr>
			<td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="formbox">				
					<tr>
						<td colspan="5" align="left" valign="middle" class="boxheadingsmall" style="color:#fff"
							height="30">Review Information and Pay</td>
					</tr>
					<tr>
						<td height="30" colspan="5" align="left" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Detail Information</strong></td>
			  </tr>
					<tr>
						<td colspan="5" align="center" valign="top"><table width="97%" border="0" cellpadding="0" cellspacing="0" class="invoicetable">
							   
<tr>
									<th width="20%" height="25" align="left" valign="middle"><strong>Invoice no</strong></th>
									<th width="20%" align="left" valign="middle"><strong>Name</strong></th>
									<th width="20%" align="left" valign="middle"><strong>City</strong></th>
									<th width="20%" align="left" valign="middle"><strong>Country</strong></th>
						  </tr>
								<tr>
									<td height="25" align="left" valign="middle"><s:property value="%{invoice.invoiceNo}"  /></td>
									<td align="left" valign="middle"><s:property value="%{invoice.name}" /></td>
									<td align="left" valign="middle"><s:property value="%{invoice.city}" /></td>
									<td align="left" valign="middle"><s:property value="%{invoice.country}" /></td>
								</tr>
								<tr>
								  <th height="25" align="left" valign="middle"><strong><span>State</span></strong></th>
								  <th align="left" valign="middle"><strong>Zip</strong></th>
								  <th align="left" valign="middle"><strong><span>Phone</span></strong></th>
								  <th align="left" valign="middle"><strong><span>Email</span></strong></th>
						  </tr>
								<tr>
								  <td height="25" align="left" valign="middle"><s:property value="%{invoice.state}" /></td>
								  <td align="left" valign="middle"><s:property value="%{invoice.zip}"/></td>
								  <td align="left" valign="middle"><s:property value="%{invoice.phone}" /></td>
								  <td align="left" valign="middle"><s:property value="%{invoice.email}" /></td>
						  </tr>
								<tr>
								  <th height="25" colspan="4" align="left" valign="middle"><span><strong>Address</strong></span></th>
						  </tr>
								<tr>
								  <td height="25" colspan="4" align="left" valign="middle"><s:property value="%{invoice.address}" /></td>
						    </tr>
								</table>
			  </td></tr>
					<tr>
					  <td height="10" colspan="5" align="left" valign="middle"></td>
			  </tr>
					<tr>
						<td height="30" colspan="5" align="left" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp; <strong>Product Information</strong></td>
			  </tr>
                    <tr>
						<td align="center" valign="top"><table width="97%" border="0" cellpadding="0" cellspacing="0" class="invoicetable">
  <tr>
         <th width="22%" height="25" align="left" valign="middle"> Name</th>
         <th colspan="2" align="left" valign="middle"> Description</th>
         <th width="12%" align="left" valign="middle"><span>Quantity</span></th>
         <th width="16%" align="left" valign="middle"><span>Amount</span></th>
  </tr>

  <tr>
         <td height="25" align="left" valign="middle"><div class="txtnew">
           <s:property value="%{invoice.productName}" />
         </div></td>
         <td colspan="2" align="left" valign="middle"><div class="txtnew">
           <s:property value="%{invoice.productDesc}" />
         </div></td>
         <td align="left" valign="middle"><div class="txtnew">
           <s:property value="%{invoice.quantity}" />
         </div></td>
         <td align="left" valign="middle"><div class="txtnew">
          <s:property value="%{invoice.amount}" />
        </div></td>
  </tr>
  <tr>
    <td height="25" align="left" valign="middle">&nbsp;</td>
    <td width="23%" align="left" valign="middle">&nbsp;</td>
    <td width="27%" align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle">&nbsp;</td>
    <th align="left" valign="middle">Service Charge</th>
  </tr>
  <tr>
    <td height="25" align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="right" valign="middle">&nbsp;</td>
    <td align="right" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><div class="txtnew">
          <s:property value="%{invoice.serviceCharge}" />
    </div></td>
  </tr> 
  <tr>
    <th height="25" align="left" valign="middle">All prices are in</th>
    <th align="left" valign="middle">Expire in days</th>
    <th align="left" valign="middle">Expire in hours</th>
    <th align="right" valign="middle">&nbsp;</th>
    <th align="left" valign="middle">Total Amount</th>
  </tr>
  <tr>
    <td height="25" align="left" valign="middle"><div class="txtnew">
      <s:property value="%{currencyName}" />
    </div></td>
    <td align="left" valign="middle"><div class="txtnew">
      <s:property value="%{invoice.expiresDay}" />
    </div></td>
    <td align="left" valign="middle"><div class="txtnew">
      <s:property value="%{invoice.expiresHour}" />
    </div></td>
    <td align="right" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><div class="txtnew">
      <s:property value="%{invoice.totalAmount}" />
    </div></td>
  </tr>
      </table></td>
					</tr>
                    <tr>
						<td align="center" valign="top"><br /><table width="100%" border="0"
						cellpadding="0" cellspacing="0">
						<tr>
						<td width="15%" align="left" valign="middle"></td>	
							<td width="5%" align="right" valign="middle">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr><td align="center" valign="middle"><s:submit
									id="btnPay" name="btnPay" class="btn btn-success btn-md btn-block"
									value="Pay Now" >
								</s:submit></td></tr>
								<tr><td>&nbsp;</td></tr>
							<tr><td align="right" valign="middle"><s:label id="lblMsg" class="redsmalltext" value="Link has been expired"></s:label></td></tr>
							</table>
								</td>
							<td width="3%" align="left" valign="middle"></td>
							<td width="15%" align="left" valign="middle"></td>	
						</tr>
					</table><br /></td>
                    </tr>	
                    
                   <tr>
                     <s:hidden id="PAY_ID" name="PAY_ID" value="%{invoice.payId}"/>
					 <s:hidden id="ORDER_ID" name="ORDER_ID" value="%{invoice.invoiceId}"/>
					 <s:hidden id="AMOUNT" name="AMOUNT" value="%{totalamount}"/>
					 <s:hidden id="TXNTYPE" name="TXNTYPE" value="SALE"/>
					 <s:hidden id="CUST_NAME" name="CUST_NAME" value="%{invoice.name}"/>
					 <s:hidden id="CUST_STREET_ADDRESS1" name="CUST_STREET_ADDRESS1" value="%{invoice.address}"/>
					 <s:hidden id="CUST_ZIP" name="CUST_ZIP" value="%{invoice.zip}"/>
					 <s:hidden id="CUST_PHONE" name="CUST_PHONE" value="%{invoice.phone}"/>
					 <s:hidden id="CUST_EMAIL" name="CUST_EMAIL" value="%{invoice.email}"/>
					 <s:hidden id="PRODUCT_DESC" name="PRODUCT_DESC" value="%{invoice.productDesc}"/>
					 <s:hidden id="CURRENCY_CODE" name="CURRENCY_CODE" value="%{invoice.currencyCode}"/>
					 <s:hidden id="RETURN_URL" name="RETURN_URL" value="%{invoice.returnUrl}"/>
					 <s:hidden id="HASH" name="HASH" value="%{hash}"/>
					
                   </tr>				
	</table></td>
		</tr>
	</table><br />
<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
</s:form>
</body>
</html>