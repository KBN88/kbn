<%@taglib prefix="s" uri="/struts-tags"%>
<link href="../css/bootstrap.minr.css" rel="stylesheet">
<link href="../fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="../css/default.css" rel="stylesheet">
<link href="../css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="../css/navigation.css">
<link rel="stylesheet" href="../css/loader.css">
<link href="../css/welcomePage.css" rel="stylesheet">
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.dimensions.js"></script>
<script type="text/javascript" src="../js/jquery.accordion.js"></script>
<link rel="stylesheet" href="../css/newtheam.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<script>
$.noConflict();
// Code that uses other library's $ can follow here.
</script>
<script type="text/javascript">
	jQuery().ready(function() {
		// simple accordion
		jQuery('#list1a').accordion();
		jQuery('#list1b').accordion({
			autoheight : false
		});

		// second simple accordion with special markup
		jQuery('#navigation').accordion({
			active : false,
			header : '.head',
			navigation : true,
			event : 'click',
			fillSpace : false,
			animated : 'easeslide'
		});
	});
</script>

<div id="fade"></div>
<div id="modal" class="lodinggif">
	<img id="loader" src="../image/loader.gif" />
</div>
<div class="row leftnavigation">
	<div class="col-md-12 col-xs-12">
		<nav class="navbar navbar-default top-navbar" role="navigation">
			<div class="headerImage">
		<a class="" href="home"> <img
				src="../image/logo-newr.png" class=""></a>
				</div>

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<!--       <a class="navbar-brand" href="home"><img src="../image/logo-newr.png" width="180"></a> -->
			</div>

			<ul class="nav navbar-top-links navbar-right">

				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#" aria-expanded="false"> <i
						class="fa fa-user fa-fw"></i><b>Welcome <s:property
								value="%{#session.USER.businessName}" /></b> <i
						class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li> -->
						<li><s:a action="logout">
								<i class="fa fa-sign-out fa-fw"></i> Logout</s:a></li>
					</ul> <!-- /.dropdown-user --></li>
				<!-- /.dropdown -->
			</ul>
		</nav>
	</div>
</div>


<div class="col-md-2 left_col col-xs-12" id="wrapperr" style="margin:0px;">

	<!-- sidebar menu -->

	<div>
		<!--/. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation">

		

			<div id="main" style="height: 100%">
				<div class="sidebar-collapse" style="height: 100%">
					<ul id="navigation" class="nav side-menu side-menubottom">
						<li><s:a action='home' class="head1">
								<i class="material-icons">dashboard</i> Dashboard</s:a></li>
						<li><a style="cursor: pointer" class="head"> <i
								class="material-icons" style="color: #27c24c">repeat</i>Transaction Reports<span
								class="fa fa-chevron-down"></span></a>

							<ul>
								<li><s:a action='authorizeTransaction'>Authorize</s:a></li>
								<li><s:a action='captureTransaction'>Sale Transactions</s:a>
								</li>
								<li><s:a action='failedTransaction'>Failed Transactions</s:a>
								</li>
								<li><s:a action='refundConfirmAction'
										onclick='return false' class="sublinks">Refund</s:a></li>
								<li><s:a action='settlementReport'>Settled Report </s:a></li>
								<li><s:a action='summaryReport'>Summary Report </s:a></li>
								<li><s:a action='refundReport'>Refund Transactions</s:a></li>
								<%--    <li><s:a action='incompleteTransaction' >Incompleted</s:a>
                                        </li> --%>

								<%--      <li><s:a action='cancelTransaction' >Cancel</s:a>
                                        </li>
                                      <li><s:a action='invalidTransaction' >Invalid</s:a>
                                        </li> --%>
							</ul></li>
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons" style="color:#26c6da;">timeline</i>Analytics<span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="merachantAnalytics"
										onclick="ajaxindicatorstart1()">Payment Methods</s:a></li>
								<li><s:a action="weeklyAnalytics"
										onclick="ajaxindicatorstart1()">Weekly Analytics</s:a></li>

							</ul></li>
						<li><s:a action='transactionSearch' class="head1">
								<i class="material-icons" style="color:#00acc1">search</i> Search Payments </s:a></li>

						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-sign-in"></i> Remittance <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action='viewRemittance'>View Remittance </s:a></li>
							</ul></li>
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons" style="color: #528ff0">link</i>Link Payment<span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="createPaymentLink">Create Payment Link</s:a></li>
							

								<li><s:a action="invoiceSearch">Search PaymentLink </s:a>

								</li>

							</ul></li>
						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-circle-o-notch"></i> Chargeback Case <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="viewChargeback">View Chargeback Case  </s:a>
								</li>

							</ul></li>



						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-users" style="color: #fbe165;"></i> Manage User <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="addUser">Add User </s:a></li>
								<li><s:a action="searchUser">User List</s:a></li>
								<li><s:a action='loginHistoryRedirectUser'>User Login History</s:a>
								</li>
							</ul></li>

						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-user" style="color: #528ff0;"></i> My Account <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="merchantProfile">My Profile </s:a></li>
								<li><s:a action="loginHistoryRedirect">Login History</s:a>
								</li>
								<li><s:a action='passwordChange'>Change Password</s:a></li>
								<li><s:a action='summary'>Account Summary</s:a></li>
								<li><s:a action='paymentPageSetting'>Design Payment Page</s:a>
								</li>
							</ul></li>
						<!-- ticketing -->
						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-list-alt"></i>Help Ticket <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="createTicket">
								 Create Tickets </s:a></li>
								<li><s:a action="viewAllTickets">
								 View All Tickets </s:a></li>
							</ul></li>
					</ul>
				</div>
			</div>




		</nav>
		<!-- /. NAV SIDE  -->
		<!-- /. PAGE WRAPPER  -->
	</div>


	<!-- /sidebar menu -->

</div>

<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->

<script src="../js/bootstrap.min.js"></script>
<script>
	;(function ($, window, document, undefined) {

	    var pluginName = "metisMenu",
	        defaults = {
	            toggle: true
	        };
	        
	    function Plugin(element, options) {
	        this.element = element;
	        this.settings = $.extend({}, defaults, options);
	        this._defaults = defaults;
	        this._name = pluginName;
	        this.init();
	    }

	    Plugin.prototype = {
	        init: function () {

	            var $this = $(this.element),
	                $toggle = this.settings.toggle;

	            $this.find('li.active').has('ul').children('ul').addClass('collapse in');
	            $this.find('li').not('.active').has('ul').children('ul').addClass('collapse');

	            $this.find('li').has('ul').children('a').on('click', function (e) {
	                e.preventDefault();

	                $(this).parent('li').toggleClass('active').children('ul').collapse('toggle');

	                if ($toggle) {
	                    $(this).parent('li').siblings().removeClass('active').children('ul.in').collapse('hide');
	                }
	            });
	        }
	    };

	    $.fn[ pluginName ] = function (options) {
	        return this.each(function () {
	            if (!$.data(this, "plugin_" + pluginName)) {
	                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
	            }
	        });
	    };

	})(jQuery, window, document);
</script>
<script>

(function ($) {
"use strict";
var mainApp = {

    initFunction: function () {
        /*MENU 
        ------------------------------------*/
        $('#main-menu').metisMenu();
		
        $(window).bind("load resize", function () {
            if ($(this).width() < 768) {
                $('div.sidebar-collapse').addClass('collapse')
            } else {
                $('div.sidebar-collapse').removeClass('collapse')
            }
        });

 
    },

    initialization: function () {
        mainApp.initFunction();

    }

}
// Initializing ///

$(document).ready(function () {
    mainApp.initFunction();
});

}(jQuery));
</script>
