<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Support</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="taskhelp">
  <tr>
    <td height="35" align="left" valign="middle" class="boxheading">Contact Customer Support</td>
  </tr>
  <tr>
    <td align="center" class="tdPad"><table width="55%" border="0" cellspacing="0" cellpadding="0" class="greyroundtble">
      <tr>
        <td colspan="2" align="right"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="greyroundtble">
      <tr>
        <td align="left" class="txtnew">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td width="25%" height="50" align="left" class="txtnew"><b>Subject:</b></td>
        <td align="left">
          <select name="select" id="select" class="signuptextfield">
          </select></td>
      </tr>
      <tr>
        <td height="50" align="left" class="txtnew"><b>Message:</b></td>
        <td align="left"><label for="textarea"></label>
          <textarea name="textarea" id="textarea" cols="45" rows="5" class="sigfld"></textarea></td>
      </tr>
      <tr>
        <td height="50" align="left" class="txtnew"><b>Attach File:</b></td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="left"><input type="submit" name="button" id="button" value="Submit" class="btn btn-success btn-md"></td>
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
    </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>