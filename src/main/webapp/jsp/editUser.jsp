<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modify SubUser</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.js"></script>
<script src="../js/commanValidate.js"></script>
<script language="JavaScript">	
$(document).ready( function () {
	handleChange();
});
	function handleChange(){

   	  var str = '<s:property value="permissionString"/>';
   	  var permissionArray = str.split("-");
   	  for(j=0;j<permissionArray.length;j++){
			selectPermissions(permissionArray[j]);
		}
    }
	
	function selectPermissions(permissionType){		
		var permissionCheckbox = document.getElementById(permissionType);
		if(permissionCheckbox==null){
			return;
		}
		permissionCheckbox.checked = true;
	}
</script>
<script type="text/javascript">	

 $(document).ready( function() {
    
	$('#btnEditUser').click(function() {	
			var answer = confirm("Are you sure you want to edit user details?");
				if (answer != true) {
					return false;
				} else {
					 $.ajax({
							url : 'editUserDetails',
							type : "POST",
							data : {
									firstName : document.getElementById('firstName').value ,						
									lastName:document.getElementById('lastName').value,
									mobile : document.getElementById('mobile').value,
									emailAddress : document.getElementById('emailAddress').value,
									isActive: document.getElementById('isActive').value,
								},									
									success:function(data){		       	    		       	    		
					       	   		var res = data.response;		       	    	
					          		},
										error:function(data){	
					           	    		alert("Unable to edit user details");
										}
									});		 
		            }
		      });	        
	   });
</script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="txnf">
  <tr>
    <td align="left"><h2>Edit SubUser</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><div id="saveMessage">
        <s:actionmessage class="success success-text" />
      </div></td>
  </tr>
  <tr>
    <td align="left" valign="top"><div class="addu">
        <s:form action="editSubUserDetails" id="frmEditUser" >
          <div class="adduT">First Name<br>
            <div class="txtnew">
              <s:textfield	name="firstName" id="firstName" cssClass="signuptextfield" autocomplete="off" />
            </div>
          </div>
          <div class="adduT">Last Name<br>
            <div class="txtnew">
              <s:textfield	name="lastName" id="lastName" cssClass="signuptextfield" autocomplete="off"/>
            </div>
          </div>
          <div class="adduT">Phone<br>
            <div class="txtnew">
              <s:textfield name="mobile" id="mobile" cssClass="signuptextfield" onkeypress="javascript:return isNumber (event)"  maxlength="13" autocomplete="off" />
            </div>
          </div>
          <div class="adduT">Email<br>
            <div class="txtnew">
              <s:textfield name="emailId" id="emailAddress" cssClass="signuptextfield" autocomplete="off" readonly="true" />
            </div>
          </div>
          
         <div class="adduT">Is Active ?
            <s:checkbox	name="isActive" id="isActive" />
            <label class="labelfont" for="1"></label>
            
          </div>
          
          <div class="adduT">Account Privileges<br>
          
            <table width="100%" cellpadding="0" cellspacing="0" class="whiteroundtble">
              <s:iterator value="listPermissionType" status="itStatus">
                <tr>
                  <td  width="5%" align="left">&nbsp;</td>
                  <td width="5%" height="30" align="left"><s:checkbox name="lstPermissionType"
																	id="%{permission}" fieldValue="%{permission}"
																	 value="false"></s:checkbox></td>
                  <td width="85%" align="left"><label class="labelfont" for="1">
                      <s:property value="permission" />
                    </label></td>
                </tr>
              </s:iterator>
            </table>
            <div class="clear"></div>
          </div>
          
          <div class="adduT" style="padding-top:10px">
            <s:submit id="btnEditUser" name ="btnEditUser" value="Save" type="button" cssClass="btn btn-success btn-md"> </s:submit>
          </div>
          <s:hidden name="payId" id="payId"  /> 
         <s:hidden name="token" value="%{#session.customToken}"></s:hidden>
        </s:form>
        <div class="clear"></div>
      </div></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>