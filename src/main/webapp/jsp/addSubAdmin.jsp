<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add SubAdmin</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.minshowpop.js"></script>
<script src="../js/jquery.formshowpop.js"></script>
<script src="../js/commanValidate.js"></script>
<script language="JavaScript">	
$(document).ready( function () {
	handleChange();
});
	function handleChange(){

   	  var str = '<s:property value="permissionString"/>';
   	  var buttonFlag = '<s:property value="disableButtonFlag"/>';
   	  if(buttonFlag=='true'){
   		 document.getElementById('submit').style.display='none';
   	  }
   	
   	  var permissionArray = str.split("-");
   	  for(j=0;j<permissionArray.length;j++){
			selectPermissions(permissionArray[j]);
		}
    }
	
	function selectPermissions(permissionType){		
		var permissionCheckbox = document.getElementById(permissionType);
		if(permissionCheckbox==null){
			return;
		}
		permissionCheckbox.checked = true;
	}
</script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="txnf">
  <tr>
    <td align="left"><h2>Add New SubAdmin</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><div id="saveMessage">
        <s:actionmessage class="success success-text" />
      </div></td>
  </tr>
  <tr>
    <td align="left" valign="top"><div class="addu">
        <s:form action="addSubAdmin" id="frmAddUser" >
          <s:token/>
          <div class="adduT">First Name<br>
            <s:textfield name="firstName" cssClass="signuptextfield" autocomplete="off"/>
            </div>
            <div class="adduT">Last Name<br>
              <s:textfield	name="lastName" cssClass="signuptextfield" autocomplete="off"/>
            </div>
            <div class="adduT">Phone<br>
              <s:textfield name="mobile" cssClass="signuptextfield" autocomplete="off" onkeypress="javascript:return isNumber (event)"/>
            </div>
            <div class="adduT">Email<br>
              <s:textfield name="emailId" cssClass="signuptextfield" autocomplete="off" />
            </div>
            <div class="adduT">Account Privileges<br>
            
              <table width="100%" cellpadding="0" cellspacing="0" class="whiteroundtble">
                <s:iterator value="listPermissionType" status="itStatus">
                  <tr>
                    <td  width="5%" align="left">&nbsp;</td>
                    <td width="5%" height="30" align="left"><s:checkbox name="lstPermissionType"
																	id="%{permission}" fieldValue="%{permission}"
																	 value="false" autocomplete="off"></s:checkbox></td>
                    <td width="85%" align="left"><label class="labelfont" for="1">
                        <s:property
																			value="permission" />
                      </label></td>
                  </tr>
                </s:iterator>
              </table>
              <div class="clear"></div>
            </div>

            <div class="adduT" style="padding-top:10px">
              <s:submit id="submit" value="Save" method ="submit" cssClass="btn btn-success btn-md"> </s:submit>
            </div>
            
            <s:hidden name="token" value="%{#session.customToken}"></s:hidden>
        </s:form>
        <div class="clear"></div>
          </div>
          
          
          </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>