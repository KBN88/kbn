<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<html>
<head>
<title>MIS Report</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script type="text/javascript">
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	function handleChange() {
		reloadTable();
		reloadCitrusTable();
	}
	$(document).ready(function() {
		$(function() {

			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});
		$(function() {
			var today = new Date();
			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			renderTable();
			renderCitrusTable();
		});

	});
	function renderTable() {
		var merchantEmailId = document.getElementById("merchants").value;
		var table = new $.fn.dataTable.Api('#misReportDataTable');
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				$(".modal").show();
			},
			complete : function() {
				$(".modal").hide();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#misReportDataTable').dataTable(
				{
					dom : 'BTftlpi',
					columnDefs : [ {
						targets : 0,
						/*    autoWidth: true,
						   searchable: false,
						   orderable: false, */
						render : function(data, type, row, info) {
							return parseInt(info.row) + 1;
						}
					} ],
					buttons : [
							{
								extend : 'copyHtml5',
								exportOptions : {
									columns : [ 0, 1, 17, 18, 4, 6, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ]
								}
							},
							{
								extend : 'csvHtml5',
								title : 'MIS Report',
								exportOptions : {
									columns : [ 0, 1, 17, 18, 4, 6, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ]
								}
							},
							{
								extend : 'colvis',
								columns : [ 1, 3, 4, 6, 8, 14 ]
							} ],
					"ajax" : {
						"url" : "misSettementReport",
						"type" : "POST",
						"data" : function (d){
							return generatePostData(d);
						}
					},
					  "processing": true,
				        "serverSide": true,
				        "paginationType": "full_numbers", 
				        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				        "order" : [ [ 1, "desc" ] ], 
					"columns" : [ {
						"data" : "",
						"width" : '1%'
					}, {
						"data" : "businessName",
						"width" : '8%'
					}, {
						"data" : "payId",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : "transactionId",
						"width" : '8%'
					}, {
						"data" : "accountNumber",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : "aggregatorName",
						"width" : '8%',
						"defaultContent" : "Urspay"
					}, {
						"data" : "nodalAccount",
						"width" : '8%',
						"defaultContent" : "'50200011639867"

					},

					{
						"data" : "txnType",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : "txnDate",
						"width" : '8%'
					}, {
						"data" : "bankName",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : "ifscCode",
						"visible" : false,
						"className" : "displayNone"

					}, {
						"data" : "bankRecieveFund",
						"width" : '7%'

					}, {
						"data" : "approvedAmount",
						"width" : '8%'

					}, {
						"data" : "totalAggregatorcommissionAmount",
						"width" : '7%'

					}, {
						"data" : "totalAmountPayToMerchant",
						"width" : '7%'
					}, {
						"data" : "totalPayoutToNodal",
						"width" : '7%'
					}, {
						"data" : "currentDate",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : null,
						"width" : '14%',
						"visible" : false,
						"mRender" : function(row) {
							return "\u0027" + row.payId;
						}
					}, {
						"data" : null,
						"width" : '14%',
						"visible" : false,
						"mRender" : function(row) {
							return "\u0027" + row.transactionId;
						}
					}, ]
				});
		$("#merchants").select2({
			//data: payId
			});
	}

	function reloadTable() {
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#misReportDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}
	function decodeDiv() {
		var divArray = document.getElementsByTagName('div');
		for (var i = 0; i < divArray.length; ++i) {
			var div = divArray[i];
			if (div.id.indexOf('param-') > -1) {
				var val = div.innerHTML;
				div.innerHTML = decodeVal(val);
			}
		}
	}

	function decodeVal(value) {
		var txt = document.createElement("textarea");
		txt.innerHTML = value;
		return txt.value;
	}

	function generatePostData(d) {
		var token = document.getElementsByName("token")[0].value;
		var merchantEmailId = document.getElementById("merchants").value;
		if (merchantEmailId == '') {
			merchantEmailId = 'ALL'
		}
		var obj = {
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			merchantEmailId : merchantEmailId,
			acquirer : document.getElementById("account").value,
			draw : d.draw,
			length :d.length,
			start : d.start,
			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}
	$(function() {
		$("#account").change(function() {
			console.log(this);
			if ($(this).val() == "CITRUS") {
				$("#dvAllAcquire").hide();
				$("#dvCitrus").show();
			} else {
				$("#dvCitrus").hide();
				$("#dvAllAcquire").show();

			}

		});
	});

	function renderCitrusTable() {
		var table = new $.fn.dataTable.Api('#misCitrustDataTable');
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				$(".modal").show();
			},
			complete : function() {
				$(".modal").hide();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#misCitrustDataTable')
				.dataTable(
						{
							"footerCallback" : function(row, data, start, end,
									display) {
								var api = this.api(), data;

								// Remove the formatting to get integer data for summation
								var intVal = function(i) {
									return typeof i === 'string' ? i.replace(
											/[\,]/g, '') * 1
											: typeof i === 'number' ? i : 0;
								};

								// Total over all pages
								total = api.column(16).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(16, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(16).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');
							},
							 "columnDefs": [{ className: "dt-body-right","targets": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]}],
							dom : 'BTftlpi',
							buttons : [
									{
										extend : 'copyHtml5',
										exportOptions : {
											columns : []
										}
									},
									{
										extend : 'csvHtml5',
										title : 'MIS Report',
										footer : true,
										header : true,
										exportOptions : {
											columns : [0,1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17,18]
										}
									},
									{
										extend : 'colvis',
										columns : []
									} ],
							"ajax" : {
								"url" : "misCitrusSettementReport",
								"type" : "POST",
								
								"data" : function (d){
									return generatePostData(d);
								}
							},
							   "processing": true,
						        "serverSide": true,
						        "paginationType": "full_numbers", 
						        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
						        "order" : [ [ 1, "desc" ] ], 
							"columns" : [
									{
										"data" : "Detail",
										"width" : '3%',
										"defaultContent" : "D"
									},
									{
										"data" : "msgType",
										"width" : '4%',
									},
									{
										"data" : "nodalAccount",
										"width" : '5%',
										"defaultContent" : "'008261100000016"

									},
									{
										"data" : "aggregatorName",
										"width" : '8%',
										"defaultContent" : "Urspay"
									}, {
										"data" : "AddressLine1",
										"visible" : false,
										"className" : "displayNone",
										"defaultContent" : ""
									}, {
										"data" : "AddressLine2",
										"visible" : false,
										"className" : "displayNone",
										"defaultContent" : ""
									}, {
										"data" : "AddressLine3",
										"visible" : false,
										"className" : "displayNone",
										"defaultContent" : ""
									}, {
										"data" : "ifscCode",
										"width" : '8%',

									}, {
										"data" : "accountNumber",
										"width" : '8%',
									}, {
										"data" : "businessName",
										"width" : '8%'
									},{
										"data" : "BenAddress1",
										"visible" : false,
										"className" : "displayNone",
										"defaultContent" : ""
									}, {
										"data" : "BenAddress2",
										"visible" : false,
										"className" : "displayNone",
										"defaultContent" : ""
									},{
										"data" : "BenAddress",
										"width" : '8%',
										"defaultContent" : "MUMBAI"
									}, {
										"data" : "BenAddress4",
										"visible" : false,
										"className" : "displayNone",
										"defaultContent" : ""
									},{
										"data" : "txnRefNumber",
										"width" : '8%'
									}, {
										"data" : "txnDate",
										"width" : '8%'
									}, {
										"data" : "totalAmountPayToMerchant",
										"width" : '8%'
									}, {
										"data" : "Sender to Rcvr Info",
										"width" : '8%',
										"defaultContent" : "Vendor Payment"
									}, {
										"data" : null,
										"width" : '14%',
										"visible" : false,
										"mRender" : function(row) {
											return "\u0027" + row.txnRefNumber;
										}
									}]
						});
	}
	function reloadCitrusTable() {
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#misCitrustDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}
</script>
<script type="text/javascript">
	
</script>
<style>
.dataTables_wrapper {
	position: relative;
	clear: both;
	*zoom: 1;
	zoom: 1;
	margin-top: -30px;
}
</style>
</head>
<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" valign="top"><table width="100%" border="0"
					align="center" cellpadding="0" cellspacing="0" class="txnf">
					<tr>
						<td colspan="5"><h2>MIS Reports</h2>
							<div class="container">

								<s:if
									test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'  || #session.USER_TYPE.name()=='SUPERADMIN'}">
									<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">
										<div class="txtnew">
											<label for="acquirer">Acquirer Name:</label><br />
											<s:select headerKey="ALL" headerValue="ALL" id="account"
												name="account" class="form-control"
												list="@com.kbn.pg.core.NodalAccountMapper@values()"
												listKey="code" listValue="name" onchange="handleChange();" autocomplete="off" />
										</div>
									</div>
								</s:if>

								<s:if
									test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
									<div class="form-group col-md-2 col-xs-6 col-sm-3 txtnew">

										<div class="txtnew">
											<label for="acquirer">Merchant Name:</label><br />
											<s:select name="merchants" class="form-control"
												id="merchants" headerKey="" headerValue="ALL"
												list="merchantList" listKey="emailId"
												listValue="businessName" onchange="handleChange();" autocomplete="off" />
										</div>
									</div>
								</s:if>

								<s:else>
									<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">


										<div class="txtnew">
											<label for="acquirer">Merchant Name:</label><br />
											<s:select name="merchants" class="form-control"
												id="merchants" headerKey="" headerValue="ALL"
												list="merchantList" listKey="emailId"
												listValue="businessName" onchange="handleChange();" autocomplete="off" />
										</div>

									</div>
								</s:else>
								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateFrom">Date From:</label> <br />
									<s:textfield type="text" readonly="true" id="dateFrom"
										name="dateFrom" class="form-control" autocomplete="off"
										onchange="handleChange();" />
								</div>
								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateTo">Date To:</label> <br />
									<s:textfield type="text" readonly="true" id="dateTo"
										name="dateTo" class="form-control" onchange="handleChange();" autocomplete="off" />
								</div>
							</div></td>
					</tr>
					<tr>
						<td colspan="5" align="left" valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td align="left" valign="top" style="padding: 10px;">
							<div class="scrollD" id="dvAllAcquire" style="display: block;">
								<table id="misReportDataTable" align="center" cellspacing="0"
									width="100%">
									<thead>
										<tr class="boxheadingsmall">
											<th>S.No</th>
											<th>Merchant Name</th>
											<th>Pay Id</th>
											<th>Txn_Id</th>
											<th>Sub Merchant Account No.</th>
											<th>Aggregator Name</th>
											<th>Nodal a/c No.</th>
											<th>Transaction Type(Sale/Refund/Hold/Chargeback)</th>
											<th>Transaction Date</th>
											<th>Beneficiary Bank Name</th>
											<th>Beneficiary IFSC code</th>
											<th>Bank from where funds to be received</th>
											<th>Gross Transaction Amt</th>
											<th>Total Aggregator Commission Amt Payable (Including Service Tax)</th>
											<th>Total Amt Payable to Merchant A/c</th>
											<th>Total Payout from Nodal Account</th>
											<th>T' DATE</th>
											<th>Pay Id</th>
											<th>Txn_Id</th>

										</tr>
									</thead>
								</table>
							</div>

							<div class="scrollD" id="dvCitrus" style="display: none">
								<table id="misCitrustDataTable" align="center" cellspacing="0"
									width="100%">
									<!--  <thead>
            </thead> -->
									<thead>
										<tr class="boxheadingsmall">
											<th>H</th>
											<th><%
   Date date = new Date();
SimpleDateFormat AppDateFormat = new SimpleDateFormat("dd/MM/yyyy");
   out.print(AppDateFormat.format(date) );
%></th>
											<th>kbn</th>
										</tr>
										<tr class="boxheadingsmall">
											<th style='text-align:left'>Detail</th>
											<th style='text-align:left'>Msg Type</th>
											<th style='text-align:center'>Debit account number</th>
											<th style='text-align:center'>Ordering Customer name</th>
											<th style='text-align:center'>Address Line 1</th>
											<th style='text-align:center'>Address Line 2</th>
											<th style='text-align:center'>Address Line 3</th>
											<th style='text-align:center'>IFSC Code</th>
											<th style='text-align:center'>Bene account no.</th>
											<th style='text-align:center'>Bene Name</th>
											<th style='text-align:center'>Bene Add Line 1</th>
											<th style='text-align:center'>Bene Add Line 2</th>
											<th style='text-align:center'>Bene Add Line 3</th>
											<th style='text-align:center'>Bene Add Line 4</th>
											<th style='text-align:center'>Txn Ref no</th>
											<th style='text-align:center'>Date</th>
											<th style='text-align:right'>Amount</th>
											<th>Sender to Rcvr Info</th>
											 <th>Txn Ref no</th> 
										</tr>
									</thead>
									<tfoot>
										<tr class="boxheadingsmall">
											<th>F</th>
											<th>1</th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th ></th>
											<th style='text-align:right; float:right; padding-right:5px;'></th>
											<th ></th>

										</tr>
									</tfoot>
								</table>
					</div>
					</td>
					</tr>
				</table></td>
		</tr>
	</table>
</body>
</html>