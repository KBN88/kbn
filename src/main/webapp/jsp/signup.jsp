<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Sign Up</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.minshowpop.js"></script>
<script src="../js/jquery.formshowpop.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<link href="http://fonts.googleapis.com/css?family=Crafty+Girls"
	rel="stylesheet" type="text/css" />
<script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
</script>
<script type="text/javascript">
	$(function() {
		$("#userRoleType").change(function() {
			if ($(this).val() == "merchant") {
				$("#tdIndustryType").show();
			} else {
				$("#tdIndustryType").hide();
			}

		});

		$("#industryCategory").change(function() {
			var industry = this.value;
			var token = document.getElementsByName("token")[0].value;
			if (industry == 'select') {
				$("#subcategorydiv").hide();
				var subCategoryText = document.getElementById("subcategory");
				subCategoryText.value = "";
				return false;
			}
			$.ajax({
				type : "POST",
				url : "industrySubCategory",
				data : {
					industryCategory : industry,
					token : token,
					"struts.token.name" : "token"
				},
				success : function(data, status) {
					var subCategoryListObj = data.subCategories;
					var subCategoryList = subCategoryListObj[0].split(',');
					var radioDiv = document.getElementById("radiodiv");
					radioDiv.innerHTML = "";
					for (var i = 0; i < subCategoryList.length; i++) {
						var subcategory = subCategoryList[i];
						var radioOption = document.createElement("INPUT");
						radioOption.setAttribute("type", "radio");
						radioOption.setAttribute("value", subcategory);
						radioOption.setAttribute("name", "subcategory");
						var labelS = document.createElement("SPAN");
						labelS.innerHTML = subcategory;
						radioDiv.appendChild(radioOption);
						radioDiv.appendChild(labelS);
					}
					$('#popup').popup('show');
				},
				error : function(status) {
					alert("Network error please try again later!!");
				}
			});
		});
	});

	function selectSubcategory() {
		var checkedRadio = $('input[name="subcategory"]:checked').val();
		var subCategoryDiv = document.getElementById("subcategorydiv");
		var subCategoryText = document.getElementById("subcategory");
		subCategoryText.value = checkedRadio;
		subCategoryDiv.style.display = "block";
		$('#popup').popup('hide');
		//make subcategory editable/
		//insert details if other selected
		//validation for required field
		//refresh if category changed
	}
</script>
</head>
<body onload="return generateCaptcha();">

	<div id="popup" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">
			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="1" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">Select
								sub category</th>
						</tr>
						<tr>
							<td colspan="2" height="30" align="left">
								<div id="radiodiv"></div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" value="Done"
								onclick="selectSubcategory()" class="btn btn-success btn-sm"
								style="margin-left: 38%; width: 21%; height: 100%; margin-top: 1%;" /></td>
						</tr>
					</table>

				</div>
			</div>
		</div>
	</div>

	<table width="100" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td align="center" valign="bottom" class="sigNew"><img
				src="../image/logo-signup.png" /></td>
		</tr>
		<tr>
			<td><s:div class="signupbox">
					<table width="295" border="0" align="center" cellpadding="0"
						cellspacing="0">
						<tr>
							<td align="center" class="signup-headingbg" style="background: linear-gradient(60deg, #425185, #4a9b9b);"><h4 style="color: #FFF">Create
									A New Account</h4></td>
						</tr>
						<tr>
							<td align="center"><table width="90%" border="0"
									cellspacing="0" cellpadding="0">

									<tr>
										<td align="left"><s:form action="signup" id="formname">
												<s:token />
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td height="10" align="left"><span id="error2"></span></td>
													</tr>
													<s:actionmessage />
													<tr>
														<td height="50" align="left"><s:select
																name="userRoleType" id="userRoleType" headerKey="1"
																list="#{'merchant':'I am a Merchant','reseller':'I am a Reseller'}"
																class="signupdropdwn">
															</s:select></td>
													</tr>
													<tr>
														<td height="50" align="left" id="tdIndustryType"
															style="display: block;"><s:select
																name="industryCategory" id="industryCategory"
																headerKey="select" headerValue="-Select Industry Type-"
																list="industryCategory"
																value="{'-Select Industry Type-'}" class="signupdropdwn"
																autocomplete="off">
															</s:select></td>
													</tr>
													<tr>
														<td>
															<div id="subcategorydiv" style="display: none;">
																<s:textfield id="subcategory" name="industrySubCategory"
																	cssClass="signuptextfield" placeholder="Sub category"
																	autocomplete="off" />
															</div>
														</td>
													</tr>
													<tr>
														<td height="50" align="left"><s:textfield
																id="businessName" name="businessName"
																cssClass="signuptextfield" placeholder="Business Name"
																autocomplete="off" onkeypress="return Validate(event);" /></td>
													</tr>
													<tr>
														<td height="50" align="left"><s:textfield
																id="emailId" name="emailId" cssClass="signuptextfield"
																placeholder="Email" autocomplete="off"
																onblur="isValidEmail()" /></td>
													</tr>
													<tr>
														<td height="50" align="left"><s:textfield id="mobile"
																name="mobile" cssClass="signuptextfield"
																placeholder="Phone" autocomplete="off"
																onkeypress="javascript:return isNumber (event)" /></td>
													</tr>
													<tr>
														<td height="50" align="left"><s:textfield
																id="password" name="password" type="password"
																cssClass="signuptextfield" placeholder="Password"
																onblur="passCheck()" autocomplete="off" /></td>
													</tr>
													<tr>
														<td height="50" align="left"><s:textfield
																id="confirmPassword" name="confirmPassword"
																type="password" cssClass="signuptextfield"
																placeholder="Confirm Password" onblur="passCheck()"
																autocomplete="off" /></td>
													</tr>
													<tr>


														<td align="left" height="50" valign="middle">
															<table width="100%" cellpadding="0" cellspacing="0">
																<tr>
																	<td width="51%" align="left"><s:textfield
																			name="captcha" type="text"
																			cssClass="signuptextfieldsml" id="captcha"
																			placeholder="Enter Captcha Code" autocomplete="off" /></td>
																	<td width="6%" align="left">&nbsp;</td>
																	<td width="32%" align="center"><s:textfield
																			name="captchaCode" type="text" cssClass="accesscode"
																			id="captchaCode" autocomplete="off"
																			oncopy="return false" /></td>
																	<td width="11%" align="right"><s:textfield
																			id="Btnaccescd" type="button" class="refreshbutton"
																			value="" onclick="return generateCaptcha();" /></td>

																</tr>
															</table>
															<div class="rederror" id="error3"></div>


														</td>

													</tr>

													<tr>
														<td height="60" align="left" valign="bottom"><s:submit
																value="Sign Up" method="submit" style="background: linear-gradient(60deg, #425185, #4a9b9b); color:#fff;font-size:14px"
																cssClass="signupbutton btn-primary"
																onclick="return ValidCaptcha();">
															</s:submit></td>
													</tr>
													<tr>
														<td height="40" align="center" valign="middle"
															class="text1">Already have an account ? <s:a
																action="index">Login here</s:a></td>
													</tr>
												</table>
											</s:form></td>
									</tr>
								</table></td>
						</tr>
					</table>
				</s:div>
		</tr>
	</table>
	<script>
		$(document)
				.ready(
						function() {

							var fields = {

								password : {
									tooltip : "Password must be minimum 8 and <br> maximum 32 characters long, with <br> special characters (! @ , _ + / =) , <br> at least one uppercase and  one <br>lower case alphabet.",
									position : 'right',
									backgroundColor : "#6ad0f6",
									color : '#FFFFFF'
								},
							};

							//Include Global Color 
							$("#formname").formtoolip(fields, {
								backgroundColor : "#000000",
								color : "#FFFFFF",
								fontSize : 14,
								padding : 10,
								borderRadius : 5
							});

						});
	</script>
	<script src="../js/commanValidate.js"></script>
	<script src="../js/captcha.js"></script>
</body>
</html>