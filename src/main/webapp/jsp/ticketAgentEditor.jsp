<%@page import="com.kbn.ticketing.core.HelpTicket"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script src="../js/continents.js" type="text/javascript"></script>
<script src="../js/jquery.js"></script>
<script src="../js/follw.js"></script>
<script src="../js/main.js"></script>

<style>
.errorMessage {
	color: #ff0000;
	text-align: right;
}
</style>
<script>
function ticketDetailsUpdate(){
	var token = document.getElementsByName("token")[0].value;
	var ticketId = document.getElementById("ticketId").value;
	var contactEmailId = document.getElementById("email").value;
	var ticketStatus = document.getElementById("ticketStatus").value;
	var subject = document.getElementById("subject").value;
	var messageBody = document.getElementById("messageBody").value;
	var agentList = document.getElementById("agentList").value;
	var contactMobileNo = document.getElementById("mobileNo").value;
	var ticketType = document.getElementById("ticketType").value;
	 $.ajax({
		 url : 'ticketUpdater',
			 type : 'post',
				 data :{
						ticketId: ticketId,
						contactEmailId : contactEmailId,
						ticketStatus : ticketStatus,
						subject : subject,
						messageBody : messageBody,
						assignedAgentPayId : agentList,
						contantMobileNo : contactMobileNo,
						ticketType : ticketType,
						
						
					token : token,
				 } ,
				 success : function(data){
					 var responseDiv = document.getElementById("response");
						responseDiv.innerHTML = data.response;
						responseDiv.style.display = "block";
						
						 if(data == null){
							 responseDiv.innerHTML = "Operation not successfull, please try again later!!"
									responseDiv.style.display = "block";
							 responseDiv.className = "error error-new-text";
							 event.preventDefault();
						 } 
					
						 responseDiv.className = "success success-text";

						
		 },
				 error : function(data) {
						var responseDiv = document.getElementById("response");
						responseDiv.innerHTML = "Ticket update not successfully, please try again later!!"
						responseDiv.style.display = "block";
						responseDiv.className = "error error-new-text";
					}
			 });
	}

function viewComments(){
	var token = document.getElementsByName("token")[0].value;
	
	var agentMessage= document.getElementById("comments").value;
	var ticketId = document.getElementById("ticketId").value;
	 $.ajax({
		 url : 'commentCreaterAction',
			 type : 'post',
				 data :{
						ticketId: ticketId,
						comment : agentMessage,
						
					token : token,
				 } ,
				 success : function(data){
					 var responseDiv = document.getElementById("response");
						responseDiv.innerHTML = data.response;
						responseDiv.style.display = "block";
						var responseData = data.response;
						 if(responseData == null){
							 responseDiv.innerHTML = "Operation not successfull, please try again later!!"
									responseDiv.style.display = "block";
							 responseDiv.className = "error error-new-text";
							 event.preventDefault();
						 } 
					 var commentFetchTextField = document.getElementById("allComments"); 
						 responseDiv.className = "success success-text";
						  window.location.reload();
						
		 },
				 error : function(data) {
						var responseDiv = document.getElementById("response");
						responseDiv.innerHTML = "message not send successfully, please try again later!!"
						responseDiv.style.display = "block";
						responseDiv.className = "error error-new-text";
					}
			 });
	}

</script>
</head>
<body>
	<div style="display: none" id="response"></div>
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td align="left" valign="middle"><s:form action="" method="post"
					autocomplete="off" class="FlowupLabels" theme="css_xhtml"
					validate="true">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" valign="top" bgcolor="#0271BB">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td align="left" width="68%" valign="middle"
											style="font: bold 14px verdana; color: #fff; padding: 4px 0 4px 8px;">Edit
											Ticket</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</s:form></td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="bottom" height="30"><div
					id="saveMessage">
					<s:actionmessage class="success success-text" />
				</div></td>
		</tr>
		<tr>
			<td align="left" valign="middle"><s:form action="" name=""
					id="formid1" method="post" theme="css_xhtml" class="FlowupLabels">
					<s:div cssClass="indent">
						<table width="100%" border="0" cellspacing="0" cellpadding="7"
							class="formboxRR">
							<tr>
								<td align="left" valign="top">&nbsp;</td>
							</tr>
							<tr>
								<td align="left" valign="top">
									<div class="addfildn">
										<div class="rkb">
											<div class="addfildn">
												<div class="fl_wrap">
													<label class='fl_label'>Ticket ID</label>
													<s:textfield type="text" name="ticketId"
														value="%{helpTicket.ticketId}" id="ticketId"
														class="fl_input"></s:textfield>
												</div>
											</div>
											<div class="addfildn">
												<div class="fl_wrap">
													<label class='fl_label'>Contact E-mail ID</label>
													<s:textfield type="text" name="contactEmailId"
														value="%{HelpTicket.contactEmailId}" id="email"
														class="fl_input" autocomplete="off" />
												</div>
											</div>
											<div class="addfildn">
												Ticket Status
												<s:select id="ticketStatus" name="ticketStatus"
													class="textFL_merch"
													list="@com.kbn.ticketing.commons.util.TicketStatus@values()"
													headerKey="statusCode" headerValue="status"
													autocomplete="off" style="height:46px;" />
											</div>
											<div class="addfildn" style="width: 209%;">
												<div class="fl_wrap">
													<label class='fl_label'>Subject</label>
													<s:textfield type="text" name="subject"
														value="%{HelpTicket.subject}" id="subject"
														class="fl_input" autocomplete="off" />
												</div>
											</div>
											<div class="rkb" style="width: 70%">
												<div class="">
													<div class='fl_wrap'
														style="height: 106px; margin-left: -9%; margin-top: -3%; width: 313%;">
														<label class='fl_label'>Ticket Body</label>
														<s:textfield type="text" rows="5" class="fl_input"
															id="messageBody" name="messageBody"
															value="%{HelpTicket.messageBody}" autocomplete="off"
															style="height:95px; resize: none; outline-width:0"
															theme="simple" />
													</div>
												</div>
											</div>

											<div class="addfildn">
												Assigned To
												<s:select name="agent" class="textFL_merch" id="agentList"
													list="agentList" listKey="payId" listValue="agentEmailId"
													style="height:46px;" />
											</div>
											<s:if test="%{#session.USER.UserType.name()=='ADMIN'}">
												<div class="addfildn">
													<input type="button" id="btnSave" name="btnSave"
														class="btn btn-success btn-md" value="Submit"
														onclick="ticketDetailsUpdate()"
														style="display: inline; margin-left: 199%;">

												</div>
											</s:if>

											<div class="clear"></div>
										</div>
										<div class="rkb">
											<div class="addfildn">
												<div class="fl_wrap">
													<label class='fl_label'>Contact Mobile No.</label>
													<s:textfield type="text" name="contantMobileNo"
														value="%{HelpTicket.contactMobileNo}" id="mobileNo"
														class="fl_input" autocomplete="off" />
												</div>
											</div>
											<div class="addfildn">
												Ticket Type
												<s:select id="ticketType" name="ticketType"
													class="textFL_merch"
													list="@com.kbn.ticketing.commons.util.TicketType@values()"
													headerKey="ticketTypeCode" headerValue="ticketType"
													autocomplete="off" style="height:46px;" />
											</div>

										</div>
									</div>
								</td>
							</tr>
						</table>
					</s:div>


				</s:form></td>
		</tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle"><s:form action="commantUpdater"
					id="formid" method="post" theme="css_xhtml" class="FlowupLabels">
					<s:div cssClass="indent">
						<table width="100%" border="0" cellspacing="0" cellpadding="7"
							class="formboxRR">
							<tr>
								<td align="left" valign="top">&nbsp;</td>
							</tr>
							<tr>
								<td align="left" valign="top">
									<div class="addfildn">
										<div class="rkb">
											<div class="addfildn">
												<span
													style="font-size: 15px; font-weight: normal; color: #1ba6af;">Comment</span>
												<div class=""
													style="border: 1px solid #a9a9a9; width: 208%; overflow-y: scroll; height: 314px;">
													<s:iterator value="commentList">
														<table
															style="border: 1px solid #a9a9a9; width: 48%; padding: 5px; background-color: white; margin-top: 5px; margin-left: 5px; margin-bottom: 5px;">
															<tr>
																<td><span style="margin-left: 1%; color: #47a447;"><s:property
																			value="commentSenderEmailId" /></span></td>
															</tr>
															<tr>
																<td>
																	<div style="word-break: break-all;">
																		<span style="margin-left: 1%; color: Black;"><s:property
																				value="commentBody" /></span>
																	</div>
																</td>

															</tr>
															<tr>
																<td><span
																	style="margin-left: 76%; font-size: 10px;"><s:property
																			value="createDate" /></span></td>
															</tr>
														</table>
													</s:iterator>
												</div>
											</div>

											<div class="addfildn" style="width: 208%;">
												<div class="fl_wrap">
													<label class='fl_label'>Type a message</label>
													<s:textfield type="text" name="comments" id="comments"
														class="fl_input" />
												</div>
											</div>

											<div class="addfildn">
												<input type="button" id="btnSave" name="btnSave"
													class="btn btn-success btn-md" value="Submit"
													onclick="viewComments()"
													style="display: inline; margin-left: 199%;">
											</div>
										</div>
									</div>
								</td>
							</tr>

						</table>
					</s:div>
				</s:form></td>
		</tr>
	</table>

	<script>
		$(document).on(
				'change',
				'.btn-file :file',
				function() {
					var input = $(this), numFiles = input.get(0).files ? input
							.get(0).files.length : 1, label = input.val()
							.replace(/\\/g, '/').replace(/.*\//, '');
					input.trigger('fileselect', [ numFiles, label ]);
				});

		$(document)
				.ready(
						function() {
							$('.btn-file :file')
									.on(
											'fileselect',
											function(event, numFiles, label) {

												var input = $(this).parents(
														'.input-group').find(
														':text'), log = numFiles > 1 ? numFiles
														+ ' files selected'
														: label;

												if (input.length) {
													input.val(log);
												} else {
													if (log)
														alert(log);
												}

											});
						});
	</script>
	<script src="../js/main.js"></script>

</body>
</html>