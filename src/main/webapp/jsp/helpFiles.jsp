<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Help Center</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="taskhelp">
                <tr>
                <td height="30" align="left" valign="middle" class="boxheading">Files</td>
              </tr>
                <tr>
                <td align="left" valign="top" class="tdPad"><div class="basic" id="list1b"> <a>Can I upload Files without creating a task?</a>
                  <div class="basicdiv">
                    <ol>
                        <li>Now we have only option to upload files while creating or replying on a task.</li>
                    </ol>
                    </div>
                    <a>How can I see all Files of my projects?</a>
                  <div class="basicdiv">
                    <ol>
                        <li>Click "<strong>Files</strong>" present on the left panel, Here, all uploaded and shared files of the current project are listed</li>
                    </ol>
                    </div>
                    <a>Can I archive or delete a file of a task?</a>
                  <div class="basicdiv">
                    <ol>
                        <li>Yes, Click on the archive icon on left side of each file to archive a file of task.</li>
<li>Yes, You can delete a file of task from the archive section.</li>
                    </ol>
                    </div>
                    <a>Can I download all Files in a zip?</a>
                  <div class="basicdiv">
                    <ol>
                        <li>You can download them one by one.</li>
                    </ol>
                    </div>
              
                  </div></td>
              </tr>
              </table>

</body>
</html>