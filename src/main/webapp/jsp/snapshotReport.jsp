<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Transaction Snapshot</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/dataTables.buttons.js" type="text/javascript"></script>
<script src="../js/pdfmake.js" type="text/javascript"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script type="text/javascript">
	function handleChange() {
		reloadTable();
	}
	$(document).ready(function() {
		$(function() {
			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});

		});
	
		$(function() {
			var today = new Date();

			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));

			renderTable();
		});
	});
	function reloadTable() {
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#snapshotReportDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}

	function renderTable() {
		var merchantEmailId = document.getElementById("merchant").value;
		var table = new $.fn.dataTable.Api('#snapshotReportDataTable');
		//to show new loader -Harpreet
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				toggleAjaxLoader();
			},
			complete : function() {
				toggleAjaxLoader();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#snapshotReportDataTable')
				.dataTable(
						{
							"footerCallback" : function(row, data, start, end,
									display) {
								var api = this.api(), data;

								// Remove the formatting to get integer data for summation
								var intVal = function(i) {
									return typeof i === 'string' ? i.replace(
											/[\,]/g, '') * 1
											: typeof i === 'number' ? i : 0;
								};
								// Total over all pages total Transaction 
								total = api.column(3).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(3, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(3).footer()).html(
										'' + pageTotal + ' ' + ' ');
								
								// Total over all pages total  Success Transaction 
								total = api.column(4).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(4, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(4).footer()).html(
										'' + pageTotal + ' ' + ' ');
								
								// Total over all pages total failed Transaction 
								total = api.column(5).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(5, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(5).footer()).html(
										'' + pageTotal + ' ' + ' ');
								
								// Total over all pages total Dropped Transaction 
								total = api.column(6).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(6, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(6).footer()).html(
										'' + pageTotal + ' ' + ' ');
								
								// Total over all pages total bounced Transaction 
								total = api.column(7).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(7, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(7).footer()).html(
										'' + pageTotal + ' ' + ' ');
								
								// Total over all pages Total Transaction Amount
								total = api.column(8).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(8, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(8).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');
							},
							"columnDefs" : [ {
								className : "dt-body-center",
								"targets" : [0,1,2],
								className : "dt-body-right",
								"targets" : [3,4,5,6,7,8]
							} ],
							dom : 'BTftlpi',
							buttons : [ {
								extend : 'copyHtml5',
								//footer : true,
								exportOptions : {
									columns : [ ':visible' ]
								}
							}, {
								extend : 'csvHtml5',
								//footer : true,
								title : 'Transaction Statistics',
								exportOptions : {
									columns : [ ':visible' ]
								}
							}, {
								extend : 'pdfHtml5',
								//footer : true,
								title : 'Transaction Statistics',
								exportOptions : {
									columns : [ ':visible' ]
								}
							}, {
								extend : 'print',
								//footer : true,
								title : 'Transaction Statistics',
								exportOptions : {
									columns : [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
								}
							}, {
								extend : 'colvis',
								columns : [ 1, 2, 3, 4, 5, 6, 7, 8 ]
							} ],
							"ajax" : {
								"url" : "snapshotReportAction",
								"type" : "POST",
								"data" : generatePostData
							},
							"searching": false,
							"processing" : true,
							"lengthMenu" : [ [ 10, 25, 50, -1 ],
									[ 10, 25, 50, "All" ] ],
							"order" : [ [ 0, "desc" ] ],
							"columns" : [ {
								"data" : "date",
								"width" : "13%"
							}, {
								"data" : "paymentMethod",
								"width" : "10%"
							}, {
								"data" : "currency",
								"width" : "10%"
							}, {
								"data" : "totalTransactions",
								"width" : "10%"
							}, {
								"data" : "successTransactions",
								"width" : "10%"
							}, {
								"data" : "failedTransactions",
								"width" : "10%"
							}, {
								"data" : "totalDropped",
								"width" : "10%"
							}, {
								"data" : "totalBaunced",
								"width" : "10%"
							}, {
								"data" : "approvedAmount",
								"width" : "10%",
								"sType" : "numeric",
								fnRender : function(oDt) {
									return RenderDecimalNumber(oDt, {
										"decimalPlaces" : 2,
										"thousandSeparator" : ",",
										"decimalSeparator" : "."
									});
								}
							} ]
						});
		
		$("#merchant").select2({
			//data: payId
			});
	}

	function generatePostData() {
		var token = document.getElementsByName("token")[0].value;
		var merchantEmailId = document.getElementById("merchant").value;
		if (merchantEmailId == '') {
			merchantEmailId = 'ALL'
		}
		var obj = {
			acquirer : document.getElementById("acquirer").value,
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			paymentType : document.getElementById("paymentMethods").value,
			merchantEmailId : merchantEmailId,
			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}
</script>
<style>
.dataTables_wrapper {
	position: relative;
	clear: both;
	*zoom: 1;
	zoom: 1;
	margin-top: -10px;
}
</style>
</head>
<body>
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0" class="txnf">
		<tr>
			<td colspan="5" align="left"><h2>All Transaction</h2>
				<div class="container">


					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER_TYPE.name()=='SUPERADMIN' || #session.USER_TYPE.name()=='SUBADMIN'}">
						<div class="form-group col-md-2 col-xs-6 col-sm-4 txtnew">

							<div class="txtnew">
								<label for="acquirer">Merchant:</label><br />
								<s:select name="merchants" class="form-control" id="merchant"
									headerKey="" headerValue="ALL" list="merchantList"
									listKey="emailId" listValue="businessName" onchange="handleChange();" autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-3 col-xs-6 col-sm-4 txtnew">


							<div class="txtnew">
								<label for="acquirer">Merchant:</label><br />
								<s:select name="merchants" class="form-control" headerKey=""
									headerValue="ALL" id="merchant" list="merchantList"
									listKey="emailId" listValue="businessName" onchange="handleChange();" autocomplete="off" />
							</div>

						</div>
					</s:else>

					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'  || #session.USER_TYPE.name()=='SUPERADMIN'}">
						<div class="form-group col-md-3 col-sm-3 col-xs-6 txtnew">
							<div class="txtnew">
								<label for="acquirer">Account:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="account" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" onchange="handleChange();" autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-3  col-xs-6 txtnew"
							style="display: none;">


							<div class="txtnew">
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="account" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" onchange="handleChange();" autocomplete="off" />
							</div>

						</div>
					</s:else>


					<div class="form-group  col-md-3 col-sm-5 txtnew  col-xs-6">
						<label for="email">Payment Method:</label> <br />
						<s:select headerKey="ALL" headerValue="All" class="form-control"
							list="@com.kbn.commons.util.PaymentType@values()"
							name="paymentMethods" id="paymentMethods" onchange="handleChange();" autocomplete="off"
							value="code" listKey="code" listValue="name" />
					</div>

					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" class="form-control" readonly="true"
							id="dateFrom" name="dateFrom" onchange="handleChange();" 
							autocomplete="off" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" onchange="handleChange();" autocomplete="off" />
					</div>
				</div></td>
		</tr>
		<tr>
			<td colspan="5" align="left" valign="top">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" valign="top" style="padding: 10px;"><div
					class="scrollD">
					<table id="snapshotReportDataTable" class="display" cellspacing="0"
						width="100%">
						<thead>
							<tr class="boxheadingsmall">
								<th style='text-align: center'>Date</th>
								<th style='text-align: center'>Payment Method</th>
								<th style='text-align: center'>Currency</th>
								<th style='text-align: right'>Total Txns</th>
								<th style='text-align: right'>Successful</th>
								<th style='text-align: right'>Failed</th>
								<th style='text-align: right'>Dropped</th>
								<th style='text-align: right'>Bounced</th>
								<th style='text-align: right'>Approved Amount</th>
							</tr>
						</thead>
						<tfoot>
							<tr class="boxheadingsmall">
								<th style='text-align: left'>Transaction Count</th>
								<th style='text-align:right;'></th>
								<th style='text-align:right;'></th>
								<th style='text-align:right;  padding-right: 5px;'></th>
								<th style='text-align:right;  padding-right: 5px;'></th>
								<th style='text-align:right;  padding-right: 5px;'></th>
								<th style='text-align:right;  padding-right: 5px;'></th>
								<th style='text-align:right;  padding-right: 5px;'></th>
								<th style='text-align:right;  padding-right: 5px;'></th>
							</tr>
						</tfoot>
					</table>
				</div></td>
		</tr>
	</table>
</body>
</html>