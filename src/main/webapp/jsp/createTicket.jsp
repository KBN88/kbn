<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import="com.kbn.commons.user.User"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Ticket Details</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.minshowpop.js"></script>
<script src="../js/jquery.formshowpop.js"></script>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/custom.css" rel="stylesheet" type="text/css" />
<script src="../js/continents.js" type="text/javascript"></script>
<script src="../js/commanValidate.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/follw.js"></script>
<link href="http://fonts.googleapis.com/css?family=Crafty+Girls"
	rel="stylesheet" type="text/css" />
<script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
</script>


</head>

<body>

	<script>
		function saveTicket() {
		
 		var token = document.getElementsByName("token")[0].value;
			var subject = document.getElementById("subject").value;
			var emailId = document.getElementById("email").value;
			var mobileNo = document.getElementById("mobileNo").value;
			var ticketType = document.getElementById("tickettype").value;
			/* var ticketTypeDropDown = document.getElementById("tickettype").options.selectedIndex; */
		
			if((document.getElementById("merchant"))){
			var merchant = document.getElementById("merchant").value;
			var merchantDropDown = document.getElementById("merchant").options.selectedIndex;
			}
			var ticketbody = document.getElementById("messageBody").value;
		
			 if(subject==("")){
					
				 document.getElementById('subjectValid').innerHTML = "Please fill subject";
				 return false;
			} else{
				document.getElementById('subjectValid').innerHTML = "";
		     
			}
			 if(ticketbody==("")){
				 document.getElementById('ticketBodyValid').innerHTML = "Please fill ticket Body";
				 return false;
			}else{
				document.getElementById('ticketBodyValid').innerHTML = "";
		      
			}
			var payId = '<s:property value="#session.USER.payId" />'; 
			var ticketColumns = [ 'subject', 'email', 'mobileNo', 'tickettype',
					 'messageBody','merchant'];
			$
					.ajax({
						url : 'helpTicket',
						type : 'post',
						data : 
							
						 {
							payId : payId,
							subject : subject,
							ticketType : ticketType,
							contactEmail : emailId,
							contactMobile : mobileNo,
							merchant : merchant ,
							ticketbody : ticketbody,
							token : token
						} ,
						success : function(data) {
							var responseDiv = document
									.getElementById("response");
							var ticketId = data.ticketId;
							responseDiv.innerHTML = data.response+ticketId;
							responseDiv.style.display = "block";
							var responseData = data.response;
							
							if (responseData == null) {
								responseDiv.innerHTML = "Operation not successfull, please try again later!!"
								responseDiv.style.display = "block";
								responseDiv.className = "error error-new-text";
								event.preventDefault();
							}
							else if(responseData == "Ticket create successfully!!"){
							clearTicketFields(ticketColumns);
							
						
							responseDiv.className = "success success-text";
							
							$('#btnSave').hide();
							}
				},
						error : function(data) {

						}
					});
		}
	
		window.onload = function() {
			  document.getElementById("mobileNo").focus();
			  document.getElementById("email").focus();
			};
	

		function clearTicketFields(ticketColumns) {

			for (var i = 0; i <= ticketColumns.length; i++) {
				var element = document.getElementById(ticketColumns[i]);
				if (element != null) {

					element.value = '';

				} else {
					break;
				}

			}
		}
		function contactEmailAndMobile() {
			var merchant = document.getElementById("merchant").options;
			var merchantDropDown = document.getElementById("merchant").options.selectedIndex;
			var token = document.getElementsByName("token")[0].value;
			$.ajax({
				url : 'merchantDetails',
				type : 'post',
				data : {
					payId : document.getElementById("merchant").value,
					token : token,

				},
				success : function(data) {
					var emailId = data.email;
					var mobileNo = data.mobileNo;
					
					document.getElementById("mobileNo").value = mobileNo;
					document.getElementById("email").value= emailId;
					$('#mobileNo').focus();
					$('#email').focus();

				},
				error : function(data) {
					alert("Something went wrong, so please try again.");
				}

			});
		}

		
	</script>
	<div style="display: none" id="response"></div>
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td align="left" valign="middle"><s:form action="" method="post"
					autocomplete="off" class="FlowupLabels" theme="css_xhtml"
					validate="true">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" valign="top" bgcolor="#0271BB">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td align="left" width="68%" valign="middle"
											style="font: bold 14px verdana; color: #fff; padding: 4px 0 4px 8px;">Create
											Ticket</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</s:form></td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle"><s:form action="" id="formid"
					method="post" theme="css_xhtml" class="FlowupLabels">
					<s:div cssClass="indent">
						<table width="100%" border="0" cellspacing="0" cellpadding="7"
							class="formboxRR">
							<tr>
								<td align="left" valign="top">&nbsp;</td>
							</tr>
							<tr>
								<td align="left" valign="top">
									<div class="addfildn">
									
										<div class="rkb">
										  <span id="ticketBodyValid" style="color:#ff0000; font-size:11px;"></span>
											<div class="addfildn">
												Ticket Type
												<s:select headerKey="ALL" headerValue="ALL" id="tickettype"
													name="tickettype" class="textFL_merch"
													list="@com.mmadpay.ticketing.commons.util.TicketType@values()"
													listKey="ticketType" listValue="ticketType"
													autocomplete="off" style="height:46px;" />
											</div>
						
													<s:if test="%{#session.USER.UserType.name()=='MERCHANT'}">
											<div class="addfildn">
											 <span id="emailValid" style="color:#ff0000; font-size:11px;"></span>
												<div class="fl_wrap">
													<label class='fl_label'>Contact E-mail Id</label>
													<s:textfield type="text" name="contactEmailId" 
														id="email" class="fl_input" autocomplete="off" value="%{#session.USER.emailId}" onblur="isValidTicketEmail()" onfocus="focus()" ></s:textfield>
												</div>
											</div>
									</s:if>
									<s:else>
									<div class="addfildn">
									 <span id="emailValid" style="color:#ff0000; font-size:11px;"></span>
												<div class="fl_wrap">
													<label class='fl_label'>Contact E-mail Id</label>
													<s:textfield type="text" name="contactEmailId" value=""
														id="email" class="fl_input" autocomplete="off" onblur="isValidTicketEmail()" />
												</div>
											</div>
									</s:else>
											<div class="addfildn" style="width: 208%;">
											  <span id="subjectValid" style="color:#ff0000; font-size:11px;"></span>
												<div class="fl_wrap">
													<label class='fl_label'>Subject</label>
													<s:textfield type="text" name="subject" value=""
														id="subject" class="fl_input" 
														></s:textfield>
												</div>
											</div>
											<div class="rkb" style="width: 70%">
												<div class="">
												  
													<div class='fl_wrap'
														style="height: 106px; margin-left: -9%; margin-top: -3%; width: 313%;">
														<label class='fl_label'>Ticket Body</label>
														<s:textarea type="text" rows="5" class="fl_input"
															id="messageBody" name="messageBody" onkeyup=""
															autocomplete="off"
															style="height:95px; resize: none; outline-width:0"
															theme="simple" />
													</div>
												</div>
											</div>


										</div>
										<div class="rkb">
					 				<s:if test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'}">
		
											<div class="addfildn">
												Concerned User
												<s:select name="merchants" class="textFL_merch"
													id="merchant" headerKey="SELECT MERCHANTS"
													headerValue="SELECT MERCHANTS" listKey="payId"
													listValue="emailId" list="merchantList"
													autocomplete="off" onchange="contactEmailAndMobile();"
													style="height:46px;" />
											</div>
											</s:if>
											<s:if test="%{#session.USER.UserType.name()=='MERCHANT'}">
											
											<div class="addfildn">
											  <span id="phoneValid" style="color:#ff0000; font-size:11px;"></span>
												<div class="fl_wrap">
													<label class='fl_label'>Contact Mobile No.</label>
													<s:textfield type="text" name="contactMobileNo" value="%{#session.USER.mobile}"
														id="mobileNo" class="fl_input" autocomplete="off"  onblur="isValidTicketMobileNo()"/>
												</div>
											</div>
											
											
											</s:if>
											<s:else>
											<div class="addfildn">
											 <span id="phoneValid" style="color:#ff0000; font-size:11px;"></span>
												<div class="fl_wrap">
													<label class='fl_label'>Contact Mobile No.</label>
													<s:textfield type="text" name="contactMobileNo" value=""
														id="mobileNo" class="fl_input" autocomplete="off" onblur="isValidTicketMobileNo()"  />
												</div>
											</div></s:else>
											
											<div class="addfildn">
												<input type="button" id="btnSave" name="btnSave"
													class="btn btn-success btn-md" value="Submit"
													onclick="saveTicket()"
													style="display: inline; margin-top: 37%; width: 27%; margin-left: 73%; font-size: 15px;"></input>
											</div>

											
											<div class="clear"></div>

										</div>
									</div>
								</td>
							</tr>
						</table>


					</s:div>
				</s:form></td>
		</tr>
	</table>
	<script>
		$(document).on(
				'change',
				'.btn-file :file',
				function() {
					var input = $(this), numFiles = input.get(0).files ? input
							.get(0).files.length : 1, label = input.val()
							.replace(/\\/g, '/').replace(/.*\//, '');
					input.trigger('fileselect', [ numFiles, label ]);
				});

		$(document)
				.ready(
						function() {
							$('.btn-file :file')
									.on(
											'fileselect',
											function(event, numFiles, label) {

												var input = $(this).parents(
														'.input-group').find(
														':text'), log = numFiles > 1 ? numFiles
														+ ' files selected'
														: label;

												if (input.length) {
													input.val(log);
												} else {
													if (log)
														alert(log);
												}

											});
						});
	</script>
	<script src="../js/main.js"></script>
	<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
</body>
</html>