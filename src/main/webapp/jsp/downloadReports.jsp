<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html dir="ltr" lang="en-US">
<head>
<title>Download Reports</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" media="all"
	href="../css/daterangepicker-bs3.css" />
<link href="../css/loader.css" rel="stylesheet" type="text/css" />


<script type="text/javascript">
/* $(function() {
 	$("#submitButton").click(function(){
		$.ajax({
			  url: "streamReport",
			  context: document.body
			}).done(function(data) {
				alert(data);
			});
	});
}); */

	$(document).ready(
		function() {
			$( "#tabs" ).tabs();
			$(function() {
				$("#dateFrom").datepicker({
					prevText : "click for previous months",
					nextText : "click for next months",
					showOtherMonths : true,
					dateFormat : 'dd-mm-yy',
					selectOtherMonths : false,
					maxDate : new Date()
				});
				$("#dateTo").datepicker({
					prevText : "click for previous months",
					nextText : "click for next months",
					showOtherMonths : true,
					dateFormat : 'dd-mm-yy',
					selectOtherMonths : false,
					maxDate : new Date()
				});
			});
			$("#summarySubmitButton").click(function(){
				var postData = preparePostData(this);
				requestReport(postData, this.form)
				return false;
			});
			$("#tdrSubmitButton").click(function(){
				var postData = preparePostData(this);
				requestReport(postData, this.form)
				return false;
			});
			$("#settlementSubmitButton").click(function(){
				var postData = preparePostData(this);
				requestReport(postData, this.form)
				return false;
			});
		});

	function preparePostData(buttonEle){
		var formElements = $(buttonEle.form).find(':input');
		var postData = new Object();
		 for(i=0;i<formElements.length;i++){
			var element = formElements[i];
			var name = element.name;
			if(name!=""){
				postData[name] = element.value;
			}
		}
		return postData; 
	}

	function requestReport(postData, form){
		$.ajax({
			  url:form.action ,
			  type:"POST",
			  data:postData
			}).done(function(data) {
				//TODO.....show response and disable/hide buttons etc
				alert("success");
			});
	}
</script>

</head>
<body>
<div id="tabs">
  <ul>
    <li><a href="#summaryTab">Summary</a></li>
    <li><a href="#tdrTab">TDR</a></li>
    <li><a href="#settlementTab">Settlement</a></li>
  </ul>

<div id="#summaryTab">
	<s:form action="prepareSummaryReport">
	 <table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left"><s:actionmessage /></td>
		</tr>
		<tr>
			<td align="left"><h2>Summary Report</h2>


					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						<div class="form-group col-md-2 col-xs-6 col-sm-4 txtnew">

							<div class="txtnew">
								<label for="acquirer">Account:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-2 col-xs-6 txtnew"
							style="display: none;">


							<div class="txtnew">
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" autocomplete="off" />
							</div>

						</div>
					</s:else>
					<div class="form-group col-md-2 txtnew col-sm-4 col-xs-6">
						<label for="merchant">Merchant:</label> <br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' ||
							 #session.USER_TYPE.name()=='SUPERADMIN' || #session.USER_TYPE.name()=='ACQUIRER'}">
							<s:select name="merchantPayId" class="form-control" id="merchant"
								headerKey="" headerValue="ALL" list="merchantList"
								listKey="payId" listValue="businessName"
								autocomplete="off" />
						</s:if>
						<s:else>
							<s:select name="merchantPayId" class="form-control" id="merchant"
								list="merchantList" listKey="payId" headerKey=""
								headerValue="ALL" listValue="businessName"
								autocomplete="off" />
						</s:else>
					</div>


					<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
						<label for="email">Payment Method:</label> <br />
						<s:select headerKey="All" headerValue="All" class="form-control"
							list="@com.kbn.commons.util.PaymentType@values()"
							name="paymentMethods" id="paymentMethods"
							autocomplete="off" value="code"
							listKey="code" listValue="name" />
					</div>
					<div class="form-group  col-md-2 col-sm-4 txtnew col-xs-6">
						<label for="currency">Currency:</label> <br />
						<s:select name="currency" id="currency" headerValue="ALL"
							headerKey="ALL" list="currencyMap" class="form-control" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" readonly="true" id="dateFrom"
							name="dateFrom" class="form-control" autocomplete="off" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" autocomplete="off" />
					</div>
				</div></td>
		</tr>
		<tr>
			<td style="padding: 10px;"><div class="scrollD">
				</div></td>
		</tr>
	</table>
	<s:hidden name="reportType" value="SUMMARY_REPORT"></s:hidden>
	<input type="button" value="submit" id="summarySubmitButton"></input>
	</s:form>
</div>
<div id="tdrTab">
	<s:form action="prepareTDRReport">
	 <table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left"><s:actionmessage /></td>
		</tr>
		<tr>
			<td align="left"><h2>TDR report</h2>


					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						<div class="form-group col-md-2 col-xs-6 col-sm-4 txtnew">

							<div class="txtnew">
								<label for="acquirer">Account:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-2 col-xs-6 txtnew"
							style="display: none;">


							<div class="txtnew">
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" autocomplete="off" />
							</div>

						</div>
					</s:else>
					<div class="form-group col-md-2 txtnew col-sm-4 col-xs-6">
						<label for="merchant">Merchant:</label> <br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' ||
							 #session.USER_TYPE.name()=='SUPERADMIN' || #session.USER_TYPE.name()=='ACQUIRER'}">
							<s:select name="merchantPayId" class="form-control" id="merchant"
								headerKey="" headerValue="ALL" list="merchantList"
								listKey="payId" listValue="businessName"
								autocomplete="off" />
						</s:if>
						<s:else>
							<s:select name="merchantPayId" class="form-control" id="merchant"
								list="merchantList" listKey="payId" headerKey=""
								headerValue="ALL" listValue="businessName"
								autocomplete="off" />
						</s:else>
					</div>


					<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
						<label for="email">Payment Method:</label> <br />
						<s:select headerKey="All" headerValue="All" class="form-control"
							list="@com.kbn.commons.util.PaymentType@values()"
							name="paymentMethods" id="paymentMethods"
							autocomplete="off" value="code"
							listKey="code" listValue="name" />
					</div>
					<div class="form-group  col-md-2 col-sm-4 txtnew col-xs-6">
						<label for="currency">Currency:</label> <br />
						<s:select name="currency" id="currency" headerValue="ALL"
							headerKey="ALL" list="currencyMap" class="form-control" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" readonly="true" id="dateFrom"
							name="dateFrom" class="form-control" autocomplete="off" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" autocomplete="off" />
					</div>
				</div></td>
		</tr>
		<tr>
			<td style="padding: 10px;"><div class="scrollD">
				</div></td>
		</tr>
	</table>
	<s:hidden name="reportType" value="TDR_REPORT"></s:hidden>
	<input type="button" value="submit" id="tdrSubmitButton"></input>
	</s:form>
</div>
<div  id="settlementTab">
	<s:form action="prepareSettlementReport">

	 <table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left"><s:actionmessage /></td>
		</tr>
		<tr>
			<td align="left"><h2>Settlement Report</h2>

					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						<div class="form-group col-md-2 col-xs-6 col-sm-4 txtnew">

							<div class="txtnew">
								<label for="acquirer">Account:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-2 col-xs-6 txtnew"
							style="display: none;">


							<div class="txtnew">
								<s:select headerKey="ALL" headerValue="ALL" id="acquirer"
									name="acquirer" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" autocomplete="off" />
							</div>

						</div>
					</s:else>
					<div class="form-group col-md-2 txtnew col-sm-4 col-xs-6">
						<label for="merchant">Merchant:</label> <br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' ||
							 #session.USER_TYPE.name()=='SUPERADMIN' || #session.USER_TYPE.name()=='ACQUIRER'}">
							<s:select name="merchantPayId" class="form-control" id="merchant"
								headerKey="" headerValue="ALL" list="merchantList"
								listKey="payId" listValue="businessName"
								autocomplete="off" />
						</s:if>
						<s:else>
							<s:select name="merchantPayId" class="form-control" id="merchant"
								list="merchantList" listKey="payId" headerKey=""
								headerValue="ALL" listValue="businessName"
								autocomplete="off" />
						</s:else>
					</div>


					<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
						<label for="email">Payment Method:</label> <br />
						<s:select headerKey="All" headerValue="All" class="form-control"
							list="@com.kbn.commons.util.PaymentType@values()"
							name="paymentMethods" id="paymentMethods"
							autocomplete="off" value="code"
							listKey="code" listValue="name" />
					</div>
					<div class="form-group  col-md-2 col-sm-4 txtnew col-xs-6">
						<label for="currency">Currency:</label> <br />
						<s:select name="currency" id="currency" headerValue="ALL"
							headerKey="ALL" list="currencyMap" class="form-control" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" readonly="true" id="dateFrom"
							name="dateFrom" class="form-control" autocomplete="off" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" autocomplete="off" />
					</div>
				</div></td>
		</tr>
		<tr>
			<td style="padding: 10px;"><div class="scrollD">
				</div></td>
		</tr>
	</table>
	<s:hidden name="reportType" value="SETTLEMENT_REPORT"></s:hidden>
	<input type="button" value="submit" id="settlementSubmitButton"></input>
	</s:form>
</div>
</div>
</body>
</html>