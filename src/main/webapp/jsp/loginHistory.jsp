<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Login History</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/jquery.popupoverlay.js"></script> 
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>  
<script type="text/javascript" src="../js/pdfmake.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		handleChange();
	});
	function handleChange() {
		var token = document.getElementsByName("token")[0].value;
        var dtRender= $('#loginHistoryDataTable').DataTable({
			dom : 'BTftlpi',
			buttons : [ {
				extend : 'copyHtml5',
				exportOptions : {
					columns : [ ':visible' ]
				}
			}, {
				extend : 'csvHtml5',
				title : 'Login History',
				exportOptions : {
					columns : [ ':visible' ]
				}
			}, {
				extend : 'pdfHtml5',
				title : 'Login History',
				exportOptions : {
					columns : [ ':visible' ]
				}
			}, {
				extend : 'print',
				title : 'Login History',
				exportOptions : {
					columns : [ 0, 1, 2, 3, 4, 5, 6 ]
				}
			},{
				extend : 'colvis',
				//           collectionLayout: 'fixed two-column',
				columns : [ 1, 2, 3, 4, 5, 6]
			} ],
			"ajax" : {
				type : "POST",
				url : "loginHistoryAction",
				data : {
					emailId : document.getElementById("merchant").value,
					token:token,
				    "struts.token.name": "token",
				}
			},
			    "processing": false,
		        "serverSide": true,
		        "paginationType": "full_numbers", 
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		       // "order" : [ [ 7, "desc" ] ], 
			"columns" : [
			{
				"data" : "businessName",
				
			}, {
				"data" : "ip",
				"width" : '14%'
			}, {
				"data" : "browser",
				"width" : '13%'
			}, {
				"data" : "os",
				"width" : '10%'
			},{
				"data" : "status",
			
			}, {
				"data" : "timeStamp",
			}, {
				"data" : "failureReason"
			}]
		});
 popup=dtRender;
dtRender.on( 'xhr', function () {
var data = dtRender.ajax.json();
dataSet=JSON.stringify(data);
} ); 
	}
</script>
</head>
<body>
	<table width="100%" align="left" cellpadding="0" cellspacing="0" class="txnf">
		<tr>
			<td align="left"><h2>Login History</h2></td>
			<s:if test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
			<td width="15%" align="left" valign="middle"><div class="txtnew" style="padding:0 8px 0 0">
					<s:select name="merchants" class="form-control" id="merchant"
						headerKey="ALL MERCHANTS" headerValue="ALL MERCHANTS" listKey="emailId"
						listValue="businessName" list="merchantList" autocomplete="off" onchange="handleChange();" />
				</div></td>
				</s:if>
				<s:elseif test="%{#session.USER.UserType.name()=='MERCHANT'}">
				<td width="15%" align="left" valign="middle"><div class="txtnew" style="padding:0 8px 0 0">
					<s:select name="merchants" class="form-control" id="merchant"
						 listKey="emailId"
						listValue="businessName" list="merchantList" autocomplete="off" onchange="handleChange();" />
				</div></td>
				</s:elseif>
				<s:elseif test="%{#session.USER.UserType.name()=='SUBUSER'}">
				<td width="15%" align="left" valign="middle"><div class="txtnew" style="padding:0 8px 0 0">
					<s:select name="merchants" class="form-control" id="merchant"
						 listKey="emailId"
						listValue="businessName" list="merchantList" autocomplete="off" onchange="handleChange();" />
				</div></td>
				</s:elseif>
					<s:elseif test="%{#session.USER.UserType.name()=='RESELLER'}">
				<td width="15%" align="left" valign="middle"><div class="txtnew" style="padding:0 8px 0 0">
					<s:select name="merchants" class="form-control" id="merchant"
						 listKey="emailId"
						listValue="businessName" list="merchantList" autocomplete="off" onchange="handleChange();" />
				</div></td>
				</s:elseif>
									
		</tr>
		<tr>
			<td align="left" colspan="3" style="padding: 10px;"><br> <br>
            <div class="scrollD">
				<table id="loginHistoryDataTable" class="display table" cellspacing="0" width="100%">
					<thead>
						<tr class="boxheadingsmall">
							<th>Business Name</th>
							<th>IP address</th>
							<th>Browser</th>
							<th>OS</th>
							 <th>Status</th>
							<th>Date</th>
							<th>Failed Login Reason</th>
						</tr>
					</thead>
				</table>
                </div>
                </td>
		</tr>
	</table>
</body>
</html>