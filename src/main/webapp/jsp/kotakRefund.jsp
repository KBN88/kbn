<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html dir="ltr" lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Kotak Refund</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script type="text/javascript" src="../js/pdfmake.js"></script>
<link href="../css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="../css/daterangepicker-bs3.css" />
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<style>
.dataTables_wrapper {
	position: relative;
	clear: both;
	*zoom: 1;
	zoom: 1;
	margin-top: -30px;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$("#submit").click(function(env) {
			generateFile();
		});
	});
	
	$(function() {

		$("#dateFromText").datepicker({
			prevText : "click for previous months",
			nextText : "click for next months",
			showOtherMonths : true,
			dateFormat : 'dd-mm-yy',
			selectOtherMonths : false,
			maxDate : new Date()
		});
		$("#dateToText").datepicker({
			prevText : "click for previous months",
			nextText : "click for next months",
			showOtherMonths : true,
			dateFormat : 'dd-mm-yy',
			selectOtherMonths : false,
			maxDate : new Date()
		});

	});

	function generateFile() {

		var token = document.getElementsByName("token")[0].value;
		var dateFrom = document.getElementById('dateFromText').value;
		var dateTo = document.getElementById('dateToText').value;
		document.getElementById('dateFrom').value = dateFrom;
		document.getElementById('dateTo').value = dateTo;

		{
			var answer = confirm("Are you sure you want to download Refund Batch File");
			if (answer != true) {
				return false;
			}
			document.downloadrefundfile.submit();
		}
	}
</script>
</head>
<body>
	<div class="modal" style="display: none">
		<div class="center">
			<img alt="" src="../image/loader.gif" />
		</div>
	</div>
	<div id="popup"></div>
	<table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left"><s:actionmessage /></td>
		</tr>
		<tr>
			<td align="left"><h2>Kotak Bank Refund</h2></td>
		</tr>
		<tr>
			<td align="left"><div class="adduT">
					<div class="bkn">
						<h4>DOWNLOAD</h4>
						<div class="form-group col-xs-4 col-md-4 text-left">
							<label for="dateFrom">Date From:</label> <br />
							<s:textfield type="text" readonly="true" name="dateFrom" 
							 id="dateFromText" class="form-control" autocomplete="off"
								 />
						</div>
						<div class="form-group  col-xs-4 col-md-4 text-left">
							<label for="dateTo">Date To:</label> <br />
							<s:textfield type="text" readonly="true" name="dateTo"
							 id="dateToText" class="form-control" autocomplete="off" />
						</div>
						<div class="form-group col-xs-2  col-md-4 text-left">
							<br />
							<span id="submit" class="file-input btn btn-success btn-file btn-sm"><span class="glyphicon glyphicon-download-alt"></span> &nbsp;&nbsp;Download
							</span>
						</div>
						<div style="height: 47px;"></div>
					</div>
					<div class="bkn">
						<h4>UPLOAD</h4>
						<div class="form-group col-xs-2 col-md-12 text-center">
							<div class="input-group">
								<span class="input-group-btn"> <input type="text"
									class="inputfieldsmall" readonly
									style="height: 26px; margin-right: 10px;" /> &nbsp;&nbsp; <span
									class="file-input btn btn-success btn-file btn-sm"> <span
										class="glyphicon glyphicon-folder-open"> </span>
										&nbsp;&nbsp;Choose file <s:file name="UserLogo" />
								</span></span>
							</div>
						</div>
						<div style="height: 47px;"></div>
					</div>
					<div class="clear"></div>
				</div></td>
		</tr>
		<tr>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left">&nbsp;</td>
		</tr>
	</table>
	<script>
		$(document).on(
				'change',
				'.btn-file :file',
				function() {
					var input = $(this), numFiles = input.get(0).files ? input
							.get(0).files.length : 1, label = input.val()
							.replace(/\\/g, '/').replace(/.*\//, '');
					input.trigger('fileselect', [ numFiles, label ]);
				});

		$(document)
				.ready(
						function() {
							$('.btn-file :file')
									.on(
											'fileselect',
											function(event, numFiles, label) {

												var input = $(this).parents(
														'.input-group').find(
														':text'), log = numFiles > 1 ? numFiles
														+ ' files selected'
														: label;

												if (input.length) {
													input.val(log);
												} else {
													if (log)
														alert(log);
												}

											});
						});
	</script>
	<s:form name="downloadrefundfile" action="kotakRefundAction">
		<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
		<s:hidden name="dateFrom" value="" id="dateFrom"></s:hidden>
		<s:hidden name="dateTo" value="" id="dateTo"></s:hidden>
	</s:form>
</body>
</html>