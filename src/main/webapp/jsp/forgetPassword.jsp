<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; X-Content-Type-Options=nosniff; charset=utf-8" />
<title>Forgot Password</title>
<link rel="stylesheet" href="../css/loader/normalize.css"/>
<link rel="stylesheet" href="../css/loader/main.css"/>
<link rel="stylesheet" href="../css/loader/customLoader.css"/>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/showhidenemu.js"></script>
<script type="text/javascript" src="../js/top-hide.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/captcha.js"></script>
<!--  loader scripts -->
	<script src="../js/loader/modernizr-2.6.2.min.js"></script>
	<script src="../js/loader/main.js"></script>
<script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
</script>

<script>
    $(document).ready(function(){    	 
      $("#submit").click(function(e){    	 
          e.preventDefault();
          var emailId = document.getElementById("emailId").value;
          if(emailId==null || emailId==""){
        	  alert("Please enter Email Address");
        	  return false;
          }			
        //to show new loader --harpreet
 		 $.ajaxSetup({
 	            global: false,
 	            beforeSend: function () {
 	            	toggleAjaxLoader();
 	            },
 	            complete: function () {
 	            	toggleAjaxLoader();
 	            }
 	        });
          var token  = document.getElementsByName("token")[0].value;
          var button = document.getElementById("submit");
          if (ValidCaptcha()) {
          button.disabled = true;          
        $.ajax({type: "POST",
               url:"resetUserPassword",
                data: { emailId: emailId,
                	    token:token,
					    "struts.token.name": "token",
					},
                success:function(data){
                	var res = data.response;
                	alert(res);
                	if(data.errorMessage == null)
                		window.location.href = "index";
        }}); 
          }
      });
      
    });
    </script>
</head>
<body onload="return generateCaptcha();">

 		<div id="loader-wrapper"  style="width:100%;height:100%;">
			<div id="loader"></div>
		</div>
		
	<table width="100" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" valign="bottom" height="140"><img src="../image/logo-signup.png" /></td>
		</tr>
		<tr>
			<td><s:div class="signupbox">
					<table width="355" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" class="signup-headingbg" style="background: linear-gradient(60deg, #425185, #4a9b9b);"><h4 style="color: #FFF">Password Reset</h4></td>
						</tr>
						<tr>
							<td align="center"><table width="90%" border="0" cellspacing="0" cellpadding="0">

									<tr>
										<td align="left"><s:form id="resetPassword" autocomplete="off">
												<s:token />
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td height="10" align="left"></td>
													</tr>
													<tr>
														<td height="50" align="left"><s:textfield placeholder="Email Address" type="text"
																name="emailId" id="emailId" cssClass="signuptextfield" autocomplete="off" /></td>
													</tr>
													<tr>
												<td height="50" align="left">
													<div  class="rederror" id="error3"></div><table width="100%" cellpadding="0" cellspacing="0">
											<tr>
											  <td width="51%" align="left"><s:textfield name="captcha" type="text" cssClass="signuptextfieldsml"
													id="captcha" placeholder="Enter Captcha Code" autocomplete="off"   /></td>
											  <td width="6%" align="left">&nbsp;</td>
											<td width="32%" align="center"><s:textfield name="captchaCode" type="text" cssClass="accesscode"
													id="captchaCode"  autocomplete="off" oncopy="return false" /></td>
											<td width="11%" align="right"><s:textfield id="Btnaccescd" type="button" class="refreshbutton" value="" onclick="return generateCaptcha();" /></td></tr></table>
											
													
													
													</td>
													</tr>
													
													<tr>
														<td height="50" align="left" valign="bottom"><s:submit id="submit" value="Submit" style="background: linear-gradient(60deg, #425185, #4a9b9b); color:#fff;font-size:14px"
																cssClass="signupbutton btn-primary" onclick="return ValidCaptcha();"></s:submit></td>
													</tr>
													<tr>
														<td height="35" align="center" valign="bottom"><span class="signupboxtext">Already
																have an account ?</span> <s:a action="index">Login here</s:a></td>
													</tr>
												</table>
											</s:form></td>
									</tr>
									<tr>
										<td height="10" align="justify"></td>
									</tr>
								</table></td>
						</tr>
					</table>
				</s:div></td>
		</tr>
	</table>
</body>
</html>