<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Remittance Report</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script type="text/javascript" src="../js/pdfmake.js"></script>
<link href="../css/font-awesome.min.css" rel="stylesheet">
<script src="../js/messi.js"></script>
<link href="../css/messi.css" rel="stylesheet" />
<script src="../js/commanValidate.js"></script>
<script type="text/javascript" src="../js/remittanceReport.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script type="text/javascript">
var checkedRows = [];
function decodeVal(text) {
	return $('<div/>').html(text).text();
}	
function handleChange() {
	reloadTable();
}
$(document).ready(function() {

	$(function() {

		$("#dateFrom").datepicker({
			prevText : "click for previous months",
			nextText : "click for next months",
			showOtherMonths : true,
			dateFormat : 'dd-mm-yy',
			selectOtherMonths : false,
			maxDate : new Date()
		});

		$("#dateTo").datepicker({
			prevText : "click for previous months",
			nextText : "click for next months",
			showOtherMonths : true,
			dateFormat : 'dd-mm-yy',
			selectOtherMonths : false,
			maxDate : new Date()
		});

	});

	$(function() {
		var today = new Date();
		$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
		$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
		renderTable();
	});
	
	$("#processAllButton").click(function(env) {
		if(checkedRows.length==0){
			alert("Please select elements to proceed");
			return false;
		}
		processAll();
	});
	$("#selectAllCheckBox").click(function(env) {
		handleSelectALLClick(this);
	});

	$(function() {
		var datepick = $.datepicker;
		var table = $('#remittanceDataTable').DataTable();
		$('#remittanceDataTable tbody').on('click', 'td', function() {
			
			popup(table, this);
			process(table, this);
		});
	});
	
	$('#remittanceDataTable').on( 'page.dt', function (event) {
		var tableObj = event.currentTarget;
		uncheckAllCheckBoxes(tableObj);
	});
});

function renderTable() {
	var merchantEmailId = document.getElementById("merchant").value;
	var table = new $.fn.dataTable.Api('#remittanceDataTable');
	//to show new loader -Harpreet
	$.ajaxSetup({
		global : false,
		beforeSend : function() {
			toggleAjaxLoader();
		},
		complete : function() {
			toggleAjaxLoader();
		}
	});
	var transFrom = $.datepicker
			.parseDate('dd-mm-yy', $('#dateFrom').val());
	var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
	if (transFrom == null || transTo == null) {
		alert('Enter date value');
		return false;
	}

	if (transFrom > transTo) {
		alert('From date must be before the to date');
		$('#dateFrom').focus();
		return false;
	}
	if (transTo - transFrom > 31 * 86400000) {
		alert('No. of days can not be more than 31');
		$('#dateFrom').focus();
		return false;
	}
	var token = document.getElementsByName("token")[0].value;
	$('#remittanceDataTable')
			.dataTable(
					{
						"footerCallback" : function(row, data, start, end,
								display) {
							var api = this.api(), data;

							// Remove the formatting to get integer data for summation
							var intVal = function(i) {
								return typeof i === 'string' ? i.replace(
										/[\,]/g, '') * 1
										: typeof i === 'number' ? i : 0;
							};

							// Total over all pages
							total = api.column(8).data().reduce(
									function(a, b) {
										return intVal(a) + intVal(b);
									}, 0);

							// Total over this page
							pageTotal = api.column(8, {
								page : 'current'
							}).data().reduce(function(a, b) {
								return intVal(a) + intVal(b);
							}, 0);

							// Update footer
							$(api.column(8).footer()).html(
									'' + pageTotal.toFixed(2) + ' ' + ' ');
						},
						"columnDefs" : [ { className : "dt-body-right",	"targets" : [8]
						} ],
						dom : 'BTftlpi',
						buttons : [ {
							extend : 'copyHtml5',
							//footer : true,
							exportOptions : {
								columns : [ 1, 2, 3, 15, 5, 6, 7, 8, 9 ]
							}
						}, {
							extend : 'csvHtml5',
							//footer : true,
							title : 'Remittance View',
							exportOptions : {
								columns : [ 1, 2, 3, 15, 5, 6, 7, 8, 9]
							}
						}, {
							extend : 'pdfHtml5',
							orientation : 'landscape',
							//footer : true,
							title : 'Remittance View',
							exportOptions : {
								columns : [ 1, 2, 3, 4, 5, 6, 7, 8, 9]
							}
						}, {
							extend : 'print',
							//footer : true,
							title : 'Remittance View',
							exportOptions : {
								columns : [ 1, 2, 3, 4, 5, 6, 7, 8, 9]
							}
						}, {
							extend : 'colvis',
							//           collectionLayout: 'fixed two-column',
							columns : [1, 2, 3, 4, 5, 6, 7, 8, 9]
						} ],

						"ajax" : {
							"url" : "remittanceView",
							"type" : "POST",
							"data" : generatePostData
						},
						"processing" : true,
						"lengthMenu" : [ [ 10, 25, 50, -1 ],
								[ 10, 25, 50, "All" ] ],
						"order" : [ [ 2, "desc" ] ],
						"aoColumns" : [
						{
										"targets" : 0,
										"searchable" : false,
										"orderable" : false,
										"width" : '1%',
										"data":null,
										/*   'className': 'dt-body-center', */
									    'render' : function(data, type, row, meta) {
									    	var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
										if (userType == "ADMIN" || userType == "SUBADMIN") {
										   var status = row.status;
										   var checkbox = ''; 
									 		if (status == "PROCESSED") {
									 			checkbox = '<input type="checkbox" id='+row.utr+' disabled="true">';
									 			//implement some css also
											}else{
												checkbox = '<input type="checkbox" id='+row.utr+'>';
											}
									 		return checkbox;
										}else{
								  		   return "";
								  	   }
									}},{
							"mData" : "utr",
							"sClass" : "my_class",
							"sWidth" : '14%'
						}, {
							"mData" : "dateFrom",
							"sWidth" : '12%',
							"render" : function(mData) {
								var date = new Date(mData);
								var dd = date.getDate();
								
								if(dd<10) {dd='0'+dd}
								var month = date.getMonth() + 1;
								return (dd + "-" + (month.length > 1 ? month : "0" + month) + "-" + date.getFullYear());
							}
						}, {
							"mData" : "remittanceDate",
							"sWidth" : '12%',
							"render" : function(
									mData) {
								var date = new Date(mData);
								var dd = date.getDate();
								
								if(dd<10) {dd='0'+dd}
								var month = date.getMonth() + 1;
								return (dd + "-" + (month.length > 1 ? month : "0" + month) + "-" + date.getFullYear());
							}
						}, {
							"mData" : "payId",
							"sWidth" : '12%',
						}, {
							"mData" : "merchant",
							"sWidth" : '12%'
						}, {
							"mData" : "currency"
						}, {
							"mData" : "netAmount",
							"sWidth" : '11%'
						}, {
							"mData" : "remittedAmount",
							"sWidth" : '10%'
						}, {
							"mData" : "status"
						}, {
							"mData" : null,
							"sClass" : "center",
							"bSortable" : false,
							"mRender" : function(
									row) {
								var selectedServer = "<s:property value='%{#session.USER.UserType.name()}'/>";
								if (selectedServer == "ADMIN" || selectedServer == "SUBADMIN") {
									if (row.status == "INITIATED") {
										return '<button class="greenrefundbtn">Process</button>';

									} else {
										return '<button hidden="hidden" class="greenrefundbtn">Process</button>';
									}

								} else {
									return '<button hidden="hidden"   class="greenrefundbtn">Process</button>';
								}
							}
						},

						{
							"mData" : "accHolderName",
							"visible" : false,
							"sClass" : "displayNone"
						},
						{
							"mData" : "accountNo",
							"visible" : false,
							"sClass" : "displayNone"
						},
						{
							"mData" : "bankName",
							"visible" : false,
							"sClass" : "displayNone"
						},
						{
							"mData" : "ifscCode",
							"visible" : false,
							"sClass" : "displayNone"
						},
						{
							"mData" : null,
							"visible" : false,
							"sClass" : "displayNone",
							"mRender" : function(
									row) {
								return "\u0027"
										+ row.payId;
							}
						} ]
					});
	$("#merchant").select2({
		//data: payId
		});
}
function reloadTable(retainPageFlag) {
	$('#selectAllCheckBox').attr('checked', false);
	checkedRows = [];
	var datepick = $.datepicker;
	var transFrom = $.datepicker
			.parseDate('dd-mm-yy', $('#dateFrom').val());
	var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
	if (transFrom == null || transTo == null) {
		alert('Enter date value');
		return false;
	}

	if (transFrom > transTo) {
		alert('From date must be before the to date');
		$('#dateFrom').focus();
		return false;
	}
	if (transTo - transFrom > 31 * 86400000) {
		alert('No. of days can not be more than 31');
		$('#dateFrom').focus();
		return false;
	}

	var tableObj = $('#remittanceDataTable');
	var table = tableObj.DataTable();
	if (retainPageFlag) {
		//set page dummy callback function
		table.ajax.reload(setPage(), false);
	} else {
		table.ajax.reload();
	}

}
//dummy callback
function setPage() {
}
function process(table, index) {
	var token = document.getElementsByName("token")[0].value;
	var columnNumber = table.cell(index).index().column;

	if (columnNumber == 10) {
		var rowIndex = table.cell(index).index().row;
		var row = table.row(rowIndex).data();

		var payId = table.cell(rowIndex,4).data();
		var merchant = table.cell(rowIndex,5).data();
		var datefrom = table.cell(rowIndex,2).data();
		var netAmount = table.cell(rowIndex,7).data();
		var remittedDate = table.cell(rowIndex,3).data();
		var remittedAmount = table.cell(rowIndex,8).data();
		var utr = table.cell(rowIndex,1).data();
		var currency = table.cell(rowIndex,6).data();
		var status = table.cell(rowIndex,9).data();

		var accHolderName = table.cell(rowIndex,11).data();
		var accountNo = table.cell(rowIndex,12).data();
		var bankName = table.cell(rowIndex,13).data();
		var ifscCode = table.cell(rowIndex,14).data();

		$.ajax({
			url : 'remittanceProcess',
			type : "POST",
			data : {
				utr : utr,
				token : token,
				payId : payId,
				merchant : merchant,
				datefrom : datefrom,
				netAmount : netAmount,
				remittedDate : remittedDate,
				remittedAmount : remittedAmount,
				status : status,
				accHolderName : accHolderName,
				accountNo : accountNo,
				"struts.token.name" : "token",
			},

			success : function(
					data) {
				var res = data.response;
				alert(res);
			//	window.location.reload();
				reloadTable(true);
			},
			error : function(
					data) {
				alert("Not processed successfully please try again later");
				//window.location.reload();
			}
		});
	}
}

function decodeDiv() {
	var divArray = document.getElementsByTagName('div');
	for (var i = 0; i < divArray.length; ++i) {
		var div = divArray[i];
		if (div.id.indexOf('param-') > -1) {
			var val = div.innerHTML;
			div.innerHTML = decodeVal(val);
		}
	}
}

function decodeVal(value) {
	var txt = document.createElement("textarea");
	txt.innerHTML = value;
	return txt.value;
}

function generatePostData() {
	var token = document.getElementsByName("token")[0].value;
	var merchantEmailId = document.getElementById("merchant").value;
	if (merchantEmailId == '') {
		merchantEmailId = 'ALL'
	}
	var obj = {
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			status : document.getElementById("status").value,
			merchantEmailId : merchantEmailId,
		token : token,
		"struts.token.name" : "token",
	};

	return obj;
}

function popup(table, index) {
	var rows = table.rows();
	var datepick = $.datepicker;
	var columnVisible = table.cell(index).index().columnVisible;
	if (columnVisible == 1) {
		var token = document.getElementsByName("token")[0].value;
		var rowIndex = table.cell(index).index().row;
		var payId = table.cell(rowIndex,4).data();
		var merchant = table.cell(rowIndex,5).data();
		var datefrom = table.cell(rowIndex,2).data();
		var netAmount = table.cell(rowIndex,7).data();
		var remittedDate = table.cell(rowIndex,3).data();
		var remittedAmount = table.cell(rowIndex,8).data();
		var utr = table.cell(rowIndex,1).data();
		var currency = table.cell(rowIndex,6).data();
		var status = table.cell(rowIndex,9).data();

		var accHolderName = table.cell(rowIndex,11).data();
		var accountNo = table.cell(rowIndex,12).data();
		var bankName = table.cell(rowIndex,13).data();
		var ifscCode = table.cell(rowIndex,14).data();

		if (columnVisible == 1) { //If the user clicks on transaction id column, only then dispay pop-up

			var message = '<link rel="stylesheet" type="text/css" href="../css/popup.css"></link><div class="scrollDR"><div style="width:900px"><table width="650" border="0" align="left" cellpadding="0" cellspacing="0"><tr><td align="left" valign="middle" bgcolor="#c9e9ff" class="boxheadingsmall"  style=" font:bold 13px verdana; color:#fff; padding:5px 0 5px 13px;">STATUS - '
					+ status
					+ '</td>'
					+ '</tr><tr><td align="center" valign="top" height="10"></td></tr>'
					+ '<tr valign="top"><td align="center"><table width="100%" bgcolor="#dedede" cellspacing="0" cellpadding="0" class="product-spec"><tr><td width="32%" align="right" valign="middle" bgcolor="#FFFFFF"><strong>PayId:</strong></td>'
					+ '<td width="68%" bgcolor="#FFFFFF">'
					+ payId
					+ '</td></tr><tr><td width="32%" align="right" valign="middle" bgcolor="#FFFFFF"><strong>Bank Name:</strong></td>'
					+ '<td width="68%" bgcolor="#FFFFFF">'
					+ bankName
					+ '</td></tr><tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>Account Holder Name:</strong></td><td bgcolor="#FFFFFF"><span class="bluetext">'
					+ accHolderName
					+ '</span></td>'
					+ '</tr><tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>Account Number:</strong></td><td bgcolor="#FFFFFF">'
					+ accountNo
					+ '</td>'
					+ '</tr><tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>IFSC Code:</strong></td><td bgcolor="#FFFFFF">'
					+ ifscCode
					+ '</td></tr><tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>Currency:</strong></td>'
					+ '<td bgcolor="#FFFFFF">'
					+ currency

					+ '</td></tr><tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>Transaction Date:</strong></td>'
					+ '<td bgcolor="#FFFFFF">'
					+ datefrom
					+ '</td></tr><tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>Remittable Amount:</strong></td>'
					+ '<td bgcolor="#FFFFFF">'
					+ netAmount
					+ '</td></tr><tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>Remitted Amount:</strong></td><td bgcolor="#FFFFFF">'
					+ remittedAmount
					+ '</td>'
					+ '</tr><tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>Remittance Date:</strong></td><td bgcolor="#FFFFFF">'
					+ remittedDate
					+ '</td>'
					+ '</tr>'

					+ '<tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>Merchant:</strong></td><td bgcolor="#FFFFFF">'
					+ merchant
					+ '</td>'
					+ '</tr>'
					+ '<tr><td align="right" valign="middle" bgcolor="#FFFFFF"><strong>UTR:</strong></td><td bgcolor="#FFFFFF">'
					+ utr
					+ '</td>'
					+ '</tr>'
					+ '<tr>'
					+ '  <td height="45" colspan="2" align="center" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>'
					+ '  </tr>'

					+ '</table></div></div></td></tr>';
			new Messi(
					message,
					{
						center : true,
						buttons : [ {
							id : 0,
							label : 'Close',
							val : 'X'
						} ],
						callback : function(
								val) {
							$(
									'.messi,.messi-modal')
									.remove();
						},
						//width : '700px',
						modal : true
					});
		}
	}
}
</script>
</head>
<body>
	<div id="popup"></div>
	<table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left"><h2>View Remittance</h2>
				<div class="container">


					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						<div class="form-group col-md-3 col-sm-6 col-xs-6 txtnew">
							<div class="txtnew">
								<label for="acquirer">Merchant:</label><br />
								<s:select name="merchant" class="form-control" id="merchant"
									headerKey="" headerValue="ALL" list="merchantList"
									listKey="emailId" listValue="businessName" onchange="handleChange();" autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-3 col-sm-6 col-xs-6 txtnew">


							<div class="txtnew">
								<label for="acquirer">Merchant:</label><br />
								<s:select name="merchant" class="form-control" id="merchant"
									list="merchantList" listKey="emailId" listValue="businessName"
									onchange="handleChange();" autocomplete="off" />
							</div>

						</div>
					</s:else>


					<div class="form-group  col-md-3 col-sm-6 txtnew  col-xs-6">
						<label for="email">Status:</label> <br />
						<s:select headerKey="ALL" headerValue="ALL" id="status"
							name="status" class="form-control"
							list="@com.kbn.commons.user.RemittanceStatusType@values()"
							value="code" listKey="code" listValue="name" onchange="handleChange();" autocomplete="off" />
					</div>
					<div class="form-group  col-md-3 col-sm-5 txtnew  col-xs-6">
						<label for="email">Date From:</label> <br />
						<s:textfield type="text" readonly="true" id="dateFrom"
							name="dateFrom" class="form-control" autocomplete="off"
							onchange="handleChange();" />
					</div>

					<div class="form-group  col-md-3 col-sm-5 txtnew col-xs-6">
						<label for="dateFrom">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" onchange="handleChange();" autocomplete="off" />
					</div>
				</div></td>
		</tr>
		<tr>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" style="padding: 10px;"><div class="scrollD">
					<table id="remittanceDataTable" class="display" cellspacing="0"
						width="100%">
						<thead>
							<tr class="boxheadingsmall">
							<s:if test ="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'}">
											<th style='text-align: left'><input type="checkbox" name="selectAllCheckBox" id="selectAllCheckBox"></input></th>
										</s:if>
										<s:else>
										<th style='text-align: center'></th>
										</s:else>
								<th>UTR No</th>
								<th>Transaction Date</th>
								<th>Remitted Date</th>
								<th>PayId</th>
								<th>Merchant</th>
								<th>Currency</th>
								<th>Remittable Amount</th>
								<th>Remitted Amount</th>
								<th>Status</th>
								<s:if test ="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'}">
												<th style='text-align: right'><input type="button" id="processAllButton" value="Process All" class="greenrefundbtn" style="width:70px"></input></th>
											</s:if>
											<s:else>
												<th style='text-align: center'></th>
											</s:else>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th>PayId</th>
							</tr>
						</thead>
					</table>
				</div></td>
		</tr>
	</table>
</body>
</html>