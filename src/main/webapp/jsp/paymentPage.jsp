<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.owasp.esapi.ESAPI"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="antlr.StringUtils"%>
<%@page import="com.kbn.pg.core.Amount"%>
<%@page import="com.kbn.pg.core.Currency"%>
<%@page import="com.kbn.commons.util.FieldType"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title><s:property value="%{#session.pageTittle}" /></title>
<script type="text/javascript" src="../js/paymentPage.js"></script>
<link href="../css/credit-card-form.css" media="all" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../js/credit-card-form.js"></script>
<style>
.textstyle, h1, h2, h3, input {	font-family: <s:property value = "%{#session.TEXT_STYLE}" /> !important; }
.textcolor, h1, h2, h3, ul.nav-tabs>li>a, p, small { color: #<s:property value="%{#session.TEXT_COLOR}"/> !important; }
.background {background-color: #<s:property value="%{#session.BACKGROUND_COLOR}"/>;}
a:link {color: #<s:property value="%{#session.HYPERLINK_COLOR}"/> !important;}
.boxbackgroundcolor {background-color: #<s:property value="%{#session.BOX_BACKGROUND_COLOR}"/> !important;}
.topbarcolor {background-color: #<s:property value="%{#session.TOP_BAR_COLOR}"/> !important;}
ul.nav-tabs>li>a {color: #<s:property value="%{#session.TAB_TEXT_COLOR}"/> !important; background-color: #<s:property value="%{#session.TAB_BACKGROUND_COLOR}"/>; }
ul.nav-tabs>li>a:hover {color: #<s:property value="%{#session.TAB_TEXT_COLOR}"/> !important; background-color: #<s:property value="%{#session.TAB_BACKGROUND_COLOR}"/>; }
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus {color: #<s:property value="%{#session.ACTIVE_TAB_TEXT_COLOR}"/> !important;	background-color: #<s:property value="%{#session.ACTIVE_TAB_COLOR}"/>;}
.buttoncolor {background-color: #<s:property value="%{#session.BUTTON_BACKGROUND_COLOR}"/> !important; border: none; color: #<s:property value="%{#session.BUTTON_TEXT_COLOR}"/> !important; }
.bordercolor, ul, .panel-heading {border-color: #<s:property value="%{#session.BORDER_COLOR}"/> !important; }
.text-danger { color:#ff0000 !important; }
</style>
<script>
$(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
</script>

<script type="text/javascript">
        var timeout = '<%=15 * 60 * 1000%>';
	
	var timer = setInterval(function() {
		timeout -= 1000;
		window.status = time(timeout);
		if (timeout == 0) {
			clearInterval(timer);
			alert('Your session has been expired !')
		}
	}, 1000);

	function two(x) {
		return ((x > 9) ? "" : "0") + x
	}

	function time(ms) {
		var t = '';
		var sec = Math.floor(ms / 1000);
		ms = ms % 1000;

		var min = Math.floor(sec / 60);
		sec = sec % 60;
		t = two(sec);

		var hr = Math.floor(min / 60);
		min = min % 60;
		t = two(min) + ":" + t;

		document.getElementById("lblSessionTime").innerText = "Your Session will expire in "
				+ t;
		return "You session will timeout in " + t + " minutes.";
	}
</script>
<style type="text/css">.error-text{color:#a94442;font-weight:bold;background-color:#f2dede;list-style-type:none;text-align:center;list-style-type: none;margin-top:10px;
}.error-text li { list-style-type:none; }</style>
</head>
<body class="background" onload="checkActiveDiv();recurringShowDiv();setTabName();getExForm();setInterval(1);" >
	<div class="center-div">
	<div class="error-text"><s:actionmessage/></div>
		<div class="container textstyle textcolor">
			<div class="text-right">
				<s:label name="lblSessionTime" id="lblSessionTime"></s:label>
				
			</div>

			<div class="panel panel-default bordercolor">

				<div class="panel-heading custom_class topbarcolor">
					<h1 class="panel-title pull-left" style="padding-top: 6px; color:#ffffff !important;">
						<%
							Logger logger = Logger.getLogger("Payment Page");
										String currencyCodeAlpha="";
										String formattedAmount="";
											try {
												String currencyCodeNumeric = (String) session
														.getAttribute(FieldType.CURRENCY_CODE.getName());
												String amount = (String) session.getAttribute(FieldType.AMOUNT
														.getName());

												currencyCodeAlpha = Currency
														.getAlphabaticCode(currencyCodeNumeric);
												formattedAmount = ESAPI.encoder().encodeForHTML(
														Amount.toDecimal(amount, currencyCodeNumeric));

											} catch (NullPointerException nPException) {
												response.sendRedirect("sessionTimeout");
											} catch (Exception exception){
												logger.error("Exception on payment page " + exception);
												response.sendRedirect("error");
											}
						%>
						<b><s:property value="getText('AmountPayable')" />  <%=currencyCodeAlpha%> <%=formattedAmount%></b>
					</h1>

					<div class="pull-right">
						
						<form action="locale" name="languageForm" id="languageForm" >

							<s:select list="@com.kbn.pg.core.LocaleLanguageType@values()" name="request_locale"
							value="defaultLanguage" listKey="code" listValue="name"
								class="form-control1 pull-right" onchange="this.form.submit()"></s:select>
								
						</form>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="panel-body boxbackgroundcolor" style="margin: 0px; padding: 6px;">

					<ul class="nav nav-tabs responsive" id="myTab"
						style="background-color: #ededed; border-radius: 4px;">
						
							  <s:if test="%{#session.EXPRESS_PAY_FLAG == true}">
				          <s:if test="%{#session.TOKEN == 'NA'}"></s:if>
						<s:else>
								<li class="active" id ="liExpresspay"><a href="#tabExpressPay" data-toggle="tab" style="color: !important;"><s:property value="getText('ExpressPay')" /></a></li>
								</s:else>
							</s:if>
							<s:set var="checkMoponlyDC" value="false" />
							<s:iterator value="#session.PAYMENT_TYPE_MOP">
							
							<s:if test="%{key=='CC'}">
								<s:set var="checkMop" value="true" />
							</s:if>
							<s:if test="%{key=='DC' && #checkMop == true}">
								<s:set var="checkMop" value="false" />
								<s:set var="checkMoponlyDC" value="true" />
							</s:if>
							<s:if  test="%{key=='DC' && #checkMoponlyDC == false}">
								<s:set var="checkMop" value="true" />
							</s:if>
							 
							<s:if test="#checkMop == true">
							 <s:if test="%{#session.EXPRESS_PAY_FLAG == true}">
							 <s:if test="%{#session.TOKEN == 'NA'}">
							  		 <li id="liCreditCard"  class="active"><a id ="hyperCard" href="#tabCreditCard" data-toggle="tab" >Card</a></s:if>
							  		  <s:else><li id="liCreditCard"><a  id ="hyperCard" href="#tabCreditCard" data-toggle="tab" >Card</a></s:else>
						 </s:if>
						 <s:else><li id="liCreditCard" class="active"><a id ="hyperCard"  href="#tabCreditCard" data-toggle="tab" >Card</a></s:else>
						<s:set var="checkMop" value="false" />
						<s:set var="checkMoponlyDC" value="true" />
							</s:if>
							
							<s:if test="%{key=='NB'}">
								<li id="liNetbanking" ><a href="#tabNetbanking" data-toggle="tab" style="color: !important;"><s:property value="getText('NetBanking')" /></a></li>
							</s:if>
							<s:if test="%{key=='WL'}">
								<li id="liWallet" ><a href="#tabWallet" data-toggle="tab" style="color: !important;"><s:property value="getText('Wallet')" /></a></li>
							</s:if>	
							<s:if test="%{key=='UP'}">
								<li id="liUPI" ><a href="#tabUPI" data-toggle="tab" style="color: !important;"><s:property value="getText('UPI')" /></a></li>
								<li id="liGPAYUPI" ><a href="#tabGPAYUPI" data-toggle="tab" style="color: !important;"><s:property value="getText('UPI')" /></a></li>
							</s:if>	
							
							<s:if test="%{key=='CC'}">
							<s:iterator value="value" status="itStatus">
							<s:if test="%{code == 'EZ'}"> 
							<li id="liEzeeclick" ><a href="#tabEzeeclick" data-toggle="tab" style="color: !important;"><s:property value="getText('EzeeClick')" /></a></li>	
							</s:if>
							</s:iterator>
							</s:if>											
						</s:iterator>
					</ul>

					<div class="tab-content active responsive">
				<s:if test="%{#session.EXPRESS_PAY_FLAG == true}">
						  <s:if test="%{#session.TOKEN == 'NA'}"></s:if>
							 <s:else> 
						<div id="tabExpressPay" class="tab-pane fade" >
       <h3><s:property value="getText('ExpressPay')" /></h3>
        <s:form autocomplete="off" name="exCard" method="post" action="pay" id="exCard" onsubmit="return submitEXform();">
		<input type="hidden" name="paymentType" value="EX" />
		<small id="cardSupportedEX" class="text-danger text-left"></small>
       <s:iterator value="#session.TOKEN" status="incrementer">       
       <div class="hoverDiv">
        <div class="row">
         <div class="mk-trc pull-left" data-style="radio" data-radius="true" style="margin: 7px 0px; display:inline-block;">
          <input type="radio" name="tokenId" id="tokenId<s:property value="key" />" onclick="checkExMop('<s:property value="%{value.mopType}"/>');handleClick(this);"  value="<s:property value="key" />" >
              
   <s:if test="%{value.mopType=='VI'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/visa_card.png" class="cardslogo"></label>
      </s:if>
      <s:if test="%{value.mopType=='AX'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/amex_card.png" class="cardslogo"></label>
      </s:if>
      <s:if test="%{value.mopType=='MC'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/master_card.png" class="cardslogo"></label>
      </s:if>
      <s:if test="%{value.mopType=='MS'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/mastero_card.png" class="cardslogo"></label>
      </s:if>
      <s:if test="%{value.mopType=='DN'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/diner_card.png" class="cardslogo"></label>
      </s:if>   
      <s:if test="%{value.mopType=='RU'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/rupay_card.png" class="cardslogo"></label>
      </s:if>       
   </div>
   <div class="col-md-7 col-xs-6" style="padding-right:10px; ">
   <small class="pull-right text-muted">
   <s:textfield  name="cardNumber" autocomplete="off" id="exCardNumber%{key}" cssClass="form-control transparent-input" value="%{value.cardMask}" theme="simple"  readonly="true" />
   <small class="text-muted"><s:if test="%{value.paymentType=='CC'}">Credit Card</s:if></small>
     <small class="text-muted"> <s:else>Debit Card</s:else> <s:if test="%{value.cardIssuerBank !=null}"  > | <s:property value="%{value.cardIssuerBank}" /> </s:if>     </small>
      </small> </div> 
         <div class="col-md-1 col-xs-10 col-md-push-1" style="margin-top:9px;padding-left:0px; ">
         <span class="pull-right">
         <s:textfield type="password"  name="cvvNumber" id="cvvNumber%{key}" maxlength="4" autocomplete="off" onkeypress="return isNumberKey(event);return disableEnterPress(event);" onkeyup="enableExButton(this);"  style="width: 40px; height: 25px; display: inline;" class="form-control" placeholder="CVV" disabled="true" />         
         </span></div>
         <div class="col-md-1 col-xs-1" style="margin-top:11px; padding-left:20px;"><button id="delbutton<s:property value="key" />"  type="button" class="glyphicon glyphicon-trash btn-link text-warning" disabled="disabled" data-toggle="modal" data-target="#myModal" style="color:#e9877c;"></button>
        
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="width:300px;  max-width:90%;">
      <div class="modal-content">
        <div class="modal-body" style="margin:0px; padding:0px;">
          <h3 class="text-center text-info" >Are you sure <i class="fa fa-question-circle fa-lg"></i></h3>

        </div>
        <div class="modal-footer" style="text-align:center;">
          <div class="row"><div class="col-md-6 col-xs-6"><s:submit name="deleteButton" action="deletecard" class="btn btn-danger btn-block text-center" id="deleteButton%{key}" theme="simple" value="Yes"></s:submit></div>
          <div class="col-md-6 col-xs-6"><s:submit type="button" class="btn btn-success btn-block text-center" data-dismiss="modal" theme="simple" value="Cancel"></s:submit></div>
        </div>
        </div>
      </div>
    </div>
  </div></div>
        </div>
       </div>
      </s:iterator>
          <div class="row">

               <div class="col-md-4 col-sm-12 col-xs-12">
               <s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
                <s:submit key="Pay" name="exSubmit" cssClass="btn-orange disabled"   formtarget="_parent" disabled="true" id="exSubmit"></s:submit>
                <s:submit key="ReturnToMerchant" action="txncancel" name="exCancelButton" formtarget="_parent" cssClass="btn-link" theme="simple" id="exCancelButton"></s:submit>
                </s:if>
                <s:else><s:submit key="Pay" name="exSubmit" cssClass="btn-orange disabled"  disabled="true"  id="exSubmit"></s:submit>
                <s:submit key="ReturnToMerchant" action="txncancel" name="exCancelButton" cssClass="btn-link" theme="simple" id="exCancelButton"></s:submit>                
               </s:else><br>
               </div>
              </div>
              </s:form>
      </div>
      </s:else></s:if>
						<s:iterator value="#session.PAYMENT_TYPE_MOP">
							<s:if test="%{key=='CC' || key=='DC'}">
								<div  id="tabCreditCard" class="tab-pane fade" >
									<s:form autocomplete="off" name="creditcard-form" method="post" action="pay" id="creditCard" onsubmit="return submitCCform();">
									<input id="mopType" name="mopType" type="hidden">
									<s:set name="valid maestro" value="getText('MaestroCardMsg')" />
										<div style="height:15px; display:block;">
										<s:hidden id="maestroNoteD" value="%{valid maestro}" />
										<h6 id="maestroNoteDspan" class="text-info"> </h6>
										</div>
									<div class="row">
											<div class="col-md-10">
												<s:iterator value="#session.CARD_PAYMENT_TYPE_MOP" >
												<s:iterator value="value" >
													<s:if test="%{code == 'VI'}">
															<img src="../image/visa_card.png" class="cardslogo" style="margin-right:10px;">
															<input id="checkmopTypeVI" type="hidden" value="VI">
													</s:if>
													<s:if test="%{code == 'MC'}">
															<img src="../image/master_card.png"	class="cardslogo" style="margin-right:10px;">
															<input id="checkmopTypeMC" type="hidden" value="MC">
													</s:if>
													<s:if test="%{code == 'AX'}">
															<img src="../image/amex_card.png" class="cardslogo" style="margin-right:10px;">
															<input id="checkmopTypeAX" type="hidden" value="AX">
													</s:if>
													<s:if test="%{code == 'DN'}">
															<img src="../image/diner_card.png" class="cardslogo" style="margin-right:10px;" />
															<input id="checkmopTypeDN" type="hidden" value="DN">
													</s:if>
													<s:if test="%{code == 'MS'}">
															<img src="../image/mastero_card.png" class="cardslogo" style="margin-right:10px;">
															<input id="checkmopTypeMS" type="hidden" value="MS">
													</s:if>
													<s:if test="%{code == 'RU'}">
															<img src="../image/rupay_card.png" class="cardslogo" style="margin-right:10px;">
															<input id="checkmopTypeRU" type="hidden" value="RU">
													</s:if>
													</s:iterator>
												</s:iterator>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="row" style="height:12px; display:block;">
										<div class="col-md-5 col-xs-6" id="divCardType">
												<s:set name="valid cardnumber" value="getText('EntervalidCardnumber')" />
												<s:hidden id="validCardDetail" value="%{valid cardnumber}" />
												<small id="demo" class="text-danger text-left" style="position:absolute; margin-top:-3px;"></small>
												
											</div>
											<div class="col-md-5 col-xs-6">									
											<s:set name="card typemsg" value="getText('CardSupported')" />
											<s:hidden id="cardTypeMsg" value="%{card typemsg}" />
											<small id="cardSupported" class="text-danger text-left" style="position:absolute; margin-top:-3px;"></small>
										</div>
										</div>										
										<div class="row">											
											<s:set name="cardNumberTextCC" value="getText('CardNumber')" />
											<s:hidden id="cardPlaceHolderCC" value="%{cardNumberTextCC}" />
											<div class="form-group col-md-10" id="divCardNumber">
												<div class="cardNumber__wrap">
												 <input type="text" id="cardNumber1" name="cardNumber" style="display: none" /> <input
														id="cardNumber" name="cardNumber" class="cardNumber form-control"
														onblur="checkLuhn(this);setMopType(this);checkCardSupported(); "
														onkeyup="enablePayButton(this);" autocomplete="off" />
													<div class="card" aria-hidden="true"></div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-8 col-xs-10" style="height:3px;">
												<s:set name="valid expiry" value="getText('Entervalidexpirydate')" />
												<s:hidden id="validExpiryDate" value="%{valid expiry}" />
												<small id="chkExpiry" class="text-danger" style="position:absolute; margin-top:-10px;"></small>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-3 col-sm-4 col-xs-6" id="divExpiryMonth">

												<s:select headerKey="" headerValue="%{getText('Month')}" cssClass="form-control"
													list="#{'01':'01','02':'02','03':'03','04':'04','05':'05','06':'06','07':'07','08':'08','09':'09','10':'10','11':'11','12':'12'}"
													name="ExpiryMonth" id="ccExpiryMonth" value="0"
													onchange="CheckExpiry();enablePayButton(this);" />
											</div>
											<div class="form-group col-md-3 col-sm-4 col-xs-6" id="divExpiryYear">

												<s:select headerKey="" headerValue="%{getText('YEAR')}" cssClass="form-control"
													list="#{'2020':'2020','2021':'2021','2022':'2022','2023':'2023','2024':'2024','2025':'2025','2026':'2026','2027':'2027','2028':'2028','2029':'2029','2030':'2030','2031':'2031','2032':'2032','2033':'2033','2034':'2034','2035':'2035','2036':'2036','2037':'2037','2038':'2038','2039':'2039','2040':'2040','2041':'2041','2042':'2042','2043':'2043','2044':'2044','2045':'2045','2046':'2046','2047':'2047','2048':'2048','2049':'2049'}"
													name="ExpiryYear" id="ccExpiryYear" value="0"
													onchange="CheckExpiry();enablePayButton(this);" />
											</div>
											<s:set name="CVV" value="getText('CVV')" />
											<div class="form-group col-md-2 col-sm-2 col-xs-6" id="divPassword">
												<input type='password' style='display: none' />
												<s:textfield type="password" name="cvvNumber" id="cvvNumber" autocomplete="off"
													onkeypress="return isNumberKey(event)" onkeyup="enablePayButton(this);"
													cssClass="form-control margintop0px mozilla-firefox" placeholder="CVV" />
											</div>
											<div class="form-group col-md-2 col-sm-2 col-xs-6" id="divCvv">
												<s:set name="cvv textimage" value="getText('CVVlen3image')" />
												<s:set name="cvv textimageAX" value="getText('CVVlen4image')" />
												<s:hidden id="cvvTextImage" value="%{cvv textimage}" />
												<s:hidden id="cvvTextImageAX" value="%{cvv textimageAX}" />
												<span onMouseOut="Hide()" onMouseOver="Show()" onMouseMove="Show()" class="cvvcard">&nbsp;</span>
												<div id="cvvtext"></div>
											</div>
										</div>
										<s:set name="NameonCard" value="getText('NameonCard')" />
										<div class="row" id="divName">
											<div class="form-group col-md-10">
												<s:textfield id="cardName" name="cardName" type="text" cssClass="form-control"
													autocomplete="off" onkeypress="noSpace(event,this);return isCharacterKey(event)"
													onkeyup="enablePayButton(this);" placeholder="%{NameonCard}" />
											</div>
										</div>
										<div class="row" id="divSaveCard">
											<s:if test="%{#session.EXPRESS_PAY_FLAG == true}">
												<div class="form-group col-md-1 col-xs-1">
													<s:checkbox name="cardsaveflag" checked="checked" id="cardsaveflag1" />
												</div>
												<div class="form-group col-md-11 col-xs-10 text-left" style="padding-left: 0px;">
													<label for="cardsaveflag1" class="text-muted"><s:property
															value="getText('SaveCard')" /></label>
												</div>
											</s:if>
										</div>
										<div id="whitetransbgsize"><div class="row">
											<div class="form-group col-md-4 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="Pay" name="ccSubmit" cssClass="btn-orange disabled"
														style="background-color:;" theme="simple" formtarget="_parent" id="ccSubmit"></s:submit>
												</s:if>
												<s:else>
													<s:submit key="Pay" name="ccSubmit" cssClass="btn-orange disabled"
														style="background-color:;" theme="simple" id="ccSubmit"></s:submit>
												</s:else>
											</div>
										</div></div>
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="ReturnToMerchant" formtarget="_parent" action="txncancel"
														name="ccCancelButton" theme="simple" cssClass="btn-link" id="ccCancelButton"></s:submit>
												</s:if>
												<s:else>
													<s:submit key="ReturnToMerchant" action="txncancel" name="ccCancelButton"
														theme="simple" cssClass="btn-link" id="ccCancelButton"></s:submit>
												</s:else>
											</div>
										</div>

									</s:form>
								</div>
							</s:if>
						<s:if test="%{key=='NB'}">
								<div class="tab-pane fade" id="tabNetbanking">
									<h3><s:property value="getText('NetBanking')" /></h3>
									<s:form autocomplete="off" name="creditcard-form" method="post" action="pay" id="card"
										onsubmit="return submitNBform();">
										<input type="hidden" name="paymentType" value="NB" />
									
										<div class="clearfix">&nbsp;</div>
										<div class="netbankingdivradio"><s:iterator value="value" status="itStatus"> 												
											<s:if test="%{code in {'1030'}  }"> 
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" id="sbi"
														onclick="onChange('sbi');enableNetbankingButton()" type="radio" value="SBI"> <label
														for="sbi"><i></i> <img src="../image/sbi-logo.png" class=""></label>
												</div>
												</s:if>
													<s:if test="%{code in {'1004'}  }"> 
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" id="hdfc"
														onclick="onChange('hdfc');enableNetbankingButton()" type="radio" value="HDFC">
													<label for="hdfc"><i></i> <img src="../image/hdfc_new.png" class=""></label>
												</div>
												</s:if>
													<s:if test="%{code in '1013'}"> 
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" id="icici"
														onclick="onChange('icici');enableNetbankingButton()" type="radio" value="ICICI">
													<label for="icici"><i></i> <img src="../image/icici_new.png" class=""></label>
												</div>
												</s:if>
											<s:if test="%{code in '1005'}"> 
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" type="radio" value="AXIS" id="axis"
														onclick="onChange('axis');enableNetbankingButton()"> <label for="axis"><i></i>
														<img src="../image/axis_new.png" class=""></label>
												</div>
												</s:if>
													<s:if test="%{code in '1010'}"> 
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" type="radio" value="CITI" id="citi"
														onclick="onChange('citi');enableNetbankingButton()"> <label for="citi"><i></i>
														<img src="../image/citibank-netbnk.png" class=""></label>
												</div>
												</s:if>
													<s:if test="%{code in '1012'}"> 
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input id="kotak" onclick="onChange('kotak');enableNetbankingButton()"
														name="netBankingType" type="radio" value="KOTAK"> <label for="kotak"><i></i>
														<img src="../image/kotak_new.png" class=""></label>
												</div>
												</s:if>										 
										</s:iterator> </div>
										<hr />
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">

												<s:select class="form-control" list="value" name="bankName" listKey="code"
													onclick="onChangeNetbankDropdwn(this.value);enableNetbankingButton()" listValue="name"
													id="ddlNetbanking" data-size="5" headerKey="-1" headerValue="Select Bank" />
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="Pay"  name="nbSubmit"
														cssClass="btn-orange disabled" style="background-color:;" theme="simple"
														formtarget="_parent" id="nbSubmit"></s:submit>
												</s:if>
												<s:else>
													<s:submit key="Pay" name="nbSubmit"
														cssClass="btn-orange disabled" style="background-color:;" theme="simple" id="nbSubmit"></s:submit>
												</s:else>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="ReturnToMerchant" formtarget="_parent" action="txncancel"
														name="netCancelButton" cssClass="btn-link" theme="simple" id="netCancelButton"></s:submit>
												</s:if>
												<s:else>
													<s:submit key="ReturnToMerchant" action="txncancel"
														name="netCancelButton" theme="simple" cssClass="btn-link" id="netCancelButton"></s:submit>
												</s:else>
											</div>
										</div>
									</s:form>
								</div>
							</s:if>
							
							<!-- Wallet  -->
									<s:if test="%{key=='WL'}">
								   <div class="tab-pane fade" id="tabWallet">
									<h3><s:property value="getText('Wallet')" /></h3>
									<s:form autocomplete="off" name="creditcard-form" method="post" action="pay" id="card"
										onsubmit="return submitWLform();">
										<input type="hidden" name="paymentType" value="WL" />
									
										<div class="clearfix">&nbsp;</div>
										<div class="netbankingdivradio"><s:iterator value="value" status="itStatus"> 												
											<s:if test="%{code in {'102'}  }"> 
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="walletType" id="mobikwikWallet"
														onclick="onChangeWallet('mobikwikWallet');enableWalletButton()" type="radio" value="MOBIKWIK"> <label
														for="mobikwikWallet"><i></i> <img
															src="../image/mobikwik.png" class="cardslogo"
															style="margin-right: 10px; width: 60px;"></label>
												</div>
												</s:if>
												
												<s:if test="%{code in {'101'}  }"> 
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="walletType" id="paytmWallet"
														onclick="onChangeWallet('paytmWallet');enableWalletButton()" type="radio" value="PAYTM"> <label
														for="paytmWallet"><i></i> <img
															src="../image/paytm-logo.png" class="cardslogo"
															style="margin-right: 10px; width: 60px;"></label>
												</div>
												</s:if>
												
																						
												<s:if test="%{code in {'119'}  }"> 
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="walletType" id="epayLaterWallet"
														onclick="onChangeWallet('epayLaterWallet');enableWalletButton()" type="radio" value="EPAYLATER"> <label
														for="epayLaterWallet"><i></i> <img
															src="../image/epaylater.svg" class="cardslogo"
															style="margin-right: 10px; width: 60px;"></label>
												</div>
												</s:if>
												
												
												
																		 
										</s:iterator> </div>
										<hr />
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">

												<s:select class="form-control" list="value" name="walletName" listKey="code"
													onclick="onChangeWalletDropdown(this.value);enableWalletButton()" listValue="name"
													id="ddlWallet" data-size="5" headerKey="-1" headerValue="Select Bank" />
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="Pay"  name="wlSubmit"
														cssClass="btn-orange disabled" style="background-color:;" theme="simple"
														formtarget="_parent" id="wlSubmit"></s:submit>
												</s:if>
												<s:else>
													<s:submit key="Pay" name="wlSubmit"
														cssClass="btn-orange disabled" style="background-color:;" theme="simple" id="wlSubmit"></s:submit>
												</s:else>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="ReturnToMerchant" formtarget="_parent" action="txncancel"
														name="wtCancelButton" cssClass="btn-link" theme="simple" id="wtCancelButton"></s:submit>
												</s:if>
												<s:else>
													<s:submit key="ReturnToMerchant" action="txncancel"
														name="wtCancelButton" theme="simple" cssClass="btn-link" id="wtCancelButton"></s:submit>
												</s:else>
											</div>
										</div>
									</s:form>
								</div>
							</s:if>
					
							
							<s:if test="%{key=='CC'}">
							<s:iterator value="value" status="itStatus">
							<s:if test="%{code == 'EZ'}"> 
							<div class="tab-pane fade" id="tabEzeeclick" >
									<h3><s:property value="getText('EzeeClick')" /></h3>
									<s:form autocomplete="off" name="ezee-form" method="post" action="pay" id="ezee-form">
									<input type="hidden" name="paymentType" value="CC" />
									<input id="mopType" name="mopType" type="hidden" value="EZ">
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="col-md-12 col-xs-12">												
												<img
														src="../image/ezeclick_logo.png" width="150" />									
											</div>
										</div>
										<hr />
										<div class="row">
											<div class="form-group col-md-7 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit type="submit" name="button" id="EzeClickPay" key="Pay" 
														class="btn-orange" formtarget="_parent"></s:submit>
												</s:if>
												<s:else>
													<s:submit type="submit" name="button" id="EzeClickPay" key="Pay"
														class="btn-orange "></s:submit>
												</s:else>

											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="ReturnToMerchant" action="txncancel" formtarget="_parent"
														name="ezCancelButton" cssClass="btn-link" id="ezCancelButton" theme="simple">
													</s:submit>
												</s:if>
												<s:else>
													<s:submit key="ReturnToMerchant" action="txncancel" name="ezCancelButton"
														cssClass="btn-link" id="ezCancelButton" theme="simple">
													</s:submit>
												</s:else>
											</div>
										</div>
									</s:form>
								</div>
						</s:if></s:iterator></s:if>
						
										<!-- UPI  -->
							<s:if test="%{key=='UP'}">
								<div class="tab-pane fade" id="tabUPI">
									<h3>
										<s:property value="getText('UPI')" />
									</h3>

									<s:form autocomplete="off" name="upi-form" method="post"
										action="" id="upi-form" onsubmit="return submitUPform();">
										<input type="hidden" name="paymentType" value="UP" />
										<div class="clearfix">&nbsp;</div>
										<div class="row" style="margin-bottom: 20px;">
											<div class="col-md-3 col-sm-3" style="text-align: center;">
												<img src="../image/bhim.svg" alt="Google Pay"
													style="max-width: 20%;">
												<p>BHIM PAY</p>
											</div>

											<div class="col-md-3 col-sm-3" style="text-align: center;">
												<img src="../image/googlepay.svg" alt="Google Pay"
													style="max-width: 20%;">
												<p class="">Google Pay</p>
											</div>

											<div class="col-md-3 col-sm-3" style="text-align: center;">
												<img src="../image/paytm.svg" alt="Google Pay"
													style="max-width: 20%;">
												<p class="">PayTm Pay</p>
											</div>
											<div class="col-md-3 col-sm-3" style="text-align: center;">
												<img src="../image/phonepe.svg" alt="Google Pay"
													style="max-width: 20%;">
												<p class="">PhonePay</p>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-12 col-xs-12">
												<div>
													<label>Virtual Payment Address</label>
													<div class="resultDiv">
														<p class="red1" id="red1">Invalid VPA</p>
														<p class="red1" id="enterVpa">Please Enter VPA</p>
													</div>
													<s:set name="TextUP" value="getText('UPI')" />
													<s:hidden id="HolderUP" value="%{TextUP}" />

													<input id="vpaCheck" name="upi" class="upi form-control"
														onkeyup="isValidVpa(); enableButton();"
														onkeydown=" return restrictKeyVpa(event,this);"
														onfocusout="isValidVpaOnFocusOut();" oncopy="return false"
														onpaste="return false" ondrop="return false" />
												</div>
											</div>
										</div>
										<div class="CTA mob-btn" id="pay-now">

											<input type="button"
												class="payment-btn form-group col-md-12 col-sm-12 col-xs-12"
												id="upi-sbmt" value="Pay" onclick="submitUpiForm()">
											<div>

												<small id="errorBox" class="text-danger"></small>
											</div>
										</div>
										<p class="returnMerchand">
											<a href="#" onclick="myCancelAction();"
												key="ReturnToMerchant" id="upCancelButton"
												name="upCancelButton" theme="simple"
												ondragstart="return false;" ondrop="return false;">Return
												to merchant</a>
										</p>
								<%-- 		<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<%
													if (branding != null && branding.isPaymentPageHeaderLogoFlag()) {
												%>
												<div class="col-md-12 col-sm-12 col-xs-12">
													<img src="data:image/png;base64,<%=paymentPageImage%>"
														class="img-responsive res-branding"
														style="max-height: 55px; margin-top: -25px; float: right;">
												</div>
												<%
													} else {
												%>
												<div class="col-md-12 col-sm-12 col-xs-12">
													<img src="../image/ssl-logos.png"
														class="img-responsive bhartipay-branding">
												</div>
												<%
													}
												%>

											</div>
										</div> --%>
										<s:hidden name="token" value="%{#session.customToken}" />
									</s:form>
								</div>

								<!-- GPAY UPI  -->
								<div class="tab-pane fade" id="tabGPAYUPI">
									<h3>
										<s:property value="getText('UPI')" />
									</h3>


									<form action="upiGpayPay" target="_parent" method="get"
										id="googlePayForm">

										<div class="googlePayDiv">
											<input type="hidden" name="paymentType" value="UP" /> <span
												class="placeHolderText placeHolderTextgooglePay">Enter
												Phone Number</span> <input id="googlePayNum" name="googlePay"
												class="inputField form-control"
												onkeydown="return numOnly(event,this);"
												onkeyup="googlePayNumCheck(this);"
												onblur="checkPhoneNo(this)" onpaste="return false"
												maxlength="10" />


										</div>
										<div class="resultDiv">
											<p class="red1" id="googlePayInvalidNo">Invalid Phone
												Number</p>
											<p class="red1" id="googlePayEnterPhone">Please Enter
												Phone Number</p>
										</div>

										<input type="hidden" id="googlePayPhoneNo" name="vpaPhone"
											value=""> <input type="hidden" id="googlePayMobtype"
											name="mopType" value="GP"> <input type="hidden"
											id="googlePayPaymentType" name="paymentType" value="UP">


										<div class="googlePayDiv">
											<input type="button" id="googlePayBtn" value="pay now"
												class="payment-btn form-group col-md-12 col-sm-12 col-xs-12"
												onclick="submitGooglePayForm()">
										</div>

										<div class="googlePayReturnMerchant">
											<a href="#" onclick="myCancelAction();"
												key="ReturnToMerchant" id="ccCancelButton"
												class="float-save" name="ccCancelButton" theme="simple"
												ondragstart="return false;" ondrop="return false;">Return
												to merchant</a>
										</div>
								<%-- 		<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<%
													if (branding != null && branding.isPaymentPageHeaderLogoFlag()) {
												%>
												<div class="col-md-12 col-sm-12 col-xs-12">
													<img src="data:image/png;base64,<%=paymentPageImage%>"
														class="img-responsive res-branding"
														style="max-height: 55px; margin-top: -25px; float: right;">
												</div>
												<%
													} else {
												%>
												<div class="col-md-12 col-sm-12 col-xs-12">
													<img src="../image/ssl-logos.png"
														class="img-responsive bhartipay-branding">
												</div>
												<%
													}
												%>

											</div>
										</div> --%>
										<s:hidden name="token" value="%{#session.customToken}" />
									</form>

								</div>
								<!-- GAPY END -->
								<div class="tab-pane fade" id="tabQR">
									<s:form autocomplete="off" name="upi-form" method="post"
										action="" id="upi-form" onsubmit="return submitUPform();">
										<input type="hidden" name="paymentType" value="UP" />
										<input type="hidden" name="mopType" value="UP" />
										<input type="hidden" name="amount" value="amount" />
										<img id="qrcodeDiv" />

									</s:form>
								</div>


							</s:if>
							<!-- End UPI -->
					
						</s:iterator>
					</div>


					<ul class="nav pull-right formobiles">
						<li class="text-center">
						<%-- <img src="../image/userlogo/<%=(String) session.getAttribute(FieldType.PAY_ID.getName())%>.png" width="180px" /> --%>
							<table class="table table-bordered" style="background-color: #fbfbfb; font-size: 11px;">
								<tr>
									<td width="40%" align="left" valign="middle"><s:property value="getText('Buyer')" />
									</td>
									<td width="60%" align="left" valign="middle"><%=ESAPI.encoder().encodeForHTML((String) session.getAttribute(FieldType.CUST_NAME
					.getName()))%></td>
								</tr>
								<tr>
									<td align="left" valign="middle"><s:property value="getText('ORDERID')" /></td>
									<td align="left" valign="middle"><%=ESAPI.encoder().encodeForHTML((String) session.getAttribute(FieldType.ORDER_ID.getName()))%></td>
								</tr>

							</table></li>
					</ul>
				</div>
				<div class="pull-right" style="margin-top: -60px;">
				<!-- 	<img src="../image/ssl-logos.png" class="img-responsive"> -->
				</div>
			</div>
		</div>
	</div>	

	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/responsive-tabs.js"></script>
	<script type="text/javascript">
		$('#myTab a').click(function(e) {
			e.preventDefault();
			$(this).tab('show');
		});
		$('#moreTabs a').click(function(e) {
			e.preventDefault();
			$(this).tab('show');
		});
		(function($) { // Test for making sure event are maintained
			$('.js-alert-test').click(function() {
				alert('Button Clicked: Event was maintained');
			});
			fakewaffle.responsiveTabs([ 'xs', 'sm' ]);
		})(jQuery);
	</script>
	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
	<script>
$('.hoverDiv').click(function() {
    $(this).addClass('active').siblings().removeClass('active');
})
</script>

<script type="text/javascript">
function checkActiveDiv(){
	var checkEX= "<s:property value="%{#session.EXPRESS_PAY_FLAG}"/>"
	var checkEXactive= "<s:property value="%{#session.TOKEN}"/>"
	if (checkEX == "true"){
		if (checkEXactive == 'NA'){
			document.getElementById('tabCreditCard').className += " in active";
	}
		else{
			document.getElementById('tabExpressPay').className += " in active";
			}
	}
	else{
			document.getElementById('tabCreditCard').className += " in active";
	}
}

 function recurringShowDiv(){
	var checkRecurring = "<s:property value="%{#session.IS_RECURRING}"/>"
	if (checkRecurring == "true"){
		document.getElementById('tabCreditCard').className += " in active";
		if (document.getElementById("tabExpressPay") != null) {
			document.getElementById('liExpresspay').style.display = "none";
			document.getElementById('tabExpressPay').style.display = "none";
		}
		if (document.getElementById("tabNetbanking") != null) {
			document.getElementById('liNetbanking').style.display = "none";
			document.getElementById('tabNetbanking').style.display = "none";
		}
		if (document.getElementById("tabWallet") != null) {
			document.getElementById('liWallet').style.display = "none";
			document.getElementById('tabWallet').style.display = "none";
		}
		if (document.getElementById("tabUPI") != null) {
			document.getElementById('liUPI').style.display = "none";
			document.getElementById('tabUPI').style.display = "none";
		}
		if (document.getElementById("tabEzeeclick") != null) {
			document.getElementById('liEzeeclick').style.display = "none";
			document.getElementById('tabEzeeclick').style.display = "none";
		}
		if (document.getElementById("tabEMI") != null) {
			document.getElementById('liEMI').style.display = "none";
			document.getElementById('tabEMI').style.display = "none";
		}
		
	}
	
 }
 function setTabName(){
	 var checkPaymentType = "<s:property value="#session.PAYMENT_TYPE_MOP"/>"
	  if ((checkPaymentType.indexOf("CC") > 0) && (checkPaymentType.indexOf("DC") > 0) ){
		 document.getElementById('hyperCard').innerHTML ="<s:property value="getText('CreditDebitCard')" />"
	 }
	 else if ((checkPaymentType.indexOf("CC") < 0) && (checkPaymentType.indexOf("DC") > 0) ){
		 document.getElementById('hyperCard').innerHTML ="<s:property value="getText('DebitCard')" />"
	 }
	 else{
		 document.getElementById('hyperCard').innerHTML ="<s:property value="getText('CreditCard')" />"
	 }
	 
 }
function checkExMop(checkedValue){
	var checkExMopType = "<s:property  value="#session.CARD_PAYMENT_TYPE_MOP" />"
	if (checkedValue == "VI" && (checkExMopType.indexOf("VISA") < 0 )){
		document.getElementById('cardSupportedEX').innerHTML = "This card is no longer supported";
	}
	else if (checkedValue == "MC" && (checkExMopType.indexOf("MASTER") < 0 )){
		document.getElementById('cardSupportedEX').innerHTML = "This card is no longer supported";
		}
	else if (checkedValue == "MS" && (checkExMopType.indexOf("MAESTRO") < 0 )){
		document.getElementById('cardSupportedEX').innerHTML = "This card is no longer supported";
		}
	else if (checkedValue == "AX" && (checkExMopType.indexOf("AMEX") < 0 )){
		document.getElementById('cardSupportedEX').innerHTML = "This card is no longer supported";
		}
	else if (checkedValue == "DN" && (checkExMopType.indexOf("DINERS") < 0 )){
		document.getElementById('cardSupportedEX').innerHTML = "This card is no longer supported";
		}
	else if (checkedValue == "RU" && (checkExMopType.indexOf("RUPAY") < 0 )){
		document.getElementById('cardSupportedEX').innerHTML = "This card is no longer supported";
		}
	else{
		document.getElementById('cardSupportedEX').innerHTML = "";
	}
	
}
</script>

</body>
</html>