<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Refund Report</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery.dataTables.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script src="../js/jquery.popupoverlay.js" type="text/javascript"></script>
<script src="../js/dataTables.buttons.js" type="text/javascript"></script>
<script src="../js/pdfmake.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/refundReport.js"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script type="text/javascript">
	var checkedRows = [];
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	function handleChange() {
		reloadTable();
	}
	$(document).ready(function() {

		$(function() {
			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});

		$(function() {
			var today = new Date();
			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			renderTable();
		});

		$("#processAllButton").click(function(env) {
			if(checkedRows.length==0){
				alert("Please select elements to proceed");
				return false;
			}
			processAll();
		});
		$("#selectAllCheckBox").click(function(env) {
			handleSelectALLClick(this);
		});
		
		$(function() {
			var table = $('#refundReportDataTable').DataTable();
			 $('#refundReportDataTable tbody').on('click', 'td', function() {
				var columnIndex = table.cell(this).index().column;
				var rowIndex = table.cell(this).index().row;
				var rowNodes = table.row(rowIndex).node();
				var rowData = table.row(rowIndex).data();
				if(columnIndex==0){
					handleSingleCheckBoxClick(rowNodes, rowData);
				}
				refundDetailsFunc(table, this);
			});

			$('#refundReportDataTable tbody').on('click', 'button[type="button"]', function() {
				var columnIndex = table.cell(this.parentNode).index().column;
				var rowIndex = table.cell(this.parentNode).index().row;
				var rowNodes = table.row(rowIndex).node();
				var rowData = table.row(rowIndex).data();
				if(columnIndex == 11){
					process(table, this.parentNode);
				}
			});

			$('#refundReportDataTable').on( 'page.dt', function (event) {
				var tableObj = event.currentTarget;
				uncheckAllCheckBoxes(tableObj);
			});
		});
	});

	function renderTable() {
		var merchantEmailId = document.getElementById("merchants").value;
		var table = new $.fn.dataTable.Api('#refundReportDataTable');
		//to show new loader -Harpreet
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				toggleAjaxLoader();
			},
			complete : function() {
				toggleAjaxLoader();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#refundReportDataTable')
				.dataTable(
						{
							"footerCallback" : function(row, data, start, end,
									display) {
								var api = this.api(), data;

								// Remove the formatting to get integer data for summation
								var intVal = function(i) {
									return typeof i === 'string' ? i.replace(
											/[\,]/g, '') * 1
											: typeof i === 'number' ? i : 0;
								};

								// Total over all pages
								total = api.column(8).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(8, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(8).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								// Total refuand amount
								total = api.column(9).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(9, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(9).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								// Total refuand Availaibale amount
								total = api.column(10).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(10, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(10).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

							},
							"columnDefs" : [ { className : "dt-body-right",	"targets" : [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
							} ],
							dom : 'BTftlpi',
							buttons : [
									{
										extend : 'copyHtml5',
										//footer : true,
										exportOptions : {
											columns : [ 13, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
										}
									},
									{
										extend : 'csvHtml5',
										//footer : true,
										title : 'Refund Report',
										exportOptions : {
											columns : [ 13, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 15 ]
										}
									},
									{
										extend : 'pdfHtml5',
										orientation : 'landscape',
										//footer : true,
										title : 'Refund Report',
										exportOptions : {
											columns : [ ':visible' ]
										}
									},
									{
										extend : 'print',
										//footer : true,
										title : 'Refund Report',
										exportOptions : {
											columns : [13, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
										}
									}, {
										extend : 'colvis',
										//           collectionLayout: 'fixed two-column',
										columns : [ 2, 3, 6, 7, 9 ]
									} ],
							"ajax" : {
								"url" : "refundReportAction",
								"type" : "POST",
								"data" : function (d){
									return generatePostData(d);
								}
							},
							 "searching": false,
							 "processing": true,
						        "serverSide": true,
						        "paginationType": "full_numbers", 
						        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
						        //"order" : [ [ 1, "desc" ] ],
						        "order": [],
						        "columnDefs": [
						            {
						            "type": "html-num-fmt", "targets": 4,
						            "orderable": false, "targets": [0,1,2,3,4,5,6,7,8,9,10]
						            }
						        ],
							"aoColumns" : [ {
								"targets" : 0,
								"searchable" : false,
								"orderable" : false,
								"width" : '1%',
								"data":null,
								/*   'className': 'dt-body-center', */
							    'render' : function(data, type, row, meta) {
							    	var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
								if (userType == "ADMIN" || userType == "SUBADMIN") {
								   var settlement = row.settlementStatus;
								   var checkbox = ''; 
							 		if (settlement == "PROCESSED") {
							 			checkbox = '<input type="checkbox" id='+row.transactionId+' disabled="true">';
							 			//implement some css also
									}else{
										checkbox = '<input type="checkbox" id='+row.transactionId+'>';
									}
							 		return checkbox;
								}else{
						  		   return "";
						  	   }
							}},{
								"data" : "transactionId",
								"className" : "my_class"
							}, {
								"data" : "txnDate"
							}, {
								"data" : "orderId"
								
							}, {
								"data" : "refundDate"
							}, {
								"data" : "businessName"
							}, {
								"data" : "paymentMethod"
							}, {
								"data" : "currencyName"
							}, {
								"data" : "approvedAmount"
							}, {
								"data" : "refundedAmount"
							}, {
								"data" : "refundableAmount"
							}, {
								"data" : null,
								"orderable" : false,
								"render" : function(row) {
									var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
									var settlement = row.settlementStatus;
									if (userType == "ADMIN" || userType == "SUBADMIN") {
										if (settlement == "PROCESSED") {
											return '<button class="grayrefundbtn" disabled="true" >Processed</button>';

										} else {
											return '<button type="button" class="greenrefundbtn" >Process</button>';
										}
									} else {
										return '<button hidden="hidden"  disabled="true" class="greenrefundbtn">Processed</button>';
									}
								}
							}, {
								"data" : "payId",
								"visible" : false,
								"className" : "displayNone"

							}, {
								"data" : null,
								"visible" : false,
								"mRender" : function(row) {
									return "\u0027" + row.transactionId;
								}
							}, {
								"data" : "internalCustIp",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "internalUserEmail",
								"visible" : false,
								"className" : "displayNone"
							}, {
								"data" : "customerEmail",
								"visible" : false,
								"className" : "displayNone"
							},]
						});
		$("#merchants").select2({
			//data: payId
			});
	}
	
	function process(table, index) {
		var token = document.getElementsByName("token")[0].value;
		var columnNumber = table.cell(index).index().column;
		var rowIndex = table.cell(index).index().row;
		var row = table.row(rowIndex).data();
		var paymentType = row.paymentMethod ;
		var res = paymentType.split("-");
		
		if (columnNumber == 11) {
			var payId = row.payId;
			var mopType = res[1].trim();
			var payment_method = res[0].trim();
			var txnDate = row.refundDate;
			var txnId = row.transactionId;
			var orderId = row.orderId;
			var currency = row.currencyCode;
			var txnAmount = row.refundedAmount;
			var net = row.refundedAmount;
			var cust_email =  row.customerEmail;
			if (cust_email == null) {
				cust_email = "Not available";
			}
			var businessName = row.businessName;
			var txnType = "REFUND";
			var token = document.getElementsByName("token")[0].value;

			$.ajax({
						url : 'processSettlement',
						type : "POST",
						data : {
							txnDate : decodeVal(txnDate),
							origTxnId : decodeVal(txnId),
							payId : decodeVal(payId),
							orderId : decodeVal(orderId),
							currencyCode : decodeVal(currency),
							mopType : decodeVal(mopType),
							paymentMethod : decodeVal(payment_method),
							amount : decodeVal(txnAmount),
							netAmount : decodeVal(net),
							merchant : decodeVal(businessName),
							cust_email : decodeVal(cust_email),
							txnType : decodeVal(txnType),
							token : token,
							"struts.token.name" : "token",
						},
						success : function(data) {
							var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
							if(null!=response){
								alert(response);			
							}
							reloadTable(true);
						},
						error : function(data) {
							alert("Settlement not processed successfully please try again later");
						}
					});
		}
	}
	
	function reloadTable(retainPageFlag) {
		$('#selectAllCheckBox').attr('checked', false);
		checkedRows = [];
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#refundReportDataTable');
		var table = tableObj.DataTable();
		if (retainPageFlag) {
			//set page dummy callback function
			table.ajax.reload(setPage(), false);
		} else {
			table.ajax.reload();
		}

	}
	//dummy callback
	function setPage() {
	}

	function generatePostData(d) {
		var token = document.getElementsByName("token")[0].value;
		var merchantEmailId = document.getElementById("merchants").value;
		if (merchantEmailId == '') {
			merchantEmailId = 'ALL'
		}
		var obj = {
			acquirer : document.getElementById("account").value,
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			paymentType : document.getElementById("paymentMethods").value,
			merchantEmailId : merchantEmailId,
			currency : document.getElementById("currency").value,
			draw : d.draw,
			length :d.length,
			start : d.start, 
			token : token,
			"struts.token.name" : "token",
		};
		return obj;
	}

	function refundDetailsFunc(table, index) {
		var rows = table.rows();
		var columnNumber = table.cell(index).index().column;

		var token = document.getElementsByName("token")[0].value;
		var rowIndex = table.cell(index).index().row;
		var transactionId = table.cell(rowIndex, 1).data();
		document.getElementById('txnId').value = table.cell(rowIndex, 1).data();
		var orderId = table.cell(rowIndex, 3).data();
		document.getElementById('orderId').value = table.cell(rowIndex, 3)
				.data();
		var payId = table.cell(rowIndex, 12).data();
		document.getElementById('payId').value = table.cell(rowIndex, 12)
				.data();
		var selectedServer = "<s:property value='%{#session.USER.UserType.name()}'/>";	
		if (selectedServer== "SUBUSER" || selectedServer== "ACQUIRER") {
		
		}else{
			if (columnNumber == 1) {
				document.refundDetails.submit();
			}
		}
		
	}	
</script>
</head>
<body>
	<div class="card ">
		<div class="card-header card-header-rose card-header-text">
			<div class="card-text">
				<h4 class="card-title">Refund Report</h4>
			</div>
			<div class="card-body ">
				<div class="container">

					<div class="form-group col-md-2 txtnew col-sm-4 col-xs-6">
						<label for="merchant">Merchant:</label> <br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN' #session.USER_TYPE.name()=='ACQUIRER'}">
							<s:select name="merchants" class="form-control" id="merchants"
								headerKey="" headerValue="ALL" list="merchantList"
								listKey="emailId" listValue="businessName"
								onchange="handleChange();" autocomplete="off" />
						</s:if>
						<s:else>
							<s:select name="merchants" class="form-control" id="merchants"
								list="merchantList" listKey="emailId" headerKey=""
								headerValue="ALL" listValue="businessName"
								onchange="handleChange();" autocomplete="off" />
						</s:else>
					</div>

					<s:if
						test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
						<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">
							<div class="txtnew">
								<label for="acquirer">Account:</label><br />
								<s:select headerKey="ALL" headerValue="ALL" id="account"
									name="account" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()" listKey="code"
									listValue="name" onchange="handleChange();" autocomplete="off" />
							</div>
						</div>
					</s:if>

					<s:else>
						<div class="form-group col-md-2 col-xs-6 txtnew"
							style="display: none;">


							<div class="txtnew">
								<s:select headerKey="ALL" headerValue="ALL" id="account"
									name="account" class="form-control"
									list="@com.kbn.pg.core.AcquirerType@values()" listKey="code"
									listValue="name" onchange="handleChange();" autocomplete="off" />
							</div>

						</div>
					</s:else>


					<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
						<label for="email">Payment Method:</label> <br />
						<s:select headerKey="ALL" headerValue="All" class="form-control"
							list="@com.kbn.commons.util.PaymentType@values()"
							name="paymentMethods" id="paymentMethods"
							onchange="handleChange();" autocomplete="off" value="code"
							listKey="code" listValue="name" />
					</div>
					<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
						<label for="email">Currency:</label> <br />
						<s:select name="currency" id="currency" headerValue="ALL"
							headerKey="ALL" list="currencyMap" class="form-control"
							onchange="handleChange();" />
					</div>

					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" class="form-control" readonly="true"
							id="dateFrom" name="dateFrom" autocomplete="off"
							onchange="handleChange();" />
					</div>
					<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
						<label for="dateTo">Date To:</label> <br />
						<s:textfield type="text" readonly="true" id="dateTo" name="dateTo"
							class="form-control" onchange="handleChange();"
							autocomplete="off" />
					</div>

				</div>
			</div>
		</div>
	</div>
<div class="card ">
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0" class="txnf">
		<tr>
			<td colspan="5" align="left" valign="top">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" valign="top" style="padding: 10px;">
				<div class="scrollD">
					<table id="refundReportDataTable" class="display" cellspacing="0"
						width="100%">
						<thead>
							<tr class="boxheadingsmall" style="font-size: 11px;">
								<s:if
									test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'}">
									<th style='text-align: left'><input type="checkbox"
										name="selectAllCheckBox" id="selectAllCheckBox"></input></th>
								</s:if>
								<s:else>
									<th style='text-align: center'></th>
								</s:else>
								<th style='text-align: center'>Txn Id</th>
								<th style='text-align: center'>Txn Date</th>
								<th style='text-align: center'>Order Id</th>
								<th style='text-align: center'>Refund Date</th>
								<th style='text-align: center'>Business Name</th>
								<th style='text-align: center'>Payment Method</th>
								<th style='text-align: center'>Currency</th>
								<th style='text-align: right'>Txn Amt</th>
								<th style='text-align: right'>Refunded Amt</th>
								<th style='text-align: right'>Available Amt</th>
								<s:if
									test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'}">
									<th style='text-align: right'><input type="button"
										id="processAllButton" value="Process All"
										class="greenrefundbtn" style="width: 70px"></input></th>
								</s:if>
								<s:else>
									<th style='text-align: center'></th>
								</s:else>
								<th style='text-align: right'>Pay Id</th>
								<th style='text-align: center'>Txn Id</th>
								<th style='text-align: center'>Customer IP</th>
								<th style='text-align: center'>Merchant EmailID</th>
								<th style='text-align: center'>Customer EmailID</th>
							</tr>
						</thead>
						<tfoot>
							<tr class="boxheadingsmall">
								<th style='text-align: left;'>Total Amount</th>
								<th style='text-align: right;'></th>
								<th style='text-align: right;'></th>
								<th style='text-align: right;'></th>
								<th style='text-align: right;'></th>
								<th style='text-align: right;'></th>
								<th style='text-align: right; padding-right: 5px;'></th>
								<th style='text-align: right; padding-right: 5px;'></th>
								<th style='text-align: right; padding-right: 5px;'></th>
								<th style='text-align: right; padding-right: 5px;'></th>
								<th style='text-align: right;'></th>
								<th style='text-align: right;'></th>
								<th style='text-align: right;'></th>
								<th style='text-align: right;'></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</td>
		</tr>
	</table>
	</div>
	<s:form name="refundDetails" action="refundConfirmAction">
		<s:hidden name="orderId" id="orderId" value="" />
		<s:hidden name="payId" id="payId" value="" />
		<s:hidden name="transactionId" id="txnId" value="" />
		<s:hidden name="token" value="%{#session.customToken}" />
	</s:form>
</body>
</html>