<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Dashboard</title>
<script src="../js/jquery.min.js"></script>
<link href="../css/custom.css" rel="stylesheet">
<link href="../css/welcomePage.css" rel="stylesheet">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		  // Initialize select2
		 $("#merchant").select2();
	});
</script>
<style>
.navbar-side{

}
</style>
<script type="text/javascript">
//to show new loader --harpreet
$.ajaxSetup({
       global: false,
       beforeSend: function () {
       	toggleAjaxLoader();
       },
       complete: function () {
       	toggleAjaxLoader();
       }
   });
   	
</script>

<script>
	$(document).ready(function() {
		handleChange();

	});
	function handleChange() {
		 lineChart();
		 var currentDate = new Date();
		 var first = currentDate.getDate();
		 var last = currentDate.getDate() + 1;
		 var dateFrom = new Date(currentDate.setDate(first));
		 var dateTo = new Date(currentDate.setDate(last));
		 statistics(dateFrom,dateTo);
		
	}
	
	
</script>
<script>

$(document).ready(function(){
	$('.newteds button').click(function(){
		$('.newteds button').removeClass('btnActive');
		$(this).addClass('btnActive');
	});

	 $("#buttonDay").click(function(env) {
		
			 var currentDate = new Date();
			 var first = currentDate.getDate();
			 var last = currentDate.getDate() +1;
			 var dateFrom = new Date(currentDate.setDate(first));
			 var dateTo = new Date(currentDate.setDate(last));
			 
				statistics(dateFrom,dateTo);
			    

		});

		
		$("#buttonWeekly").click(function(env) {
			var currentDate = new Date();
			var first = currentDate.getDate();
			var last = first - 6;
			var dateTo = new Date(currentDate.setDate(first));
			var dateFrom = new Date(currentDate.setDate(last));
			statistics(dateFrom,dateTo);
		   

		});

		$("#buttonMonthly").click(function(env) {
			
			var currentDate = new Date();
			var first = currentDate.getDate();
			var last = first - 30;
			var dateTo = new Date(currentDate.setDate(first));
			var dateFrom = new Date(currentDate.setDate(last));
			statistics(dateFrom,dateTo);
			
		});
				
				
		$("#buttonYearly").click(function(env) {
			
			var currentDate = new Date();
			var first = currentDate.getDate();
			var last = first - 365;
			var dateTo = new Date(currentDate.setDate(first));
			var dateFrom = new Date(currentDate.setDate(last));
			statistics(dateFrom,dateTo);
			
		});
	
});
		
	
</script>

<script type="text/javascript">
function statistics(dateFrom,dateTo) {
			var token = document.getElementsByName("token")[0].value;
			$
					.ajax({
						url : "statisticsAction",
						type : "POST",
						data : {
							dateFrom : dateFrom,
							dateTo : dateTo,
							emailId : document.getElementById("merchant").value,
							currency : document.getElementById("currency").value,
							token : token,
							"struts.token.name" : "token",
						},
						success : function(data) {
							document.getElementById("dvTotalSuccess").innerHTML = data.statistics.totalSuccess;
							document.getElementById("dvTotalFailed").innerHTML = data.statistics.totalFailed;
							document.getElementById("dvTotalFailedAmount").innerHTML = data.statistics.totalFailedAmount;
							//document.getElementById("dvTotalPending").innerHTML = data.statistics.totalPending;
							//document.getElementById("dvTotalPendingAmount").innerHTML = data.statistics.totalPendingAmount;
							//document.getElementById("dvTotalCancel").innerHTML = data.statistics.totalCancel;
							//document.getElementById("dvTotalCancelAmount").innerHTML = data.statistics.totalCancelAmount;
							document.getElementById("dvTotalInvalid").innerHTML = data.statistics.totalInvalid;
							document.getElementById("dvtotalInvalidAmount").innerHTML = data.statistics.totalInvalidAmount;
							document.getElementById("dvTotalRefunded").innerHTML = data.statistics.totalRefunded;
							document.getElementById("dvRefundedAmount").innerHTML = data.statistics.refundedAmount;
							document.getElementById("dvApprovedAmount").innerHTML = data.statistics.approvedAmount;
							// lineChart();

						}
					});
			
			
		}

 function lineChart(){
	var token = document.getElementsByName("token")[0].value;
	$.ajax({
		url : "lineChartAction",
		type : "POST",
		data : {
			emailId : document.getElementById("merchant").value,
			currency : document.getElementById("currency").value,
			token : token,
			"struts.token.name" : "token",
		},
		success : function(data) {
			var a=[];
			var b=[];
			var c=[];
			var pieChartList = data.pieChart;
			for (var i = 0; i < pieChartList.length; i++) {
				var piechart = pieChartList[i];
				var success =parseInt(piechart.totalSuccess);
				var refund =parseInt(piechart.totalRefunded);
				var failled =parseInt(piechart.totalFailed);
				a.push(success);
				b.push(refund);
				c.push(failled);
				
				
			}
    $(function () {
    $('#container').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ['1', '2', '3', '4', '5', '6',
                '7', '8', '9', '10', '11', '12','13', '14', '15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
        },
        yAxis: {
            title: {
                text: 'Number Of Transaction'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Total Success',
            data:a
        }, {
            name: 'Total Refunded',
            data: b
        }, {
            name: 'Total Failed',
            data:c
        }]
    });
});
		}
});
}
 
</script>
</head>
<body onload="handleChange();" style="margin: 0px; padding: 0px;">
	<div id="page-inner">


		<div class="row">
			<div class="col-md-1 col-xs-12">&nbsp;</div>
			<div class="col-md-8 col-xs-12">
				<s:if
					test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
					<h1 class="page-headerr">Super Admin Dashboard</h1>
				</s:if>

				<s:else>
					<h1 class="page-headerr">Super Admin Dashboard</h1>
				</s:else>
			</div>
			<div class="col-md-2 col-xs-6">
				<div class="page-headerr">
					<s:select name="merchants" class="form-control" id="merchant"
						headerKey="ALL MERCHANTS" headerValue="ALL MERCHANTS"
						listKey="emailId" listValue="businessName" list="merchantList"
						autocomplete="off" onchange="handleChange();" />
				</div>

			</div>
			<div class="col-md-1 col-xs-6">
				<div class="page-headerr" style="border-bottom: none;">
					<s:select name="currency" id="currency" list="currencyMap"
						class="form-control" onchange="handleChange();" />
				</div>

			</div>
		</div>


		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-10 text-center">
				<div class="newteds">
					<button type="button" id="buttonDay" class="newround btnActive newButton">Day</button>
					<button type="button" id="buttonWeekly" class="newround newButton">Week</button>
					<button type="button" id="buttonMonthly" class="newround newButton">Month</button>
					<button type="button" id="buttonYearly" class="newround newButton">Year</button>
				</div>
			</div>
		</div>

		<div class="row" style="margin-top: 2%;">

					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="card card-stats">
						  <div class="card-header card-header-warning card-header-icon">
							<div class="card-icon">
							  <i class="fa fa-thumbs-up fa-5x"></i>
							</div>
							<p class="card-category">Total Success</p>
							<h3 class="card-title" id="dvTotalSuccess">0</h3>
							<s:property value="%{statistics.totalSuccess}" />
						  </div>
						  <div class="card-footer">
							<div class="stats">
								<i class="fa fa-thumbs-up">Success Rates</i>
								<p id="dvCurrency"></p>  <p id="dvApprovedAmount" class="analyse-amount">
										<s:property value="%{statistics.approvedAmount}" />
									</p>
							 
							</div>
						  </div>
						</div>
					      </div>
					      
					   	<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="card card-stats">
						  <div class="card-header card-header-rose card-header-icon">
							<div class="card-icon">
							 <i class="fa fa-thumbs-down fa-5x"> </i>
							</div>
							<p class="card-category">Total Failed</p>
							<h3 class="card-title" id="dvTotalFailed">0</h3>
							<s:property value="%{statistics.totalFailed}" />
						  </div>
						  <div class="card-footer">
							<div class="stats">
								<i class="fa fa-thumbs-up">Success Rates</i>
								<p id="dvCurrency"></p>  <p id="dvTotalFailedAmount" class="analyse-amount">
										<s:property value="%{statistics.totalFailedAmount}" />
									</p>
							 
							</div>
						  </div>
						</div>
					      </div>
					      
					      
					   	<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="card card-stats">
						  <div class="card-header card-header-success card-header-icon">
							<div class="card-icon">
							
							<i class="fa fa-warning fa-5x"> </i>
							</div>
							<p class="card-category">Total Invalid</p>
							<h3 class="card-title" id="dvTotalInvalid">0</h3>
							<s:property value="%{statistics.totalInvalid}" />
						  </div>
						  <div class="card-footer">
							<div class="stats">
								<i class="fa fa-thumbs-up">Success Rates</i>
								<p id="dvCurrency"></p>  <p id="dvtotalInvalidAmount" class="analyse-amount">
										<s:property value="%{statistics.totalInvalidAmount}" />
									</p>
							 
							</div>
						  </div>
						</div>
					      </div>
					      
					      
					    	<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="card card-stats">
						  <div class="card-header card-header-info card-header-icon">
							<div class="card-icon">
							
							<i class="fa fa-share fa-5x"> </i>
							</div>
							<p class="card-category">Total Refund</p>
							<h3 class="card-title" id="dvTotalRefunded">0</h3>
							<s:property value="%{statistics.totalRefunded}" />
						  </div>
						  <div class="card-footer">
							<div class="stats">
								<i class="fa fa-thumbs-up">Success Rates</i>
								<p id="dvCurrency"></p>  <p id="dvRefundedAmount" class="analyse-amount">
										<s:property value="%{statistics.refundedAmount}" />
									</p>
							 
							</div>
						  </div>
						</div>
					      </div>
					      
					  
					
					
				</div>
		<div class="row">
		
		
		<div class="col-md-12 col-xs-12">
					
						<div class="card">
						  <div class="card-header card-header-icon card-header-info">
							<div class="card-icon">
							  <i class="material-icons">timeline</i>
							  
							</div>
							<h4 class="card-title">Monthly Transaction
							 
							</h4>
						  </div>
						  <div class="card-body panel-body scrollD">
                          <div id="container"
							style="min-width: 310px; height: 200px; margin: 0 auto"></div>
						  </div>
						</div>
					  
				
				</div>
		</div>
	
	</div>
</body>
</html>