<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Fraud Prevention System</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery-ui.js"></script>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/popup.css" />
</head>
<script type="text/javascript">
$(document).ready(function() {
	 $.ajaxSetup({
			beforeSend : function(){
				jQuery('body').toggleClass('loaded');
			},
			complete : function(){
				jQuery('body').toggleClass('loaded');
			}
		});	  
	 
	$('#popupButton').click(function(){
        $('#popup').popup('show');
   });
	$('#popupButton2').click(function(){
        $('#popup2').popup('show');
   });
	$('#popupButton3').click(function(){
        $('#popup3').popup('show');
   });
	$('#popupButton4').click(function(){
        $('#popup4').popup('show');
   });
	$('#popupButton5').click(function(){
        $('#popup5').popup('show');
   });
	$('#popupButton6').click(function(){
        $('#popup6').popup('show');
   });
	$('#popupButton7').click(function(){
        $('#popup7').popup('show');
   });
	$('#popupButton8').click(function(){
        $('#popup8').popup('show');
   });
	$('#popupButton9').click(function(){
        $('#popup9').popup('show');
   });
	$('#popupButton0').click(function(){
        $('#popup0').popup('show');
   });
});
</script>

<script type="text/javascript">
var ipRuleColumns = ['ipAddListBody','ipAddress','subnetMask'];
var	domainRuleColumns = ['domainListBody','domainName'];
var emailRuleColumns = ['emailListBody','email'];
var	userCountryColumns = ['userCountryListBody','userCountry'];
var issuerCountryColumns = ['issuerCountryListBody','issuerCountry'];
var perMerchantTxnsColumns = ['noOfTxnsListBody','hourlyTxnLimit', 'dailyTxnLimit','weeklyTxnLimit','monthlyTxnLimit'];
var cardBinColumns = ['cardBinListBody','negativeBin'];
var cardNoColumns = ['cardNoListBody','negativeCard'];
var txnAmountColumns = ['txnAmountListBody','currencyName', 'minTransactionAmount', 'maxTransactionAmount',];
var perCardTxnsColumns = ['perCardTxnsListBody','perCardTransactionAllowed'];
var ajaxValidationFlag = false;

var numberOfTransaction = 'BLOCK_NO_OF_TXNS';
var transactionAmount = 'BLOCK_TXN_AMOUNT';
var ipAddress = 'BLOCK_IP_ADDRESS';
var domainName = 'BLOCK_DOMAIN_NAME';
var email = 'BLOCK_EMAIL_ID';
var userCountry = 'BLOCK_USER_COUNTRY';
var issuerCountry = 'BLOCK_CARD_ISSUER_COUNTRY';
var negativeBin = 'BLOCK_CARD_BIN';
var negativeCard = 'BLOCK_CARD_NO';
var perCardTransactionAllowed ='BLOCK_CARD_TXN_THRESHOLD';

function deleteFraudRule(ruleId){
	//alert(ruleId);
	 $.ajaxSetup({
			beforeSend : function(){
				jQuery('body').toggleClass('loaded');
			},
			complete : function(){
				jQuery('body').toggleClass('loaded');
			}
		});	  
	
		$.ajax({
			url : 'deleteFraudRule',
			type : 'post',
			data : {
				token : document.getElementsByName("token")[0].value,
				payId : fraudFieldValidate('payId',null), //TODO for merchant module
				ruleId : ruleId,
			},
			success : function(data){
				//console.log(data.fraudRuleList);
				 
				if((data.response)!=null){
					 alert(data.response);
					 window.location.reload();
				 }else{
					 alert("Try again, Something went wrong!")
				 }
			},
			error :  function(data){
				alert(data.response);
			}
		}); 
}


function fillDynamicValues(rule,ruleColumns, styleMargin, headerNames){
	
	var tableHtml='<tr';
	var fraudType = rule['fraudType'];
	var col1 ;
	var col2;
	col1 = (headerNames[0]!=null) ? headerNames[0] : '';
	col2 = (headerNames[1]!=null) ? headerNames[1] : '';
	//adding header to fraud rule tables
	var header = 	"<tr class='boxheadingsmall' style='text-align:center;background-color: #2b6dd1;color: white' role='row' ><th style='width:10%;text-align:center'>"+col1+"</th><th style='width:10%;text-align:center'>"+col2+"</th><th style='width:10%;text-align:center'></th></tr>";
		if(document.getElementById(ruleColumns[0]).childNodes[1].childNodes[1] == null){
			$('#'+ruleColumns[0]).append(header);
			document.getElementById(ruleColumns[0]+'Msg').style="display:none";
		}else{			
			}
			for(var i=1; i<ruleColumns.length; i++){
				var noOfRows = document.getElementById(ruleColumns[0]).childNodes[1].childNodes.length-2;
				if(noOfRows %2 ==0){
						tableHtml+=' class="even"><td style="text-align:center;width:133%" role="row">'+rule[ruleColumns[i]]+'</td>';
				}else{
					tableHtml+=' class="odd"><td style="text-align:center;width:42%" role="row" >'+rule[ruleColumns[i]]+'</td>';
				}
			}	
	tableHtml+="<td><input class='btn btn-info btn-xs' style='margin-left:"+styleMargin+"%'  type='submit' value='Delete' onclick='deleteFraudRule("+rule['id']+")'/>";
	$('#'+ruleColumns[0]).append(tableHtml);
	console.log(rule['ipAddress']+':'+rule['subnetMask']);
}
	
function writeFraudTable(item, index){
	var rule = item;
	var fraudType = rule['fraudType'];
	//to genrate dynamic headers
	switch(fraudType){
		case numberOfTransaction:{
			fillDynamicValues(rule,perMerchantTxnsColumns,'46',['Hourly','Daily','Weekly','Monthly'])
		}
		break;
		case transactionAmount:{
			fillDynamicValues(rule,txnAmountColumns,'16',['Currency', 'Min. Amount', 'Max. Amount'])
		}
		break;
		case ipAddress :{
			fillDynamicValues(rule,ipRuleColumns,'18',['Ip Address','Subnet Mask'])
		}
		break;
		case domainName :{
			fillDynamicValues(rule,domainRuleColumns,'46',['Domain Name'])
		}
		break;
		case email:{
			fillDynamicValues(rule,emailRuleColumns,'46',['Email Address'])
		}
		break;
		case userCountry:{
			fillDynamicValues(rule,userCountryColumns,'46',['User Country'])
		}
		break;
		case issuerCountry:{
			fillDynamicValues(rule,issuerCountryColumns,'46',['Card Issuer Country'])
		}
		break;
		case negativeBin:{
			fillDynamicValues(rule,cardBinColumns,'46',['Card Bin'])
		}
		break;
		case negativeCard:{
			fillDynamicValues(rule,cardNoColumns,'46',['Card No.'])
		}
		break;
		case perCardTransactionAllowed:{
			fillDynamicValues(rule,perCardTxnsColumns,'54',['Allowed Transactions per Card'])
		}
		break;
		default:{
			alert('Something went wrong');
		}
	}
}
function fetchFraudRuleList(payIdValue){
	 	$.ajax({
			url : 'fetchFraudRules',
			type : 'post',
			data : {
				token : document.getElementsByName("token")[0].value,
				payId : (payIdValue == 'ALL')? payIdValue :getFieldValue('payId'),
			},
			success : function(data){
				//console.log(data.fraudRuleList);
				 var fraudRuleList = data.fraudRuleList;
				 var noOfRules = fraudRuleList.length;
				 fraudRuleList.forEach(function(item, index){
					 writeFraudTable(item, index);
				 });
			},
			error :  function(data){
				alert('soemthing went wrong'+data.response);
			}
		}); 
}

//clear all the old displayed fraud rules on merchant changed
function clearFraudRules(tableNames){
	var index=0;
	while(index<10){
		for(var i=2;;i++){
		   var element = document.getElementById(tableNames[index]).childNodes[1].childNodes[i];   
	   	   if(element!=null){
	   		   element.innerHTML='';
	   	   }else{
	   		   break;
	   	   }
		}
		index++;
	}
}

function showDialog(data){
	$("#dialogBox").dialog({
	    modal: true,
	    draggable: false,
	    resizable: false,
	    position: ['center', 'center'],
	    show: 'blind',
	    hide: 'blind',
	    width: 328,
	    height:72,
	    buttons: [
	              {
	                  text: "Ok, Proceed",
	                  click: function() {
	                	  $( this ).dialog( "close" );
	                  }
	              },
	          ],
	    open: function(){
	    	$("#dialogBox").css("overflow","hidden");
	    },
	    dialogClass: 'ui-dialog-osx',
	});
}

$(document).ready(function() {
	//by default fraud rules for ALL MERCHANTS will be displayed
	fetchFraudRuleList('ALL');
   $('#payId').change(function(event){
	   clearFraudRules([ipRuleColumns[0],domainRuleColumns[0],emailRuleColumns[0],userCountryColumns[0],issuerCountryColumns[0],perMerchantTxnsColumns[0],txnAmountColumns[0],cardBinColumns[0],cardNoColumns[0],perCardTxnsColumns[0]]);
	   fetchFraudRuleList('payId');	   
    });
});

function makeCardMask(){
	var element = document.getElementById('negativeCard');
	var initialDigits = document.getElementById('cardIntialDigits').value;
	var lastDigits = document.getElementById('cardLastDigits').value;
	value = element.value;
	if(initialDigits.length == 6 && lastDigits.length == 4){
		element.value = initialDigits+"******"+lastDigits;
	}
}

function hideAllPopups(){
	$('#popup').popup('hide');
	$('#popup2').popup('hide');
	$('#popup3').popup('hide');
	$('#popup4').popup('hide');
	$('#popup5').popup('hide');
	$('#popup6').popup('hide');
	$('#popup7').popup('hide');
	$('#popup8').popup('hide');
	$('#popup9').popup('hide');
	$('#popup10').popup('hide');
}

function getFieldValue(fieldName){
	 var element = document.getElementById(fieldName);
		if(element.tagName != 'SELECT'){
			var fieldValue = element.value;
			var finalValue = (fieldValue != '') ?  fieldValue :  ''; //TODO regex
			return finalValue;
			validateFieldValue(finalValue, fraudType);
		}else{
			var fieldValue = element.options[element.selectedIndex].value;
			var finalValue = (!fieldValue.match('SELECT')) ?fieldValue: '';
			return finalValue;
		} 
}

function fraudFieldValidate(fieldName, fraudType){
	if(fieldName == 'payId'){
		return getFieldValue(fieldName);
	}
	switch(fraudType){
	case ipAddress:{
		if(fieldName == "ipAddress" || fieldName == "subnetMask" ){
			var value = getFieldValue(fieldName);
			if(value !='' && value!=null){
				ajaxValidationFlag = false;
				return value;
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	case domainName:{
		if(fieldName == "domainName"){
			var value = getFieldValue(fieldName);
			if(value !='' && value!=null){
				ajaxValidationFlag = false;
				return value;
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	case email:{
		if(fieldName == "email"){
			var value = getFieldValue(fieldName);
			if(value !='' && value!=null){
				var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
				if(value.match(emailRegex)){
					ajaxValidationFlag = false;
					return value;						
				}else{
					ajaxValidationFlag = true;
					break;						
				}
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	case userCountry:{
		if(fieldName == "userCountry"){
			var value = getFieldValue(fieldName);
			if(value !='' && value!=null){
				ajaxValidationFlag = false;
				return value;
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	case issuerCountry:{
		if(fieldName == "issuerCountry"){
			var value = getFieldValue(fieldName);
			if(value !='' && value!=null){
				ajaxValidationFlag = false;
				return value;
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	case numberOfTransaction:{
		if(fieldName == "hourlyTxnLimit" || fieldName=="dailyTxnLimit" || fieldName=="weeklyTxnLimit"
				|| fieldName=="monthlyTxnLimit"){
			var value = getFieldValue(fieldName);
			if(value !='' && value!=null){
				ajaxValidationFlag = false;
				return value;
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	case transactionAmount:{
		
		if(fieldName == "minTransactionAmount" || fieldName == "maxTransactionAmount" || fieldName=="currency"){
			var minAmount = getFieldValue("minTransactionAmount");
			var maxAmount = getFieldValue("maxTransactionAmount");
			var currency = getFieldValue("currency");
			var value = getFieldValue(fieldName);
			if((minAmount.trim()!=null && maxAmount.trim()!=null) && (parseFloat(minAmount.trim())<= parseFloat(maxAmount.trim()))){
				ajaxValidationFlag = false;
				return value;
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	case negativeBin:{
		if(fieldName == "negativeBin"){
			var value = getFieldValue(fieldName);
			if(value !='' && value!=null){
				ajaxValidationFlag = false;
				return value;
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	case negativeCard:{
		if(fieldName == "negativeCard"){
			var value = getFieldValue(fieldName);
			if(value !='' && value!=null){
				ajaxValidationFlag = false;
				return value;
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	case perCardTransactionAllowed:{
		if(fieldName == "perCardTransactionAllowed"){
			var value = getFieldValue(fieldName);
			if(value.trim() != null){
				ajaxValidationFlag = false;
				return value;	
			}else{
				ajaxValidationFlag = true;
				break;
			}
		}
	}break;
	}
}

function ajaxFraudRequest(fraudType){	
	var ipAddress = fraudFieldValidate('ipAddress',fraudType);
	var subnetMask = fraudFieldValidate('subnetMask',fraudType);
	var token = document.getElementsByName("token")[0].value;
	var payId = fraudFieldValidate('payId',fraudType);
	var issuerCountry = fraudFieldValidate('issuerCountry',fraudType);
	var email = fraudFieldValidate('email',fraudType);
	var domainName = fraudFieldValidate('domainName',fraudType);
	var negativeBin = fraudFieldValidate('negativeBin',fraudType);
	var negativeCard = fraudFieldValidate('negativeCard',fraudType);
	var currency = fraudFieldValidate('currency', fraudType);
	var minTransactionAmount = fraudFieldValidate('minTransactionAmount',fraudType);
	var maxTransactionAmount = fraudFieldValidate('maxTransactionAmount',fraudType);
	var userCountry	= fraudFieldValidate('userCountry',fraudType);
	var hourlyTxnLimit = fraudFieldValidate('hourlyTxnLimit', fraudType);
	var dailyTxnLimit = fraudFieldValidate('dailyTxnLimit', fraudType);
	var weeklyTxnLimit = fraudFieldValidate('weeklyTxnLimit', fraudType);
	var monthlyTxnLimit = fraudFieldValidate('monthlyTxnLimit', fraudType);
	var perCardTransactionAllowed = fraudFieldValidate('perCardTransactionAllowed', fraudType);
	if(!ajaxValidationFlag){
		var ajaxRequest =$.ajax({
			url : 'addFraudRule',
			type : 'post',
			data : {
				ipAddress : ipAddress,
				subnetMask : subnetMask,
				token : token,
				payId : payId,
				issuerCountry :issuerCountry,
				email :email,
				domainName : domainName,
				negativeBin : negativeBin,
				negativeCard : negativeCard,
				currency : currency,
				minTransactionAmount : minTransactionAmount,
				maxTransactionAmount : maxTransactionAmount,
				userCountry	: userCountry,
				fraudType : fraudType,
				perCardTransactionAllowed : perCardTransactionAllowed,
				hourlyTxnLimit : hourlyTxnLimit,
				dailyTxnLimit : dailyTxnLimit,
				weeklyTxnLimit : weeklyTxnLimit,
				monthlyTxnLimit : monthlyTxnLimit
			},
			success : function(data){
				hideAllPopups();
				//jQuery('body').toggleClass('loaded');
				var result = data;
				if(result!=null){
					var errorFieldMap = data["Invalid request"];
					if(errorFieldMap !=null){
						var error ;
						for(key in errorFieldMap){
							(error!=null)?( error+','+key):(error=key);
						}
						alert('Please enter correct value in '+error);
					}
					  if(data.responseCode == '342'){
                		  alert(data.responseMsg);
	                		window.location.reload();	
	  					}else{
	  					//TODO
	  					alert(data.response);
	  					document.getElementById("dialogBox").title = data.responseMsg;
	  					}
					showDialog(data);
				}else{
					
				}
			},
			error :  function(data){
				alert('Try Again, Soemthing went wrong! ');
			}
		});
	}else{
		alert('Please enter correct values');
	}
} 


</script>
<style>
.product-spec input[type=text] {
	width: 35px;
}


.ui-dialog-osx {
    -moz-border-radius: 0 0 8px 8px;
    -webkit-border-radius: 0 0 8px 8px;
    border-radius: 0 0 8px 8px; border-width: 3px 3px 3px 3px;
}
.ui-widget-header {
    background: #2b6dd1;
    border: 0;
    color: #fff;
    font-weight: normal;
}
.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset {
    text-align: center;
    float: none !important;
} 
.ui-dialog-content {
display:none;
}
.btn-custom{
width:95px;
background-color: #2b6dd1;
margin-left:20px;
margin-right:20px;
border-color:#5d96ec;
color:#fff;
}
.btn-custom:hover{
background-color: #2b6dd1;
border-color:#5d96ec;
color:#fff;
}
.ui-state-default {
    border: 1px solid #5d96ec;
    background-color: #2b6dd1;
    font-weight: bold;
    color:#fff;
    font-size: 11px;
    height: auto;
}
</style>
</head>
<body>

<div id="dialogBox" title="Fraud rule added successfully">
</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="txnf">
			<tr>
				<td width="21%"><h2>Create Risk Rules</h2></td>
			</tr>
			<tr>
				<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						<tr>
							<td align="left">
								<div class="container">
								<h2 width="75%" align="left">Account Restrictions</h2>
									<table>
									<tr>
									  <td style="width:20%">Search Merchant</td>
									  <td><s:select headerValue="ALL MERCHANTS"
											headerKey="ALL" name="payId" class="form-control"
											id="payId" list="merchantList" listKey="payId"
											listValue="businessName" autocomplete="off"/></td>
									</tr>
									</table>
									<div class="form-group col-md-4 txtnew col-sm-4 col-xs-6"  style="margin-bottom:32px;"></div>
								</div>
							</td>
						</tr>
					</table></td>
			</tr>
		</table>
		<s:hidden name="token" value="%{#session.customToken}"></s:hidden>

	<!-- Dynamically generated content of the restriction page for Admin module -->
	<div id="popup" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="1" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">Block customer IPv4 Address</th>
						</tr>
						<tr>
							<td colspan="2" height="30" align="left">
								<p>
									Enter the IPv4 address and subnet mask you wish to block.<br>
									<br> e.g. 192.168.100.1<br>
									<br> Once added, all transactions from the IP address /
									subnet mask range will be blocked. (Subnet Mask for future use)
								</p>
							</td>
						</tr>
						<tr>
							<td width="7%">IPv4 Address *</td>
							<td width="30%"><input id="ipAddress" type="text" placeholder="192.168.100.1" maxlength="15"
								name="ipAddress" value="" class="form-control" /></td>
						</tr>
						<tr>
							<td>Subnet mask *</td>
							<!--  <td><input type="text" name="subnetMask" class="form-control"></td> -->
							<td><input type="text" id="subnetMask" name="subnetMask" placeholder="255.255.255.0" maxlength="15"
								value="" class="form-control" /></td>
						</tr>
												<tr>
							<td colspan="2"><input type="submit" value="Block"
								onclick="ajaxFraudRequest(ipAddress)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" /></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="popup2" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="5" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">Block card issuer country</th>
						</tr>
						<tr>
							<td width="7%">Countries *</td>
							<td width="30%"><select name="country" id="issuerCountry"
								class="form-control">
									<option value="">Select Card Issuer Country</option>
									<option value="Antigua Barbuda">Antigua Barbuda</option>
									<option value="Argentina">Argentina</option>
									<option value="Armenia">Armenia</option>
									<option value=" Australia">Australia</option>
									<option value=" Austria">Austria</option>
									<option value=" Azerbaijan">Azerbaijan</option>
									<option value=" Bahrain">Bahrain</option>
									<option value=" Belgium">Belgium</option>
									<option value=" Botswana">Botswana</option>
									<option value=" Brazil">Brazil</option>
									<option value=" Brunei Darussalam ">Brunei Darussalam
									</option>
									<option value=" Cambodia ">Cambodia</option>
									<option value=" Cameroon ">Cameroon</option>
									<option value=" Canada ">Canada</option>
									<option value=" Chile ">Chile</option>
									<option value=" China ">China</option>
									<option value=" Colombia">Colombia</option>
									<option value=" Croatia ">Croatia</option>
									<option value=" Cuba ">Cuba</option>
									<option value=" Cyprus ">Cyprus</option>
									<option value=" Czech Republic ">Czech Republic</option>
									<option value=" Denmark ">Denmark</option>
									<option value=" Egypt ">Egypt</option>
									<option value=" Estonia ">Estonia</option>
									<option value=" Ethiopia ">Ethiopia</option>
									<option value=" Fiji ">Fiji</option>
									<option value=" Finland ">Finland</option>
									<option value=" France ">France</option>
									<option value=" Germany ">Germany</option>
									<option value=" Ghana ">Ghana</option>
									<option value=" Greece ">Greece</option>
									<option value=" Hong Kong ">Hong Kong</option>
									<option value=" Hungary ">Hungary</option>
									<option value=" Iceland ">Iceland</option>
									<option value=" India ">India</option>
									<option value=" Indonesia ">Indonesia</option>
									<option value=" Ireland ">Ireland</option>
									<option value=" Italy ">Italy</option>
									<option value=" Jamaica ">Jamaica</option>
									<option value=" Japan ">Japan</option>
									<option value=" Jordan ">Jordan</option>
									<option value=" Kazakhstan ">Kazakhstan</option>
									<option value=" Kenya ">Kenya</option>
									<option value=" Kuwait ">Kuwait</option>
									<option value=" Kyrgyzstan ">Kyrgyzstan</option>
									<option value=" Laos ">Laos</option>
									<option value=" Latvia ">Latvia</option>
									<option value="Lebanon ">Lebanon</option>
									<option value="Libya">Libya</option>
									<option value="Liechtenstein">Liechtenstein</option>
									<option value="Lithuania">Lithuania</option>
									<option value="Luxembourg">Luxembourg</option>
									<option value="Madagascar">Madagascar</option>
									<option value="Maldives">Maldives</option>
									<option value="Malaysia">Malaysia</option>
									<option value="Malta">Malta</option>
									<option value="Mauritius">Mauritius</option>
									<option value="Monaco">Monaco</option>
									<option value="Mongolia">Mongolia</option>
									<option value="Morocco">Morocco</option>
									<option value="Mozambique">Mozambique</option>
									<option value="Myanmar (Burma)">Myanmar (Burma)</option>
									<option value="Namibia">Namibia</option>
									<option value="Nepal">Nepal</option>
									<option value="Netherlands">Netherlands</option>
									<option value="New Zealand">New Zealand</option>
									<option value="Nigeria">Nigeria</option>
									<option value="Norway">Norway</option>
									<option value="Oman">Oman</option>
									<option value="Pakistan">Pakistan</option>
									<option value="Peru">Peru</option>
									<option value="Philippines">Philippines</option>
									<option value="Poland">Poland</option>
									<option value="Portugal">Portugal</option>
									<option value="Qatar">Qatar</option>
									<option value="Russia">Russia</option>
									<option value="Singapore">Singapore</option>
									<option value="Slovakia">Slovakia</option>
									<option value="Slovenia">Slovenia</option>
									<option value="South Africa">South Africa</option>
									<option value="South Korea">South Korea</option>
									<option value="Spain">Spain</option>
									<option value="Sri Lanka">Sri Lanka</option>
									<option value="Sweden">Sweden</option>
									<option value="Switzerland">Switzerland</option>
									<option value="Taiwan">Taiwan</option>
									<option value="Tajikistan">Tajikistan</option>
									<option value="Tanzania">Tanzania</option>
									<option value="Thailand">Thailand</option>
									<option value="Tunisia">Tunisia</option>
									<option value="Turkey">Turkey</option>
									<option value="Turkmenistan">Turkmenistan</option>
									<option value="United Arab Emirates">United Arab
										Emirates</option>
									<option value="Ukraine">Ukraine</option>
									<option value="United Kingdom">United Kingdom</option>
									<option value="UK">UK</option>
									<option value="United States of America">United States
										of America</option>
									<option value="USA">USA</option>
									<option value="Uzbekistan">Uzbekistan</option>
									<option value="Vietnam">Vietnam</option>
									<option value="Zimbabwe">Zimbabwe</option>

							</select></td>
						</tr>
						<tr>
							
							<td colspan="2"><input type="submit" value="Block"
								onclick="ajaxFraudRequest(issuerCountry)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" />
								<!-- <input type="submit" name="remittSubmit" value="Block" id="" class="btn btn-success btn-sm" /> --></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="popup3" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">Block card bin range</th>
						</tr>
						<tr>
							<td colspan="2" height="30" align="left"><p>
									Enter the first 6 digits of the card number you wish to block;<br>
									<br> Once added, all transactions within that card range
									will be blocked.
								</p></td>
						</tr>
						<tr>
							<td width="7%">Card range *</td>
							<td width="30%"><input id="negativeBin" type="number" placeholder="6-digit bin range" maxlength="6" 
								name="negativeBin" class="form-control" /></td>
						</tr>

						<tr>
                        <td colspan="2"><input type="submit" value="Block"
								onclick="ajaxFraudRequest(negativeBin)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" /></td>
                        </tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="popup4" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="2" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">Block customer country</th>
						</tr>
						<tr>
							<td width="7%">Countries *</td>
							<td width="30%"><select name="userCountry" id="userCountry"
								class="form-control">
									<option value="">Select User Country</option>
									<option value="Antigua Barbuda">Antigua Barbuda</option>
									<option value="Argentina">Argentina</option>
									<option value="Armenia">Armenia</option>
									<option value=" Australia">Australia</option>
									<option value=" Austria">Austria</option>
									<option value=" Azerbaijan">Azerbaijan</option>
									<option value=" Bahrain">Bahrain</option>
									<option value=" Belgium">Belgium</option>
									<option value=" Botswana">Botswana</option>
									<option value=" Brazil">Brazil</option>
									<option value=" Brunei Darussalam ">Brunei Darussalam
									</option>
									<option value=" Cambodia ">Cambodia</option>
									<option value=" Cameroon ">Cameroon</option>
									<option value=" Canada ">Canada</option>
									<option value=" Chile ">Chile</option>
									<option value=" China ">China</option>
									<option value=" Colombia">Colombia</option>
									<option value=" Croatia ">Croatia</option>
									<option value=" Cuba ">Cuba</option>
									<option value=" Cyprus ">Cyprus</option>
									<option value=" Czech Republic ">Czech Republic</option>
									<option value=" Denmark ">Denmark</option>
									<option value=" Egypt ">Egypt</option>
									<option value=" Estonia ">Estonia</option>
									<option value=" Ethiopia ">Ethiopia</option>
									<option value=" Fiji ">Fiji</option>
									<option value=" Finland ">Finland</option>
									<option value=" France ">France</option>
									<option value=" Germany ">Germany</option>
									<option value=" Ghana ">Ghana</option>
									<option value=" Greece ">Greece</option>
									<option value=" Hong Kong ">Hong Kong</option>
									<option value=" Hungary ">Hungary</option>
									<option value=" Iceland ">Iceland</option>
									<option value=" India ">India</option>
									<option value=" Indonesia ">Indonesia</option>
									<option value=" Ireland ">Ireland</option>
									<option value=" Italy ">Italy</option>
									<option value=" Jamaica ">Jamaica</option>
									<option value=" Japan ">Japan</option>
									<option value=" Jordan ">Jordan</option>
									<option value=" Kazakhstan ">Kazakhstan</option>
									<option value=" Kenya ">Kenya</option>
									<option value=" Kuwait ">Kuwait</option>
									<option value=" Kyrgyzstan ">Kyrgyzstan</option>
									<option value=" Laos ">Laos</option>
									<option value=" Latvia ">Latvia</option>
									<option value="Lebanon ">Lebanon</option>
									<option value="Libya">Libya</option>
									<option value="Liechtenstein">Liechtenstein</option>
									<option value="Lithuania">Lithuania</option>
									<option value="Luxembourg">Luxembourg</option>
									<option value="Madagascar">Madagascar</option>
									<option value="Maldives">Maldives</option>
									<option value="Malaysia">Malaysia</option>
									<option value="Malta">Malta</option>
									<option value="Mauritius">Mauritius</option>
									<option value="Monaco">Monaco</option>
									<option value="Mongolia">Mongolia</option>
									<option value="Morocco">Morocco</option>
									<option value="Mozambique">Mozambique</option>
									<option value="Myanmar (Burma)">Myanmar (Burma)</option>
									<option value="Namibia">Namibia</option>
									<option value="Nepal">Nepal</option>
									<option value="Netherlands">Netherlands</option>
									<option value="New Zealand">New Zealand</option>
									<option value="Nigeria">Nigeria</option>
									<option value="Norway">Norway</option>
									<option value="Oman">Oman</option>
									<option value="Pakistan">Pakistan</option>
									<option value="Peru">Peru</option>
									<option value="Philippines">Philippines</option>
									<option value="Poland">Poland</option>
									<option value="Portugal">Portugal</option>
									<option value="Qatar">Qatar</option>
									<option value="Russia">Russia</option>
									<option value="Singapore">Singapore</option>
									<option value="Slovakia">Slovakia</option>
									<option value="Slovenia">Slovenia</option>
									<option value="South Africa">South Africa</option>
									<option value="South Korea">South Korea</option>
									<option value="Spain">Spain</option>
									<option value="Sri Lanka">Sri Lanka</option>
									<option value="Sweden">Sweden</option>
									<option value="Switzerland">Switzerland</option>
									<option value="Taiwan">Taiwan</option>
									<option value="Tajikistan">Tajikistan</option>
									<option value="Tanzania">Tanzania</option>
									<option value="Thailand">Thailand</option>
									<option value="Tunisia">Tunisia</option>
									<option value="Turkey">Turkey</option>
									<option value="Turkmenistan">Turkmenistan</option>
									<option value="United Arab Emirates">United Arab
										Emirates</option>
									<option value="Ukraine">Ukraine</option>
									<option value="United Kingdom">United Kingdom</option>
									<option value="UK">UK</option>
									<option value="United States of America">United States
										of America</option>
									<option value="USA">USA</option>
									<option value="Uzbekistan">Uzbekistan</option>
									<option value="Vietnam">Vietnam</option>
									<option value="Zimbabwe">Zimbabwe</option>

							</select></td>
						</tr>
						<tr>
						
							<td colspan="2"><input type="submit" value="Block"
								onclick="ajaxFraudRequest(userCountry)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" /></td>
						</tr>

					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="popup5" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="3" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">Block customer email addres</th>
						</tr>
						<tr>
							<td width="7%">Email *</td>
							<td width="30%"><input id="email" type="email" name="email" placeholder="user@domain.xyz"
								class="form-control" /></td>
						</tr>
						<tr>
							
							<td colspan="2"><input type="submit" value="Block"
								onclick="ajaxFraudRequest(email)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" /></td>
						</tr>

					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="popup6" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="2" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">Add
								a Blocked Domains</th>
						</tr>
						<tr>
							<td width="7%">Domain *</td>
							<td width="30%"><input type="text" id="domainName" placeholder="www.domain.xyz"
								name="domainName" class="form-control"></td>
						</tr>
						<tr>
							
							<td colspan="2"><input type="submit" value="Block"
								onclick="ajaxFraudRequest(domainName)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" /></td>
						</tr>

					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="popup7" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="6" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">No. of transaction allowed</th>
						</tr>
						<tr>
							<td width="7%">Hourly</td>
							<td width="30%"><input id="hourlyTxnLimit" type="number" placeholder="1" maxlength="15"
								name="" value="" class="form-control" /></td>
						</tr>
												<tr>
							<td width="7%">Daily</td>
							<td width="30%"><input id="dailyTxnLimit" type="number" placeholder="1" maxlength="15"
								name="" value="" class="form-control" /></td>
						</tr>
												<tr>
							<td width="7%">Weekly</td>
							<td width="30%"><input id="weeklyTxnLimit" type="number" placeholder="1" maxlength="15"
								name="" value="" class="form-control" /></td>
						</tr>
											<tr>
							<td width="7%">Monthly</td>
							<td width="30%"><input id="monthlyTxnLimit" type="number" placeholder="1" maxlength="15"
								name="" value="" class="form-control" /></td>
						</tr>

						<tr>
						
							<td colspan="2"><input type="submit" value="Block"
								onclick="ajaxFraudRequest(numberOfTransaction)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" /></td>
						</tr>

					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="popup8" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="7" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">Limit transaction amount</th>
						</tr>
						<tr>
							<td width="7%">Currency *</td>
							<td width="30%"><s:select name="currency" class="form-control"
											id="currency" list="currencyMap"/></td>
						</tr>
						
						<tr>
							<td width="7%">Amount(Min Amount) *</td>
							<td width="30%"><input type="number" id="minTransactionAmount" placeholder="10"
								name="minTransactionAmount" class="form-control"></td>
						</tr>
						<tr>
							<td width="7%">Amount(Max Amount) *</td>
							<td width="30%"><input type="number" id="maxTransactionAmount" placeholder="110"
								name="maxTransactionAmount" class="form-control"></td>
						</tr>
						
						<tr>
							
							<td colspan="2"><input type="submit" value="Block"
								onclick="ajaxFraudRequest(transactionAmount)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" /></td>
						</tr>

					</table>
				</div>
			</div>
		</div>
	</div>
    <div id="popup9" style="display: none;">
	<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="6" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="3" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">
								Enter card no </th>
						</tr>
						
						<tr>
							<td width="13%" >Sample card no.</td>
							<td width="24%"><span style="font: normal 15px Arial">411111</span>
							<span style="font: normal 10px Arial">******</span>
							<span style="font: normal 15px Arial">1111</span>
							</td>
						</tr>
						<tr>
								<td width="7%">Card Number *</td>
							<td width="30%">
							<input id="cardIntialDigits" placeholder="Initial 6-digits of car	d" type="number" maxlength="6" class="form-control" onkeyup="makeCardMask()")>
							<br/>
								<input id="cardLastDigits" placeholder="Last 4-digits of card" type="number" maxlength="4" name="negativeCard" onkeyup="makeCardMask()" class="form-control" >
								<input type="hidden" id="negativeCard" name="negativeCard" value="">
								</td>
						</tr>
						<tr>
				
							<td colspan="2"><input type="submit" value="Block"
								onclick="ajaxFraudRequest(negativeCard)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" /></td>
						</tr>

					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="popup0" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="6" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="3" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">
								Limit no. of transactions per card </th>
						</tr>
						
						<tr>
						<td width="7%">No. of Transactions *</td>
					    <td width="30%">
							<input id="perCardTransactionAllowed" type="number" placeholder="e.g 10" class="form-control" min="0">
						</td>
						</tr>
						<tr>
                 		<td colspan="2"><input type="submit" value="Block" onclick="ajaxFraudRequest(perCardTransactionAllowed)" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;"  /></td>
					</table>
				</div>
			</div>
		</div>
	</div>
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="txnf">
		<tr>
			<td align="left"><table width="100%" border="0">
					<tr>

						<td width="4%" align="center">&nbsp;</td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td align="left" valign="top">
			<div class="adduT">
					<div class="bkn">
						<h4>Blocked IP Addresses</h4>
						<div class="adduT">
						<table>
						  <tr>
						    <td id="ipAddListBodyMsg" style="display:block">No blocked IP addresses</td>
						    
						    
						    <td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;"><input type="submit" name="remittSubmit" value="Add Rule"
								id="popupButton" class="btn btn-success btn-md" /></div></td>
							
						  </tr>
						</table>
						
						</div>
						
						<div>
							<table id="ipAddListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
								<tbody>
								
								</tbody>
							</table>
						</div>
						
						<div class="clear"></div>
					</div>
					<div class="bkn">
						<h4>Blocked Issuer Countries</h4>
						<div class="adduT">
						   <table>
						  <tr>
						    <td id="issuerCountryListBodyMsg" style="display:block">No blocked issuer countries</td>
						    <td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;"><input type="submit" name="remittSubmit" value="Add Rule"
								id="popupButton2" class="btn btn-success btn-md" /></div></td>
						  </tr>
						</table>
						</div>
						<div>
						    <table id="issuerCountryListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
								<tbody>
									
								 
								</tbody>
							</table>
							<div class="clear"></div>
						</div>
					</div>
		     </div>
            <div class="clear">
            </div>
		    <div class="adduT">
				<div class="bkn">
					<h4>Blocked Card Ranges</h4>
				        <div class="adduT">
				           <table>
						  <tr>
						    <td id="cardBinListBodyMsg" style="display:block">No blocked card ranges</td>
						    <td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;"><input type="submit" name="remittSubmit" value="Add Rule"
								id="popupButton3" class="btn btn-success btn-md" /></div></td>
						  </tr>
						</table>
				        
				        </div>
						<div>
						 <table id="cardBinListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
								<tbody>
									
								
								</tbody>
							</table>
						</div>
						
				</div>
				<div class="bkn">
						<h4>Blocked User Countries</h4>
				<div class="adduT">
						   <table>
						  <tr>
						    <td id="userCountryListBodyMsg" style="display:block">No blocked user countries</td>
						    <td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;"><input type="submit" name="remittSubmit" value="Add Rule"
								id="popupButton4" class="btn btn-success btn-md" /></div></td>
						  </tr>
						</table>
					</div>
						<div>
						 <table id="userCountryListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
								<tbody>
								
								</tbody>
							</table>
							
			          </div>
			          
                  </div>
            <div class="clear"></div>
            </div>
			<div class="clear"></div>
			<div class="adduT">
			<div class="bkn">
						<h4>Blocked Email Addresses</h4>
						<div class="adduT">
						   <table>
						  <tr>
						    <td id="emailListBodyMsg" style="display:block">No blocked Email Addresses</td>
						    <td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;"><input type="submit" name="remittSubmit" value="Add Rule"
								id="popupButton5" class="btn btn-success btn-md" /></div></td>
						  </tr>
						</table>
						
						
						</div>
						<div>
						    <table id="emailListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
								<tbody>
								
								 
								</tbody>
							</table>
							<div class="clear"></div>
			        </div>
				</div>
			<div class="bkn">
						<h4>Blocked Domains</h4>
						<div class="adduT">
					   <table>
						  <tr>
						    <td id="domainListBodyMsg" style="display:block">No blocked Domains</td>
						    <td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;"><input type="submit" name="remittSubmit" value="Add Rule"
								id="popupButton6" class="btn btn-success btn-md" /></div></td>
						  </tr>
						</table>	
						
						</div>
						<div>
						  <table id="domainListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
								<tbody>
								
								
								</tbody>
							</table>
						</div>
						
						<div class="clear"></div>
					</div>

</div>
			<div class="clear"></div>
			<div class="adduT">
			<div class="bkn">
			<h4>Blocked on Transactional Velocity</h4>
			<div class="adduT">
			 <table>
				<tr>
				 <td id="noOfTxnsListBodyMsg" style="display:block">You have blocked on Transactional Velocity</td>
				  <td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;"><input type="submit" name="remittSubmit" value="Add Rule"
								id="popupButton7" class="btn btn-success btn-md" /></div></td>
				</tr>
			</table>	
		  </div>
						<div>
							
							<table id="noOfTxnsListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
							    <tbody>
											<tr class="boxheadingsmall" style="text-align:center;background-color: #2b6dd1;color: white" role="row">
													<th style="width:10%;text-align:center">Hourly</th>
													<th style="width:10%;text-align:center">Daily</th>
													<th style="width:10%;text-align:center">Weekly</th>
													<th style="width:10%;text-align:center">Monthly</th>
											        <th style="width:10%;text-align:center"></th>
											</tr>
											<tr class="even">
												<td style="text-align:center;" role="row">1</td>
												<td style="text-align:center;" role="row">1</td>
												<td style="text-align:center;" role="row">1</td>
												<td style="text-align:center;" role="row">1</td>
												<td style="text-align:center;" role="row"><input class="btn btn-info btn-xs" style="margin-left:46%" type="submit" value="Delete" onclick="deleteFraudRule(1609271111571001)"></td>
										   </tr>
							     </tbody>
							</table>
						</div>
						<div class="clear"></div>
					</div>
					<div class="bkn">
						<h4>Blocked Transactional Amount</h4>
						<div class="adduT">
					<table>
						<tr>
						 <td id="txnAmountListBodyMsg" style="display:block">No blocked Transactional Amount</td>
							  <td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;"><input type="submit" name="remittSubmit" value="Add Rule"
								id="popupButton8" class="btn btn-success btn-md" /></div></td>
						</tr>
					</table>
						
						</div>
						<div>
							
							<table id="txnAmountListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
							<tbody>
									<tr class="boxheadingsmall" style="text-align:center;background-color: #2b6dd1;color: white" role="row">
									  <th style="width:10%;text-align:center">Currency</th>
									  <th style="width:10%;text-align:center">Min. Amount</th>
									  <th style="width:10%;text-align:center">Max. Amount</th>
								      <th style="width:10%;text-align:center"></th>
									</tr>
									<tr class="even">
									  <td style="text-align:center;" role="row">INR</td>
									  <td style="text-align:center;" role="row">10</td>
									  <td style="text-align:center;" role="row">110</td>
									  <td style="text-align:center;" role="row"><input class="btn btn-info btn-xs" style="margin-left:60%" type="submit" value="Delete" onclick="deleteFraudRule(1609271124341003)"></td>
								   </tr>
						 </tbody>
							</table>
						</div>
							
					</div>
				</div>	
            <div class="clear"></div> 
            <div class="adduT">
			<div class="bkn">
							<h4>Blocked Card Nos.</h4>
						<div class="adduT">
							<table>
								<tr>
						 			<td id="cardNoListBodyMsg" style="display:block">No blocked card nos.</td>
							  		<td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;"><input type="submit" name="remittSubmit" value="Add Rule"
									id="popupButton9" class="btn btn-success btn-md" /></div></td>
								</tr>
							</table>
					   </div>
					<div>
							<table id="cardNoListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
								<tbody>
							
								</tbody>
								<div class="clear"></div>
							</table>
						</div>
					</div>
					<div class="bkn">
							<h4>Blocked Card Transaction</h4>
						<div class="adduT">
							<table>
								<tr>
						 			<td id="perCardTxnsListBodyMsg" style="display:block">No blocked card txn</td>
							  		<td style="width:6%"><div class="adduT" style="text-align: right; padding: 14px 0 0 0;">
							  		<input type="submit" name="remittSubmit" value="Add Rule"
									id="popupButton0" class="btn btn-success btn-md" /></div></td>
								</tr>
							</table>
					   </div>
					<div>
							<table id="perCardTxnsListBody" class="display dataTable no-footer" aria-describedby="invoiceDataTable_info" role="grid">
								<tbody>
									
								</tbody>
								<div class="clear"></div>
							</table>
						</div>
					</div>
	         </div>
		
	</td>
		</tr>
	</table>
</body>
</html>