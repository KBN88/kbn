<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
	<div class="content">
		<div class="container-fluid">
			<div id="loading" style="text-align: center; display: none;">
				<img id="loading-image" style="width: 70px; height: 70px;"
					src="../image/sand-clock-loader.gif" alt="Sending SMS...">
			</div>


			<form id="routerConfigurationForm" name="routerConfigurationForm"
				action="/crm/jsp/routerConfigurationActionMerchant" method="post">

				<div class="col-md-12">
					<div class="card ">
						<div class="card-header card-header-rose card-header-text">
							<div class="card-text">
								<h4 class="card-title">Router Configuration</h4>
							</div>
						</div>
						<div class="card-body ">
							<div class="container">
								<div class="row">
									<!-- <div class="col-md-12" style="margin-bottom:30px"> -->
									<div class="col-sm-6 col-lg-3">
										<label style="float: left;">Merchant Name:</label><br>
										<div id="wwgrp_merchantName" class="wwgrp">
											<div id="wwctrl_merchantName" class="wwctrl">

												<select name="merchantName" id="merchantName"
													class="input-control select2-hidden-accessible"
													autocomplete="off" tabindex="-1" aria-hidden="true">
													<option value="">Select Merchant</option>
													<option value="1004400109080101">OYO ROOM</option>


												</select> <span
													class="select2 select2-container select2-container--default"
													dir="ltr" style="width: 255px;"><span
													class="selection"> <span
														class="select2-selection select2-selection--single"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0"
														aria-labelledby="select2-merchantName-container"> <span
															class="select2-selection__rendered"
															id="select2-merchantName-container"
															title="Select Merchant">Select Merchant</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span class="dropdown-wrapper"
													aria-hidden="true"></span></span>

											</div>
										</div>
									</div>
									<div class="col-sm-6 col-lg-3">
										<label style="float: left;">Payment Type:</label><br>

										<div id="wwgrp_paymentMethods" class="wwgrp">
											<div id="wwctrl_paymentMethods" class="wwctrl">

												<select name="paymentMethod" id="paymentMethods"
													class="input-control" autocomplete="off">
													<option value="Select PaymentType">Select Payment
														Type</option>
													<option value="CC">Credit Card</option>
													<option value="DC">Debit Card</option>
													<option value="NB">Net Banking</option>
													<option value="EM">EMI</option>
													<option value="WL">Wallet</option>
													<option value="RP">Recurring Payment</option>
													<option value="EX">Express Pay</option>
													<option value="DP">Debit Card With Pin</option>
													<option value="UP">UPI</option>
													


												</select>
												

											</div>
										</div>
									</div>
									<div class="col-sm-6 col-lg-3">
										<label style="float: left;">Payment Region Type:</label><br>
										<div id="wwgrp_cardHolderType" class="wwgrp">
											<div id="wwctrl_cardHolderType" class="wwctrl">

												<select name="cardHolderType" id="cardHolderType"
													class="input-control">
													<option value="Select Payment region">Select
														Payment region</option>
													<option value="COMMERCIAL">Commercial</option>
													<option value="CONSUMER">Consumer</option>


												</select>

											</div>
										</div>
									</div>



									<div class="col-sm-6 col-lg-3">

										<div class="txtnew">
											<button class="btn btn-primary  mt-4 submit_btn disabled"
												id="Download" disabled="">Submit</button>

										</div>
									</div>


								</div>
							</div>


						</div>
					</div>
				</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="txnf" style="margin-top: 1%;">

					<tbody>
						<tr>
							<!-- <td width="21%"><h2 style="margin-bottom:5%;">Router Configuration</h2></td> -->
						</tr>

						<tr>
							<td align="center" valign="top">
								<!-- <table width="98%" border="0" cellspacing="0" cellpadding="0">
				
						<tr>
							<td align="left"> -->

							</td>
						</tr>

						<tr>

							<td align="left">
								<div id="datatable2" class="scrollD">
									<br>





								</div>
							</td>
						</tr>
					</tbody>
				</table>









			</form>
		</div>
	</div>
</body>
</html>