<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	String error = request.getParameter("error");
	if (error == null || error == "null") {
		error = "";
	}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="899;url=index" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>login</title>
<link rel="stylesheet" type="text/css"
	href="<s:url value="../css/default.css"/>" />
<script src="../js/login.js"></script>
<script src="../js/captcha.js"></script>
<style type="text/css">
/* Manual css s:actionmessage for this page */
.errorMessage {
	font: normal 11px arial;
	color: #ff0000;
	display: block;
	margin: -15px 0px 3px 0px;
	padding: 0px 0px 0px 0px;
}
</style>
<script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		if (theBody != null) {
			theBody.style.display = "block";
		}
	} else {
		top.location = self.location;
	}
</script>
</head>
<body class="bodyColor"
	onload="callMerchantEnv();return generateCaptcha();" style="background:#ebeef1">
	<s:form name=" " action="login" method="post"
		onselectstart="return false" oncontextmenu="return false;" style="height: 100vh; display: flex;justify-content: center;align-items: center;
    position: relative;
    top: -50px;
		">
		<s:token />
		<br />
		
		
			   
	
		
		<span id="merchantEnviroment"></span>
		<table width="100" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td height="60"> <h1 style="text-align:center;font-size:40px">UrsPay</h1>  &nbsp;</td>
			</tr>
			<tr>
				<td><s:div class="signupbox">
						<table width="360" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<!-- <td align="center" class="signup-headingbg2"><img
									src="" width="180" /></td> -->
								<td align="center" class="signup-headingbg2" style="background: linear-gradient(60deg, #425185, #4a9b9b); color:#fff;font-size:14px"><p>Welcome back !</p></td>	
							</tr>
							<tr>
								<td align="center"><table width="90%" border="0"
										align="center" cellpadding="0" cellspacing="0">
										<tr>
											<td height="20" align="left" valign="middle"></td>
										</tr>
										<tr>
											<td><span><s:actionmessage /></span></td>
										</tr>
										<tr>
											<td align="left" height="50" valign="middle"><span
												class="user_icon"></span> <s:textfield name="emailId"
													type="text" cssClass="signuptextfieldicon" id="emailId"
													placeholder="User Id" autocomplete="off"
													required="required" onkeypress="emailCheck()" /><span
												id="error2"></span></td>
										</tr>
										<tr>
											<td align="left" height="50" valign="middle"><span
												class="pswrd_icon"></span> <s:textfield name="password"
													type="password" cssClass="signuptextfieldicon"
													id="password" placeholder="Password" autocomplete="off"
													required="required" onkeypress="passCheck()" /><span
												id="error2"></span></td>
										</tr>
										<tr>
											<td align="left" height="50" valign="middle">
												<div class="rederror" id="error3"></div>
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr>
														<td width="51%" align="left"><s:textfield
																name="captcha" type="text" cssClass="signuptextfieldsml"
																id="captcha" placeholder="Enter Captcha Code"
																autocomplete="off" maxlength="4" /></td>
														<td width="6%" align="left">&nbsp;</td>
														<td width="32%" align="center"><s:textfield
																name="captchaCode" type="text" cssClass="accesscode"
																id="captchaCode" autocomplete="off"
																oncopy="return false" /></td>
														<td width="11%" align="right"><s:textfield
																id="Btnaccescd" type="button" class="refreshbutton"
																value="" onclick="return generateCaptcha();" /></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="left" height="60" valign="middle"><s:fielderror
													class="redvalid">
													<s:param>userId</s:param>
													<s:param>password</s:param>
												</s:fielderror> <s:submit cssClass="signupbutton btn-primary" key="submit" style="background: linear-gradient(60deg, #425185, #4a9b9b);"
													value="Submit" onclick="return ValidCaptcha();"></s:submit></td>
										</tr>
										<tr>
											<td colspan="2" align="center" valign="middle">
												<table width="100%" border="0" align="center"
													cellpadding="4" cellspacing="0">
													<tr>
														<td align="left" valign="middle"><s:a
																action="merchantSignup">Don't have an account? Sign Up</s:a></td>
														<td align="right" valign="middle"><s:a
																action="forgetPassword">Forgot Password?</s:a></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="left" colspan="2" class="lightbluebg" height="1"></td>
										</tr>
										<tr>
											<td align="left" colspan="2">&nbsp;</td>
										</tr>
									</table></td>
							</tr>
						</table>
					</s:div></td>
			</tr>
		</table>
	</s:form>
</body>
</html>