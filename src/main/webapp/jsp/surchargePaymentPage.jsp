<%@page import="com.kbn.pg.core.Amount"%>
<%@page import="com.kbn.pg.core.Currency"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.owasp.esapi.ESAPI"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="antlr.StringUtils"%>
<%-- <%@page import="com.mmadpay.pg.core.Amount"%>
<%@page import="com.mmadpay.pg.core.Currency"%> --%>
<%@page import="com.kbn.commons.util.FieldType"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title><s:property value="%{#session.pageTittle}" /></title>
<script type="text/javascript" src="../js/surchargePaymentPage.js"></script>
<link href="../css/credit-card-form.css" media="all" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../js/credit-card-form.js"></script>
<style>
.textstyle, h1, h2, h3, input {	font-family: <s:property value = "%{#session.TEXT_STYLE}" /> !important; }
.textcolor, h1, h2, h3, ul.nav-tabs>li>a, p, small { color: #<s:property value="%{#session.TEXT_COLOR}"/> !important; }
.background {background-color: #<s:property value="%{#session.BACKGROUND_COLOR}"/>;}
a:link {color: #<s:property value="%{#session.HYPERLINK_COLOR}"/> !important;}
.boxbackgroundcolor {background-color: #<s:property value="%{#session.BOX_BACKGROUND_COLOR}"/> !important;}
.topbarcolor {background-color: #<s:property value="%{#session.TOP_BAR_COLOR}"/> !important;}
ul.nav-tabs>li>a {color: #<s:property value="%{#session.TAB_TEXT_COLOR}"/> !important; background-color: #<s:property value="%{#session.TAB_BACKGROUND_COLOR}"/>; }
ul.nav-tabs>li>a:hover {color: #<s:property value="%{#session.TAB_TEXT_COLOR}"/> !important; background-color: #<s:property value="%{#session.TAB_BACKGROUND_COLOR}"/>; }
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus {color: #<s:property value="%{#session.ACTIVE_TAB_TEXT_COLOR}"/> !important;	background-color: #<s:property value="%{#session.ACTIVE_TAB_COLOR}"/>;}
.buttoncolor {background-color: #<s:property value="%{#session.BUTTON_BACKGROUND_COLOR}"/> !important; border: none; color: #<s:property value="%{#session.BUTTON_TEXT_COLOR}"/> !important; }
.bordercolor, ul, .panel-heading {border-color: #<s:property value="%{#session.BORDER_COLOR}"/> !important; }
</style>
<script>
$(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
</script>
 <script type="text/javascript">
 <%--$(document).ready(function() {
	   $('#liCreditCard').click(function(event){
		   calculateSurchargeAmount();
	   });
	   
	   $('#liDebitCard').click(function(event){
		   calculateSurchargeAmount();
	});

	function calculateSurchargeAmount(){
		//var token  = document.getElementsByName("token")[0].value;
		/* String payId = ((String) session.getAttribute(FieldType.PAY_ID.getName())
		String amount = ESAPI.encoder().encodeForHTML((String) session.getAttribute(FieldType.CUST_NAME
					.getName() */

		 /* currencyCodeAlpha = Currency.getAlphabaticCode(currencyCodeNumeric);
		formattedAmount = ESAPI.encoder().encodeForHTML(
				Amount.toDecimal(amount, currencyCodeNumeric));  */
		var amount =  '10';
		var paymentType = 'Credit Card';
		var payId = '1701191819111000';
		$.ajax({
			type: "POST",
			url:"surchargeAmount",
			data:{"payId":payId, "paymentType":paymentType, "amount":amount,},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				//TODO....clean values......using script to avoid page refresh
				window.location.reload();
		    },
			error:function(data){
				alert("Network error, surcharge details not fetched");
			}
		});
	}
});
 --%>
function checkActiveDiv(){
	var checkEX= "<s:property value="%{#session.EXPRESS_PAY_FLAG}"/>"
	var checkEXactive= "<s:property value="%{#session.TOKEN}"/>"
	if (checkEX == "true"){
		if (checkEXactive == 'NA'){
			document.getElementById('tabCreditCard').className += " in active";
	}
		else{
			document.getElementById('tabExpressPay').className += " in active";
			}
	}
	else{
			document.getElementById('tabCreditCard').className += " in active";
	}
}

function popup(){
var trasaction = "<s:property value='trasactionFaileFlag'/>"
if (trasaction !=false) {
	alert("Your Trasaction has been Failed. Please retry")
	}
}

</script>
<script type="text/javascript">
        var timeout = '<%=15 * 60 * 1000%>';
	
	var timer = setInterval(function() {
		timeout -= 1000;
		window.status = time(timeout);
		if (timeout == 0) {
			clearInterval(timer);
			alert('Your session has been expired !')
		}
	}, 1000);

	function two(x) {
		return ((x > 9) ? "" : "0") + x
	}

	function time(ms) {
		var t = '';
		var sec = Math.floor(ms / 1000);
		ms = ms % 1000;

		var min = Math.floor(sec / 60);
		sec = sec % 60;
		t = two(sec);

		var hr = Math.floor(min / 60);
		min = min % 60;
		t = two(min) + ":" + t;

		document.getElementById("lblSessionTime").innerText = "Your Session will expire in "
				+ t;
		return "You session will timeout in " + t + " minutes.";
	}
</script>
</head>
<body class="background" onload="checkActiveDiv();getCreditForm();getDebitForm();getExForm();popup();setInterval(1);" >
	<div class="center-div">
		<div class="container textstyle textcolor">
			<div class="text-right">
				<s:label name="lblSessionTime" id="lblSessionTime"></s:label>
			</div>

			<div class="panel panel-default bordercolor">

				<div class="panel-heading custom_class topbarcolor">
					<h1 class="panel-title pull-left" style="padding-top: 6px; color:#ffffff !important;">
						<%
						Logger logger = Logger.getLogger("Payment Page");
						String currencyCodeAlpha="";
						String formattedAmount="";
							try {
								/* String currencyCodeNumeric = (String) session
										.getAttribute(FieldType.CURRENCY_CODE.getName());
								String amount = (String) session.getAttribute(FieldType.AMOUNT
										.getName());

								 currencyCodeAlpha = Currency.getAlphabaticCode(currencyCodeNumeric); */
								 DecimalFormat df = new DecimalFormat("0.00");
								 
								formattedAmount = df.format(session.getAttribute(FieldType.SURCHARGE_CC_AMOUNT
										.getName()));
							} catch (NullPointerException nPException) {
								response.sendRedirect("sessionTimeout");
							} catch (Exception exception){
								logger.error("Exception on payment page " + exception);
								response.sendRedirect("error");
							}
						%>
						<b>Amount Payable  <%=currencyCodeAlpha%> <%=formattedAmount%></b>
					</h1>

					<div class="pull-right">

						<form action="surchargelocale">

							<s:select list="@com.mmadpay.pg.core.LocaleLanguageType@values()" name="request_locale"
								value="%{locale.toString()}" listKey="code" listValue="name"
								class="form-control1 pull-right" onchange="this.form.submit()"></s:select>
						</form>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="panel-body boxbackgroundcolor" style="margin: 0px; padding: 6px;">

					<ul class="nav nav-tabs responsive" id="myTab"
						style="background-color: #ededed; border-radius: 4px;">
						
							  <s:if test="%{#session.EXPRESS_PAY_FLAG == true}">
				          <s:if test="%{#session.TOKEN == 'NA'}"></s:if>
						<s:else>
								<li class="active" id ="liExpresspay"><a href="#tabExpressPay" data-toggle="tab" style="color: !important;"><s:property value="getText('ExpressPay')" /></a></li>
								</s:else>
							</s:if>
							<s:iterator value="#session.PAYMENT_TYPE_MOP">
							<s:if test="%{key=='CC'}">
							 <s:if test="%{#session.EXPRESS_PAY_FLAG == true}">
							 <s:if test="%{#session.TOKEN == 'NA'}">
							  		 <li id="liCreditCard"  class="active"><a  href="#tabCreditCard" data-toggle="tab" ><s:property value="getText('CreditCard')" /></a></s:if>
							  		  <s:else><li  id="liCreditCard"><a  href="#tabCreditCard" data-toggle="tab" ><s:property value="getText('CreditCard')" /></a></s:else>
						 </s:if>
						 <s:else><li  id="liCreditCard" class="active"><a  href="#tabCreditCard" data-toggle="tab" ><s:property value="getText('CreditCard')" /></a></s:else>

							</s:if>
							<s:if test="%{key=='DC'}">
								<li id="liDebitCard" ><a href="#tabDebitCard" data-toggle="tab" style="color: !important;"><s:property value="getText('DebitCard')" /></a></li>
							</s:if>
							<s:if test="%{key=='NB'}">
								<li><a href="#tabNetbanking" data-toggle="tab" style="color: !important;"><s:property value="getText('NetBanking')" /></a></li>
							</s:if>
							<s:if test="%{key=='WL'}">
								<li><a href="#tabWallet" data-toggle="tab" style="color: !important;"><s:property value="getText('Wallet')" /></a></li>
							</s:if>							
						</s:iterator>
						<li><a href="#tabEMI" data-toggle="tab" style="color: !important;">Credit Cards (EMI)</a></li>
					</ul>

					<div class="tab-content active responsive">
					<div class="tab-pane fade" id="tabEMI">
							<h3>Credit Card EMI</h3>
							<div class="row">
											<div class="form-group col-md-8 col-sm-12 col-xs-12">
<select class="form-control">
<option>Select type of card</option>
<option>Axis Bank</option>
<option>HDFC Bank</option>
</select>
											</div>		
											<%-- <div class="form-group col-md-8 col-sm-12 col-xs-12"><span class="glyphicon glyphicon-ok"></span> Select a plan</div> --%>
											
											<div class="table-responsive">
											
  <table class="table-hover product-spec" style="width:480px; margin-left:15px; border-collapse: collapse; font-size:13px;">
     <thead>   
      <tr>
        <th height="35" width="20%">EMI Tenure</th>
        <th width="25%">Bank Interest Rate</th>
        <th width="28%">Monthly Installments</th>
        <th width="28%">Interest Paid to bank</th>
      </tr>
    </thead>
    <tbody>
      <tr title="Over 3 Months, you pay Rs.221.00 as total interest to the bank on the billed amount of Rs. 10.999.00" data-toggle="tooltip" data-placement="right">
        <td align="left">
        <div class="mk-trc" data-style="radio" data-radius="true" style="margin:0px; margin-left:-10px; padding:0px;">
															<input onclick="validateCredit(this);enablePayButton(this);checkCard(this);"
																name="mopType" type="radio" id="rdbVisa11" value="VI" /> <label for="rdbVisa"><i></i>
																3 Monhts</label>
														</div>
        </td>
        <td>12%</td>
        <td>Rs. 3740.00</td>
        <td>Rs. 221.00</td>
      </tr>
      <tr>
        <td align="left">
        <div class="mk-trc" data-style="radio" data-radius="true" style="margin:0px; margin-left:-10px; padding:0px;">
															<input onclick="validateCredit(this);enablePayButton(this);checkCard(this);"
																name="mopType" type="radio" id="rdbVisa1" value="VI" /> <label for="rdbVisa1"><i></i>
																6 Monhts</label>
														</div>
        </td>
        <td>12%</td>
        <td>Rs. 3740.00</td>
        <td>Rs. 221.00</td>
      </tr>
      <tr>
        <td align="left">
        <div class="mk-trc" data-style="radio" data-radius="true" style="margin:0px; margin-left:-10px; padding:0px;">
															<input onclick="validateCredit(this);enablePayButton(this);checkCard(this);"
																name="mopType" type="radio" id="rdbVisa2" value="VI" /> <label for="rdbVisa2"><i></i>
																9 Monhts</label>
														</div>
        </td>
        <td>12%</td>
        <td>Rs. 3740.00</td>
        <td>Rs. 221.00</td>
      </tr>
      <tr>
        <td align="left">
        <div class="mk-trc" data-style="radio" data-radius="true" style="margin:0px; margin-left:-10px; padding:0px;">
															<input onclick="validateCredit(this);enablePayButton(this);checkCard(this);"
																name="mopType" type="radio" id="rdbVisa3" value="VI" /> <label for="rdbVisa3"><i></i>
																12 Monhts</label>
														</div>
        </td>
        <td>12%</td>
        <td>Rs. 3740.00</td>
        <td>Rs. 221.00</td>
      </tr>
      <tr>
        <td align="left">
        <div class="mk-trc" data-style="radio" data-radius="true" style="margin:0px; margin-left:-10px; padding:0px;">
															<input onclick="validateCredit(this);enablePayButton(this);checkCard(this);"
																name="mopType" type="radio" id="rdbVisa4" value="VI" /> <label for="rdbVisa4"><i></i>
																18 Monhts</label>
														</div>
        </td>
        <td>12%</td>
        <td>Rs. 3740.00</td>
        <td>Rs. 221.00</td>
      </tr>
      <tr>
        <td align="left">
        <div class="mk-trc" data-style="radio" data-radius="true" style="margin:0px; margin-left:-10px; padding:0px;">
															<input onclick="validateCredit(this);enablePayButton(this);checkCard(this);"
																name="mopType" type="radio" id="rdbVisa5" value="VI" /> <label for="rdbVisa5"><i></i>
																24 Monhts</label>
														</div>
        </td>
        <td>12%</td>
        <td>Rs. 3740.00</td>
        <td>Rs. 221.00</td>
      </tr>
    </tbody>
  </table>
</div>
										</div>
										<div class="clear">&nbsp;</div>										
							<div class="row">
								<div class="form-group col-md-4 col-sm-12 col-xs-12">
									<s:submit key="Pay" name="dcSubmit" cssClass="btn btn-block sharp btn-warning btn-md"
										style="background-color:;" theme="simple" formtarget="_parent" id="dcSubmit"></s:submit>
								</div>
							</div>
							<div class="row">
							<div class="form-group col-md-10 col-sm-12 col-xs-12">
							<a href="#">Return to merchant's website</a></div>
						</div>
						</div>
  						<s:if test="%{#session.EXPRESS_PAY_FLAG == true}">
						  <s:if test="%{#session.TOKEN == 'NA'}"></s:if>
							 <s:else> 
						<div id="tabExpressPay" class="tab-pane fade" >
       <h3><s:property value="getText('ExpressPay')" /></h3>
        <s:form autocomplete="off" name="exCard" method="post" action="pay" id="exCard"> 
		<input type="hidden" name="paymentType" value="EX" />
		 <!-- <a href="#tabCreditCard" class="text-right">Use another card</a>  -->
       <s:iterator value="#session.TOKEN" status="incrementer">       
       <div class="hoverDiv">
        <div class="row">
         <div class="mk-trc pull-left" data-style="radio" data-radius="true" style="margin: 7px 0px; display:inline-block;">
          <input type="radio" name="tokenId"  id="tokenId<s:property value="key" />" onclick="handleClick(this);"  value="<s:property value="key" />" >
              
   <s:if test="%{value.mopType=='VI'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/visa_card.png" class="cardslogo"></label>
      </s:if>
      <s:if test="%{value.mopType=='AX'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/amex_card.png" class="cardslogo"></label>
      </s:if>
      <s:if test="%{value.mopType=='MC'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/master_card.png" class="cardslogo"></label>
      </s:if>
      <s:if test="%{value.mopType=='MS'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/mastero_card.png" class="cardslogo"></label>
      </s:if>
      <s:if test="%{value.mopType=='DN'}">
      <label for="tokenId<s:property value="key" />"><i></i> <img src="../image/diner_card.png" class="cardslogo"></label>
      </s:if>        
   </div>
   <div class="col-md-7 col-xs-6" style="padding-right:10px; ">
   <small class="pull-right text-muted">
   <s:textfield  name="cardNumber" autocomplete="off" id="exCardNumber%{key}" cssClass="form-control transparent-input" value="%{value.cardMask}" theme="simple"  readonly="true" />
   <small class="text-muted"><s:if test="%{value.paymentType=='CC'}">Credit Card</s:if></small>
     <small class="text-muted"> <s:else>Debit Card</s:else> | <s:property value="%{value.cardIssuerBank}" /></small>
      </small> </div>
         <div class="col-md-1 col-xs-10 col-md-push-1" style="margin-top:9px;padding-left:0px; ">
         <span class="pull-right">
         <s:textfield type="password"  name="cvvNumber" id="cvvNumber%{key}" maxlength="4" autocomplete="off" onkeypress="return isNumberKey(event);return disableEnterPress(event)" onkeyup="enableExButton(this);" style="width: 40px; height: 25px; display: inline;" class="form-control" placeholder="CVV" disabled="true" />         
         </span></div>
         <div class="col-md-1 col-xs-1" style="margin-top:11px; padding-left:20px;"><button id="delbutton<s:property value="key" />"  type="button" class="glyphicon glyphicon-trash btn-link text-warning" disabled="disabled" data-toggle="modal" data-target="#myModal" style="color:#e9877c;"></button>
        
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="width:300px;  max-width:90%;">
      <div class="modal-content">
        <div class="modal-body" style="margin:0px; padding:0px;">
          <h3 class="text-center text-info" >Are you sure <i class="fa fa-question-circle fa-lg"></i></h3>

        </div>
        <div class="modal-footer" style="text-align:center;">
          <div class="row"><div class="col-md-6 col-xs-6"><s:submit name="deleteButton" action="deletecard" class="btn btn-danger btn-block text-center" id="deleteButton%{key}" theme="simple" value="Yes"></s:submit></div>
          <div class="col-md-6 col-xs-6"><s:submit type="button" class="btn btn-success btn-block text-center" data-dismiss="modal" theme="simple" value="Cancel"></s:submit></div>
        </div>
        </div>
      </div>
    </div>
  </div></div>
        </div>
       </div>
      </s:iterator>
          <div class="row">

               <div class="col-md-4 col-sm-12 col-xs-12">
               <s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
                <s:submit key="Pay" name="exSubmit" cssClass="btn-orange disabled"   formtarget="_parent" disabled="true" id="exSubmit"></s:submit>
                <s:submit value="Return to merchant" action="txncancel" name="exCancelButton" formtarget="_parent" cssClass="btn-link" theme="simple" id="exCancelButton"></s:submit>
                </s:if>
                <s:else><s:submit key="Pay" name="exSubmit" cssClass="btn-orange disabled"  disabled="true"  id="exSubmit"></s:submit>
                <s:submit value="Return to merchant" action="txncancel" name="exCancelButton" cssClass="btn-link" theme="simple" id="exCancelButton"></s:submit>                
               </s:else><br>
               </div>
              </div>
              </s:form>
      </div>
      </s:else></s:if>
						<s:iterator value="#session.PAYMENT_TYPE_MOP">
							<s:if test="%{key=='CC'}">
								<div  id="tabCreditCard" class="tab-pane fade" >
									<s:form autocomplete="off" name="creditcard-form" method="post" action="pay" id="creditCard" onsubmit="return submitCCform();">
									<input type="hidden" name="paymentType" value="CC" />
									<h3><s:property value="getText('CreditCard')" /></h3>
									<div class="col-md-6 col-xs-10 text-left">
											<s:set name="card typemsg" value="getText('SelectCardType')" />
											<s:hidden id="cardTypeMsg" value="%{card typemsg}" />
											<small id="radioValidate" class="text-danger"></small>
										</div>
										<div class="row">
											<div class="col-md-10">
												<s:iterator value="value" status="itStatus">
													<s:if test="%{code == 'VI'}">
														<div class="mk-trc" data-style="radio" data-radius="true">
															<input onclick="validateCredit(this);enablePayButton(this);checkCard(this);"
																name="mopType" type="radio" id="rdbVisa" value="VI" /> <label for="rdbVisa"><i></i>
																<img src="../image/visa_card.png" class="cardslogo"></label>
														</div>
													</s:if>
													<s:if test="%{code == 'MC'}">
														<div class="mk-trc" data-style="radio" data-radius="true">
															<input name="mopType" type="radio" id="rdbMaster"
																onclick="validateCredit(this);enablePayButton(this);checkCard(this);" value="MC">
															<label for="rdbMaster"><i></i> <img src="../image/master_card.png"
																class="cardslogo "></label>
														</div>
													</s:if>
													<s:if test="%{code == 'AX'}">
														<div class="mk-trc" data-style="radio" data-radius="true">
															<input name="mopType" id="rdbAmex" type="radio" value="AX"
																onclick="validateCredit(this);enablePayButton(this);checkCard(this);"> <label
																for="rdbAmex"><i></i> <img src="../image/amex_card.png" class="cardslogo"></label>
														</div>
													</s:if>
													<s:if test="%{code == 'DN'}">
														<div class="mk-trc" data-style="radio" data-radius="true">
															<input name="mopType" id="rdbDiners" type="radio" value="DN"
																onchange="validateCredit(this);enablePayButton(this);checkCard(this);"> <label
																for="rdbDiners"><i></i> <img src="../image/diner_card.png" class="cardslogo" /></label>
														</div>
													</s:if>
													<s:if test="%{code == 'EZ'}">
														<div class="mk-trc" data-style="radio" data-radius="true">
															<input name="mopType" id="rdbEzee" type="radio" value="EZ"
																onclick="validateCredit(this);enablePayButton(this);"> <label for="rdbEzee"><i></i>
																<img src="../image/ezeclick_card.png" class="cardslogo"></label>
														</div>
													</s:if>
												</s:iterator>
											</div>
										</div>
										<div id="divCardType">
												<s:set name="valid cardnumber" value="getText('EntervalidCardnumber')" />
												<s:hidden id="validCardDetail" value="%{valid cardnumber}" />
												<div><small id="demo" class="text-danger"></small></div>
												<s:set name="correct cardtype" value="getText('SelectCorrectCardType')" />
												<s:hidden id="correctCardType" value="%{correct cardtype}" />
												<small id="demo1" class="text-danger"></small>
											</div>
										<div class="row">											
											<s:set name="cardNumberTextCC" value="getText('CardNumber')" />
											<s:hidden id="cardPlaceHolderCC" value="%{cardNumberTextCC}" />
											<div class="form-group col-md-10" id="divCardNumber">
												<div class="cardNumber__wrap">
												 <input type="text" id="cardNumber1" name="cardNumber" style="display: none" /> <input
														id="cardNumber" name="cardNumber" class="cardNumber form-control"
														onblur="validateCredit(this);checkLuhn(this); "
														onkeyup="enablePayButton(this);return isNumberKeyCard(this);" autocomplete="off" />
													<div class="card" aria-hidden="true"></div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-8 col-xs-10">
												<s:set name="valid expiry" value="getText('Entervalidexpirydate')" />
												<s:hidden id="validExpiryDate" value="%{valid expiry}" />
												<small id="chkExpiry" class="text-danger"></small>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-3 col-sm-4 col-xs-6" id="divExpiryMonth">

												<s:select headerKey="" headerValue="%{getText('Month')}" cssClass="form-control"
													list="#{'01':'01','02':'02','03':'03','04':'04','05':'05','06':'06','07':'07','08':'08','09':'09','10':'10','11':'11','12':'12'}"
													name="ExpiryMonth" id="ccExpiryMonth" value="0"
													onchange="CheckExpiry();enablePayButton(this);" />
											</div>
											<div class="form-group col-md-3 col-sm-4 col-xs-6" id="divExpiryYear">

												<s:select headerKey="" headerValue="%{getText('YEAR')}" cssClass="form-control"
													list="#{'2016':'2016','2017':'2017','2018':'2018','2019':'2019','2020':'2020','2021':'2021','2022':'2022','2023':'2023','2024':'2024','2025':'2025','2026':'2026','2027':'2027','2028':'2028','2029':'2029','2030':'2030','2031':'2031','2032':'2032','2033':'2033','2034':'2034','2035':'2035','2036':'2036','2037':'2037','2038':'2038','2039':'2039','2040':'2040','2041':'2041','2042':'2042','2043':'2043','2044':'2044','2045':'2045','2046':'2046','2047':'2047','2048':'2048','2049':'2049'}"
													name="ExpiryYear" id="ccExpiryYear" value="0"
													onchange="CheckExpiry();enablePayButton(this);" />
											</div>
											<s:set name="CVV" value="getText('CVV')" />
											<div class="form-group col-md-2 col-sm-2 col-xs-6" id="divPassword">
												<input type='password' style='display: none' />
												<s:textfield type="password" name="cvvNumber" id="cvvNumber" autocomplete="off"
													onkeypress="return isNumberKey(event)" onkeyup="enablePayButton(this);"
													cssClass="form-control margintop0px mozilla-firefox" placeholder="CVV" />
											</div>
											<div class="form-group col-md-2 col-sm-2 col-xs-6" id="divCvv">
												<s:set name="cvv textimage" value="getText('CVVlen3image')" />
												<s:set name="cvv textimageAX" value="getText('CVVlen4image')" />
												<s:hidden id="cvvTextImage" value="%{cvv textimage}" />
												<s:hidden id="cvvTextImageAX" value="%{cvv textimageAX}" />
												<span onMouseOut="Hide()" onMouseOver="Show()" onMouseMove="Show()" class="cvvcard">&nbsp;</span>
												<div id="cvvtext"></div>
											</div>
										</div>
										<s:set name="NameonCard" value="getText('NameonCard')" />
										<div class="row" id="divName">
											<div class="form-group col-md-10">
												<s:textfield id="cardName" name="cardName" type="text" cssClass="form-control"
													autocomplete="off" onkeypress="noSpace(event,this);return isCharacterKey(event);"
													onkeyup="enablePayButton(this);" placeholder="%{NameonCard}" />
											</div>
										</div>
										<div class="row" id="divSaveCard">
											<s:if test="%{#session.EXPRESS_PAY_FLAG == true}">
												<div class="form-group col-md-1 col-xs-1">
													<s:checkbox name="cardsaveflag" checked="checked" id="cardsaveflag1" />
												</div>
												<div class="form-group col-md-11 col-xs-10 text-left" style="padding-left: 0px;">
													<label for="cardsaveflag1" class="text-muted"><s:property
															value="getText('SaveCard')" /></label>
												</div>
											</s:if>
										</div>
										<div id="whitetransbgsize"><div class="row">
											<div class="form-group col-md-4 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="Pay" name="ccSubmit" cssClass="btn-orange disabled"
														style="background-color:;" theme="simple" formtarget="_parent" id="ccSubmit"></s:submit>
												</s:if>
												<s:else>
													<s:submit key="Pay" name="ccSubmit" cssClass="btn-orange disabled"
														style="background-color:;" theme="simple" id="ccSubmit"></s:submit>
												</s:else>
											</div>
										</div></div>
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit value="Return to merchant" formtarget="_parent" action="txncancel"
														name="ccCancelButton" theme="simple" cssClass="btn-link" id="ccCancelButton"></s:submit>
												</s:if>
												<s:else>
													<s:submit value="Return to merchant" action="txncancel" name="ccCancelButton"
														theme="simple" cssClass="btn-link" id="ccCancelButton"></s:submit>
												</s:else>
											</div>
										</div>

									</s:form>
								</div>
							</s:if>
							<s:if test="%{key=='DC'}">
								<div class="tab-pane fade" id="tabDebitCard">
									<s:form autocomplete="off" name="debitcard-form" method="post" action="pay" id="debitCard"
										onsubmit="return submitDCform();">
										<input type="hidden" name="paymentType" value="DC" />
										<h3><s:property value="getText('DebitCard')" /></h3>
										<div class="col-md-6 col-xs-10 text-left">
											<s:set name="card typemsgdc" value="getText('SelectCardType')" />
											<s:hidden id="cardTypeMsgDC" value="%{card typemsgdc}" />
											<small id="radioValidateDebit"
												class="text-danger"></small>
										</div>
										<div class="row">											
											<div class="col-md-10 col-xs-12">
												<s:iterator value="value" status="itStatus">
													<s:if test="%{code == 'VI'}">
														<div class="mk-trc" data-style="radio" data-radius="true">
															<input onclick="validateDebit(this);enablePayButtonDebit(this);checkCardDebit(this);"
																name="mopType" type="radio" id="rdbVisaD" value="VI" /> <label for="rdbVisaD"><i></i>
																<img src="../image/visa_card.png" class="cardslogo"></label>
														</div>
													</s:if>
													<s:if test="%{code == 'MC'}">
														<div class="mk-trc" data-style="radio" data-radius="true">
															<input onclick="validateDebit(this);enablePayButtonDebit(this);checkCardDebit(this);"
																name="mopType" type="radio" id="rdbMasterD" value="MC" /> <label for="rdbMasterD"><i></i>
																<img src="../image/master_card.png" class="cardslogo"></label>
														</div>
													</s:if>
													<s:if test="%{code == 'MS'}">
														<div class="mk-trc" data-style="radio" data-radius="true">
															<input name="mopType"
																onclick="MaestroNote();checkCardDebit(this);validateDebit(this);" id="rdbMasteroD"
																type="radio" value="MS" /> <label for="rdbMasteroD"><i></i> <img
																src="../image/mastero_card.png" class="cardslogo"></label>
														</div>
													</s:if>
													<s:if test="%{code == 'RU'}">
														<div class="mk-trc" data-style="radio" data-radius="true">
															<input name="mopType"
																onclick="validateDebit(this);enablePayButtonDebit(this);checkCardDebit(this);"
																id="rdbRupayD" type="radio" value="MS" /> <label for="rdbRupayD"><i></i> <img
																src="../image/rupay_card.png" class="cardslogo"></label>
														</div>
													</s:if>

												</s:iterator>
											</div>
										</div>
										<div><s:set name="valid maestro" value="getText('MaestroCardMsg')" />
										<div class="inputfield">
										<s:hidden id="maestroNoteD" value="%{valid maestro}" />
										</div>
										<small id="maestroNoteDspan" class="text-info"></small>
										<s:set name="valid cardnumberD" value="getText('EntervalidCardnumber')" />
										<s:hidden id="validCardDetailD" value="%{valid cardnumberD}" />
										<div><small id="demoD" class="text-danger"></small></div>
										<s:set name="correct cardtypeD" value="getText('EntervalidCardnumber')" />
										<s:hidden id="correctCardTypeD" value="%{correct cardtypeD}" />
										<small id="demoD1" class="text-danger"></small>
										</div>
										<div class="row">										
											<div class="form-group col-md-10">												
												<div class="cardNumber__wrap">
													<input type="text" id="dccardNumber1" name="cardNumber" style="display: none" /> <input
														id="dccardNumber" name="cardNumber" class="cardNumber form-control"
														onblur="validateDebit(this);checkLuhn(this);"
														onkeyup="enablePayButtonDebit(this);return isNumberKeyCardDebit(this);"
														autocomplete="off" />
													<div class="card" aria-hidden="true"></div>
												</div>
											</div>
										</div>
										<div class="row">
															<div class="col-md-8 col-xs-10">
															<s:set name="valid expiry" value="getText('Entervalidexpirydate')" />
																<s:hidden id="validExpiryDateD" value="%{valid expiry}"/>
																<span id="chkExpiryDD" class="redsmalltext text-left help-block"></span>
															</div>
													</div>
										<div class="row">
											<div class="form-group col-md-3 col-sm-4 col-xs-6">
												<s:select headerKey="" headerValue="%{getText('Month')}" cssClass="form-control"
													list="#{'01':'01','02':'02','03':'03','04':'04','05':'05','06':'06','07':'07','08':'08','09':'09','10':'10','11':'11','12':'12'}"
													name="ExpiryMonth" id="dcExpiryMonth" value="0"
													onchange="CheckExpiryDC();enablePayButtonDebit(this);" />
											</div>
											<div class="form-group col-md-3 col-sm-4 col-xs-6">
												<s:select headerKey="" headerValue="%{getText('YEAR')}" cssClass="form-control"
													list="#{'2016':'2016','2017':'2017','2018':'2018','2019':'2019','2020':'2020','2021':'2021','2022':'2022','2023':'2023','2024':'2024','2025':'2025','2026':'2026','2027':'2027','2028':'2028','2029':'2029','2030':'2030','2031':'2031','2032':'2032','2033':'2033','2034':'2034','2035':'2035','2036':'2036','2037':'2037','2038':'2038','2039':'2039','2040':'2040','2041':'2041','2042':'2042','2043':'2043','2044':'2044','2045':'2045','2046':'2046','2047':'2047','2048':'2048','2049':'2049','2050':'2050','2051':'2051','2052':'2052','2053':'2053','2054':'2054','2055':'2055','2056':'2056','2057':'2057','2058':'2058','2059':'2059','2060':'2060','2061':'2061','2062':'2062','2063':'2063','2064':'2064','2065':'2065','2066':'2066','2067':'2067','2068':'2068','2069':'2069'}"
													name="ExpiryYear" id="dcExpiryYear" value="0"
													onchange="CheckExpiryDC();enablePayButtonDebit(this);" />
											</div>
											<div class="form-group col-md-2 col-sm-2 col-xs-6">
												<input type='password' style='display: none' />
												<s:textfield type="password" maxlength="3" name="cvvNumber" id="dccvvNumber"
													autocomplete="off" onkeypress="return isNumberKey(event)"
													onkeyup="enablePayButtonDebit(this);" cssClass="form-control" placeholder="CVV" />
											</div>
											<div class="form-group col-md-2 col-sm-2 col-xs-6">
												<s:set name="cvv textimageDD" value="getText('CVVlen3image')" />
												<s:hidden id="cvvTextImageDD" value="%{cvv textimageDD}" />
												<span onMouseOut="Hide()" onMouseOver="Show()" onMouseMove="Show()"
													class="cvvcard">&nbsp;</span>
												<div id="cvvtextDD"></div>
											</div>
										</div>
										<s:set name="NameonCard" value="getText('NameonCard')" />
										<div class="row">
											<div class="form-group col-md-10">
												<s:textfield id="dccardName" name="cardName" type="text" cssClass="form-control"
													autocomplete="off" onkeypress="noSpace(event,this);return isCharacterKey(event)"
													onkeyup="enablePayButtonDebit(this);" placeholder="%{NameonCard}" />
											</div>
										</div>
										<s:if test="%{#session.EXPRESS_PAY_FLAG == true}">
											<div class="row">
												<div class="form-group col-md-1 col-xs-1">
													<s:checkbox name="cardsaveflag" checked="checked" id="cardsaveflag" />
												</div>
												<div class="form-group col-md-11 col-xs-10 text-left" style="padding-left: 0px;">
													<label for="cardsaveflag" class="text-muted"><s:property
															value="getText('SaveCard')" /></label>
												</div>
											</div>
										</s:if>
										<div class="row">
											<div class="form-group col-md-4 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="Pay" name="dcSubmit" cssClass="btn-orange disabled" theme="simple"
														formtarget="_parent" id="dcSubmit"></s:submit>
												</s:if>
												<s:else>
													<s:submit key="Pay" name="dcSubmit" cssClass="btn-orange disabled" theme="simple"
														id="dcSubmit"></s:submit>
												</s:else>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit value="Return to merchant" formtarget="_parent" action="txncancel"
														name="ccCancelButton" cssClass="btn-link" id="dcCancelButton" theme="simple"></s:submit>
												</s:if>
												<s:else>
													<s:submit value="Return to merchant" action="txncancel" name="ccCancelButton"
														cssClass="btn-link" id="dcCancelButton" theme="simple"></s:submit>
												</s:else>
											</div>
										</div>
									</s:form>
								</div>

							</s:if>
							<s:if test="%{key=='NB'}">
								<div class="tab-pane fade" id="tabNetbanking" >
									<h3><s:property value="getText('NetBanking')" /></h3>
									<s:form autocomplete="off" name="creditcard-form" method="post" action="pay" id="card"
										onsubmit="return submitNBform();">
										<input type="hidden" name="paymentType" value="NB" />
										<div class="text-muted">
											<small>Note:<br /> On clicking "Pay", you will be redirected to your
												bank's site for authorization.<br /> Please do not refresh page or click back button on
												browser while the authorization <br />process is being undertaken.
											</small>
										</div>
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="form-group col-md-12 col-xs-12">
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" id="sbi"
														onclick="onChange('sbi');enableNetbankingButton()" type="radio" value="SBI"> <label
														for="sbi"><i></i> <img src="../image/sbi-logo.png" class=""></label>
												</div>
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" id="hdfc"
														onclick="onChange('hdfc');enableNetbankingButton()" type="radio" value="HDFC">
													<label for="hdfc"><i></i> <img src="../image/hdfc_new.png" class=""></label>
												</div>
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" id="icici"
														onclick="onChange('icici');enableNetbankingButton()" type="radio" value="ICICI">
													<label for="icici"><i></i> <img src="../image/icici_new.png" class=""></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" type="radio" value="AXIS" id="axis"
														onclick="onChange('axis');enableNetbankingButton()"> <label for="axis"><i></i>
														<img src="../image/axis_new.png" class=""></label>
												</div>
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input name="netBankingType" type="radio" value="CITI" id="citi"
														onclick="onChange('citi');enableNetbankingButton()"> <label for="citi"><i></i>
														<img src="../image/citibank-netbnk.png" class=""></label>
												</div>
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input id="kotak" onclick="onChange('kotak');enableNetbankingButton()"
														name="netBankingType" type="radio" value="KOTAK"> <label for="kotak"><i></i>
														<img src="../image/kotak_new.png" class=""></label>
												</div>
											</div>
										</div>
										<hr />
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">

												<s:select class="form-control" list="value" name="bankName" listKey="code"
													onclick="onChangeNetbankDropdwn(this.value);;enableNetbankingButton()" listValue="name"
													id="ddlNetbanking" data-size="5" headerKey="-1" headerValue="Select Bank" />
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit key="Pay" value="Pay" name="nbSubmit"
														cssClass="btn-orange disabled" style="background-color:;" theme="simple"
														formtarget="_parent" id="nbSubmit"></s:submit>
												</s:if>
												<s:else>
													<s:submit key="Pay" value="Pay" name="nbSubmit"
														cssClass="btn-orange disabled" style="background-color:;" theme="simple" id="nbSubmit"></s:submit>
												</s:else>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit value="Return to merchant" formtarget="_parent" action="txncancel"
														name="netCancelButton" cssClass="btn-link" theme="simple" id="netCancelButton"></s:submit>
												</s:if>
												<s:else>
													<s:submit value="Return to merchant" action="txncancel"
														name="netCancelButton" theme="simple" cssClass="btn-link" id="netCancelButton"></s:submit>
												</s:else>
											</div>
										</div>
									</s:form>
								</div>
							</s:if>
							<s:if test="%{key=='WL'}">
								<div class="tab-pane fade" id="tabWallet" >
									<h3><s:property value="getText('Wallet')" /></h3>
									<s:form autocomplete="off" name="wallet-form" method="post" action="pay" id="">
										<input type="hidden" name="paymentType" value="WL" />
										<div class="clearfix">&nbsp;</div>
										<div class="row">
											<div class="col-md-12 col-xs-12">
												<div class="mk-trc" data-style="radio" data-radius="true">
													<input id="mobikwikWallet" type="radio" name="mopType" onchange="enableWalletButton()"
														value="MWL" /> <label for="mobikwikWallet"><i></i> <img
														src="../image/mobikwik.png" width="100" /></label>
												</div>
												<!-- <div class="mk-trc" data-style="radio" data-radius="true">
													<input id="paytmWallet" name="mopType" type="radio" onchange="enableWalletButton()"
														value="PPI" /> <label for="paytmWallet" class="css-label radGroup1"><i></i> <img
														src="../image/paytm-logo.png" width="80" /></label>
												</div> -->
											</div>
										</div>
										<hr />
										<div class="row">
											<div class="form-group col-md-7 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit type="submit" name="button" id="walletSubmit" key="Pay" disabled="true"
														class="btn-orange disabled" formtarget="_parent"></s:submit>
												</s:if>
												<s:else>
													<s:submit type="submit" name="button" id="walletSubmit" key="Pay" disabled="true"
														class="btn-orange disabled"></s:submit>
												</s:else>

											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-10 col-sm-12 col-xs-12">
												<s:if test="%{#session.IFRAME_PAYMENT_PAGE == true}">
													<s:submit value="Return to merchant" action="txncancel" formtarget="_parent"
														name="wtCancelButton" cssClass="btn-link" id="wtCancelButton" theme="simple">
													</s:submit>
												</s:if>
												<s:else>
													<s:submit value="Return to merchant" action="txncancel" name="wtCancelButton"
														cssClass="btn-link" id="wtCancelButton" theme="simple">
													</s:submit>
												</s:else>
											</div>
										</div>
									</s:form>
								</div>
							</s:if>
						</s:iterator>
					</div>


					<ul class="nav pull-right formobiles">
						<li class="text-center">
						<%-- <img src="../image/userlogo/<%=(String) session.getAttribute(FieldType.PAY_ID.getName())%>.png" width="180px" /> --%>
							<table class="table table-bordered" style="background-color: #fbfbfb; font-size: 11px;">
								<tr>
									<td width="40%" align="left" valign="middle"><s:property value="getText('Buyer')" />
									</td>
									<td width="60%" align="left" valign="middle"><%=ESAPI.encoder().encodeForHTML((String) session.getAttribute(FieldType.CUST_NAME
					.getName()))%></td>
								</tr>
								<tr>
									<td align="left" valign="middle"><s:property value="getText('ORDERID')" /></td>
									<td align="left" valign="middle"><%=ESAPI.encoder().encodeForHTML((String) session.getAttribute(FieldType.ORDER_ID.getName()))%></td>
								</tr>

							</table></li>
					</ul>
				</div>
				<div class="pull-right" style="margin-top: -60px;">
					<img src="../image/ssl-logos.png" class="img-responsive">
				</div>
			</div>
		</div>
	</div>	

	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/responsive-tabs.js"></script>
	<script type="text/javascript">
		$('#myTab a').click(function(e) {
			e.preventDefault();
			$(this).tab('show');
		});
		$('#moreTabs a').click(function(e) {
			e.preventDefault();
			$(this).tab('show');
		});
		(function($) { // Test for making sure event are maintained
			$('.js-alert-test').click(function() {
				alert('Button Clicked: Event was maintained');
			});
			fakewaffle.responsiveTabs([ 'xs', 'sm' ]);
		})(jQuery);
	</script>
	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
	<script>
$('.hoverDiv').click(function() {
    $(this).addClass('active').siblings().removeClass('active');
})
</script>
</body>
</html>