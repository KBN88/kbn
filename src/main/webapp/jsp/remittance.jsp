<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Remittance</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery-ui.js"></script>
<script src="../js/commanValidate.js"></script>
<script src="../js/jquery.dataTables.js"></script> 
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script language="JavaScript">
$(document).ready(function() {
	   $('#merchant').change(function(event){
		   var merchants = $("select#merchants").val();
		   handleChange();	 	   
	   });
	 
	   $('#dateFrom').change(function(event){
		   var merchants = $("select#dateFrom").val();
		   DateChange();	 	   
	   });
	   
	   $('#currency').change(function(event){
		   var merchants = $("select#currency").val();
		   CurrencyChange();	 	   
	   });
	   
	   $("#dateFrom").datepicker({
			prevText : "click for previous months",
			nextText : "click for next months",
			showOtherMonths : true,
			dateFormat : 'dd-mm-yy',
			selectOtherMonths : false,
			maxDate : new Date()
		});
	   $("#remittanceDate").datepicker({
			prevText : "click for previous months",
			nextText : "click for next months",
			showOtherMonths : true,
			dateFormat : 'dd-mm-yy',
			selectOtherMonths : false,
			maxDate : new Date()
		});
});

$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'B',
        buttons: [
            'csv'
        ]
    });
});
	function handleChange(){
		var token  = document.getElementsByName("token")[0].value;
		 $.ajax({ 
		      url  : "remittanceDetails",
		      type : "POST",
		      data : { 
					payId : document
					.getElementById("merchant").value,
					token:token,
				    "struts.token.name": "token",					
		},
		      success : function(data){
		    	    document.getElementById("bankName").value = data.bankName;
			        document.getElementById("accHolderName").value = data.accHolderName;
			        document.getElementById("accountNo").value = data.accountNo;
			        document.getElementById("ifscCode").value = data.ifscCode;
			        document.getElementById("currency").value = "";
			        document.getElementById("dateFrom").value = "";
			        document.getElementById("netAmount").value = "";
			        document.getElementById("remittedAmount").value = "";
			        document.getElementById("remittanceDate").value = "";
			        document.getElementById("utr").value = ""; 
			        
			       var select = document.getElementById("currency"); 
			       
			       removeOptions(document.getElementById("currency"));
			       
	function removeOptions(selectbox){
		var i;
		for(i=selectbox.options.length-1;i>=0;i--)
		     {
		       selectbox.remove(i);
		     }
	}
					var options = data.curr; 
					for(var i = 0; i < options.length; i++) {
    				var opt = options[i];
    				var el = document.createElement("option");
    	            el.textContent = opt;
    				el.value = opt;
    				select.appendChild(el);
					}
	   		}
      	});
	}
	
	function DateChange(){
		var token  = document.getElementsByName("token")[0].value;
		 $.ajax({ 
		      url  : "remittanceAmountCall",
		      type : "POST",
		      data : { 
		    	  payId : document
					.getElementById("merchant").value,
					dateFrom : document
					.getElementById("dateFrom").value,
					currency : document
					.getElementById("currency").value,
					token:token,
				    "struts.token.name": "token",
		},
		      success : function(data){
		    	    document.getElementById("netAmount").value = data.netAmount;
		    	    document.getElementById("remittedAmount").value = data.netAmount;
		    	    document.getElementById("utr").value = ""; 
		    	    document.getElementById("remittanceDate").value = "";
			       			     
		      }
		    });
	}
	
	function CurrencyChange(){
		var token  = document.getElementsByName("token")[0].value;
		 $.ajax({ 
		      url  : "remittanceAmountCall",
		      type : "POST",
		      data : { 
		    	  payId : document
					.getElementById("merchant").value,
					dateFrom : document
					.getElementById("dateFrom").value,
					currency : document
					.getElementById("currency").value,
					token:token,
				    "struts.token.name": "token",
		},
		      success : function(data){
		    	    document.getElementById("netAmount").value = data.netAmount;
		    	    document.getElementById("remittedAmount").value = data.netAmount; 
		    	    document.getElementById("utr").value = ""; 
		    	    document.getElementById("remittanceDate").value = "";
			       			     
		      }
		    });
	}
	
</script>
</head>
<body>
<s:form  action ="remittancePay" autocomplete="off">
  <table width="100%" border="0"
						align="center" cellpadding="0" cellspacing="0" class="txnf">
    <tr>
      <td align="left"><h2>Remittance</h2></td>
    </tr>
    <tr>
      <td align="left" valign="middle">&nbsp;
        <div id="saveMessage">
          <s:actionmessage class="success success-text" />
        </div></td>
    </tr>
    <tr>
      <td align="left" valign="top"><div class="adduT">
          <div class="bkn">
          <h4>BANK DETAILS</h4>
            <div class="adduT">Merchant Name:<br />
              <div class="txtnew">
                <s:if
												test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
                  <s:select name="merchant" class="form-control"
													id="merchant" headerKey="ALL" headerValue="ALL"
													list="merchantList" listKey="payId"
													listValue="businessName" autocomplete="off" />
                </s:if>
                <s:else>
                  <s:select name="merchants" class="form-control"
									id="merchant" headerKey="ALL" headerValue="ALL"
									listKey="emailId" listValue="businessName"
									list="merchantList" autocomplete="off" />
                </s:else>
              </div>
            </div>
            <div class="adduT">Bank Name:<br>
              <div class="txtnew">
                <s:textfield name="bankName"  id="bankName"  cssClass="form-control" readonly="true" autocomplete="off" />
              </div>
            </div>
            <div class="adduT">Account Holder Name:<br>
              <div class="txtnew">
                <s:textfield name="accHolderName" id="accHolderName"  cssClass="form-control" readonly="true" autocomplete="off"/>
              </div>
            </div>
            <div class="adduT">Account Number:<br>
              <div class="txtnew">
                <s:textfield name="accountNo" id="accountNo"   cssClass="form-control" readonly="true" autocomplete="off" />
              </div>
            </div>
            <div class="adduT">IFSC Code:<br>
              <div class="txtnew">
                <s:textfield name="ifscCode" id="ifscCode"  cssClass="form-control" readonly="true" autocomplete="off"/>
              </div>
            </div>
            <div class="clear"></div>
            <div style="height:60px;"></div>
          </div>
          <div class="bkn">
          <h4>REMITTANCE DETAILS</h4>
            <div class="adduT">Currency<br>
              <div class="txtnew">
                <s:select name="currency"
								id="currency" list="currencyMap" class="form-control" autocomplete="off" />
              </div>
            </div>
            <div class="adduT">Transaction Date:<br>
              <div class="txtnew">
                <s:textfield type="text" readonly="true" id="dateFrom"
												name="dateFrom" class="form-control"
												autocomplete="off" />
              </div>
            </div>
            <div class="adduT">Remittable Amount:<br>
              <div class="txtnew">
                <s:textfield name="netAmount"  id ="netAmount"  cssClass="form-control" readonly="true" autocomplete="off"/>
              </div>
            </div>
            <div class="adduT">Remitted Amount:<br>
              <div class="txtnew">
                <s:textfield name="remittedAmount"  id ="remittedAmount"  cssClass="form-control" autocomplete="off" onkeypress="javascript:return isNumber (event)"/>
              </div>
            </div>
            <div class="adduT">UTR:<br>
              <div class="txtnew">
                <s:textfield name="utr"  id = "utr" cssClass="form-control" autocomplete="off" onkeypress="return Validate(event);"/>
              </div>
            </div>
            <div class="adduT">Remittance Date:<br>
              <div class="txtnew">
                <s:textfield type="text" readonly="true" id="remittanceDate"
												name="remittanceDate" class="form-control" autocomplete="off" />
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <div class="adduT" style="text-align:center; padding:14px 0 0 0;"><input type="submit" name="remittSubmit" value="Submit" id="remittSubmit" class="btn btn-success btn-md" /></div>
          
          <div class="clear"></div>
        </div></td>
    </tr>
    <tr>
      <td align="center" valign="top">&nbsp;</td>
    </tr>
  </table>
  <s:hidden name="token" value="%{#session.customToken}"></s:hidden>
</s:form><br /><br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="txnf product-spec">
    <tr>
      <th colspan="3" align="left">UPLOAD REMITTANCE FILE</th>
      </tr>
      <tr>
      <td width="46%" height="50" align="left" valign="bottom"><table id="example"  style="display:none;">
 <thead>
      <tr>
       <th>payId</th>
       <th>merchant</th>
       <th>bankName</th>
       <th>accHolderName</th>
       <th>accountNo</th>
       <th>ifscCode</th>
       <th>currency</th>
       <th>dateFrom</th>
       <th>netAmount</th>
       <th>remittedAmount</th>
        <th>utr</th>
       <th>remittanceDate</th>
        <th>status</th>
      </tr>
     </thead>
    </table> Simple CSV File Format</td>
      <s:form action="uploadRemittance" method="POST" enctype="multipart/form-data">
      <script type="text/javascript">
    $("body").on("click", "#btnUpload", function () {
        var allowedFiles = [".csv"];
        var fileUpload = $("#fileUpload");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;
    });
</script>
      <td width="29%" align="center" valign="middle"><div class="input-group">
	<span class="input-group-btn"> <input type="text" class="inputfieldsmall" id="fileUpload"  readonly> 
	<span class="file-input btn btn-success btn-file btn-small"> 
	<span class="glyphicon glyphicon-folder-open"></span> &nbsp;&nbsp;Browse 
	<s:file name="fileName" />
	</span>
	</span><span id="lblError" style="color: red;"></span>
<br />
	</div></td>
      <td width="25%" align="center" valign="middle"> <s:submit value="Upload" name="fileName"  id="btnUpload" class="btn btn-success btn-sm" />
</td></s:form>
    </tr>
    </table>
<script>
		$(document).on(
				'change',
				'.btn-file :file',
				function() {
					var input = $(this), numFiles = input.get(0).files ? input
							.get(0).files.length : 1, label = input.val()
							.replace(/\\/g, '/').replace(/.*\//, '');
					input.trigger('fileselect', [ numFiles, label ]);
				});

		$(document)
				.ready(
						function() {
							$('.btn-file :file')
									.on(
											'fileselect',
											function(event, numFiles, label) {

												var input = $(this).parents(
														'.input-group').find(
														':text'), log = numFiles > 1 ? numFiles
														+ ' files selected'
														: label;

												if (input.length) {
													input.val(log);
												} else {
													if (log)
														alert(log);
												}

											});
						});
	</script>
</body>
</html>