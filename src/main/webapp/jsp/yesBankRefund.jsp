<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Yes Bank Refund</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>

<script type="text/javascript">
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	$(document)
			.ready(
					function() {

						$(function() {

							$("#dateFrom").datepicker({
								prevText : "click for previous months",
								nextText : "click for next months",
								showOtherMonths : true,
								dateFormat : 'dd-mm-yy',
								selectOtherMonths : false,
								maxDate : new Date()
							});
							$("#dateTo").datepicker({
								prevText : "click for previous months",
								nextText : "click for next months",
								showOtherMonths : true,
								dateFormat : 'dd-mm-yy',
								selectOtherMonths : false,
								maxDate : new Date()
							});

						});

						$(function() {
							var today = new Date();
							$('#dateTo').val(
									$.datepicker.formatDate('dd-mm-yy', today));
							$('#dateFrom').val(
									$.datepicker.formatDate('dd-mm-yy', today));
							renderTable();
							var selectedServer = "<s:property value='%{#session.USER.UserType.name()}'/>";
							if (selectedServer == "SUBUSER") {
								var permission = "<s:property value='%{#session.USER_PERMISSION}'/>"
								if (permission.indexOf("Void/Refund") < 0) {
									var table = $('#captureDataTable')
											.DataTable();
									table.column(8).visible(false);
								}
							}
						});

						$("#submit").click(function(env) {
							reloadTable();
						});
					});

	function numberRows() {
		$('input[name^="SNo1"]').each(function(i) {
			$(this).val(i + 1);
		});
	}
	numberRows();

	function renderTable() {
		var table = new $.fn.dataTable.Api('#captureDataTable');
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				$(".modal").show();
			},
			complete : function() {
				$(".modal").hide();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#captureDataTable').dataTable({
			dom : 'Bfrtip',
			buttons : [ {
				extend : 'csv',
				title : 'Yes Bank Refund Transactions',

			} ],
			"ajax" : {
				"url" : "YesBankRefundAction",
				"type" : "POST",
				"data" : generatePostData
			},
			"processing" : true,
			"lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
			"order" : [ [ 1, "desc" ] ],
			"columns" : [ {
				"data" : "rowNumber",
				"width" : '10%',

			}, {
				"data" : null,
				"mRender" : function(row) {
					return "YBK";
				}
			}, {
				"data" : null,
				"width" : '14%',
				"mRender" : function(row) {
					return "\u0027" + row.payId;
				}
			}, {
				"data" : "txnDate",
				"width" : '13%'
			}, {
				"data" : "refundDate",
				"width" : '10%',

			}, {
				"data" : null,
				"mRender" : function(row) {
					return "KBN";
				}
			}, {
				"data" : "acqId",
				"width" : '13%'
			}, {
				"data" : null,
				"width" : '14%',
				"mRender" : function(row) {
					return "\u0027" + row.origTransactionId;
				}
			}, {
				"data" : "netAmount",
				"width" : '7%'
			}, {
				"data" : "refundedAmount",
				"width" : '10%'
			}

			]
		});
	}

	function reloadTable() {
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#captureDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}

	function decodeDiv() {
		var divArray = document.getElementsByTagName('div');
		for (var i = 0; i < divArray.length; ++i) {
			var div = divArray[i];
			if (div.id.indexOf('param-') > -1) {
				var val = div.innerHTML;
				div.innerHTML = decodeVal(val);
			}
		}
	}

	function decodeVal(value) {
		var txt = document.createElement("textarea");
		txt.innerHTML = value;
		return txt.value;
	}

	function generatePostData() {
		var token = document.getElementsByName("token")[0].value;
		var obj = {
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,

			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}
</script>
</head>
<body>
	<table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left" colspan="3"><div class="container">
					<div class="row">
						<div class="col-md-7 col-xs-12 text-left">
							<h2>Yes Bank Refund</h2>
						</div>
						<div class="col-md-2 col-xs-6">
							<div class="txtnew" style="padding: 0 8px 0 0">
								<br /> <label for="dateFrom">Date From:</label> <br />
								<s:textfield type="text" readonly="true" id="dateFrom"
									placeholder="Date From" name="dateFrom" class="form-control"
									autocomplete="off" onsubmit="validate()" />
							</div>
						</div>
						<div class="col-md-2 col-xs-6">
							<div class="txtnew" style="padding: 0 8px 0 0">
								<br /> <label for="dateTo">Date To:</label> <br />
								<s:textfield type="text" readonly="true" id="dateTo"
									name="dateTo" class="form-control" autocomplete="off" />
							</div>
						</div>
						<div
							class="form-group col-md-1 txtnew col-sm-2 col-xs-6 text-left">
							<br /> <label for="dateTo">&nbsp;</label>
							<s:submit value="Submit" id="submit" align="center"
								class="btn btn-sm btn-block btn-success" />
						</div>
					</div>

				</div></td>
		</tr>
		<tr>
			<td align="left" colspan="3" style="padding: 10px;"><br> <br>
				<div class="scrollD">
					<table id="captureDataTable" class="display table" cellspacing="0"
						width="100%">
						<thead>
							<tr class="boxheadingsmall">
								<th>Sr.No</th>
								<th>Bank Id</th>
								<th>Merchant Name</th>
								<th>Txn Date</th>
								<th>Refund Date</th>
								<th>Bank Merchant Code</th>
								<th>Bank Ref No.</th>
								<th>PGI Reference No.</th>
								<th>Txn Amount (Rs Ps)</th>
								<th>Refund Amount (Rs Ps)</th>
							</tr>
						</thead>
					</table>
				</div></td>
		</tr>
	</table>
</body>
</html>