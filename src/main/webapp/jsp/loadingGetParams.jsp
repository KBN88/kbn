<!DOCTYPE html>
<%@taglib prefix="s" uri="/struts-tags"%>
<html>

<head>

<title>Loading...</title>
<meta http-equiv="refresh" content="1; url=<s:url includeParams="all"/>"></meta> 
	<link rel="stylesheet" href="../css/loader/normalize.css">
	<link rel="stylesheet" href="../css/loader/main.css">
	<link rel="stylesheet" href="../css/loader/customLoader.css">
	<script src="../js/loader/modernizr-2.6.2.min.js"></script>
<style>
#loader-wrapper {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1000;
}
    #loader {
        display: block;
        position: relative;
        left: 50%;
        top: 50%;
        width: 150px;
        height: 150px;
        margin: -75px 0 0 -75px;
 
        border: 3px solid #3498db;
        z-index: 1500;
    }
    #loader:before {
    content: "";
    position: absolute;
    top: 5px;
    left: 5px;
    right: 5px;
    bottom: 5px;
    border: 3px solid #e74c3c;
}
#loader:after {
    content: "";
    position: absolute;
    top: 15px;
    left: 15px;
    right: 15px;
    bottom: 15px;
    border: 3px solid #f9c922;
}


<!-- removin brode -->
/* change border to transparent and set only border-top to a solid color */
#loader {
    border: 3px solid transparent;
    border-top-color: #3498db;
}
#loader:before {
    border: 3px solid transparent;
    border-top-color: #e74c3c;
}
#loader:after {
    border: 3px solid transparent;
    border-top-color: #f9c922;
}

#loader {
    border-radius: 50%;
}
#loader:before {
    border-radius: 50%;
}
#loader:after {
    border-radius: 50%;
}


/* copy and paste the animation inside all 3 elements */
/* #loader, #loader:before, #loader:after */
-webkit-animation: spin 1.5s linear infinite;
animation: spin 1.5s linear infinite;
 
/* include this only once */
@-webkit-keyframes spin {
    0%   {
        -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(0deg);  /* IE 9 */
        transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
    }
    100% {
        -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(360deg);  /* IE 9 */
        transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
    }
}
@keyframes spin {
    0%   {
        -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(0deg);  /* IE 9 */
        transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
    }
    100% {
        -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(360deg);  /* IE 9 */
        transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
    }
}

#loader {
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
}
#loader:before {
    -webkit-animation: spin 3s linear infinite;
    animation: spin 3s linear infinite;
}
</style>



<%-- <style>
.loader:before,
.loader:after,
.loader {
  border-radius: 50%;
  width: 2.5em;
  height: 2.5em;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
  -webkit-animation: load7 0.9s infinite ease-in-out;
  animation: load7 0.9s infinite ease-in-out;
}
.loader {
  color: #2b6dd1;
  font-size: 10px;
  margin: 80px auto;
  position: relative;
  text-indent: -9999em;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation-delay: -0.8s;
  animation-delay: -0.8s;
}
.loader:before {
  left: -3.5em;
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
}
.loader:after {
  left: 3.5em;
}
.loader:before,
.loader:after {
  content: '';
  position: absolute;
  top: 0;
}
@-webkit-keyframes load7 {
  0%,
  80%,
  100% {
    box-shadow: 0 2.5em 0 -1.3em;
  }
  40% {
    box-shadow: 0 2.5em 0 0;
  }
}
@keyframes load7 {
  0%,
  80%,
  100% {
    box-shadow: 0 2.5em 0 -1.3em;
  }
  40% {
    box-shadow: 0 2.5em 0 0;
  }
}
</style> --%>
<script>
function refresh(){
	   //update src attribute with a cache buster query
	   location.reload(true);
	   setTimeout("refresh();",1000)
	}
	</script>
</head>
 
 <body onload="">
 
 
 <div id="loader-wrapper">
			<div id="loader"></div>

			<div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>

		</div>
 
 
<!--  <div class="loader">Loading...</div> -->
<!-- <div id="progress" style="align-content=center;" >
   <img src="ripple.gif"/>
</div> -->

</body>
</html>