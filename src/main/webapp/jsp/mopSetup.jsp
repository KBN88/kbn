<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Merchant Mapping</title>
<!-- <link href="../css/bootstrap.min.css" rel="stylesheet"> -->
<link href="../css/font-awesome.min.css" rel="stylesheet"></link>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script src="../js/mapping.js"></script>
<script type="text/javascript">
	function showMe(it, box) {
		var vis = (box.checked) ? "block" : "none";
		var ele = document.getElementById(it);
		ele.style.display = vis;

		deselectAllCheckboxesWithinDiv(ele.id);
	}
	function hidefields(currencyBox){
		var vis = (currencyBox.checked) ? "block" : "none";
		var fieldsDiv = document.getElementById('boxdiv' + currencyBox.id)
		fieldsDiv.style.display = vis;
	}
	function getMops() {
		var merchantId = document.getElementById("merchants").value;
		var acquirer = document.getElementById("acquirer").value;

		if (merchantId == "" || merchantId == null || acquirer == "") {
			document.getElementById("err").style.display = "block";
			return false;
		}
		document.mopSetupForm.submit();
	}

	function resetAcq() {
		var acquirerDropDown = document.getElementById("acquirer");
		acquirerDropDown.selectedIndex = 0;
		refresh();
	}

	$(document)
			.ready(
					function() {
						getMapping();
						var mainDiv = document.getElementById('id+checkBoxes');
						mainDiv.style.display = "block";
						$("#btnsubmit")
								.click(
										function(evt) {
											var index, element;
											var form = document
													.getElementById("mopSetupForm");
											var num_of_ele = form.elements.length;
											var mappingString = "";
											var count = 0;
											for (index = 0; index < num_of_ele; ++index) {
												element = form.elements[index];
												if (element.type.toUpperCase() == "CHECKBOX"
														&& element.checked) {
													var mapStringVal;

													if (!(element.id == 'Credit Cardbox'
															|| element.id == 'Debit Cardbox'
															|| element.id == 'Net Bankingbox'
															|| element.id == 'Walletbox' 
															|| element.id == 'UPIbox' || element.id
															.indexOf('id') > -1)) {
														if (count == 0)
															mapStringVal = element.id;
														else {
															mapStringVal = ','
																	+ element.id;
														}
														count++;
														mappingString = mappingString
																.concat(mapStringVal);
													}
												}
											}
											var merchantId = document
													.getElementById("merchants").value;
											var acquirer = document
													.getElementById("acquirer").value;
											var currencyboxes = document
													.getElementsByName("currency");
											var accountCurrencySet = [];
											var k = 0;
											for (j = 0; j < currencyboxes.length; j++) {
												boxelement = currencyboxes[j];
												if (boxelement.checked) { 
													var password = document.getElementById("idpassword+"+boxelement.value).value;
													var txnKey = document.getElementById("idtxnkey+"+boxelement.value).value;
													var merchant = document.getElementById("idmerchantid+"+boxelement.value).value;
													if (acquirer == "CITRUS" || acquirer =="FSS"){
														var directTxn = document.getElementById("id3dflag+"+boxelement.value).checked;	
													}
													var currencyCode = boxelement.value;
													var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
													var emailId = "<s:property value='%{#session.USER.EmailId}'/>";
												    var accountCurrency = {currencyCode:currencyCode,password:password,txnKey:txnKey,merchantId:merchant,directTxn:directTxn};
												    accountCurrencySet.push(accountCurrency);
											}
												}
	
											if (merchantId == null
													|| merchantId == undefined
													|| merchantId == ""
													|| acquirer == ""
													|| (accountCurrencySet.length == 0 && mappingString != "")) {
												document.getElementById("err").style.display = "block";
												evt.preventDefault();
											} else {
												var token  = document.getElementsByName("token")[0].value;
												//to show new loader -Harpreet
												 $.ajaxSetup({
											            global: false,
											            beforeSend: function () {
											            	toggleAjaxLoader();
											            },
											            complete: function () {
											            	toggleAjaxLoader();
											            }
											        });
														$.ajax({
															type : "POST",
															url : "mopSetUp",
															data : {
																merchantEmailId : merchantId,
																acquirer : acquirer,
																userType : userType,
																mapString : mappingString,
																emailId : emailId,
																accountCurrencySet : JSON.stringify(accountCurrencySet),
																		token:token,
																	    "struts.token.name": "token",
															},
															success : function(
																	data,
																	status) {
																var response = data.response;
																alert(response);
														/* 		window.location
																		.reload(); */
															},
															error : function(
																	status) {
																alert("Error, mapping not saved!!!! ");
																window.location
																		.reload();
															}
														})
											}
										});
					});
</script>

</head>
<body>
<div class="transactions"></div>
	<s:form theme="simple" id="mopSetupForm" name="mopSetupForm"
		action="mopSetUpAction">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="txnf">
			<tr>
				<td align="left"><h2>Merchant Mapping</h2></td>
			</tr>
			<tr>
				<td colspan="3" class="txtnew" align="left" style="padding: 10px;">
				<div id="err" class="actionMessage" style="display: none; margin-bottom: 10px;">
						<ul>
							<li>Please select user, merchant and acquirer to proceed</li>
						</ul>
					</div>
					<div id="success" class="success success-text" style="display: none;">Mappings saved successfully</div>
			   </td>

			</tr>
			<tr>
				<td colspan="3" align="center">
                <div class="wd70" style="width:100%; margin-left:65px;">
					<table width="100%" border="0" cellpadding="0" cellspacing="0"
						class='greyroundtble'>
						<tr>
						  <td height="80" colspan="3" align="right" valign="bottom" class="txtnew"><table width="100%" border="0" cellspacing="0" cellpadding="0.">
						    <tr>
						      <td width="7%" align="right" valign="middle"><b>User
							  :</b><span style="color: red">*</span>&nbsp;&nbsp;</td>
						      <td width="19%" align="left" valign="middle"><s:select label="User"
									headerKey="-1" headerValue="Select User"
									list="#{'1':'Merchant'}" class="form-control" id="user"
									name="user" value="1" /></td>
						      <td width="10%" align="right"><b>Merchant : </b><span
								style="color: red">*</span>&nbsp;&nbsp;</td>
						      <td width="21%"><s:select label="Merchant"
									name="merchantEmailId" class="form-control" id="merchants"
									headerKey="" headerValue="Select Merchant" list="listMerchant"
									listKey="emailId" listValue="businessName"
									value="%{merchantEmailId}" onchange="resetAcq()" autocomplete="off"/></td>
						      <td width="8%" align="right"><b>Acquirer :</b>&nbsp;&nbsp;</td>
						      <td width="21%"><s:select label="Merchant" name="acquirer"
									class="form-control" id="acquirer" headerKey=""
									headerValue="Select Acquirer"
									list="@com.kbn.pg.core.AcquirerType@values()"
									listKey="code" listValue="name" onchange="getMops()" autocomplete="off"/></td>
					        </tr>
					      </table></td>
						  <td width="12" align="left">&nbsp;</td>
  </tr>
						<tr>
							<td align="left" colspan="3" style="padding-left:15px;">
								<br />
									<br />
								<div id="id+checkBoxes" class="filters" style="display: none">
									<s:set value="0" var="listCounter" />
										 <s:iterator value="currencies">
											<s:checkbox name="currency" id="%{'id+' +key}"
												fieldValue="%{key}" value="false" onclick="hidefields(this)"></s:checkbox>
											<b><s:property value="value" /></b>
											<s:div id="%{'boxdivid+' +key}" style="display: none">
												<s:div id="%{'elements'+key}">
													<span>Merchant ID</span>
													<s:textfield name="merchantId" id="%{'idmerchantid+' +key}"
														value="%{merchantId}" placeholder="MerchantId" autocomplete="off"></s:textfield>
													<span>Txn Key</span>
													<s:textfield name="txnKey" id="%{'idtxnkey+' +key}"
														value="%{txnKey}" placeholder="Txn Key" autocomplete="off"></s:textfield>
													<span>Password</span>
													<s:textfield name="password" id="%{'idpassword+' +key}"
														value="%{password}" placeholder="Password" autocomplete="off"></s:textfield>
													<s:if test="%{acquirer  in {'FSS','CITRUS'}}">
														<span style="display: inline;">Non 3DS</span>
														<s:checkbox name="directTxn" id="%{'id3dflag+' +key}"
															style="display:inline;" type="text" value="%{directTxn}"></s:checkbox>
													</s:if>
												</s:div>
											</s:div>
											<br />
										</s:iterator> 
										<br />
									<br />

									<s:iterator value="mopList" status="itStatus">
										<ul class="filters">
											<li><label> <s:checkbox
														name="paymentSelectedList" type="checkbox"
														id="%{key+'box'}" value="false"
														onclick="showMe('%{key}', this)" cssClass="roundedTwo">
													</s:checkbox> <span class="icon"><i class="fa fa-check"></i></span> <s:property
														value="key" />
											</label></li>
											<li><s:div id="%{key}"
													cssStyle="display:none; padding-left:20px; padding-top:1px; padding-bottom:1px;">

													<s:if test="%{key=='Net Banking'}">
														<label><s:checkbox name="selectAll" value="false"
																id="id+selectAllButton" fieldValue="selectAll"
																label="All"
																onclick="selectAllCheckboxesWithinDiv('%{key}')">
															</s:checkbox> <span class="redsmalltext"><b>Select All</b></span> </label>
														<br />
													</s:if> 
													
													<s:if test="%{key=='Wallet'}">
															<label><s:checkbox name="selectAll1"
																	value="false" id="id+selectAllButtonWallet"
																	fieldValue="selectAll1" label="All"
																	onclick="selectAllCheckboxesWithinDivWallet('%{key}')">
																</s:checkbox> <span class="redsmalltext"><label>Select
																		All</label></span> </label>
															<br />
														</s:if>
													

													<s:iterator value="value" status="mopStatus">

														<s:if
															test="%{[1].key!=('Net Banking') && ([1].key!='Wallet') && ([1].key!='UPI')}">
															<label><s:checkbox name="mopSelectedList"
																	id="id+%{[1].key+'-'+code}" fieldValue="%{name}"
																	label="%{name}" value="false"
																	onclick="showMe('%{[1].key+'-'+code}', this)">
																</s:checkbox> <span class="redsmalltext"><b><s:property
																			value="name" /></b></span>&nbsp;&nbsp;</label>

															<s:div id="%{[1].key+'-'+code}"
																cssStyle="display:none; padding-left:20px; padding-top:10px; padding-bottom:10px;">
																<s:iterator value="transList" status="txnStatus">
																	<label><s:checkbox
																			id="%{[2].key+'-'+[1].code+'-'+code}"
																			name="txnSelectedList" fieldValue="%{name}"
																			label="%{name}" value="false">
																		</s:checkbox> <span class="black9"><s:property value="name" /></span>&nbsp;&nbsp;</label>
																</s:iterator>
															</s:div>
														</s:if>
														<s:else>
															<s:div cssStyle="padding-left:0px; padding-top:5px; padding-bottom:5px; float:left;"><label class="netbankinglabel"><s:checkbox name="mopSelectedList"
																	id="%{key+'-'+code}" fieldValue="%{name}"
																	label="%{name}" value="false">
																</s:checkbox> <span class="black9"><b><s:property
																			value="name" /></b></span>&nbsp;&nbsp;</label></s:div>
														</s:else>
													</s:iterator>
												</s:div></li>
										</ul>

									</s:iterator>
								</div>
							</td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td height="40" valign="bottom" colspan="4" align="center"><s:textfield type="button"
									value="Save" class="btn btn-success btn-sm" id="btnsubmit" theme="simple" /></td>
						</tr>
						<tr>
							<td width="96" height="5" align="left"></td>
							<td width="195" height="5" align="left"></td>
							<td colspan="4" align="left"></td>
						</tr>
						<tr>
							<td height="30" colspan="4" align="left">&nbsp;</td>
						</tr>
					</table>
					<br /><br />
                    <div class="clear"></div>
                    </div>
				</td>
				  <td width="15%" align="center" valign="middle" ></td>
			</tr>
		</table>
		<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
	</s:form>
</body>
</html>
