<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Summary Report</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script type="text/javascript" src="../js/pdfmake.js"></script>
<script type="text/javascript" src="../js/summaryReport.js"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script type="text/javascript">
	var checkedRows = [];
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	function handleChange() {
		reloadTable();
	}
	$(document)
			.ready(
					function() {
						$(function() {

							$("#dateFrom").datepicker({
								prevText : "click for previous months",
								nextText : "click for next months",
								showOtherMonths : true,
								dateFormat : 'dd-mm-yy',
								selectOtherMonths : false,
								maxDate : new Date()
							});
							$("#dateTo").datepicker({
								prevText : "click for previous months",
								nextText : "click for next months",
								showOtherMonths : true,
								dateFormat : 'dd-mm-yy',
								selectOtherMonths : false,
								maxDate : new Date()
							});
						});
						$(function() {
							var today = new Date();
							$('#dateTo').val(
									$.datepicker.formatDate('dd-mm-yy', today));
							$('#dateFrom').val(
									$.datepicker.formatDate('dd-mm-yy', today));
							renderTable();
						});

						$("#processAllButton").click(function(env) {
							if (checkedRows.length == 0) {
								alert("Please select elements to proceed");
								return false;
							}
							processAll();
						});
						$("#selectAllCheckBox").click(function(env) {
							handleSelectALLClick(this);
						});
						$(function() {
							var table = $('#summaryReportDataTable')
									.DataTable();
							$('#summaryReportDataTable tbody')
									.on(
											'click',
											'td',
											function() {
												var columnIndex = table.cell(
														this).index().column;
												var rowIndex = table.cell(this)
														.index().row;
												var rowNodes = table.row(
														rowIndex).node();
												var rowData = table.row(
														rowIndex).data();
												if (columnIndex == 0) {
													handleSingleCheckBoxClick(
															rowNodes, rowData);
												}
											});

							$('#summaryReportDataTable tbody')
									.on(
											'click',
											'button[type="button"]',
											function() {
												var columnIndex = table.cell(
														this.parentNode)
														.index().column;
												var rowIndex = table.cell(
														this.parentNode)
														.index().row;
												var rowNodes = table.row(
														rowIndex).node();
												var rowData = table.row(
														rowIndex).data();
												if (columnIndex == 17) {
													process(table,
															this.parentNode);
												}
											});

							$('#summaryReportDataTable').on('page.dt',
									function(event) {
										var tableObj = event.currentTarget;
										uncheckAllCheckBoxes(tableObj);
									});
						});
					});
	$(document).ready(function() {
		$('#example').DataTable({
			dom : 'B',
			buttons : [ 'csv' ]
		});
	});

	function renderTable() {
		var merchantEmailId = document.getElementById("merchants").value;
		var table = new $.fn.dataTable.Api('#summaryReportDataTable');
		//to show new loader -Harpreet
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				toggleAjaxLoader();
			},
			complete : function() {
				toggleAjaxLoader();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#summaryReportDataTable')
				.dataTable(
						{
							//		dom : 'T<"clear">lfrtip',
							"footerCallback" : function(row, data, start, end,
									display) {
								var api = this.api(), data;

								// Remove the formatting to get integer data for summation
								var intVal = function(i) {
									return typeof i === 'string' ? i.replace(
											/[\,]/g, '') * 1
											: typeof i === 'number' ? i : 0;
								};

								// Total over all pages
								total = api.column(10).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(10, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(10).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								// Total over all pages
								total = api.column(11).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page refund
								pageTotal = api.column(11, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(11).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								// Total over all pages
								total = api.column(11).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(12, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(12).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								// Total over all pages
								total = api.column(12).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(14, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(14).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								// Total over all pages
								total = api.column(15).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(15, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(15).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');

								// Total over all pages
								total = api.column(16).data().reduce(
										function(a, b) {
											return intVal(a) + intVal(b);
										}, 0);

								// Total over this page
								pageTotal = api.column(16, {
									page : 'current'
								}).data().reduce(function(a, b) {
									return intVal(a) + intVal(b);
								}, 0);

								// Update footer
								$(api.column(16).footer()).html(
										'' + pageTotal.toFixed(2) + ' ' + ' ');
							},

							"columnDefs" : [ {
								className : "dt-body-right",
								"targets" : [ 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
										13, 14, 15, 16, 17 ]
							} ],
							dom : 'BTftlpi',
							buttons : [
									{
										extend : 'copyHtml5',
										//footer : true,
										exportOptions : {
											columns : [ 18, 2, 3, 4, 5, 6, 7,
													8, 10, 11, 13, 14, 15, 16 ]
										}
									},
									{
										extend : 'csvHtml5',
										//footer : true,
										title : 'Summary Transactions',
										exportOptions : {
											columns : [ 18, 2, 3, 4, 5, 6, 7,
													8, 10, 11, 13, 14, 15, 16 ]
										}
									},
									{
										extend : 'pdfHtml5',
										//footer : true,
										orientation : 'landscape',
										title : 'Summary Transactions',
										exportOptions : {
											columns : [ 1, 2, 3, 4, 5, 6, 7, 8,
													10, 11, 13, 14, 15, 16 ]
										}
									},
									{
										extend : 'print',
										//footer : true,
										title : 'Summary Transactions',
										exportOptions : {
											columns : [ 18, 2, 3, 4, 5, 6, 7,
													8, 10, 11, 13, 14, 15, 16 ]
										}
									},
									{
										extend : 'colvis',
										//           collectionLayout: 'fixed two-column',
										columns : [ 3, 4, 5, 6, 7, 8, 9, 10, 12 ]
									} ],
							"ajax" : {
								"url" : "summaryReportAction",
								"type" : "POST",
								"data" : function(d) {
									return generatePostData(d);
								}
							},
							"searching" : false,
							"processing" : true,
							"serverSide" : true,
							"paginationType" : "full_numbers",
							"lengthMenu" : [ [ 10, 25, 50, -1 ],
									[ 10, 25, 50, "All" ] ],
							//"order" : [ [ 1, "desc" ] ],
							"order" : [],
							"columnDefs" : [ {
								"type" : "html-num-fmt",
								"targets" : 4,
								"orderable" : false,
								"targets" : [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
										11, 12, 13, 14, 15, 16 ]
							} ],
							"columns" : [
									{
										"targets" : 0,
										"searchable" : false,
										"orderable" : false,
										"width" : '1%',
										"data" : null,
										/*   'className': 'dt-body-center', */
										'render' : function(data, type, row,
												meta) {
											var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
											if (userType == "ADMIN"
													|| userType == "SUBADMIN") {
												var settlement = row.settlementStatus;
												var checkbox = '';
												if (settlement == "PROCESSED") {
													checkbox = '<input type="checkbox" id='+row.transactionId+' disabled="true">';
													//implement some css also
												} else {
													checkbox = '<input type="checkbox" id='+row.transactionId+'>';
												}
												return checkbox;
											} else {
												return "";
											}
										}
									},
									{
										"data" : "transactionId",
										"width" : '11%'
									},
									{
										"data" : "txnDate",
										"width" : '8%'
									},
									{
										"data" : "authCode",
										"visible" : false
									},
									{
										"data" : "orderId",
										"width" : '9%'
									},
									{
										"data" : "businessName",
										"visible" : false
									},
									{
										"data" : "customerEmail",
										"width" : '6%',
										"visible" : false
									},
									{
										"data" : "paymentMethod",
										"width" : '12%',
										"render" : function(data, type, full) {
											return full['paymentMethod'] + ' '
													+ '-' + ' '
													+ full['mopType'];
										}
									},
									{
										"data" : "currencyName",

									},
									{
										"data" : "countryName",
										"visible" : false
									},
									{
										"data" : "approvedAmount"

									},
									{
										"data" : "merchantFixCharge"

									},
									{
										"data" : "refundedAmount"

									},
									{
										"data" : "chargebackAmount",
										"visible" : false,
										"className" : "displayNone"

									},
									{
										"data" : "tdr"

									},
									{
										"data" : "serviceTax"

									},
									{
										"data" : "netAmount",
										"width" : '7%',
									},
									{
										"data" : null,
										"orderable" : false,
										"render" : function(row) {
											var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
											var settlement = row.settlementStatus;
											if (userType == "ADMIN"
													|| userType == "SUBADMIN") {
												if (settlement == "PROCESSED") {
													return '<button class="grayrefundbtn" disabled="true" >Processed</button>';

												} else {
													return '<button type="button" class="greenrefundbtn" >Process</button>';
												}
											} else {
												return '<button hidden="hidden"  disabled="true" class="greenrefundbtn">Processed</button>';
											}
										}
									},
									{
										"data" : null,
										"width" : '14%',
										"visible" : false,
										"mRender" : function(row) {
											return "\u0027" + row.transactionId;
										}
									}, ]
						});
		$("#merchants").select2({
		//data: payId
		});
	}
	function process(table, index) {
		var token = document.getElementsByName("token")[0].value;
		var columnNumber = table.cell(index).index().column;

		if (columnNumber == 17) {
			var rowIndex = table.cell(index).index().row;
			var row = table.row(rowIndex).data();
			var payId = row.payId;
			var mopType = row.mopType;
			var paymentType = row.paymentMethod;
			var txnDate = row.txnDate;
			var txnId = row.transactionId;
			var orderId = row.orderId;
			var currency = row.currencyCode;
			var payment_method = row.paymentMethod;
			var fixCharge = row.merchantFixCharge;
			var capturedAmount = row.approvedAmount;
			var tdr = row.tdr;
			var service_tax = row.serviceTax;
			var net = row.netAmount;
			var cust_email = row.customerEmail;
			if (cust_email == null) {
				cust_email = "Not available";
			}
			var merchant = decodeVal(row.businessName);
			var txnType = "SALE";
			var token = document.getElementsByName("token")[0].value;

			$
					.ajax({
						url : 'processSettlement',
						type : "POST",
						data : {
							txnDate : decodeVal(txnDate),
							origTxnId : decodeVal(txnId),
							amount : decodeVal(capturedAmount),
							payId : decodeVal(payId),
							orderId : decodeVal(orderId),
							currencyCode : decodeVal(currency),
							mopType : decodeVal(mopType),
							paymentMethod : decodeVal(payment_method),
							tdr : decodeVal(tdr),
							serviceTax : decodeVal(service_tax),
							netAmount : decodeVal(net),
							merchantFixCharge : decodeVal(fixCharge),
							cust_email : decodeVal(cust_email),
							merchant : decodeVal(merchant),
							txnType : decodeVal(txnType),
							token : token,
							"struts.token.name" : "token",
						},
						success : function(data) {
							var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0])
									: (data.response));
							if (null != response) {
								alert(response);
							}
							reloadTable(true);
						},
						error : function(data) {
							alert("Settlement not processed successfully please try again later");
						}
					});
		}
	}
	function reloadTable(retainPageFlag) {
		$('#selectAllCheckBox').attr('checked', false);
		checkedRows = [];
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#summaryReportDataTable');
		var table = tableObj.DataTable();
		if (retainPageFlag) {
			//set page dummy callback function
			table.ajax.reload(setPage(), false);
		} else {
			table.ajax.reload();
		}

	}
	//dummy callback
	function setPage() {
	}

	function generatePostData(d) {
		var token = document.getElementsByName("token")[0].value;
		var merchantEmailId = document.getElementById("merchants").value;
		if (merchantEmailId == '') {
			merchantEmailId = 'ALL'
		}
		var obj = {
			acquirer : document.getElementById("account").value,
			paymentType : document.getElementById("paymentMethods").value,
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			merchantEmailId : merchantEmailId,
			currency : document.getElementById("currency").value,
			draw : d.draw,
			length : d.length,
			start : d.start,
			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}
</script>
<style>
.dataTables_wrapper {
	position: relative;
	clear: both;
	*zoom: 1;
	zoom: 1;
	margin-top: -30px;
}
</style>
</head>
<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">

		<tr>
			<td align="center" valign="top"><table width="100%" border="0"
					align="center" cellpadding="0" cellspacing="0" class="txnf">

					<tr>

						<td colspan="5"><h2>Transaction Summary</h2>

							<div class="container">


								<s:if
									test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
									<div class="form-group col-md-2 col-xs-6 col-sm-3 txtnew">

										<div class="txtnew">
											<label for="acquirer">Merchant:</label><br />
											<s:select name="merchants" class="form-control"
												id="merchants" headerKey="" headerValue="ALL"
												list="merchantList" listKey="emailId"
												listValue="businessName" onchange="handleChange();"
												autocomplete="off" />
										</div>
									</div>
								</s:if>

								<s:else>
									<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">


										<div class="txtnew">
											<label for="acquirer">Merchant:</label><br />
											<s:select name="merchants" class="form-control"
												id="merchants" headerKey="" headerValue="ALL"
												list="merchantList" listKey="emailId"
												listValue="businessName" onchange="handleChange();"
												autocomplete="off" />
										</div>

									</div>
								</s:else>

								<s:if
									test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
									<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">
										<div class="txtnew">
											<label for="acquirer">Account:</label><br />
											<s:select headerKey="ALL" headerValue="ALL" id="account"
												name="account" class="form-control"
												list="@com.kbn.pg.core.AcquirerType@values()"
												listKey="code" listValue="name" onchange="handleChange();"
												autocomplete="off" />
										</div>
									</div>
								</s:if>

								<s:else>
									<div class="form-group col-md-2 col-xs-6 txtnew"
										style="display: none;">


										<div class="txtnew">
											<s:select headerKey="ALL" headerValue="ALL" id="account"
												name="account" class="form-control"
												list="@com.kbn.pg.core.AcquirerType@values()"
												listKey="code" listValue="name" onchange="handleChange();"
												autocomplete="off" />
										</div>

									</div>
								</s:else>


								<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
									<label for="email">Payment Method:</label> <br />
									<s:select headerKey="ALL" headerValue="ALL"
										class="form-control"
										list="@com.kbn.commons.util.PaymentType@values()"
										name="paymentMethods" id="paymentMethods"
										onchange="handleChange();" autocomplete="off" value="code"
										listKey="code" listValue="name" />
								</div>
								<div class="form-group  col-md-2 col-sm-4 txtnew  col-xs-6">
									<label for="email">Currency:</label> <br />
									<s:select name="currency" id="currency" headerValue="ALL"
										headerKey="ALL" list="currencyMap" class="form-control"
										onchange="handleChange();" />
								</div>

								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateFrom">Date From:</label> <br />
									<s:textfield type="text" readonly="true" id="dateFrom"
										name="dateFrom" class="form-control" autocomplete="off"
										onchange="handleChange();" />
								</div>
								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateTo">Date To:</label> <br />
									<s:textfield type="text" readonly="true" id="dateTo"
										name="dateTo" class="form-control" onchange="handleChange();"
										autocomplete="off" />
								</div>
							</div></td>
					</tr>
					<tr>
						<td colspan="5" align="left" valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td align="left" valign="top" style="padding: 10px;">
							<div class="scrollD">
								<table id="summaryReportDataTable" align="center"
									cellspacing="0" width="100%">
									<thead>
										<tr class="boxheadingsmall" style="font-size: 11px;">
											<s:if
												test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'}">
												<th style='text-align: left'><input type="checkbox"
													name="selectAllCheckBox" id="selectAllCheckBox"></input></th>
											</s:if>
											<s:else>
												<th style='text-align: center'></th>
											</s:else>
											<th style='text-align: center'>Txn Id</th>
											<th style='text-align: center'>Date</th>
											<th style='text-align: center'>Auth Code</th>
											<th style='text-align: center'>Order Id</th>
											<th style='text-align: center'>Merchant Name</th>
											<th style='text-align: center'>Customer Email</th>
											<th style='text-align: center'>Payment Method</th>
											<th style='text-align: center'>Currency</th>
											<th style='text-align: center'>Country</th>
											<th style='text-align: center'>Captured</th>
											<th style='text-align: center'>Fixed Charge</th>
											<th style='text-align: center'>Refund</th>
											<th style='text-align: center'>Charge Back</th>
											<th style='text-align: center'>TDR</th>
											<th style='text-align: center'>Ser.Tax</th>
											<th style='text-align: center'>Net</th>
											<s:if
												test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'}">
												<th style='text-align: right'><input type="button"
													id="processAllButton" value="Process All"
													class="greenrefundbtn" style="width: 70px"></input></th>
											</s:if>
											<s:else>
												<th style='text-align: center'></th>
											</s:else>
											<th style='text-align: right'>Txn Id</th>
										</tr>
									</thead>
									<tfoot>
										<tr class="boxheadingsmall">
											<th style='text-align: left;'></th>
											<th style='text-align: left;'>Total Amount</th>
											<th style='text-align: left;'></th>
											<th style='text-align: left;'></th>
											<th style='text-align: left;'></th>
											<th style='text-align: left;'></th>
											<th style='text-align: left;'></th>
											<th style='text-align: left;'></th>
											<th style='text-align: left;'></th>
											<th style='text-align: left;'></th>
											<th style='text-align: right; padding-right: 5px;'></th>
											<th style='text-align: right; padding-right: 5px;'></th>
											<th style='text-align: right; padding-right: 5px;'></th>
											<th style='text-align: right; padding-right: 5px;'></th>
											<th style='text-align: right; padding-right: 5px;'></th>
											<th style='text-align: right; padding-right: 5px;'></th>
											<th style='text-align: right; padding-right: 5px;'></th>
											<th style='text-align: left;'></th>
											<th style='text-align: left;'></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</td>
					</tr>
				</table></td>
		</tr>
	</table>




	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0" class="txnf product-spec">

		<td colspan="3" align="left"><s:actionmessage /></td>

		<tr>
			<th colspan="3" align="left">UPLOAD FILE</th>
		</tr>
		<tr>
			<td width="46%" height="50" align="left" valign="bottom"><table
					id="example" style="display: none;">
					<thead>

						<tr>
							<th>TXN_ID</th>
							<th>PAY_ID</th>
							<th>ORIG_TXN_ID</th>
							<th>AMOUNT</th>
							<th>AUTH_CODE</th>
							<th>RRN</th>
							<th>ACQ_ID</th>
							<th>STATUS</th>
							<th>RESPONSE_CODE</th>
							<th>RESPONSE_MESSAGE</th>
							<th>PG_TXN_MESSAGE</th>
							<th>PG_RESP_CODE</th>
							<th>PG_REF_NUM</th>
							<th>RFU1</th>
							<th>RFU2</th>
							<th>ACQUIRER_TYPE</th>
							<th>ORDER_ID</th>
							<th>TXNTYPE</th>
							<th>PG_DATE_TIME</th>
							<th>CURRENCY_CODE</th>

						</tr>
					</thead>
				</table> Simple CSV File Format</td>
			<s:form action="uploadStatusEnquiry" method="POST"
				enctype="multipart/form-data">
				<script type="text/javascript">
					$("body")
							.on(
									"click",
									"#btnUpload",
									function() {
									
										var allowedFiles = [ ".csv" ];
										var fileUpload = $("#fileUpload");
										var lblError = $("#lblError");
										var regex = new RegExp(
												"([a-zA-Z0-9\s_\\.\-:])+("
														+ allowedFiles
																.join('|')
														+ ")$");
										if (!regex.test(fileUpload.val()
												.toLowerCase())) {
											lblError
													.html("Please upload files having extensions: <b>"
															+ allowedFiles
																	.join(', ')
															+ "</b> only.");
											return false;
										}
										lblError.html('');
										return true;
									});
				</script>
				<td width="29%" align="center" valign="middle"><div
						class="input-group">
						<span class="input-group-btn"> <input type="text"
							class="inputfieldsmall" id="fileUpload" readonly="readonly" /> <span
							class="file-input btn btn-success btn-file btn-small"> <span
								class="glyphicon glyphicon-folder-open"></span>
								&nbsp;&nbsp;Browse <s:file name="fileName" />
						</span></span><span id="lblError" style="color: red;"></span> <br />
					</div></td>
				<td width="25%" align="center" valign="middle"><s:submit
						value="Upload" name="fileName" id="btnUpload"
						class="btn btn-success btn-sm" /></td>
				<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
			</s:form>
		</tr>
	</table>

	<script>
		$(document).on(
				'change',
				'.btn-file :file',
				function() {
					var input = $(this), numFiles = input.get(0).files ? input
							.get(0).files.length : 1, label = input.val()
							.replace(/\\/g, '/').replace(/.*\//, '');
					input.trigger('fileselect', [ numFiles, label ]);
				});

		$(document)
				.ready(
						function() {
							$('.btn-file :file')
									.on(
											'fileselect',
											function(event, numFiles, label) {

												var input = $(this).parents(
														'.input-group').find(
														':text'), log = numFiles > 1 ? numFiles
														+ ' files selected'
														: label;

												if (input.length) {
													input.val(log);
												} else {
													if (log)
														alert(log);
												}

											});
						});
	</script>
</body>
</html>