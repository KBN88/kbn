<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; X-Content-Type-Options=nosniff; charset=utf-8" />
<title>Reset Password</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.minshowpop.js"></script>
<script src="../js/jquery.formshowpop.js"></script>
<%-- <script>
	if (self == top) {
		var theBody = document.getElementsByTagName('body')[0];
		theBody.style.display = "block";
	} else {
		top.location = self.location;
	}
</script> --%>
<script>
    $(document).ready(function(){
      $("#submit").click(function(e){
          e.preventDefault();
        var token  = document.getElementsByName("token")[0].value;
        $.ajax({type: "POST",
                url: "resetPasswordAction",
                data: { payId: document
					.getElementById("payId").value,
					newPassword : document
					.getElementById("newPassword").value,
					confirmNewPassword : document
					.getElementById("confirmNewPassword").value,
					token:token,
				    "struts.token.name": "token",
				    },
					
                success:function(data){
                	var res = data.response;
                	alert(res);
                	if(data.errorCode == "314")
                		window.location.href = "index";
        }});
      });
    });
    </script>
    
<s:head/>
</head>
<body>
<table width="100" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr><td align="center" valign="bottom" height="140"><img src="../image/logo-signup.png"/></td></tr>
  <tr>
  <%-- <s:token/> --%>
    <td><s:div class="signupbox">
        <table width="355" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td  align="center" class="signup-headingbg"><h4>Reset Password</h4></td>
          </tr>
          <tr>
            <td align="center"><table width="90%" border="0" cellspacing="0" cellpadding="0">                                

                <tr>
                  <td align="left"><s:form id="resetPassword"  autocomplete="off">
    <s:token/>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td height="10" align="left"> <s:hidden  value="%{payId}" name="payId" id="payId" />
    <s:hidden  value="%{emailId}" name="emailId" id="emailId" /></td>
                        </tr>  
                        <tr>
                          <td height="50" align="left"><input type="password" name="newPassword" style='display: none'></input>
                          <s:textfield  placeholder="New Password " type="password" name="newPassword" id="newPassword" cssClass="signuptextfield"  autocomplete="off"/></td>
                        </tr>
                        <tr>
                          <td height="50" align="left"><s:textfield placeholder="Confirm Password " type="password" name="confirmNewPassword" id="confirmNewPassword" cssClass="signuptextfield"  autocomplete="off"/></td>
                        </tr>
                        <tr>
                          <td height="50" align="left" valign="bottom"><s:submit value="Submit" id="submit" cssClass="signupbutton btn-primary"></s:submit>
                          </td>
                        </tr>                     
                      </table>
                  </s:form></td>
                </tr>               
                        <tr>
                          <td height="10" align="justify"></td>
                          </tr>
              </table></td>
          </tr>
        </table>
      </s:div>
      </td>
  </tr>
</table>
<script>
			$(document).ready(function(){
				
				var fields = {
						
						newPassword : {
							tooltip: "Password must be minimum 8 and <br> maximum 32 characters long, with <br> special characters (! @ , _ + / =) , <br> at least one uppercase and  one <br>lower case alphabet.",
							position: 'right',
							backgroundColor: "#6ad0f6",
							color: '#FFFFFF'
							},
						//color : {
							//tooltip: "This is for your cover color~~~ <a href='#'>here</a>"
							//},
						//text : {
							//tooltip: "Please provide your comment here."
							//}
						};
				
				//Include Global Color 
				$("#resetPassword").formtoolip(fields, { backgroundColor: "#000000" , color : "#FFFFFF", fontSize : 14, padding : 10, borderRadius :  5});
					
				});
</script>
</body>
</html>