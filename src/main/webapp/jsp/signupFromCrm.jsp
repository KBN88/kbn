<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Sign Up</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.minshowpop.js"></script>
<script src="../js/jquery.formshowpop.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script src="../js/commanValidate.js"></script>
<script src="../js/captcha.js"></script>
<link href="http://fonts.googleapis.com/css?family=Crafty+Girls"
	rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(function() {
		$("#userRoleType").change(function() {
			if ($(this).val() == "merchant") {
				$("#tdIndustryType").show();
			} else {
				$("#tdIndustryType").hide();
				$("#subcategorydiv").hide();
			}

		});

		$("#industryCategory").change(function() {
			var industry = this.value;
			if (industry == 'select') {
				$("#subcategorydiv").hide();
				var subCategoryText = document.getElementById("subcategory");
				subCategoryText.value = "";
				return false;
			}
			var token = document.getElementsByName("token")[0].value;
			$.ajax({
				type : "POST",
				url : "industrySubCategory",
				data : {
					industryCategory : industry,
					token : token,
					"struts.token.name" : "token"
				},
				success : function(data, status) {
					var subCategoryListObj = data.subCategories;
					var subCategoryList = subCategoryListObj[0].split(',');
					var radioDiv = document.getElementById("radiodiv");
					radioDiv.innerHTML = "";
					for (var i = 0; i < subCategoryList.length; i++) {
						var subcategory = subCategoryList[i];
						var radioOption = document.createElement("INPUT");
						radioOption.setAttribute("type", "radio");
						radioOption.setAttribute("value", subcategory);
						radioOption.setAttribute("name", "subcategory");
						var labelS = document.createElement("SPAN");
						labelS.innerHTML = subcategory;
						radioDiv.appendChild(radioOption);
						radioDiv.appendChild(labelS);
					}
					$('#popup').popup('show');
				},
				error : function(status) {
					alert("Network error please try again later!!");
				}
			});
		});
	});

	function selectSubcategory() {
		var checkedRadio = $('input[name="subcategory"]:checked').val();
		var subCategoryDiv = document.getElementById("subcategorydiv");
		var subCategoryText = document.getElementById("subcategory");
		subCategoryText.value = checkedRadio;
		subCategoryDiv.style.display = "block";
		$('#popup').popup('hide');
		//make subcategory editable/
		//insert details if other selected
		//validation for required field
		//refresh if category changed
	}
</script>
</head>
<body>
	<div id="popup" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">
			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="1" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background: linear-gradient(60deg, #425185, #4a9b9b);; color: #ffffff; border-top-right-radius: 13px !important;">Select
								sub category</th>
						</tr>
						<tr>
							<td colspan="2" height="30" align="left">
								<div id="radiodiv"></div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" value="Done"
								onclick="selectSubcategory()" class="btn btn-success btn-sm"
								style="margin-left: 38%; width: 21%; height: 100%; margin-top: 1%;" /></td>
						</tr>
					</table>

				</div>
			</div>
		</div>
	</div>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" valign="top"><table width="100%" border="0"
					align="center" cellpadding="0" cellspacing="0" class="txnf">
					<tr>
						<td align="left"><h2
								style="margin-bottom: 0px; background: linear-gradient(60deg, #425185, #4a9b9b); color: #fff;">Create
								A New Account</h2></td>
					</tr>
					<tr>
						<td align="left"><s:actionmessage
								class="success success-text" theme="simple" /></td>
					</tr>
					<tr>
						<td align="left" valign="top"><s:form action="signupMerchant"
								id="formname">
								<s:token />
								<div class="adduR">
									<span id="error2" style="color: #ff0000; font-size: 11px;"></span>
									<div class="adduTR">
										Select merchant/reseller Type<br>
										<div class="txtnew">
											<s:select name="userRoleType" id="userRoleType" headerKey="1"
												list="#{'merchant':'I am a Merchant','reseller':'I am a Reseller'}"
												class="signupdropdwn">
											</s:select>
										</div>
									</div>

									<div class="adduTR" id="tdIndustryType" style="display: block;">
										Industry Type<br>
										<div class="txtnew">
											<s:select name="industryCategory" id="industryCategory"
												headerKey="select" headerValue="-Select Industry Type-"
												value="{'-Select Industry Type-'}" list="industryCategory"
												class="signupdropdwn" autocomplete="off">
											</s:select>
										</div>
									</div>
									<div id="subcategorydiv" style="display: none;">
										<span>Industry sub category</span>
										<s:textfield id="subcategory" name="industrySubCategory"
											cssClass="signuptextfield" placeholder="Sub category"
											autocomplete="off" />
									</div>

									<div class="adduTR">
										Business Name<br>
										<div class="txtnew">
											<s:textfield id="businessName" name="businessName"
												cssClass="signuptextfield" placeholder="Business Name"
												autocomplete="off"
												onkeypress="return ValidateBussinessName(event);" />
										</div>
									</div>
									<div class="adduTR">
										Email<br>
										<div class="txtnew">
											<s:textfield id="emailId" name="emailId"
												cssClass="signuptextfield" placeholder="Email"
												autocomplete="off" onblur="isValidEmail()" />
										</div>
									</div>
									<div class="adduTR">
										Phone<br>
										<div class="txtnew">
											<s:textfield id="mobile" name="mobile"
												cssClass="signuptextfield" placeholder="Phone"
												autocomplete="off"
												onkeypress="javascript:return isNumber (event)" />
										</div>
									</div>
									<div class="adduTR">
										Password<br>
										<div class="txtnew">
											<s:textfield id="password" name="password" type="password"
												cssClass="signuptextfield" placeholder="Password"
												onblur="passCheck()" autocomplete="off" />
										</div>
									</div>
									<div class="adduTR">
										Confirm Password<br>
										<div class="txtnew">
											<s:textfield id="confirmPassword" name="confirmPassword"
												type="password" cssClass="signuptextfield"
												placeholder="Confirm Password" onblur="passCheck()"
												autocomplete="off" />
										</div>
									</div>
									<div class="adduTR"
										style="text-align: center; padding: 14px 0 0 0;">
										<s:submit value="Sign Up" method="submit"
											cssClass="signupbutton btn-primary"
											style="background:linear-gradient(60deg, #425185, #4a9b9b);">
										</s:submit>
										<br>
									</div>
									<div class="clear"></div>
								</div>
							</s:form></td>
					</tr>
					<tr>
						<td align="center" valign="top">&nbsp;</td>
					</tr>
				</table></td>
		</tr>
	</table>
	<script>
		$(document)
				.ready(
						function() {

							var fields = {

								password : {
									tooltip : "Password must be minimum 8 and <br> maximum 32 characters long, with <br> special characters (! @ , _ + / =) , <br> at least one uppercase and  one <br>lower case alphabet.",
									position : 'right',
									backgroundColor : "#6ad0f6",
									color : '#FFFFFF'
								},
							};

							//Include Global Color 
							$("#formname").formtoolip(fields, {
								backgroundColor : "#000000",
								color : "#FFFFFF",
								fontSize : 14,
								padding : 10,
								borderRadius : 5
							});

						});
	</script>
</body>
</html>