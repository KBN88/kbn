<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Charging Platform</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popup.css" />
<script type="text/javascript">

$(document).ready(function() {
	   $('#acquirer').change(function(event){
		   var merchants = $("select#merchants").val();
		   var acquirer = $("select#acquirer").val();

		   if(acquirer==null ||acquirer=="" ||merchants==null ||merchants==""){
			   return false;
			 }
		   document.getElementById("chargingdetailform").submit();
	   });

   $('#merchants').change(function(event){
	   var acquirer = document.getElementById("acquirer").value;
	  	   if(acquirer==null||acquirer==""){
	  		   return false;
	  	   }
	  // window.location.href="chargingPlatformAction";
	   document.getElementById("chargingdetailform").submit();	
    });
   
/*    $('#updateServiceTax').click(function(event){
	 updateServiceTax();
   }); */
   
   $('#updateServiceTax').click(function(){
       $('#popup').popup('show');
  });
});

var editMode;

function editCurrentRow(divId,curr_row,ele){
	var div = document.getElementById(divId);

	var table = div.getElementsByTagName("table")[0];

	var merchantId = document.getElementById("merchants").value;
	var acquirer = document.getElementById("acquirer").value;
	var rows = table.rows;
	var currentRowNum = Number(curr_row);
	var currentRow = rows[currentRowNum];
	var cells = currentRow.cells;
	var cell0 = cells[0];
	var cell1 = cells[1];
	var cell2 = cells[2];
	var cell3 = cells[3]; //+3 ignoring the first three columns of the table

	var cell4 =  cells[4].children[0];
	var cell5 =  cells[4].children[1];
	var cell6 =  cells[5].children[0];
	var cell7 =  cells[5].children[1];
	var cell8 =  cells[6].children[0];
	var cell9 =  cells[6].children[1];
	var cell10 = cells[7].children[0];
	var cell11 = cells[7].children[1];
	var cell12 = cells[8].children[0];
	var cell13 = cells[8].children[1];

	var cell14 = cells[9].children[0];
	var cell15 = cells[9].children[1];;
	
	var cell16 = cells[10];
	var cell17 = cells[11];
	var cell18 = cells[12];

	var cell0Val = cell0.innerText;
	var cell1Val = cell1.innerText;
	var cell2Val = cell2.innerText;
	var cell3Val = cell3.innerText;
	var cell4Val = cell4.innerText.trim();
	var cell5Val = cell5.innerText.trim();
	var cell6Val = cell6.innerText.trim();
	var cell7Val = cell7.innerText.trim();
	var cell8Val = cell8.innerText.trim();
	var cell9Val = cell9.innerText.trim();
	var cell10Val = cell10.innerText.trim();
	var cell11Val = cell11.innerText.trim();
	var cell12Val = cell12.innerText.trim();
	var cell13Val = cell13.innerText.trim();
	var cell14Val = cell14.innerText.trim();
	var cell15Val = cell15.innerText.trim();
	var cell16Val = cell16.innerText.trim();
	var cell17Val = cell17.innerText.trim();
	
	var cell18Val = cell18.querySelector('input[type=checkbox]').checked;
	var id = cells[14].innerText;

	if(ele.value=="edit"){
		if(editMode) 
		{
				alert('Please edit the current row to proceed');
				return;
		}
		
		$('body').on('change', '#cell4Val, #cell8Val', function() {
		    $('#cell12Val').val(parseFloat($('#cell4Val').val())+parseFloat($('#cell8Val').val()));
		});
		$('body').on('change', '#cell5Val, #cell9Val', function() {
		    $('#cell13Val').val(parseFloat($('#cell5Val').val())+parseFloat($('#cell9Val').val()));
		});
		$('body').on('change', '#cell6Val, #cell10Val', function() {
		    $('#cell14Val').val(parseFloat($('#cell6Val').val())+parseFloat($('#cell10Val').val()));
		});
		$('body').on('change', '#cell7Val, #cell11Val', function() {
		    $('#cell15Val').val(parseFloat($('#cell7Val').val())+parseFloat($('#cell11Val').val()));
		});
		
		ele.value="save";
		ele.className ="btn btn-success btn-xs";
		cell4.innerHTML = "<input type='number' id='cell4Val'   class='chargingplatform' min='1'  step='0.0' value="+cell4Val+"></input>";
		cell5.innerHTML = "<input type='number' id='cell5Val'   class='chargingplatform' min='1' step='0.0' value="+cell5Val+"></input>";
		cell6.innerHTML = "<input type='number' id='cell6Val'   class='chargingplatform' min='1' step='0.0' value="+cell6Val+"></input>";
		cell7.innerHTML = "<input type='number' id='cell7Val'   class='chargingplatform' min='1' step='0.0' value="+cell7Val+"></input>";
		cell8.innerHTML = "<input type='number' id='cell8Val'   class='chargingplatform' min='1' step='0.0' value="+cell8Val+"></input>";
		cell9.innerHTML = "<input type='number' id='cell9Val'   class='chargingplatform' min='1' step='0.0' value="+cell9Val+"></input>";
		cell10.innerHTML = "<input type='number' id='cell10Val' class='chargingplatform' min='1' step='0.0' value="+cell10Val+"></input>";		
		cell11.innerHTML = "<input type='number' id='cell11Val' class='chargingplatform' min='1' step='0.0' value="+cell11Val+"></input>";
		cell12.innerHTML = "<input type='number' id='cell12Val' class='chargingplatform' min='1' step='0.0' value="+cell12Val+"></input>";
		cell13.innerHTML = "<input type='number' id='cell13Val' class='chargingplatform' min='1' step='0.0' value="+cell13Val+"></input>";
		cell14.innerHTML = "<input type='number' id='cell14Val' class='chargingplatform' min='1' step='0.0' value="+cell14Val+"></input>";
		cell15.innerHTML = "<input type='number' id='cell15Val' class='chargingplatform' min='1' step='0.0' value="+cell15Val+"></input>";
		cell16.innerHTML = "<input type='number' id='cell16Val' class='chargingplatform' min='1' step='0.0' value="+cell16Val+"></input>";
		cell17.innerHTML = "<input type='number' id='cell17Val' class='chargingplatform' min='1' step='0.0' value="+cell17Val+"></input>";
		
		cell18.innerHTML = "";
		if(cell18Val){
			cell18.innerHTML = "<input type='checkbox' id='cell18Val' checked='true'></input>";
		}else{
		    cell18.innerHTML = "<input type='checkbox' id='cell18Val'></input>";
		}
		editMode = true;
	}
	else{
		var pgTdr = document.getElementById('cell4Val').value;
		var bankTdr = document.getElementById('cell8Val').value;
		var merchantTdr = document.getElementById('cell12Val').value;
		var serviceTax = document.getElementById('cell16Val').value;
		//validation
		 if(parseFloat(bankTdr) == 0.0 || parseFloat(merchantTdr) == 0.0 || parseFloat(serviceTax) == 0.0 ||
				(((parseFloat(pgTdr) + parseFloat(bankTdr))).toFixed(2) != parseFloat(merchantTdr).toFixed(2))){
			alert("Enter proper values for mapping before saving");
			return false;
		}

		cell4.innerHTML = pgTdr;
		console.log(cell4.innerHTML.vlaue);
		cell5.innerHTML = document.getElementById('cell5Val').value;
		cell6.innerHTML = document.getElementById('cell6Val').value;
		cell7.innerHTML = document.getElementById('cell7Val').value;
		cell8.innerHTML = bankTdr;
		cell9.innerHTML = document.getElementById('cell9Val').value;
		cell10.innerHTML = document.getElementById('cell10Val').value;
		cell11.innerHTML = document.getElementById('cell11Val').value;
		cell12.innerHTML = merchantTdr;
		cell13.innerHTML = document.getElementById('cell13Val').value;
		cell14.innerHTML = document.getElementById('cell14Val').value;
		cell15.innerHTML = document.getElementById('cell15Val').value;
		cell16.innerHTML = serviceTax;
		cell17.innerHTML = document.getElementById('cell17Val').value;
		editMode = false;
		var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		var loginUserEmailId = "<s:property value='%{#session.USER.EmailId}'/>";
		ele.value="edit";
		ele.className ="btn btn-info btn-xs";		
		var token  = document.getElementsByName("token")[0].value;
		$.ajax({
			type: "POST",
			url:"editChargingDetail",
			data:{"id":id,"emailId":merchantId, "acquirer":acquirer, "paymentType":cell0Val, "mopType":cell2Val, "transactionType":cell3Val, "pgTDR":cell4.innerHTML,
				   "pgTDRAFC":cell5.innerHTML, "pgFixCharge":cell6.innerHTML, "pgFixChargeAFC":cell7.innerHTML, "bankTDR":cell8.innerHTML, "bankTDRAFC":cell9.innerHTML,
				   "bankFixCharge":cell10.innerHTML, "bankFixChargeAFC":cell11.innerHTML, "merchantTDR":cell12.innerHTML, "merchantTDRAFC":cell13.innerHTML,
				   "merchantFixCharge":cell14.innerHTML,  "merchantFixChargeAFC":cell15.innerHTML, "merchantServiceTax":cell16.innerHTML,"fixChargeLimit":cell17.innerHTML,
				   "allowFixCharge":cell18Val, "token":token,"struts.token.name": "token",  "currency":cell1Val ,"userType":userType, "loginUserEmailId":loginUserEmailId},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				//TODO....clean values......using script to avoid page refresh
				window.location.reload();
		    },
			error:function(data){
				alert("Network error, charging detail may not be saved");
			}
		});
	}
}

function editAllRows(divId,curr_row,ele){

	var table = document.getElementById('editAllTable');

	var merchantId = document.getElementById("merchants").value;
	var acquirer = document.getElementById("acquirer").value;
	var rows = table.rows;
	var currentRowNum = Number(curr_row);
	var currentRow = rows[currentRowNum];
	var cells = currentRow.cells;
	var cell0 = cells[0];
	var cell1 = cells[1];
	var cell2 = cells[2];
	var cell3 = cells[3]; //+3 ignoring the first three columns of the table

	var cell4 =  cells[4].children[0];
	var cell5 =  cells[4].children[1];
	var cell6 =  cells[5].children[0];
	var cell7 =  cells[5].children[1];
	var cell8 =  cells[6].children[0];
	var cell9 =  cells[6].children[1];
	var cell10 = cells[7].children[0];
	var cell11 = cells[7].children[1];
	var cell12 = cells[8].children[0];
	var cell13 = cells[8].children[1];

	var cell14 = cells[9].children[0];
	var cell15 = cells[9].children[1];;
	
	var cell16 = cells[10];
	var cell17 = cells[11];
	var cell18 = cells[12];

	var cell0Val = cell0.innerText;
	var cell1Val = cell1.innerText;
	var cell2Val = cell2.innerText;
	var cell3Val = cell3.innerText;
	var cell4Val = cell4.innerText.trim();
	var cell5Val = cell5.innerText.trim();
	var cell6Val = cell6.innerText.trim();
	var cell7Val = cell7.innerText.trim();
	var cell8Val = cell8.innerText.trim();
	var cell9Val = cell9.innerText.trim();
	var cell10Val = cell10.innerText.trim();
	var cell11Val = cell11.innerText.trim();
	var cell12Val = cell12.innerText.trim();
	var cell13Val = cell13.innerText.trim();
	var cell14Val = cell14.innerText.trim();
	var cell15Val = cell15.innerText.trim();
	var cell16Val = cell16.innerText.trim();
	var cell17Val = cell17.innerText.trim();
	
	var cell18Val = cell18.querySelector('input[type=checkbox]').checked;
	var id = cells[14].innerText;

	if(ele.value==="Edit All"){
		if(editMode) 
		{
				alert('Please edit the current row to proceed');
				return;
		}
		
		$('body').on('change', '#cell4Val, #cell8Val', function() {
		    $('#cell12Val').val(parseFloat($('#cell4Val').val())+parseFloat($('#cell8Val').val()));
		});
		$('body').on('change', '#cell5Val, #cell9Val', function() {
		    $('#cell13Val').val(parseFloat($('#cell5Val').val())+parseFloat($('#cell9Val').val()));
		});
		$('body').on('change', '#cell6Val, #cell10Val', function() {
		    $('#cell14Val').val(parseFloat($('#cell6Val').val())+parseFloat($('#cell10Val').val()));
		});
		$('body').on('change', '#cell7Val, #cell11Val', function() {
		    $('#cell15Val').val(parseFloat($('#cell7Val').val())+parseFloat($('#cell11Val').val()));
		});
		
		ele.value="Save All";
		ele.className ="btn btn-success btn-xs";
		cell4.innerHTML = "<input type='number' id='cell4Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell4Val+"></input>";
		cell5.innerHTML = "<input type='number' id='cell5Val'   class='chargingplatform'  min='1'  step='0.0' class='chargingplatform' value="+cell5Val+"></input>";
		cell6.innerHTML = "<input type='number' id='cell6Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell6Val+"></input>";
		cell7.innerHTML = "<input type='number' id='cell7Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell7Val+"></input>";
		cell8.innerHTML = "<input type='number' id='cell8Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell8Val+"></input>";
		cell9.innerHTML = "<input type='number' id='cell9Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell9Val+"></input>";
		cell10.innerHTML = "<input type='number' id='cell10Val' class='chargingplatform'  min='1'  step='0.0' value="+cell10Val+"></input>";		
		cell11.innerHTML = "<input type='number' id='cell11Val' class='chargingplatform'  min='1'  step='0.0' value="+cell11Val+"></input>";
		cell12.innerHTML = "<input type='number' id='cell12Val' class='chargingplatform'  min='1'  step='0.0' value="+cell12Val+"></input>";
		cell13.innerHTML = "<input type='number' id='cell13Val' class='chargingplatform'  min='1'  step='0.0' value="+cell13Val+"></input>";
		cell14.innerHTML = "<input type='number' id='cell14Val' class='chargingplatform'  min='1'  step='0.0' value="+cell14Val+"></input>";
		cell15.innerHTML = "<input type='number' id='cell15Val' class='chargingplatform'  min='1'  step='0.0' value="+cell15Val+"></input>";
		cell16.innerHTML = "<input type='number' id='cell16Val' class='chargingplatform'  min='1'  step='0.0' value="+cell16Val+"></input>";
		cell17.innerHTML = "<input type='number' id='cell17Val' class='chargingplatform'  min='1'  step='0.0' value="+cell17Val+"></input>";

		cell18.innerHTML = "";
		if(cell18Val){
			cell18.innerHTML = "<input type='checkbox' id='cell18Val' checked='true'></input>";
		}else{
		    cell18.innerHTML = "<input type='checkbox' id='cell18Val'></input>";
		}
		editMode = true;
	}
	else{
		var pgTdr = document.getElementById('cell4Val').value;
		var bankTdr = document.getElementById('cell8Val').value;
		var merchantTdr = document.getElementById('cell12Val').value;
		var serviceTax = document.getElementById('cell16Val').value;
		//validation
		 if(parseFloat(bankTdr) == 0.0 || parseFloat(merchantTdr) == 0.0 || parseFloat(serviceTax) == 0.0 ||
					(((parseFloat(pgTdr) + parseFloat(bankTdr))).toFixed(2) != parseFloat(merchantTdr).toFixed(2))){
				alert("Enter proper values for mapping before saving");
				return false;
		 }
		cell4.innerHTML = pgTdr;
		cell5.innerHTML = document.getElementById('cell5Val').value;
		cell6.innerHTML = document.getElementById('cell6Val').value;
		cell7.innerHTML = document.getElementById('cell7Val').value;
		cell8.innerHTML = bankTdr;
		cell9.innerHTML = document.getElementById('cell9Val').value;
		cell10.innerHTML = document.getElementById('cell10Val').value;
		cell11.innerHTML = document.getElementById('cell11Val').value;
		cell12.innerHTML = merchantTdr;
		cell13.innerHTML = document.getElementById('cell13Val').value;
		cell14.innerHTML = document.getElementById('cell14Val').value;
		cell15.innerHTML = document.getElementById('cell15Val').value;
		cell16.innerHTML = serviceTax;
		cell17.innerHTML = document.getElementById('cell17Val').value;
		var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		editMode = false;
		

		ele.value="Edit All";
		ele.className ="btn btn-info btn-xs";	
		var token  = document.getElementsByName("token")[0].value;

		$.ajax({
			type: "POST",
			url:"editAllChargingDetail",
			data:{"id":id,"emailId":merchantId, "acquirer":acquirer, "paymentType":cell0Val, "pgTDR":cell4.innerHTML,
				   "pgTDRAFC":cell5.innerHTML, "pgFixCharge":cell6.innerHTML, "pgFixChargeAFC":cell7.innerHTML, "bankTDR":cell8.innerHTML, "bankTDRAFC":cell9.innerHTML,
				   "bankFixCharge":cell10.innerHTML, "bankFixChargeAFC":cell11.innerHTML, "merchantTDR":cell12.innerHTML, "merchantTDRAFC":cell13.innerHTML,
				   "merchantFixCharge":cell14.innerHTML,  "merchantFixChargeAFC":cell15.innerHTML, "merchantServiceTax":cell16.innerHTML,"fixChargeLimit":cell17.innerHTML,
				   "allowFixCharge":cell18Val, "token":token,"struts.token.name": "token",  "currency":cell1Val , "userType":userType},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				window.location.reload();
		    },
			error:function(data){
				alert("Network error, charging detail may not be saved");
			}
		});
	}
}

function cancel(curr_row,ele){
	var parentEle = ele.parentNode;
	
	if(editMode){
	 	window.location.reload();
	}
}

function updateServiceTax(){
	var newValue = document.getElementById('newServiceTaxValue').value;
	var token  = document.getElementsByName("token")[0].value;
	var button = document.getElementById('updateserviceTaxButton');
	
	$.ajax({
		type: "POST",
		url:"updateServiceTax",
		data:{"newServiceTax":newValue, "token":token},
		success:function(data){
			var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
			if(null!=response){
				alert(response);			
			}
			$('#popup').popup('hide');
	    },
		error:function(data){
			alert("Network error, charging detail may not be saved");
			$('#popup').popup('hide');
		}
	});
}


// Wallet 

function editAllRowsWallet(divId,curr_row,ele){

	var table = document.getElementById('editAllTableWallet');

	var merchantId = document.getElementById("merchants").value;
	var acquirer = document.getElementById("acquirer").value;
	var rows = table.rows;
	var currentRowNum = Number(curr_row);
	var currentRow = rows[currentRowNum];
	var cells = currentRow.cells;
	var cell0 = cells[0];
	var cell1 = cells[1];
	var cell2 = cells[2];
	var cell3 = cells[3]; //+3 ignoring the first three columns of the table

	var cell4 =  cells[4].children[0];
	var cell5 =  cells[4].children[1];
	var cell6 =  cells[5].children[0];
	var cell7 =  cells[5].children[1];
	var cell8 =  cells[6].children[0];
	var cell9 =  cells[6].children[1];
	var cell10 = cells[7].children[0];
	var cell11 = cells[7].children[1];
	var cell12 = cells[8].children[0];
	var cell13 = cells[8].children[1];

	var cell14 = cells[9].children[0];
	var cell15 = cells[9].children[1];;
	
	var cell16 = cells[10];
	var cell17 = cells[11];
	var cell18 = cells[12];

	var cell0Val = cell0.innerText;
	var cell1Val = cell1.innerText;
	var cell2Val = cell2.innerText;
	var cell3Val = cell3.innerText;
	var cell4Val = cell4.innerText.trim();
	var cell5Val = cell5.innerText.trim();
	var cell6Val = cell6.innerText.trim();
	var cell7Val = cell7.innerText.trim();
	var cell8Val = cell8.innerText.trim();
	var cell9Val = cell9.innerText.trim();
	var cell10Val = cell10.innerText.trim();
	var cell11Val = cell11.innerText.trim();
	var cell12Val = cell12.innerText.trim();
	var cell13Val = cell13.innerText.trim();
	var cell14Val = cell14.innerText.trim();
	var cell15Val = cell15.innerText.trim();
	var cell16Val = cell16.innerText.trim();
	var cell17Val = cell17.innerText.trim();
	
	var cell18Val = cell18.querySelector('input[type=checkbox]').checked;
	var id = cells[14].innerText;

	if(ele.value==="Edit All"){
		if(editMode) 
		{
				alert('Please edit the current row to proceed');
				return;
		}
		
		$('body').on('change', '#cell4Val, #cell8Val', function() {
		    $('#cell12Val').val(parseFloat($('#cell4Val').val())+parseFloat($('#cell8Val').val()));
		});
		$('body').on('change', '#cell5Val, #cell9Val', function() {
		    $('#cell13Val').val(parseFloat($('#cell5Val').val())+parseFloat($('#cell9Val').val()));
		});
		$('body').on('change', '#cell6Val, #cell10Val', function() {
		    $('#cell14Val').val(parseFloat($('#cell6Val').val())+parseFloat($('#cell10Val').val()));
		});
		$('body').on('change', '#cell7Val, #cell11Val', function() {
		    $('#cell15Val').val(parseFloat($('#cell7Val').val())+parseFloat($('#cell11Val').val()));
		});
		
		ele.value="Save All";
		ele.className ="btn btn-success btn-xs";
		cell4.innerHTML = "<input type='number' id='cell4Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell4Val+"></input>";
		cell5.innerHTML = "<input type='number' id='cell5Val'   class='chargingplatform'  min='1'  step='0.0' class='chargingplatform' value="+cell5Val+"></input>";
		cell6.innerHTML = "<input type='number' id='cell6Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell6Val+"></input>";
		cell7.innerHTML = "<input type='number' id='cell7Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell7Val+"></input>";
		cell8.innerHTML = "<input type='number' id='cell8Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell8Val+"></input>";
		cell9.innerHTML = "<input type='number' id='cell9Val'   class='chargingplatform'  min='1'  step='0.0' value="+cell9Val+"></input>";
		cell10.innerHTML = "<input type='number' id='cell10Val' class='chargingplatform'  min='1'  step='0.0' value="+cell10Val+"></input>";		
		cell11.innerHTML = "<input type='number' id='cell11Val' class='chargingplatform'  min='1'  step='0.0' value="+cell11Val+"></input>";
		cell12.innerHTML = "<input type='number' id='cell12Val' class='chargingplatform'  min='1'  step='0.0' value="+cell12Val+"></input>";
		cell13.innerHTML = "<input type='number' id='cell13Val' class='chargingplatform'  min='1'  step='0.0' value="+cell13Val+"></input>";
		cell14.innerHTML = "<input type='number' id='cell14Val' class='chargingplatform'  min='1'  step='0.0' value="+cell14Val+"></input>";
		cell15.innerHTML = "<input type='number' id='cell15Val' class='chargingplatform'  min='1'  step='0.0' value="+cell15Val+"></input>";
		cell16.innerHTML = "<input type='number' id='cell16Val' class='chargingplatform'  min='1'  step='0.0' value="+cell16Val+"></input>";
		cell17.innerHTML = "<input type='number' id='cell17Val' class='chargingplatform'  min='1'  step='0.0' value="+cell17Val+"></input>";

		cell18.innerHTML = "";
		if(cell18Val){
			cell18.innerHTML = "<input type='checkbox' id='cell18Val' checked='true'></input>";
		}else{
		    cell18.innerHTML = "<input type='checkbox' id='cell18Val'></input>";
		}
		editMode = true;
	}
	else{
		var pgTdr = document.getElementById('cell4Val').value;
		var bankTdr = document.getElementById('cell8Val').value;
		var merchantTdr = document.getElementById('cell12Val').value;
		var serviceTax = document.getElementById('cell16Val').value;
		//validation
		 if(parseFloat(bankTdr) == 0.0 || parseFloat(merchantTdr) == 0.0 || parseFloat(serviceTax) == 0.0 ||
					(((parseFloat(pgTdr) + parseFloat(bankTdr))).toFixed(2) != parseFloat(merchantTdr).toFixed(2))){
				alert("Enter proper values for mapping before saving");
				return false;
		 }
		cell4.innerHTML = pgTdr;
		cell5.innerHTML = document.getElementById('cell5Val').value;
		cell6.innerHTML = document.getElementById('cell6Val').value;
		cell7.innerHTML = document.getElementById('cell7Val').value;
		cell8.innerHTML = bankTdr;
		cell9.innerHTML = document.getElementById('cell9Val').value;
		cell10.innerHTML = document.getElementById('cell10Val').value;
		cell11.innerHTML = document.getElementById('cell11Val').value;
		cell12.innerHTML = merchantTdr;
		cell13.innerHTML = document.getElementById('cell13Val').value;
		cell14.innerHTML = document.getElementById('cell14Val').value;
		cell15.innerHTML = document.getElementById('cell15Val').value;
		cell16.innerHTML = serviceTax;
		cell17.innerHTML = document.getElementById('cell17Val').value;
		var userType = "<s:property value='%{#session.USER.UserType.name()}'/>";
		editMode = false;
		

		ele.value="Edit All";
		ele.className ="btn btn-info btn-xs";	
		var token  = document.getElementsByName("token")[0].value;

		$.ajax({
			type: "POST",
			url:"editAllChargingDetailWallet",
			data:{"id":id,"emailId":merchantId, "acquirer":acquirer, "paymentType":cell0Val, "pgTDR":cell4.innerHTML,
				   "pgTDRAFC":cell5.innerHTML, "pgFixCharge":cell6.innerHTML, "pgFixChargeAFC":cell7.innerHTML, "bankTDR":cell8.innerHTML, "bankTDRAFC":cell9.innerHTML,
				   "bankFixCharge":cell10.innerHTML, "bankFixChargeAFC":cell11.innerHTML, "merchantTDR":cell12.innerHTML, "merchantTDRAFC":cell13.innerHTML,
				   "merchantFixCharge":cell14.innerHTML,  "merchantFixChargeAFC":cell15.innerHTML, "merchantServiceTax":cell16.innerHTML,"fixChargeLimit":cell17.innerHTML,
				   "allowFixCharge":cell18Val, "token":token,"struts.token.name": "token",  "currency":cell1Val , "userType":userType},
			success:function(data){
				var response = ((data["Invalid request"] != null) ? (data["Invalid request"].response[0]) : (data.response));
				if(null!=response){
					alert(response);			
				}
				window.location.reload();
		    },
			error:function(data){
				alert("Network error, charging detail may not be saved");
			}
		});
	}
}

function cancel(curr_row,ele){
	var parentEle = ele.parentNode;
	
	if(editMode){
	 	window.location.reload();
	}
}


</script>
<style>
.product-spec input[type=text] {
	width: 35px;
}
</style>
</head>
<body>
<s:actionmessage class="error error-new-text" />
<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="txnf" >
<tr>
<td>
<div>
<!-- <input type="button" value="Update Service Tax" id="updateServiceTax" class="btn btn-success btn-md" style="display: inline;margin-top:1%;width:16%;margin-left:1%;font-size: 15px;margin-bottom:1%;"></input> -->
	<div id="popup" style="display: none;">
		<div class="modal-dialog" style="width: 400px;">

			<!-- Modal content-->
			<div class="modal-content"
				style="background-color: transparent; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">
				<div id="1" class="modal-body"
					style="background-color: #ffffff; border-radius: 13px; -webkit-box-shadow: 0px 0px 0px 0px; -moz-box-shadow: 0px 0px 0px 0px; box-shadow: 0px 0px 0px 0px; box-shadow: 0px;">

					<table class="detailbox table98" cellpadding="20">
					   <tr>
          <td align="left" valign="top">
          <div class="button-position2">
              <button class="popup_close closepopupbtn"></button>
          </div>
         </td>
        </tr>
						<tr>
							<th colspan="2" width="16%" height="30" align="left"
								style="background-color: #2b6dd1; color: #ffffff; border-top-right-radius: 13px !important;">Update Service Tax</th>
						</tr>						
						<tr>
							<td width="7%">New Service Tax Value</td>
							<td width="30%"><input id="newServiceTaxValue" type="text" placeholder="Value without % symbol" maxlength="15"
								name="ipAddress" value="" class="form-control" /></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" value="Update" id="updateserviceTaxButton"
								onclick="updateServiceTax()" class="btn btn-success btn-sm" style="margin-left:38%;width:21%;height:100%;margin-top:1%;" /></td>
						</tr>
						
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</td>
</tr>
</table>
	<s:form id="chargingdetailform" action="chargingPlatformAction"
		method="post">
		<div class="card ">
			  <div class="card-header card-header-rose card-header-text">
					<div class="card-text">
					  <h4 class="card-title">Charging Platform</h4>
					</div>
				  </div>
				    <div class="card-body ">
				    <div class="container">
				      <div class="row">
				      <div class="col-sm-6 col-lg-3">
				      <label style="float: left;">Select User : </label><br>
				      <div class="txtnew">
                        <s:select headerKey="-1" headerValue="Select User"
						list="#{'1':'Merchant'}" id="user" name="user" value="1"
						class="form-control" autocomplete="off" />
								</div>
				      
				    </div>
				      <div class="col-sm-6 col-lg-3">
				      <label style="float: left;">Select Merchant :</label><br>
				      <div class="txtnew">
				      <s:select headerValue="Select Merchant" headerKey=""
											name="emailId" class="form-control" id="merchants"
											list="listMerchant" listKey="emailId"
											listValue="businessName" autocomplete="off" />
				      </div>
				    </div>
				      <div class="col-sm-6 col-lg-3">
								<label style="float: left;">Select Acquirer :</label><br>
								 <div class="txtnew">
								 <s:select class="form-control" headerKey=""
											headerValue="Select Acquirer"
											list="@com.kbn.pg.core.AcquirerType@values()"
											listKey="code" listValue="name" name="acquirer" id="acquirer"
											autocomplete="off" />
								</div>
								</div>
				    </div>
				    </div>
				    
				    </div>

		
		</div>

		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="txnf" style="margin-top:1%;">
			<tr>
				<td align="center" valign="top"><table width="98%" border="0"
						cellspacing="0" cellpadding="0">
						
						<tr>
							<td align="left"><div id="datatable" class="scrollD">
									<s:iterator value="aaData" status="pay">
										<br>
										<span class="text-primary"><strong><s:property
													value="key" /></strong></span>
										<br>
										<s:property value="paymentType" />
										<div style="margin-bottom: 0px;">										
											<s:if test='key.equals("Net Banking")'>
											<div>Update All</div>
												<s:div id="divAll" class="scrollD">
													<table width="100%" border="0" align="center"
														class="product-spec" id="editAllTable">
														<tr class="boxheading">
															<th width="5%" align="left" valign="middle"
																style="display: none">Payment</th>
															<th width="6%" align="left" valign="middle">Currency</th>
															<th width="4%" align="left" valign="middle">Mop</th>
															<th width="8%" align="left" valign="middle">Transaction</th>
															<th width="7%" align="left" valign="middle">PG
																TDR</th>
															<th width="6%" align="left" valign="middle">PG
																FC</th>
															<th width="7%" align="left" valign="middle">Bank TDR</th>
															<th width="6%" align="left" valign="middle">Bank FC</th>
															<th width="9%" align="left" valign="middle">Merchant
																TDR</th>
															<th width="5%" align="left" valign="middle">Merchant
																FC</th>
															<th width="7%" align="left" valign="middle">Merchant
																S Tax</th>
															<th width="6%" align="left" valign="middle">FC Limit</th>
															<th width="6%" align="left" valign="middle">Allow FC</th>
															<th width="5%" align="left" valign="middle">Update</th>
															<th width="2%" align="left" valign="middle"
																style="display: none">id</th>
															<th width="5%" align="left" valign="middle">Cancel</th>
														</tr>
														<tr class="boxtext">
															<td rowspan="2" align="left" valign="middle"
																style="display: none">NET_BANKING</td>
															<td width="60" rowspan="2" align="left" valign="middle">356</td>
															<td width="310" rowspan="2" align="left" valign="middle">ALL
																BANKS</td>
															<td width="70" rowspan="2" align="left" valign="middle">Transaction</td>
															<td width="50" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="32" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="32" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="58" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="55" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="60" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="70" rowspan="2" align="left" valign="middle">0.0</td>
															<td width="30" rowspan="2" align="left" valign="middle">0.0</td>
															<td width="40" rowspan="2" align="left" valign="middle"><s:checkbox
																	name="allowFixCharge" value="allowFixCharge"
																	onclick="clickOnOff" /></td>
															<td width="40" rowspan="2" align="left" valign="middle"><s:div>
																	<s:textfield id="edit" value="Edit All" type="button"
																		onclick="editAllRows('%{key +'Div'}',1, this)"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>

																	<s:textfield id="cancelBtn" value="cancel"
																		type="button" onclick="cancel(this)"
																		style="display:none" autocomplete="off"></s:textfield>
																</s:div></td>
															<td rowspan="2" align="left" valign="middle"
																style="display: none"><s:property value="id" /></td>
															<td rowspan="2" align="left" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="cancel"
																	type="button"
																	onclick="cancel('%{#itStatus.count}',this)"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</table>
												</s:div>
												<div>Update individual Bank</div><!-- For netbanking only -->
											</s:if>
										</div>
										<!-- Wallet -->
										
										<div style="margin-bottom: 0px;">										
											<s:if test='key.equals("Wallet")'>
											<div>Update All</div>
												<s:div id="divAllWallet" class="scrollD">
													<table width="100%" border="0" align="center"
														class="product-spec" id="editAllTableWallet">
														<tr class="boxheading">
															<th width="5%" align="left" valign="middle"
																style="display: none">Payment</th>
															<th width="6%" align="left" valign="middle">Currency</th>
															<th width="4%" align="left" valign="middle">Mop</th>
															<th width="8%" align="left" valign="middle">Transaction</th>
															<th width="7%" align="left" valign="middle">PG
																TDR</th>
															<th width="6%" align="left" valign="middle">PG
																FC</th>
															<th width="7%" align="left" valign="middle">Bank TDR</th>
															<th width="6%" align="left" valign="middle">Bank FC</th>
															<th width="9%" align="left" valign="middle">Merchant
																TDR</th>
															<th width="5%" align="left" valign="middle">Merchant
																FC</th>
															<th width="7%" align="left" valign="middle">Merchant
																S Tax</th>
															<th width="6%" align="left" valign="middle">FC Limit</th>
															<th width="6%" align="left" valign="middle">Allow FC</th>
															<th width="5%" align="left" valign="middle">Update</th>
															<th width="2%" align="left" valign="middle"style="display: none">id</th>
															<th width="5%" align="left" valign="middle">Cancel</th>
														</tr>
														<tr class="boxtext">
															<td rowspan="2" align="left" valign="middle"
																style="display: none">WALLET</td>
															<td width="60" rowspan="2" align="left" valign="middle">356</td>
															<td width="310" rowspan="2" align="left" valign="middle">ALL
																WALLET</td>
															<td width="70" rowspan="2" align="left" valign="middle">Transaction</td>
															<td width="50" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="32" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="32" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="58" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="55" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="60" align="left" valign="middle"><div
																	title="TDR below fix charge">&nbsp; 0.0</div>
																<div class="cellborder" title="TDR above fix charge">&nbsp;
																	0.0</div></td>
															<td width="70" rowspan="2" align="left" valign="middle">0.0</td>
															<td width="30" rowspan="2" align="left" valign="middle">0.0</td>
															<td width="40" rowspan="2" align="left" valign="middle"><s:checkbox
																	name="allowFixCharge" value="allowFixCharge"
																	onclick="clickOnOff" /></td>
															<td width="40" rowspan="2" align="left" valign="middle"><s:div>
																	<s:textfield id="edit" value="Edit All" type="button"
																		onclick="editAllRowsWallet('%{key +'Div'}',1, this)"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>

																	<s:textfield id="cancelBtn" value="cancel"
																		type="button" onclick="cancel(this)"
																		style="display:none" autocomplete="off"></s:textfield>
																</s:div></td>
															<td rowspan="2" align="left" valign="middle"
																style="display: none"><s:property value="id" /></td>
															<td rowspan="2" align="left" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="cancel"
																	type="button"
																	onclick="cancel('%{#itStatus.count}',this)"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</table>
												</s:div>
												<div>Update individual WALLET</div><!-- For netbanking only -->
											</s:if>
										</div>
										
										
										<div class="scrollD">
											<s:div id="%{key +'Div'}">
												<table width="100%" border="0" align="center"
													class="product-spec">
													<tr class="boxheading">
														<th width="5%" height="25" valign="middle"
															style="display: none">Payment</th>
														<th width="6%" align="left" valign="middle">Currency</th>
														<th width="4%" align="left" valign="middle">Mop</th>
														<th width="8%" align="left" valign="middle">Transaction</th>
														<th width="7%" align="left" valign="middle">PG
															TDR</th>
														<th width="6%" align="left" valign="middle">PG FC</th>
														<th width="7%" align="left" valign="middle">Bank TDR</th>
														<th width="6%" align="left" valign="middle">Bank FC</th>
														<th width="9%" align="left" valign="middle">Merchant
															TDR</th>
														<th width="8%" align="left" valign="middle">Merchant
															FC</th>
														<th width="10%" align="left" valign="middle">Merchant
															S Tax</th>
														<th width="6%" align="left" valign="middle">FC Limit</th>
														<th width="6%" align="left" valign="middle">Allow FC</th>
														<th width="5%" align="left" valign="middle">Update</th>
														<th width="2%" align="left" valign="middle"
															style="display: none">id</th>
														<th width="5%" align="left" valign="middle"><span
															id="cancelLabel">Cancel</span></th>
													</tr>
													<s:iterator value="value" status="itStatus">
														<tr class="boxtext">
															<td align="left" valign="middle" style="display: none"><s:property
																	value="paymentType" /></td>
															<td align="left" valign="middle"><s:property
																	value="currency" /></td>
															<td align="left" valign="middle"><s:property
																	value="mopType" /></td>
															<td align="left" valign="middle"><s:property
																	value="transactionType" /></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="pgTDR" />
																</div>
																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="pgTDRAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="pgFixCharge" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="pgFixChargeAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="bankTDR" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="bankTDRAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="bankFixCharge" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="bankFixChargeAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="merchantTDR" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="merchantTDRAFC" />
																</div></td>
															<td align="left" valign="middle" class="nomarpadng"><div
																	title="TDR below fix charge">
																	&nbsp;
																	<s:property value="merchantFixCharge" />
																</div>

																<div class="cellborder" title="TDR above fix charge">
																	&nbsp;
																	<s:property value="merchantFixChargeAFC" />
																</div></td>
															<td align="center" valign="middle"><div>
																	<s:property value="merchantServiceTax" />
																</div></td>
															<td align="center" valign="middle"><div>
																	<s:property value="fixChargeLimit" />
																</div></td>
															<td align="center" valign="middle"><s:checkbox
																	name="allowFixCharge" value="allowFixCharge"
																	onclick="return false" /></td>
															<td align="center" valign="middle"><s:div>
																	<s:textfield id="edit%{#itStatus.count}" value="edit"
																		type="button"
																		onclick="editCurrentRow('%{key +'Div'}','%{#itStatus.count}', this)"
																		class="btn btn-info btn-xs" autocomplete="off"></s:textfield>

																	<s:textfield id="cancelBtn%{#itStatus.count}"
																		value="cancel" type="button"
																		onclick="cancel('%{#itStatus.count}',this)"
																		style="display:none" autocomplete="off"></s:textfield>
																</s:div></td>
															<td align="center" valign="middle" style="display: none"><s:property
																	value="id" /></td>
															<td align="center" valign="middle"><s:textfield
																	id="cancelBtn%{#itStatus.count}" value="cancel"
																	type="button"
																	onclick="cancel('%{#itStatus.count}',this)"
																	class="btn btn-danger btn-xs" autocomplete="off"></s:textfield></td>
														</tr>
													</s:iterator>
												</table>
											</s:div>
										</div>
									</s:iterator>
								</div></td>
						</tr>
					</table></td>
			</tr>
		</table>
		<s:hidden name="token" value="%{#session.customToken}"></s:hidden>
	</s:form>
</body>
</html>