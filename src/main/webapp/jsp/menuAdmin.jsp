<%@taglib prefix="s" uri="/struts-tags"%>
<link href="../css/bootstrap.minr.css" rel="stylesheet">
<link href="../fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="../css/default.css" rel="stylesheet">
<link href="../css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="../css/navigation.css">
<link href="../css/welcomePage.css" rel="stylesheet">
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.dimensions.js"></script>
<script type="text/javascript" src="../js/jquery.accordion.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link rel="stylesheet" href="../css/loader.css">
<link rel="stylesheet" href="../css/newtheam.css">
<script>
	$.noConflict();
	// Code that uses other library's $ can follow here.
</script>
<script type="text/javascript">
	jQuery().ready(
			function() {
				// simple accordion
				jQuery('#list1a').accordion();
				jQuery('#list1b').accordion({
					autoheight : false
				});

				// second simple accordion with special markup
				jQuery('#navigation').accordion({
					active : false,
					header : '.head',
					navigation : true,
					event : 'click',
					fillSpace : false,
					animated : 'easeslide'
				});
				var notificationVal = document
						.getElementsByName("notification")[0].value;
				var notificationSpan = document
						.getElementById("notificationSpan");
				notificationSpan.innerHTML = notificationVal;
				/*  myFunction(); */

			});
	function myFunction() {
		var token = document.getElementsByName("token")[0].value;
		var payId = '<s:property value="#session.USER.payId" />';
		var dropdown= document.getElementById("dropdownMenu");
		/* if($('#dropdownMenu > option[value!=""]').length == 0) { */
		    //dropdown contains no non-null options
		 $('#dropdownMenu').val('');
		$.ajax({
			"url" : "notificationHandler",
			"type" : "POST",
			"data" : {
				payId : payId,
				token : token,

			},
			success : function(data) {
				/* dropdown.selectedIndex = -1; */
				var notificationMessage = data.message;
				/* var notificationSpan=	data.count;
				var notificationCount=	document.getElementById("notificationSpan");
				notificationCount.innerHTML=notificationSpan; */
				var jsArr = new Array();  
				var unreadId = new Array();
				/* var notification = document
						.getElementById("notificationMessage");
				notification.innerHTML = notificationMessage; */
				jsArr=data.messageList;
				unreadId= data.unreadNotification;
				var dropdown= document.getElementById("dropdownMenu");
				/* var size = dropdown.length;
				var value = dropdown.innerHTML;
				/* var length = dropdown.length; */
			/* 	var length2 = dropdown.option.length; */
				/* for (var i = 0; i < length; i++) {
					
					     selectobject.remove(i);
					  } */ 
					/*  select.options[i] = null; */
				
				for ( var i = 0; i <= jsArr.length; i++ ){
					
				      var option = document.createElement("option");
				      // set the text that displays for the option
				       option.innerHTML = jsArr[i];
				     // add the option to your select dropdown
				       dropdown.appendChild(option);

				   }
				readNotification(unreadId); 
				/* window.location.reload(); */
				
				
			},
			error : function(data) {
				alert("Something went wrong, so please try again.");
			}
		});
		/* } */
	}
	function readNotification(unreadId) {
		$.ajax({
			"url" : "readNotification",
			"type" : "POST",
			"data" : {
				message : JSON.stringify(unreadId),
				response:"test"
			},
			success : function(data) {
				var response = data.response;
			
			 	alert(response);  
			
			  /* var notificationVal = document
		.getElementsByName("notification")[0].value; */
var notificationSpan = document
		.getElementById("notificationSpan");
notificationSpan.innerHTML = 0; 
			},
			error : function(data) {
				
				alert("Something went wrong, so please try again."); 
			}
	});
	}
</script>

<div id="fade"></div>
<div id="modal" class="lodinggif">
	<img id="loader" src="../image/loader.gif" />
</div>
<div class="row leftnavigation">
	<div class="col-md-12 col-xs-12">
			
		<nav class="navbar navbar-default top-navbar" role="navigation">
		<div class="headerImage">
		<a class="" href="home"> <img
				src="../image/logo-newr.png" class=""></a>
				</div>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>


			</div>

			<ul class="nav navbar-top-links navbar-right">

				<li class="dropdown"><a class="dropdown-toggle" role="button"
					href="#" aria-expanded="false" data-toggle="dropdown"><img
						src="../image/notificationlogo.png" onclick="myFunction()"><span
						id="notificationSpan"></span></a>

					<ul class="dropdown-menu" id="dropdownMenu"background-color: #000;>




						<li><s:a action="#">
								<i class="fa fa-sign-out fa-fw" id="notificationMessage"></i>
							</s:a></li>


					</ul></li>


				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#" aria-expanded="false"> <i
						class="fa fa-user fa-fw"></i> <b>Welcome <s:property
								value="%{#session.USER.businessName}" /></b> <i
						class="fa fa-caret-down"></i>

				</a>


					<ul class="dropdown-menu dropdown-user">
						<li><s:a action="logout">
								<i class="fa fa-sign-out fa-fw"></i> Logout</s:a></li>
					</ul> <!-- /.dropdown-user --></li>
				<!-- /.dropdown -->
			</ul>
		</nav>
	</div>
</div>
<div class="col-md-2 left_col col-xs-12" id="wrapperr" style="margin:0px">

	<!-- sidebar menu -->

	<div>
		<!--/. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation">


			<div id="main" style="height: 100%">
				<div class="sidebar-collapse" style="height: 100%">
					<ul id="navigation" class="nav side-menu side-menubottom">
						<li><s:a action='home' class="head1">
								<i class="material-icons">dashboard</i> Dashboard</s:a></li>
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons" style="color:#fbe165;">account_balance_wallet</i> Merchant Setup
								<span class="fa fa-chevron-down"></span></a>

							<ul>
								<li><s:a action='merchantCrmSignup'>User Registration</s:a></li>
								<li><s:a action='merchantList'>Merchant Account</s:a></li>
								<li><s:a action='merchantSetup' class="sublinks"
										onclick='return false'>Merchant Setup</s:a></li>
								<li><s:a action='adminResellers'>Reseller Account</s:a></li>
								<li><s:a action='merchantSubUsers'>Merchant SubUsers</s:a></li>
								<li><s:a action='ruleEngine'>Rule Engine</s:a></li>
								<li><s:a action='routerConfiguration'>Smart router</s:a></li>
								<li><s:a action='#'>Smart router Audit Trail</s:a></li>
								<li><s:a action='#'>Rule Engine Audit Trail</s:a></li>
							</ul></li>



						<li><a style="cursor: pointer" class="head"><i
								class="material-icons" style="color: #f05050;">account_balance</i> Merchant Billing <span
								class="fa fa-chevron-down"></span></a>

							<ul>
								<li><s:a action='mopSetUpAction'>Merchant Mapping</s:a></li>
								<li><s:a action='serviceTaxPlatform'>GST</s:a></li>
								<li><s:a action='chargingPlatform'>TDR Setting</s:a></li>
								<li><s:a action='surchargePlatform'>Surcharge Setting</s:a></li>
								<li><s:a action='resellerMappingAction'>Reseller Mapping</s:a></li>
								<li><s:a action='pendingRequest'>Pending Request</s:a></li>
							</ul></li>
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons" style="color:#26c6da;">timeline</i>Analytics<span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="analyticsReports">Payment Methods</s:a></li>
								<li><s:a action="weeklyAnalytics"
										onclick="ajaxindicatorstart1()">Weekly Analytics</s:a></li>
								<li><s:a action="hourlyAnalytics"
										onclick="ajaxindicatorstart1()">Hourly Analytics</s:a></li>
							</ul></li>

						<li><a style="cursor: pointer" class="head"><i
								class="material-icons" style="color: #27c24c">repeat</i>Transaction Reports<span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action='authorizeTransaction'>Authorized Transactions</s:a></li>
								<li><s:a action='captureTransaction'> Sale Transactions</s:a></li>
								<li><s:a action='failedTransaction'>Failed Transactions</s:a></li>
								<li><s:a action='refundReport'>Refund Transactions</s:a></li>
								<li><s:a action='fraudTransaction'>Risk Transactions</s:a></li>
								<li><s:a action='summaryReport'>Summary Reports</s:a></li>
								<li><s:a action="settlementReport">Settled Report </s:a></li>
							</ul></li>
						<li><s:a action='transactionSearch' class="head1">
								<i class="material-icons" style="color:#00acc1">search</i> Search Payments </s:a></li>

						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-pencil-square-o"></i> Account & Finance <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action='misReports'>MIS Reports</s:a></li>
								<li><s:a action='downloadReports'>Download reports</s:a></li>
							</ul></li>

						<li><a style="cursor: pointer" class="head"><i
								class="material-icons" style="color: #528ff0">link</i>Link Payment<span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="createPaymentLink">Create Payment Link</s:a></li>
								<li><s:a action="invoiceSearch">Search PaymentLink</s:a></li>
							</ul></li>
						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-compress" style="color: #26c6da"></i> Remittance <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="viewRemittance">View Remittance </s:a></li>
								<li><s:a action="addRemittance">Add Remittance</s:a></li>
							</ul></li>

						<li><a style="cursor: pointer" class="head"><i
								class="material-icons">apps</i> Batch Operations <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="kotakRefund">Kotak </s:a></li>
								<li><s:a action="yesBankRefund">Yes Bank</s:a></li>
								<li><s:a action="manageBinRange">Manage Bin Ranges</s:a></li>
							</ul></li>
						<li><a style="cursor: pointer" class="head"><i
								class="material-icons" style="color: #f05050;">location_disabled</i>Risk Rules Engine
								 <span class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="riskRules">Create Risk Rules</s:a></li>
							</ul></li>
						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-users" style="color: #fbe165;"></i> Manage SubAdmin <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="addUser">Add SubAdmin </s:a></li>
								<li><s:a action="searchSubAdmin">SubAdmin List</s:a></li>
								<li><s:a action="subAdminList">Change Admin</s:a></li>
							</ul></li>

						<li><a style="cursor: pointer" class="head"><i
								class="material-icons" style="color: #ec407a">message</i>Bulk Email/Sms <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="bulkEmail">Send Bulk Email</s:a></li>
								<li><s:a action="bulkSms">Send Bulk Sms</s:a></li>
							</ul></li>
						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-circle-o-notch"></i> Chargeback Case<span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="viewChargeback">View Chargeback</s:a></li>
							</ul></li>
						<li><a style="cursor: pointer" class="head"><i
								class="fa fa-user" style="color: #528ff0;"></i> My Account <span
								class="fa fa-chevron-down"></span></a>
							<ul>
								<li><s:a action="adminProfile">My Profile </s:a></li>
								<li><s:a action="loginHistoryRedirect">Login History</s:a></li>
								<li><s:a action='passwordChange'>Change Password</s:a></li>
							</ul></li>
						<!-- ticketing -->
						<%-- 	<li><a style="cursor: pointer" class="head"><i
								class="fa fa-list-alt"></i>Help Ticket <span
								class="fa fa-chevron-down"></span></a>
							<ul>
							<li><s:a action="createTicket" >
								 Create Tickets </s:a>
								</li>
							<li><s:a action="viewAllTickets" >
								 View All Tickets </s:a>
								</li>
								</ul>
								</li> --%>
					</ul>
				</div>
			</div>




		</nav>
		<!-- /. NAV SIDE  -->
		<!-- /. PAGE WRAPPER  -->
	</div>


	<!-- /sidebar menu -->

</div>
<script src="../js/bootstrap.min.js"></script>
<script>
	;
	(function($, window, document, undefined) {

		var pluginName = "metisMenu", defaults = {
			toggle : true
		};

		function Plugin(element, options) {
			this.element = element;
			this.settings = $.extend({}, defaults, options);
			this._defaults = defaults;
			this._name = pluginName;
			this.init();
		}

		Plugin.prototype = {
			init : function() {

				var $this = $(this.element), $toggle = this.settings.toggle;

				$this.find('li.active').has('ul').children('ul').addClass(
						'collapse in');
				$this.find('li').not('.active').has('ul').children('ul')
						.addClass('collapse');

				$this.find('li').has('ul').children('a').on(
						'click',
						function(e) {
							e.preventDefault();

							$(this).parent('li').toggleClass('active')
									.children('ul').collapse('toggle');

							if ($toggle) {
								$(this).parent('li').siblings().removeClass(
										'active').children('ul.in').collapse(
										'hide');
							}
						});
			}
		};

		$.fn[pluginName] = function(options) {
			return this.each(function() {
				if (!$.data(this, "plugin_" + pluginName)) {
					$.data(this, "plugin_" + pluginName, new Plugin(this,
							options));
				}
			});
		};

	})(jQuery, window, document);
</script>
<script>
	(function($) {
		"use strict";
		var mainApp = {

			initFunction : function() {
				/*MENU 
				------------------------------------*/
				$('#main-menu').metisMenu();

				$(window).bind("load resize", function() {
					if ($(this).width() < 768) {
						$('div.sidebar-collapse').addClass('collapse')
					} else {
						$('div.sidebar-collapse').removeClass('collapse')
					}
				});

			},

			initialization : function() {
				mainApp.initFunction();

			}

		}
		// Initializing ///

		$(document).ready(function() {
			mainApp.initFunction();
		});

	}(jQuery));
</script>
