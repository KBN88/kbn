<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HelpDesk</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
	
	</head>

	<body>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" valign="top">Header</td>
      </tr>
      <tr>
        <td align="left" valign="top"><div class="helpMenu">
            <ul>
            <li><a href="helpDesk.jsp" class="helpselect">Help</a></li>
            <li><a href="contactSupport.jsp" class="contact">Contact</a></li>
          </ul>
          </div></td>
      </tr>
      <tr>
        <td height="20" align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="60%" border="0" align="center" cellpadding="0" cellspacing="0" class="whiteBg">
            <tr>
            <td align="center"><h6>Welcome to the Mmadpay knowledge base.<br />
                What can we help you with today?</h6></td>
          </tr>
            <tr>
            <td height="55" align="center" valign="top"><label for="textfield"></label>
                <input type="text" name="textfield" id="textfield" class="textfild" />
                <input type="submit" name="button" id="button" value="Search" class="helpButton" /></td>
          </tr>
          </table></td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td width="18%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                <td height="30" align="left" valign="middle" class="boxheading">Category</td>
              </tr>
                <tr>
                <td align="left" valign="top" bgcolor="#FFFFFF"><div class="helpleft">
                    <ul>
                      <li><a href="#">Tasks</a></li>
                      <li><a href="#">Task Type</a></li>
                      <li><a href="#">Files</a></li>
                      <li><a href="#">Projects</a></li>
                    </ul>
                  </div></td>
              </tr>
              </table></td>
            <td width="1%" align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="taskhelp">
                <tr>
                <td height="30" align="left" valign="middle" class="boxheading">Tasks</td>
              </tr>
                <tr>
                <td align="left" valign="top" class="tdPad"><div class="basic" id="list1b"> <a>How to create a new Task?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>Click on "Create Task" button or click Task from "+Add" menu. This will open up create task page. Enter Title of the task.</li>
                        <li>By default, current project selected in project list box, you can always choose different project. Then choose Task Type, Priority and Assigned to from the respective list box.</li>
                        <li>Attach file or link your files from Google Drive or DropBox.</li>
                        <li>To create a detailed task: Set the due date and choose which task group it will be belong to, Enter estimated hours, choose start time and end time from a list box, enter break time and spent hours will be calculated automatically.</li>
                        <li>Select the users from drop down, you want to notify the new task details via email notification</li>
                      </ol>
                      <img src="../image/demo.jpg"/></div>
                    <a>Can I see tasks of all projects?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>Please select an "All" from the project drop down and you can see all the tasks in a single page.</li>
                      </ol>
                      <img src="../image/demo.jpg"/></div>
                    <a>How to filter Tasks?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>Click on the "Filters" present at the top right corner of "Task Page".</li>
                        <li>Click on the filter type to get the filter options.</li>
                        <li>You can select multiple filters there.</li>
                        <li>You can close the filters one by one or reset them all.</li>
                      </ol>
                      <img src="../image/demo.jpg"/></div>
                    <a>How to view Task details and reply on a task?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>Please click on a task to view the details.</li>
                        <li>Put the task details, where you can specify Status (as In Progress or Resolve or Close), Assign to, Set the priority, start time, end time, break time and select the option "Is Billable?" while replying on a Task.</li>
                        <li>Share your documents (if required) using your Google Drive or DropBox account</li>
                        <li>Select concerned members to send email notification and hit "Post" to reply.</li>
                      </ol>
                      <img src="../image/demo.jpg"/></div>
                    <a>How to edit a Task?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>There are two ways you can edit a task;
                          <ol>
                            <li>On the task listing page, click the drop down icon and select edit to edit the task.<br />
                            <img src="../image/demo.jpg"/></li>
                            
                          </ol>
                        </li>
                        <li>In the task detail page, on the top right hand corner you will be able to see the edit icon
                          <ol>
                            <li>Click on it to edit the task.<br />
                             <img src="../image/demo.jpg"/></li>
                           
                          </ol>
                        </li>
                        <li>Note: A task can only be modified if the status is "NEW" and the user who would have created the task.</li>
                      </ol>
                    </div>
                    <a>Can I resolve or close a Task without any reply?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>Sure, there is an option icon  on left side of each Task, choose from option to resolve or close the task.</li>
                        <li>Also, you can check multiple tasks and do the same by selecting Close or Resolve on the top of the list.<br />
                          <img src="../image/demo.jpg"/></li>
                        <li>You can resolve or close task from the task detail page.<br />
                          <img src="../image/demo.jpg"/></li>
                      </ol>
                    </div>
                    <a>How to change the "task type", "assign to", &amp; "due date" on the task listing page?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>You can change task type from the left side option of each task.</li>
						<li>You can change assign to and due date on the right side of each task listing.</li>
                      </ol>
                      <img src="../image/demo.jpg"/></div>
                    <a>How can I set different tabs above task listing?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>Click "+" on the tab section, Select/Deselect the checkboxes and click save.<br />
<img src="../image/demo.jpg"/></li>
                        <li>You can view the tabs on the tab section.<br />
<img src="../image/demo.jpg"/></li>
                      </ol>
                      </div>
                    <a>How to archive or delete a task?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>Click the option icon  on the task listing; select "Archive" to archive the task.</li>
						<li>You can later restore or remove the archive permanently from the "Archive" icon  in the left.<br />
<img src="../image/demo.jpg"/><br />
<img src="../image/demo.jpg"/></li>
                        <li>Note: Only new tasks created by you can be archived.</li>
                      </ol>
                      </div>
                    <a>How to Create or Import Task ?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>In the dashboard, go to "+Add" Menu.</li>
						<li>Click on "Import &amp; Export" sub-menu present under Company Settings. The screenshot for "Import &amp; Export" has been shown below.<br />
<img src="../image/demo.jpg"/></li>
<li>Click on "Download .csv file" option to downlaod a sample file. Insert you task in the prescribed format of downloaded file.</li>
<li>Click on "Choose File" to upload the .csv file.</li>
<li>Click on "Continue" to add multiple tasks from your .csv file.</li>
<li>You may export your created/uploaded task by clicking this button.<br />
<img src="../image/demo.jpg"/></li>

                      </ol>
                      </div>
                    <a>How to create Task Template?</a>
                    <div class="basicdiv">
                      <ol>
                        <li>Click on Task Template icon  from left side navigation. Click on + Create task Template box.<br />
<img src="../image/demo.jpg"/></li>
                        <li>It will pop out a box , You need to enter template name and its content.<br />
<img src="../image/demo.jpg"/></li>
                      </ol>
                      </div>
                  </div></td>
              </tr>
                <tr>
                <td align="left" valign="top">&nbsp;</td>
              </tr>
                <tr>
                <td align="left" valign="top">&nbsp;</td>
              </tr>
              </table></td>
          </tr>
          </table></td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top">footer</td>
      </tr>
    </table>
</body>
</html>