<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<html>
<head>
<title>MIS Report</title>
<link href="../css/Jquerydatatableview.css" rel="stylesheet" />
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dataTables.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<link href="../css/loader.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function handleChange() {
	reloadTable();
}
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	$(document).ready(function() {
		$(function() {

			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
			$("#dateTo").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});
		$(function() {
			var today = new Date();
			$('#dateTo').val($.datepicker.formatDate('dd-mm-yy', today));
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			renderTable();
		});

	});
	function renderTable() {
		var table = new $.fn.dataTable.Api('#misReportDataTable');
		$.ajaxSetup({
			global : false,
			beforeSend : function() {
				$(".modal").show();
			},
			complete : function() {
				$(".modal").hide();
			}
		});
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}
		var token = document.getElementsByName("token")[0].value;
		$('#misReportDataTable').dataTable(
				{
					dom : 'BTftlpi',
					columnDefs : [ {
						targets : 0,
						render : function(data, type, row, info) {
							return parseInt(info.row) + 1;
						}
					} ],
					buttons : [
							{
								extend : 'copyHtml5',
								exportOptions : {
									columns : [ 0, 1, 17, 18, 4, 6, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ]
								}
							},
							{
								extend : 'csvHtml5',
								title : 'MIS Report',
								exportOptions : {
									columns : [ 0, 1, 17, 18, 4, 6, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ]
								}
							},
							{
								extend : 'colvis',
								columns : [ 1, 3, 4, 6, 8, 14 ]
							} ],
					"ajax" : {
						"url" : "acquirerMisSettementReport",
						"type" : "POST",
						"data" : function (d){
							return generatePostData(d);
						}
					},
					  "processing": true,
				        "serverSide": true,
				        "paginationType": "full_numbers", 
				        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				        "order" : [ [ 1, "desc" ] ], 
					"columns" : [ {
						"data" : "",
						"width" : '1%'
					}, {
						"data" : "businessName",
						"width" : '8%'
					}, {
						"data" : "payId",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : "transactionId",
						"width" : '8%'
					}, {
						"data" : "accountNumber",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : "aggregatorName",
						"width" : '8%',
						"defaultContent" : "ASIANCHEKOUT PAYMENTS SOLUTION PVT LTD"
					}, {
						"data" : "nodalAccount",
						"width" : '8%',
						"defaultContent" : "'50200011639867"

					},

					{
						"data" : "txnType",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : "txnDate",
						"width" : '8%'
					}, {
						"data" : "bankName",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : "ifscCode",
						"visible" : false,
						"className" : "displayNone"

					}, {
						"data" : "bankRecieveFund",
						"width" : '7%'

					}, {
						"data" : "approvedAmount",
						"width" : '8%'

					}, {
						"data" : "totalAggregatorcommissionAmount",
						"width" : '7%'

					}, {
						"data" : "totalAmountPayToMerchant",
						"width" : '7%'
					}, {
						"data" : "totalPayoutToNodal",
						"width" : '7%'
					}, {
						"data" : "currentDate",
						"visible" : false,
						"className" : "displayNone"
					}, {
						"data" : null,
						"width" : '14%',
						"visible" : false,
						"mRender" : function(row) {
							return "\u0027" + row.payId;
						}
					}, {
						"data" : null,
						"width" : '14%',
						"visible" : false,
						"mRender" : function(row) {
							return "\u0027" + row.transactionId;
						}
					}, ]
				});
	}

	function reloadTable() {
		var datepick = $.datepicker;
		var transFrom = $.datepicker
				.parseDate('dd-mm-yy', $('#dateFrom').val());
		var transTo = $.datepicker.parseDate('dd-mm-yy', $('#dateTo').val());
		if (transFrom == null || transTo == null) {
			alert('Enter date value');
			return false;
		}

		if (transFrom > transTo) {
			alert('From date must be before the to date');
			$('#dateFrom').focus();
			return false;
		}
		if (transTo - transFrom > 31 * 86400000) {
			alert('No. of days can not be more than 31');
			$('#dateFrom').focus();
			return false;
		}

		var tableObj = $('#misReportDataTable');
		var table = tableObj.DataTable();
		table.ajax.reload();
	}
	function decodeDiv() {
		var divArray = document.getElementsByTagName('div');
		for (var i = 0; i < divArray.length; ++i) {
			var div = divArray[i];
			if (div.id.indexOf('param-') > -1) {
				var val = div.innerHTML;
				div.innerHTML = decodeVal(val);
			}
		}
	}

	function decodeVal(value) {
		var txt = document.createElement("textarea");
		txt.innerHTML = value;
		return txt.value;
	}

	function generatePostData(d) {
		var token = document.getElementsByName("token")[0].value;
		var obj = {
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateTo").value,
			merchantEmailId : document.getElementById("merchants").value,
			draw : d.draw,
			length :d.length,
			start : d.start,
			token : token,
			"struts.token.name" : "token",
		};

		return obj;
	}
	
</script>
<script type="text/javascript">
	
</script>
<style>
.dataTables_wrapper {
	position: relative;
	clear: both;
	*zoom: 1;
	zoom: 1;
	margin-top: -30px;
}
</style>
</head>
<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" valign="top"><table width="100%" border="0"
					align="center" cellpadding="0" cellspacing="0" class="txnf">
					<tr>
						<td colspan="5"><h2>MIS Reports</h2>
							<div class="container">

								<s:if
									test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
									<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">
										<div class="txtnew">
											<label for="acquirer">Acquirer Name:</label><br />
											<s:select headerKey="ALL" headerValue="ALL" id="account"
												name="account" class="form-control"
												list="@com.kbn.pg.core.NodalAccountMapper@values()"
												listKey="code" listValue="name" onchange="handleChange();" autocomplete="off" />
										</div>
									</div>
								</s:if>

								<s:if
									test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
									<div class="form-group col-md-2 col-xs-6 col-sm-3 txtnew">

										<div class="txtnew">
											<label for="acquirer">Merchant Name:</label><br />
											<s:select name="merchants" class="form-control"
												id="merchants" headerKey="ALL" headerValue="ALL"
												list="merchantList" listKey="emailId"
												listValue="businessName" onchange="handleChange();" autocomplete="off" />
										</div>
									</div>
								</s:if>

								<s:else>
									<div class="form-group col-md-2 col-sm-3 col-xs-6 txtnew">


										<div class="txtnew">
											<label for="acquirer">Merchant Name:</label><br />
											<s:select name="merchants" class="form-control"
												id="merchants" headerKey="ALL" headerValue="ALL"
												list="merchantList" listKey="emailId"
												listValue="businessName" onchange="handleChange();" autocomplete="off" />
										</div>

									</div>
								</s:else>
								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateFrom">Date From:</label> <br />
									<s:textfield type="text" readonly="true" id="dateFrom"
										name="dateFrom" class="form-control" autocomplete="off"
										onchange="handleChange();" />
								</div>
								<div class="form-group  col-md-2 col-sm-3 txtnew col-xs-6">
									<label for="dateTo">Date To:</label> <br />
									<s:textfield type="text" readonly="true" id="dateTo"
										name="dateTo" class="form-control" onchange="handleChange();" autocomplete="off" />
								</div>
							</div></td>
					</tr>
					<tr>
						<td colspan="5" align="left" valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td align="left" valign="top" style="padding: 10px;">
							<div class="scrollD" id="dvAllAcquire" style="display: block;">
								<table id="misReportDataTable" align="center" cellspacing="0"
									width="100%">
									<thead>
										<tr class="boxheadingsmall">
											<th>S.No</th>
											<th>Merchant Name</th>
											<th>Pay Id</th>
											<th>Txn_Id</th>
											<th>Sub Merchant Account No.</th>
											<th>Aggregator Name</th>
											<th>Nodal a/c No.</th>
											<th>Transaction Type(Sale/Refund/Hold/Chargeback)</th>
											<th>Transaction Date</th>
											<th>Beneficiary Bank Name</th>
											<th>Beneficiary IFSC code</th>
											<th>Bank from where funds to be received</th>
											<th>Gross Transaction Amt</th>
											<th>Total Aggregator Commission Amt Payable (Including Service Tax)</th>
											<th>Total Amt Payable to Merchant A/c</th>
											<th>Total Payout from Nodal Account</th>
											<th>T' DATE</th>
											<th>Pay Id</th>
											<th>Txn_Id</th>

										</tr>
									</thead>
								</table>
							</div>
					</td>
					</tr>
				</table></td>
		</tr>
	</table>
</body>
</html>