
<%@page import="com.kbn.crm.commons.action.ForwardAction"%>
<%@page import="com.kbn.commons.user.User"%>
<%@page import="com.kbn.commons.util.Constants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.kbn.pg.core.Currency"%>
<%@page import="com.kbn.pg.core.Amount"%>
<%@ page import="org.owasp.esapi.ESAPI"%>
<%@page import="com.kbn.commons.util.FieldType"%>
<%@page import="org.apache.log4j.Logger"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Profile</title>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.dimensions.js"></script>
<script type="text/javascript" src="../js/jquery.accordion.js"></script>

<script type="text/javascript">
	function sendDefaultCurrency() {
		var token = document.getElementsByName("token")[0].value;
		var dropDownOption = document.getElementById("defaultCurrency").options;
		var dropDown = document.getElementById("defaultCurrency").options.selectedIndex;
		var payId = '<s:property value="#session.USER.payId" />';
		$
				.ajax({
					url : 'setDefaultCurrency',
					type : 'post',
					data : {
						payId : payId,
						defaultCurrency : document
								.getElementById("defaultCurrency").value,
						token : token
					},
					success : function(data) {
						var responseDiv = document.getElementById("response");
						responseDiv.innerHTML = data.response;
						responseDiv.style.display = "block";
						 var responseData = data.response;
						 if(responseData == null){
							 responseDiv.innerHTML = "Operation not successfull, please try again later!!"
							 responseDiv.style.display = "block";
							 responseDiv.className = "error error-new-text";
							 event.preventDefault();
					 }
						var currencyDropDown = document
								.getElementById("defaultCurrency");
						responseDiv.className = "success success-text";
					},
					error : function(data) {
						var responseDiv = document.getElementById("response");
						responseDiv.innerHTML = "Error updating default currency please try again later!!"
						responseDiv.style.display = "block";
						responseDiv.className = "error error-new-text";
					}
				});
	}
</script>
<style type="text/css">.error-text{color:#a94442;font-weight:bold;background-color:#f2dede;list-style-type:none;text-align:center;list-style-type: none;margin-top:10px;
}.error-text li { list-style-type:none; }</style>
</head>
<body>
<div class="error-text"><s:actionmessage/></div>
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td height="10" align="center">
				<ul id="tabs" class="nav nav-tabs" data-tabs="tabs"
					style="border-bottom: none;">
					<li class="active"><a href="#MyPersonalDetails"
						data-toggle="tab">My Personal Details</a></li>
					<li><a href="#MyContactDetails" data-toggle="tab">My
							Contact Details</a></li>
				</ul>
			</td>
			<td align="center">&nbsp;</td>
		</tr>

		<tr>
			<td height="10" align="center">
				<div id="my-tab-content" class="tab-content">
					<div class="tab-pane active" id="MyPersonalDetails">
						<br>
						<br>
						 <div style="display:none"  id = "response"></div>
						<s:div>
							<%
								    Logger logger = Logger.getLogger("Profile Admin");
									String currencyCodeAlpha = "";
									try {
										User sessionUser = (User) session
												.getAttribute(Constants.USER.getValue());
										String currencyNumeric = sessionUser.getDefaultCurrency();
										currencyCodeAlpha = Currency
												.getAlphabaticCode(currencyNumeric);
									} catch (Exception exception) {
										logger.error("Exception on Profile Admin page " + exception);
									}
							%>
							<table width="70%" align="center" class="product-specbigstripes">
								<tr>
									<td width="30%" height="25" align="left"
										class="greytdbg borderleftradius"><strong>Business
											name:</strong></td>
									<td width="70%" align="left" class="greytdbg borderrightradius"><s:property
											value="#session.USER.businessName" /></td>
								<tr>
									<td height="30" align="left" valign="middle"><strong>Email
											ID:</strong></td>
									<td align="left"><s:property
											value="#session.USER.emailId" /></td>
								</tr>
								<tr>
									<td height="30" align="left" class="greytdbg"><strong>First
											Name:</strong></td>
									<td align="left" class="greytdbg"><s:property
											value="#session.USER.firstName" /></td>
								</tr>
								<tr>
									<td height="30" align="left"><strong>Last Name:</strong></td>
									<td align="left"><s:property
											value="#session.USER.lastName" /></td>
								</tr>

								<tr>
									<td height="30" align="left" class="greytdbg"><strong>Company
											Name:</strong></td>
									<td align="left" class="greytdbg"><s:property
											value="#session.USER.companyName" /></td>
								</tr>
								<tr>
									<td height="30" align="left" class="borderbtmleftradius"><strong>Business
											Type:</strong></td>
									<td align="left" class="borderbtmrightradius"><s:property
											value="#session.USER.businessType" /></td>
								</tr>
								<tr>
									<td height="30" align="left" class="borderbtmleftradius"><strong>Default
											Currency:</strong></td>
									<td align="left" class="borderbtmrightradius">
										<table>
											<tr>
											<td style="border: none !important;" height="30"><s:select
														name="defaultCurrency" id="defaultCurrency"
														list="currencyMap" style="width:100px; display:inline;"
														class="form-control"/></td>
												<td style="border: none !important;" height="30"><input
													type="button" id="btnSave" name="btnSave"
													class="btn btn-success btn-md" value="Submit"
													onclick="sendDefaultCurrency()" style="display: inline;">
												</td>
											</tr>


										</table>
									</td>
								</tr>
						
							</table>
						</s:div>

					</div>

					<div class="tab-pane" id="MyContactDetails">
						<br>
						<br>
						<s:div>
							<table width="60%" align="center" border="0" cellspacing="0"
								cellpadding="0" class="product-specbigstripes">

								<tr>
									<td width="30%" height="25" align="left" valign="middle"
										class="greytdbg borderleftradius"><strong>Mobile:</strong></td>
									<td width="70%" align="left" class="greytdbg borderrightradius"><s:property
											value="#session.USER.mobile" /></td>
								</tr>
								<tr>
									<td height="25" align="left" valign="middle"><strong>Telephone
											No.:</strong></td>
									<td align="left"><s:property
											value="#session.USER.telephoneNo" /></td>
								</tr>
								<tr>
									<td height="25" align="left" valign="middle" class="greytdbg"><strong>Address:</strong></td>
									<td align="left" class="greytdbg"><s:property
											value="#session.USER.address" /></td>
								</tr>
								<tr>
									<td height="25" align="left" valign="middle"><strong>City:</strong></td>
									<td align="left"><s:property value="#session.USER.city" /></td>
								</tr>
								<tr>
									<td height="25" align="left" valign="middle" class="greytdbg"><strong>State:</strong></td>
									<td align="left" class="greytdbg"><s:property
											value="#session.USER.state" /></td>
								</tr>
								<tr>
									<td height="25" align="left" valign="middle"><strong>Country:</strong></td>
									<td align="left"><s:property
											value="#session.USER.country" /></td>
								</tr>
								<tr>
									<td height="25" align="left" valign="middle"
										class="greytdbg borderbtmleftradius"><strong>Postal
											Code:</strong></td>
									<td align="left" class="greytdbg borderbtmrightradius"><s:property
											value="#session.USER.postalCode" /></td>
								</tr>

							</table>
						</s:div>
					</div>
					
				</div>
		</tr>
	</table>
	<script>
		jQuery(document).ready(function($) {
			$('#tabs').tab();
		});
	</script>
	</body>
</html>