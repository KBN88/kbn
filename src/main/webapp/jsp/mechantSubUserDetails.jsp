<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="s" uri="/struts-tags" %>
<html dir="ltr" lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Merchant Accounts</title>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<link href="../css/Jquerydatatable.css" rel="stylesheet" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../js/dataTables.buttons.js"></script>
<script type="text/javascript" src="../js/pdfmake.js"></script>
<link href="../css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
	<div class="form-group col-md-2 txtnew col-sm-4 col-xs-6">
						<label for="merchant">Merchant List:</label> <br />
						<s:if test="%{#session.USER.UserType.name()=='ADMIN' || #session.USER.UserType.name()=='SUBADMIN'}">
						 <s:select name="merchants" class="form-control"
									id="merchant" headerKey="ALL" headerValue="ALL"
									listKey="emailId" listValue="businessName"
									list="merchantList" autocomplete="off" onchange="handleChange();" />
						</s:if>
						<s:else>
						</s:else>
					</div>
	<table width="100%" align="left" cellpadding="0" cellspacing="0" class="formbox">		
		<tr>
		<td align="left" style="padding:10px;"><br /><br />
        <div class="scrollD">
	<table id="datatable" class="display" cellspacing="0" width="100%">
		<thead>
			<tr class="boxheadingsmall">
				<th>Pay Id</th>
				<th>Email</th>
				<th>Business Name </th>
				<th>Status</th>
				<th>UserType</th>
				<th>Mobile</th>
				<th>Registration Date</th>
				<th>Pay Id</th>
			</tr>
		</thead>
	</table>
    </div>
    </td></tr></table>
	<script type="text/javascript">
	$(document).ready(function() {
		$(function() {
		renderTable();
		});
	});
	function handleChange() {
		reloadTable();
	}
	function decodeVal(text) {
		return $('<div/>').html(text).text();
	}
	function renderTable() {
			var token  = document.getElementsByName("token")[0].value;
			$('#datatable').dataTable({
				dom : 'BTftlpi',
				buttons : [ {
					extend : 'copyHtml5',
					exportOptions : {
						columns : [ ':visible' ]
					}
				}, {
					extend : 'csvHtml5',
					title : 'Merchant List',
					exportOptions : {
						columns : [ 8, 1, 2, 3, 4, 5, 6 ]
					}
				}, {
					extend : 'pdfHtml5',
					title : 'Merchant List',
					exportOptions : {
						columns : [ ':visible' ]
					}
				}, {
					extend : 'print',
					title : 'Merchant List',
					exportOptions : {
						columns : [ 0, 1, 2, 3, 4, 5, 6]
					}
				},{
					extend : 'colvis',
					//           collectionLayout: 'fixed two-column',
					columns : [ 1, 2, 3, 4, 5, 6,7]
				}],			
				"ajax" : {
					"url" : "merchantSubUserList",
					"type" : "POST",
					"data" : generatePostData
				},
				"bProcessing" : true,
				"bLengthChange" : true,
				"bAutoWidth" : false,
				"iDisplayLength" : 10,
				"order": [[ 5, "desc" ]],
				"aoColumns" : [ {
					"mData" : "payId"
				}, {
					"mData" : "emailId"
				}, 
				{
					"mData" : "businessName"
				},{
					"mData" : "status"
				},{
					"mData" : "userType"
				},	{
					"mData" : "mobile"
				},{
					"mData" : "registrationDate"
				},{
					"data" : null,
					"visible" : false,
					"className" : "displayNone",
					"mRender" : function(row) {
			              return "\u0027" + row.payId;
			       }
				} ]
			});
		}
		function reloadTable() {
			var tableObj = $('#datatable');
			var table = tableObj.DataTable();
			table.ajax.reload();
		}
		function generatePostData() {
			var token = document.getElementsByName("token")[0].value;
			var payId = null;
			if(null != document.getElementById("merchant")){
				payId = document.getElementById("merchant").value;
			}else{
				payId = 'ALL';
			}
			var obj = {				
					token : token,
					payId : payId,
			};

			return obj;
		}
	</script>
 </body>
</html>