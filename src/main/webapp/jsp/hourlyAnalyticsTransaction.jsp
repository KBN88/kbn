<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Analytics</title>
<link href="../css/jquery-ui.css" rel="stylesheet" />
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/highcharts.js"></script>
<script src="../js/highchart.exporting.js"></script>
<script src="../js/jquery.popupoverlay.js"></script>
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/popup.css" />
<script type="text/javascript" src="../js/daterangepicker.js"></script>
<script>
	$(document).ready(function() {
		handleChange();
		$("#buttonDay").click(function(env) {
			lineChart();
		});
		$(function() {
			$("#dateFrom").datepicker({
				prevText : "click for previous months",
				nextText : "click for next months",
				showOtherMonths : true,
				dateFormat : 'dd-mm-yy',
				selectOtherMonths : false,
				maxDate : new Date()
			});
		});

		$(function() {
			var today = new Date();
			$('#dateFrom').val($.datepicker.formatDate('dd-mm-yy', today));
			lineChart();
			
		});
		$("#submit").click(function(env) {
			lineChart();
		});
	});
	function handleChange() {
		lineChart();
		
	}
</script>
<script type="text/javascript">
function lineChart(){
	var token = document.getElementsByName("token")[0].value;
	$.ajax({
		url : "hourlyLineChartAction",
		type : "POST",
		data : {
			dateFrom : document.getElementById("dateFrom").value,
			dateTo : document.getElementById("dateFrom").value,
			payId : document.getElementById("merchant").value,
			industryTypes : document.getElementById("industryTypes").value,
			currency : document.getElementById("currency").value,
			token : token,
			"struts.token.name" : "token",
		},
		success : function(data) {
			var a=[];
			var b=[];
			var c=[];
			var pieChartList = data.pieChart;
			for (var i = 0; i < pieChartList.length; i++) {
				var piechart = pieChartList[i];
				var success =parseInt(piechart.totalSuccess);
				var refund =parseInt(piechart.totalRefunded);
				var failled =parseInt(piechart.totalFailed);
				a.push(success);
				b.push(refund);
				c.push(failled);
			}
$(function () {
    $('#container').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ['1', '2', '3', '4', '5', '6',
                '7', '8', '9', '10', '11', '12','13', '14', '15','16','17','18','19','20','21','22','23','24']
        },
        yAxis: {
            title: {
                text: 'Number Of Transaction'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Total Success',
            data:a
        }, {
            name: 'Total Refunded',
            data: b
        }, {
            name: 'Total Failed',
            data:c
        }]
    });
});
		}
});
}
</script>
</head>
<body onload="handleChange();"
	style="margin: 0px; padding: 0px;">
	<table width="100%" align="left" cellpadding="0" cellspacing="0"
		class="txnf">
		<tr>
			<td align="left"><h2>Hourly Transactions</h2>
				<div class="container">
					<div class="form-group col-md-2 txtnew col-sm-4 col-xs-6">
						<label for="merchants">Merchants:</label><br />
						<s:if
							test="%{#session.USER.UserType.name()=='ADMIN' ||  #session.USER.UserType.name()=='SUBADMIN' || #session.USER_TYPE.name()=='SUPERADMIN'}">
							   <s:select name="merchant" class="form-control" id="merchant"
								headerKey="ALL" headerValue="ALL" list="merchantList"
								listKey="payId" listValue="businessName"
								onchange="handleChange();"/>
						</s:if>
						<s:else>
							<s:select name="merchant" class="form-control" id="merchant"
								headerKey="" headerValue="ALL" list="merchantList"
								listKey="payId" listValue="businessName"
								onchange="handleChange();" />
						</s:else>
					</div>
						<div class="form-group col-md-2 txtnew col-sm-4 col-xs-6">
						<label for="merchant">Business Type:</label> <br />
							<s:select headerKey="ALL" headerValue="ALL" name="industryTypes"  id="industryTypes"
								class="form-control"  list="industryTypes" value="ALL"
								onchange="handleChange();"/>
					</div>
					<div class="form-group col-md-2 col-xs-6 col-sm-4 txtnew">
						<label for="currency">Currency:</label><br />
						<s:select name="currency" id="currency" list="currencyMap"
							class="form-control" onchange="handleChange();" />
					</div>
					<div class="form-group col-md-2 col-xs-6 col-sm-4 txtnew">
						<label for="dateFrom">Date From:</label> <br />
						<s:textfield type="text" readonly="true" id="dateFrom"
							name="dateFrom" class="form-control" autocomplete="off"
							onchange="handleChange();" />
					</div>
				</div>
		</tr>
		<tr>
			
		</tr>
	</table>
	  <div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="panel panel-default">
						<div class="borderbottom">
						
						<div class="row">
						<div class="col-md-12 col-xs-12">Hourly Transactions</div>						
						</div>
						</div>
						<div class="panel-body scrollD">
                       <div id="container" style="min-width: 305px; height: 350px; margin: 0 auto"></div>
						</div>							
					</div>		
				</div> 
				</div> 	
</body>
</html>