<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>
<%@taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="refresh" content="905;url=redirectLogin" /> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">   
    <title><decorator:title default="KBN Payments" /></title>
   
<decorator:head />

	<script>
	//to show new loader
	 $.ajaxSetup({
           global: false,
           beforeSend: function () {
           	toggleAjaxLoader();
           },
           complete: function () {
           	toggleAjaxLoader();
           }
       });
		if (self == top) {
			var theBody = document.getElementsByTagName('body')[0];
			if(theBody!=null){
				theBody.style.display = "block";	
			}
		} else {
			top.location = self.location;
		}
	</script>	

	<script src="../js/loader/modernizr-2.6.2.min.js"></script>
	<script src="../js/loader/main.js"></script>
</head>


<body onLoad="" class="nav-md smart-style-3 fixed-navigation fixed-header fixed-ribbon fixed-page-footer">

    <div class="container body">
	<s:set name ="tokenS" value="%{#session.customToken}"/>
	<s:hidden id="token" name="token" value="%{tokenS}"></s:hidden>
        <div class="main_container">
         <s:if test="%{#session.USER.UserType.name()=='SUPERADMIN'}"> <%@ include file="/jsp/menuSuperAdmin.jsp"%> </s:if> 		
          <s:if test="%{#session.USER.UserType.name()=='ADMIN'}"> <%@ include file="/jsp/menuAdmin.jsp"%> </s:if> 
          <s:elseif test="%{#session.USER.UserType.name()=='SUBADMIN'}"><%@ include file="/jsp/menuSubAdmin.jsp"%></s:elseif>
          <s:elseif test="%{#session.USER.UserType.name()=='MERCHANT'}"><%@ include file="/jsp/menuMerchant.jsp"%></s:elseif>
		  <s:elseif test="%{#session.USER.UserType.name()=='SUBUSER'}"><%@ include file="/jsp/menuSubUser.jsp"%></s:elseif>
		  <s:elseif test="%{#session.USER.UserType.name()=='POSMERCHANT'}"><%@ include file="/jsp/menuMerchant.jsp"%></s:elseif>
		  <s:elseif test="%{#session.USER.UserType.name()=='RESELLER'}"><%@ include file="/jsp/menuReseller.jsp"%></s:elseif>
		  <s:elseif test="%{#session.USER.UserType.name()=='ACQUIRER'}"><%@ include file="/jsp/menuAcqire.jsp"%></s:elseif>
		  <s:elseif test="%{#session.USER.UserType.name()=='SUBACQUIRER'}"><%@ include file="/jsp/menuAcquirerSubUser.jsp"%></s:elseif>
            <!-- page content -->
           <div class="right_col">
 <div class="row"><div class="col-md-12 text-left"><a href="home" class="newredtxt">Home</a> | <a href="javascript:window.history.back();" class="newredtxt">Back</a></div></div>
			<decorator:body />
			
              <!-- programmer work eara content -->
            </div>
            <%@ include file="/jsp/footer.jsp"%> 	
            <!-- /page content -->

        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
    <s:token/>
</body>
</html>
	